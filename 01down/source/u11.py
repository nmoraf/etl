# coding: latin1

"""
.
"""

import multiprocessing.pool as p

import shared as s
import sisaptools as u


TABLE = "sisap_u11"
EAPP = "sisap_u11_eapp"


class U11(object):
    """."""

    def __init__(self):
        """."""
        self.get_ficticis()
        self.get_dades()
        self.get_duplicates()
        self.create_tables()
        self.upload_data()
        self.get_eapp()
        self.post_upload_redics()
        self.post_upload_sidics()

    def get_ficticis(self):
        """."""
        sql = "select id from sisap_ficticis"
        self.ficticis = set([id for id,
                             in u.Database(*s.REDICS_DB).get_all(sql)])

    def get_dades(self):
        """."""
        self.dades = []
        fets = {}
        c_id_cip = 0
        c_id_cip_sec = 0
        pool = p.Pool(len(s.SECTORS_DB))
        captura = pool.map(self._worker, s.SECTORS_DB, chunksize=1)
        for worker in captura:
            for hash_a, hash_d, sector in worker:
                if hash_a not in self.ficticis:
                    if hash_d in fets:
                        id_cip = fets[hash_d]
                    else:
                        c_id_cip += 1
                        id_cip = c_id_cip
                        fets[hash_d] = id_cip
                    if (hash_d, sector) in fets:
                        id_cip_sec = fets[(hash_d, sector)]
                    else:
                        c_id_cip_sec += 1
                        id_cip_sec = c_id_cip_sec
                        fets[(hash_d, sector)] = id_cip_sec
                    this = (hash_a, hash_d, sector, id_cip, id_cip_sec)
                    self.dades.append(this)

    def _worker(self, sector):
        """."""
        sql = "select cip_cip_anterior, cip_usuari_cip, cip_sector_codi_sector \
               from redics.usutb011_t \
               where cip_sector_codi_sector = '{}'".format(sector)
        return list(u.Database(*s.REDICS_DB).get_all(sql))

    def get_duplicates(self):
        """."""
        self.duplicates = set()
        fets = set()
        for hash_a, hash_d, sector, id_cip, id_cip_sec in self.dades:
            key = (hash_a, sector)
            if key in fets:
                self.duplicates.add(key)
            else:
                fets.add(key)

    def create_tables(self):
        """."""
        columns_m = ("hash_a varchar(40)", "hash_d varchar(40)",
                     "sector varchar(4)", "id_cip int", "id_cip_sec int")
        columns_o = [col.replace("varchar", "varchar2") for col in columns_m]
        with u.Database(*s.REDICS_DB) as redics:
            for table in (TABLE, EAPP):
                redics.create_table(table, columns_o, remove=True)
        with u.Database(*s.SIDICS_DB) as sidics:
            sidics.execute("set global max_heap_table_size = 1024 * 1024 * 1024 * 16")  # noqa
            sidics.execute("set global tmp_table_size = 1024 * 1024 * 1024 * 16")  # noqa
        with u.Database(*s.SIDICS_DB) as sidics:
            for table in (TABLE, EAPP):
                sidics.create_table(table, columns_m, storage="MEMORY", remove=True)  # noqa

    def upload_data(self):
        """."""
        upload = []
        i = 0
        for row in self.dades:
            if (row[0], row[2]) not in self.duplicates:
                upload.append(row)
                i += 1
                if i % 10**5 == 0:
                    U11.__upload_data_worker(upload)
                    upload = []
        U11.__upload_data_worker(upload)

    @staticmethod
    def __upload_data_worker(upload):
        """."""
        for db in (s.REDICS_DB, s.SIDICS_DB):
            with u.Database(*db) as conn:
                conn.list_to_table(upload, TABLE)

    def get_eapp(self):
        """."""
        done = {}
        data = []
        max = 0
        sql = "select cip_cip_anterior, cip_usuari_cip from redics51.usutb011r"
        for hash_a, hash_d in u.Database(*s.REDICS_DB).get_all(sql):
            if hash_d in done:
                id = done[hash_d]
            else:
                max -= 1
                id = max
                done[hash_d] = id
            data.append([hash_a, hash_d, "6951", id, id])
        for desti in (s.REDICS_DB, s.SIDICS_DB):
            with u.Database(*desti) as db:
                db.list_to_table(data, EAPP)

    def post_upload_redics(self):
        """."""
        with u.Database(*s.REDICS_DB) as redics:
            columns = "(hash_a, sector, id_cip, id_cip_sec)"
            for table in (TABLE, EAPP):
                sql = "create index {0}_IDX on {0} {1}".format(table, columns)
                redics.execute(sql)
                redics.set_grants("select", table, "PDP")
                redics.set_statistics(table)

    def post_upload_sidics(self):
        """."""
        sqls = ("alter table {} add unique (hash_a, sector) using hash",
                "alter table {} add index (hash_a) using hash")
        with u.Database(*s.SIDICS_DB) as sidics:
            for table in (TABLE, EAPP):
                for sql in sqls:
                    sidics.execute(sql.format(table))
