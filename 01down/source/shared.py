# coding: latin1

"""
.
"""

import os


DESTI_DB = ("p2262", "import")
EAPP_DB = ("p2262", "import_jail")

REDICS_DB = ("redics", "data")
PDP_DB = ("redics", "pdp")
SIDICS_DB = ("sidics", "sidics")
SECTORS_DB = ("6102", "6209", "6211", "6310", "6416", "6519", "6520", "6521",
              "6522", "6523", "6625", "6626", "6627", "6728", "6731", "6734",
              "6735", "6837", "6838", "6839", "6844")

STRUCTURE_FILE = "../proc/structure.json"
FATHER_FILE = "../proc/__dataFather.csv"
CHILD_FILE = "../proc/dataChild.csv"
BLACK_FILE = "../proc/dataBlack.csv"
SPECIAL_FILE = "../proc/special.json"
CATALEGS_FILE = "../proc/catalegs.json"

TMP_FOLDER = os.environ["TMP_FOLDER"]
KING_FILE = TMP_FOLDER + "/download.json"

ORIGEN_DB = {"redics": REDICS_DB, "sidics": SIDICS_DB}
QUEUE_PROCS = {"redics": 40, "sidics": 16, "upload": 32}
QUEUE_LENGTH = {"redics": QUEUE_PROCS["redics"], "sidics": 0}
ROWS = {"redics": 10 ** 5, "sidics": 10 ** 5}
CONTROL_EVERY = 2 * 10 ** 6

DELIMITER = "@,"
NEWLINE = "|;"
HOST = os.environ.get("HOST", "unknown")
