# coding: latin1

"""
.
"""

import csv
import cx_Oracle
import simplejson as j
import os

import shared as s
import sisaptools as u


def main():
    """."""
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    with u.Database(*s.DESTI_DB) as desti:
        for sql in j.load(open(s.STRUCTURE_FILE)):
            desti.execute(sql)
    export = []
    reader = csv.reader(open(s.FATHER_FILE), delimiter=";")
    for row in reader:
        this = Download(*row)
        if this.resultat:
            export.append(this.resultat)
    j.dump(export, open(s.KING_FILE, "w"))


class Download(object):
    """."""

    def __init__(self, id, nom, taula, crear, baixar, eapp, sidics):
        """."""
        self.id = id
        self.nom = nom
        self.taula_orig = taula
        self.crear = crear == "1"
        self.creable = crear != "NA"
        self.taula = "sisap_{}".format(self.nom) if self.creable else taula
        self.baixar = baixar == "1"
        self.te_eapp = eapp != "0"
        self.model_eapp = int(eapp)
        self.sidics = sidics == "1"
        self.origen = s.SIDICS_DB if self.sidics else s.REDICS_DB
        self.odn = self.creable and self.nom[:3] == "odn"
        self.resultat = {}
        if self.crear or self.baixar:
            if nom == "catalegs":
                sql = "insert ignore into ctl_create values ('{}', 0, 0, '')"
                u.Database(*s.DESTI_DB).execute(sql.format(self.nom))
                self.resultat = {"domini": self.nom, "crear": True, "baixar": False}  # noqa
            else:
                with u.Database(*self.origen) as self.connexio:
                    self.get_taula_real()
                    self.get_cip()
                    self.get_columnes_raw()
                    if self.creable:
                        self.get_create_origen()
                    if self.baixar:
                        self.get_columnes()
                        self.get_te_sector()
                        self.get_where()
                        self.get_sql()
                        self.get_particions()
                        self.get_create_desti()
                        if self.te_eapp:
                            self.get_eapp()
                    self.set_control()
                    self.get_resultat()

    def get_taula_real(self):
        """."""
        if self.sidics:
            self.taula_real = self.taula
            self.taula_orig_real = self.taula
        elif self.creable:
            self.taula_real = ("PREDUFFA", self.taula.upper())
            self.taula_orig_real = self.connexio.get_table_owner(self.taula_orig)  # noqa
        else:
            self.taula_real = self.connexio.get_table_owner(self.taula)
            self.taula_orig_real = self.taula_real

    def get_cip(self):
        """."""
        hacked = ("REDICS", "PRSTB505R") if self.odn else self.taula_orig_real
        if self.sidics:
            sql = "select column_name \
                   from information_schema.columns \
                   where table_schema = 'sidics' and \
                         table_name = '{}' and \
                         character_maximum_length = 40 \
                   order by ordinal_position".format(hacked)
        else:
            sql = "select column_name \
                   from all_tab_columns \
                   where owner = '{}' and \
                         table_name = '{}' and \
                         data_length = 40 and \
                         nvl(avg_col_len, 40) >= 40 \
                   order by column_id".format(*hacked)
        try:
            self.cip, = self.connexio.get_one(sql)
        except TypeError:
            self.cip = False
        else:
            self.cip = self.cip.lower()

    def get_columnes_raw(self):
        """."""
        self.columnes_raw = [column for (id, column, where)
                             in csv.reader(open(s.CHILD_FILE), delimiter=";")
                             if id == self.id]

    def get_create_origen(self):
        """."""
        special = j.load(open(s.SPECIAL_FILE))["create"]
        standard = """create table {taula}
                      partition by list (codi_sector) ({particions}) as
                      select {cip}, codi_sector, {vars}
                      from {owner_orig_real}.{taula_orig_real}"""
        odn = """create table {taula} as
                 select a.*, b.cfd_cip
                 from {taula_orig} a, prstb505 b
                 where a.{prefix}_cod = b.cfd_cod and
                       a.codi_sector=b.codi_sector"""
        u11 = """create table {taula}
                 (hash_a varchar2(40), hash_d varchar2(40), sectpr varchar2(4),
                  id_cip int, id_cip_sec int)"""
        if self.nom in special:
            base = special[self.nom]
        elif self.odn:
            if self.nom == "odn506":
                base = odn.replace("{taula} as",
                                   "{taula} partition by list (codi_sector) ({particions}) as")  # noqa            
            else:
                base = odn
        elif self.nom == "u11":
            base = u11
        else:
            base = standard
        particions = ', '.join(["partition s{0} values ('{0}')".format(sec)
                                for sec in s.SECTORS_DB])
        particions += ", partition sError values (default)"
        self.create_origen = base.format(
                taula=self.taula,
                particions=particions,
                cip=self.cip,
                vars=", ".join(self.columnes_raw),
                owner_orig_real=self.taula_orig_real[0],
                taula_orig_real=self.taula_orig_real[1],
                taula_orig=self.taula_orig,
                prefix=self.columnes_raw[0].split("_")[0] if self.odn else None
        )
        self._create_if_not_exists()

    def _create_if_not_exists(self):
        """."""
        try:
            if self.nom == "u11":
                sql = self.create_origen
            else:
                particula = " and " if "where" in self.create_origen else " where "  # noqa
                sql = self.create_origen + particula + " 1 = 0"
            self.connexio.execute(sql)
        except cx_Oracle.DatabaseError as e:
            if "ORA-00955" in str(e):
                pass
            else:
                raise

    def get_columnes(self):
        """."""
        self.columnes = [self.connexio.get_column_information(column, self.taula)  # noqa
                         for column in self.columnes_raw]

    def get_te_sector(self):
        """."""
        if self.sidics:
            sql = "select column_name \
                   from information_schema.columns \
                   where table_schema = 'sidics' and \
                         table_name = '{}' and \
                         column_name = 'CODI_SECTOR'".format(self.taula)
        else:
            sql = "select column_name \
                   from all_tab_columns \
                   where owner = '{}' and \
                         table_name = '{}' and \
                         column_name = 'CODI_SECTOR'".format(*self.taula_real)
        try:
            sector, = self.connexio.get_one(sql)
        except TypeError:
            self.te_sector = False
        else:
            self.te_sector = True

    def get_where(self):
        """."""
        self.where = [" ".join((column, where)) for (id, column, where)
                      in csv.reader(open(s.CHILD_FILE), delimiter=";")
                      if id == self.id and where != ""]

    def get_sql(self):
        """."""
        redics = """select /*+NO_INDEX(a) */ id_cip, id_cip_sec, sector, {vars}
                    from {owner}.{taula}{{}} a, preduffa.sisap_u11 b
                    where a.{cip} = b.hash_a
                    {sector}{where}"""
        no_cip = """select 0, 0, {sector_}, {vars} from {owner}.{taula}{where_}"""  # noqa
        sidics = """select id_cip, id_cip_sec, sector, {vars}
                    from {taula}{{}} a
                    inner join sisap_u11 b on a.{cip} = b.hash_a
                    {sector}{where_}"""
        special = j.load(open(s.SPECIAL_FILE))["download"]
        if self.nom in special:
            base = special[self.nom]
        elif not self.cip:
            base = no_cip
        else:
            base = sidics if self.sidics else redics
        self.sql = base.format(
                        vars=", ".join([col["query"] for col in self.columnes]),  # noqa
                        owner=self.taula_real[0],
                        taula=self.taula if self.sidics else self.taula_real[1],  # noqa
                        cip=self.cip,
                        sector=" and a.codi_sector = b.sector" if self.te_sector else "",  # noqa
                        where="" if not self.where else " and " + " and ".join(self.where),  # noqa
                        where_="" if not self.where else " where " + " and ".join(self.where),  # noqa
                        sector_="codi_sector" if self.te_sector else "''"
        )

    def get_particions(self):
        """."""
        self.particions = [("{}_{}".format(self.nom, k),
                            v,
                            self.sql.format(" partition({})".format(k)))
                           for (k, v)
                           in self.connexio.get_table_partitions(self.taula).items()  # noqa
                           if [self.nom, k]
                           not in list(csv.reader(open(s.BLACK_FILE), delimiter=";"))]  # noqa
        if self.particions == []:
            self.particions.append((self.nom,
                                    self.connexio.get_table_count(self.taula),
                                    self.sql.format("")))

    def get_create_desti(self):
        """."""
        self.create_desti = ["id_cip int", "id_cip_sec int", "codi_sector varchar(4)"]  # noqa
        self.create_desti += [col["create"] for col in self.columnes]

    def get_eapp(self):
        """."""
        inexistents = {"visites": ("visi_bloc_codi_bloc", "visi_forcada_s_n")}.get(self.nom, [])  # noqa
        vars = ", ".join(["''" if any([i in col["query"] for i in inexistents])
                          else col["query"] for col in self.columnes])
        where = "" if not self.where else " where " + " and ".join(self.where)
        if self.model_eapp == 9:
            base = j.load(open(s.SPECIAL_FILE))["download"][self.nom] + "_eapp"
            sql = [base.format(vars=vars, where=where)]
            db = s.REDICS_DB
            id = None
        elif self.model_eapp == 1:
            base = "select /*+NO_INDEX(a) */ {hash}, {vars} \
                    from REDICS51.{taula}{where}"
            taula = self.taula_orig_real[1][:8] + "R"
            sql = [base.format(hash=self.cip, vars=vars, taula=taula, where=where)]  # noqa
            db = s.REDICS_DB
            id = "hash"
        elif self.model_eapp == 2:
            base = "select {hash}, {vars} \
                    from {taula} partition(s6951){where}"
            taula = self.taula
            sql = [base.format(hash=self.cip, vars=vars, taula=taula, where=where)]  # noqa
            db = s.SIDICS_DB
            id = "hash"
        elif self.model_eapp == 3:
            base = "select {hash}, {vars} from {taula}{where}"
            taula = self.taula_orig_real[1].replace("SISAP_", "")[:8]
            sql = [base.format(hash=self.cip, vars=vars, taula=taula, where=where)]  # noqa
            db = ("6951", "data")
            id = "cip"
        elif self.model_eapp == 4:
            sql = [sql.replace("sisap_u11", "sisap_u11_eapp")
                   for (_r, _r, sql) in self.particions]
            db = s.SIDICS_DB
            id = None
        elif self.model_eapp == 5:
            sql = ["dummy"]
            db = ""
            id = None
        elif self.model_eapp == 6:
            base = "select /*+NO_INDEX(a) */ {hash}, {vars} \
                    from {taula} partition(s6951) a{where}"
            taula = self.taula
            sql = [base.format(hash=self.cip, vars=vars, taula=taula, where=where)]  # noqa
            db = s.REDICS_DB
            id = "hash"
        self.eapp = {"nom": self.nom, "jobs": sql, "origen": db, "id": id}

    def set_control(self):
        """."""
        origen = "sidics" if self.sidics else "redics"
        sql_c = "insert ignore into ctl_create \
                 values ('{}', 0, 0, '')"
        sql_d = "insert ignore into ctl_down \
                 values ('{}', '{}', '{}', 0, 0, 0, 0, 0, 0, {})"
        with u.Database(*s.DESTI_DB) as desti:
            if self.creable:
                desti.execute(sql_c.format(self.nom))
            if self.baixar:
                for tab, _n, _sql in self.particions:
                    no_control = (
                        self.where != [] or
                        self.nom in j.load(open(s.SPECIAL_FILE))["download"] or
                        self.nom in ("altes_rca_2", "baixes") or
                        "6951" in tab)
                    desti.execute(sql_d.format(tab, self.nom, origen, 1 * no_control))  # noqa
            if self.te_eapp:
                desti.execute(sql_c.format("eapp"))

    def get_resultat(self):
        """."""
        self.resultat = {"domini": self.nom,
                         "crear": self.crear,
                         "baixar": self.baixar}
        if self.crear:
            self.resultat["create_origen"] = self.create_origen
        if self.baixar:
            self.resultat["origen"] = "sidics" if self.sidics else "redics"
            self.resultat["create_desti"] = self.create_desti
            self.resultat["merge"] = len(self.particions) > 1
            self.resultat["jobs"] = self.particions
            if self.te_eapp:
                self.resultat["eapp"] = self.eapp


if __name__ == "__main__":
    main()
