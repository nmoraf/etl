# coding: latin1

"""
.
"""

import simplejson as j
import multiprocessing as m
import os
import time

import shared as s
import sisaptools as u


class Main(object):
    """."""

    def __init__(self):
        """."""
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        for cataleg in j.load(open(s.CATALEGS_FILE)):
            if cataleg.pop("do"):
                Cataleg(**cataleg)


class Cataleg(object):
    """."""

    def __init__(self, taula, origen, owner=None, where=[], post={}):
        """."""
        self.taula_origen = taula
        self.taula_desti = "cat_{}".format(taula)
        self.origens = Cataleg._get_origens(origen)
        self.leader = self.origens[0] if len(self.origens) == 1 else ("6209", "data")  # noqa
        self.owner = owner
        self.where = where
        self.repetir = True
        while self.repetir:
            self.create_table()
            self.get_data()
            for suffix, sql in post.items():
                self.get_post("{}_{}".format(self.taula_desti, suffix),
                              sql.format(self.taula_desti))

    @staticmethod
    def _get_origens(origen):
        """."""
        if origen == "redics":
            return [s.REDICS_DB]
        elif origen == "pdp":
            return [s.PDP_DB]
        elif origen == "sectors":
            return [(sector, "data") for sector in s.SECTORS_DB]
        elif origen == "sectors+":
            return [(sector, "data") for sector in s.SECTORS_DB] + [("6951", "data")]  # noqa
        else:
            return [(origen, "data")]

    def create_table(self):
        """."""
        with u.Database(*self.leader) as origen:
            self.columns = [origen.get_column_information(column, self.taula_origen, owner=self.owner)  # noqa
                            for column
                            in origen.get_table_columns(self.taula_origen, owner=self.owner)]  # noqa
            if len(self.origens) > 1:
                self.columns.insert(0, {"query": "(select codi_sector from pritb999)",  # noqa
                                        "create": "CODI_SECTOR varchar(6)"})
            create = [column["create"] for column in self.columns]
        u.Database(*s.DESTI_DB).create_table(self.taula_desti, create, remove=True)  # noqa

    def get_data(self):
        """."""
        self.errors = m.Manager().list()
        columns = ", ".join([column["query"] for column in self.columns])
        self.sql = "select {} from {{}}.{{}}".format(columns)
        procs = []
        for origen in self.origens:
            proc = m.Process(target=self._worker, args=(origen,))
            procs.append(proc)
        for proc in procs:
            proc.start()
        good = True
        i = 0
        while any([proc.is_alive() for proc in procs]):
            time.sleep(0.5)
            i += 1
            if i == 240:
                for proc in procs:
                    proc.terminate()
                good = False
        if good:
            self.repetir = False
        if self.errors:
            raise BaseException("/n".join(self.errors))

    def _worker(self, origen):
        """."""
        try:
            with u.Database(*origen) as conn:
                if self.owner:
                    owner, taula_real = self.owner, self.taula_origen
                else:
                    owner, taula_real = conn.get_table_owner(self.taula_origen)
                sql = self.sql.format(owner, taula_real)
                if self.where:
                    sql += " where " + " and  ".join(self.where)
                data = list(conn.get_all(sql))
            u.Database(*s.DESTI_DB).list_to_table(data, self.taula_desti)
        except Exception as e:
            info = (self.taula_origen, origen[0], str(e))
            self.errors.append(", ".join(info))

    def get_post(self, taula, sql):
        """."""
        with u.Database(*s.DESTI_DB) as db:
            db.execute("drop table if exists {}".format(taula))
            db.execute("create table {} as {}".format(taula, sql))
