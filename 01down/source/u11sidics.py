# coding: latin1

"""
Execució a demanda per passar U11 de REDICS a SIDICS.
"""

import multiprocessing.pool as p

import shared as s
import sisaptools as u


TABLE = "sisap_u11"
EAPP = "sisap_u11_eapp"


class U11(object):
    """."""

    def __init__(self):
        """."""
        self.create_tables()
        self.get_dades()
        self.post_upload()

    def create_tables(self):
        """."""
        with u.Database(*s.REDICS_DB) as redics:
            columns = [redics.get_column_information(column, TABLE)["create"]
                       for column in redics.get_table_columns(TABLE)]
        with u.Database(*s.SIDICS_DB) as sidics:
            sidics.execute("set global max_heap_table_size = 1024 * 1024 * 1024 * 16")  # noqa
            sidics.execute("set global tmp_table_size = 1024 * 1024 * 1024 * 16")  # noqa
        with u.Database(*s.SIDICS_DB) as sidics:
            for table in (TABLE, EAPP):
                sidics.create_table(table, columns, storage="MEMORY", remove=True)  # noqa

    def get_dades(self):
        """."""
        jobs = []
        digits = [format(digit, 'x').upper() for digit in range(16)]
        for digit1 in digits:
            for digit2 in digits:
                jobs.append(digit1 + digit2)
        pool = p.Pool(32)
        for job in jobs:
            pool.apply_async(self._worker, args=(TABLE, job))
        pool.apply_async(self._worker, args=(EAPP, ""))
        pool.close()
        pool.join()

    def _worker(self, table, digit):
        """."""
        sql = "select * from {} where hash_a like '{}%'".format(table, digit)
        dades = list(u.Database(*s.REDICS_DB).get_all(sql))
        u.Database(*s.SIDICS_DB).list_to_table(dades, table)

    def post_upload(self):
        """."""
        sqls = ("alter table {} add unique (hash_a, sector) using hash",
                "alter table {} add index (hash_a) using hash")
        with u.Database(*s.SIDICS_DB) as sidics:
            for table in (TABLE, EAPP):
                for sql in sqls:
                    sidics.execute(sql.format(table))


if __name__ == "__main__":
    U11()
