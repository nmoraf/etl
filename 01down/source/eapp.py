# coding: latin1

"""
.
"""

import simplejson as j
import multiprocessing as m
import os

import shared as s
import sisaptools as u


class Main(object):
    """."""

    def __init__(self):
        """."""
        self.conversors = {}
        self.get_hash_to_id()
        self.get_cip_to_id()
        self.get_dominis()
        self.get_bolets()

    def get_hash_to_id(self):
        """."""
        sql = "select hash_a, id_cip from sisap_u11_eapp"
        self.conversors["hash"] = {hash: (id, id, "6951") for (hash, id)
                                   in u.Database(*s.REDICS_DB).get_all(sql)}

    def get_cip_to_id(self):
        """."""
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        self.conversors["cip"] = {cip: self.conversors["hash"][hash]
                                  for (cip, hash)
                                  in u.Database("6951", "pdp").get_all(sql)
                                  if hash in self.conversors["hash"]}

    def get_dominis(self):
        """."""
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        for domini in j.load(open(s.KING_FILE)):
            if "eapp" in domini:
                Domini(self.conversors, **domini["eapp"])

    def get_bolets(self):
        """."""
        sql = "select table_name from all_tables \
               where table_name like 'SISAP_EAPP_%'"
        bolets = [taula for taula, in u.Database(*s.REDICS_DB).get_all(sql)]
        with u.Database(*s.REDICS_DB) as redics, u.Database(*s.EAPP_DB) as eapp:  # noqa
            eapp.execute("set session wait_timeout = 28800")
            for taula in bolets:
                nom = taula[6:]
                cols = [redics.get_column_information(col, taula)
                        for col in redics.get_table_columns(taula)]
                create = ["id_cip int", "id_cip_sec int"]
                create.extend([col["create"] for col in cols][1:])
                eapp.create_table(nom, create, remove=True)
                query = ", ".join([col["query"] for col in cols])
                sql = "select {} from {}".format(query, taula)
                data = (self.conversors["hash"][row[0]] + row[2:]
                        for row in redics.get_all(sql)
                        if row[0] in self.conversors["hash"])
                eapp.list_to_table(data, nom)


class Domini(object):
    """."""

    def __init__(self, conversors, nom, jobs, origen, id):
        """."""
        self.nom = nom
        self.jobs = jobs
        self.origen = origen
        self.id = id
        self.conversors = conversors
        self.create_tables()
        if jobs[0] != "dummy":
            self.get_data()

    def create_tables(self):
        """."""
        with u.Database(*s.DESTI_DB) as desti:
            sqls = ["create database if not exists {eapp}",
                    "create or replace table {eapp}.{nom} as \
                     select * from {imp}.{nom} \
                     where 1 = 0",
                    "create or replace table {imp}.{nom}WithJail \
                     like {imp}.{nom}"]
            sql = "select engine from information_schema.tables \
                   where table_schema = '{}' and \
                         table_name='{}'".format(s.DESTI_DB[1], self.nom)
            engine, = desti.get_one(sql)
            if engine[:3] == "MRG":
                nom, script = desti.get_one("show create table {}.{}".format(
                                                    s.DESTI_DB[1], self.nom))
                actual = script[script.find("UNION=") + 7:][:-1]
                sqls.append("alter table {imp}.{nom}WithJail \
                             UNION=({actual}, {eapp}.{nom})")
            else:
                actual = ""
                sqls.append("alter table {imp}.{nom}WithJail ENGINE=MERGE")
                sqls.append("alter table {imp}.{nom}WithJail \
                             UNION=({imp}.{nom}, {eapp}.{nom})")
            for sql in sqls:
                desti.execute(sql.format(imp=s.DESTI_DB[1], eapp=s.EAPP_DB[1],
                                         nom=self.nom, actual=actual))

    def get_data(self):
        """."""
        if len(self.jobs) == 1:
            self._worker(self.jobs[0])
        else:
            pool = m.Pool(16)
            for sql in self.jobs:
                pool.apply_async(self._worker, args=(sql,))
            pool.close()
            pool.join()

    def _worker(self, sql):
        """."""
        if self.id:
            data = (self.conversors[self.id][row[0]] + row[1:]
                    for row in u.Database(*self.origen).get_all(sql)
                    if row[0] in self.conversors[self.id])
        else:
            data = u.Database(*self.origen).get_all(sql)
        upload = list(data)
        u.Database(*s.EAPP_DB).list_to_table(upload, self.nom)
