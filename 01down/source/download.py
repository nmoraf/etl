# coding: latin1

"""
.
"""

import bisect as b
import collections as c
import simplejson as j
import multiprocessing as m
import MySQLdb as my
import time
import threading as t

import catalegs
import eapp
import shared as s
import sisaptools as u
import u11


class Download(object):
    """."""

    def __init__(self):
        """."""
        self.get_downloads()
        if self.create_threads:
            self.launch_create()
        self.launch_upload()
        if self.jobs["sidics"]:
            self.launch_sidics()
        if self.jobs["redics"] or self.create_threads:
            self.launch_redics()
        self._wait_and_kill("upload")
        self._create_worker({"domini": "eapp"})

    def get_downloads(self):
        """."""
        self.create_threads = {}
        self.jobs = {k: [] for k in s.ORIGEN_DB}
        for download in j.load(open(s.KING_FILE)):
            domini = download["domini"]
            crear = download["crear"]
            baixar = download["baixar"]
            if crear:
                thread = t.Thread(target=self._create_worker, args=(download,))
                self.create_threads[domini] = thread
            if baixar:
                origen = download["origen"]
                columns = download["create_desti"]
                jobs = download["jobs"]
                merge = download["merge"]
                particions = []
                with u.Database(*s.DESTI_DB) as desti:
                    for taula, recompte, sql in jobs:
                        desti.create_table(taula, columns, remove=True)
                        desti.execute(self._control("pre", taula, recompte))
                        particions.append(taula)
                    if merge:
                        create = "create or replace table {} ({}) \
                                  ENGINE=MERGE UNION=({})".format(
                                                download["domini"],
                                                ", ".join(columns),
                                                ", ".join(particions))
                        desti.execute(create)
                if not crear:
                    for taula, recompte, sql in jobs:
                        this = (recompte, domini, sql, taula)
                        b.insort(self.jobs[origen], this)

    def launch_create(self):
        """."""
        if "catalegs" in self.create_threads:
            thread = self.create_threads.pop("catalegs")
            thread.start()
        if "u11" in self.create_threads:
            thread = self.create_threads.pop("u11")
            thread.start()
            thread.join()
        for thread in self.create_threads.values():
            thread.start()

    def launch_upload(self):
        """."""
        self.queue = {}
        self.procs = c.defaultdict(list)
        self.queue["upload"] = m.Manager().JoinableQueue()
        for i in range(s.QUEUE_PROCS["upload"]):
            proc = m.Process(target=self._upload)
            proc.start()
            self.procs["upload"].append(proc)

    def launch_sidics(self):
        """."""
        self._launch_queue("sidics")
        while self.jobs["sidics"]:
            self.queue["sidics"].put(self.jobs["sidics"].pop())
        self._wait_and_kill("sidics")

    def launch_redics(self):
        """."""
        while sum([t.isAlive() for t in self.create_threads.values()]) > 3:
            time.sleep(30)
        self._launch_queue("redics")
        while (len(self.jobs["redics"]) > 0 or
               any([t.isAlive() for t in self.create_threads.values()])):
            if len(self.jobs["redics"]) > 0 and not self.queue["redics"].full():  # noqa
                self.queue["redics"].put(self.jobs["redics"].pop())
        time.sleep(30)
        sql = "select count(1) from ctl_down \
               where origen = 'redics' and ts_start > 0 and ts_finish = 0"
        doing = u.Database(*s.DESTI_DB).get_one(sql)[0] > 0
        if doing:
            self.queue["dummy"] = m.JoinableQueue()
            for i in range(s.QUEUE_PROCS["redics"]):
                proc = m.Process(target=self._dummy)
                proc.start()
                self.procs["redics"].append(proc)
            for sector in s.SECTORS_DB:
                for i in range(2):
                    self.queue["dummy"].put(sector)
        self._wait_and_kill("redics")

    def _create_worker(self, download):
        """."""
        domini = download["domini"]
        taula = "sisap_{}".format(domini)
        sql = "update ctl_create \
               set ts_start = current_timestamp, ts_finish = 0, error = '' \
               where conc = '{0}'".format(domini)
        u.Database(*s.DESTI_DB).execute(sql)
        try:
            if domini == "u11":
                u11.U11()
            elif domini == "catalegs":
                catalegs.Main()
            elif domini == "eapp":
                eapp.Main()
            else:
                with u.Database(*s.ORIGEN_DB["redics"]) as redics:
                    redics.drop_table(taula)
                    redics.execute(download["create_origen"])
                    redics.set_statistics(taula)
        except Exception as e:
            error = str(my.escape_string(str(e)))
        else:
            error = ""
        sql = "update ctl_create \
               set ts_finish = current_timestamp, error = '{}' \
               where conc = '{}'".format(error, domini)
        u.Database(*s.DESTI_DB).execute(sql)
        if download.get("baixar"):
            with u.Database(*s.ORIGEN_DB["redics"]) as redics:
                if len(download["jobs"]) == 1:
                    recomptes = {domini: redics.get_table_count(taula)}
                else:
                    recomptes = {"{}_{}".format(domini, particio): n
                                 for particio, n
                                 in redics.get_table_partitions(taula).items()}
            for taula, recompte_old, sql in download["jobs"]:
                recompte_new = recomptes[taula]
                this = (recompte_new, domini, sql, taula)
                b.insort(self.jobs["redics"], this)

    def _launch_queue(self, key):
        """."""
        self.queue[key] = m.JoinableQueue(s.QUEUE_LENGTH[key])
        for i in range(s.QUEUE_PROCS[key]):
            proc = m.Process(target=self._worker, args=(key,))
            proc.start()
            self.procs[key].append(proc)

    def _wait_and_kill(self, key):
        """."""
        self.queue[key].join()
        for proc in self.procs[key]:
            proc.terminate()

    def _control(self, step, taula, n=None):
        """."""
        if step == "pre":
            sql = "update ctl_down \
                   set ts_start = 0, ts_finish = 0, \
                       ora = {}, my = 0, diff = 0, prog = 0 \
                   where tab = '{}'".format(n, taula)
        if step == "ini":
            sql = "update ctl_down \
                   set ts_start = current_timestamp, ora = {} \
                   where tab = '{}'".format(n, taula)
        elif step == "prog":
            sql = "update ctl_down set prog = prog + {} \
                   where tab = '{}'".format(n, taula)
        elif step == "fi":
            sql = "update ctl_down \
                   set ts_finish = current_timestamp, my = {0}, \
                       diff = if(ora > 0, (ora - my) / ora, 0), \
                       prog = {0} \
                   where tab = '{1}'".format(n, taula)
        return sql

    def _worker(self, queue):
        """."""
        while True:
            n_rows = s.ROWS[queue]
            n_control = s.CONTROL_EVERY / n_rows
            recompte, domini, sql, taula = self.queue[queue].get()
            u.Database(*s.DESTI_DB, retry=1).execute(self._control("ini", taula, recompte))  # noqa
            i = -1
            with u.Database(*s.ORIGEN_DB[queue], retry=3) as origen:
                if queue == "sidics":
                    origen.execute("set session net_write_timeout = 3600")
                for i, rows in enumerate(origen.get_many(sql, n_rows)):
                    filename = '{}/{}_{}.txt'.format(s.TMP_FOLDER, taula, i)
                    u.TextFile(filename).write_iterable(rows, s.DELIMITER, s.NEWLINE)  # noqa
                    self.queue["upload"].put((filename, taula, (i + 1) % n_control == 0, len(rows) < n_rows))  # noqa
            if i == -1:
                u.Database(*s.DESTI_DB, retry=1).execute(self._control("fi", taula, 0))  # noqa
            self.queue[queue].task_done()

    def _upload(self):
        """."""
        with u.Database(*s.DESTI_DB, retry=1) as desti:
            if s.HOST == "P2262":
                desti.local_infile = False
            desti.execute("set session wait_timeout = 28800")
            while True:
                filename, taula, control, last = self.queue["upload"].get()
                desti.file_to_table(filename, taula, None, s.DELIMITER, s.NEWLINE)  # noqa
                if last:
                    time.sleep(2)
                    n = desti.get_table_count(taula)
                    desti.execute(self._control("fi", taula, n))
                elif control:
                    desti.execute(self._control("prog", taula, s.CONTROL_EVERY))  # noqa
                self.queue["upload"].task_done()

    def _dummy(self):
        """."""
        sql = "select id_cip_sec \
               from sisap_assignada partition(s{0}) a, sisap_u11 b \
               where a.usua_cip = b.hash_a and a.codi_sector = b.sector"
        sector = self.queue["dummy"].get()
        while True:
            with u.Database(*s.ORIGEN_DB["redics"], retry=3) as origen:
                for row in origen.get_all(sql.format(sector)):
                    continue


if __name__ == "__main__":
    Download()
