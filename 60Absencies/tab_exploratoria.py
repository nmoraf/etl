# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb absentisme a nivell visita.
Es tracta d'una actualització i rentat de cara del procés del procés de 96informes\124Absentisme.py i derivats.
Les dades es refereixen a 2017, i s'obtenen de la BD de tancament de P2262.
La ID de SISAP es transforma a HASH de l'ICS per si cal obtenir noves
dades en un futur.
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

inclusio = "where festa_local=0 and festa_agenda=0 and dia_feiner=1 and forcada not in ('E') and bloc_horari='S' and forcada='N' and data_visita not in ('2017-10-03','2017-11-08','2017-12-14','2017-12-15','2017-12-18','2017-12-19','2017-12-20') and tipus_visita='9C'"

class Absentisme(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_moduls()
        self.get_hash()
        self.get_festius()
        self.get_noAgenda()
        self.get_visites()
        self.export_dades_visita()
        self.marqueminclosos()

    def get_centres(self):
        """EAP ICS i no ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_moduls(self):
        """Moduls de MG lligats a UBA"""
        sql = ("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul, modu_codi_uab \
                from cat_vistb027 \
                where modu_codi_uab <> '' and modu_servei_codi_servei='MG'", "import")
        self.moduls = {(sec, cen, cla, ser, cod): (uba) for (sec, cen, cla, ser, cod, uba)
                        in u.getAll(*sql)}
    
    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        self.hash = {(id):(hashs) for id, hashs 
                    in u.getAll(*sql)}

    def get_festius(self):
        """
        Agafem els festius de les localitats i les lliguem amb el seu centre
        Problema: al catàleg només guarda any en curs. Agafem les descripcions que són FESTA LOCAL i assumim que són iguals cada any
        No tots els sectors tenen les festes locals posades...
        no sé si val la pena usar això
        """

        sql = "select a.edce_codi_centre, to_char(to_date(b.cale_dia_festiu,'J'),'MM-DD') \
               from pritb120 a, vistb007 b \
               where a.edce_codi_local = b.cale_loc_codi_loc and \
                     b.cale_data_baixa = 1 and cale_descripcio='FESTA LOCAL'"
        self.festius_centre = defaultdict(list)
        for sector in u.sectors:
            for centre, festiu in u.getAll(sql, sector):
                festiu17 = '2017-' + festiu
                self.festius_centre[centre].append(festiu17)
        
    def get_noAgenda(self):
        """Agafem dies agenda tancades per explorar"""
        self.noAgenda = {}
        sql = ("select codi_sector,fest_centre_codi_centre,fest_centre_classe_centre,fest_servei_codi_servei,fest_modul_codi_modul,fest_dia_festiu,fest_data_baixa \
            from acc_festius", "import")
        for sector,centre,classe,servei,modul,diafesta,databaixa in u.getAll(*sql):
            try:
                festa = datetime.date(*(u.jd2gcal(diafesta)))
            except ValueError:
                continue
            if databaixa <> 1:
                dbaixa = datetime.date(*(u.jd2gcal(databaixa)))
            self.noAgenda[(sector,centre,classe,servei,modul,festa)] = dbaixa

    
    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        Si l'agenda és per capes, separem les visites segons el seu tipus (9C o 9R).
        En el cas de les agendes seqüencials,agafem visites del centre
        segons el lloc de realització.
        """
        self.dades = []
        visitesMG = 0
        lligadesUBA = 0
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), "import")
            if dat == 2017:
                sql ="select  codi_sector, id_cip_sec, visi_up, visi_centre_codi_centre, visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul,\
                        visi_tipus_visita, visi_lloc_visita, visi_situacio_visita, \
                        visi_data_visita,visi_dia_peticio, extract(year_month from visi_data_visita),\
                        visi_forcada_s_n, if(visi_bloc_codi_bloc ='', 'N', 'S'), visi_tipus_citacio \
                        from {} \
                        where visi_servei_codi_servei = 'MG' and visi_data_baixa = 0 ".format(table)
                for sector, id, up, centre, classe, servei, modul, tipus, lloc, sit, datvisi, datpeti, periode, forcada, forcadaprof, cita in u.getAll(sql, "import"):
                    if up in self.centres:
                        visitesMG +=1
                        if (sector, centre, classe, servei, modul) in self.moduls:
                            uba = self.moduls[(sector, centre, classe, servei, modul)]
                            lligadesUBA += 1
                            tipvisita,visitafeta = 0,0
                            if tipus in ('9C', '9R'):
                                tipvisita = tipus
                            else:
                                if tipus in ('9D','9T','9E', '9R','9C'):
                                    tipvisita = 'capes'
                                else:
                                    if lloc == 'C':
                                        tipvisita = 'S'
                            festa_local, festa_agenda = 0, 0
                            festius = set(self.festius_centre[centre])
                            if str(datvisi) in festius:
                                festa_local = 1
                            if (sector,centre,classe,servei,modul,datvisi) in self.noAgenda:
                                dbaixa = self.noAgenda[(sector,centre,classe,servei,modul,datvisi)]
                                if dbaixa == 1 or dbaixa > datvisi:
                                    festa_agenda = 1
                            try:
                                diesEntre = u.daysBetween(datpeti,datvisi)
                            except:
                                diesEntre = -1
                            diesWorkEntre = 0
                            if u.isWorkingDay(datvisi):
                                workday = 1
                            else:
                                workday = 0
                            try:
                                for single_date in u.dateRange(datpeti,datvisi):
                                    if u.isWorkingDay(single_date) and single_date not in festius:
                                        diesWorkEntre += 1       
                            except:
                                diesWorkEntre = -1
                            if tipvisita in ('9C','9R','S'):
                                self.dades.append([id, sector, self.hash[(id)] if (id) in self.hash else '', centre,classe,servei,modul,up, uba, datvisi, datpeti, datvisi.weekday(), periode, festa_local,
                                                    festa_agenda, workday, diesEntre, diesWorkEntre, forcada, forcadaprof, 1 if sit == 'R' else 0, cita, tipvisita, 0])
        print "Visites MG toal any: ", visitesMG
        print "Visites MG lligades a UBA. ", lligadesUBA
        
    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules de My a test."""
        table = 'SISAP_ABSENTISME_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db , rm=True)
        u.listToTable(dades, table,  db)

    def export_dades_visita(self):
        """Exporta les dades de nivell visita."""
        columns = ('id_cip_sec int', 'sector varchar(4)', 'hash varchar(40)', 'centre varchar(20)', 'classe varchar(3)','servei varchar(10)','modul varchar(10)','up varchar(5)', 'uba varchar(5)',
                   'data_visita date', 'data_peticio date', 'dia int', 'periode varchar(6)',
                   'festa_local int', 'festa_agenda int', 'dia_feiner int', 'diesEntre int', 'dieslaboralsEntre int',
                   'forcada varchar(2)', 'bloc_horari varchar(1)', 'visita_feta int', 'citacio varchar(10)', 'tipus_visita varchar(10)', 'inclos int')
        Absentisme.export_dades("VISITA", columns, self.dades)                                                

    def marqueminclosos(self):
        """Marquem inclosos segons exploracio taula"""
        db = "test"
        table = 'SISAP_ABSENTISME_VISITA'
        sql = 'update {} set inclos = 1 {}'.format(table, inclusio)
        u.execute(sql, db)
        
if __name__ == '__main__':
    Absentisme()
    