# coding: utf8


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *



"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb absentisme a nivell uba/agenda.

"""


imp = 'import'
nod = 'nodrizas'


accessibilitat = Counter()
sql = "select * from sisap_accessibilitat where  peticions is not null and to_char(dia, 'YYYYMMDD') <'20180101'"
for row in getAll(sql, 'redics'):  
    up, uba, servei, peticions =  row[1], row[2], row[3], row[5]
    forats48 = row[6] + row[7] + row[8]
    forats5d = row[6] + row[7] + row[8] + row[9] + row[10] + row[11]
    forats10d = row[6] + row[7] + row[8] + row[9] + row[10] + row[11] + row[12] + row[13] + row[14] + row[15] + row[16]
    dia = row[4]
    if forats48 > peticions:
        forats48 = peticions
    if forats5d > peticions:
        forats5d = peticions
    if forats10d > peticions:
        forats10d = peticions
    if servei == 'MG':
        accessibilitat[(up, uba, dia, 'peti')] += peticions
        accessibilitat[(up, uba, dia, 'forats48')] += forats48
        accessibilitat[(up, uba, dia, 'forats5d')] += forats5d
        accessibilitat[(up, uba, dia, 'forats10d')] += forats10d
    
upload = []
for (up, uba, dia, tipus),r in accessibilitat.items():
    if tipus == 'peti':
        forats48 = accessibilitat[(up, uba, dia, 'forats48')]
        forats5d = accessibilitat[(up, uba, dia, 'forats5d')]
        forats10d = accessibilitat[(up, uba, dia, 'forats10d')]
        upload.append([up, uba, dia, r, forats48, forats5d, forats10d])

        
table = 'SISAP_ABSENTISME_UBA'
columns = ('up varchar(5)', 'uba varchar(5)', 'data date', 'peticions int', 'forats48 int', 'forats5d int','forats10d int')
columns_str = "({})".format(', '.join(columns))
db = "test"
createTable(table, columns_str, db , rm=True)
listToTable(upload, table,  db)
