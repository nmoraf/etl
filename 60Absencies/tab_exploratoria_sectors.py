# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb absentisme a nivell visita.
Es tracta d'una actualització i rentat de cara del procés del procés de 96informes\124Absentisme.py i derivats.
Les dades es refereixen a 2017, i s'obtenen de la BD de tancament de P2262.
La ID de SISAP es transforma a HASH de l'ICS per si cal obtenir noves
dades en un futur.
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u


class Absentisme(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_moduls()
        self.get_u11()
        self.get_anonymizer()
        self.get_id()
        self.get_festius()
        self.get_noAgenda()
        self.get_visites()
        self.export_dades_visita()

    def get_centres(self):
        """EAP ICS i no ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_moduls(self):
        """Moduls de MG lligats a UBA"""
        sql = ("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul \
                from cat_vistb027 \
                where modu_codi_uab <> '' and modu_servei_codi_servei='MG'", "import")
        self.moduls = {(sec, cen, cla, ser, cod): (True) for (sec, cen, cla, ser, cod)
                        in u.getAll(*sql)}
                        
    def get_u11(self):
        """cips anteriors per visita"""
        self.cips_anteriors = {}
        for sector in u.sectors:
            sql = 'select cip_cip_anterior,cip_usuari_cip,cip_sector_codi_sector from usutb011'
            for cipa, cip, sector in u.getAll(sql, sector):
                self.cips_anteriors[(sector, cipa)] = cip
    
    def get_anonymizer(self):
        self.anonymizer = {}
        sql = 'select usua_cip,usua_cip_cod from pdptb101'
        for cip, hashs in u.getAll(sql, 'pdp'):
            self.anonymizer[cip] = hashs
        
    
    def get_id(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, codi_sector, hash_d from u11", "import")
        self.hash = {(hashs, sector):(id) for id, sector, hashs
                    in u.getAll(*sql)}

    def get_festius(self):
        """
        Agafem els festius de les localitats i les lliguem amb el seu centre
        Problema: al catàleg només guarda any en curs. Agafem les descripcions que són FESTA LOCAL i assumim que són iguals cada any
        No tots els sectors tenen les festes locals posades...
        no sé si val la pena usar això
        """

        sql = "select a.edce_codi_centre, to_char(to_date(b.cale_dia_festiu,'J'),'MM-DD') \
               from pritb120 a, vistb007 b \
               where a.edce_codi_local = b.cale_loc_codi_loc and \
                     b.cale_data_baixa = 1 and cale_descripcio='FESTA LOCAL'"
        self.festius_centre = defaultdict(list)
        for sector in u.sectors:
            for centre, festiu in u.getAll(sql, sector):
                festiu17 = '2017-' + festiu
                self.festius_centre[centre].append(festiu17)
        
    def get_noAgenda(self):
        """Agafem dies agenda tancades per explorar"""
        self.noAgenda = {}
        sql = ("select codi_sector,fest_centre_codi_centre,fest_centre_classe_centre,fest_servei_codi_servei,fest_modul_codi_modul,fest_dia_festiu,fest_data_baixa \
            from acc_festius", "import")
        for sector,centre,classe,servei,modul,diafesta,databaixa in u.getAll(*sql):
            try:
                festa = datetime.date(*(u.jd2gcal(diafesta)))
            except ValueError:
                continue
            if databaixa <> 1:
                dbaixa = datetime.date(*(u.jd2gcal(databaixa)))
            self.noAgenda[(sector,centre,classe,servei,modul,festa)] = dbaixa

    
    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        Si l'agenda és per capes, separem les visites segons el seu tipus (9C o 9R).
        En el cas de les agendes seqüencials,agafem visites del centre
        segons el lloc de realització.
        """
        self.dades = []
        visitesMG = 0
        lligadesUBA = 0
        for sector in u.sectors:
            sql ="select  visi_usuari_cip, visi_up, visi_centre_codi_centre, visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul,\
                        visi_tipus_visita, visi_lloc_visita, visi_situacio_visita, \
                        to_char(to_date(visi_data_visita,'J'),'YYYY-MM-DD'),to_char(to_date(visi_dia_peticio,'J'),'YYYY-MM-DD'),to_char(to_date(visi_data_visita,'J'),'YYYYMM'),\
                        to_date(visi_data_visita,'J'),to_date(visi_dia_peticio,'J'),\
                        visi_forcada_s_n,  case when visi_bloc_codi_bloc is null then 'N' else 'S' end , visi_tipus_citacio \
                        from vistb043 \
                        where visi_servei_codi_servei = 'MG' and visi_data_baixa = 1 and to_char(to_date(visi_data_visita,'J'),'YYYY')='2017'"
            for cip, up, centre, classe, servei, modul, tipus, lloc, sit, datvisi, datpeti, periode, datvisid, datpetid, forcada, forcadaprof, cita in u.getAll(sql, sector):
                if up in self.centres:
                    visitesMG +=1
                    if (sector, centre, classe, servei, modul) in self.moduls:
                        lligadesUBA += 1
                        try:
                            cipant = self.cips_anteriors[(sector, cip)]
                            cod = self.anonymizer[cipant[:13]]
                        except KeyError:
                            cod = ''
                        tipvisita,visitafeta = 0,0
                        if tipus in ('9C', '9R'):
                            tipvisita = tipus
                        else:
                            if tipus in ('9D','9T','9E', '9R','9C'):
                                tipvisita = 'capes'
                            else:
                                if lloc == 'C':
                                    tipvisita = 'S'
                        festa_local, festa_agenda = 0, 0
                        festius = set(self.festius_centre[centre])
                        if datvisi in festius:
                            festa_local = 1
                        if (sector,centre,classe,servei,modul,datvisid) in self.noAgenda:
                            dbaixa = self.noAgenda[(sector,centre,classe,servei,modul,datvisid)]
                            if dbaixa == 1 or dbaixa > datvisid:
                                festa_agenda = 1
                        try:
                            diesEntre = u.daysBetween(datpetid,datvisid)
                        except:
                            diesEntre = -1
                        diesWorkEntre = 0
                        if u.isWorkingDay(datvisid):
                            workday = 1
                        else:
                            workday = 0
                        try:
                            for single_date in u.dateRange(datpetid,datvisid):
                               if u.isWorkingDay(single_date) and single_date not in festius:
                                   diesWorkEntre += 1       
                        except:
                            diesWorkEntre = -1
                        if tipvisita in ('9C','9R','S'):
                            self.dades.append([self.hash[(cod,sector)] if (cod,sector) in self.hash else '', sector, cod, centre,classe,servei,modul,up, datvisi, datpeti, datvisid.weekday(), periode, festa_local,
                                                festa_agenda, workday, diesEntre, diesWorkEntre, forcada, forcadaprof, 1 if sit == 'R' else 0, cita, tipvisita])
        print "Visites MG toal any: ", visitesMG
        print "Visites MG lligades a UBA. ", lligadesUBA
        
    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules de My a test."""
        table = 'SISAP_ABSENTISME_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db , rm=True)
        u.listToTable(dades, table,  db)

    def export_dades_visita(self):
        """Exporta les dades de nivell visita."""
        columns = ('id_cip_sec int', 'sector varchar(4)', 'hash varchar(40)', 'centre varchar(20)', 'classe varchar(3)','servei varchar(10)','modul varchar(10)','up varchar(5)', 
                   'data_visita date', 'data_peticio date', 'dia int', 'periode varchar(6)',
                   'festa_local int', 'festa_agenda int', 'dia_feiner int', 'diesEntre int', 'dieslaboralsEntre int',
                   'forcada varchar(2)', 'bloc_horari varchar(1)', 'visita_feta int', 'citacio varchar(10), tipus_visita varchar(10)')
        Absentisme.export_dades("VISITA_SECTOR", columns, self.dades)                                                

if __name__ == '__main__':
    Absentisme()
    