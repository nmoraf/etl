from sisapUtils import *
import sys
from time import strftime

db= "ass"

error= []
nom= {'eqa_ind':'','pedia':'PED','social':'SOC','odn':'ODN','ass':'ASS'}
centres="nodrizas.ass_centres"

taula="%s.exp_khalix_cataleg" % db
query="select * from %s" % taula
file="EQA%s_CAT" % nom[db]
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_up_ind" % db
query="select indicador,concat('A','periodo'),br_assir,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=up_assir group by indicador,concat('A','periodo'),br_assir,conc,edat,comb,sexe,'N',n " % {'taula': taula,'centres':centres}
#query="select indicador,'periodo',br_assir,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=up_assir group by indicador,concat('A','periodo'),br_assir,conc,edat,comb,sexe,'N',n " % {'taula': taula,'centres':centres}
file="EQA%s_NOU" % nom[db]
#file="EQA%s_NOU_1IND" % nom[db]
error.append(exportKhalix(query,file))

tb = 'mst_ubas'
query = "select \
    upper(left(concat('S', sha1(concat(a.up, if(a.tipus = 'E', 'L', a.tipus), a.uba))), 10)),  \
    if(b.ide_dni is null, upper(left(concat('S', sha1(concat(a.up, if(a.tipus = 'E', 'L', a.tipus), a.uba))), 10)), concat(a.up, ' - ', descripcio)),  \
    if(a.tipus = 'E', 'L', a.tipus),  \
    c.up_assir,  \
    c.br_assir  \
  from \
    {}.{} a \
    left join (select ide_dni, max(concat_ws(' ', nom, cognom1, cognom2)) descripcio from import.cat_professionals_assir group by ide_dni) b on a.uba = b.ide_dni \
    inner join {} c on a.up = c.up".format(db, tb, centres)
file = 'EQAASS_UBA_CATALEG'
error.append(exportKhalix(query, file))

file = 'Crea_Simbolo_ASSIR.txt'
fileTime = file.replace('.', strftime('_%Y%m%d_%H%M%S.'))
command = 'Crea_Simbolo.bat && copy {} .\\Crea_Simbolo_log\\{}'.format(file, fileTime)
existents = set([s for s, in getAll('select sym_name from icsap.klx_master_symbol where dim_index = 2', 'khalix')])
nous = []
for entity, descripcio, tipus, up, br in getAll(query, 'import'):
    if entity not in existents:
        nous.append((entity, descripcio, tipus, 'P{}'.format(br)))
if len(nous) > 0:
    text = ''
    with open(tempFolder + file, 'wb') as f:
        f.write('MAINTENANCE ON\r\n')
        for entity, descripcio, tipus, father in nous:
            f.write('CREATE SYMBOL ENTITIES {} "{}" STANDARD MANUAL NEITHER PARENT {}{} "+"\r\n'.format(entity, descripcio,father, tipus))  
            f.write('SET SYMBOL ENTITIES {} DESCRIPTION FR TO "{}"\r\n'.format(entity, descripcio))
            text += '{}\r\n'.format(entity)
        f.write('MAINTENANCE OFF\r\n')
    sshPutFile(file, 'cargas', 'khalix')
    # status, stdout, stderr = sshExecuteCommand(command, 'procesos', 'khalix')
    # sendSISAP('amercadecosta@gencat.cat','Nuevas entities creadas','Albert',text)

tb = 'exp_khalix_uba_ind'
query = "select indicador, concat('A', 'periodo'), upper(left(concat('S', sha1(concat(up, if(tipus = 'E', 'L', tipus), uba))), 10)), analisis, 'NOCAT', detalle, 'DIM6SET', 'N', valor from {}.{} a where exists (select 1 from {} b where a.up = b.up)".format(db, tb, centres)
file = 'EQAASS_UBA'
error.append(exportKhalix(query, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
