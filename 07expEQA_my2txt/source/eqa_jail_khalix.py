# coding: iso-8859-1
from sisapUtils import *
import sys

db = "eqa_ind"
taula="%s.exp_khalix_up_ind" % db
centres = "nodrizas.jail_centres"

error = []

ind_out = ('EQA0401A','EQA0402A','EQA0403A','EQA0404A','EQA0406A','EQA0407A','EQA0601A')
query="select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi where indicador not in %(ind_out)s" % {'taula': taula,'centres':centres, 'ind_out': ind_out}
file="EQA_JAIL" 
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_up_pob" % db
query="select 'EQAPOBL',concat('A','periodo'),ics_codi,'NOCLI',edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi"  % {'taula': taula,'centres':centres}
file="EQAPOBCP_NOU" 
error.append(exportKhalix(query,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
