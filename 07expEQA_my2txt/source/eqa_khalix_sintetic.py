from sisapUtils import *
import sys

try:
    db= sys.argv[1]
except:
    sys.exit("no m'has passat arguments")

error= []
nom= "ADULTS" if db == "eqa_ind" else "APEDIA"
ped= "PED" if db == "pedia" else ""
letter= "A" if db == "eqa_ind" else "P"
doCarta= True if db[:3]== 'eqa' else False
centres="export.khx_centres"

taula="%s.exp_khalix_uba_sintetic" % db
query="select 'EQUB%(nom)s',concat('A','periodo'),concat(ics_codi,tipus,uba),analisis,'NOCAT','NOINSAT','DIM6SET','N',round(dada,4) from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'nom': nom,'taula': taula,'centres':centres}
file="EQA%s_IS_UBA_NOU" % ped
error.append(exportKhalix(query,file))

if doCarta:
    taula="%s.exp_khalix_uba_carta" % db
    query="select 'EQ%(letter)sUBAIND',concat('A','periodo'),concat(ics_codi,tipus,uba),analisis,'NOCAT','NOINSAT','DIM6SET','N',round(dada,4) from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'letter': letter,'taula': taula,'centres':centres}
    file="EQA%s_IS_UBA_CARTA_NOU" % ped
    error.append(exportKhalix(query,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")