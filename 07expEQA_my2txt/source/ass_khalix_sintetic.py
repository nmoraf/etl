from sisapUtils import exportKhalix


error= []

db = 'ass'
tb = 'exp_khalix_uba_sintetic'
query = "select 'EQASSIR', concat('A', 'periodo'), upper(left(concat('S', sha1(concat(up, tipus, uba))), 10)), analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', 'N', round(dada, 4) from {}.{} a where exists (select 1 from nodrizas.ass_centres b where a.up = b.up)".format(db, tb)
file = "EQAASS_IS_UBA"
error.append(exportKhalix(query, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")