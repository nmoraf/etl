# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

printTime('Inici')

db = 'ass'

path = ecapFolder

nod = 'nodrizas'
dext = 'dextraccio'
centres = 'ass_centres'
OutFile = tempFolder + 'acc_assir.txt'
tableMy = 'exp_khalix_acc_assir'

OutFileUP = tempFolder + 'acc_ass_ups.txt'

fitxer = 'DEMORA_ASSIR_TRANS_'.lower()
fitxer2 = 'DEMORA_ASSIR_LONG_'.lower()

sql = "select date_format(data_ext,'%m'),date_format(data_ext,'%y') from {}".format(dext)
for mes, anys in getAll(sql, nod):
    mes = mes
    anys = anys

periode = mes + anys + '.txt'

assirs = {}
sql = 'select up, br_assir from {}'.format(centres)
for up, up_assir in getAll(sql, nod):
    assirs[up] = up_assir


Noassirs = {}
accAss = {}
for file in os.listdir(path):
    if (file.startswith(fitxer) or file.startswith(fitxer2)) and file.endswith(periode):
        pathFile = path + file
        with open(pathFile, 'rb') as fitxerKhx:
            p = csv.reader(fitxerKhx, delimiter=';')
            for ind in p:
                up = ind[1]
                servei = ind[6]
                tvisita = ind[10]
                if file.startswith(fitxer):
                    num = ind[11]
                    num10 = ind[12]
                    den = 0
                    tipus = 'TRANS'
                elif file.startswith(fitxer2):
                    num = ind[12]
                    num10 = 0
                    den = ind[13]
                    tipus = 'LONG'
                if num == '-':
                    num = 0
                if num10 == '-':
                    num10 = 0
                if den == '-':
                    den = 0
                num10 = int(num10)
                num = int(num)
                den = int(den)
                try:
                    br = assirs[up]
                except KeyError:
                    Noassirs[up] = True
                    continue
                if (tipus, br, servei, tvisita) in accAss:
                    accAss[(tipus, br, servei, tvisita)]['num'] += num
                    accAss[(tipus, br, servei, tvisita)]['num10'] += num10
                    if file.startswith(fitxer):
                        accAss[(tipus, br, servei, tvisita)]['den'] += 1
                    elif file.startswith(fitxer2):
                        accAss[(tipus, br, servei, tvisita)]['den'] += den
                else:
                    if file.startswith(fitxer):
                        accAss[(tipus, br, servei, tvisita)] = {'num': num, 'num10': num10, 'den': 1}
                    elif file.startswith(fitxer2):
                        accAss[(tipus, br, servei, tvisita)] = {'num': num, 'num10': num10, 'den': den}


with openCSV(OutFile) as c:
    for (tipus, br, servei, tvisita), d in accAss.items():
        c.writerow([tipus, br, servei, tvisita, d['num'], d['num10'], d['den']])

execute("drop table if exists {}".format(tableMy), db)
execute("create table {} (tipus varchar(10) not null default'', br varchar(5) not null default'', servei varchar(10) not null default'', tvisita varchar(10) not null default'', num int, num10 int, den int)".format(tableMy), db)
loadData(OutFile, tableMy, db)

error = []

sql = "select 'DEMDIES',concat('A','periodo'),br,'NUM',concat('DEM',servei),concat('VIS',tvisita),'DIM6SET', 'N', num from {0}.{1} where tipus='TRANS' \
        union select 'DEMDIES',concat('A','periodo'),br,'DEN',concat('DEM',servei),concat('VIS',tvisita),'DIM6SET', 'N', den from {0}.{1} where tipus='TRANS' \
        union select 'DEMCAPA',concat('A','periodo'),br,'NUM',concat('DEM',servei),concat('VIS',tvisita),'DIM6SET', 'N', num10 from {0}.{1} where tipus='TRANS' \
        union select 'DEMCAPA',concat('A','periodo'),br,'DEN',concat('DEM',servei),concat('VIS',tvisita),'DIM6SET', 'N', den from {0}.{1} where tipus='TRANS' \
        union select 'DEMVISI',concat('A','periodo'),br,'NUM',concat('DEM',servei),concat('VIS',tvisita),'DIM6SET', 'N', num from {0}.{1} where tipus='LONG' \
        union select 'DEMVISI',concat('A','periodo'),br,'DEN',concat('DEM',servei),concat('VIS',tvisita),'DIM6SET', 'N', den from {0}.{1} where tipus='LONG'".format(db, tableMy)
file = "ACC_ASSIR"
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

with openCSV(OutFileUP) as c:
    for (up), d in Noassirs.items():
        c.writerow([up])
'''
text = "Us fem arribar un fitxer amb les UP no lligades a cap assir"
sendSISAP(['edelatorre@gencat.cat', 'mmedinap@gencat.cat'], 'ACC assir: UPs no lligades', 'Manolo i Enrique', text, OutFileUP)
'''
try:
    remove(OutFileUP)
except:
    pass

printTime('Fi')
