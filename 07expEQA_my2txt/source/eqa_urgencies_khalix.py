# coding: iso-8859-1
from sisapUtils import *

db = "eqa_ind"
nod = "nodrizas"
tableK = "exp_khalix_eqa_urgencies_up"
centres = "urg_centres"

error = []

sql = "select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',valor from {0}.{1} a inner join {2}.{3} on up=scs_codi".format(db, tableK, nod, centres)
file = "EQA_URGENCIES"
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
