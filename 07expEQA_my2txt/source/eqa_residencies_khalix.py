# coding: iso-8859-1
import sisapUtils as u
import sys

file = "EQA_RESIDENCIES"

tableK = ".".join(("eqa_ind", "exp_khalix_EQAresis_up a"))
cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

cat_cond = "WHERE a.up_resi = b.resi_cod and b.sisap_class = 1"
cat_cond = " ".join((cataleg, cat_cond))
cat_cond = "exists (select 1 from {cat_cond})".format(cat_cond=cat_cond)

grup_cond = "indicador, up_resi, edat, sexe"

dims_num = "indicador, 'Aperiodo', concat('R', replace(up_resi,'-','_')), 'NUM', edat"
dims_den = "indicador, 'Aperiodo', concat('R', replace(up_resi,'-','_')), 'DEN', edat"
dims_ates = "'INSAT', sexe, 'N'"
dims_ass = "'INSASS', sexe, 'N'"

sql = """
    select {dims_num}, {dims_ates}, sum(num)
    from {tableK} where {cat_cond} and ates = 1
    group by {grup_cond}
        UNION
    select {dims_den}, {dims_ates}, sum(den)
    from {tableK} where {cat_cond} and ates = 1
    group by {grup_cond}
        UNION
    select {dims_num}, {dims_ass}, sum(num)
    from {tableK} where {cat_cond}
    group by {grup_cond}
        UNION
    select {dims_den}, {dims_ass}, sum(den)
    from {tableK} where {cat_cond}
    group by {grup_cond}
    """.format(tableK=tableK,
               cat_cond=cat_cond,
               grup_cond=grup_cond,
               dims_num=dims_num,
               dims_den=dims_den,
               dims_ates=dims_ates,
               dims_ass=dims_ass)

error = []
error.append(u.exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
