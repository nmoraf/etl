from sisapUtils import printTime, tempFolder, readCSV, createTable, listToTable
from sisapUtils import execute, getAll, sectors, multiprocess, writeCSV
from collections import defaultdict


def get_file_data(file):
    file_data = {}
    file_header = []
    first = True
    for row in readCSV(tempFolder + file, sep=separator):
        if header and first:
            file_header = row
            first = False
        else:
            file_data[row[int(position)]] = row
    return file_data, file_header


def get_all_hashs():
    # parameters
    db = 'redics'
    tb1 = 'sisap_temp_1'
    tb2 = 'sisap_temp_2'
    tb3 = 'sisap_temp_3'
    # upload hash_m
    createTable(tb1, '(hash_m varchar2(40), sector varchar2(4), primary key (hash_m, sector))', db, rm=True)
    listToTable([patient.split(':') for patient in file_data], tb1, db)
    # upload hash_d
    createTable(tb2, '(hash_m varchar2(40), sector varchar2(4), hash_d varchar2(40), primary key (hash_d, sector))', db, rm=True)
    sql = 'insert into {} select b.hash_m, b.sector, a.hash_d from sisap_u11 a, {} b where a.hash_a = b.hash_m and a.sector = b.sector'.format(tb2, tb1)
    execute(sql, db)
    # upload hash_a
    createTable(tb3, '(hash_m varchar2(40), sector_m varchar2(4), hash_a varchar2(40), sector_a varchar2(4))', db, rm=True)
    sql = 'insert into {} select b.hash_m, b.sector sector_m, a.hash_a, a.sector sector_a from sisap_u11 a, {} b where a.hash_d = b.hash_d'.format(tb3, tb2)
    execute(sql, db)
    # download
    all_hashs = {hash_a: hash_m for (hash_a, hash_m) in getAll("select hash_a||':'||sector_a, hash_m||':'||sector_m from {}".format(tb3), db)}
    nf = [hash_m for hash_m, in getAll("select hash_m||':'||sector from {} a where not exists (select 1 from {} b where a.hash_m = b.hash_m and a.sector = b.sector)".format(tb1, tb2), db)]
    # drop
    sql = 'drop table {}'
    execute(sql.format(tb1), db)
    execute(sql.format(tb2), db)
    execute(sql.format(tb3), db)
    # return
    return all_hashs, nf


def do_it(params):
    sector, hashs = params
    execute('delete from flt_car', sector, anonim=True)
    listToTable(hashs, 'flt_car', sector, anonim=True)
    sql = "select sc_cip, to_char(sc_datseg,'YYYY-MM-DD') as data, sc_tipus, substr(sc_numcol,1,1) as cat_prof, sc_textseg \
            from prstb318 a, flt_car b where a.sc_cip = b.filtre"
    data = [list(row) for row in getAll(sql, sector, anonim=True)]
    return (sector, data)


def replace_salts(s):
    if s:
        s = s.replace('\r\n', ' .').replace('\n', ' .').replace('\r', ' .').replace(';', '.,')
    return s


if __name__ == '__main__':
    # raw input
    file = raw_input('nom del fitxer (amb ext) a tempFolder: ')
    separator = raw_input('separador de columnes (best ";" cause it gets replaced for ".,": ')
    position = raw_input('posicio de la columna hash:sector (inici a 0): ')
    header = raw_input('te header (s/n)? ')
    # read file
    file_data, file_header = get_file_data(file)
    # all hashs of hash
    printTime('getting patients')
    all_hashs, nf = get_all_hashs()
    upload = defaultdict(list)
    for hash in all_hashs:
        upload[hash.split(':')[1]].append((hash.split(':')[0],))
    # get data
    printTime('getting data')
    mega_data = multiprocess(do_it, [(sector, upload[sector]) for sector in sectors])
    # save data
    printTime('dumping data')
    data_to_file = [file_header + ['sector', 'data', 'tipus', 'especialitat', 'text']] if header else []
    for sector, data in mega_data:
        for row in data:
            data_to_file.append(file_data[all_hashs['{}:{}'.format(row[0], sector)]] + [sector] + map(replace_salts, row[1:]))
    writeCSV(tempFolder + '{}___sc.{}'.format('.'.join(file.split('.')[:-1]), file.split('.')[-1]), data_to_file, sep=separator)
    # nf
    if len(nf) > 0:
        data_to_nf = [file_header] if header else []
        data_to_nf.extend(file_data[hash] for hash in nf)
        writeCSV(tempFolder + '{}___nf.{}'.format('.'.join(file.split('.')[:-1]), file.split('.')[-1]), data_to_nf, sep=separator)
    printTime('done')
