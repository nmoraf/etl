# -*- coding: utf8 -*-

"""
Vinculació de pacients nascuts a partir de 2005 amb els seus
possibles familiars a través del NASS. S'utilitzen altres dades
administratives (edat, sexe, cognoms) per afinar la cerca.
Les dades es treuen d'explotació i el resultat es puja
a dues taules de REDICS (població i candidats).
"""

import collections as c

import sisapUtils as u


tb1 = 'sidiap_progenitors_poblacio'
tb2 = 'sidiap_progenitors_candidats'
db = 'redics'
users = ('PREDULMB', 'PREDUEHE', 'PREDUMAP')


class Progenitors(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """
        Creació de l'estructura necessària i execució en paral·lel
        a cada sector mitjançant la instanciació de Sector.
        """
        self.create_tables()
        u.multiprocess(Sector, u.sectors)
        # Sector('6837')  # debug

    def create_tables(self):
        """
        Crea les taules necessàries, que les ompliran
        els workers de Sector.
        """
        u.createTable(tb1, '(id int, codi_sector varchar2(4), \
                            usua_cip varchar2(40), cohort int, has_nass int, \
                            n_candidats int)', db, rm=True)
        u.grantSelect(tb1, users, db)
        u.createTable(tb2, '(id int, codi_sector varchar2(4), \
                            usua_cip varchar2(40), sexe varchar2(1), \
                            anys int, c1_is_c1 int, c1_is_c2 int, \
                            c2_is_c1 int, c2_is_c2 int)', db, rm=True)
        u.grantSelect(tb2, users, db)


class Sector(object):
    """Worker per sector."""

    def __init__(self, sector):
        """Execució seqüencial."""
        self.sector = sector
        self.get_anonymizer()
        self.get_dades()
        self.get_candidates()
        self.upload_tables()

    def get_anonymizer(self):
        """Obtenció PDPTB101."""
        sql = 'select usua_cip, usua_cip_cod from pdptb101'
        self.anonymizer = {cip: hash for (cip, hash) in u.getAll(
                                                            sql,
                                                            self.sector + 'a')}

    def get_dades(self):
        """
        Obtenció USUTB040 i creació de les estructures
        de població i de candidats.
        """
        self.poblacio = set()
        self.candidats = c.defaultdict(set)
        sql = "select usua_cip, usua_nass1||usua_nass2||usua_nass3, \
               usua_sexe, to_date(usua_data_naixement, 'J'), \
               usua_cognom1, usua_cognom2 \
               from usutb040"
        for cip, nass, sex, naix, c1, c2 in u.getAll(sql, self.sector):
            if cip in self.anonymizer:
                hash = self.anonymizer[cip]
                if naix.year > 2004:
                    self.poblacio.add((hash, nass, naix, c1, c2))
                if nass:
                    self.candidats[nass].add((hash, sex, naix, c1, c2))

    def get_candidates(self):
        """Unió de població amb candidats."""
        self.upload_poblacio = []
        self.upload_candidats = []
        for i, (hash, nass, naix, c1, c2) in enumerate(self.poblacio):
            id = '{}{}'.format(self.sector, i)
            has_nass = nass is not None
            if has_nass:
                candidats = self.candidats[nass]
                n_candidats = len(candidats) - 1
                for hash_c, sex_c, naix_c, c1_c, c2_c in candidats:
                    if hash_c != hash:
                        years = u.yearsBetween(naix_c, naix)
                        self.upload_candidats.append((id, self.sector, hash_c,
                                                      sex_c, years,
                                                      c1_c == c1, c1_c == c2,
                                                      c2_c == c1, c2_c == c2))
            else:
                n_candidats = 0
            self.upload_poblacio.append((id, self.sector, hash, naix.year,
                                         has_nass, n_candidats))

    def upload_tables(self):
        """Insert a REDICS."""
        u.listToTable(self.upload_poblacio, tb1, db)
        u.listToTable(self.upload_candidats, tb2, db)


if __name__ == '__main__':
    Progenitors()
