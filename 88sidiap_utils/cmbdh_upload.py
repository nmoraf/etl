from sisapUtils import tempFolder, createTable, listToTable, calcStatistics, grantSelect, printTime, execute, readCSV


table = 'sisap_cmbdh_ics'
rm = False
db = 'redics'
users = ('PREDULMB', 'PREDUEHE', 'PREDUMAP', 'PREDUMRR', 'PREDUEFC')


def create_table():
    fields = "(FITXER varchar2(4), \
               HOSPI varchar2(5), \
               CIP varchar2(40), \
               DNAIX varchar2(8), \
               SEXE varchar2(1), \
               MUNI varchar2(8), \
               DIST varchar2(8), \
               PAIS varchar2(8), \
               HISTORIA varchar2(10), \
               TACT varchar2(4), \
               REGECO varchar2(2), \
               DINGRES varchar2(8), \
               CINGRES varchar2(1), \
               PRINGRES varchar2(1), \
               DALTA varchar2(8), \
               CALTA varchar2(1), \
               UPDESTI varchar2(5), \
               PROGRAMA varchar2(1), \
               DP  varchar2(15), \
               DS1 varchar2(15), \
               DS2 varchar2(15), \
               DS3 varchar2(15), \
               DS4 varchar2(15), \
               DS5 varchar2(15), \
               DS6 varchar2(15), \
               DS7 varchar2(15), \
               DS8 varchar2(15), \
               DS9 varchar2(15), \
               CE1 varchar2(15), \
               CE2 varchar2(15), \
               CE3 varchar2(15), \
               CE4 varchar2(15), \
               CE5 varchar2(15), \
               PP  varchar2(15), \
               PS1 varchar2(15), \
               PS2 varchar2(15), \
               PS3 varchar2(15), \
               PS4 varchar2(15), \
               PS5 varchar2(15), \
               PS6 varchar2(15), \
               PS7 varchar2(15), \
               PX1 varchar2(15), \
               PX2 varchar2(15), \
               HINGRES varchar2(4), \
               HALTA varchar2(4), \
               SERALTA varchar2(5), \
               TGESTACIO varchar2(3), \
               PES1R varchar2(4), \
               SEXE1R varchar2(1), \
               PES2N varchar2(4), \
               SEXE2N varchar2(1), \
               NUMASSIS varchar2(22), \
               NUMEROID varchar2(1), \
               TCIS varchar2(1), \
               CIS varchar2(1), \
               TDI varchar2(1), \
               CDI varchar2(1), \
               CODING varchar2(8)) \
               PARTITION BY LIST (FITXER) (PARTITION d1899 VALUES ('1899'))"
    createTable(table, fields, db, rm=rm)


def upload_data(dat, header=True):
    file = tempFolder + 'CMBD_{}___hash.txt'.format(dat)
    try:
        execute("alter table {0} add partition d{1} VALUES ('{1}')".format(table, dat), db)
    except:
        pass
    execute('alter table {} truncate partition d{}'.format(table, dat), db)
    data = []
    for row in readCSV(file, sep=';'):
        if header:
            header = False
            continue
        if int(dat) >= 2018:
            row = row + ['CIM10MC']
        else:
            row = row + ['CIM9']
        data.append([dat] + row)
    listToTable(data, table, db)


if __name__ == '__main__':
    dat = raw_input('assumim que es mantenen les 56 + 1 columnes de la taula de redics (cmbdh_pre.py helps), amb header i separades per ;\r\nprimer cal anonimitzar, i mantenir el fitxer (CBMD_ANY___hash.txt) a tempFolder\r\nquin any carreguem? ')
    create_table()
    printTime('uploading')
    upload_data(dat)
    printTime('statistics')
    calcStatistics(table, db)
    grantSelect(table, users, db)
    printTime('done')
