# -*- coding: utf8 -*-

"""
.
"""

import hashlib as h

import sisaptools as u


SIDIAP_DB = ("nymeria_d", "sidiap_data")
SALT = "PADRIS"
REDICS_DB = ("redics", "data")
REDICS_TB = "sisap_conversio_padris"
REDICS_USR = ("PREDUMMP", "PREDUMBO")


class Padris(object):
    """."""

    def __init__(self):
        """."""
        sql = "select id_hash from poblacio"
        data = [(hash,) for hash, in u.Database(*SIDIAP_DB).get_all(sql)]
        with u.Database(*REDICS_DB) as redics:
            redics.create_table(REDICS_TB, ("ID_REDICS VARCHAR2(40)",),
                                remove=True)
            redics.set_grants("select", REDICS_TB, REDICS_USR)
            redics.list_to_table(data, REDICS_TB)


if __name__ == "__main__":
    Padris()
