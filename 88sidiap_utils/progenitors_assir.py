# -*- coding: utf8 -*-

"""
Vinculació de pacients nascuts a partir de 2005 amb les seves
possibles mares a través de les dates de naixement / part i l'ABS.
S'utilitzen altres dades administratives (edat, cognoms) per
afinar la cerca. Les dades es treuen d'explotació i el resultat
es puja a dues taules de REDICS (població i candidates).
"""

import collections as c

import sisapUtils as u


tb1 = 'sidiap_mares_assir_poblacio'
tb2 = 'sidiap_mares_assir_candidates'
db = 'redics'
users = ('PREDULMB', 'PREDUEHE', 'PREDUMAP')


class Mares(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """
        Creació de l'estructura necessària i execució en paral·lel
        a cada sector mitjançant la instanciació de Sector.
        """
        self.create_tables()
        u.multiprocess(Sector, u.sectors)
        # Sector('6837')  # debug

    def create_tables(self):
        """
        Crea les taules necessàries, que ompliran
        els workers de Sector.
        """
        u.createTable(tb1, '(id int, codi_sector varchar2(4), \
                            usua_cip varchar2(40), cohort int, \
                            n_candidates int)', db, rm=True)
        u.grantSelect(tb1, users, db)
        u.createTable(tb2, '(id int, codi_sector varchar2(4), \
                            usua_cip varchar2(40), anys int, \
                            c1_is_c1 int, c1_is_c2 int, \
                            c2_is_c1 int, c2_is_c2 int)', db, rm=True)
        u.grantSelect(tb2, users, db)


class Sector(object):
    """Worker per sector."""

    def __init__(self, sector):
        """Execució seqüencial."""
        self.sector = sector
        self.get_anonymizer()
        self.get_poblacio()
        self.get_embaras()
        self.upload_tables()

    def get_anonymizer(self):
        """Obtenció PDPTB101."""
        sql = 'select usua_cip, usua_cip_cod from pdptb101'
        self.anonymizer = {cip: hash for (cip, hash) in u.getAll(
                                                            sql,
                                                            self.sector + 'a')}

    def get_poblacio(self):
        """Obtenció població (USUTB040)."""
        self.poblacio = {}
        self.dnaix = c.defaultdict(set)
        self.dones = {}
        sql = "select usua_cip, usua_abs_codi_abs, usua_sexe, \
               to_date(usua_data_naixement, 'J'), \
               usua_cognom1, usua_cognom2 \
               from usutb040"
        i = 0
        for cip, abs, sex, naix, c1, c2 in u.getAll(sql, self.sector):
            if cip in self.anonymizer and abs:
                hash = self.anonymizer[cip]
                if naix.year > 2004:
                    i += 1
                    self.poblacio[hash] = ('{}{}'.format(self.sector, i),
                                           abs, naix, c1, c2)
                    self.dnaix[(abs, naix)].add(hash)
                if sex == 'D':
                    self.dones[hash] = (abs, naix, c1, c2)

    def get_embaras(self):
        """Obtenció embarassos (PRSTB220) i creuament de dades."""
        self.candidates = c.defaultdict(set)
        sql = "select cip_usuari_cip, emb_d_fi \
               from prstb220, usutb011 \
               where emb_c_tanca in ('P', 'C') and \
                     emb_d_fi is not null and \
                     emb_cip = cip_cip_anterior"
        for cip, dfi in u.getAll(sql, self.sector):
            if cip in self.anonymizer:
                hash_m = self.anonymizer[cip]
                if hash_m in self.dones:
                    abs_m, naix_m, c1_m, c2_m = self.dones[hash_m]
                    for hash_n in self.dnaix[(abs_m, dfi)]:
                        id_n, abs_n, naix_n, c1_n, c2_n = self.poblacio[hash_n]
                        self.candidates[id_n].add((hash_m,
                                                   u.yearsBetween(naix_m, dfi),
                                                   c1_m == c1_n, c1_m == c2_n,
                                                   c2_m == c1_n, c2_m == c2_n))

    def upload_tables(self):
        """Insert a REDICS."""
        upload_poblacio = [(id, self.sector, hash, naix.year,
                            len(self.candidates[id]))
                           for (hash, (id, abs, naix, c1, c2))
                           in self.poblacio.items()]
        u.listToTable(upload_poblacio, tb1, db)
        upload_candidates = []
        for id, candidates in self.candidates.items():
            upload_candidates.extend([(id, self.sector) + candidata
                                     for candidata in candidates])
        u.listToTable(upload_candidates, tb2, db)


if __name__ == '__main__':
    Mares()
