# coding: utf8

import pandas as pd

df = pd.read_csv('CMBD_2018.bak', delimiter=';', low_memory=False, usecols=range(56), dtype=str)

df.HOSP, df.CIP, df.Numero_id, df.Cdi, df.Tcis =  df.HOSP.str[-5:], df.CIP.str[:13], None, None, df.Tcis.str[-1:]


df.to_csv('CMBD_2018.txt', sep=';', index=False)


