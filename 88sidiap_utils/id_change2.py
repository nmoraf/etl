# -*- coding: utf8 -*-

"""
Converteix fitxers identificats (raw / sha1) amb CIP / NIA / DNI
al mateix fitxer però identificat amb HASH.
El fitxer d'entrada cal deixar-lo a tempFolder, i allà es generen
un màxim de 3 fitxers (només es generen els fitxers necessaris):
- __hash: fitxer anonimitzat amb HASH
- __nf: fitxer no anonimitzat amb els registres on no s'ha pogut trobat HASH
- __dup: fitxer no anonimitzat amb els registres que generen duplicats de HASH
La gestió i custòdia d'aquests fitxers és responsabilitat de l'usuari.
"""

import collections as c
import glob
import hashlib as h
import os

import sisapUtils as u


class Change(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_user_input()
        self.get_input_data()
        self.get_converter()
        self.get_output_data()
        self.write_output_files()
        self.print_result()

    @staticmethod
    def utf_to_latin(input):
        try:
            output = input.decode('utf-8').encode('latin1')
        except UnicodeDecodeError:
            output = input
        return output

    def get_user_input(self):
        """Preguntes inicials a l'usuari."""
        filename = raw_input('nom del fitxer (amb ext) a tempFolder: ')
        separator = raw_input('separador de columnes: ')
        header = raw_input('te header (s/n): ')
        id_in = raw_input('id entrada (cip/nia/dni): ')
        sha_in = raw_input('id amb sha1 (s/n): ')
        salt_in = raw_input('salt: ') if sha_in == 's' else None
        position = raw_input('posicio id (inici a 0): ')
        self.user_input = {
                'filename': u.tempFolder + filename,
                'separator': separator,
                'header': header == 's',
                'id_entrada': id_in,
                'sha1': sha_in == 's',
                'salt': salt_in,
                'id_posicio': int(position)}

    def get_input_data(self):
        """Dades del fitxer."""
        self.header = []
        self.input_data = [map(Change.utf_to_latin, row) for row
                           in u.readCSV(self.user_input['filename'],
                                        sep=self.user_input['separator'])]
        if self.user_input['header']:
            self.header = self.input_data[0]
            self.input_data = self.input_data[1:]
        n = 8 if self.user_input['id_entrada'] == 'dni' and self.user_input['sha1'] == False else None
        self.ids_in = set([str(row[self.user_input['id_posicio']][:n]).upper()
                          for row in self.input_data])

    def get_converter(self):
        """
        Conversor de CIP / NIA /DNI a HASH.
        En cas de CIP / NIA executa 16 workers en paral·lel
        segons el primer dígit del HASH.
        En cas de DNI executa 21 workers en paral·lel, un per cada sector.
        """
        if self.user_input['id_entrada'] in ('cip', 'nia'):
            jobs = [format(digit, 'x').upper() for digit in range(16)]
        elif self.user_input['id_entrada'] == 'dni':
            jobs = ['{}a'.format(sector) for sector in u.sectors]
        resultat = u.multiprocess(self.get_converter_worker, jobs)
        self.converter = c.defaultdict(set)
        for worker in resultat:
            for entrada, hash in worker:
                self.converter[entrada].add(hash)

    def get_sha1(self, input):
        """Retorna sha1."""
        return h.sha1(input + self.user_input['salt']).hexdigest().upper()

    def get_converter_worker(self, subgroup):
        """Worker de get_converter."""
        if self.user_input['id_entrada'] == 'cip':
            sql = "select usua_cip, usua_cip_cod from pdptb101 \
                   where usua_cip_cod like '{}%'".format(subgroup)
            db = 'pdp'
        elif self.user_input['id_entrada'] == 'nia':
            sql = "select usua_nia||'', usua_cip from md_poblacio \
                   where usua_cip like '{}%' \
                   and usua_nia is not null".format(subgroup)
            db = 'redics'
        elif self.user_input['id_entrada'] == 'dni':
            sql = "select substr(usua_dni, 0, 8), usua_cip_cod \
                   from usutb040 a, pdptb101 b \
                   where a.usua_cip = b.usua_cip \
                   and usua_dni is not null"
            db = subgroup
        converter = []
        for entrada, hash in u.getAll(sql, db):
            if self.user_input['sha1']:
                entrada = self.get_sha1(entrada)
            if entrada in self.ids_in:
                converter.append((entrada, hash))
        return converter

    def get_output_data(self):
        """Anonimització de dades."""
        self.output_data = c.defaultdict(list)
        for row in self.input_data:
            entrada = str(row[self.user_input['id_posicio']]).upper()
            if self.user_input['id_entrada'] == 'dni' and self.user_input['sha1'] == False:
                entrada = entrada[:8]
            if entrada in self.converter:
                hashs = list(self.converter[entrada])
                if len(hashs) == 1:
                    row[self.user_input['id_posicio']] = hashs[0]
                    self.output_data['hash'].append(row)
                else:
                    row[self.user_input['id_posicio']] = hashs[:]
                    self.output_data['dup'].append(row)
            else:
                self.output_data['nf'].append(row)

    def write_output_files(self):
        """Fitxers de sortida."""
        filename = '.'.join(self.user_input['filename'].split('.')[:-1])
        extension = self.user_input['filename'].split('.')[-1]
        for f in glob.glob(filename + '___*'):
            os.remove(f)
        for concept, dades in self.output_data.items():
            file = '{}___{}.{}'.format(filename, concept, extension)
            if self.header:
                upload = [self.header] + dades
            else:
                upload = dades
            u.writeCSV(file, upload, sep=self.user_input['separator'])

    def print_result(self):
        """Imprimeix per pantalla un resum del procés."""
        print 'origen: {} registres'.format(len(self.input_data))
        print 'convertits: {} registres'.format(len(self.output_data['hash']))
        print 'no trobats: {} registres'.format(len(self.output_data['nf']))
        print 'duplicats: {} registres'.format(len(self.output_data['dup']))


if __name__ == '__main__':
    Change()
