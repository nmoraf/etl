import datetime as d
import json
import socket
import sys


def send_petition(d):
    d = [d, "fi_de_la_transmissio"]
    HOST = "10.80.217.84"
    PORT = 53007
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    data_json = json.dumps(d)
    s.sendall(data_json)

    received = ""
    while True:
        packet = s.recv(4096)
        if not packet:
            break
        received += packet
        if '"fi_de_la_transmissio"]' in received:
            break

    received_json = json.loads(received)
    s.close()
    return received_json


resul = send_petition({'fx': 'create'})[0]
if resul != "fet":
    sys.exit(resul)

"""
print send_petition({'fx': 'patient', 'key': '700E6ECCF399128D364DC5FA41AE560552080237:6519'})
print send_petition({'fx': 'patient', 'key': '700E6ECCF399128D364DC5FA41AE560552080237:6519/'})
print send_petition({'fx': 'agrupador', 'key': '16'})
print send_petition({'fx': 'agrupador', 'key': '16/'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0203A'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0203A/'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0202A'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0202A/'})
"""
