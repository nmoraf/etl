from sisapUtils import *
from os import remove

imp = 'import_jail'
nod = 'nodrizas'
table = 'jail_centres'
eap = 'cat_centres'
merge = 'cat_centres_with_jail'
execute('drop table if exists {}'.format(table),nod)
execute('create table {} like {}'.format(table,eap),nod)
new = []
davidFile = tempFolder + 'centres_sisap.txt'
jailFile = 'jail_centres_klx.txt'


sql = "select distinct usua_uab_up from assignada where usua_situacio='A' and usua_uab_up<>''"

eapp = {up: val for (up, val) in readCSV('dades_noesb' + SLASH + jailFile)}

for up, in getAll(sql,imp):
#    sql = "select description from icsap.klx_sym_desc where micro_obj_id = (select micro_obj_id from icsap.klx_sym_desc where lang_code = 'FR' and description = '{}') and lang_code = 'EN'".format(up)
    try:
    #    br, lit = getOne(sql,'khalix')[0].split(" - ")
        if up in eapp:
            br, lit = eapp[up].split(" - ")
        else:
            br,lit = '',''
            new.append(up)
    except TypeError:
        br,lit = '',''
        new.append(up)
    execute('insert into {} (sector,ep,scs_codi,ics_codi,ics_desc,ics_codi_orig) VALUES ("6951","0208","{}","{}","{}","{}")'.format(table,up,br,lit,br),nod)

execute('drop table if exists {}'.format(merge),nod)    
execute('create table {} like {}'.format(merge,table),nod)
execute('alter table {} ENGINE=MERGE'.format(merge),nod)
execute('alter table {} UNION=({},{})'.format(merge,eap,table),nod)
    
if len(new) > 0:
    sendSISAP('jcamus@gencat.cat','Nuevas UP prisiones','Joan','\r\n'.join(map(str,new)))

davidList = [['ep', 'amb_codi', 'amb_desc', 'sap_codi', 'sap_desc', 'scs_codi', 'ics_codi', 'ics_desc', 'medea']]
sql = 'select ep, amb_codi, amb_desc, sap_codi, sap_desc, scs_codi, ics_codi, ics_desc, medea from {}'.format(merge)
for row in getAll(sql, nod):
    davidList.append(list(row))
writeCSV(davidFile, davidList, sep=';')
dat, = getOne("select date_format(data_ext,'%d %b') from dextraccio", nod)
sendSISAP('dmonterde@gencat.cat', 'Centres SISAP', 'David', 'Adjuntem centres SISAP en data {}.'.format(dat), file=davidFile)
remove(davidFile)

covid_table = "sisap_covid_cat_up_ecap"
covid_db = "redics"
covid_cols = "(up varchar2(5), ep varchar2(4), abs varchar2(3), is_linia int, \
               is_eapp int, medea varchar2(2), sector varchar2(4))"
covid_sql = "select scs_codi, ep, if(abs=0, null, lpad(abs, 3, '0')), tip_eap = 'N', 0, medea, sector from cat_centres"
covid_sql += " union select scs_codi, '0208', null, 0, 1, '', sector from jail_centres"
covid_data = list(getAll(covid_sql, "nodrizas"))
createTable(covid_table, covid_cols, covid_db, rm=True)
listToTable(covid_data, covid_table, covid_db)
grantSelect(covid_table, "PDP", covid_db)
calcStatistics(covid_table, covid_db)
