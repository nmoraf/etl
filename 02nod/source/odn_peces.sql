use nodrizas;

drop table if exists odn_peces;
create table odn_peces (
 id_cip_sec int
 ,peca int
 ,pato varchar(2000)
 ,tto varchar(2000)
 ,primary key (id_cip_sec,peca)
)
select
    id_cip_sec
    ,dpd_cod_p peca
    ,group_concat(distinct dpd_pat separator '@') pato
    ,'' tto
from
    import.odn507
    ,dextraccio
where
    dpd_data <= data_ext
group by
    1,2
;

drop table if exists odn_peces1;
create table odn_peces1 (
 id_cip_sec int
 ,peca int
 ,tto varchar(2000)
 ,primary key (id_cip_sec,peca)
)
select
    id_cip_sec
    ,dtd_cod_p peca
    ,group_concat(distinct dtd_tra separator '@') tto
from
    import.odn508
    ,dextraccio
group by
    1,2
;

update odn_peces a inner join odn_peces1 b on a.id_cip_sec=b.id_cip_sec and a.peca=b.peca
set a.tto=b.tto
;

drop table if exists odn_peces2;
create table odn_peces2 (
 id_cip_sec int
 ,tto varchar(2000)
 ,primary key (id_cip_sec)
)
select
    id_cip_sec
    ,group_concat(distinct tb_trac separator '@') tto
from
    import.odn511
    ,dextraccio
group by
    1
;

update odn_peces a inner join odn_peces2 b on a.id_cip_sec=b.id_cip_sec
set a.tto=concat(a.tto,if(a.tto = '','','@'),b.tto)
;

update odn_peces
set pato=concat('@',pato,'@')
,tto=concat('@',tto,'@')
;

drop table if exists odn_peces1;
drop table if exists odn_peces2;
