use nodrizas
;

drop table if exists eqa_proves;
create table eqa_proves(
id_cip_sec int,
agrupador int,
data date,
index(id_cip_sec)
)
select
    a.id_cip_sec
    ,agrupador
    ,oc_data data
from
    nod_proves a
    inner join eqa_criteris_proves c on a.inf_codi_prova = c.criteri_codi
where
    oc_data between inici and final
;
