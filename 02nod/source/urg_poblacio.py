from sisapUtils import *
from collections import Counter,defaultdict

imp = 'import'
nod = 'nodrizas'
file = tempFolder + 'urg.txt'
tabPacients = 'urg_poblacio'
tabCentres = 'urg_centres'

centres = {}
sql = "select up_codi_up_scs, a.up_codi_up_ics, up_desc_up_ics, c.dap_codi_dap, c.dap_desc_dap, d.amb_codi_amb, d.amb_desc_amb \
       from cat_gcctb007 a \
       inner join cat_gcctb008 b on a.up_codi_up_ics = b.up_codi_up_ics \
       inner join cat_gcctb006 c on a.dap_codi_dap = c.dap_codi_dap \
       inner join cat_gcctb005 d on c.amb_codi_amb = d.amb_codi_amb \
       where (a.up_desc_up_ics like 'ACUT%' or \
	          a.up_desc_up_ics like 'CUAP%' or \
              a.up_desc_up_ics like 'PAC %' or \
	          a.up_desc_up_ics like 'CAC %' or \
	          a.up_desc_up_ics like 'DISPOSITIU%' or \
	          a.up_desc_up_ics like '%CONTINUADA%') and \
	         a.up_data_baixa = 0 and \
	         b.up_data_baixa = 0"
for up, br, des, sapc, sapd, ambc, ambd in getAll(sql, imp):
    centres[up] = ['CUAP' + br if br[0] == "0" else br, des, br, sapc, sapd, ambc, ambd]

idcip = defaultdict(list)
idcipsec = {}
sql = "select id_cip,id_cip_sec from u11"
for cip,sec in getAll(sql,'import'):
    idcip[cip].append(sec)
for cip,secs in idcip.items():
    for sec in secs:
        idcipsec[sec] = secs

visitats = {}
visitatsPerCentre = Counter()
sql = "select id_cip_sec,visi_up from visites1,nodrizas.dextraccio where visi_data_visita between date_add(date_add(data_ext,interval - 12 month),interval +1 day) and data_ext and visi_situacio_visita='R'"
for id,up in getAll(sql,imp):
    if up in centres and (id,up) not in visitats:
        visitatsPerCentre[up] += 1
        for sec in idcipsec[id]:
            if id == sec:
                val = 1
            else:
                val = 0
            visitats[sec,up,val] = True
    
with openCSV(file) as c:
    for id,up,val in visitats:
        c.writerow([id,up,val])
execute('drop table if exists {}'.format(tabPacients),nod)
execute('create table {} (id_cip_sec int,up varchar(5),isReal int)'.format(tabPacients),nod)
loadData(file,tabPacients,nod)
execute('alter table {} add unique id (id_cip_sec,up,isReal)'.format(tabPacients),nod)

with openCSV(file) as c:
    for up,n in visitatsPerCentre.items():
        c.writerow([up] + centres[up] + [n])
execute('drop table if exists {}'.format(tabCentres),nod)
execute('create table {} (scs_codi varchar(5),ics_codi varchar(10),ics_desc varchar(40),ics_codi_orig varchar(5),sap_codi varchar(5), sap_desc varchar(255), amb_codi varchar(5), amb_desc varchar(255), visitats int,unique (scs_codi))'.format(tabCentres),nod)
loadData(file,tabCentres,nod)
