# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "inf_problemes"
DATABASE = "nodrizas"


SPEC = {
    "HTA": {"ag": [55]},
    "DM": {"ag": [18, 24]},
    "IC": {"ag": [21]},
    "MPOC": {"ag": [62]},
    "RESP": {"ag": [136],
             "ps": ["J60", "J61", "J62", "J62.0", "J62.8", "J63", "J63.0",
                    "J63.1", "J63.2", "J63.3", "J63.4", "J63.5", "J63.8",
                    "J84.1", "J96.1"]},
    "SAOS": {"ag": [356]},
    "ASMA": {"ag": [58]},
    "DLP": {"ag": [47]},
    "OBE": {"ag": [239]},
    "INURI": {"ag": [454]},
    "INFEC": {"ag": [455]},
    "DEMEN": {"ag": [86]},
    "FERID": {"ps": ["L89", "I83.0", "I73.9", "L89.4", "I00044", "I00046"]},
    "FA": {"ag": [180]},
    "OSTEO": {"ps": ["M17", "M17.0", "M17.1", "M17.2", "M17.3", "M17.4",
                     "M17.5", "M17.9"],
              "th": [5371, 1425, 1426, 1451]},
    "VIH": {"ag": [666]},
    "HEP": {"ps": ["B18", "B18.0", "B18.1", "B18.2", "B18.8", "B18.9"]},
    "CI": {"ag": [1]},
    "DIGES1": {"ag": [328],
               "ps": ["K90.0", "K58", "K58.0", "K58.9", "C01-K58.8"]},
    "DIGES2": {"ps": ["K50", "K50.0", "K50.1", "K50.8", "K50.9", "K51",
                      "K51.0", "K51.1", "K51.2", "K51.3", "K51.4", "K51.5",
                      "K51.8", "K51.9"]},
    "OP": {"ag": [77]},
    "COSES": {"ps": ["M79.7", "G93.3", "T78.4"]},
    "ANSI": {"ps": ["F41", "F41.0", "F41.1", "F41.2", "F41.3",
                    "F41.8", "F41.9"]},
    "DEPRE": {"ag": [425]},
    "IR": {"ag": [52, 53, 54]},
    "ONCO": {"ag": [721],
             "tancat": True},
    "IV": {"ps": ["I87.2"]},
    "MAP": {"ag": [11]},
    "NEURO": {"ag": [728, 344],
              "th": [5262]},
    "FRAG": {"ag": [764]},
    "DISC": {"ag": [301, 45]},
    "OH": {"ag": [84]},
    "DROGUES": {"ag": [797]},
    "ADD": {"ps": ["F63.0"]},
    "DISF": {"ps": ["R13"]}
}


class Problemes(object):
    """."""

    def __init__(self):
        """."""
        self.invert_spec()
        self.create_table()
        self.get_problemes()

    def invert_spec(self):
        """."""
        self.codis = c.defaultdict(set)
        self.tancats = set()
        cim10mc = {row[0]: row[1] for row
                   in u.getAll("select ps_cod_o_ap, cim10mc \
                                from cat_prstb001cmig", "import")}
        for grup, codis in SPEC.items():
            for key, values in codis.items():
                if key == "tancat":
                    self.tancats.add(grup)
                else:
                    for value in values:
                        if key == "ag":
                            sql = "select criteri_codi from eqa_criteris \
                                   where agrupador = {}".format(value)
                            for fill, in u.getAll(sql, "nodrizas"):
                                self.codis[("ps", fill)].add(grup)
                        elif key == "ps":
                            for ps in (value, cim10mc.get(value, value)):
                                self.codis[("ps", ps)].add(grup)
                        elif key == "th":
                            for th in (value, value + 10000):
                                self.codis[("th", th)].add(grup)

    def create_table(self):
        """."""
        u.createTable(TABLE, "(id int, ps varchar(255))", DATABASE, rm=True)

    def get_problemes(self):
        """."""
        self.resultat = []
        jobs = [k for (k, v) in sorted(u.getSubTables("problemes").items(),
                                       key=lambda x: x[1], reverse=True)
                if v > 0]
        u.multiprocess(self._get_problemes_worker, jobs, 12)

    def _get_problemes_worker(self, taula):
        """."""
        problemes = c.defaultdict(set)
        sql = "select id_cip_sec, pr_cod_ps, pr_th, \
                      if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) \
               from {}, \
                    nodrizas.dextraccio \
               where pr_dde <= data_ext and \
                     (pr_data_baixa = 0 or \
                      pr_data_baixa > data_ext)".format(taula)
        for id, ps, th, tancat in u.getAll(sql, "import"):
            for key in [("ps", ps), ("th", th)]:
                for grup in self.codis[key]:
                    if not tancat or grup in self.tancats:
                        problemes[id].add(grup)
        upload = [(id, ",".join(grups)) for (id, grups) in problemes.items()]
        u.listToTable(upload, TABLE, DATABASE)


if __name__ == "__main__":
    Problemes()
