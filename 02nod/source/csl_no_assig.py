from sisapUtils import createTable, getOne, multiprocess, getAll, listToTable


orig = 'import'
table = 'csl_no_assig'
db = 'nodrizas'


def do_it(params):
    taula, poblacio = params
    data = set()
    sql = "select id_cip_sec, visi_up, visi_centre_codi_centre from {}, nodrizas.dextraccio \
           where visi_data_visita between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext and visi_situacio_visita = 'R' and visi_rel_proveidor in ('3', '4')".format(taula)
    for id, up, centre in getAll(sql, orig):
        if id not in poblacio or centre != poblacio[id]:
            data.add((id, up))
    listToTable(data, table, db)


if __name__ == '__main__':
    createTable(table, '(id_cip_sec int, up varchar(5))', db, rm=True)
    poblacio = {id: centre for (id, centre) in getAll('select id_cip_sec, centre_codi from assignada_tot', 'nodrizas')}
    tables = getOne('show create table visites1', 'import')[1].split('UNION=(')[1][:-1].split(',')
    jobs = [(t, poblacio) for t in tables]
    multiprocess(do_it, jobs)
