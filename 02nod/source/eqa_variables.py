from sisapUtils import createDatabase, createTable, readCSV, baseFolder, getSubTables, multiprocess, getOne, getAll, listToTable, getTablePartitions, yearsBetween, monthsBetween, execute, SLASH
from collections import defaultdict
from datetime import datetime
import multiprocessing as m


imp = 'import'
jail = 'import_jail'
temp = ('sisap', 'sisap_3308')
nod = 'nodrizas'

source = {
    'variables': {'model': 'temps', 'dat': 'vu_dat_act', 'var': 'vu_cod_vs', 'val': 'vu_val', 'uni': 1, 'eapp': True},
    'laboratori': {'model': 'temps', 'dat': 'cr_data_reg', 'var': 'cr_codi_prova_ics',
                   'val': "trim(replace(replace(replace(replace(replace(replace(cr_res_lab, ',', '.'), '>', ''), '<', ''), '+', ''), CHAR(9), ''), 'mL/min/1.73m2', ''))",
                   'uni': 'cr_unitats_lab', 'eapp': True},
    'activitats': {'model': 'temps', 'dat': 'au_dat_act', 'var': 'au_cod_ac', 'val': 'if(au_val = \'\', 0, au_val)', 'uni': 1, 'eapp': True},
    'pla2': {'model': 'sector', 'dat': 'snnv_data_alta', 'var': 'snnv_cod_ac', 'val': 0, 'uni': 1, 'eapp': False},
    'nen11': {'model': 'sector', 'dat': 'if(val_d_v > 0, val_d_v, val_data)', 'var': 'val_var', 'val': 'val_val', 'uni': 1, 'eapp': False},
    'nen12': {'model': 'sector', 'dat': 'if(val_d_v > 0, val_d_v, val_data)', 'var': 'val_var', 'val': 'val_val', 'uni': 1, 'eapp': False},
    'assir216': {'model': 'sector', 'dat': 'val_data', 'var': 'val_var', 'val': 'val_val', 'uni': 1, 'eapp': False},
    'xml_detall': {'model': 'sector', 'dat': 'xml_data_alta', 'var': "concat(xml_tipus_orig, '_', camp_codi)", 'val': 'camp_valor', 'uni': 1, 'eapp': False},
}

criteris = {
    'A': ('eqa_criteris_{theme}', 'minimt >= {diff}'),
    'N': ('ped_criteris_var_{theme}', "if(unitat = 'a', 12, 1) * minim_t >= {diff}"),
    'S': ('eqa_criteris_soc{theme}', 'minimt >= 0'),
    'X': ('ass_criteris_var_{theme}', 'minim_t >= {diff}'),
    'P': ('eqa_criteris_psicosi_{theme}', 'minimt >= {diff}'),
}

bolets = {
    'A': 'eqa_variables_bolets',
    'N': 'ped_variables_bolets',
    'S': 'ts_variables_bolets',
    'X': 'ass_variables_bolets',
}

destination = {
    'A': ('eqa_variables', '(id_cip_sec int, agrupador int, data_var date, valor double null, usar int)'),
    'N': ('ped_variables', '(id_cip_sec int, agrupador int, val varchar(50), dat date, edat_a int, edat_m int)'),
    'S': ('ts_variables', '(id_cip_sec int, agrupador int, val varchar(50), dat date, last int)'),
    'X': ('ass_variables', '(id_cip_sec int, agrupador int, val_num double, val_txt varchar(150), dat date)'),
    'P': ('csl_psicosi', '(id_cip_sec int, agrupador int, unique(id_cip_sec, agrupador))'),
}

mitjanes = {int(codi): (n, temps, agr) for (codi, n, temps, agr) in getAll('select criteri_codi, inclusio, ps_tancat, agrupador from eqa_criteris_mitjanes', nod)}


def create_structure():
    createDatabase(temp, rm=True)
    for grup in destination:
        if grup == 'A':
            createTable(grup, '(id_cip_sec int, agr int, dat date, val double null) partition by hash(id_cip_sec) partitions 64', temp, rm=True)
        else:
            createTable(grup, '(id_cip_sec int, agr int, dat date, val varchar(50)) partition by hash(id_cip_sec) partitions 4', temp, rm=True)
        createTable(destination[grup][0], destination[grup][1], nod, rm=True)


def get_poblacio():
    poblacio = defaultdict(set)
    naixement = {}
    sql = 'select id_cip_sec, edat, data_naix from assignada_tot_with_jail'
    for id, edat, naix in getAll(sql, nod):
        poblacio['S'].add(id)
        if edat > 14:
            poblacio['A'].add(id)
        else:
            poblacio['N'].add(id)
            naixement[id] = naix
    sql = 'select id_cip_sec from ass_poblacio'
    for id, in getAll(sql, nod):
        poblacio['X'].add(id)
    sql = 'select id_cip_sec from eqa_problemes where ps=415'
    for id, in getAll(sql, nod):
        poblacio['P'].add(id)
    return poblacio, naixement


def get_lab_conversion():
    limits = {}
    for agr, inf, sup in readCSV('dades_noesb' + SLASH + 'labLimits.txt', sep=';'):
        limits[int(agr)] = float(inf), float(sup)
    unitats = {}
    for agr, uni, factor in readCSV('dades_noesb' + SLASH + 'labUnitats.txt', sep=';'):
        unitats[int(agr), uni.lower()] = float(factor), limits[int(agr)][0], limits[int(agr)][1]
    return unitats


def get_partitions():
    unitats = get_lab_conversion()
    partitions = []
    for table, attrs in source.items():
        partitions.extend([(table, partition, imp, load, attrs['model'] == 'temps', unitats) for partition, load in getSubTables(table).items() if load > 0])
        if attrs['eapp']:
            partitions.append((table, table, jail, 0, False, unitats))
    return sorted(partitions, key=lambda x: x[3], reverse=True)


def check_period(tb, db, dat, lab):
    sql = "select period_diff(date_format(data_ext, '%Y%m'), date_format({0}, '%Y%m')) from {1}, nodrizas.dextraccio limit 1".format(dat, tb)
    diff, = getOne(sql, db)
    return diff


def get_codes(theme, diff):
    codes = defaultdict(list)
    master = "select '{grup}', criteri_codi, agrupador, date_format(inici, '%Y%m%d'), date_format(final,'%Y%m%d') from {table} where {where}"
    for grup, data in criteris.items():
        sql = master.format(grup=grup, table=data[0].format(theme=theme), where=data[1].format(diff=diff))
        try:
            for grup, codi, agr, ini, fi in getAll(sql, nod):
                codes[codi].append((grup, agr, ini, fi))
        except:
            pass
    return codes


def download_it():
    while queue.qsize() > 0:
        theme, tb, db, load, check, unitats = queue.get()
        attrs = source[theme]
        if check:
            diff = check_period(tb, db, attrs['dat'], theme == 'laboratori')
        else:
            diff = 0
        codes = get_codes(theme, diff)
        if len(codes) > 0 and diff >= 0:
            sql = "select id_cip_sec, {var}, date_format({dat}, '%Y%m%d'), {val}, {uni} from {tb} where {var} in {codes}".format(
                var=attrs['var'],
                dat=attrs['dat'],
                val=attrs['val'],
                uni=attrs['uni'],
                tb=tb,
                codes=str(tuple([code for code in codes]))
                )
            data = defaultdict(list)
            for id, var, dat, val, uni in getAll(sql, db):
                vmin, vmax = -99999, 99999
                for grup, agr, dini, dfi in codes[var]:
                    if id in poblacio[grup]:
                        if grup == 'A':
                            if agr == 447:
                                vmin = 10
                            try:
                                val = float(val)
                            except ValueError:
                                if agr in (388, 644, 650, 652, 654, 655, 777, 778, 779, 780):
                                    val = 1
                                else:
                                    continue
                            if theme == 'laboratori':
                                try:
                                    fct, vmin, vmax = unitats[agr, uni.lower()]
                                except KeyError:
                                    try:
                                        u = 'g' if 'g' in uni.lower() else 'm' if 'mol' in uni.lower() else ''
                                        fct, vmin, vmax = unitats[agr, u]
                                    except KeyError:
                                        try:
                                            fct, vmin, vmax = unitats[agr, 'd']
                                        except KeyError:
                                            fct = -1
                                val = val * fct
                            if vmin <= val <= vmax and dini <= dat <= dfi:
                                data[grup].append([id, agr, dat, val])
                        elif grup == 'S':
                            if dat <= dfi:
                                data[grup].append([id, agr, dat, val])
                        else:
                            if dini <= dat <= dfi:
                                data[grup].append([id, agr, dat, val])
            for grup in data:
                listToTable(data[grup], grup, temp)
        queue.task_done()


def get_bolets(params):
    grup, table, poblacio = params
    data = []
    sql = "select id_cip_sec, agr, date_format(dat,'%Y%m%d'), val from {}".format(table)
    for id, agr, dat, val in getAll(sql, nod):
        if id in poblacio:
            data.append([id, agr, dat, val])
    listToTable(data, grup, temp)


def get_jobs():
    jobs = []
    for grup in criteris:
        table = grup
        jobs.extend([(grup, table, partition) for partition in getTablePartitions(table, temp)])
    return jobs


def process_it():
    while queue.qsize() > 0:
        grup, table, partition = queue.get()
        dest = destination[grup][0]
        sql = "select id_cip_sec, agr, date_format(dat, '%Y%m%d'), val from {} partition ({})".format(table, partition)
        if grup == 'A':
            process_adults(sql, dest)
        elif grup == 'N':
            process_nens(sql, naixement, dest)
        elif grup == 'S':
            process_social(sql, dest)
        elif grup == 'X':
            process_assir(sql, dest)
        elif grup == 'P':
            process_psicosi(sql, dest)
        queue.task_done()

def process_adults(sql, dest):
    dext_d, = getOne("select data_ext from dextraccio", 'nodrizas')
    dext_s, = getOne("select date_format(data_ext, '%Y%m%d') from dextraccio", 'nodrizas')
    data = defaultdict(lambda: defaultdict(list))
    for id, agr, dat, val in getAll(sql, temp):
        data[id, agr][dat].append(val)
    upload = []
    for (id, agr), dates in data.items():
        pac = ((id, agr, dat, sum(valors) / len(valors)) for dat, valors in dates.items())
        res = ((id, agr, dat, val, i) for i, (id, agr, dat, val) in enumerate(sorted(pac, key=lambda x: x[2], reverse=True), start=1))
        if agr not in mitjanes:
            upload.extend(res)
        else:
            res = list(res)
            valor = 0.0
            n = 0
            for id, agr, dat, val, i in res:
                if i <= mitjanes[agr][0] and monthsBetween(datetime.strptime(dat, '%Y%m%d').date(), dext_d) < mitjanes[agr][1]:
                    valor += val
                    n += 1
            if n > 0:
                res.append((id, mitjanes[agr][2], dext_s, round(valor / n, 2), 1))
            upload.extend(res)
    listToTable(upload, dest, nod)


def process_nens(sql, naixement, dest):
    data = []
    for id, agr, dat, val in getAll(sql, temp):
        naix = naixement[id]
        _dat = datetime.strptime(dat, '%Y%m%d').date()
        if _dat >= naix:
            anys = yearsBetween(naix, _dat)
            mesos = monthsBetween(naix, _dat)
            data.append([id, agr, val, dat, anys, mesos])
    listToTable(data, dest, nod)


def process_social(sql, dest):
    data = []
    last = defaultdict(str)
    for id, agr, dat, val in getAll(sql, temp):
        data.append([id, agr, dat, val])
        last[id, agr] = max(dat, last[id, agr])
    upload = []
    for id, agr, dat, val in data:
        upload.append([id, agr, val, dat, 1 if dat == last[id, agr] else 0])
    listToTable(upload, dest, nod)


def process_assir(sql, dest):
    data = []
    for id, agr, dat, val in getAll(sql, temp):
        data.append([id, agr, val, val, dat])
    listToTable(data, dest, nod)


def process_psicosi(sql, dest):
    data = []
    for id, agr, dat, val in getAll(sql, temp):
        data.append([id, agr])
    listToTable(data, dest, nod)


def clean_house():
    createDatabase(temp, rm=True)


if __name__ == '__main__':
    create_structure()
    poblacio, naixement = get_poblacio()
    jobs = get_partitions()
    queue = m.JoinableQueue()
    for job in jobs:
        queue.put(job)
    for i in range(16):
        proc = m.Process(target=download_it)
        proc.daemon = True
        proc.start()
    queue.join()
    # multiprocess(download_it, jobs, 8, close=True)
    jobs = [(grup, table, poblacio[grup]) for grup, table in bolets.items()]
    multiprocess(get_bolets, jobs, close=True)
    jobs = get_jobs()
    queue = m.JoinableQueue()
    for job in jobs:
        queue.put(job)
    for i in range(8):
        proc = m.Process(target=process_it)
        proc.daemon = True
        proc.start()
    queue.join()
    # multiprocess(process_it, jobs, 8, close=True)
    clean_house()
