from sisapUtils import *

imp = 'import'
nod = 'nodrizas'
tab = 'inf_grupal'

grupal1 = {}
sql = 'select id_cip_sec,codi_sector,cinp_num_int from grupal1'
for id,sec,num in getAll(sql,imp):
    grupal1[sec,num] = id

grupal3 = {}
sql = 'select id_cip_sec,codi_sector,pagr_num_grup from grupal3'
for id,sec,grup in getAll(sql,imp):
    grupal3[id,sec,grup] = True

resultat = []    
sql = 'select codi_sector,inp_num_int,inp_data,inp_num_grup from grupal2'
for sec,num,data,grup in getAll(sql,imp):
    try:
        id = grupal1[sec,num]
    except KeyError:
        continue
    else:
        if (id,sec,grup) in grupal3:
            resultat.append([id,data])
    
file = tempFolder + tab
writeCSV(file,resultat)
execute('drop table if exists {}'.format(tab),nod)
execute('create table {} (id_cip_sec int,data date)'.format(tab),nod)
loadData(file,tab,nod)
