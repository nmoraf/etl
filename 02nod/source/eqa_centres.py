# encoding: utf8
from sisapUtils import createTable, tempFolder, baseFolder, readCSV, getAll, sectors, multiprocess, listToTable, execute, writeCSV, getOne, sendSISAP, yearsBetween, SLASH, grantSelect, calcStatistics
from collections import Counter, defaultdict
from os.path import realpath
from unicodedata import normalize


medeaFile = 'medea_eap.txt'
aquasFile = 'aquas.txt'
imp = 'import'
nod = 'nodrizas'
table = 'cat_centres'
ctl = 'sisap_ctl'
table_new = 'eap_nous'
permanent = "permanent"


def do_it(params):
    sector, ups, medea, aquas, has_soc, has_odn = params
    dext, = getOne("select data_ext from dextraccio", "nodrizas")
    sql = "select usua_uab_up, usua_data_naixement from assignada_s{} where usua_uab_up <> '' and usua_situacio = 'A'".format(sector)
    assig = defaultdict(Counter)
    for up, naix in getAll(sql, imp):
        assig[up][yearsBetween(naix, dext) > 14] += 1

    sql = "select pr_up from problemes_s{}".format(sector)
    ps = Counter()
    for up, in getAll(sql, imp):
        ps[up] += 1

    upload = []
    for up in assig:
        if sum(assig[up].values()) > 1000 and up in ps and ps[up] > 1000 and up in ups:
            br = ups[up]['br']
            data = ups[up]['data']
            grup = medea.get(br)
            aquas_val = aquas.get(up)
            adults = assig[up][True]
            nens = assig[up][False]
            if adults < 50:
                tipus = "N"
            elif nens < 50:
                tipus = "A"
            else:
                tipus = "M"
            upload.append((sector,) + data + (grup, aquas_val, adults, nens, tipus, 1 * (up in has_soc), 1 * (up in has_odn)))
            listToTable(upload, table, nod)


if __name__ == '__main__':

    createTable(table, "(sector varchar(4), ep varchar(4) not null default '', amb_codi varchar(2) not null default '', amb_desc varchar(40) not null default '', sap_codi varchar(2) not null default '', \
                        sap_desc varchar(40) not null default '', scs_codi varchar(5) not null default '', ics_codi varchar(5) not null default '', ics_desc varchar(40) not null default '', \
                        ics_codi_orig varchar(5), abs int, rs varchar(4), aga varchar(8), medea varchar(2) not null default '', aquas double null, adults int, nens int, tip_eap varchar(1), soc int, odn int, unique(scs_codi))", nod, rm=True)
    createTable(table_new, "(scs_codi varchar(5), ics_codi varchar(5), ics_desc varchar(40))", ctl, rm=True)

    medea = {br: grup for (br, num, grup) in readCSV('dades_noesb' + SLASH + medeaFile)}
    aquas = {up: val for (up, val) in readCSV('dades_noesb' + SLASH + aquasFile)}
    
    has_soc = set([up for up, in getAll("select up from cat_profvisual a where a.visualitzacio = 1 and exists (select 1 from cat_professionals b where a.ide_usuari = b.ide_usuari and b.tipus = 'T')", imp)])
    has_odn = set([up for up, in getAll("select up from cat_profvisual a where a.visualitzacio = 1 and exists (select 1 from cat_professionals b where a.ide_usuari = b.ide_usuari and b.tipus = 'O')", imp)])

    sql = "select \
            up.up_codi_up_scs \
            ,if(br.up_codi_up_ics='04374','BR366',if(br.up_codi_up_ics like '0%',concat('B',right(br.up_codi_up_ics,4)),br.up_codi_up_ics)) \
            ,br.e_p_cod_ep \
            ,amb.amb_codi_amb \
            ,amb.amb_desc_amb \
            ,sap.dap_codi_dap \
            ,sap.dap_desc_dap \
            ,up.up_codi_up_scs \
            ,if(br.up_codi_up_ics='04374','BR366',if(br.up_codi_up_ics like '0%',concat('B',right(br.up_codi_up_ics,4)),br.up_codi_up_ics)) \
            ,br.up_desc_up_ics \
            ,br.up_codi_up_ics \
        from \
            cat_gcctb007 br \
            inner join cat_gcctb008 up on br.up_codi_up_ics=up.up_codi_up_ics \
            inner join cat_gcctb006 sap on br.dap_codi_dap=sap.dap_codi_dap \
            inner join cat_gcctb005 amb on sap.amb_codi_amb=amb.amb_codi_amb \
        where \
            br.up_data_baixa = 0"

    abs_up = {abs: up for (up, abs) in getAll('select abs_codi_up, abs_codi_abs from cat_rittb001', imp)}
    agas = {abs_up[abs]: (abs, 'RS' + str(rs).zfill(2), 'AGA' + str(aga).zfill(5)) for (abs, rs, aga) in getAll('select abs_cod, rs_cod, aga_cod from cat_sisap_agas', imp) if abs in abs_up}
    up_ori = {up: ori for (up, ori) in getAll("select lloc_codi_up, min(lloc_up_rca) from cat_pritb025_def where lloc_codi_up <> lloc_up_rca and lloc_modul_codi_servei = 'PED' group by lloc_codi_up", imp)}
    
    ups = {}
    for row in getAll(sql, imp):
        up, br, data = row[0], row[1], row[2:]
        des_norm = normalize('NFD', data[7].decode('latin1')).encode('ascii', 'ignore')
        is_linia = 'LINIA PEDIATRICA' in des_norm and up in up_ori
        if is_linia:
            data_aga = ((None,) + agas[up_ori[up]][1:]) if up_ori[up] in agas else (None, None, None)
        else:
            data_aga = agas[up] if up in agas else (None, None, None)
        ups[up] = {'br': br, 'data': data + data_aga}

    jobs = [(sector, ups, medea, aquas, has_soc, has_odn) for sector in sectors]
    multiprocess(do_it, jobs, 8)

    execute("insert into {} select scs_codi, ics_codi, ics_desc from {}.{} where medea = ''".format(table_new, nod, table), ctl)

    gervasi_data = [row for row in getAll("select scs_codi, ics_codi, medea from cat_centres", "nodrizas") if row[2]]
    gervasi_table = "sisap_eap_tipus"
    gervasi_cols = "(up_codi_up_scs varchar(5), up_codi_up_ics varchar(5), sisap_tipus varchar(2))"
    gervasi_db = "pdp"
    gervasi_users = ("PREDURAP", "PREDUFFA")
    createTable(gervasi_table, gervasi_cols, gervasi_db, rm=True)
    listToTable(gervasi_data, gervasi_table, gervasi_db)
    grantSelect(gervasi_table, gervasi_users, gervasi_db)
    
    execute("drop table if exists {0}".format(table), permanent)
    execute("create table {0} like {1}.{0}".format(table, nod), permanent)
    execute("insert into {0} select * from {1}.{0}".format(table, nod), permanent)
    
