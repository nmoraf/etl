use nodrizas
;

drop table if exists eqa_variables_bolets;
create table eqa_variables_bolets (
    id_cip_sec int
    ,agr int
    ,val double
    ,dat date
);

drop table if exists ped_variables_bolets;
create table ped_variables_bolets like eqa_variables_bolets;

drop table if exists ass_variables_bolets;
create table ass_variables_bolets like eqa_variables_bolets;

drop table if exists ts_variables_bolets;
create table ts_variables_bolets like eqa_variables_bolets;

###############################################################################################

create temporary table valoracio1 (
    id_cip_sec int
    ,var varchar(3)
    ,dat date
    ,primary key (id_cip_sec,var)
) ignore
select
    id_cip_sec
    ,valh_grup var
    ,valh_data dat 
from 
    import.valoracio
    ,dextraccio 
where
    valh_data between date_add(date_add(data_ext,interval -1 year),interval +1 day) and data_ext
;

create temporary table valoracio2 (
    id_cip_sec int
    ,primary key (id_cip_sec)
)
select
    id_cip_sec
from
    valoracio1
where
    var<>'ANA'
group by
    id_cip_sec
having
    count(1)>=14
;

insert ignore into valoracio2
select
    id_cip_sec
from
    valoracio1
where
    var='ANA'
;

insert into eqa_variables_bolets
select
	b.id_cip_sec
	,95 agr
	,1 val
	,max(dat) dat
from
	valoracio2 a
    inner join valoracio1 b on a.id_cip_sec=b.id_cip_sec
group by
	id_cip_sec
;

###############################################################################################

insert into eqa_variables_bolets
select
    id_cip_sec
    ,'104' agr
    ,0 val
    ,val_data dat
from
    import.social a
    ,dextraccio
where
    val_data between date_add(date_add(data_ext,interval - 24 month),interval +1 day) and data_ext
;

insert into ped_variables_bolets
select
    id_cip_sec
    ,'104' agr
    ,0 val
    ,val_data dat
from
    import.social a
    ,dextraccio
where
    val_data<=data_ext
;

insert into ts_variables_bolets
select
    id_cip_sec
    ,'104' agr
    ,0 val
    ,val_data dat
from
    import.social a
    ,dextraccio
where
    val_data<=data_ext
;

drop table if exists ts_valoracio;
create table ts_valoracio as
select
    id_cip_sec
    ,left(val_var, 4) tip
    ,val_data dat
    ,min(if(val_var in ('VSHA22', 'VSHA23', 'VSSC11', 'VSSE07', 'VSSL02'), 1, 0)) obs
from
    import.social a
    ,dextraccio
where
    left(val_var, 4) in ('VSHA', 'VSSC', 'VSSE', 'VSSL')
group by 1, 2, 3
;

###############################################################################################

insert into eqa_variables_bolets
select
    id_cip_sec
    ,549 agr
    ,if(val_val = 'E', 1, 0) val
    ,val_data dat
from 
    import.prealt2 a
    ,dextraccio
where
    val_var = 'TAO3'
    and val_data <= data_ext
    and val_hist = 1
;

###############################################################################################

insert into ass_variables_bolets
select
    id_cip_sec
    ,agrupador agr
    ,0 val
    ,pavi_data_a
from 
    import.assir221 a
    inner join nodrizas.ass_criteris_var_assir221 b on a.pavi_seg_tipus=b.criteri_codi
where
    pavi_data_a between inici and final
;

###############################################################################################

#cui_rel �s per poder excloure pacients amb cuidador assalariat (formaran part del denominador per� quedaran exclosos)
#cui_per_ref t� valors C (cuidador), PC (persona referent i cuidador) o P (persona referent)
#aquests �ltims no entraran al indicador; amb aix� no cal anar a buscar el check-box de sense cuidador a una altra taula, ja que quan el marquen els obliguen a eliminar tots els cuidadors (excepte els cui_per_ref = 'P')
#cui_ref, per si mai hem de utilitzar, marca si �s cuidador principal (S/N)
#despr�s de molts anys sense canvis, el 2019 excloem CR i NCR de cui_tipus_cuidador (https://trello.com/c/SH2FXFa3)
insert into eqa_variables_bolets
select
    id_cip_sec
    ,'103' agr
    ,if(cui_rel='L',1,0) val
    ,data_ext dat
from
    import.cuidador
    ,dextraccio
where
    cui_db = 0
    and cui_per_ref<>'P'
    and cui_tipus_cuidador not in ('CR', 'NCR')
;

insert into ts_variables_bolets
select
    id_cip_sec
    ,'103' agr
    ,if(cui_rel='L',1,0) val
    ,data_ext dat
from
    import.cuidador
    ,dextraccio
where
    cui_db = 0
    and cui_per_ref<>'P'
    and cui_tipus_cuidador not in ('CR', 'NCR')
;
