import sisapUtils as u
import collections as c


DEBUG = False

TABLE = "sisap_cim10mc"
DATABASE = "pdp"


class CIM10MC(object):
    """."""

    def __init__(self):
        """."""
        self.get_migrats()
        self.get_diagnostics()
        self.get_cataleg()
        self.upload_data()

    def get_migrats(self):
        """."""
        sql = "select distinct cim10mc from cat_prstb001cmig"
        self.migrats = set([codi for codi, in u.getAll(sql, "import")])

    def get_diagnostics(self):
        """."""
        self.diagnostics = c.Counter()
        if DEBUG:
            resultat = [self._worker("problemes_s6837")]
        else:
            jobs = u.getSubTables("problemes", ordenat=True)
            resultat = u.multiprocess(self._worker, jobs, 12)
        for worker in resultat:
            for k, v in worker:
                self.diagnostics[k] += v

    def _worker(self, table):
        """."""
        this = c.Counter()
        sql = "select pr_cod_ps from {}".format(table)
        for codi, in u.getAll(sql, "import"):
            if codi[:4] == "C01-" and codi not in self.migrats:
                this[codi] += 1
        return [(k, v) for (k, v) in this.items()]

    def get_cataleg(self):
        """."""
        sql = "select ps_cod, ps_def from cat_prstb001"
        self.cataleg = {cod: des for cod, des in u.getAll(sql, "import")
                        if cod in self.diagnostics}

    def upload_data(self):
        """."""
        upload = [(k, self.cataleg[k], v) for (k, v)
                  in self.diagnostics.items()]
        cols = "(cim10mc varchar2(20), def varchar2(200), n int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(upload, TABLE, DATABASE)
        u.grantSelect(TABLE, ("PREDUPRP", "PREDULMB"), DATABASE)


if __name__ == "__main__":
    CIM10MC()
