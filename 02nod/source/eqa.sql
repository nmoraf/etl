#importo taula de criteris eqa

use nodrizas
;

DROP TABLE IF EXISTS eqa_minimst;
create table eqa_minimst(
agrupador double,
data_min double NULL,
index(agrupador)
)
select
	agrupador
	,max(data_min) as data_min
from
	eqa_relacio
where
    baixa = 0
group by
	agrupador
;


DROP TABLE IF EXISTS eqa_criteris;
create table eqa_criteris(
taula varchar(25) not null default'',
inclusio double,
criteri_codi varchar(20) not null default'',
agrupador_desc varchar(150) not null default'',
agrupador double,
ps_tancat double,
minimt double,
primary key (taula,criteri_codi,agrupador,ps_tancat),
index(criteri_codi),
index(ps_tancat),
index(taula)
)
select
	taula
	,inclusio
	,criteri_codi
	,agrupador_desc
	,agrupador
	,ps_tancat
	,0 as minimt
from
	import_eqa_criteris
;

insert ignore into eqa_criteris select 'pla2' as taula, inclusio, criteri_codi, agrupador_desc, agrupador, ps_tancat, minimt from eqa_criteris where taula = 'activitats';

insert ignore into eqa_criteris select 'activitats' as taula, 1 as inclusio, ac_cod as criteri_codi,'Valoraci� social' as agrupador_desc,'104' as agrupador,0 as ps_tancat,24 as minimt from import.cat_prstb002_social;

insert ignore into eqa_criteris select 'socactivitats' as taula, 1 as inclusio, ac_cod as criteri_codi,'Valoraci� social' as agrupador_desc,'104' as agrupador,0 as ps_tancat,24 as minimt from import.cat_prstb002_social;

insert ignore into eqa_criteris select 'socactivitats' as taula, 1 as inclusio, ac_cod as criteri_codi,'Coordinaci� Altres inst.' as agrupador_desc,'333' as agrupador,0 as ps_tancat,1000 as minimt from import.cat_prstb002_social where left(ac_cod,3)='VSC';

update eqa_criteris a inner join eqa_minimst b on a.agrupador=b.agrupador
set minimt=data_min
;

#Faig updates dels que tenen un proc�s separat (i nom�s surt en aquest proc�s)

update eqa_criteris a
set minimt=1000 where minimt=0
;

update eqa_criteris
set minimt=18 where minimt=1000 and taula = 'cmbdh'
;

drop table if exists import_eqa_criteris;
drop table if exists eqa_minimst;
