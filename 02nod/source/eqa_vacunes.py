from sisapUtils import createTable, getAll, getOne, getSubTables, multiprocess, monthsBetween, listToTable, execute
from collections import defaultdict


imp = 'import'
jail = 'import_jail'
nod = 'nodrizas'
grip_agr = 99
vac_tables = {'A': 'eqa_vacunes', 'N': 'ped_vacunes'}
exc_tables = {'A': 'eqa_exclusions', 'N': 'ped_exclusions'}
imm_tables = {'A': 'eqa_immunitzats', 'N': 'ped_immunitzats'}
prp_tables = {'A': None, 'N': 'ped_propostes'}


def vacunes(params):
    poblacio, table, db, agrupadors, ext = params
    vacunes = {}
    sql = "select id_cip_sec, va_u_cod, date_format(va_u_data_vac, '%Y%m%d'), va_u_data_vac, va_u_dosi from {}, nodrizas.dextraccio \
          where va_u_data_alta <= data_ext and (va_u_data_baixa=0 or va_u_data_baixa > data_ext)".format(table)
    for id, vac, dat_str, dat, dosi in getAll(sql, db):
        try:
            naix = poblacio[id]
        except KeyError:
            continue
        edat = 'A' if naix == 'A' else 'N'
        nen = edat == 'N'
        if vac in agrupadors[edat]:
            if nen:
                if dat < naix:
                    continue
                mesos = monthsBetween(naix, dat)
                fa = monthsBetween(dat, ext)
            for agr, dosis, inici, final in agrupadors[edat][vac]:
                if inici <= dat_str <= final:
                    if (id, agr) not in vacunes:
                        vacunes[(id, agr)] = {'e': edat, 'M': dat_str, 'n': 1, 'd': dosi}
                        if nen:
                            vacunes[(id, agr)]['m'] = dat_str
                            vacunes[(id, agr)]['i'] = dosis
                            vacunes[(id, agr)]['f'] = fa
                            vacunes[(id, agr)]['T'] = mesos
                            vacunes[(id, agr)]['t'] = mesos
                    else:
                        vacunes[(id, agr)]['M'] = max(dat_str, vacunes[(id, agr)]['M'])
                        vacunes[(id, agr)]['n'] += 1
                        vacunes[(id, agr)]['d'] = max(dosi, vacunes[(id, agr)]['d'])
                        if nen:
                            vacunes[(id, agr)]['m'] = min(dat_str, vacunes[(id, agr)]['m'])
                            vacunes[(id, agr)]['i'] = min(dosis, vacunes[(id, agr)]['i'])
                            vacunes[(id, agr)]['f'] = min(fa, vacunes[(id, agr)]['f'])
                            vacunes[(id, agr)]['T'] = max(mesos, vacunes[(id, agr)]['T'])
                            vacunes[(id, agr)]['t'] = min(mesos, vacunes[(id, agr)]['t'])

    upload = {key: [] for key in vac_tables}
    for (id, agr), data in vacunes.iteritems():
        dosi = max(data['n'], data['d'])
        if data['e'] == 'A':
            upload['A'].append([id, agr, data['M'], dosi])
        else:
            upload['N'].append([id, agr, data['M'], data['m'], dosi, data['i'], data['f'], data['T'], data['t']])

    for key in upload:
        listToTable(upload[key], vac_tables[key], nod)


def exclusions(taula, poblacio, agrupadors):
    sql = "select id_cip_sec, ief_cod_v, date_format(ief_data_alta, '%Y%m%d'), ief_exc from {}, nodrizas.dextraccio \
            where ief_inc_exc = 'E' and ief_data_alta <= data_ext and (ief_data_baixa = 0 or ief_data_baixa > data_ext)".format(taula)
    upload = {key: [] for key in exc_tables}
    for id, vac, dat, mot in getAll(sql, imp):
        try:
            edat = 'A' if poblacio[id] == 'A' else 'N'
        except KeyError:
            continue
        if vac in agrupadors:
            for agr in agrupadors[vac]:
                upload[edat].append([id, agr, dat, mot])
    for key in upload:
        listToTable(upload[key], exc_tables[key], nod)


def immunitzats(poblacio):
    sql = "select id_cip_sec, agrupador from immunitzatswithjail a inner join nodrizas.eqa_criteris_inatural b on a.imu_cod_ps = b.criteri_codi, nodrizas.dextraccio \
            where imu_data_alta <= data_ext and imu_data_baixa = 0"
    upload = {key: [] for key in imm_tables}
    for id, agr in getAll(sql, imp):
        try:
            edat = 'A' if poblacio[id] == 'A' else 'N'
        except KeyError:
            continue
        upload[edat].append([id, agr])
    for key in upload:
        listToTable(upload[key], imm_tables[key], nod)


def propostes(poblacio, agrupadors):
    sql = 'select id_cip_sec, vaf_cod, vaf_data_vac from propostes'
    upload = []
    for id, vac, dat in getAll(sql, imp):
        try:
            edat = 'A' if poblacio[id] == 'A' else 'N'
        except KeyError:
            continue
        else:
            if edat == 'N' and vac in agrupadors:
                for agr in agrupadors[vac]:
                    upload.append((id, agr, monthsBetween(dat, ext)))
    listToTable(upload, prp_tables['N'], nod)


if __name__ == '__main__':
    poblacio = {}
    sql = 'select id_cip_sec, edat, data_naix from assignada_tot_with_jail'
    for id, edat, naix in getAll(sql, nod):
        poblacio[id] = 'A' if edat >= 15 else naix
    sql = 'select id_cip_sec from ass_poblacio'
    for id, in getAll(sql, nod):
        poblacio[id] = 'A'
    ext, = getOne('select data_ext from dextraccio', nod)

    createTable(vac_tables['A'], '(id_cip_sec int, agrupador int, datamax date, dosis int)', nod, rm=True)
    createTable(vac_tables['N'], '(id_cip_sec int, agrupador int, datamax date, datamin date, dosis int, dosisind int, temps int, maxedat int, minedat int)', nod, rm=True)
    sql = "select vacuna, agrupador, dosis \
                ,if(agrupador = {0}, concat(if(grip=1, year(data_ext), year(data_ext)-1), '0901'), date_format(inici, '%Y%m%d')) inici \
                ,if(agrupador = {0}, if(grip=1, date_format(data_ext, '%Y%m%d'), concat(year(data_ext) - 1, '1231')), date_format(final, '%Y%m%d')) final \
                from eqa_criteris_vacunes{1} a inner join import.cat_prstb040_new b on a.criteri_codi = b.antigen, dextraccio"
    agrupadors = {}
    for key in ('A', 'N'):
        agrupadors[key] = defaultdict(set)
        q = sql.format(grip_agr, '' if key == 'A' else 'ped')
        for vac, agr, dosis, inici, final in getAll(q, nod):
            agrupadors[key][vac].add((agr, dosis, inici, final))
    jobs = [(poblacio, taula, imp, agrupadors, ext) for taula, n in sorted(getSubTables('vacunes').items(), key=lambda x: x[1], reverse=True) if n > 0]
    jobs.extend([(poblacio, 'vacunes', jail, agrupadors, ext)])
    multiprocess(vacunes, jobs, 8)
    for key in vac_tables:
        execute('alter table {} add index id (id_cip_sec)'.format(vac_tables[key]), nod)

    for key in exc_tables:
        createTable(exc_tables[key], '(id_cip_sec int, agrupador int, dvacuna date, motiu varchar(10))', nod, rm=True)
    sql = "select agrupador, vacuna from eqa_criteris_exclusions a inner join import.cat_prstb040_new b on a.criteri_codi = b.antigen"
    agrupadors = defaultdict(set)
    for agr, vac in getAll(sql, nod):
        agrupadors[vac].add(agr)
    exclusions('exclusionswithjail', poblacio, agrupadors)
    exclusions('exclusions2withjail', poblacio, agrupadors)

    for key in imm_tables:
        createTable(imm_tables[key], '(id_cip_sec int, agrupador int)', nod, rm=True)
    immunitzats(poblacio)

    createTable(prp_tables['N'], '(id_cip_sec int, agrupador int, mesos int)', nod, rm=True)
    sql = "select agrupador, vacuna from eqa_criteris_propostes a inner join import.cat_prstb040_new b on a.criteri_codi = b.antigen"
    agrupadors = defaultdict(set)
    for agr, vac in getAll(sql, nod):
        agrupadors[vac].add(agr)
    propostes(poblacio, agrupadors)
