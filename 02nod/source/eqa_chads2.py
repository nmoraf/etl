from sisapUtils import getAll, createTable, listToTable, getOne, execute


nod = 'nodrizas'
tmp = 'eqa_chads2_tmp'
tb = 'eqa_chads2'


pacients = {id: 0 for id, in getAll('select id_cip_sec from eqa_problemes where ps = 180 and edat > 14', nod)}
createTable(tmp, '(id_cip_sec int, primary key (id_cip_sec))', nod, rm=True)
listToTable([(id,) for id in pacients], tmp, nod)

sql = 'select a.id_cip_sec, edat, sexe from assignada_tot_with_jail a inner join {} b on a.id_cip_sec = b.id_cip_sec'.format(tmp)
for id, edat, sexe in getAll(sql, nod):
    # edat -= 2   # para PADRIS (que se hace en 2019) queremos chads a 2017, resto 2 temporalmente
    pacients[id] += 2 if edat >= 75 else 0
    pacients[id] += 1 if 65 <= edat <= 74 else 0
    pacients[id] += 1 * sexe == 'D'

sql = 'select a.id_cip_sec, ps from eqa_problemes a inner join {} b on a.id_cip_sec = b.id_cip_sec where ps in (7, 55, 18, 194, 11, 181)'.format(tmp)
for id, ps in getAll(sql, nod):
    pacients[id] += 2 if ps == 7 else 1

sql = 'select a.id_cip_sec from eqa_variables a inner join {} b on a.id_cip_sec = b.id_cip_sec where agrupador = 191 and valor < 40 and usar = 1'.format(tmp)
for id, in getAll(sql, nod):
    pacients[id] += 1

dat, = getOne('select date_add(data_ext, interval -1 month) from dextraccio', nod)
createTable(tb, '(id_cip_sec int, agrupador int, valor int, data_var date)', nod, rm=True)
listToTable([(id, 215, chads2, dat) for id, chads2 in pacients.items()], tb, nod)
execute('drop table {}'.format(tmp), nod)
