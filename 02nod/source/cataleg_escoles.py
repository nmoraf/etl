# coding: latin1

"""
Creem el catàleg escoles a partir de dos catalegs:
1) escoles - abs que hem tret a partir dels mapes de la Maria
2) nombre alumnes: que ens passen periòdicament.
Passem per assignada per veure en quins abs estan les línies
"""


import collections as c

import sisapUtils as u
import sisaptools as t
import csv

FILE_IN = "./dades_noesb/sie_escoles_abs.txt"
CURSOS_IN = "./dades_noesb/sie_cursos.csv"
COORD_IN = "./dades_noesb/sie_coordenades.txt"
db = "nodrizas"

def readCSV2(file,sep='@',encoding='UTF-8', header=False):

    with open(file,'rb') as f:
        primera = True
        c= csv.reader(f,delimiter=sep)
        for row in c:
            if header and primera:
                primera = False
            else:
                yield [unicode(cell, encoding).encode('latin-1') for cell in row]


class escoles(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_relacions()
        self.get_cursos()
        self.get_coordenades()
        self.get_cataleg()
        self.upload_data()
        
    def get_centres(self):
        """agafem ABS i mirem quina es la seva UP"""
        self.centres = {}
        self.up_to_br = {}
        sql = "select abs, scs_codi, ics_codi, ics_desc from cat_centres"
        for abs, up, br, desc in u.getAll(sql, 'nodrizas'):
            abs = int(abs)
            self.centres[(abs)] =  up 
            self.up_to_br[(up)] = {'br': br, 'desc':desc}
        
    def get_relacions(self):
        """Mirem les poblacions infantils ups rca"""
        self.relacions = {}
        sql = 'select up, up_rca, count(*) from nodrizas.assignada_tot where edat < 15 group by up, up_rca'
        for up, up_rca, rec in u.getAll(sql, 'nodrizas'):
            if up != up_rca:
                if rec > 500:
                    self.relacions[up_rca] = up
        
    def get_cursos(self):
        """Agafem cursos de excel de manolo"""
        self.cursos = {}
        for escola, municipi, BATX1, BATX2, P3, P4, P5, EP1, EP2, EP3, EP4, EP5, EP6, ESO1, ESO2, ESO3, ESO4 in u.readCSV(CURSOS_IN, sep=';'):
            self.cursos[escola] = municipi, BATX1, BATX2, P3, P4, P5, EP1, EP2, EP3, EP4, EP5, EP6, ESO1, ESO2, ESO3, ESO4
   
    def get_coordenades(self):
        """Agafem coordenades"""
        self.coordenades = {}
        for escola, titulacio, comarca, utmX, utmY, Geox, GeoY in readCSV2(COORD_IN, sep='@'):
            self.coordenades[escola] = titulacio, comarca, utmX, utmY, Geox, GeoY
        
   
    def get_cataleg(self):
        """Llegeixo catàleg i li poso la up a partir de la ABS, passo els abs de les línies a les línies"""
        self.upload = []
        for codi, desc, abs, desc_abs in readCSV2(FILE_IN, sep='@'):
            try:
                abs = int(abs)
            except ValueError:
                abs = None
            up = self.centres[(abs)] if (abs) in self.centres else None
            if up in self.relacions and False:
                up = self.relacions[up]
            up_desc = self.up_to_br[(up)]['desc'] if (up) in self.up_to_br else None
            br = self.up_to_br[(up)]['br'] if (up) in self.up_to_br else None
            try:
                titulacio, comarca, utmX, utmY, Geox, GeoY = self.coordenades[codi][0], self.coordenades[codi][1], self.coordenades[codi][2], self.coordenades[codi][3], self.coordenades[codi][4], self.coordenades[codi][5]
            except KeyError:
                titulacio, comarca, utmX, utmY, Geox, GeoY = None, None, None, None, None, None   
            try:
                self.upload.append([codi, desc, titulacio, utmX, utmY, Geox, GeoY, abs, desc_abs, up, br, up_desc, self.cursos[codi][0], comarca, self.cursos[codi][1],self.cursos[codi][2],self.cursos[codi][3],self.cursos[codi][4],
                   self.cursos[codi][5],self.cursos[codi][6],self.cursos[codi][7],self.cursos[codi][8],self.cursos[codi][9],self.cursos[codi][10],self.cursos[codi][11]
                   ,self.cursos[codi][12],self.cursos[codi][13],self.cursos[codi][14],self.cursos[codi][15]])
            except KeyError:
                self.upload.append([codi, desc, titulacio, utmX, utmY, Geox, GeoY, abs, desc_abs, up, None, None, None, comarca, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None])
       
    def upload_data(self):
        """pugem a taula a nodrizas i redics"""
        table = "cat_escoles"
        columnsU = ["codi_escola varchar(40)", "nom_escola varchar(300)", "titulacio varchar(100)", "Coordenades_UTM_X double", "Coordenades_UTM_Y double",	"Coordenades_GEO_X double", "Coordenades_GEO_Y double",
        "abs varchar(10)", "desc_abs varchar(300)", "up varchar(5)",  "br varchar(5)","up_desc varchar(300)",
        "municipi varchar(150)", "comarca varchar(150)",
        "N_1_BATX int", "N_2_BATX int","N_1_EI int","N_2_EI int","N_3_EI int","N_1_EP int","N_2_EP int","N_3_EP int","N_4_EP int","N_5_EP int","N_6_EP int","N_1_ESO int",
        "N_2_ESO int","N_3_ESO int","N_4_ESO int"]
        u.createTable(table, "({})".format(", ".join(columnsU)), db, rm=True)
        u.listToTable(self.upload, table, db)
        
        table = "sie_escoles"
        columnsU = ["codi_escola varchar2(40)", "nom_escola varchar2(300)", "titulacio varchar2(100)", "Coordenades_UTM_X number", "Coordenades_UTM_Y number",	"Coordenades_GEO_X number", "Coordenades_GEO_Y number",
        "abs varchar2(10)", "desc_abs varchar2(300)", "up varchar2(5)", "br varchar2(5)", "up_desc varchar2(300)","municipi varchar2(150)","comarca varchar2(150)",
        "N_1_BATX number", "N_2_BATX number","N_1_EI number","N_2_EI number","N_3_EI number","N_1_EP number","N_2_EP number","N_3_EP number","N_4_EP number","N_5_EP number","N_6_EP number","N_1_ESO number",
        "N_2_ESO number","N_3_ESO number","N_4_ESO number"]
        u.createTable(table, "({})".format(", ".join(columnsU)), 'redics', rm=True)
        u.listToTable(self.upload, table, 'redics')
        u.grantSelect(table,('PREDUMMP','PREDUPRP','PREDUECR','PREDULMB', 'PDP'),'redics')
           
if __name__ == "__main__":
    escoles()