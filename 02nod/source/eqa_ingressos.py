from sisapUtils import getAll, createTable, listToTable


conditions = "procedencia <> '2' and calta not in ('2', '6') and datediff(dalta, dingres) > 1 and posicio < 3"  # http://10.80.217.68/wikindicadors/Alta_hospital%C3%A0ria
db = 'nodrizas'
tb_dx = 'eqa_ingressos'
tb_px = 'eqa_procediments'


data = []
sql = 'select a.id_cip_sec, a.id_ingres, a.up, a.dnaix, a.sexe, b.agrupador, a.dingres, a.posicio, b.ps_tancat from nod_cmbdh_dx a inner join eqa_criteris_cmbdh b on a.codi = b.criteri_codi \
       where a.dingres between b.inici and b.final and {}'.format(conditions)
for id, ing, up, naix, sexe, agr, dat, pos, tip in getAll(sql, db):
    if tip == 0 or tip == pos:
        data.append([id, ing, up, naix, 'H' if sexe == '0' else 'D', agr, dat])

createTable(tb_dx, '(id_cip_sec int, ingres int, up varchar(5), naix date, sexe varchar(1), agr int, dat date)', db, rm=True)
listToTable(data, tb_dx, db)


data = []
sql = 'select a.id_cip_sec, a.id_ingres, b.agrupador from nod_cmbdh_px a inner join eqa_criteris_cmbdhpx b on a.codi = b.criteri_codi'
for id, ing, agr in getAll(sql, db):
    data.append([id, ing, agr])

createTable(tb_px, '(id_cip_sec int, ingres int, agr int)', db, rm=True)
listToTable(data, tb_px, db)
