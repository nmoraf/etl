# -*- coding: latin1 -*-

"""
Proc�s per eliminar duplicats a CMBDH introdu�ts durant la desc�rrega per
la conversi� de HASH a ID.
El DISTINCT colapsa registres amb igual ID_CIP per� diferent ID_CIP_SEC,
mentre que el GROUP BY elimina els que tenen diferent ID_CIP per un
mateix NUMERO_ID (mateix HASH en origen).
"""

import sisapUtils as u


ORIGEN = ("cmbdh2", "import")
DESTI = ("nod_cmbdh2", "nodrizas")


cols = [col for col in u.getTableColumns(*ORIGEN)
        if col not in ("id_cip_sec", "codi_sector")]
sql = "as select * \
          from (select distinct {} from {}.{}) a \
          group by numero_id \
          having count(1) = 1".format(", ".join(cols), ORIGEN[1], ORIGEN[0])
u.createTable(DESTI[0], sql, DESTI[1], rm=True)
