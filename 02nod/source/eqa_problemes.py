from sisapUtils import getAll, monthsBetween, multiprocess, createTable, getSubTables, printTime, execute, listToTable, yearsBetween, getOne
from collections import defaultdict

imp = 'import'
jail = 'import_jail'
nod = 'nodrizas'

thesaurus = {5780: 'E78.0a', 5781: 'E78.0a', 2485: 'N39.0a', 1241: 'I48a',
             1242: 'I48a', 5141: 'R79.8a', 5142: 'R79.8a', 1022: 'K91.1a',
             369: 'E03.9a', 5356: 'R94.2a', 5619: 'R76.8a', 605: 'R74.8a',
             616: 'T78.3a', 11358: 'C01-I67.89a', 15556: 'C01-I67.89a',
             11136: 'C01-H54.7a', 11055: 'C01-H54.7a', 11054: 'C01-H54.7a'}
last_date = set((39, 597))

def do_it(params):
    taula, db, poblacio, rip, agrupadors, acut_up, recurrencia, recurrencia_inv, _dext = params
    sqls = ["select id_cip_sec, pr_cod_ps, pr_th, date_format(pr_dde,'%Y%m%d'), pr_dde, date_format(pr_dba, '%Y%m%d'), pr_gra, pr_up, pr_usu \
             ,if(pr_dde > date_add(data_ext, interval -1 year), 1, 0) \
             ,if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) \
             from {}, nodrizas.dextraccio \
             where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext)".format(taula)]
    sqls.append("select id_cip_sec, eli_codi_element, 0, date_format(eli_dia_inici,'%Y%m%d'), eli_dia_inici, date_format(eli_dia_fi, '%Y%m%d'), 0, '99999', 'OPSOPSOPS' \
                 ,if(eli_dia_inici > date_add(data_ext, interval -1 year), 1, 0) \
                 ,if(eli_dia_fi > 0 and eli_dia_fi <= data_ext, 1, 0) \
                 from {}, nodrizas.dextraccio \
                 where eli_dia_inici <= data_ext".format(taula.replace("problemes", "atic")))

    problemes = {}
    numcol = {usu: col for (usu, col) in getAll("select ide_usuari, ide_numcol from cat_pritb992 where codi_sector = '{}' and ide_numcol <> ''".format(taula[-4:]), imp)}
    recurrents = defaultdict(list)
    upload = {'eqa_problemes': [], 'eqa_problemes_incid': [], 'ped_problemes': [], 'ped_problemes_incid': [], 'urg_problemes': [], 'rip_problemes': []}

    for sql in sqls:
        for id, ps, th, dde, _dde, dba, gra, up, usu, incid, tancat in getAll(sql, db):
            try:
                pob = poblacio[id]
            except KeyError:
                if id in rip:
                    if (ps, tancat) in agrupadors:
                        for agr in agrupadors[(ps, tancat)]:
                            upload['rip_problemes'].append([id, agr, dde])
                continue
            try:
                edat, _naix, naix = poblacio[id]
                nen = True
            except TypeError:
                edat = pob
                nen = False
            if nen:
                mesos = monthsBetween(_naix, _dde)
            for cod in (th, th - 10000):
                if cod in thesaurus:
                    ps = thesaurus[cod]
            if (ps, tancat) in agrupadors:
                for agr in agrupadors[(ps, tancat)]:
                    if (id, agr) not in problemes:
                        problemes[(id, agr)] = {'d': dde, 'g': gra, 'i': incid, 'n': 1, 'ni': incid, 'e': edat}
                        if nen:
                            problemes[(id, agr)]['x'] = naix
                            problemes[(id, agr)]['m'] = mesos
                    else:
                        problemes[(id, agr)]['d'] = max(problemes[(id, agr)]['d'], dde) if agr in last_date else min(problemes[(id, agr)]['d'], dde)
                        problemes[(id, agr)]['g'] = max(problemes[(id, agr)]['g'], gra)
                        problemes[(id, agr)]['i'] = max(problemes[(id, agr)]['i'], incid) if agr in last_date else min(problemes[(id, agr)]['i'], incid)
                        problemes[(id, agr)]['n'] += 1
                        problemes[(id, agr)]['ni'] += incid
                        if nen:
                            problemes[(id, agr)]['m'] = max(problemes[(id, agr)]['m'], mesos)
                    upload['eqa_problemes_incid'].append([id, agr, dde, gra])
                    if agr in recurrencia and yearsBetween(_dde, _dext) < 2:
                        recurrents[id].append([agr, _dde, dde])
                    if nen:
                        upload['ped_problemes_incid'].append([id, agr, dde, gra, naix, mesos])
                    if up in acut_up:
                        col = numcol[usu] if usu in numcol else ''
                        upload['urg_problemes'].append([id, agr, dde, gra, up, usu, col])

    for (id, agr), data in problemes.items():
        upload['eqa_problemes'].append([id, agr, data['d'], data['g'], data['i'], data['n'], data['ni'], data['e']])
        try:
            upload['ped_problemes'].append([id, agr, data['d'], data['g'], data['x'], data['m']])
        except KeyError:
            pass

    for id, rows in recurrents.items():
        fet = defaultdict(list)
        for agr, _dde, dde in sorted(rows, key=lambda x: x[1]):
            homol = recurrencia_inv[agr]
            if yearsBetween(_dde, _dext) == 0:
                previs = sum([yearsBetween(_dat, _dde) == 0 for _dat in fet[homol]])
                nou = recurrencia[agr][previs + 1 >= recurrencia[agr]['n']]
                upload['eqa_problemes_incid'].append([id, nou, dde, previs+1])
            fet[homol].append(_dde)

    for table, data in upload.items():
        listToTable(data, table, nod)


if __name__ == '__main__':
    createTable('eqa_problemes', '(id_cip_sec int, ps int, dde date, pr_gra int, incident int, n int, ninc int, edat int)', nod, rm=True)
    createTable('eqa_problemes_incid', '(id_cip_sec int, ps int, dde date, pr_gra int)', nod, rm=True)
    createTable('ped_problemes', '(id_cip_sec int, ps int, dde date, pr_gra int, data_naix date, edat_m int)', nod, rm=True)
    createTable('ped_problemes_incid', '(id_cip_sec int, ps int, dde date, pr_gra int, data_naix date, edat_m int)', nod, rm=True)
    createTable('urg_problemes', '(id_cip_sec int, ps int, dde date, pr_gra int, up varchar(5), usu varchar(20), numcol varchar(9))', nod, rm=True)
    createTable('rip_problemes', '(id_cip_sec int, ps int, dde date)', nod, rm=True)

    printTime('inici poblacio')
    poblacio = defaultdict(dict)
    sql = "select id_cip_sec, codi_sector, edat, data_naix, date_format(data_naix,'%Y%m%d') from assignada_tot_with_jail"
    for id, sector, edat, _naix, naix in getAll(sql, nod):
        poblacio[sector][id] = edat if edat >= 15 else (edat, _naix, naix)
    sql = "select id_cip_sec, codi_sector, edat from ass_poblacio"
    for id, sector, edat in getAll(sql, nod):
        poblacio[sector][id] = edat
    rip = set([id for id, in getAll('select id_cip_sec from rip_assignada', nod)])
    printTime('fi poblacio')

    agrupadors = defaultdict(list)
    sql = "select criteri_codi, agrupador, ps_tancat from eqa_criteris_problemes"
    for ps, agr, tancat in getAll(sql, nod):
        agrupadors[(ps, tancat)].append(int(agr))

    acut_up = set()
    sql = 'select scs_codi from urg_centres'
    for up, in getAll(sql, nod):
        acut_up.add(up)

    recurrencia = defaultdict(dict)
    for pre, post, n, tipus in getAll('select criteri_codi, agrupador, inclusio, ps_tancat from eqa_criteris_recurrencia', nod):
        recurrencia[int(pre)]['n'] = int(n)
        recurrencia[int(pre)][tipus == 1] = int(post)
    recurrencia_temp = defaultdict(list)
    for cod, desc in getAll('select distinct agrupador, agrupador_desc from eqa_criteris_problemes where agrupador in {}'.format(tuple(recurrencia)), nod):
        recurrencia_temp[desc].append(int(cod))
    i = 0
    recurrencia_inv = {}
    for desc, cods in recurrencia_temp.items():
        i += 1
        for cod in cods:
            recurrencia_inv[cod] = i
    dext, = getOne('select data_ext from dextraccio', nod)

    jobs = [(taula, imp, poblacio[taula[-4:]], rip, agrupadors, acut_up, recurrencia, recurrencia_inv, dext) for taula, n in sorted(getSubTables('problemes').items(), key=lambda x: x[1], reverse=True) if n > 0]
    jobs.extend([('problemes', jail, poblacio['6951'], set(), agrupadors, acut_up, recurrencia, recurrencia_inv, dext)])

    printTime('inici ps')
    multiprocess(do_it, jobs, 8)
    printTime('fi ps')

    printTime('inici indexing')
    execute('alter table eqa_problemes add index pk (id_cip_sec, ps), add index id (id_cip_sec), add index ps (ps)', nod)
    execute('alter table eqa_problemes_incid add index id (id_cip_sec), add index ps (ps)', nod)
    printTime('inici indexing')
