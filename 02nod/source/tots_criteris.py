from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

pathodn="../06odn/dades_noesb/odn_relacio.txt"
pathpedd="../04ped/dades_noesb/ped_den.txt"
pathpedn="../04ped/dades_noesb/ped_num.txt"
pathpede="../04ped/dades_noesb/ped_excl.txt"
pathjail="../16jail/dades_noesb/jail_relacio.txt"
pathjailcri="../16jail/dades_noesb/jail_criteris.txt"

desti="mst_relacio"
rel="nodrizas.eqa_relacio"
criteris="nodrizas.eqa_criteris"
criterisped="nodrizas.ped_criteris_var"

t= {'d':'den','n':'num','e':'excl','num_e':'num','excl':'excl'}  

db="nodrizas"
conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()

c.execute("drop table if exists %s" % desti)
c.execute("create table %s(ind_codi VARCHAR(10) NOT NULL DEFAULT'',taula varchar(15) not null default'',tipus varchar(5) not null default'',numero_criteri double,agrupador double,\
    data_min double NULL,data_max double NULL,baixa double,data_baixa VARCHAR(10) NOT NULL DEFAULT'',valor_min varchar(100),valor_max varchar(100),var_ult double NULL,var_num varchar(100),\
    sense double NULL,index (ind_codi,agrupador),index(agrupador),index(tipus,baixa),index(tipus,ind_codi,taula,baixa),index(tipus,ind_codi,taula,baixa,var_num)); " %(desti))
c.execute("insert into %s select * from %s" % (desti,rel))

with open(pathpedd, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if agr=="z":
         ok=1
      else:
         c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            taula= i[0]
            c.execute("insert ignore into %s Values('%s','%s','den' ,'%s' ,'%s' ,'%s' ,'%s' ,0 , 0 , '%s' , '%s' ,'' , '' , '' )" % (desti,ind,taula,f,agr,tmin,tmax,vmin,vmax))

with open(pathpedn, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,n,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8]
      if agr=="z":
         ok=1
      else:
         c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            taula= i[0]
            c.execute("insert ignore into %s Values('%s','%s','num' ,'%s' ,'%s' ,'%s' ,'%s' ,0 , 0 , '%s' , '%s' ,'' , '%s' , '' )" % (desti,ind,taula,f,agr,tmin,tmax,vmin,vmax,n))
            

with open(pathpede, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f= ind[0],ind[1],ind[2],ind[3]
      if agr=="z":
         ok=1
      else:
         c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            taula= i[0]
            c.execute("insert ignore into %s Values('%s','%s','excl' ,'%s' ,'%s' ,'' ,'' ,0 , 0 , '' , '' ,'' , '' , '' )" % (desti,ind,taula,f,agr))


with open(pathodn, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tipus,agr,f,vmin,vmax,tmin,tmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if agr=="z":
         ok=1
      else:
         c.execute("select distinct taula from %s where agrupador='%s'" % (criteris,agr))
         pres=c.fetchall()
         for i in pres:
            taula= i[0]
            dne=t[tipus]
            c.execute("insert ignore into %s Values('%s','%s','%s' ,'%s' ,'%s' ,'%s' ,'%s' ,0 , 0 , '%s' , '%s' ,'' , %s , '' )" % (desti,ind,taula,dne,f,agr,tmin,tmax,vmin,vmax,n))
  
jail_crit = 'jail_criteris'
create = "(taula varchar(15) not null default'',inclusio double,criteri_codi varchar(20) not null default'',agrupador_desc varchar(150) not null default'',agrupador double,ps_tancat double)"
createTable(jail_crit,create,db, rm=True)
upload = []
with open(pathjailcri, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
        upload.append([ind[0], ind[1], ind[2],ind[3], ind[4], ind[5]])
listToTable(upload, jail_crit, db)

def get_crit_jail():
    jailc = {}
    sql = 'select taula, agrupador from {}'.format(jail_crit)
    for taula, agr in getAll(sql, db):
        jailc[(agr)] = {'taula':taula}
    return jailc

jailc = get_crit_jail()

with open(pathjail, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tipus,f,agr,dmin, dmax, d,dbaixa,vmin,vmax,vult,varnum,sense = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8],ind[9],ind[10],ind[11], ind[12]
      try:
        agr = int(agr)
      except ValueError:
        continue
      try:
        taula = jailc[(agr)]['taula']
      except KeyError:
        taula = 'sense'
      c.execute("insert ignore into %s Values('%s','%s','%s' ,'%s' ,'%s' ,'%s' ,'%s' ,0, 0, '%s' , '%s' ,'%s' , %s , '%s' )" % (desti,ind,taula,tipus,f,agr,dmin,dmax,vmin,vmax,vult,varnum,sense))            
 
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")