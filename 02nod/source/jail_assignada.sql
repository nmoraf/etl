use nodrizas;

drop table if exists jail_assignada;
create table jail_assignada like assignada_tot;
insert ignore into jail_assignada
select
    id_cip_sec
    ,id_cip
    ,codi_sector
    ,usua_uab_ep ep
    ,usua_uab_up up
    ,usua_uab_codi_uab uba
    ,usua_abs_codi_abs
    ,ass_codi_up upinf
    ,ass_codi_unitat ubainf
    ,usua_uab_servei_centre centre_codi
    ,usua_uab_servei_centre_classe centre_classe
    ,usua_up_rca
    ,usua_data_naixement data_naix
    ,(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) edat
    ,usua_sexe sexe
    ,usua_nacionalitat nacionalitat
    ,cart_tipus nivell_cobertura
    ,usua_relacio relacio
    ,usua_data_peticio_lms
    ,usua_data_connexio_lms
    ,'' upOrigen
    ,'' upABS
    ,'' seccio_censal
    ,'' last_visit
    ,0 ates
    ,0 n_year
    ,'' last_odn
    ,'' up_residencia
    ,0 institucionalitzat
    ,0 institucionalitzat_ps
    ,0 trasplantat
    ,0 maca
    ,0 pcc
from
    import_jail.assignada a
    ,nodrizas.dextraccio
where
    usua_data_naixement<=data_ext
    and usua_situacio='A'
    and exists (select '1' from nodrizas.jail_centres b where a.usua_uab_up=b.scs_codi)
    and concat(usua_uab_up, usua_uab_codi_uab) <> '07674M15'
;

update jail_assignada a inner join jail_atesa b on a.id_cip_sec = b.id_cip_sec and a.up = b.up
set ates = 1, last_visit = ingres
;

drop table if exists assignada_tot_with_jail;
create table assignada_tot_with_jail like assignada_tot;
alter table assignada_tot_with_jail ENGINE=MERGE;
alter table assignada_tot_with_jail UNION=(assignada_tot,jail_assignada);
