from sisapUtils import getAll, getOne, multiprocess, createTable, listToTable, monthsBetween, execute


tb = 'ped_visites'
nod = 'nodrizas'
imp = 'import'


def get_data(param):
    table, pacients = param
    sql = "select id_cip_sec, visi_data_visita from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_situacio_visita = 'R'".format(table)
    resul = [(id, monthsBetween(pacients[id], dat)) for (id, dat) in getAll(sql, imp) if id in pacients]
    listToTable(resul, tb, nod)


if __name__ == '__main__':
    pacients = {id: naix for (id, naix) in getAll('select id_cip_sec, data_naix from ped_assignada', nod)}
    tables = getOne('show create table visites2', imp)[1].split('UNION=(')[1][:-1].split(',')
    jobs = [(table, pacients) for table in tables]
    createTable(tb, '(id_cip_sec int, edat_m int)', nod, rm=True)
    multiprocess(get_data, jobs, 12)
    execute('alter table {} add index (id_cip_sec, edat_m)'.format(tb), nod)
