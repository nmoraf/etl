use nodrizas;

drop table if exists eqa_u11;
create table eqa_u11 as select * from import.u11;

drop table if exists jail_u11;
create table jail_u11 as select * from import_jail.u11;

drop table if exists eqa_u11_with_jail;
create table eqa_u11_with_jail like eqa_u11;
alter table eqa_u11_with_jail ENGINE=MERGE;
alter table eqa_u11_with_jail UNION=(eqa_u11,jail_u11);
