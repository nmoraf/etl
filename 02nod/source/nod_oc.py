# coding: utf8

from sisapUtils import createTable, getAll, sectors, multiprocess, listToTable, execute, TrelloCard
from collections import defaultdict
import unicodedata as ud

cardmembers = ['booleo', 'ramospaqui1']
derivacions_in = ('AD00001', 'AD00002')
proves_in = ('RA', 'PD', 'IQ', 'MN', 'AP')
inici = '20111101'
imp = 'import'
jail = 'import_jail'
nod = 'nodrizas'
tables = {
        'proves': {
            'nom': 'nod_proves',
            'cols': '(id_cip_sec int, id_cip int, codi_sector varchar(4), oc_data date, oc_servei_ori varchar(5), homol_origen varchar(8), inf_codi_prova varchar(10), inf_radiologia varchar(1), oc_up_ori varchar(5))',
            'idx': 'id_cip_sec, oc_data, inf_codi_prova',
            },
        'deriv': {
            'nom': 'nod_derivacions',
            'cols': '(id_cip_sec int, id_cip int, codi_sector varchar(4), oc_data date, oc_servei_ori varchar(5), homol_origen varchar(8), inf_servei_d_codi varchar(5), homol_desti varchar(8), motiu varchar(500), \
            ciap2 varchar(8), oc_up_ori varchar(5), der_up_des varchar(5), inf_prioritat varchar(1), oc_usuari_alta varchar(20), oc_collegiat varchar(10))',
            'idx': 'id_cip_sec, oc_data, inf_servei_d_codi',
            },
        'altres': {
            'nom': 'nod_oc_altres',
            'cols': '(id_cip_sec int, id_cip int, codi_sector varchar(4), oc_data date, oc_servei_ori varchar(5), homol_origen varchar(8), inf_codi_prova varchar(10), inf_radiologia varchar(1), oc_up_ori varchar(5), inf_servei_d_codi varchar(5))',
            'idx': 'id_cip_sec, oc_data, inf_codi_prova',
            },
        }


def dd():
    return defaultdict(int)

def do_it(params):
    sector, db, catalegs = params

    sexe = {}
    tb = 'assignada' if db == jail else 'assignada_s{}'.format(sector)
    query = "select id_cip_sec, usua_sexe from {} where usua_sexe in ('H', 'D')".format(tb)
    for id, sex in getAll(query, db):
        sexe[id] = sex

    info = {}
    tb = 'oc1' if db == jail else 'oc1_s{}'.format(sector)
    query = "select oc_numid, date_format(oc_data, '%Y%m%d'), oc_servei_ori, oc_up_ori, oc_usuari_alta, oc_collegiat from {}, {}.dextraccio \
            where oc_data between {} and data_ext and (oc_data_baixa = 0 or oc_data_baixa > data_ext)".format(tb, nod, inici)
    for key, dat, serv, up, usu, numcol in getAll(query, db):
        info[key] = {'dat': dat,  'serv': serv, 'up': up, 'usu': usu, 'numcol': numcol, 'ps': []}

    tb = 'oc3' if db == jail else 'oc3_s{}'.format(sector)
    query = "select oc_numid, ppp_codi_problema from {} where ppp_codi_problema is not null".format(tb)
    for key, value in getAll(query, db):
        try:
            info[key]['ps'].append(value)
        except KeyError:
            pass

    dest = {}
    tb = 'oc4' if db == jail else 'oc4_s{}'.format(sector)
    query = "select gpi_rel_peticio, der_up_des from {} where gpi_rel_peticio <> '' and der_data_baix = 0".format(tb)
    for key, up in getAll(query, db):
        dest[key] = up

    data = {key: [] for key in tables}
    new = defaultdict(dd)
    tb = 'oc2' if db == jail else 'oc2_s{}'.format(sector)
    query = "select id_cip_sec, id_cip, inf_numid, inf_codi_prova, inf_servei_d_codi, inf_radiologia, inf_prioritat, inf_relacio_deriv from {}, {}.dextraccio \
            where (inf_data_baixa = 0 or inf_data_baixa > data_ext)".format(tb, nod)
    for id, id_sidiap, key, prova, desti, radiologia, prioritat, relacio in getAll(query, db):
        if key in info and id in sexe:
            sex = sexe[id]
            homol_origen = ''
            if (sector, info[key]['serv']) in catalegs['origen']:
                homol_origen = catalegs['origen'][(sector, info[key]['serv'])]
                if homol_origen == '':
                    continue
            else:
                homol_origen = 'ONOCAT'
                new['origen'][(sector, info[key]['serv'])] += 1
            if prova in derivacions_in and desti != '':
                homol_desti, ciap, up_desti = '', '', ''
                if desti in catalegs['desti']:
                    homol_desti = catalegs['desti'][desti]
                    if homol_desti == '':
                        continue
                else:
                    homol_desti = 'DNOCAT'
                    new['desti'][desti] += 1
                if len(info[key]['ps']) == 0:
                    ciap = 'NOCIAP'
                elif len(info[key]['ps']) == 1:
                    cim10, = info[key]['ps']
                    if (cim10, sex) in catalegs['ciap2']:
                        try:
                            ciap, = catalegs['ciap2'][(cim10, sex)]
                        except ValueError:
                            ciap = 'MULTCIAP'
                    else:
                        ciap = 'NOCIAP'
                        new['ciap2'][(cim10, sex)] += 1
                else:
                    ciap = 'MULTCIAP'
                if relacio in dest:
                    up_desti = dest[relacio]
                data['deriv'].append([id, id_sidiap, sector, info[key]['dat'], info[key]['serv'], homol_origen, desti, homol_desti, '|'.join(info[key]['ps']), ciap, info[key]['up'], up_desti, prioritat, info[key]['usu'], info[key]['numcol']])
            elif prova[:2] in proves_in:
                data['proves'].append([id, id_sidiap, sector, info[key]['dat'], info[key]['serv'], homol_origen, prova, radiologia, info[key]['up']])
            else:
                data['altres'].append([id, id_sidiap, sector, info[key]['dat'], info[key]['serv'], homol_origen, prova, radiologia, info[key]['up'], desti])

    for key, val in tables.items():
        listToTable(data[key], val['nom'], nod)

    return new


if __name__ == '__main__':
    for key, val in tables.items():
        createTable(val['nom'], val['cols'], nod, rm=True)

    catalegs = {'origen': {}, 'desti': {}, 'ciap2': defaultdict(list)}
    query = "select sector, servei, codi_khalix from cat_ct_der_serveio"
    for sector, servei, khalix in getAll(query, imp):
        catalegs['origen'][(sector, servei)] = khalix
    query = "select servei, codi_khalix from cat_ct_der_serveid"
    for servei, khalix in getAll(query, imp):
        catalegs['desti'][servei] = khalix
    query = "select codi_cim10, if(sexe='', 'B', sexe), codi_ciap_m from cat_md_ct_cim10_ciap"
    for cim10, sexe, ciap in getAll(query, imp):
        if sexe == 'B':
            catalegs['ciap2'][(cim10, 'H')].append(ciap)
            catalegs['ciap2'][(cim10, 'D')].append(ciap)
        else:
            catalegs['ciap2'][(cim10, sexe)].append(ciap)

    jobs = [(sector,  imp, catalegs) for sector in sectors]
    jobs.append(['6951', jail, catalegs])
    new = multiprocess(do_it,  jobs,  8)

    for key, val in tables.items():
        execute("alter table {} add index ({})".format(val['nom'], val['idx']), nod)

    send_codes = defaultdict(dd)
    for proc in new:
        for k, d in proc.items():
            for c, n in d.items():
                send_codes[k][c] += n

    descripcions = {}
    descripcions['origen'] = {(sec, cod): ud.normalize('NFKD', des.decode('latin1')).encode('ascii', 'ignore') for (sec, cod, des) in getAll('select codi_sector, s_codi_servei, s_descripcio from cat_pritb103', 'import')}
    descripcions['desti'] = {cod: ud.normalize('NFKD', des.decode('latin1')).encode('ascii', 'ignore') for (cod, des) in getAll("select rv_low_value, max(rv_meaning) from cat_pritb000 where rv_table = 'GPITB004' and rv_column = 'INF_SERVEI_D_CODI' group by rv_low_value", 'import')}
    descripcions['cim10'] = {cod: ud.normalize('NFKD', des.decode('latin1')).encode('ascii', 'ignore') for (cod, des) in getAll('select ps_cod, ps_def_norm from cat_prstb001', 'import')}

    nf = 'NO TROBAT'
    minim = 49
    algo = False
    if len(send_codes) > 0:
        card = TrelloCard('definició', 'CATALEGS', 'Nous codis OC')
        # card.add_description("Codis pendent d'homologar")
        for cod, n in send_codes['origen'].items():
            if n > minim:
                des = descripcions['origen'][cod] if cod in descripcions['origen'] else nf
                card.add_to_checklist('Servei origen', ', '.join(map(str, (cod[0], cod[1], des))))
                algo = True
        for cod, n in send_codes['desti'].items():
            if n > minim:
                des = descripcions['desti'][cod] if cod in descripcions['desti'] else nf
                card.add_to_checklist('Servei desti', ', '.join(map(str, (cod, des))))
                algo = True
        for cod, n in send_codes['ciap2'].items():
            if n > minim:
                des = descripcions['cim10'][cod[0]] if cod[0] in descripcions['cim10'] else nf
                card.add_to_checklist('Codi cim10', ', '.join(map(str, (cod[0], cod[1], des))))
                algo = True
        if algo:
            card.add_members(*cardmembers)
            card.add_comment('nous codis')
