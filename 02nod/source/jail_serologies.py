from sisapUtils import getAll, createTable, listToTable, writeCSV, tempFolder, sendSISAP, multiprocess, getSubTables
from collections import Counter
from os import remove


table = 'jail_serologies'
table_tot = 'nod_serologies'
table_sidiap = 'sidiap_serologies'
db = 'nodrizas'
descartar = 'eliminar'
proves = {codi: [c.lower() for c in agrupador.split('_')[1:3]] for (codi, agrupador) in getAll("select codi, agrupador from cat_dbscat where taula = 'serologies'", 'import')}
file = tempFolder + 'homologacio_serologies.csv'


def homologar(tipus, val, ref, uni):
    if tipus == 'serologia':
        homologat = homologar_serologia(val, ref)
    elif tipus == 'carrega':
        homologat = homologar_carrega(val, uni)
    else:
        homologat = homologar_valor(val)
    return homologat


def homologar_serologia(val, ref):
    val = val.replace(',', '.')
    ref = ref.replace(',', '.')
    hom = 9
    try:
        val = float(val)
    except ValueError:
        if ('NEG' in val.upper() and 'CONEGUT' not in val.upper() and 'NO PROCEDEIX' not in val.upper() and 'POSIT' not in val.upper()) or '<' in val.upper() or 'NO ES DETECTA' in val.upper() or 'NO SE DETECTA' in val.upper() or 'NAGATI' in val.upper() or 'NO REACTI' in val.upper():
            hom = 0
        elif ('POS' in val.upper() or '>' in val.upper() or 'CONFIRMADA' in val.upper()) and ('PENDENT' not in val.upper() and 'NO ' not in val.upper() and 'BAIX' not in val.upper() and 'FEBLE' not in val.upper() and 'DUBT' not in val.upper() and 'BIL' not in val.upper() and 'NEGAT' not in val.upper()):
            hom = 1
    else:
        try:
            ref = float(ref)
        except (ValueError, TypeError):
            pass
        else:
            hom = 1 if val > ref else 0
    return hom


def homologar_carrega(val, uni):
    if '10' in uni.upper():
        val = val + uni
    val = val.replace(',', '.').replace('"', '').replace('10^', 'x10E').replace('x10E', 'E').replace(' E', 'E').replace('UI/mL', '').replace('UI/ml', '').replace("> ", "").replace(">", "")
    hom = descartar
    try:
        val = int(float(val.split(' ')[0]))
    except ValueError:
        if len(val) < 70:
            if 'POSIT' in val.upper() or 'SE DETECTA GENOMA DE' in val.upper() or 'NO PROCEDE' in val.upper():
                hom = 10000
            elif 'NEG' in val.upper() or 'INDETEC' in val.upper() or ('<' in val.upper() and 'DETECTABLE' not in val.upper()) or 'INFERIOR' in val.upper() or ('DET' in val.upper() and 'NO' in val.upper() and 'NOVA' not in val.upper()) or ('REACTI' in val.upper() and 'NO' in val.upper()):
                hom = 0
    else:
        if val < 15:
            hom = 0
        else:
            hom = val
    return hom


def homologar_valor(val):
    val = val.replace(',', '.')
    try:
        hom = float(val.split(' ')[0])
    except ValueError:
        hom = descartar
    return hom


def get_dades(taula):
    jail = taula == 'laboratori'
    validacio = Counter()
    upload = []
    upload_tot = []
    upload_sidiap = set()
    sql = "select id_cip_sec, id_cip, cr_codi_lab, date_format(cr_data_reg, '%Y%m%d'), cr_codi_prova_ics, cr_res_lab, cr_ref_max_lab, cr_unitats_lab from {} where cr_codi_prova_ics in {}".format(taula, tuple(proves))
    for id, id_s, lab, dat, prova, resultat, referencia, unitat in getAll(sql, 'import_jail' if jail else 'import'):
        homologat = homologar(proves[prova][1], resultat, referencia, unitat)
        validacio[(lab, prova, resultat, referencia, homologat)] += 1
        if homologat != descartar:
            upload_tot.append([id, dat, lab, prova, homologat])
            if jail:
                upload.append([id, dat, lab, prova, homologat])
            upload_sidiap.add((id_s, dat, prova, homologat))
    listToTable(upload_tot, table_tot, db)
    if jail:
        listToTable(upload, table, db)
    listToTable(upload_sidiap, table_sidiap, db)
    return [(key, n) for key, n in validacio.items()]


if __name__ == '__main__':
    createTable(table, '(id_cip_sec int, dat varchar(8), lab varchar(5), cod varchar(6), val double)', db, rm=True)
    createTable(table_tot, '(id_cip_sec int, dat varchar(8), lab varchar(5), cod varchar(6), val double)', db, rm=True)
    createTable(table_sidiap, '(id_cip int, dat varchar(8), cod varchar(6), val double)', db, rm=True)
    jobs = sorted(getSubTables('laboratori'), key=getSubTables('laboratori').get, reverse=True) + ['laboratori']
    resul = multiprocess(get_dades, jobs, 12)
    validacio = Counter()
    for job in resul:
        for key, n in job:
            validacio[key] += n
    writeCSV(file, sorted([[laboratori] + proves[prova] + [n, resultat, referencia, homologat] for (laboratori, prova, resultat, referencia, homologat), n in validacio.items()], reverse=True), sep=';')
    # sendSISAP(['mfabregase@gencat.cat','cguiriguet.bnm.ics@gencat.cat'], 'Valors serologies', 'Carol', 'Veure arxiu adjunt.', file=file)
    remove(file)
