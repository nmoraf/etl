use nodrizas
;

drop table if exists lab_anual;
create table lab_anual as
select
    id_cip
    ,id_cip_sec
    ,cr_codi_lab
    ,cr_codi_prova_ics
    ,cr_data_reg
    ,codi_up
from
    import.laboratori1
    ,dextraccio
where
    cr_data_reg between date_add(date_add(data_ext,interval - 1 year),interval +1 day) and data_ext
;
