use nodrizas
;

drop table if exists embaras2;
CREATE TABLE embaras2 (
  id_cip_sec int,
  cod varchar(15),
  emb_d_ini date,
  inici date,
  fi date,
  temps double,
  risc varchar(15),
  emb_c_tanca varchar(15),
  index (temps)
)
select 
    id_cip_sec
    ,concat(codi_sector, ':', emb_cod) cod
    ,emb_d_ini
	,if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur)) as inici
	,if
				(if(emb_d_fi > 0,emb_d_fi
				,if(emb_d_tanca>0,emb_d_tanca
				,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week)
				,date_add(emb_dur, interval 42 week))))
				> data_ext,data_ext,
				if(emb_d_fi > 0,emb_d_fi
				,if(emb_d_tanca>0,emb_d_tanca
				,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week)
				,date_add(emb_dur, interval 42 week))
				))) as fi
	,datediff(if
				(if(emb_d_fi > 0,emb_d_fi
				,if(emb_d_tanca>0,emb_d_tanca
				,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week)
				,date_add(emb_dur, interval 42 week))))
				> data_ext,data_ext,
				if(emb_d_fi > 0,emb_d_fi
				,if(emb_d_tanca>0,emb_d_tanca
				,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week)
				,date_add(emb_dur, interval 42 week))
				))),
			if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur))
			) as temps
    ,emb_risc risc
    ,emb_c_tanca
from
	import.embaras e
	,dextraccio
where
	if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur)) between date_add(date_add(data_ext,interval -12 month),interval -42 week) and data_ext
;

drop table if exists embaras3;
create table embaras3 as select * from embaras2 where temps > 90
;

alter table embaras3 add index (id_cip_sec,emb_d_ini),add index (id_cip_sec,temps)
;

drop table if exists embaras4;
create table embaras4 (
	id_cip_sec int,
	max_d_ini date,
	unique(id_cip_sec, max_d_ini)
)
select
	id_cip_sec
	,max(emb_d_ini) as max_d_ini
from
	embaras3
group by
	id_cip_sec
;

drop table if exists embaras5;
create table embaras5 as
select e3.* from embaras3 e3 inner join embaras4 e4 on e3.id_cip_sec=e4.id_cip_sec and emb_d_ini=max_d_ini
;

drop table if exists embaras6;
create table embaras6 (
	id_cip_sec int,
	min_temps double,
	index(id_cip_sec,min_temps)
)
select
	id_cip_sec
	,min(temps) as min_temps
from
	embaras5
group by
	id_cip_sec
;

drop table if exists embaras7;
create table embaras7 as
select e5.* from embaras5 e5 inner join embaras6 e6 on e5.id_cip_sec=e6.id_cip_sec and temps=min_temps
;

drop table if exists ass_embaras;
create table ass_embaras (
 id_cip_sec int
 ,cod varchar(15)
 ,inici date
 ,fi date
 ,temps double
 ,risc varchar(15)
 ,puerperi int
 ,index(id_cip_sec)
 ,index(cod)
)
select id_cip_sec,min(cod) cod,inici,fi,temps,max(risc) risc,0 puerperi from embaras7 where emb_c_tanca not in ('A','Al','IV','MF','MH','EE') group by id_cip_sec,inici,fi,temps having min(cod)=max(cod)
;

create temporary table b as select distinct concat(codi_sector, ':', pavi_emb_cod) cod from import.assir221 where pavi_seg_tipus in ('PPD', 'PPC', 'PSD', 'PSC')
;

update ass_embaras a inner join b on a.cod = b.cod
set puerperi = 1
;

create temporary table c as select id_cip_sec from ass_embaras group by id_cip_sec having count(1) > 1
;

delete from ass_embaras where id_cip_sec in (select id_cip_sec from c)
;

create or replace table ass_visites_embaras as
select distinct e.id_cip_sec, e.inici, datediff(v.visi_data_visita, e.inici) dies
from ass_embaras e inner join ass_visites v on e.id_cip_sec = v.id_cip_sec
where visi_data_visita between inici and fi
;

alter table ass_visites_embaras add unique(id_cip_sec, inici, dies)
;

drop table if exists embaras1;
drop table if exists embaras2;
drop table if exists embaras3;
drop table if exists embaras4;
drop table if exists embaras5;
drop table if exists embaras6;
drop table if exists embaras7;
