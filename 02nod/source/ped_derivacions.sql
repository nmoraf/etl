use nodrizas;

drop table if exists ped_derivacions;
create table ped_derivacions (
 id_cip_sec int,
 agrupador int,
 dat date,
 motiu varchar(500),
 edat_a int,
 edat_m int,
 edat_d int
)
select
    a.id_cip_sec
    ,agrupador
    ,oc_data dat
    ,motiu
    ,(YEAR(oc_data) - YEAR(data_naix)) - (MID(oc_data, 6, 5) < MID(data_naix, 6, 5)) as edat_a
    ,(YEAR(oc_data) - YEAR(data_naix)) * 12 + (MONTH(oc_data) - MONTH(data_naix)) - (DAYOFMONTH(oc_data) < DAYOFMONTH(data_naix)) edat_m
    ,datediff(oc_data,data_naix) edat_d
from
    nod_derivacions a
    inner join ped_assignada b on a.id_cip_sec = b.id_cip_sec
    inner join eqa_criteris_ped_deriv c on a.inf_servei_d_codi = c.criteri_codi
;
