from sisapUtils import getAll, createTable, execute, createTable, printTime, getSubTables, multiprocess, listToTable
from collections import defaultdict


new = {'bolquer': '23C0000', 'aas': 'AAS', 'fentanil': 'N02AB03r'}
imp = 'import'
jail = 'import_jail'
nod = 'nodrizas'


def isAAS(atc, n, freq):
    return atc == 'N02BA01' and 500.0 * n / (freq / 24.0) > 300.0
    
def do_it(params):
    taula, db, agrupadors, pf_to_atc, acut_up = params
    temp = 'temp' + db + taula
    createTable(temp, "as select id_cip_sec, ppfmc_pf_codi, ppfmc_atccodi, ppfmc_posologia, ppfmc_freq, date_format(ppfmc_pmc_data_ini,'%Y%m%d'), \
                    date_format(ppfmc_data_fi,'%Y%m%d'), if(ppfmc_data_fi > data_ext, 0, 1), ppfmc_pmc_amb_cod_up, ppfmc_pmc_amb_num_col \
                    from {}.{}, nodrizas.dextraccio where ppfmc_pmc_data_ini<=data_ext".format(db, taula), nod, rm=True)
    execute("insert into {} select id_cip_sec, ppfmc_pf_codi, pf_cod_atc, ppfmc_posologia, ppfmc_freq, date_format(ppfmc_pmc_data_ini,'%Y%m%d'), \
                    date_format(ppfmc_data_fi, '%Y%m%d'), if(ppfmc_data_fi > data_ext, 0, 1), '', '' \
                    from {}.prescripcioHistorica, nodrizas.dextraccio where codi_sector = '{}' and ppfmc_pmc_data_ini <= data_ext".format(temp, imp, taula[-4:]), nod)

    tractaments = {}
    upload = {'eqa_tractaments': [], 'eqa_tractaments_ep': [], 'urg_tractaments': []}
    sql = 'select * from {}'.format(temp)
    for id, pf, atc, n, freq, ini, fi, tancat, up, numcol in getAll(sql, nod):
        if pf in pf_to_atc:
            atc = pf_to_atc[pf]
        '''
        if isAAS(atc, n, freq):
            atc = new['aas']
        '''
        try:
            agrs = agrupadors[(atc, tancat)]
        except KeyError:
            continue
        else:
            for agr in agrs:
                upload['eqa_tractaments_ep'].append((id, agr, ini, fi))
                if tancat == 0:
                    if (id, agr) not in tractaments:
                        tractaments[(id, agr)] = {'i': ini, 'f': fi}
                    else:
                        tractaments[(id, agr)]['i'] = min(tractaments[(id, agr)]['i'], ini)
                        tractaments[(id, agr)]['f'] = max(tractaments[(id, agr)]['f'], fi)
                elif tancat == 1:
                    upload['eqa_tractaments'].append([id, agr, ini, fi, 1])
                if up in acut_up:
                    upload['urg_tractaments'].append([id, agr, ini, fi, up, numcol])

    for (id, agr), data in tractaments.items():
        upload['eqa_tractaments'].append([id, agr, data['i'], data['f'], 0])
    execute('drop table {}'.format(temp), nod)

    for table, data in upload.items():
        listToTable(data, table, nod)


if __name__ == '__main__':

    createTable('eqa_tractaments', '(id_cip_sec int,farmac int,pres_orig date,data_fi date,tancat int)', nod, rm=True)
    createTable('eqa_tractaments_ep', '(id_cip_sec int,farmac int,pres_orig date,data_fi date)', nod, rm=True)
    createTable('urg_tractaments', '(id_cip_sec int,farmac int,pres_orig date,data_fi date,up varchar(5),numcol varchar(9))', nod, rm=True)

    atcs = defaultdict(list)
    sql = "select atc_codi from cat_cpftb010_def"
    for atc, in getAll(sql, imp):
        for codi in (atc, atc[:5], atc[:4], atc[:3]):
            atcs[codi].append(atc)

    agrupadors = defaultdict(list)
    sql = "select criteri_codi, agrupador, ps_tancat from eqa_criteris_prescripcions"
    for atc, agr, tancat in getAll(sql, nod):
        if atc in new.values():
            agrupadors[atc, int(tancat)].append(int(agr))
        else:
            for codi in atcs[atc]:
                if int(agr) not in agrupadors[codi, int(tancat)]:
                    agrupadors[codi, int(tancat)].append(int(agr))

    pf_to_atc = {}
    sql = 'select pf_codi from cat_cpftb006_bolquers'
    for pf, in getAll(sql, imp):
        pf_to_atc[int(pf)] = new['bolquer']
    sql = "select pf_codi from cat_cpftb006 where pf_cod_atc = 'N02AB03' and pf_via_adm in ('B22', 'B24', 'B25', 'B34')"
    for pf, in getAll(sql, imp):
        pf_to_atc[int(pf)] = new['fentanil']
    rapids = (659213, 659214, 965194, 656757, 656758, 656671, 656670, 656669, 821934, 820860, 787945, 787911,
              650823, 650685, 650687, 663684, 650691, 663685,
              967224, 679588, 712740, 712741, 712742, 712743, 712744, 712746, 712747, 712748, 712749, 712750, 712751, 712754, 712755, 712757, 712758)
    sql = 'select pf_codi, pf_cod_atc from cat_cpftb006 where pf_codi in {}'.format(rapids)
    for pf, atc in getAll(sql, imp):
        pf_to_atc[int(pf)] = atc + 'r'
    for pf in (710663, 724999):
        pf_to_atc[pf] = 'A10BJ02r'

    acut_up = set()
    sql = 'select scs_codi from urg_centres'
    for up, in getAll(sql, nod):
        acut_up.add(up)

    jobs = [(taula, imp, agrupadors, pf_to_atc, acut_up) for taula, n in sorted(getSubTables('tractaments').items(), key=lambda x: x[1], reverse=True) if n > 0]
    jobs.extend([('tractaments', jail, agrupadors, pf_to_atc, acut_up)])

    printTime('inici iterate')
    multiprocess(do_it, jobs, 8)
    printTime('fi iterate')

    printTime('inici indexing')
    execute('alter table eqa_tractaments add index id (id_cip_sec), add index farmac (farmac), add index dat (pres_orig)', nod)
    execute('alter table eqa_tractaments_ep add index farmac (farmac)', nod)
    printTime('final indexing')
