from sisapUtils import createTable, getAll, listToTable
from collections import defaultdict


imp_db = 'import'
imp_tb = 'cmbdh'
nod_db = 'nodrizas'
nod_tb_dx = 'nod_cmbdh_dx'
nod_tb_px = 'nod_cmbdh_px'

fields = '(id_cip_sec int, id_ingres int, up varchar(5), dnaix date, sexe varchar(1), dingres date, dalta date, cingres varchar(1), calta varchar(1), procedencia varchar(1), codi varchar(5), posicio int)'
createTable(nod_tb_dx, fields, nod_db, rm=True)
createTable(nod_tb_px, fields, nod_db, rm=True)


idcip = defaultdict(set)
sql = 'select usua_nia, id_cip_sec from crg'
for nia, id in getAll(sql, imp_db):
    idcip[nia].add(id)

sql = 'select nia, eap, d_naix, sexe, d_ingres, d_alta, c_ingres, c_alta, pr_ingres, dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7 from {}'.format(imp_tb)
data_dx, data_px = [], []
ing = 0
for row in getAll(sql, imp_db):
    nia = row[0]
    if nia in idcip:
        base = row[1:9]
        dx = row[9:19]
        px = row[19:27]
        ing += 1
        for id in idcip[nia]:
            for d in range(10):
                if dx[d]:
                    data_dx.append((id, ing) + base + (dx[d], d + 1))
            for p in range(8):
                if px[p]:
                    data_px.append((id, ing) + base + (px[p], p + 1))


listToTable(data_dx, nod_tb_dx, nod_db)
listToTable(data_px, nod_tb_px, nod_db)
