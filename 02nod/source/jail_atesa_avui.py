from sisapUtils import getAll, createTable, listToTable, getOne


table = 'jail_atesa_avui'
db = 'nodrizas'
dies = 0
dext, dext_s = getOne("select data_ext, date_format(data_ext, '%Y%m%d') from dextraccio", db)


moviments = []
actual = [None, None, None, None, None]
sql = "select id_cip_sec, huab_data_ass, if(huab_data_final = 0, null, huab_data_final), huab_up_codi, huab_uab_codi from moviments where huab_data_ass <= {} order by 1 asc, 2 asc, 3 desc".format(dext_s)
for id, ini, fi, up, uba in getAll(sql, 'import_jail'):
    fi = dext if not fi or fi > dext else fi
    if id == actual[0] and up == actual[3]:
        actual[2] = fi
    else:
        if actual[2] == dext and (dext - actual[1]).days > dies:
            moviments.append([actual[0], actual[3], actual[1], actual[4]])
        actual = [id, ini, fi, up, uba]

createTable(table, '(id_cip_sec int, up varchar(5), ingres date, uba varchar(5), primary key (id_cip_sec))', db, rm=True)
listToTable(moviments, table, db)
