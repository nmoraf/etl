from sisapUtils import getAll, getOne, sectors, multiprocess, createTable, listToTable


tb = 'rip_assignada'
db = 'nodrizas'


def get_historica():
    dext, = getOne('select year(data_ext) from dextraccio', 'nodrizas')
    dats = [dext - i for i in range(1, 4)]
    tables = {dat: 'assignadaHistorica_s{}'.format(dat) for dat in dats}
    historica = {sector: {dat: {} for dat in dats} for sector in sectors}
    sql = 'select codi_sector, id_cip_sec, up, uab from {}'
    for dat, table in tables.items():
        for sector, id, up, uba in getAll(sql.format(table), 'import'):
            historica[sector][dat][id] = (up, uba)
    return historica


def get_exitus(param):
    sector, historica = param
    centres = set([up for up, in getAll('select scs_codi from cat_centres', 'nodrizas')])
    sql = "select id_cip_sec, year(usua_data_situacio), (year(usua_data_situacio)-year(usua_data_naixement))-(date_format(usua_data_situacio,'%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe from assignada_s{}, nodrizas.dextraccio \
           where usua_situacio = 'D' and usua_data_situacio between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext".format(sector)
    upload = []
    for id, dat, edat, sexe in getAll(sql, 'import'):
        if id in historica[dat - 1]:
            up, uba = historica[dat - 1][id]
        elif id in historica[dat - 2]:
            up, uba = historica[dat - 2][id]
        else:
            up, uba = None, None
        if up in centres:
            upload.append([sector, id, up, uba, edat, sexe])
    listToTable(upload, tb, db)


if __name__ == '__main__':
    historica = get_historica()
    createTable(tb, '(codi_sector varchar(4), id_cip_sec int, up varchar(5), uba varchar(5), edat int, sexe varchar(1))', db, rm=True)
    jobs = [(sector, historica[sector]) for sector in sectors]
    multiprocess(get_exitus, jobs, 8)
