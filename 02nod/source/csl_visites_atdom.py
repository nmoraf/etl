from sisapUtils import getAll, sectors, multiprocess, createTable, listToTable, execute, getOne
from collections import defaultdict


tb = 'csl_visites_atdom'
nod = 'nodrizas'
imp = 'import'


def get_poblacio():
    poblacio = defaultdict(dict)
    for sector, id, up, edat in getAll('select codi_sector, id_cip_sec, up, edat from assignada_tot', nod):
        poblacio[sector][id] = (up, edat)
    return poblacio


def get_atdom(param):
    sector, poblacio = param
    sql = "select id_cip_sec from import.problemes_s{}, nodrizas.dextraccio \
          where pr_cod_o_ps = 'C' and pr_cod_ps like 'Z74%' and pr_hist = 1 and pr_dde <= data_ext \
          and (pr_dba = 0 or pr_dba > date_add(data_ext,interval -1 year)) and (pr_data_baixa = 0 or pr_data_baixa > date_add(data_ext,interval -1 year))".format(sector)
    atdom = {id: poblacio[id] for id, in getAll(sql, imp) if id in poblacio}
    return atdom


def get_visites(param):
    table, atdom = param
    sql = "select id_cip_sec, s_espe_codi_especialitat, visi_lloc_visita, visi_up from {}, nodrizas.dextraccio \
          where visi_data_visita between date_add(date_add(data_ext,interval - 1 year), interval +1 day) and data_ext and visi_situacio_visita = 'R'".format(table)
    resul = [(id,) + atdom[id] + (espe, lloc, up) for (id, espe, lloc, up) in getAll(sql, imp) if id in atdom]
    listToTable(resul, tb, nod)


if __name__ == '__main__':
    poblacio = get_poblacio()
    mega_atdom = multiprocess(get_atdom, [(sector, poblacio[sector]) for sector in sectors], 12)
    atdom = {}
    for d in mega_atdom:
        for k, v in d.items():
            atdom[k] = v
    tables = getOne('show create table visites1', imp)[1].split('UNION=(')[1][:-1].split(',')
    jobs = [(table, atdom) for table in tables]
    createTable(tb, '(id_cip_sec int, up_pacient varchar(5), edat int, espe varchar(5), lloc varchar(1), up_visita varchar(5))', nod, rm=True)
    multiprocess(get_visites, jobs)
    execute('alter table {} add index (id_cip_sec)'.format(tb), nod)
