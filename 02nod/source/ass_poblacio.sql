use nodrizas
;

drop table if exists ass_visites1;
create table ass_visites1 as
select distinct
    id_cip_sec
    ,visi_data_visita
    ,visi_up
    ,visi_dni_prov_resp
    ,visi_dia_peticio
    ,codi_sector
    ,visi_tipus_visita
    ,visi_tipus_citacio
from
    import.visites2 a
    ,dextraccio
where
    visi_data_visita between date_add(date_add(data_ext,interval - 18 month),interval +1 day) and data_ext
    and visi_situacio_visita='R'
    and (
        s_espe_codi_especialitat in ('10099','10098','30084','30085')
        or
        (s_espe_codi_especialitat = '30999' and visi_up in (select up from ass_centres where br like 'SD%' and amb_desc like '%METRO%SUD%'))
        )
    and visi_tipus_visita not in ('9T', 'VTEL', '9E')
    and visi_modul_codi_modul not like 'ECO%'
    and visi_etiqueta not like 'ECO%'
    and visi_tipus_visita not in ('U', 'URG', 'GR')
;

alter table ass_visites1 add index (id_cip_sec)
;

drop table if exists ass_poblacio1;
create table ass_poblacio1 (
  codi_sector varchar(4),
  id_cip_sec double null,
  edat int,
  index (id_cip_sec,edat)
)
select
    codi_sector
	,id_cip_sec
	,IF((YEAR(data_ext) - YEAR(usua_data_naixement)) <= 0,0,(YEAR(data_ext) - YEAR(usua_data_naixement)) - (MID(data_ext, 6, 5) < MID(usua_data_naixement, 6, 5))) as edat 
from
	import.assignada a
	,dextraccio
where
    usua_sexe = 'D'
	and usua_situacio = 'A' 
    and IF((YEAR(data_ext) - YEAR(usua_data_naixement)) <= 0,0,(YEAR(data_ext) - YEAR(usua_data_naixement)) - (MID(data_ext, 6, 5) < MID(usua_data_naixement, 6, 5))) > 14
;

drop table if exists ass_poblacio2;
create table ass_poblacio2 as
select
    id_cip_sec
    ,visi_data_visita
    ,visi_up
    ,visi_dni_prov_resp
from
    ass_visites1
    ,dextraccio
where
    visi_data_visita between date_add(date_add(data_ext,interval - 1 year),interval +1 day) and data_ext
;

alter table ass_poblacio2 add index (id_cip_sec)
;

drop table if exists ass_poblacio3;
create table ass_poblacio3 as
select * from ass_poblacio1 a where exists (select 1 from ass_poblacio2 b where a.id_cip_sec=b.id_cip_sec)
;

drop table if exists ass_poblacio;
create table ass_poblacio as
select codi_sector,id_cip_sec,round(avg(edat),0) edat from ass_poblacio3 group by codi_sector,id_cip_sec having count(1)=1
;

alter table ass_poblacio add unique (id_cip_sec,edat)
;

drop table if exists ass_visites;
create table ass_visites as
select distinct id_cip_sec,visi_data_visita,visi_up,visi_dni_prov_resp,visi_dia_peticio,codi_sector,visi_tipus_visita,visi_tipus_citacio from ass_visites1 a where exists (select 1 from ass_poblacio b where a.id_cip_sec=b.id_cip_sec) and visi_dni_prov_resp <> ''
;

alter table ass_visites add unique (id_cip_sec,visi_data_visita,visi_up,visi_dni_prov_resp,visi_dia_peticio,codi_sector,visi_tipus_visita,visi_tipus_citacio)
;

drop table if exists ass_imputacio_up;
create table ass_imputacio_up as
select distinct id_cip_sec,visi_up from ass_poblacio2 a where exists (select 1 from ass_poblacio b where a.id_cip_sec=b.id_cip_sec)
;

alter table ass_imputacio_up add unique (id_cip_sec,visi_up)
;

create temporary table ass_professionals as select prov_dni_proveidor,prov_categoria,max(if(left(prov_categoria,1)=1,'G','L')) tipus from import.cat_pritb031_assir where prov_dni_proveidor<>'' group by prov_dni_proveidor;
alter table ass_professionals add unique dni (prov_dni_proveidor,tipus);

drop table if exists ass_imputacio_dni;
create table ass_imputacio_dni as
select distinct id_cip_sec,visi_up,visi_dni_prov_resp,prov_categoria,tipus from ass_poblacio2 a inner join ass_professionals p on a.visi_dni_prov_resp=p.prov_dni_proveidor where exists (select 1 from ass_poblacio b where a.id_cip_sec=b.id_cip_sec)
;

alter table ass_imputacio_dni add unique (id_cip_sec,visi_up,visi_dni_prov_resp,tipus)
;

drop table if exists ass_poblacio1;
drop table if exists ass_poblacio2;
drop table if exists ass_poblacio3;
drop table if exists ass_visites1;
