from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random
import operator

nod="nodrizas"
imp="import"
dextraccio = "nodrizas.dextraccio"
fa15m = "date_add(date_add(data_ext, interval -15 month), interval + 1 day)"
avui = "data_ext"
limit=""


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {};".format(dextraccio)
    return getOne(sql,nod)[0]

def data(params):
    table,current_date=params
    id_data=defaultdict(set)
    sql="select id_cip_sec,vu_cod_vs,vu_val,vu_dat_act from {}, {} where vu_cod_vs in ('DSINT', 'DALDO') and VU_USU not like 'TAO%' and vu_dat_act between {} and {}".format(table, dextraccio, fa15m, avui)
    for id,cod,val,dat in getAll(sql,imp):
        if monthsBetween(dat,current_date) < 12:
            id_data[id].add(('DSINT',val,dat))
    return id_data


def get_rows(id_data):
    return [(id,cod,val,dat,usar+1) for id in id_data for usar,(cod,val,dat) in enumerate(sorted(id_data[id], key=operator.itemgetter(2), reverse=True)) ]


def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)


 

if __name__ == '__main__':
    printTime('inici')
    current_date=get_date_dextraccio()
    id_data_total=defaultdict(set)
    for id_data in multiprocess(data, [(table,current_date) for table in getSubTables('variables')], 8):
        for id in id_data:
            id_data_total[id] |=id_data[id]
    printTime('data')
    rows=get_rows(id_data_total)
    print rows[0]
    printTime('rows')
    columns='(id_cip_sec int(11), agrupador varchar(12), valor int(4), data datetime, usar int(1))'
    export_table('eqa_variables_tao',columns,nod,rows)