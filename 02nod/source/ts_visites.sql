use nodrizas
;

DROP TABLE IF EXISTS ts_visites;
CREATE TABLE ts_visites (
  id_cip_sec int,
  domi int
)
select 
    id_cip_sec,
    max(if(visi_lloc_visita = 'D' or visi_tipus_visita = '9D', 1, if(visi_tipus_visita in ('9T', '9E'), -1, 0))) domi
from
	import.visites1
    ,dextraccio
where
    visi_data_visita between date_add(date_add(data_ext,interval - 1 year),interval +1 day) and data_ext
    and visi_situacio_visita = 'R'
    and s_espe_codi_especialitat = '05999'
group by
    id_cip_sec
;
