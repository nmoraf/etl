import sisapUtils as u
from collections import Counter

imp= 'import'
nod= 'nodrizas'
file= u.tempFolder + 'poblacio.txt'
table= 'assignada_tot'

trasplantats = [cod for cod, in u.getAll('select criteri_codi from eqa_criteris_problemes where agrupador in (529, 665)', nod)]
trasplantats.append('Z59.3')
trasplantats.append('C01-Z59.3')

mst= {
    'sql': """
            select id_cip_sec,
                id_cip {}
            from assignada a,
                nodrizas.dextraccio
            where usua_data_naixement <= data_ext
                and usua_situacio = 'A'
                and exists (
                    select '1'
                    from nodrizas.cat_centres b
                    where a.usua_uab_up = b.scs_codi)
            """
    ,'create': 'create table {} (id_cip_sec int, id_cip int{}, primary key(id_cip_sec))'
    }

pob= {
    'sql': mst['sql'].format(",codi_sector,usua_uab_ep,usua_uab_up,usua_uab_codi_uab,usua_abs_codi_abs,ass_codi_up,ass_codi_unitat,usua_uab_servei_centre,usua_uab_servei_centre_classe,usua_up_rca,date_format(usua_data_naixement,'%Y%m%d'),(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe,usua_nacionalitat,cart_tipus,usua_relacio,usua_data_peticio_lms,usua_data_connexio_lms")
    ,'create': ',codi_sector varchar(5),ep varchar(4),up varchar(5),uba varchar(5),abs int,upinf varchar(5),ubainf varchar(5),centre_codi varchar(9),centre_classe varchar(2),up_rca varchar(5),data_naix date,edat int,sexe varchar(1),nacionalitat varchar(3),nivell_cobertura varchar(2),relacio varchar(1),lms_peticio date,lms_connexio date'
    }
    
components= {
    '0origen': {
        'sql': "select distinct lloc_codi_up up,lloc_uab_codi_uab_assignat__a uba,lloc_up_rca rca from cat_pritb025_def where lloc_codi_up in (select scs_codi from nodrizas.cat_centres where tip_eap = 'N') and lloc_codi_up<>lloc_up_rca"
        ,'db': imp
        ,'creua': 'up'
        ,'default': ('',)
        ,'create': ',upOrigen varchar(5)'
        }
    ,'1abs': {
        'sql': "select abs_codi_abs, abs_codi_up from cat_rittb001"
        ,'db': imp
        ,'creua': 'abs'
        ,'default': ('',)
        ,'create': ',upABS varchar(5)'
        }
    ,'2seccio': {
        'sql': "select id_cip_sec,sector_censal from crg"
        ,'db': imp
        ,'creua': 'id'
        ,'default': ('',)
        ,'create':',seccio_censal varchar(10)'
        }
    ,'3visites': {
        'sql': "select id_cip_sec,date_format(last_visit,'%Y%m%d'),if(n_year>0,1,0),n_year,if(last_odn=0,'', date_format(last_odn,'%Y%m%d')) from eqa_last_visit,dextraccio"
        ,'db': nod
        ,'creua': 'id'
        ,'default': ('',0,0,'')
        ,'create':',last_visit date, ates int, n_year int, last_odn date'
        }
    ,'4institucionalizats': {
        'sql':  """
                select
                    distinct id_cip_sec, b.src_code, b.sisap_class
                from
                    import.institucionalitzats a inner join import.cat_sisap_map_grup_rup b
                        on a.codi_sector = b.sec and a.ug_codi_grup = b.grup
                where
                    b.sisap_class = 1
                """
        ,'db': imp
        ,'creua': 'id'
        ,'default': ('',0)
        ,'create': ',up_residencia varchar(13), institucionalitzat int'
        }
    ,'5trasplantats': {
        'sql': "select id_cip_sec,if(pr_cod_ps in ('Z59.3', 'C01-Z59.3'),1,0),if(pr_cod_ps not in ('Z59.3', 'C01-Z59.3'),1,0) from problemes,nodrizas.dextraccio where pr_cod_ps in {} and pr_dde<=data_ext and (pr_dba=0 or pr_dba>data_ext) and (pr_data_baixa=0 or pr_data_baixa>data_ext) and pr_hist=1".format(tuple(trasplantats))
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,0)
        ,'create': ',institucionalitzat_ps int, trasplantat int'
        }
    ,'6maca': {
        'sql': "select id_cip_sec,1 from estats,nodrizas.dextraccio where es_cod='ER0002' and es_dde<=data_ext \
            union select id_cip_sec,1 from problemes,nodrizas.dextraccio where pr_cod_ps in ('Z51.5', 'C01-Z51.5') and pr_dde<=data_ext and (pr_dba=0 or pr_dba>data_ext) and (pr_data_baixa=0 or pr_data_baixa>data_ext) and pr_hist=1"
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,)
        ,'create': ',maca int'
        }
    ,'7pcc': {
        'sql': "select id_cip_sec,1 from estats,nodrizas.dextraccio where es_cod='ER0001' and es_dde<=data_ext"
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,)
        ,'create': ',pcc int'
        }    
    }

post= ("delete a from {} a inner join cat_centres b on a.up = b.scs_codi where (tip_eap = 'N' and edat > 14) or (tip_eap = 'A' and edat < 15)".format(table),
       "update {} set upOrigen = if(upOrigen = '', up, upOrigen), pcc = if(maca= 1, 0, pcc)".format(table),
       "drop table if exists cat_linies",
       "create table cat_linies as select distinct up,uba,uporigen from {} where edat<12 and up<>uporigen".format(table), )
    
def addComponent(sql,db,creua):

    component= {}
    for row in u.getAll(sql,db):
        if creua== 'id':
            id= row[0]
            data= row[1:]
            if ('s',id) not in pac:
                continue
        elif creua== 'up':
            id= (row[0],row[1])
            data= row[2:]
        elif creua== 'abs':
            id= row[0]
            data= row[1:]
        component[id]= data
    return component

def checkComponent(id,up,uba,abs,component):
    
    if components[component]['creua']== 'up':
        id= (up,uba)
    elif components[component]['creua']== 'abs':
        id= abs
    try:
        resul= components[component]['data'][id]
    except KeyError:
        resul= components[component]['default']
    return resul
        
u.printTime('inici pob')
pac= Counter()
sql= mst['sql'].format('')
for cipsec,cip in u.getAll(sql,imp):
    pac['s',cipsec]+= 1
    pac['c',cip]+= 1
u.printTime('fi pob')
    
for component in sorted(components.keys()):
    u.printTime('inici',component)
    components[component]['data']= addComponent(components[component]['sql'],components[component]['db'],components[component]['creua'])
    u.printTime('fi',component)
    
u.printTime('inici join')
with u.openCSV(file) as c:
    for row in u.getAll(pob['sql'],imp):
        cipsec,cip,sector,ep, up,uba,abs= row[:7]
        if pac['s',cipsec]== 1 and pac['c',cip]== 1:
            moreData= ()
            for component in sorted(components.keys()):
                moreData= moreData + checkComponent(cipsec,up,uba,abs,component)
            c.writerow(row + moreData)
u.printTime('fi join')

u.printTime('inici up')
u.execute('drop table if exists {}'.format(table),nod)
create= mst['create'].format(table,pob['create'] + ''.join([value['create'] for key,value in sorted(components.items())]))
u.execute(create,nod)
u.loadData(file,table,nod)
u.printTime('fi up')

u.printTime('inici post')
for sql in post:
    u.execute(sql,nod)
u.printTime('fi post')
