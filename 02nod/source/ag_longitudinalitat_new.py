# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import psutil as p

import sisapUtils as u


tb = "ag_longitudinalitat_new"
db = "nodrizas"
imp = "import"

SERVEIS_GC = ("INFG", "UGC", "GCAS")


class Longitudinalitat(object):
    """."""

    def __init__(self):
        """."""
        self.create_tables()
        self.get_poblacio()
        self.get_atdom()
        self.get_especialitats()
        self.get_professionals()
        self.get_moduls()
        self.get_responsable()
        self.get_delegats()
        self.get_gestors()
        self.get_tipus()
        self.get_serveis()
        self.get_centres()
        self.execute_multiprocess()

    def create_tables(self):
        """."""
        cols = ("pac_id int", "pac_up varchar(5)", "pac_uba varchar(5)",
                "pac_ubainf varchar(5)", "pac_edat int", "pac_sexe varchar(1)",
                "pac_instit int", "pac_pcc int", "pac_maca int", "pac_atdom int",
                "prof_dni varchar(10)", "prof_numcol varchar(10)",
                "prof_categ varchar(5)", "prof_responsable int",
                "prof_delegat int", "prof_rol_gestor int",
                "modul_serv varchar(5)", "modul_espe varchar(5)",
                "modul_serv_homol varchar(5)", "modul_serv_gestor int",
                "modul_codi varchar(5)", "modul_up varchar(5)",
                "modul_eap int", "modul_uba varchar(5)",
                "modul_relacio varchar(1)", "vis_data date",
                "vis_laborable int", "vis_tipus varchar(4)",
                "vis_cita varchar(1)", "vis_tipus_homol varchar(5)",
                "vis_etiqueta varchar(4)", "particio int")
        pars = ["PARTITION p{0} VALUES IN ({0})".format(i) for i in range(13)]
        sql = "({}) PARTITION BY LIST(particio) ({})".format(
                                                            ", ".join(cols),
                                                            ", ".join(pars))
        u.createTable(tb, sql, db, rm=True)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, \
                      pcc, maca, institucionalitzat \
               from assignada_tot"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, db)}

    def get_atdom(self):
        """."""
        sql = "select id_cip_sec from eqa_problemes where ps = 45"
        self.atdom = set([id for id, in u.getAll(sql, db)
                          if id in self.poblacio])

    def get_especialitats(self):
        """."""
        sql = "select espe_codi_especialitat, espe_especialitat \
               from cat_pritb023"
        self.especialitats = {cod: des for (cod, des) in u.getAll(sql, imp)}

    def get_professionals(self):
        """."""
        sql = "select codi_sector, prov_dni_proveidor, prov_categoria \
               from cat_pritb031"
        self.professionals = {(sector, dni): categ for (sector, dni, categ)
                              in u.getAll(sql, imp)}

    def get_moduls(self):
        """."""
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, modu_codi_uab \
               from cat_vistb027"
        self.moduls = {row[:4]: row[4] for row in u.getAll(sql, imp)}

    def get_responsable(self):
        """."""
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, lloc_numcol \
               from cat_pritb025"
        llocs = {(up, lloc): col for (up, lloc, col)
                 in u.getAll(sql, imp)}
        sql = "select {}, '{}', {}, {} from {}"
        self.responsable = c.defaultdict(set)
        for param in (("uab_codi_up", "M", "uab_codi_uab",
                       "uab_lloc_de_tr_codi_lloc_de_tr", "cat_vistb039"),
                      ("uni_codi_up", "I", "uni_codi_unitat",
                       "uni_ambit_treball", "cat_vistb059")):
            this = sql.format(*param)
            for up, tipus, uba, lloc in u.getAll(this, imp):
                if uba and lloc and (up, lloc) in llocs:
                    self.responsable[(up, tipus, uba)].add(llocs[(up, lloc)])

    def get_delegats(self):
        """."""
        sql = "select codi_sector, delm_numcol, delm_numcol_del \
               from cat_pritb777"
        self.delegats = c.defaultdict(list)
        for sector, delegador, delegat in u.getAll(sql, imp):
            self.delegats[(sector, delegador)].append(delegat)

    def get_gestors(self):
        """."""
        sql = "select a.codi_sector, ide_numcol \
              from cat_pritb799 a inner join cat_pritb992 b \
              on a.rol_usu = b.ide_usuari and a.codi_sector = b.codi_sector \
              where rol_rol = 'ECAP_GEST' and ide_numcol <> ''"
        self.gestors = set([row for row in u.getAll(sql, imp)])

    def get_tipus(self):
        """."""
        sql = "select codi_sector, tv_tipus, tv_cita, tv_homol, tv_model_nou \
               from cat_vistb206"
        self.tipus = {(sector, tipus, cita): tipus if model == "S" else homol
                      for (sector, tipus, cita, homol, model)
                      in u.getAll(sql, imp)}

    def get_serveis(self):
        """."""
        sql = "select codi_sector, s_codi_servei, s_descripcio, \
                      s_espe_codi_especialitat, s_codi_servei_hom \
               from cat_pritb103"
        self.serveis = {row[:2]: row[3:] for row in u.getAll(sql, imp)}

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = set([up for up, in u.getAll(sql, db)])

    def execute_multiprocess(self):
        """."""
        sql = "show create table visites1"
        tables = u.getOne(sql, imp)[1].split("UNION=(")[1][:-1].split(",")
        workers = 8 if (p.virtual_memory()[0] / 1024**3) > 50 else 4
        u.multiprocess(self.get_visites, enumerate(tables), workers)

    def get_visites(self, params):
        """."""
        i, table = params
        sql = "select id_cip_sec, \
                      codi_sector, \
                      visi_data_visita, \
                      visi_up, \
                      visi_dni_prov_resp, \
                      visi_col_prov_resp, \
                      visi_centre_codi_centre, \
                      visi_centre_classe_centre, \
                      visi_servei_codi_servei, \
                      visi_modul_codi_modul, \
                      visi_rel_proveidor, \
                      visi_tipus_visita, \
                      visi_tipus_citacio, \
                      visi_etiqueta \
               from {}, nodrizas.dextraccio \
               where visi_data_visita between \
                        adddate(data_ext, interval -1 year) and \
                        data_ext and \
                     visi_situacio_visita = 'R'".format(table)
        upload = []
        for (id, sector, dat, up, dni, col, codi, classe, servei,
             modul, rel, tipus, citacio, etiqueta) in u.getAll(sql, imp):
            if id in self.poblacio:
                up_pob, uba, ubainf, edat, sexe, pcc, maca, instit = self.poblacio[id]
                atdom = 1 * (id in self.atdom)
                categ = self.professionals.get((sector, dni))
                responsables = self.responsable[(up, "M", uba)] | self.responsable[(up, "I", ubainf)]
                delegats = []
                for resp in responsables:
                    delegats.extend(self.delegats.get((sector, resp), []))
                ubamod = self.moduls.get((codi, classe, servei, modul))
                lab = 1 * u.isWorkingDay(dat)
                v_homol = self.tipus.get((sector, tipus, citacio))
                espe, s_homol = self.serveis.get((sector, servei), [None, None])
                this = (id, up_pob, uba, ubainf, edat, sexe,
                        instit, pcc, maca, atdom,
                        dni, col, categ,
                        9 if not col else (1 * (col in responsables)),
                        9 if not col else (1 * (col in delegats)),
                        9 if not col else (1 * ((sector, col) in self.gestors)),
                        servei, espe, s_homol,
                        1 * (servei in SERVEIS_GC),
                        modul, up, 1 * (up in self.centres), ubamod, rel,
                        dat, lab, tipus, citacio, v_homol, etiqueta, i)
                upload.append(this)
        u.listToTable(upload, tb, db, partition="p{}".format(i))


if __name__ == "__main__":
    Longitudinalitat()
