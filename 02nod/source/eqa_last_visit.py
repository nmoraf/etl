from sisapUtils import createTable, multiprocess, getSubTables, getAll, listToTable, execute, getOne, monthsBetween
from collections import defaultdict
from dateutil.relativedelta import relativedelta


imp = 'import'
nod = 'nodrizas'
tb = 'eqa_last_visit'
tb_odn = 'odn_last_visit'

# exclusions pedia
etiquetes = ('ESCO', 'VAES', 'RODN', 'VESC', 'VACU', 'RODON', 'V.ES')
moduls = ('VESC', 'I-COL', 'I-VAC', 'RODN', 'RODN1', 'RODN2', 'RODN3', 'RODN4', 'RODN5', 'RODON', 'RODON1', 'RODON2', 'RODON3', 'RODON4', 'RODON5', 'REV', 'UAAU', 'ALTES', 'ADM')
serveis = ('UAAU', 'ADM')

def do_it(taula):
    sql = "select id_cip_sec, date_format(visi_data_visita, '%Y%m%d'), s_espe_codi_especialitat, \
           if(visi_data_visita between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext, 1, 0), \
           if(visi_modul_codi_modul in {} or visi_etiqueta in {} or visi_servei_codi_servei in {}, 1, 0) \
           from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_situacio_visita = 'R' and visi_data_baixa = 0".format(moduls, etiquetes, serveis, taula)
    data = defaultdict(str)
    n = defaultdict(int)
    odn = defaultdict(str)
    for id, dat, espe, last, excl in getAll(sql, imp):
        if not excl:
            if dat > data[id]:
                data[id] = dat
            if last:
                n[id] += last
            if espe in ('10106', '10777') and dat > odn[id]:
                odn[id] = dat
    return [(id, dat) for id, dat in data.items()], [(id, c) for id, c in n.items()], [(id, dat) for id, dat in odn.items()]


if __name__ == '__main__':
    createTable(tb, '(id_cip_sec int, last_visit date, n_year int, last_odn date)', nod, rm=True)
    proc_results = multiprocess(do_it, [taula for taula in sorted(getSubTables('visites'))], 12, close=True)
    get_together = defaultdict(str)
    get_together_n = defaultdict(int)
    get_together_odn = defaultdict(str)
    for result, result_n, result_odn in proc_results:
        for id, dat in result:
            if dat > get_together[id]:
                get_together[id] = dat
        for id, n in result_n:
            get_together_n[id] += n
        for id, dat in result_odn:
            if dat > get_together_odn[id]:
                get_together_odn[id] = dat
    listToTable([(id, dat, get_together_n[id], get_together_odn[id]) for id, dat in get_together.items()], tb, nod)
    execute("alter table {} add index id (id_cip_sec)".format(tb), nod)
