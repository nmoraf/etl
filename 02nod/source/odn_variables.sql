use nodrizas;

drop table if exists odn_variables;
create table odn_variables (
 id_cip_sec int,
 agrupador int,
 valor double,
 dat date,
 baixa int
);

insert into odn_variables
select id_cip_sec,307,cfd_ind_cod,cfd_data_c,-1 baixa from import.odn505;

insert into odn_variables
select id_cip_sec,308,cfd_ind_caod,cfd_data_c,-1 baixa from import.odn505;

insert into odn_variables
select id_cip_sec,agrupador,999,dtd_data,-1 baixa from import.odn508 a inner join eqa_criteris_odn508 b on a.dtd_tra=b.criteri_codi;

insert into odn_variables
select id_cip_sec,agrupador,cb_val,cb_data,if(cb_data_fi=0,0,1) baixa from import.odn509 a inner join eqa_criteris_odn509 b on a.cb_car=b.criteri_codi,dextraccio;

insert into odn_variables
select id_cip_sec,309,999,ro_data,-1 baixa from import.odn510;

insert into odn_variables
select id_cip_sec,agrupador,999,tb_data,-1 baixa from import.odn511 a inner join eqa_criteris_odn511 b on a.tb_trac=b.criteri_codi;
