use nodrizas
;

drop tables if exists eqa_tires;
create table eqa_tires (
  id_cip_sec int
  ,tires double
  ,unique(id_cip_sec)
)
select
    id_cip_sec
    ,max(tires) tires
from
    (
    select
        id_cip_sec
        ,round(if(fit_per_rec_s = 0,0,7 * fit_cont_rec_s / fit_per_rec_s),1) tires
    from
        import.tireswithjail
        ,nodrizas.dextraccio
    where
        fit_data_alta <= data_ext
        and (fit_dat_baixa = 0 or fit_dat_baixa > data_ext)
    ) t
where
    tires > 0
group by
    id_cip_sec
;
