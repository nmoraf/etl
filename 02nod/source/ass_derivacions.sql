use nodrizas
;

drop table if exists ass_derivacions;
create table ass_derivacions (
 id_cip_sec int,
 agrupador int,
 dat date
)
select
    a.id_cip_sec
    ,agrupador
    ,oc_data dat
from
    nod_derivacions a
    inner join ass_poblacio b on a.id_cip_sec = b.id_cip_sec
    inner join eqa_criteris_assir_deriv c on a.inf_servei_d_codi = c.criteri_codi
;

insert into ass_derivacions
select
    a.id_cip_sec
    ,agrupador
    ,oc_data dat
from
    nod_oc_altres a
    inner join ass_poblacio b on a.id_cip_sec = b.id_cip_sec
    inner join eqa_criteris_assir_deriv c on a.inf_servei_d_codi = c.criteri_codi
where
    inf_codi_prova = 'AD00044'
;

drop table if exists ass_proves;
create table ass_proves (
 id_cip_sec int,
 agrupador int,
 dat date
)
select
    a.id_cip_sec
    ,agrupador
    ,oc_data dat
from
    nod_proves a
    inner join ass_poblacio b on a.id_cip_sec = b.id_cip_sec
    inner join eqa_criteris_assir_proves c on a.inf_codi_prova = c.criteri_codi
;

drop table if exists ass_ecos;
create table ass_ecos (
 id_cip_sec int
 ,ecg_data_alta date
)
select
    a.id_cip_sec
    ,ecg_data_alta
from
    import.assir226 a
    inner join ass_poblacio b on a.id_cip_sec = b.id_cip_sec
    ,dextraccio
where
    ecg_data_alta > 0
    and ecg_data_alta <= data_ext
    and (ecg_data_baixa=0 or ecg_data_baixa > data_ext)
;
