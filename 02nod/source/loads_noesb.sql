use nodrizas;

#creo taula per data de c�lcul

DROP TABLE IF EXISTS dextraccio;
CREATE TABLE `dextraccio` (
  `data_ext` DATE NOT NULL DEFAULT 0,
  grip int,
  grip_ind varchar(100),
  atdom_ind varchar(100),    #per eliminar deteccio individual
  withMaca_ind varchar(1000),    #per afegir maca
  mensual int
);

#tema grip, en el moment de la creacio (maig14) es per (apunto per adults, es replicara tb a pedia):
##dates nodriza vacunes depenent del mes - eqa_vacunes.sql de nodrizas
##eliminacio indicadors per khalix depenent del mes (eap i uab) - eqa_final.py de eqa
##ocultacio indicadors ecap depenent del mes - eqa_final.py de eqa (compte que seguim pujant llistats inutilment, resultats cal pujar per punt seguent)
##calcul metes / sintetic (en el primer conten sempre i per aixo hem de calcular i pujar a ecap, en el segon depen del mes) - eqa_ecap_sintetic.py de export (no hi ha res especial, ho treu de toShow creat en punt anterior)

DELIMITER |
CREATE TRIGGER dextraccio BEFORE INSERT ON dextraccio
  FOR EACH ROW BEGIN
    set new.grip=if(month(new.data_ext)>new.grip or (month(new.data_ext)=new.grip and datediff(last_day(new.data_ext), new.data_ext) < 6),1,0),
    new.mensual=if(datediff(last_day(new.data_ext), new.data_ext) < 6,1,0);
  END
|
DELIMITER ;

#creo taula per criteris eqa

DROP TABLE IF EXISTS import_eqa_criteris;
create table import_eqa_criteris(
taula varchar(25) not null default'',
inclusio double,
criteri_codi varchar(20) not null default'',
agrupador_desc varchar(150) not null default'',
agrupador double,
ps_tancat double,
primary key (taula,criteri_codi,agrupador,ps_tancat)
); 

#creo taula per relacions indicadors

DROP TABLE IF EXISTS eqa_relacio;
create table eqa_relacio(
ind_codi VARCHAR(10) NOT NULL DEFAULT'',
taula varchar(25) not null default'',
tipus varchar(5) not null default'',
numero_criteri double,
agrupador double,
data_min double NULL,
data_max double NULL,
baixa double,
data_baixa VARCHAR(10) NOT NULL DEFAULT'',
valor_min double NULL,
valor_max double NULL,
var_ult double NULL,
var_num double NULL,
sense double NULL,
index (ind_codi,agrupador),
index(agrupador),
index(tipus,baixa),
index(tipus,ind_codi,taula,baixa),
index(tipus,ind_codi,taula,baixa,var_num)
); 

#cat�leg nacionalitat

DROP TABLE IF EXISTS cat_nacionalitat;
CREATE TABLE cat_nacionalitat (
 codi_k varchar(10) NOT NULL DEFAULT'',
 regio_desc varchar(150) not null default'',
 codi_nac double,
 desc_nac varchar(150) not null default'',
 renta double,
 idioma double,
 comunitari double,
 index(codi_k,codi_nac)
);

#Grups edat del khalix (1a)

DROP TABLE IF EXISTS khx_edats1a;
CREATE TABLE khx_edats1a (
  edat double null,
  khalix varchar(6),
  index(edat,khalix)
);

#Grups edat del khalix (5a)


DROP TABLE IF EXISTS khx_edats5a;
CREATE TABLE khx_edats5a (
  edat double null,
  khalix varchar(6),
  descripcio VARCHAR(50) NOT NULL DEFAULT'',
  ordre double,
  index(edat,khalix,descripcio,ordre)
);


#Criteris variables de pedia!


DROP TABLE IF EXISTS ped_criteris_var;
create table ped_criteris_var(
taula varchar(15) not null default'',
inclusio double,
criteri_codi varchar(20) not null default'',
agrupador_desc varchar(150) not null default'',
agrupador double,
ps_tancat double,
unitat varchar(1) not null default'',
minim_t double,
primary key (taula,criteri_codi,agrupador,ps_tancat,unitat,minim_t)
); 


DROP TABLE IF EXISTS ass_criteris_var;
create table ass_criteris_var(
taula varchar(15) not null default'',
inclusio double,
criteri_codi varchar(20) not null default'',
agrupador_desc varchar(150) not null default'',
agrupador double,
ps_tancat double,
minim_t double,
primary key (taula,criteri_codi,agrupador,ps_tancat,minim_t)
); 

drop table if exists ass_centres;
create table ass_centres(
amb_codi varchar(2) not null default '',
amb_desc VARCHAR(100) not null default'',
sap_desc VARCHAR(100) not null default'',
up varchar(5) not null default'',
br varchar(5) not null default'',
up_desc varchar(150) not null default'',
assir varchar(150) not null default'',
up_assir varchar(5) not null default'',
br_assir varchar(5) not null default''
);
