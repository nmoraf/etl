from sisapUtils import getAll, createTable, listToTable, multiprocess, sectors
import collections as c


imp = 'import'
nod = 'nodrizas'
tab_all = 'nod_ram'
tab_eqa = 'eqa_ram'


def get_things():
    atcs = {}
    sql = 'select atc_codi from cat_cpftb010_def'
    for atc, in getAll(sql, imp):
        for i in xrange(1, 8):
            short = atc[0:i]
            if short not in atcs:
                atcs[short] = []
            atcs[short].append(atc)

    grups = {}
    sql = 'select cre_codi_grup, cre_atc_codi from cat_cpftb091'
    for grup, atc in getAll(sql, imp):
        if grup not in grups:
            grups[grup] = []
        grups[grup].extend(atcs[atc])
    for grup in grups:
        grups[grup] = set(grups[grup])

    combinats = {}
    sql = 'select ass_atc_codi_combinat, ass_atc_codi_pur from cat_cpftb090'
    for comb, pur in getAll(sql, imp):
        if pur not in combinats:
            combinats[pur] = []
        combinats[pur].append(comb)

    return atcs, grups, combinats


def do_it(params):
    sector, atcs, grups, combinats = params
    data = c.defaultdict(set)
    ids = {}
    i = 0
    sql = "select id_cip_sec, id_cip, codi_sector, ram_data_alta, ram_atc, ram_atc_creuada, ram_gravetat from ram, {}.dextraccio \
           where codi_sector='{}' and ram_data_alta <= data_ext and ram_data_baixa = 0".format(nod, sector)
    for id, idcip, sec, dat, atc, grup, gravetat in getAll(sql, imp):
        if grup > 0:
            codis = grups[grup]
        else:
            try:
                codis = atcs[atc]
            except KeyError:
                continue
        key = (id, dat)
        for codi in codis:
            data[key].add((codi, gravetat))
            if codi in combinats:
                for cod in combinats[codi]:
                    data[key].add((cod, gravetat))
        if id not in ids:
            ids[id] = [idcip, sec]

    eqa = {}
    sql = "select criteri_codi, agrupador from eqa_criteris where taula = 'ram'"
    for atc, agr in getAll(sql, nod):
        codis = atcs[atc]
        for codi in codis:
            if codi not in eqa:
                eqa[codi] = []
            if agr not in eqa[codi]:
                eqa[codi].append(agr)

    upload_all = []
    upload_eqa = []
    for (id, dat), atcs in data.items():
        for (atc, gravetat) in atcs:
            upload_all.append([id] + ids[id] + [dat, atc, gravetat])
            if atc in eqa:
                for cod in eqa[atc]:
                    upload_eqa.append([id, cod, gravetat])

    listToTable(upload_all, tab_all, nod)
    listToTable(upload_eqa, tab_eqa, nod)


if __name__ == '__main__':
    createTable(tab_all, '(id_cip_sec int, id_cip int, codi_sector varchar(4), dat date, atc varchar(7), gravetat int)', nod, rm=True)
    createTable(tab_eqa, '(id_cip_sec int, agr int, gravetat int, primary key(id_cip_sec, agr, gravetat))', nod, rm=True)
    atcs, grups, combinats = get_things()
    jobs = [(sector, atcs, grups, combinats) for sector in sectors]
    multiprocess(do_it, jobs, 8)
