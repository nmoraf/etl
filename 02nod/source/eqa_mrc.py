# coding: iso-8859-1

"""Creaci� de l'agrupador de MRC, incloent dx i FG."""


import collections as c

import sisapUtils as u


TB = "eqa_microalb"  # compte, s'afegeix a una taula ja existent
DB = "nodrizas"
AGR = 853


class MRC(object):
    """."""

    def __init__(self):
        """."""
        self.get_diagnostics()
        self.get_determinacions()
        self.get_pacients()
        self.upload_data()

    def get_diagnostics(self):
        """."""
        sql = "select id_cip_sec from eqa_problemes where ps = 53"
        self.diagnostics = set([id for id, in u.getAll(sql, DB)])

    def get_determinacions(self):
        """."""
        self.determinacions = c.defaultdict(set)
        sql = "select id_cip_sec, data_var \
               from eqa_variables \
               where agrupador = 30 and \
                     valor between 0.01 and 44.99 and \
                     usar < 3"
        for id, dat in u.getAll(sql, DB):
            self.determinacions[id].add(dat)

    def get_pacients(self):
        """."""
        self.pacients = set()
        for id in self.diagnostics:
            if len(self.determinacions[id]) == 2:
                dates = list(self.determinacions[id])
                dies = abs((dates[0] - dates[1]).days)
                if dies > 89:
                    self.pacients.add(id)

    def upload_data(self):
        """."""
        u.execute("delete from {} where agrupador = {}".format(TB, AGR), DB)
        data = [(id, AGR) for id in self.pacients]
        u.listToTable(data, TB, DB)


if __name__ == "__main__":
    MRC()
