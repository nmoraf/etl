from sisapUtils import tempFolder, getDataExt


first_year = 2012
dominis = {'Problemes': {'model': 's', 'tb': 'problemes', 'db': 'import', 'dat': 'pr_dde', 'cod': 'pr_cod_ps'},
           'Prescripcions': {'model': 's', 'tb': 'tractaments', 'db': 'import', 'dat': 'ppfmc_pmc_data_ini', 'cod': 'pf_cod_atc'},
           'V.Cliniques': {'model': 't', 'tb': 'variables', 'db': 'import', 'dat': 'vu_dat_act', 'cod': 'vu_cod_vs'},
           'V.Actuacions': {'model': 't', 'tb': 'activitats', 'db': 'import', 'dat': 'au_dat_act', 'cod': 'au_cod_ac'},
           'V.Analitiques': {'model': 't', 'tb': 'laboratori', 'db': 'import', 'dat': 'cr_data_reg', 'cod': 'cr_codi_prova_ics'},
           'Proves': {'model': 's', 'tb': 'nod_proves', 'db': 'nodrizas', 'dat': 'oc_data', 'cod': 'inf_codi_prova'},
           'Derivacions': {'model': 's', 'tb': 'nod_derivacions', 'db': 'nodrizas', 'dat': 'oc_data', 'cod': 'inf_servei_d_codi'},
           'Vacunes': {'model': 's', 'tb': 'vacunes', 'db': 'import', 'dat': 'va_u_data_vac', 'cod': 'va_u_cod'},
           'Visites': {'model': 't', 'tb': 'visites', 'db': 'import', 'dat': 'visi_data_visita', 'cod': 's_espe_codi_especialitat'},
           'ASSIR': {'model': 's', 'tb': 'assir216', 'db': 'import', 'dat': 'val_data', 'cod': 'val_var'},
           'Nen sa (211)': {'model': 's', 'tb': 'nen11', 'db': 'import', 'dat': 'val_data', 'cod': 'val_var'},
           'Nen sa (212)': {'model': 's', 'tb': 'nen12', 'db': 'import', 'dat': 'val_data', 'cod': 'val_var'}}
folder = '{}\\druid\\redics'.format(tempFolder)
folder_unix = '/home/sisap/druid/redics'
spec = {"type": "index_hadoop",
        "spec": {
            "ioConfig": {
                "type": "hadoop",
                "inputSpec": {"type": "static", "paths": "{}/*.txt".format(folder_unix)}},
            "dataSchema": {
                "dataSource": "redics",
                "granularitySpec": {
                    "type": "uniform",
                    "segmentGranularity": "year",
                    "queryGranularity": "day",
                    "intervals": ["{}-01-01/{}-{}-{}".format(first_year, getDataExt()[:4], getDataExt()[4:6], getDataExt()[6:8])]},
                "parser": {
                    "type": "string",
                    "parseSpec": {
                        "format": "csv",
                        "columns": ["sector", "dia", "domini", "count"],
                        "dimensionsSpec": {"dimensions": ["domini", "sector"]},
                        "timestampSpec": {
                            "format": "auto",
                            "column": "dia"}}},
                "metricsSpec": [
                    {"name": "count", "type": "longSum", "fieldName": "count"}]},
            "tuningConfig": {
                "type": "hadoop",
                "partitionsSpec": {"type": "hashed", "targetPartitionSize": 5000000000, "assumeGrouped": True},
                "ignoreInvalidRows": True,
                "jobProperties": {}}}}
