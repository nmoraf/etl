# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"
db = 'depart'

calcular_grip = {
                  1: {'Calcular': True, 'mesos': [11,12,1]},
                  2: {'Calcular': True, 'mesos': [11,12,1,2]},
                  3: {'Calcular': True, 'mesos': [11,12,1,2,3]},
                  4: {'Calcular': True, 'mesos': [11,12,1,2,3,4]},
                  5: {'Calcular': True, 'mesos': [11,12,1,2,3,4]},
                  6: {'Calcular': False, 'mesos': []},
                  7: {'Calcular': False, 'mesos': []},
                  8: {'Calcular': False, 'mesos': []},
                  9: {'Calcular': False, 'mesos': []},
                  10: {'Calcular': False, 'mesos': []},
                  11: {'Calcular': True, 'mesos': [11]},
                  12: {'Calcular': True, 'mesos': [11, 12]},
                }
                
dext, = getOne('select month(data_ext) from dextraccio', nod)

for mes in calcular_grip:
    if mes == dext:
        Calcular = calcular_grip[mes]['Calcular']
        mesos = calcular_grip[mes]['mesos']

codis_vacunes = { 'T': 49,
                  'GRIP': 99,
                  'TF': 631,
                  }   
                  
vacs = { 
        'VAC074': {'desc': 'Tètanues en embarassades', 'vacuna': 'T', 'dosis': 1},
        'VAC075': {'desc': 'Grip en embarassades', 'vacuna': 'GRIP', 'dosis': 1},
        'VAC095': {'desc': 'Tos ferina en embarassades', 'vacuna': 'TF', 'dosis': 1},
      }

      
antig,ant_vac = defaultdict(list), {}
for c in codis_vacunes:
    agr = codis_vacunes[c]
    sql = "select distinct criteri_codi from eqa_criteris where taula in ('vacunesped','vacunes') and agrupador = {}".format(agr)
    for crit, in getAll(sql, nod):
        antig[c].append(crit)
        antig[c].append('cutre')
    in_crit = tuple(antig[c]) 
    sql = "select vacuna from cat_prstb040_new where antigen in {0}".format(in_crit)
    for vac, in getAll(sql, imp):
        ant_vac[vac] = c

dvacunes =  defaultdict(list)

sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    sql = "select id_cip_sec,va_u_cod, va_u_data_vac from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, dataVac in getAll(sql, imp):  
        try:
            tipusv = ant_vac[vac]
        except KeyError:
            continue
        dvacunes[(id, tipusv)].append(dataVac)
      
assig = {}
sql = "select id_cip_sec, up, ates, edat, sexe from assignada_tot where sexe='D'"
for id, up, ates, edat,sexe in getAll(sql, nod):
    assig [id] = {'up':up, 'ates':ates, 'edat': edat, 'sexe':sexe}
      
sql = 'drop table if exists embaras'
execute(sql, db)
sql = "CREATE TABLE embaras (id_cip_sec int,emb_d_ini date,inici date,fi date,temps double,risc varchar(15),emb_c_tanca varchar(15), index (temps)) \
    select  id_cip_sec,emb_d_ini,if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur)) as inici,if(if(emb_d_fi > 0,emb_d_fi \
    ,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week))))	> data_ext,data_ext, \
    if(emb_d_fi > 0,emb_d_fi,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week))))) as fi \
    ,datediff(if(if(emb_d_fi > 0,emb_d_fi,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week)))) > data_ext,data_ext, \
    if(emb_d_fi > 0,emb_d_fi,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week))))),if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur))\
    ) as temps  ,emb_risc risc   ,emb_c_tanca from	import.embaras e, nodrizas.dextraccio where	if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur))"
execute(sql, db)

recompte = Counter()


sql = "select id_cip_sec, month(fi), inici, fi from embaras,nodrizas.dextraccio where  temps>90 and emb_c_tanca not in ('A','Al','IV','MF','MH','EE') and fi<data_ext and fi > date_add(data_ext,interval - 1 year)"
for id, mes, inici, fi in getAll(sql, db):
    for indicador in vacs:
        try:
            edat = assig[id]['edat']
            up = assig[id]['up']
            sexe = assig[id]['sexe']
            ates = assig[id]['ates']
        except KeyError:
            continue
        vac = vacs[indicador]['vacuna']
        dosi = vacs[indicador]['dosis']
        desc = vacs[indicador]['desc']
        if vac == 'GRIP':
            if Calcular:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
                if (id, vac) in dvacunes:
                    ok = 0
                    for data in dvacunes[(id, vac)]:
                        if inici <= data <= fi:
                            ok += 1
                    if ok >= dosi:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                        num = 1
                        if ates == 1:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
        else:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
            if (id, vac) in dvacunes:
                ok = 0
                for data in dvacunes[(id, vac)]:
                    if inici <= data <= fi:
                        ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
                        
table = 'exp_dep_vacunes_embarassades'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

upload = []
for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
    indicador = 'D' + indicador
    upload.append([up, edat, sexe, comb, indicador, tip , rec])
listToTable(upload, table, db)  

