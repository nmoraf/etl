# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"
db = 'depart'

codis_vacunes = { 'XRP': 38,
                  'T': 49,
                  'P': 311,
                  'Hib': 312,
                  'HB': 15,
                  'MCC': 313,
                  'VNC': 698,
                  'VVZ': 314,
                  'VPH': 782,
                  'HA': 34,
                  'R': 790,
                  'TF': 631,
                  'VNC23': 48,
                  'MeB': 548,
                  'MACYW': 883,
                  }   

vacs = { 
        0:{
            'VAC005': {'desc': 'DTPa Primovacunació 0 anys', 'vacuna': 'T', 'dosis': 2},
            'VAC009': {'desc': 'VPI Primovacunació 0 anys',  'vacuna': 'P', 'dosis': 2}, 
            'VAC014': {'desc': 'Hib Primovacunació 0 anys',  'vacuna': 'Hib', 'dosis': 2},
            'VAC018': {'desc': 'HB Primovacunació 0 anys',  'vacuna': 'HB', 'dosis': 2},
            'VAC022': {'desc': 'MCC Primovacunació 0 anys',  'vacuna': 'MCC', 'dosis': 1},
            'VAC024': {'desc': 'VNC Primovacunació 0 anys',  'vacuna': 'VNC', 'dosis': 2},
            'VAC056': {'desc': 'Rotavirus 2 dosis 0 anys',  'vacuna': 'R', 'dosis': 2},
            'VAC096': {'desc': 'Meningitis B 2 dosis < 2 anys en 0 i 1 anys',  'vacuna': 'MeB', 'dosis': 2},
          },
        1: {
            'VAC001': {'desc': 'XRP Primera dosi 1 any', 'vacuna': 'XRP', 'dosis': 1},
            'VAC006': {'desc': 'DTPa Primovacunació 1 any',  'vacuna': 'T', 'dosis': 2},
            'VAC007': {'desc': 'DTPa Record 1 any',  'vacuna': 'T', 'dosis': 3},
            'VAC010': {'desc': 'VPI Primovacunació 1 any',  'vacuna': 'P', 'dosis': 2},
            'VAC011': {'desc': 'VPI Record 1 any',  'vacuna': 'P', 'dosis': 3},
            'VAC015': {'desc': 'Hib Primovacunació 1 any',  'vacuna': 'Hib', 'dosis': 2},
            'VAC016': {'desc': 'Hib Record 1 any',  'vacuna': 'Hib', 'dosis': 3},
            'VAC019': {'desc': 'HB Primovacunació 1 anys',  'vacuna': 'HB', 'dosis': 2},
            'VAC020': {'desc': 'HB Record 1 any',  'vacuna': 'HB', 'dosis': 3},
            'VAC023': {'desc': 'MCC Primovacunació 1 any',  'vacuna': 'MCC', 'dosis': 1},
            'VAC025': {'desc': 'VNC Primovacunació 1 any',  'vacuna': 'VNC', 'dosis': 2},
            'VAC026': {'desc': 'VNC Record 1 any',  'vacuna': 'VNC', 'dosis': 3},
            'VAC028': {'desc': 'VVZ Primera dosi 1 any' , 'vacuna': 'VVZ', 'dosis': 1},
            'VAC049': {'desc': 'HA 1 dosi 1 any',  'vacuna': 'HA', 'dosis': 1},
            'VAC057': {'desc': 'Rotavirus 2 dosis 1 any',  'vacuna': 'R', 'dosis': 2},
            'VAC096': {'desc': 'Meningitis B 2 dosis < 2 anys en 0 i 1 anys',  'vacuna': 'MeB', 'dosis': 2},
            },
         2: {   
            'VAC002': {'desc': 'XRP Primera dosi 2 anys',  'vacuna': 'XRP', 'dosis': 1},
            'VAC008': {'desc': 'DTPa Record 2 anys',  'vacuna': 'T', 'dosis': 3},
            'VAC012': {'desc': 'VPI Record 2 anys',  'vacuna': 'P', 'dosis': 3},
            'VAC017': {'desc': 'Hib Record 2 anys',  'vacuna': 'Hib', 'dosis': 3},
            'VAC021': {'desc': 'HB Record 2 anys',  'vacuna': 'HB', 'dosis': 3},
            'VAC027': {'desc': 'VNC Record 2 anys' ,  'vacuna': 'VNC', 'dosis': 3},
            'VAC029': {'desc': 'VVZ Primera dosi 2 anys' ,  'vacuna': 'VVZ', 'dosis': 1},
            'VAC050': {'desc': 'HA 1 dosi 2 anys',  'vacuna': 'HA', 'dosis': 1},
            'VAC077': {'desc': 'DTPa Primovacunació 2 anys',  'vacuna': 'T', 'dosis': 2},
            'VAC078': {'desc': 'VPI Primovacunació 2 anys',  'vacuna': 'P', 'dosis': 2}, 
            'VAC079': {'desc': 'Hib Primovacunació 2 anys',  'vacuna': 'Hib', 'dosis': 2},
            'VAC080': {'desc': 'HB Primovacunació 2 anys',  'vacuna': 'HB', 'dosis': 2},
            'VAC081': {'desc': 'MCC Primovacunació 2 anys',  'vacuna': 'MCC', 'dosis': 1},
            'VAC082': {'desc': 'Rotavirus 2 dosis 2 anys',  'vacuna': 'R', 'dosis': 2},
            'VAC083': {'desc': 'VNC Primovacunació 2 anys',  'vacuna': 'VNC', 'dosis': 2},
            },
        3: {
            'VAC084': {'desc': 'HA 1 dosi 3 anys',  'vacuna': 'HA', 'dosis': 1},
            },
        4: {
            'VAC003': {'desc': 'XRP Segona dosi 4 anys',  'vacuna': 'XRP', 'dosis': 2},
            'VAC030': {'desc': 'VVZ Segona dosi 4 anys' ,  'vacuna': 'VVZ', 'dosis': 2},
            },
        5:{
            'VAC004': {'desc': 'XRP Segona dosi 5 anys',  'vacuna': 'XRP', 'dosis': 2},
            'VAC013': {'desc': 'VPI tres dosis',  'vacuna': 'P', 'dosis': 3},
            'VAC031': {'desc': 'VVZ Segona dosi 5 anys' ,  'vacuna': 'VVZ', 'dosis': 2},
            },
        6:{
            'VAC051': {'desc': 'HA 2 dosis 6 anys',  'vacuna': 'HA', 'dosis': 2},
            },
        7:{
            'VAC052': {'desc': 'HA 2 dosis 7 anys',  'vacuna': 'HA', 'dosis': 2},
            'VAC103': {'desc': 'DT 4 dosis als 7 anys',  'vacuna': 'T', 'dosis': 4},
            'VAC104': {'desc': 'Pertussis 4 dosis als 7 anys',  'vacuna': 'TF', 'dosis': 4},
            'VAC105': {'desc': 'Polio 4 dosis als 7 anys',  'vacuna': 'P', 'dosis': 4},
            'VAC106': {'desc': 'Hib 4 dosis als 7 anys',  'vacuna': 'Hib', 'dosis': 4},
            'VAC107': {'desc': 'HB 3 dosis als 7 anys',  'vacuna': 'HB', 'dosis': 3},
            'VAC108': {'desc': 'VNC 3 dosis als 7 anys',  'vacuna': 'VNC', 'dosis': 3},
            'VAC109': {'desc': 'XRP 2 dosis als 7 anys',  'vacuna': 'XRP', 'dosis': 2},
            'VAC110': {'desc': 'VVZ 2 dosis als 7 anys',  'vacuna': 'VVZ', 'dosis': 2},
            'VAC111': {'desc': 'MCC 3 dosis als 7 anys',  'vacuna': 'MCC', 'dosis': 3},
            },
        8:{
            'VAC112': {'desc': 'DT 4 dosis als 8 anys',  'vacuna': 'T', 'dosis': 4},
            'VAC113': {'desc': 'Pertussis 4 dosis als 8 anys',  'vacuna': 'TF', 'dosis': 4},
            'VAC114': {'desc': 'Polio 4 dosis als 8 anys',  'vacuna': 'P', 'dosis': 4},
            'VAC115': {'desc': 'Hib 4 dosis als 8 anys',  'vacuna': 'Hib', 'dosis': 4},
            'VAC116': {'desc': 'HB 3 dosis als 8 anys',  'vacuna': 'HB', 'dosis': 3},
            'VAC117': {'desc': 'VNC 2 dosis als 8 anys',  'vacuna': 'VNC', 'dosis': 3},
            'VAC118': {'desc': 'XRP 2 dosis als 8 anys',  'vacuna': 'XRP', 'dosis': 2},
            'VAC119': {'desc': 'VVZ 2 dosis als 8 anys',  'vacuna': 'VVZ', 'dosis': 2},
            'VAC120': {'desc': 'MCC 3 dosis als 8 anys',  'vacuna': 'MCC', 'dosis': 3},
            },
        12:{
            'VAC032': {'desc': 'VPH una dosi als 12 anys',  'vacuna': 'VPH', 'dosis': 1, 'sexe':'D'},
            'VAC034': {'desc': 'VPH dues dosis als 12 anys',  'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            'VAC044': {'desc': 'VVZ agrupat dues dosis als 12 anys',  'vacuna': 'VVZ', 'dosis': 2},
            'VAC126': {'desc': 'HA agrupat dues dosis als 12 anys',  'vacuna': 'HA', 'dosis': 2},
            'VAC129': {'desc': 'HA una dosis als 12 anys',  'vacuna': 'HA', 'dosis': 1},
            },   
        13:{
            'VAC033': {'desc': 'VPH una dosi als 13 anys',  'vacuna': 'VPH', 'dosis': 1, 'sexe':'D'},
            'VAC035': {'desc': 'VPH dues dosis als 13 anys',  'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            'VAC045': {'desc': 'VVZ agrupat dues dosis als 13 anys',  'vacuna': 'VVZ', 'dosis': 2},
            'VAC053': {'desc': 'HA agrupat dues dosis als 13 anys',  'vacuna': 'HA', 'dosis': 2},
            }, 
        15:{
            'VAC127': {'desc': 'VPH una dosi als 15 anys',  'vacuna': 'VPH', 'dosis': 1, 'sexe':'D'},
            'VAC128': {'desc': 'VPH dues dosis als 15 anys',  'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            }, 
        16:{
            'VAC058': {'desc': 'DT 5 dosis als 16 anys',  'vacuna': 'T', 'dosis': 5},
            'VAC059': {'desc': 'Pertussis 4 dosis als 16 anys',  'vacuna': 'TF', 'dosis': 4},
            'VAC060': {'desc': 'Polio 4 dosis als 16 anys',  'vacuna': 'P', 'dosis': 4},
            'VAC061': {'desc': 'Hib 3 dosis als 16 anys',  'vacuna': 'Hib', 'dosis': 3},
            'VAC062': {'desc': 'HB 3 dosis als 16 anys',  'vacuna': 'HB', 'dosis': 3},
            'VAC063': {'desc': 'VNC 3 dosis als 16 anys',  'vacuna': 'VNC', 'dosis': 3},
            'VAC064': {'desc': 'XRP 2 dosis als 16 anys',  'vacuna': 'XRP', 'dosis': 2},
            'VAC065': {'desc': 'VVZ 2 dosis als 16 anys',  'vacuna': 'VVZ', 'dosis': 2},
            'VAC066': {'desc': 'VPH 2 dosis als 16 anys',  'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            },  
        17:{
            'VAC086': {'desc': 'DT 5 dosis als 17 anys',  'vacuna': 'T', 'dosis': 5},
            'VAC087': {'desc': 'Pertussis 4 dosis als 17 anys',  'vacuna': 'TF', 'dosis': 4},
            'VAC088': {'desc': 'Polio 4 dosis als 17 anys',  'vacuna': 'P', 'dosis': 4},
            'VAC089': {'desc': 'Hib 3 dosis als 17 anys',  'vacuna': 'Hib', 'dosis': 3},
            'VAC090': {'desc': 'HB 3 dosis als 17 anys',  'vacuna': 'HB', 'dosis': 3},
            'VAC091': {'desc': 'VNC 3 dosis als 17 anys',  'vacuna': 'VNC', 'dosis': 3},
            'VAC092': {'desc': 'XRP 2 dosis als 17 anys',  'vacuna': 'XRP', 'dosis': 2},
            'VAC093': {'desc': 'VVZ 2 dosis als 17 anys',  'vacuna': 'VVZ', 'dosis': 2},
            'VAC094': {'desc': 'VPH 2 dosis als 17 anys',  'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            }, 
}

vacsXedat = { 
        0:{
            'VAC036': {'desc': 'HB 1 dosi abans 1 mes  0 anys', 'vacuna': 'HB', 'dosis': 1, 'edatd': 0, 'tip': 'abans'},
          },
        1:{
            'VAC037': {'desc': 'HB 1 dosi abans 1 mes  1 any', 'vacuna': 'HB', 'dosis': 1, 'edatd': 0, 'tip': 'abans'},
            'VAC038': {'desc': 'MCC 1 dosi després primer any de vida  1 any', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
            'VAC097': {'desc': 'Meningitis B 2 dosis < 2 anys en 1 i 2 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
          },
        2:{
            'VAC039': {'desc': 'MCC 1 dosi després primer any de vida  2 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
            'VAC097': {'desc': 'Meningitis B 2 dosis < 2 anys en 1 i 2 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC098': {'desc': 'Meningitis B 2 dosis < 2 anys en 2 i 3 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
          },
        3:{
            'VAC098': {'desc': 'Meningitis B 2 dosis < 2 anys en 2 i 3 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           }, 
        4:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           }, 
        5:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           }, 
        6:{
            'VAC046': {'desc': 'T 1 dosi després 6 anys  6 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
          },
        7:{
            'VAC047': {'desc': 'T 1 dosi després 6 anys  7 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
          }, 
        8:{
            'VAC085': {'desc': 'T 1 dosi després 6 anys  8 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
          },
        9:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           'VAC125': {'desc': 'T 1 dosi després 6 anys  9 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
           }, 
        10:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
           }, 
        11:{
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'després'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            },
        12:{
            'VAC040': {'desc': 'MCC 1 dosi després 10 anys de vida  12 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC130': {'desc': 'MACYW 1 dosi després 10 anys de vida  12 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC042': {'desc': 'VVZ 2 dosis després 10 anys de vida  12 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
          },
        13:{
            'VAC041': {'desc': 'MCC 1 dosi després 10 anys de vida  13 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC131': {'desc': 'MACYW 1 dosi després 10 anys de vida  13 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC043': {'desc': 'VVZ 2 dosis després 10 anys de vida  13 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC054': {'desc': 'HA 2 dosis després 10 anys de vida  13 anys', 'vacuna': 'HA', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            'VAC132': {'desc': 'MACYW 1 dosi després dels 12 anys en 13 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 144, 'tip': 'després'},
          },
         14:{
            'VAC048': {'desc': 'T 1 dosi després 12 anys  14 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
            'VAC055': {'desc': 'HA 2 dosis després 10 anys de vida  14 anys', 'vacuna': 'HA', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            'VAC133': {'desc': 'MACYW 1 dosi després dels 12 anys en 14 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 144, 'tip': 'després'},
          },
        15:{
            'VAC068': {'desc': 'T 1 dosi després 12 anys  15 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'després'},
            'VAC134': {'desc': 'MACYW 1 dosi després dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'després'},
          },   
        16:{
            'VAC067': {'desc': 'MCC 1 dosi després 10 anys  16 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC134': {'desc': 'MACYW 1 dosi després dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'després'},
          }, 
        17:{
            'VAC124': {'desc': 'MCC 1 dosi després 10 anys  17 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC134': {'desc': 'MACYW 1 dosi després dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'després'},
          },
        18:{
            'VAC134': {'desc': 'MACYW 1 dosi després dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'després'},
          },
}

antig,ant_vac = defaultdict(list), defaultdict(list)
for c in codis_vacunes:
    agr = codis_vacunes[c]
    sql = "select distinct criteri_codi from eqa_criteris where taula in ('vacunesped','vacunes') and agrupador = {}".format(agr)
    for crit, in getAll(sql, nod):
        antig[c].append(crit)
        antig[c].append('cutre')
    in_crit = tuple(antig[c]) 
    sql = "select vacuna from cat_prstb040_new where antigen in {0}".format(in_crit)
    for vac, in getAll(sql, imp):
        ant_vac[vac].append(c)

for rrr in ant_vac.items():
    print rrr

vacunats = {}
sql = 'select id_cip_sec, agrupador, dosis from ped_vacunes'
for id, agr, dosi in getAll(sql, nod):
    vacunats[(id, agr)] = dosi

sql = 'select id_cip_sec, agrupador, dosis from eqa_vacunes'
for id, agr, dosi in getAll(sql, nod):
    vacunats[(id, agr)] = dosi
    
    
dvacunes =  defaultdict(list)

sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    sql = "select id_cip_sec,va_u_cod, va_u_data_vac from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, dataVac in getAll(sql, imp):  
        if vac in ant_vac:
            for tipusv in ant_vac[vac]:
                dvacunes[(id, tipusv)].append(dataVac)
    
resultatsIndividual = {}
recompte = Counter()

sql = 'select id_cip_sec, up, ates, edat_a, sexe, data_naix from ped_assignada union\
        select id_cip_sec, up, ates, edat, sexe, data_naix from assignada_tot where edat <19'
for id, up, ates, edat, sexe, dnaix in getAll(sql, nod):
    if edat in vacs:
        for indicador in vacs[edat]:
            den, num = 1, 0
            vac = vacs[edat][indicador]['vacuna']
            dosi = vacs[edat][indicador]['dosis']
            desc = vacs[edat][indicador]['desc']
            agr_vac = codis_vacunes[vac]
            try:
                sex = vacs[edat][indicador]['sexe']
            except KeyError:
                sex = sexe
            if sex == sexe:
                edat = int(edat)
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
                if (id, agr_vac) in vacunats:
                    dosiposada = vacunats[(id, agr_vac)]
                    if dosiposada >= dosi:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                        num = 1
                        if ates == 1:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
                resultatsIndividual[(id, indicador)] = {'ates': ates, 'desc': desc, 'up': up, 'den': den, 'num': num}
    if edat in vacsXedat:
        for indicador in vacsXedat[edat]:
            edat = int(edat)
            den, num = 1, 0
            vac = vacsXedat[edat][indicador]['vacuna']
            dosi = vacsXedat[edat][indicador]['dosis']
            desc = vacsXedat[edat][indicador]['desc']
            edatDosi = vacsXedat[edat][indicador]['edatd']
            tip = vacsXedat[edat][indicador]['tip']
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
            if (id, vac) in dvacunes:
                ok = 0
                for data in dvacunes[(id, vac)]:
                    edat_m = monthsBetween(dnaix, data)
                    if tip == 'abans':
                        if edat_m <= edatDosi:
                            ok += 1
                    else:
                        if edat_m >= edatDosi:
                            ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
            resultatsIndividual[(id, indicador)] = {'ates': ates, 'desc': desc, 'up': up, 'den': den, 'num': num}

#calcul estrany pel meningo B (VAC069 i VAC070)

vac069 = 'Meningitis B en 2-10 anys amb 3 dosis i que tenen 1 dosi abans dels 2'
vac070 = 'Meningitis B en 2-10 anys amb 2 dosis i sense cap dosi abans dels 2' 

meningoB = {}
sql = 'select id_cip_sec, datamin, dosis from ped_vacunes where agrupador=548'
for id, datamin, dosi in getAll(sql, nod):
    meningoB[(id)] = {'datamin':datamin, 'dosi':dosi}

sql = 'select id_cip_sec, up, ates, edat_a, sexe, data_naix from ped_assignada where edat_a between 2 and 10'
for id, up, ates, edat, sexe, naix in getAll(sql, nod):
    edat = int(edat)
    if (id) in meningoB:
        datamin = meningoB[(id)]['datamin']
        dosis = meningoB[(id)]['dosi']
        edat_V = monthsBetween(naix, datamin)
        if 0 <= int(edat_V) <= 24:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC069', 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC069', 'DEN'] += 1
            if dosis >= 3: 
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC069', 'NUM'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC069', 'NUM'] += 1
        else:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC070', 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC070', 'DEN'] += 1
            if dosis >= 2: 
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC070', 'NUM'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC070', 'NUM'] += 1
    else:
        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC070', 'DEN'] += 1
        if ates == 1:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC070', 'DEN'] += 1
            
table = 'exp_dep_vacunes'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

upload = []
for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
    indicador = 'D' + indicador
    upload.append([up, edat, sexe, comb, indicador, tip , rec])
listToTable(upload, table, db)
