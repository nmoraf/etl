# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'depart'
imp = 'import'
nod = 'nodrizas'

components = ['DNOFUMA', 'DFUMA', 'DEXFUMA', 'DNOINFOTAB']


def get_tabac():
    tabacs = {}
    sql = 'select id_cip_sec, tab from eqa_tabac, dextraccio where data_ext between dalta and dbaixa'
    for id, tabac in getAll(sql, nod):
        tabac = int(tabac)
        if tabac == 0:
            tabacTxt = 'DNOFUMA'
        elif tabac == 1:
            tabacTxt = 'DFUMA'
        elif tabac == 2:
            tabacTxt = 'DEXFUMA'
        else:
            tabacTxt = 'DNOINFOTAB'
        tabacs[(id)] = {'tabac': tabacTxt}
    return tabacs

def do_things(tabacs):
    uploadTabac = []
    recompte = Counter()
    sql = "select id_cip_sec, edat, sexe, up, if(institucionalitzat=1,'INS','NOINS'), ates from assignada_tot where edat >14"
    for id, edat, sexe, up, insti, ates in getAll(sql, nod):
        for component in components:
            comb = insti + 'ASS'
            recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, component,'DEN'] += 1
            if int(ates) == 1:
                comb = insti + 'AT'
                recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, component,'DEN'] += 1
        try:
            tabacTxt = tabacs[(id)]['tabac']
        except KeyError:
            tabacTxt = 'DNOINFOTAB'
        comb = insti + 'ASS'
        recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, tabacTxt, 'NUM'] += 1
        if int(ates) == 1:
            comb = insti + 'AT'
            recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, tabacTxt, 'NUM'] += 1
    for (up, edat, sexe, comb, tabac, tip), rec in recompte.items():
         uploadTabac.append([up, edat, sexe, comb, tabac, tip , rec])
    listToTable(uploadTabac, TableTabac, db)
    
              
tabacs = get_tabac()

TableTabac = 'exp_dep_tabac'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(TableTabac,create,db, rm=True)

do_things(tabacs)

