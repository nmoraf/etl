from sisapUtils import *
import sys

db = 'depart'
centres="nodrizas.cat_centres"

taules_dep  = ['exp_dep_tabac', 'exp_dep_ta','exp_dep_cessacions', 'exp_dep_vacunes', 'exp_dep_vacunes_adults', 'exp_dep_vacunes_embarassades']

error = []

tableMy = 'exp_khalix_departament_up'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(tableMy,create,db, rm=True)

for taula in taules_dep:
    execute('insert into {0} select * from {1}'.format(tableMy, taula), db)
    

sql = "select  indicador, concat('A','periodo'), ics_codi, tipus, edat, comb, sexe, 'N',recompte from {0}.{1} a inner join {2} b on a.up=scs_codi".format(db, tableMy, centres)
file="DEPTSALUT"
error.append(exportKhalix(sql, file))

sql = "select indicador, concat('A', 'periodo'), aga, tipus, edat, comb, sexe, 'N', sum(recompte) from {}.{} a inner join {} b on a.up = b.scs_codi group by 1, 2, 3, 4, 5, 6, 7, 8".format(db, tableMy, centres)
file = 'DEPTSALUT_AGA'
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime()