# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'depart'
nod = 'nodrizas'

indicador = 'PS0001'

def get_dextraccio():
    dext = {}
    sql = 'select data_ext from dextraccio'
    for data, in getAll(sql, nod):
        dext = data
    return dext

def get_HTA():
    pacients = {}
    sql = 'select id_cip_sec from eqa_problemes where ps=55'
    for id, in getAll(sql, nod):
        pacients[id] = True
    return pacients
    
def get_TA():
    tensions = {}
    sql = 'select id_cip_sec, data_var, agrupador, valor from eqa_variables where usar=1 and agrupador in (16, 17)'
    for id, data, agr, val in getAll(sql, nod):
        fa = monthsBetween(data, dext)
        if 0 <= fa <= 11:
            tensions[(id, agr)] = val
    return tensions
    
def do_process(pacients, tensions):
    upload = []
    recompte = Counter()
    sql = "select id_cip_sec, edat, sexe, up, if(institucionalitzat=1,'INS','NOINS'), ates from assignada_tot where edat >14"
    for id, edat, sexe, up, insti, ates in getAll(sql, nod):
        den, numTAS, numTAD, num, excl = 0, 0, 0, 0, 0
        if id in pacients:
            if (id, 16) in tensions and (id, 17) in tensions:
                den = 1
                TAS = tensions[id, 16]
                TAD = tensions[id, 17]
                if 0 <= TAS <= 140:
                    numTAS = 1
                if 0<= TAD <= 90:
                    numTAD = 1
                if numTAS ==1 and numTAD == 1:
                    num =1
                comb = insti + 'ASS'
                recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'DEN'] += den
                recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'NUM'] += num
                if int(ates) == 1:
                    comb = insti + 'AT'
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'DEN'] += den
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'NUM'] += num
    for (up, edat, sexe, comb, tip), rec in recompte.items():
         upload.append([up, edat, sexe, comb, indicador, tip , rec])
    listToTable(upload, table, db)
    
              
dext = get_dextraccio()
pacients = get_HTA()
tensions = get_TA()

table = 'exp_dep_TA'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

do_process(pacients, tensions)
        