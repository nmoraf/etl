# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"
db = 'depart'

calcular_grip = {
                  1: {'Calcular': True, 'mesos': [11,12,1]},
                  2: {'Calcular': True, 'mesos': [11,12,1,2]},
                  3: {'Calcular': True, 'mesos': [11,12,1,2,3]},
                  4: {'Calcular': True, 'mesos': [11,12,1,2,3,4]},
                  5: {'Calcular': True, 'mesos': [11,12,1,2,3,4]},
                  6: {'Calcular': False, 'mesos': []},
                  7: {'Calcular': False, 'mesos': []},
                  8: {'Calcular': False, 'mesos': []},
                  9: {'Calcular': False, 'mesos': []},
                  10: {'Calcular': False, 'mesos': []},
                  11: {'Calcular': True, 'mesos': [11]},
                  12: {'Calcular': True, 'mesos': [11, 12]},
                }
                
dext, = getOne('select month(data_ext) from dextraccio', nod)
dextd, = getOne('select data_ext from dextraccio', nod)

for mes in calcular_grip:
    if mes == dext:
        Calcular = calcular_grip[mes]['Calcular']
        mesos = calcular_grip[mes]['mesos']


codis_vacunes = { 'T': 49,
                  'VNC': 698,
                  'VNC23': 48,
                  'GRIP': 99,
                  }   
                  
vacs = { 
        'VAC071': {'desc': 'VNC23 1 dosi en >= 65 anys', 'vacuna': 'VNC23', 'dosis': 1},
        'VAC072': {'desc': 'VNC13 1 dosi en >= 65 anys', 'vacuna': 'VNC', 'dosis': 1},
        'VAC073': {'desc': 'Tètanus 1 dosi en >= 65 anys', 'vacuna': 'T', 'dosis': 1},
        'VAC076': {'desc': 'Grip en >= 65 anys', 'vacuna': 'GRIP', 'dosis': 1},
        }
        
vacs6570 = { 
        'VAC121': {'desc': 'VNC23 1 dosi en 65 - 70 anys', 'vacuna': 'VNC23', 'dosis': 1},
        'VAC122': {'desc': 'VNC13 1 dosi en 65 - 70 anys', 'vacuna': 'VNC', 'dosis': 1},
        'VAC123': {'desc': 'Tètanus 1 dosi en 65 - 70 anys', 'vacuna': 'T', 'dosis': 1},
        }
        
antig,ant_vac = defaultdict(list), {}
for c in codis_vacunes:
    agr = codis_vacunes[c]
    sql = "select distinct criteri_codi from eqa_criteris where taula in ('vacunesped','vacunes') and agrupador = {}".format(agr)
    for crit, in getAll(sql, nod):
        antig[c].append(crit)
        antig[c].append('cutre')
    in_crit = tuple(antig[c]) 
    sql = "select vacuna from cat_prstb040_new where antigen in {0}".format(in_crit)
    for vac, in getAll(sql, imp):
        ant_vac[vac] = c
        
dvacunes =  defaultdict(list)

sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    sql = "select id_cip_sec,va_u_cod, va_u_data_vac from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, dataVac in getAll(sql, imp):  
        try:
            tipusv = ant_vac[vac]
        except KeyError:
            continue
        dvacunes[(id, tipusv)].append(dataVac)

recompte = Counter()
       
sql = 'select id_cip_sec, up, ates, edat, sexe, data_naix from assignada_tot where edat >64'
for id, up, ates, edat, sexe, dnaix in getAll(sql, nod):
    for indicador in vacs:
        edat = int(edat)
        den, num = 1, 0
        vac = vacs[indicador]['vacuna']
        dosi = vacs[indicador]['dosis']
        desc = vacs[indicador]['desc']
        if vac == 'GRIP':
            if Calcular:
                if mes in mesos:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
                    if (id, vac) in dvacunes:
                        ok = 0
                        for data in dvacunes[(id, vac)]:
                            b = monthsBetween(data, dextd)
                            if 0 <= b <= 6:
                                ok += 1
                        if ok >= dosi:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                            num = 1
                            if ates == 1:
                                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
        else:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
            if (id, vac) in dvacunes:
                ok = 0
                for data in dvacunes[(id, vac)]:
                    edat_y = yearsBetween(dnaix, data)
                    if edat_y > 64:
                        ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
    for indicador in vacs6570:
        if 65 <= edat <= 70:
            vac = vacs6570[indicador]['vacuna']
            dosi = vacs6570[indicador]['dosis']
            desc = vacs6570[indicador]['desc']
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
            if (id, vac) in dvacunes:
                ok = 0
                for data in dvacunes[(id, vac)]:
                    edat_y = yearsBetween(dnaix, data)
                    if edat_y > 64:
                        ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
                        
table = 'exp_dep_vacunes_adults'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

upload = []
for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
    indicador = 'D' + indicador
    upload.append([up, edat, sexe, comb, indicador, tip , rec])
listToTable(upload, table, db)
