from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="ass"
conn = connect((db,'aux'))
c=conn.cursor()

nod = 'nodrizas'

path="./dades_noesb/ass_relacio.txt"
pathc="./dades_noesb/ass_ind.txt"
pathmetes="./dades_noesb/eqassir_metesres_subind_anual.txt"
pathpond="./dades_noesb/ass_ponderacio_anual.txt"

OutFile = tempFolder + 'excloureEmb.txt'
TableMyExc = "ass_exclusions_gine"

pac1 = "indicadors_pacient1"
pac = "mst_indicadors_pacient"
den="indicador"
embaras="ass_embaras"
puerperi="ass_puerperi"
poblacio="nodrizas.ass_imputacio_dni"
poblacioup="nodrizas.ass_imputacio_up"
assignada="nodrizas.ass_poblacio"
edats_k = "nodrizas.khx_edats5a"
u11 = "mst_u11"
u11nod = "nodrizas.eqa_u11"
centres="nodrizas.ass_centres"

up1 = "eqa_khalix_up_pre1"
up2 = "eqa_khalix_up_pre2"
up = "eqa_khalix_up_pre"
pobk = "eqa_poblacio_khx"
khalix_pre = "eqa_khalix_up_ind"
khalix = "exp_khalix_up_ind"
conceptes=[['NUM','num'],['DEN','den']]
sexe = "if(sexe='H','HOME','DONA')"
comb="NOINSAT"
condicio_khalix = "excl=0" 

ecap_ind = "exp_ecap_uba"  
uba5="uba5"
condicio_ecap_ind = "excl=0"
uba_in = "mst_ubas"
minPob = 50

khalix_uba = 'exp_khalix_uba_ind'
dimensions = [['NUM','resolts', 1],['DEN','detectats', 1],['AGNORESOL','llistat', 1],['AGRESOLTR','resolucio', 100],['AGRESOLT','resolucioPerPunts', 100],['AGRESULT','resultatPerPunts', 100],['AGASSOLP','resultatPerPunts', 100],['AGASSOL','punts', 1],['AGMMINRES','mmin_p', 100],['AGMMAXRES','mmax_p', 100]]

ecap_pac_pre = "ass_ecap_pacient_pre"
ecap_pac = "exp_ecap_pacient"
ecap_marca_exclosos = "if(excl=1,4,0)"
condicio_ecap_llistat = "llistat=1 and excl=0"

metesOrig = "ass_metes_subind_anual"
metes = "eqa_metes"
ponderacio = "ass_ponderacio"


indic = 'ass_indicadors'
cataleg = "exp_ecap_cataleg"
catalegPare = "exp_ecap_catalegPare"
catalegPunts = "exp_ecap_catalegPunts"
catalegKhx = "exp_khalix_cataleg"

sql = 'select data_ext from dextraccio'
for d in getOne(sql, 'nodrizas'):
    dara = d

c.execute("drop table if exists %s" % pac1)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',dni varchar(10) not null default'',tipus varchar(1) not null default'',num double,den double,excl double,llistat double)" % pac1)

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="e":
         sql = "insert into %s select a.id_cip_sec,'%s',visi_up up,visi_dni_prov_resp dni, tipus,%s num,1 den,0 excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(pac1,ind,ind,embaras,poblacio)
         where = []
         if tmin and tmax:
            where.append("temps between {} and {}".format(tmin, tmax))
         if vmin and vmax:
            where.append("exists (select 1 from nodrizas.ass_visites_embaras c where a.id_cip_sec = c.id_cip_sec and a.inici = c.inici and c.dies between {} and {})".format(vmin, vmax))
         if where:
            sql += " where {}".format(" and ".join(where))
         c.execute(sql)
      elif tipus=="p":
         indDEn = ind + '_den'
         c.execute("insert into %s select a.id_cip_sec,'%s',visi_up up,visi_dni_prov_resp dni,tipus,%s num,1 den,0 excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where %s = 1" %(pac1,ind,ind,puerperi,poblacio, indDEn))
      elif tipus=="etancat":
         c.execute("delete a.* from %s a where indicador='%s'" % (pac1,ind))
         c.execute("insert into %s select a.id_cip_sec,'%s',visi_up up,visi_dni_prov_resp dni,tipus,%s num,1 den,0 excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" % (pac1,ind,ind,ind,poblacio))
      elif tipus=="t":
         taulatab = 'ass_tabac'
         c.execute("insert into %s select a.id_cip_sec,'%s',visi_up up,visi_dni_prov_resp dni,tipus,num,den,0 excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where den=1" % (pac1,ind,taulatab,poblacio))
      elif tipus=="bolet":
         taula = 'ass_indicador_{}'.format(ind)
         c.execute("insert into %s select a.id_cip_sec,'%s',visi_up up,visi_dni_prov_resp dni,tipus,num,den,0 excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where den=1" % (pac1,ind,taula,poblacio))
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="d":
         if agr=="z":
            t="%s_%s" % (den,ind)
            c.execute("insert into %s select a.id_cip_sec,'%s',visi_up up,visi_dni_prov_resp dni,tipus, num,den,excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(pac1,ind,t,poblacio))

sql = "select up from nodrizas.ass_centres \
       where up_assir not in (select up_assir \
                              from nodrizas.ass_imputacio_dni a \
                              inner join nodrizas.ass_centres b on a.visi_up = b.up \
                              where tipus = 'G' \
                              group by up_assir \
                              having count(1) > 299)"
assir_sense_gine = tuple([k for k, in getAll(sql, "nodrizas")])
execute("update {} set tipus = 'E' where tipus = 'L' and up in {}".format(pac1, assir_sense_gine), db)

indGine = []
c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',dni varchar(10) not null default'',tipus varchar(1) not null default'',edat double,sexe varchar(1) not null default'',num double,den double,excl double,llistat double)" % pac)         
inversos = set(["fake"])
with open(pathc, 'rb') as file:
    d=csv.reader(file, delimiter='@', quotechar='|')
    for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
        if baixa==1:
            ok=1
        else:
            c.execute("insert into %s select a.id_cip_sec,indicador,up,dni,tipus,edat,'D' sexe, num,den,excl,if(%s and num + %s in (0, 2), 1, 0) llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s'" % (pac,int(llistat),int(invers),pac1,assignada,i))
        if grup == 'EQAA02':
            indGine.append(i)
        if int(invers):
            inversos.add(i)
in_crit = tuple(indGine)        
excloure = {}
sql = "select id_cip_sec, inici, fi from {}".format(embaras)
for id, inici, fi in getAll(sql, db):
    if fi == dara:
        excloure[id] = True
sql = "select id_cip_sec, dat from {} ".format(puerperi)
for id, data in getAll(sql, db):
    dies = daysBetween(data, dara)
    if dies < 90:
        excloure[id] = True
with openCSV(OutFile) as w:
    for (id), d in excloure.items():
         w.writerow([id])
execute("drop table if exists {}".format(TableMyExc), db)
execute("create table {} (id_cip_sec int)".format(TableMyExc), db)
loadData(OutFile, TableMyExc, db)
execute("alter table {} add index (id_cip_sec)".format(TableMyExc), db)
execute("update {0} a inner join {1} b on a.id_cip_sec=b.id_cip_sec set excl = 1 where indicador in {2}".format(pac, TableMyExc, in_crit), db)

excloure_dT = []
ind_dt = 'EQA9203'
sql = "select id_cip_sec from eqa_exclusions where agrupador=576 and motiu in ('Z28.0', '1')"
for id, in getAll(sql, nod):
    excloure_dT.append([id])
Table_dt = 'exclusions_dt'
create = "(id_cip_sec int, index(id_cip_sec))"
createTable(Table_dt,create,db, rm=True)
listToTable(excloure_dT, Table_dt, db)

execute("update {0} a inner join {1} b on a.id_cip_sec=b.id_cip_sec set excl = 1 where indicador in ('{2}')".format(pac, Table_dt, ind_dt), db)


c.execute("drop table if exists %s" % ecap_pac_pre)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default'',dni varchar(10) not null default'',tipus varchar(1) not null default'',indicador varchar(10) not null default'',excl int,exclos int,toShow int)" % ecap_pac_pre)
c.execute("insert into %s select id_cip_sec,up,dni,tipus,indicador,excl,%s,1 from %s where %s" % (ecap_pac_pre,ecap_marca_exclosos,pac,condicio_ecap_llistat))
c.execute("alter table %s add index(id_cip_sec)" % ecap_pac_pre)


c.execute("drop table if exists %s" % ecap_pac)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',dni varchar(10) not null default'',tipus varchar(1) not null default'',indicador varchar(10) not null default'',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("drop table if exists %s" % u11)
c.execute("create table %s like %s" % (u11,u11nod))
c.execute("insert into %s select * from %s a where exists (select 1 from %s b where toShow=1 and a.id_cip_sec=b.id_cip_sec)" % (u11,u11nod,ecap_pac_pre))
c.execute("alter table %s add index(id_cip_sec,hash_d,codi_sector)" % u11)
c.execute("insert into %s select a.id_cip_sec,up,dni,tipus,indicador as grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1" % (ecap_pac,ecap_pac_pre,u11))

         
c.execute("drop table if exists %s" % up1)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',indicador varchar(10) not null default'',num double,den double, excl double)" % up1)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="e":
         sql = "insert into %s select a.id_cip_sec,visi_up up,'%s', %s num,1 den,0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(up1,ind,ind,embaras,poblacioup)
         where = []
         if tmin and tmax:
            where.append("temps between {} and {}".format(tmin, tmax))
         if vmin and vmax:
            where.append("exists (select 1 from nodrizas.ass_visites_embaras c where a.id_cip_sec = c.id_cip_sec and a.inici = c.inici and c.dies between {} and {})".format(vmin, vmax))
         if where:
            sql += " where {}".format(" and ".join(where))
         c.execute(sql)
      elif tipus=="p":
         c.execute("insert into %s select a.id_cip_sec,visi_up up,'%s', %s num,1 den,0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(up1,ind,ind,puerperi,poblacioup))
      elif tipus=="etancat":
         c.execute("delete a.* from %s a where indicador='%s'" % (up1,ind))
         c.execute("insert into %s select a.id_cip_sec,visi_up up,'%s', %s num,1 den,0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(up1,ind,ind,ind,poblacioup))

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="d":
         if agr=="z":
            t="%s_%s" % (den,ind)
            c.execute("insert into %s select a.id_cip_sec,visi_up up,'%s', num, den, excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(up1,ind,t,poblacioup))
      elif tipus=="bolet":
         #T21 = 'ass_indicador_EQA9221'
         taula = 'ass_indicador_{}'.format(ind)
         c.execute("insert into %s select a.id_cip_sec,visi_up up,'%s', num, den, 0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(up1,ind,taula,poblacioup))
   
         
c.execute("drop table if exists %s" % up2)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',indicador varchar(10) not null default'',num double,den double,excl double)" % up2)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
      if baixa==1:
         ok=1
      else:
         c.execute("insert into %s select a.id_cip_sec,up,edat,'D' sexe,indicador,num,den,excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s'" % (up2,up1,assignada,i))

c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',indicador varchar(10) not null default'',num double,den double)" % up)        
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,up2,edats_k,condicio_khalix))       

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up_assir,edat,sexe,'%s' comb,indicador,'%s',sum(%s) n from %s a inner join %s b on a.up=b.up group by 1,2,3,4,5" % (khalix,comb,conc,var,khalix_pre,centres))

c.execute("drop table if exists %s" % uba_in)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default '',primary key(up,uba,tipus))" % uba_in)
c.execute("insert into %s select visi_up up, visi_dni_prov_resp uba, if(tipus = 'L' and visi_up in %s, 'E', tipus) from %s where visi_up <> '' group by 1, 2, 3 having count(1)>=%d" % (uba_in,assir_sense_gine,poblacio,minPob))
    
c.execute("drop table if exists %s" % uba5)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default'',indicador varchar(8) not null default '',detectats int,resolts int)" % uba5)
c.execute("insert into %s select up,dni uba,tipus,indicador,sum(den) detectats,sum(num) resolts from %s where %s group by 1,2,3,4" % (uba5,pac,condicio_ecap_ind))

c.execute("drop table if exists %s" % ecap_ind)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default '', indicador varchar(8) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int, resolucioPerPunts double, invers int, mmin_p double, mmax_p double)" % ecap_ind)
c.execute("insert into %s select a.up, uba,tipus, a.indicador,0 esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from %s a" % (ecap_ind,uba5))
c.execute("update %s set deteccio=if((detectats/esperats) is null,0,detectats/esperats),resolucio=if(detectats=0,0,resolts/detectats)" % ecap_ind)


#aqui anirien esperades Com que no hi ha esperades fem resultat igual resolucio. Posem 1 per si de cas!
c.execute("update %s set deteccioPerResultat=1" % ecap_ind)

c.execute("update %s set resultat=resolucio" % ecap_ind)
c.execute("delete from %(ecap_ind)s where not exists (select 1 from %(uba_in)s where %(ecap_ind)s.up=%(uba_in)s.up and %(ecap_ind)s.uba=%(uba_in)s.uba and %(ecap_ind)s.tipus=%(uba_in)s.tipus)" % {'ecap_ind': ecap_ind,'uba_in':uba_in})
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % ecap_ind)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
      if baixa==1:
         ok=1
      else:
        c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,1 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in))

c.execute("drop table if exists %s" % metes)
c.execute("create table %s (indicador varchar(8) not null default '',mmin double,mint double,mmax double,unique (indicador,mmin,mmax))" % metes)
c.execute("drop table if exists %s" % metesOrig)
c.execute("create table %s(indicador varchar(8) not null default '',z1 varchar(10) not null default '',z2 varchar(10) not null default '',meta varchar(10) not null default '',z3 varchar(10) not null default '',z4 varchar(10) not null default '',z5 varchar(10) not null default '',z6 varchar(10) not null default '',valor double)" % metesOrig)
with open(pathmetes, 'rb') as file:
   m=csv.reader(file, delimiter='{', quotechar='|')
   for met in m:
      indicador,z1,z2,meta,z3,z4,z5,valor = met[0],met[1],met[2],met[3],met[4],met[5],met[6],met[7]
      c.execute("insert into %s Values('%s','%s','%s','%s','%s','%s','%s','',%s)" % (metesOrig,indicador,z1,z2,meta,z3,z4,z5,valor))
c.execute("insert into %s select a.indicador,a.valor/100,b.valor/100,c.valor/100 from (select indicador,valor from %s where meta='AGMMINRES') a inner join (select indicador,valor from %s where meta='AGMINTRES') b on a.indicador=b.indicador inner join (select indicador,valor from %s where meta='AGMMAXRES') c on a.indicador=c.indicador" % (metes,metesOrig,metesOrig,metesOrig))
c.execute('update {} set mmin = mmax * 0.9 where mmin > mmax * 0.9'.format(metes))
c.execute('update {} set mint = (mmax + mmin) / 2'.format(metes))
c.execute("update {} a inner join {} b on a.indicador = b.indicador set mmin_p = floor(mmin * detectats), mmax_p = floor(mmax * detectats)".format(ecap_ind, metes))
c.execute('update {} set mmin_p = mmin_p - 1 where mmin_p = mmax_p and mmax_p > 0'.format(ecap_ind))
c.execute("update {} set resolucioPerPunts=if(resolts>=mmax_p,1,if(resolts<mmin_p,0,(resolts-mmin_p)/(mmax_p-mmin_p)))".format(ecap_ind))
c.execute('update {} set mmin_p = mmin_p / detectats, mmax_p = mmax_p / detectats where detectats > 0'.format(ecap_ind))
c.execute("update {} a set llistat=if(indicador in {}, resolts, detectats-resolts)".format(ecap_ind, tuple(inversos)))
c.execute("update {} a set invers = 1 where indicador in {}".format(ecap_ind, tuple(inversos)))
c.execute("update %s a set resultatPerPunts=resolucioPerPunts*deteccioPerResultat" % (ecap_ind))

c.execute("drop table if exists %s" % ponderacio)
c.execute("create table %s (indicador varchar(8) not null default '',tipus varchar(1) not null default '',valor double,unique (indicador,tipus,valor))" % ponderacio)
with open(pathpond, 'rb') as file:
   pon=csv.reader(file, delimiter='@', quotechar='|')
   for pond in pon:
      indicador,mf,inf,esp= pond[0],pond[1],pond[2],pond[3]
      c.execute("insert into %s values('%s','G',%s)" % (ponderacio,indicador,mf))
      c.execute("insert into %s values('%s','L',%s)" % (ponderacio,indicador,inf))
      c.execute("insert into %s values('%s','E',%s)" % (ponderacio,indicador,esp))
c.execute("update %s a inner join %s b on a.indicador=b.indicador and a.tipus=b.tipus set punts=resolucioPerPunts*deteccioPerResultat*valor" % (ecap_ind,ponderacio))

c.execute("alter table %s add unique (up,uba,tipus,indicador)" % ecap_ind)


with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
      c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p  from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in))

createTable(khalix_uba, '(up varchar(5), uba varchar(10), tipus varchar(1), indicador varchar(10), analisis varchar(12), detalle varchar(12), valor double)', db, rm=True)
for analisis, variable, coeficient in dimensions:
    dades = (row for row in getAll("select up, uba, if(tipus = 'E', 'L', tipus), indicador, '{}', 'NOINSAT', round({} * {} , 6) from {}".format(analisis, variable, coeficient, ecap_ind), db))
    listToTable(dades, khalix_uba, db)

c.execute("drop table if exists %s,%s,%s,%s" % (cataleg,catalegPare,catalegPunts,catalegKhx))
c.execute("create table %s (indicador varchar(8),literal varchar(80),ordre int,pare varchar(8),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),pantalla varchar(50))" % cataleg)
c.execute("drop table if exists %s" % indic)
c.execute("create table %s (id int, indicador varchar(8), literal varchar(80),grup varchar(8),grup_desc varchar(80),baixa_ int, dbaixa varchar(80),llistats int,invers int,ordre int,grup_ordre int)" % indic)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
      if baixa==1:
         ok=1
      else:
         c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (indic,id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,invers,ordre,grup_ordre))
c.execute("insert into %s select distinct a.indicador indicador,literal,ordre,grup pare,llistats llistat,invers,0 mdet,if(mmin is null,0,mmin),if(mint is null,0,mint),if(mmax is null,0,mmax),1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',a.indicador) wiki,'GENERAL' pantalla from %s a left join %s b on a.indicador=b.indicador where baixa_=0" % (cataleg,indic,metes))
c.execute("create table %s (pare varchar(10) not null default '',literal varchar(80) not null default '',ordre int,proces varchar(50))" % catalegPare)
c.execute("insert into %s select distinct grup pare, grup_desc literal,grup_ordre ordre,'' proces from %s where baixa_=0" % (catalegPare,indic))
c.execute("create table %s like %s" % (catalegPunts,ponderacio))
c.execute("insert into %s select * from %s" % (catalegPunts,ponderacio))
c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s Values('%s','%s','%s','%s')" % (catalegKhx,i,desc,grup,grupdesc))     
        
#c.execute("update exp_ecap_catalegpunts set valor=0")
#c.execute("update exp_ecap_cataleg set mdet=0")

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")
