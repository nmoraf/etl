# -*- coding: latin1 -*-

"""
.
"""

import sisapUtils as u


TABLE = "ass_anticoncepcio"
DATABASE = 'ass'


class Anticoncepcio(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_denominador()
        self.get_numerador()
        self.get_indicador()
        self.upload_table()

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec from ass_poblacio \
               where edat between 15 and 55"
        self.poblacio = set([id for id, in u.getAll(sql, "nodrizas")])

    def get_denominador(self):
        """."""
        sql = "select id_cip_sec from ass_variables, dextraccio \
               where agrupador = 853 and \
                     val_num = 1 and \
                     dat between adddate(data_ext, interval -1 year) and data_ext"  # noqa
        self.denominador = set([id for id, in u.getAll(sql, "nodrizas")
                                if id in self.poblacio])

    def get_numerador(self):
        """."""
        self.numerador = {}
        sqls = {"EQA9222": "select id_cip_sec from eqa_variables, dextraccio \
                             where agrupador in (16, 17) and \
                                   data_var between adddate(data_ext, interval -1 year) and data_ext",  # noqa
                "EQA9224": "select id_cip_sec from eqa_tabac \
                            where tab in (0, 2) and last = 1"}
        for indicador, sql in sqls.items():
            self.numerador[indicador] = set([id for id,
                                             in u.getAll(sql, "nodrizas")
                                             if id in self.denominador])

    def get_indicador(self):
        """."""
        self.dades = []
        for indicador, pacients in self.numerador.items():
            self.dades.extend([(id, indicador, 1 * (id in pacients), 1)
                               for id in self.denominador])

    def upload_table(self):
        """."""
        u.createTable(TABLE, "(id_cip_sec int, indicador varchar(20), \
                               num int, den int)", DATABASE, rm=True)
        u.listToTable(self.dades, TABLE, DATABASE)


if __name__ == '__main__':
    Anticoncepcio()
