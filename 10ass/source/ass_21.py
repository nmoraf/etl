# coding: iso-8859-1

from sisapUtils import *
import time
from collections import defaultdict,Counter
from datetime import *


nod="nodrizas"
imp="import"
alt="ass"

file_name="EQA9221"
codi_indicador="EQA9221"

agr_cl_gon=(652,654,835,858)

#exclusions
exclusions=('O36.9','Z35','Z35.0','Z35.1','Z35.2','Z35.3','Z35.4','Z35.5','Z35.6','Z35.7','Z35.8','Z35.9','C01-O09.899','C01-O09.619','C01-O09.90','C01-O09.299','C01-O09.70','C01-O09.30','C01-O36.90X9','C01-O09.00','C01-O09.40','C01-O09.519','O06.9','O04.9','O03.9','C01-O03.9','C01-Z33.2','C01-O02.1')

validation=False
ups=('01422',)
#835

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]	

def get_embaras(current_date):
    """ Consigue para cada id de paciente un set de tuples con fechas de inicio y fin de embarazo. Los embarazos que se añaden son solo aquellos en los
        que hay mas de 3 meses entre inicio y fin y que han sido en el ultimo any (o la fecha de ini o la de fin son del ultimo any). Los ids se filtran
        segun si la paciente tenia 25 o menos de 25 anys en el inicio del embarazo.
        Devuelve un diccionario {id: set( tuple(ini embaras,fi embaras))}
    """
    id_embaras=defaultdict(set)
    ids_data_naix=get_ids_data_naix()

    sql="select id_cip_sec,inici,fi from {}.ass_embaras".format(nod)
    for id,inici,fi in getAll(sql,nod):
        if monthsBetween(inici, fi) > 3 and (monthsBetween(inici,current_date) <12 or monthsBetween(fi,current_date) <12):
            if id in ids_data_naix and yearsBetween(ids_data_naix[id],inici) < 25:
                id_embaras[id].add((inici,fi))
    return id_embaras

def get_ids_data_naix():
    """ Construye un diccionario: {id: fecha de nacimiento}
    """
    sql="select id_cip_sec,data_naix from {}.assignada_tot where sexe='D' and edat < 28".format(nod)
    return {id:data_naix for id,data_naix in getAll(sql,nod)}

def get_cla_gon(agrs):
    """ Consigue para cada id un set fechas en las que se ha realizado alguna de las pruebas indicadas por agrs. Filtra por fechas en el ultimo any.
        Devuelve un diccionario {id: set(fechas)}
    """
    sql="select id_cip_sec, dat from {}.ass_variables where agrupador in {}".format(nod,agrs)
    print(sql)
    id_data=defaultdict(set)
    for id,data in getAll(sql,nod):
        id_data[id].add(data)
    return id_data

def get_indicador (id_embaras,id_data,val=True):
    """ Construye las filas de la tabla final. Iterando sobre las ups de ass_imputacio_up, si el id tiene algun embarazo registrado en id_embaras,
        se itera sobre las los tuples de ini, fi en este diccionario y para cada uno se anyade 1 en el denominador. Si el id tiene fechas de pruebas, para 
        cada una de estas fechas se comprueba que esta entre inicio y el fin del embarazo en cuestion y se anyade uno en el numerador. Se corta el loop, ya que
        con que haya una prueba ya se da por valido.
        Devuelve una lista de filas (tuple) a nivel de up
    """
    if val:
        counter_1=0
        counter_2=0
    
    cumplidores_ids=defaultdict(set)
    no_cumplidores_ids=defaultdict(set)
    #excloses
    excloses_ids=defaultdict(set)
    dext=get_date_dextraccio()
    sql = "select id_cip_sec, pr_cod_ps, pr_dde from {}.problemes where pr_cod_ps in {}".format(imp, exclusions)
    for id, ps, dde in getAll(sql,imp):
        if id not in excloses_ids and monthsBetween(dde, dext)<12:
            excloses_ids[id].add(id)
    
    #excloses
    recomptes = {}
    uba_rows=[]
    indicador=defaultdict(Counter)
    sql = "select id_cip_sec, visi_up from {}.ass_imputacio_up".format(nod)
    for id, up in getAll(sql, nod):
        num, den = 0, 0
        #excloses
        if id in id_embaras and id not in excloses_ids:
            for inici, fi in id_embaras[id]:
                indicador[up]["DEN"]+=1
                den = 1
                if id in id_data:
                    for data in id_data[id]:
                        if inici < data <= fi:
                            indicador[up]["NUM"]+=1
                            num = 1
                            if val and counter_1 < 10 and up in ups:
                                cumplidores_ids[up].add(id)
                                counter_1+=1
                            break
                    
                else:
                    if val and counter_2 < 10 and up in ups:
                        no_cumplidores_ids[up].add(id)
                        counter_2+=1
                recomptes[(id)] = {'num': num, 'den': den}
                
                
    for (id), n in recomptes.items():
        uba_rows.append((id, n['num'], n['den']))
        
    return uba_rows,no_cumplidores_ids,cumplidores_ids

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)


def push_to_khalix(taula,taula_centres,file_name,khalix=False):
    query_string="select indicador,concat('A','periodo'),ics_codi,{var},'NOCAT','NOIMP','DIM6SET','N',{tipus} from altres.{taula} a inner join nodrizas.{taula_centres} b on a.up=scs_codi where a.{tipus} != 0"
    query_final=" union ".join([query_string.format(taula=taula,taula_centres=taula_centres,var=var,tipus=tipus) for (var,tipus) in zip(["'NUM'","'DEN'"],["numerador","denominador"])])
    print(query_string)
    if khalix:
        exportKhalix(query_final,file_name)

def get_up_desc(up):
    sql="select up_desc from ass_centres where up={};".format(up)
    return getOne(sql,"nodrizas")[0]

def get_hashd_dict():
    sql="select id_cip_sec,hash_d from import.u11;"
    return {id:hashd for (id,hashd) in getAll(sql,"import")}	

def get_cip_dict(hashd):
    sql="select usua_cip,usua_cip_cod from pdptb101 where usua_cip_cod='{}'".format(hashd)
    print(sql)
    return getOne(sql,"pdp")[0]

def write(filename,dict_ids,hash_dict):
    f=open(filename,"w")
    for up in dict_ids:
        up_desc=get_up_desc(up)
        for id in dict_ids[up]:
            print(hash_dict[id])
            cip=get_cip_dict(hash_dict[id])
            printTime("cip")
            f.write("{}\t{}\n".format(up_desc,cip))
    f.close()
if __name__ == '__main__':
    printTime('inici')
    current_date=get_date_dextraccio()
    printTime(current_date)
    if validation:
        current_date=date.today()
    printTime(current_date)
    
    id_data=get_cla_gon(agr_cl_gon)
    printTime('id_data {}'.format(len(id_data)))

    id_embaras=get_embaras(current_date)
    printTime('id_embaras {}'.format(len(id_embaras)))
    

    rows,no_cumplidores_ids,cumplidores_ids=get_indicador(id_embaras,id_data)
    
    table_name="ass_indicador_{}".format(codi_indicador)
    columns="(id_cip_sec int, num int, den int)"
    export_table(table_name,columns,alt,rows)
    printTime("Exxxxported!")
    """
    push_to_khalix(table_name,"cat_centres",file_name)
    """
    if validation:
    
        hash_id_dict=get_hashd_dict()
        printTime("hash")
        #cip_hash_dict=get_cip_dict()
        #printTime("cip")

       
        for filename,dict_ids in zip(["{}_{}.txt".format(codi_indicador,cump) for cump in ["no_cumplidores","cumplidores"]], [no_cumplidores_ids,cumplidores_ids]):
            write(filename,dict_ids,hash_id_dict)