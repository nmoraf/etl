# -*- coding: latin1 -*-

"""
.
"""

import sisapUtils as u
import collections as c
from datetime import datetime
import dateutil.relativedelta as r




INDICADOR = 'EQA9226'
TABLE="ass_indicador_{}".format(INDICADOR)
DATABASE = 'ass'



class EQA9226(object):
    """."""

    def __init__(self):
        """."""
        self.get_dextraccio()
        self.get_centres()
        self.get_imputacioup()
        self.get_embaras()
        self.get_denominador()
        self.get_numerador()
        self.get_indicador()
        self.upload_table()
        self.get_recompte()
        #self.get_exportcsv()
        #self.export_csv()


    def get_dextraccio(self):
        """ . """
        db = "nodrizas"
        sql="select data_ext from dextraccio"
        for data_ext in u.getOne(sql,db):
            self.dextraccio = data_ext
        print(self.dextraccio)

    def get_centres(self):
        """."""
        self.centres = {}
        select = "select up, up_desc, assir, up_assir from ass_centres"
        bd = "nodrizas"
        for up, up_desc, assir_desc, assir in u.getAll(select, bd):
            self.centres[up]={'up_desc': up_desc,'assir': assir,'assir_desc': assir_desc}
        #print(self.centres)
        print("N UP ass_centres:", len(self.centres))

    def get_imputacioup(self):
        """."""
        self.imputacioup = {}
        select = "select id_cip_sec, visi_up from ass_imputacio_up"
        bd = "nodrizas"
        for id, up in u.getAll(select, bd):
            if up in self.centres:
                self.imputacioup[id]={'up': up}
        print("N Pacients",len(self.imputacioup))

    def get_embaras(self):
        """ . """
        self.embaras = c.defaultdict(set)
        db = "nodrizas"
        sql="select id_cip_sec, inici, fi from {}.ass_embaras".format(db)
        for id, inici, fi in u.getAll(sql,db):
            if id in self.imputacioup:
                if fi > self.dextraccio - r.relativedelta(years=1):
                    self.embaras[id].add((inici,fi))
        print("embaras:", len(self.embaras))

    def get_denominador(self):
        """."""
        self.denominador = {}
        select = "select id_cip_sec, dat from ass_variables, dextraccio \
                  where agrupador in (861) \
                  and val_num =  1 \
                  and dat between adddate(data_ext, interval -1 year) and data_ext" # noqa
        bd = "nodrizas"
        for id, dat in u.getAll(select,bd):
            if id in self.embaras:
                for inici, fi in self.embaras[id]:
                    if inici <= dat <= fi:
                        self.denominador[id]=True
        print("den:", len(self.denominador))

    def get_numerador(self):
        """."""
        self.numerador = {}
        sql = "select id_cip_sec, dat from ass_variables, dextraccio \
               where agrupador in (862) \
               and dat between adddate(data_ext, interval -1 year) and data_ext" # noqa
        for id, dat in u.getAll(sql, "nodrizas"):
            if id in self.embaras:
                if id in self.denominador:
                    for inici, fi in self.embaras[id]:
                        if inici <= dat <= fi:
                            self.numerador[id]=True
        print("num:", len(self.numerador))

    def get_indicador(self):
        """."""
        self.dades = []
        for id in self.denominador:
            den = 1
            num = 1 if id in self.numerador else None
            self.dades.append((id, INDICADOR, num, den))
        print("dades:", len(self.dades))
        print("dades first element list:",self.dades[0])

    def upload_table(self):
        """."""
        u.createTable(TABLE, "(id_cip_sec int, \
                               indicador varchar(20), \
                               num int, den int)", DATABASE, rm=True)
        u.listToTable(self.dades, TABLE, DATABASE)

    def get_recompte(self):
        """."""
        self.recompte = c.Counter()
        for id in self.denominador:
            up = self.imputacioup[id]['up']
            up_desc = self.centres[up]['up_desc']
            assir = self.centres[up]['assir']
            assir_desc = self.centres[up]['assir_desc']
            self.recompte[INDICADOR, up, up_desc, assir, assir_desc, 'DEN'] += 1
            if id in self.numerador:
                self.recompte[INDICADOR, up, up_desc, assir, assir_desc, 'NUM'] += 1
        #print(self.recompte)

    def get_exportcsv(self):
        """."""
        self.dadescsv = []
        for (indicador, up, up_desc, assir, assir_desc, tip), rec in self.recompte.items():
            self.dadescsv.append((indicador, up, up_desc, assir, assir_desc, tip, rec))
        print("Ejemplo dadescsv", self.dadescsv[0])
        print("N registros dadescsv:", len(self.dadescsv))

    def export_csv(self):
        """."""
        u.writeCSV(u.tempFolder + 'EQA9226ASSIR.csv', self.dadescsv)





if __name__ == '__main__':
    EQA9226()
