use ass;
#Copiem proc�s antic, no

set @fa1any = (select date_add(date_add(data_ext, interval -1 year), interval +1 day) from nodrizas.dextraccio);
set @fa2any = (select date_add(date_add(data_ext, interval -2 year), interval +1 day) from nodrizas.dextraccio);
set @fa6any = (select date_add(date_add(data_ext, interval -6 year), interval +1 day) from nodrizas.dextraccio);
set @en6mesos = (select date_add(date_add(data_ext, interval + 6 month), interval +1 day) from nodrizas.dextraccio);
set @en7mesos = (select date_add(date_add(data_ext, interval + 7 month), interval +1 day) from nodrizas.dextraccio);
set @fa18mesos = (select date_add(date_add(data_ext, interval - 18 month), interval +1 day) from nodrizas.dextraccio);
set @fa6mesos = (select date_add(date_add(data_ext, interval - 6 month), interval +1 day) from nodrizas.dextraccio);
set @avui = (select data_ext from nodrizas.dextraccio);

#EQA9210: Prevenci� dels embarassos no desitjats

insert into indicador_EQA9210_1
select
   id_cip_sec,dde dat,0 num, 1 den,0 excl from nodrizas.eqa_problemes
where 
   ps in ('404','405','702') and dde between @fa1any and @avui
;

insert into indicador_EQA9210_1
select
	id_cip_sec,dat,0 num, 1 den,0 excl from nodrizas.ass_variables 
where 
   agrupador in ('393', '836')
;

alter table indicador_EQA9210_1 add index(id_cip_sec,dat,num)
;

update indicador_EQA9210_1 g inner join nodrizas.ass_variables v on g.id_cip_sec=v.id_cip_sec
set num=1 where agrupador=390  and v.dat between date_add(g.dat,interval - 0 month) and date_add(g.dat,interval + 3 month) 
;

insert into indicador_EQA9210
select id_cip_sec, max(num) num , den,excl  from indicador_EQA9210_1 group by id_cip_sec
;

drop table if exists indicador_EQA9210_1;


#EQA9211: Registre del m�tode anticonceptiu

drop table if exists indicador_EQA9211_1;

insert into indicador_EQA9211
select 
   id_cip_sec,0 num, 1 den, 0 excl from nodrizas.ass_poblacio
where 
   edat between 13 and 55
   and id_cip_sec not in (select id_cip_sec from nodrizas.eqa_problemes where ps = 39 and incident = 1)
;

alter table indicador_EQA9211 add index(id_cip_sec,num,excl)
;

update indicador_EQA9211 a inner join ass_embaras b on a.id_cip_sec=b.id_cip_sec
set excl=1
;

update indicador_EQA9211 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=390
;

update indicador_EQA9211 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador=490 and val_txt like ('Mai%')
;

update indicador_EQA9211 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador=818 and val_num in (2, 0)
;

update indicador_EQA9211 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador=819
;

update indicador_EQA9211 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in (78, 493, 852)
;


#EQA9212: Criteri cl�nic en mamografies fora del programa de cribatge

drop table if exists mamogr;
create table mamogr(
id_cip_sec int,
dat date,
index(id_cip_sec, dat))
;

insert into mamogr
select 
   id_cip_sec, dat from nodrizas.ass_variables b 
where 
   agrupador=394 and val_num=1 and dat between @fa1any and @avui
;

insert into mamogr
select 
   id_cip_sec, dat from nodrizas.ass_proves b 
where 
   agrupador=394 and dat between @fa1any and @avui
;

insert into indicador_EQA9212_1
select 
   id_cip_sec,max(dat),0 num, 1 den, 0 excl from mamogr b 
group by
   id_cip_sec
;

alter table indicador_EQA9212_1 add index(id_cip_sec,dat,num,den,excl)
;

update indicador_EQA9212_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador in ('395','396') and a.dat>=b.dat and (val_num > 0 or left(val_txt,5)<>'Sense')
;

update indicador_EQA9212_1 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set num=1 where ps=411
;

update indicador_EQA9212_1 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps=560
;

insert into indicador_EQA9212
select id_cip_sec,0 num, 0 den, 0 excl from nodrizas.ass_poblacio
where edat<50 or edat>70
;

update indicador_EQA9212 a inner join indicador_EQA9212_1 b on a.id_cip_sec=b.id_cip_sec
set a.den=b.den
,a.num=b.num
,a.excl=b.excl
;
delete a.* from indicador_EQA9212 a where den=0;

drop table if exists indicador_EQA9212_1
;

#EQA9213: Registre dels factors de risc de c�ncer de mama

drop table if exists indicador_EQA9213_1
;

insert into indicador_EQA9213
select id_cip_sec, 0 num, 1 den, 0 excl from nodrizas.ass_poblacio
where (edat between  35 and 49) or (edat between 70 and 74)
;

alter table indicador_EQA9213 add index(id_cip_sec,num,den,excl)
;

update indicador_EQA9213 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=396
;

update indicador_EQA9213 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in ('560')
;

#EQA9214: Demanar serologia VIH davant nous diagn�stics ITS



insert into indicador_EQA9214_1
select
   id_cip_sec,dde dat,0 num, 1 den,0 excl from nodrizas.eqa_problemes_incid
where 
   ps in ('406','680') and dde between @fa1any and @avui
;


alter table indicador_EQA9214_1 add index(id_cip_sec,num,excl)
;

update indicador_EQA9214_1 a inner join nodrizas.ass_embaras b on a.id_cip_sec=b.id_cip_sec
set excl=1
;

update indicador_EQA9214_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=397 and b.dat between @fa2any and @avui
;

update indicador_EQA9214_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=398 and val_num=1
;

update indicador_EQA9214_1 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set num=1 where ps=101 
;

update indicador_EQA9214_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=388 and b.dat between @fa1any and @avui
;

insert into indicador_EQA9214
select id_cip_sec,0 num, 0 den, 0 excl from nodrizas.ass_poblacio
;

update indicador_EQA9214 a inner join indicador_EQA9214_1 b on a.id_cip_sec=b.id_cip_sec
set a.den=b.den
,a.num=b.num
,a.excl=b.excl
;
delete a.* from indicador_EQA9214 a where den=0;

drop table if exists indicador_EQA9214_1
;

#EQA9215: Cribratge del c�ncer de c�rvix segons protocol

drop table if exists indicador_EQA9215_1;

insert into indicador_EQA9215
select id_cip_sec,0 num, 1 den, 0 excl from nodrizas.ass_poblacio
where edat between 40 and 64
;

alter table indicador_EQA9215 add index (id_cip_sec,num,excl)
;

delete a.* from indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
where agrupador in (203,440) and  STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%d-%M-%Y') between @fa6any and @fa1any
;

delete a.* from indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
where agrupador in (204,202,400) and dat between @fa6any and @fa1any
;

delete a.* from indicador_EQA9215 a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
where agrupador in (400) and dat between @fa6any and @fa1any
;

drop table if exists excloure_9215;
create table excloure_9215(id_cip_sec int);
insert into excloure_9215 select id_cip_sec from nodrizas.ass_variables where agrupador=203 or (agrupador=202 AND VAL_num=1);
alter table excloure_9215 add index(id_cip_sec);

delete a.* from indicador_EQA9215 a where a.id_cip_sec not in (select id_cip_sec from excloure_9215)
;

drop table if exists excloure_9215;

update indicador_EQA9215 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in ('407','491','492','493','852','494','409','495','521','522')
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador = 696 and val_txt = 'Mai RS' and dat between @fa1any and @avui
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador=818 and val_num in (2, 0)
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador in (203,440) and  STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%d-%M-%Y') between @fa1any and @avui
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador in (204,400) and val_num=1 and dat between @fa1any and @avui
;

update indicador_EQA9215 a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador in (400) and dat between @fa1any and @avui
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=205  and STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%d-%M-%Y') between @avui and @en6mesos
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=num+1 where agrupador=201 and val_num=1 and dat between @fa1any and @avui
;

update indicador_EQA9215 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=num+1 where agrupador=402 and dat between @fa1any and @avui
;

update indicador_EQA9215 a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set num=2 where agrupador in (596) and dat between @fa1any and @avui
;

update indicador_EQA9215
set num=if(num>=2,1,0)
;


#EQA9216: Seguiment adequat de displ�sia lleu de c�rvix



insert into indicador_EQA9216_1
select
   id_cip_sec,min(dde) dat,0 num, 1 den,0 excl from nodrizas.eqa_problemes
where 
   ps in ('407') and dde between @fa18mesos and @fa6mesos group by id_cip_sec
;

insert into indicador_EQA9216_1
select id_cip_sec,dat,0 num,1 den, 0 excl from nodrizas.ass_variables where agrupador=400 and val_num=5 and dat between  @fa18mesos and @fa6mesos
;

drop table if exists citopat;
create table citopat (
id_cip_sec int,
dat date,
post int,
index(id_cip_sec, dat)
)
select
  id_cip_sec
  ,min(dat) as dat
  ,0 as post
from
  nodrizas.ass_variables
where
   ((agrupador=400 and val_num=5) or (agrupador=864 and val_num=9)) and dat between @fa18mesos and @avui
group by 
    id_cip_sec
;

insert into citopat
select
   id_cip_sec, min(dde) as dat,0 as post from nodrizas.eqa_problemes
where 
   ps in ('407') and dde between @fa18mesos and @avui 
group by id_cip_sec
;

update citopat a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set post =1 where agrupador = 400 and b.dat> a. dat
;

alter table indicador_EQA9216_1 add index(id_cip_sec,dat,num,excl)
;
#Per acord amb �lex Allepuz hem de treure els que tenen dos resultats negatius despr�s del diagn�stic i abans del per�ode avaluaci�

drop table if exists cito7;
create table cito7 (
id_cip_sec int,
dat date,
cuenta double
)
select
  id_cip_sec
  ,max(dat) as dat
  ,count(*) as cuenta
from
  nodrizas.ass_variables
 where
  agrupador=400 and val_num in ('0','1') and dat<@fa18mesos
 group by
 id_cip_sec
;
  
delete a.* from cito7 a where cuenta<2
;
  
update  indicador_EQA9216_1 a inner join cito7 b on a.id_cip_sec=b.id_cip_sec
 set excl=1 where a.dat<b.dat
;

update  indicador_EQA9216_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=399 and val_num=1 and b.dat between @fa18mesos and @avui
;

update  indicador_EQA9216_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=401 and b.dat between @fa18mesos and @avui
;

update  indicador_EQA9216_1 a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=401 and b.dat between @fa18mesos and @avui
;

update indicador_EQA9216_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=204 and val_num=1 and b.dat between @fa18mesos and @avui
;
update indicador_EQA9216_1 a inner join citopat b on a.id_cip_sec=b.id_cip_sec
set num=1 where post = 1
;
update indicador_EQA9216_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=205  and STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%Y-%M-%d') between @avui and @en7mesos
;

insert into indicador_EQA9216
select id_cip_sec,max(num),den,max(excl) from indicador_EQA9216_1 group by id_cip_sec
;

drop table if exists indicador_EQA9216_1
;

#EQA9217: Seguiment adequat ASCUS i ASCH positius

drop table if exists ASCUS;
CREATE TABLE ASCUS (
  id_cip_sec int,
  flagvph double null,
  datvph date,
  resultatvph double null,
  flagcolpo double null,
  flag double null,
  excl_cito double null,
  excl_ascus double null
)
SELECT
	id_cip_sec,0 as flagvph, 0 as resultatvph,0 as flagcolpo, 0 as flag, 0 as excl_cito, 0 as excl_ascus from nodrizas.ass_variables where agrupador=400 and val_num in ('3','9') and dat between @fa1any and @avui
;

alter table ascus add index(id_cip_sec,flagvph,resultatvph,flagcolpo,flag)
;

update ASCUS a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set flagvph=1,
resultatvph=1 where ps=408
;

update ASCUS a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagvph=1,
datvph = dat where agrupador=402 and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set resultatvph=1 where agrupador=402 and val_num between 2 and 3 and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador = '399' and val_num=1 and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador ='401' and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador in ('401') and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador=496  and b.dat between @fa1any and @avui
;

-- si resultat vph negatiu i colpo anterior no comptar com a colpo feta

update ASCUS a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=0 where resultatvph=0 and dat < datvph and agrupador = '399' and val_num=1 and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=0 where resultatvph=0 and dat < datvph and agrupador ='401' and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=0 where resultatvph=0 and dat < datvph and agrupador in ('401') and  dat between @fa1any and @avui
;

update ASCUS a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=0 where resultatvph=0 and dat < datvph and agrupador=496  and b.dat between @fa1any and @avui
;



update ASCUS a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl_cito=1 where ps in ('494','409','495')
;

update ASCUS
set flag=1 where flagvph=1 and resultatvph=1 and flagcolpo=1
;

update ASCUS
set flag=1 where flagvph=1 and resultatvph=0 and flagcolpo=0
;

update ASCUS
set flag=1 where flagvph=1 and resultatvph=0 and flagcolpo=1 and excl_cito=1
;


drop table if exists ASCH;
CREATE TABLE ASCH (
  id_cip_sec int,
  flagvph double null,
  flagcolpo double null,
  exclvph int,
  flag double null
)
SELECT
	id_cip_sec,0 as flagvph, 0 as resultatvph,0 as flagcolpo, 0 as exclvph, 0 as flag
from nodrizas.ass_variables where agrupador=400 and val_num =4 and dat between @fa1any and @avui
;

alter table asch add index(id_cip_sec,flagvph,flagcolpo,flag)
;


update ASCH a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagvph=1 where agrupador=402 and  dat between @fa1any and @avui
;

create or replace table excl_vph as
select id_cip_sec
from nodrizas.ass_variables
where agrupador = 400 and dat between @fa6any and @fa1any
;

update ASCH set exclvph = 1 where id_cip_sec not in (select id_cip_sec from excl_vph)
;

update ASCH a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador in ('399','204') and val_num=1 and  dat between @fa1any and @avui
;

update ASCH a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador = '401' and dat between @fa1any and @avui
;

update ASCH a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador in ('401') and  dat between @fa1any and @avui
;

update ASCH a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set flagcolpo=1 where agrupador=496  and b.dat between @fa1any and @avui
;

update ASCH
set flag=1 where (flagvph=0 or exclvph=1) and flagcolpo=1
;

insert into indicador_EQA9217_1 
select id_cip_sec,0 dat, flag as num,1 den,excl_ascus excl from ASCUS;

insert into indicador_EQA9217_1 
select id_cip_sec,0 dat, flag as num,1 den,0 excl from ASCh;

update indicador_EQA9217_1 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in ('407','491','492', 493, 852);

insert into indicador_EQA9217
SELECT a.id_cip_sec,min(num), den, excl from indicador_EQA9217_1 a inner join nodrizas.ass_poblacio b on a.id_cip_sec=b.id_cip_sec
where edat >24 group by id_cip_sec
;

drop table if exists indicador_EQA9217_1;

#EQAASSIR9218: Control ecogr�fic en metrorr�gia postmenop�usica
  
insert into indicador_EQA9218_1
select
   id_cip_sec,dde dat,0 num, 1 den,0 excl from nodrizas.eqa_problemes
where 
   ps in ('409','410') and dde between @fa1any and @avui
;

alter table indicador_EQA9218_1 add index(id_cip_sec,dat,num)
;

update indicador_EQA9218_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=403  and b.dat between a.dat and date_add(a.dat,interval + 15 day) 
;

update indicador_EQA9218_1 a inner join nodrizas.ass_derivacions b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=437  and b.dat between a.dat and date_add(a.dat,interval + 15 day) 
;

update indicador_EQA9218_1 a inner join nodrizas.ass_proves b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=438  and b.dat between a.dat and date_add(a.dat,interval + 15 day) 
;

update indicador_EQA9218_1 a inner join nodrizas.ass_ecos b on a.id_cip_sec=b.id_cip_sec
set num=1 where b.ecg_data_alta between a.dat and date_add(a.dat,interval + 15 day) 
;

update indicador_EQA9218_1 a inner join nodrizas.eqa_xml b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=438  and b.dat between a.dat and date_add(a.dat,interval + 15 day) 
;

update indicador_EQA9218_1 a inner join nodrizas.ass_variables b on a.id_cip_sec=b.id_cip_sec
set num=1 where agrupador=820  and b.dat between a.dat and date_add(a.dat,interval + 45 day) 
;

update indicador_EQA9218_1 a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in (493, 852)
;


insert into indicador_EQA9218
select id_cip_sec, max(num) num , max(den),max(excl)  from indicador_EQA9218_1 group by id_cip_sec
;

drop table if exists indicador_EQA9218_1;
