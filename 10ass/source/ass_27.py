# -*- coding: latin1 -*-

"""
.
"""

import sisapUtils as u
import collections as c
import dateutil.relativedelta as r
from datetime import datetime as d


INDICADOR = "EQA9227"
TABLE = "ass_indicador_{}".format(INDICADOR)
DATABASE = 'ass'

class EQA9227(object):
    """."""
    
    
    def __init__(self):
        """."""

        self.get_dextraccio()
        self.get_embaras()

        ts = d.now()
        self.get_problemes()
        print('Time execution EXC DX: time {} / nrow {}'.format(d.now() - ts, len(self.exc_dx)))

        self.get_visites()
        self.get_exclusions()
        self.get_denominador()
        self.get_numerador()
        self.get_indicador()
        self.export_to_table()


    def get_dextraccio(self):
        """ . """
        sql = "select data_ext from dextraccio"
        for dat in u.getOne(sql, "nodrizas"):
            self.dextraccio = dat
        print("data extraccio:", self.dextraccio)

    def get_embaras(self):
        """ . """
        self.embaras = c.defaultdict(set)
        sql = "select id_cip_sec, inici, fi from ass_embaras"
        for id, inici, fi in u.getAll(sql, "nodrizas"):
            if fi > self.dextraccio - r.relativedelta(years=1):
                self.embaras[id].add((inici,fi))
        print("embaras: ", len(self.embaras))
        print(self.dextraccio - r.relativedelta(years=1))

    def get_problemes(self):
        """ . """
        self.exc_dx = {}
        sql = "select id_cip_sec, pr_dde \
               from problemes \
               where pr_cod_o_ps='C' \
               and pr_cod_ps in ('O06.9','O03.9','O04.9','C01-O03.9')"
        for id in u.getAll(sql, "import"):
            if id in self.embaras:
                self.exc_dx[id]=True
        print("exc dx: ", len(self.exc_dx))

    def get_visites(self):
        """ . """
        self.exc_vis = {}
        self.visitesin = {}
        self.visitesnotin = {}
        sql = "select id_cip_sec, visi_data_visita, visi_tipus_visita from ass_visites"
        for id, dat, tip in u.getAll(sql,"nodrizas"):
            if id in self.embaras:
                for inici, fi in self.embaras[id]:
                    if inici <= dat <= fi and tip in ('VU','U','E','ES','TN','GR','GM'):
                        self.visitesin[id]=True
                    if inici <= dat <= fi and tip not in ('VU','U','E','ES','TN','GR','GM'):
                        self.visitesnotin[id]=True
        for id in self.visitesin:
            if id not in self.visitesnotin:
                self.exc_vis[id]=True
        print("visites exc:", len(self.visitesin))
        print("visites no exc:", len(self.visitesnotin))
        print("visites:", len(self.exc_vis))

    def get_exclusions(self):
        """ . """
        self.exclusions = {}
        for id in self.embaras:
            if id in self.exc_dx or id in self.exc_vis:
                self.exclusions[id]=True
        print("N?mero de pacients exclosos:", len(self.exclusions))

    def get_denominador(self):
        """ . """
        self.denominador = {}
        sql = "select id_cip_sec, dat from ass_variables \
               where agrupador in (860) \
                     and val_num in (1)"
        for id, dat in u.getAll(sql, "nodrizas"):
            if id in self.embaras:
                for inici, fi in self.embaras[id]:
                    if inici <= dat <= fi:
                        self.denominador[id]=True
        print("den :", len(self.denominador))

    def get_numerador(self):
        """ . """
        
        self.numerador = {}
        sql = "select id_cip_sec, dat from ass_variables \
               where agrupador in (863)"
        for id, dat in u.getAll(sql, "nodrizas"):
            if id in self.denominador:
                if id in self.embaras:
                    for inici, fi in self.embaras[id]:
                        if inici <= dat <= fi:
                            self.numerador[id]=True
        print("num : ", len(self.numerador))

    def get_indicador(self):
        """ . """
        self.dades=[]
        for id in self.denominador:
            if id not in self.exclusions:
                den=1
                num=1 if id in self.numerador else None
                self.dades.append((id,INDICADOR,num,den))
        print("N pacients INDICADOR (den):", len(self.dades))
        print("Exemple pacient (first):", self.dades[0])

    def export_to_table(self):
        """ . """
        u.createTable(TABLE, "(id_cip_sec int, \
                               indicador varchar(20), \
                               num int, den int)", DATABASE, rm=True)
        u.listToTable(self.dades, TABLE, DATABASE)





if __name__ == '__main__':
    EQA9227()