# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict
import sys
import datetime
from time import strftime

printTime('Inici')

# db

ass = 'ass'
nod = 'nodrizas'

# tables
dext = 'dextraccio'
embaras = 'ass_embaras'
tabac = 'eqa_tabac'
variables = 'ass_variables'

OutFile = tempFolder + 'ass_tabac.txt'
TableMy = 'ass_tabac'

indicador = 'EQA9219'

# taules previes
sql = "select date_add(data_ext,interval - 12 month),data_ext from {}" .format(dext)
for f, a in getAll(sql, nod):
    fa1any = f
    ara = a

emb = {}
sql = 'select id_cip_sec, inici, fi from {}'.format(embaras)
for id, inici, fi in getAll(sql, nod):
    if inici > fa1any or fi > fa1any:
        emb[id] = {'inici': inici, 'fi': fi}

fuma, nofuma = {}, {}
sql = 'select id_cip_sec, tab, dalta, dbaixa from {}'.format(tabac)
for id, tab, alta, baixa in getAll(sql, nod):
    try:
        inicie = emb[id]['inici']
    except KeyError:
        continue
    if alta <= inicie <= baixa:
        if tab == 1:
            fuma[id] = {'den': 1}
    if inicie <= alta:
        if tab != 1:
            nofuma[id] = True
mindat = {}
sql = 'select id_cip_sec, val_txt, dat from {0} where agrupador = {1}'.format(variables, 385)
for id, txt, dat in getAll(sql, nod):
    try:
        inicie = emb[id]['inici']
    except KeyError:
        continue
    if inicie <= dat:
        if txt == 'NO':
            nofuma[id] = True
    if dat < inicie:
        if id in mindat:
            dat0 = mindat[id]['data']
            if dat0 < dat:
                mindat[id]['data'] = dat
                mindat[id]['txt'] = txt
        else:
            mindat[id] = {'data': dat, 'txt': txt}

for id, d in mindat.items():
    if d['txt'] == 'SI':
        if id in fuma:
            continue
        else:
            fuma[id] = {'den': 1}

sql = 'select id_cip_sec, val_txt, dat from {0} where agrupador = {1}'.format(variables, 444)
for id, txt, dat in getAll(sql, nod):
    try:
        inicie = emb[id]['inici']
    except KeyError:
        continue
    if inicie <= dat:
        if txt == 'SI':
            nofuma[id] = True

with openCSV(OutFile) as c:
    for (id), d in fuma.items():
        den = d['den']
        if id in nofuma:
            num = 1
        else:
            num = 0
        c.writerow([id, indicador, num, den])
execute("drop table if exists {}".format(TableMy), ass)
execute("create table {} (id_cip_sec int, indicador varchar(20), num double, den double)".format(TableMy), ass)
loadData(OutFile, TableMy, ass)
execute("alter table {} add index(id_cip_sec)".format(TableMy), ass)

printTime('fi')
