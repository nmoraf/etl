from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="ass"
conn = connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/ass_relacio.txt"
den="indicador"

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="d":
         if agr=="z":
            t="%s_%s" % (den,ind)
            t2="%s_%s_1" % (den,ind)
            c.execute("drop table if exists %s" % t)
            c.execute("create table %s (id_cip_sec int,num double,den double,excl double)" % t)
            c.execute("drop table if exists %s" % t2)
            c.execute("create table %s (id_cip_sec int,dat date, num double,den double,excl double)" % t2)

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")
