from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="ass"
conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()

path="./dades_noesb/ass_relacio.txt"

ass_emb="nodrizas.ass_embaras"
criteris="ass_criteris"
embaras="ass_embaras"
variables="nodrizas.ass_variables"
vacunes="nodrizas.eqa_vacunes"
dext="nodrizas.dextraccio"
odn="nodrizas.odn_variables"

#indicador tensio arterial es una mica diferent. El marco per tractar de forma especifica
ta="EQA9209"

c.execute("select date_add(data_ext,interval - 12 month),data_ext from %s" % (dext))
fa1any,avui,=c.fetchone()

c.execute("drop table if exists %s" % embaras)
c.execute("create table %s like %s" % (embaras,ass_emb))
c.execute("insert into %s select * from %s where inici>'%s' or fi >'%s'"  % (embaras,ass_emb,fa1any,fa1any))
c.execute("alter table %s add index(id_cip_sec,inici,fi,risc)" % embaras)


with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="e":
         c.execute("alter table %s add column %s int" % (embaras,ind))
         c.execute("update %s set %s=0" % (embaras,ind))
      elif tipus=="num_e":
         if agr=="risc":
            c.execute("update %s set %s=1 where risc<>''" % (embaras,ind))
         else:
            c.execute("select taula from %s where agrupador='%s' " % (criteris,agr))
            pres=c.fetchall()
            for a in pres:
               taula= a[0]
               if vmin:
                  if vmin.isalpha():
                     c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and val_txt='%s' and (dat between date_add(inici,interval - %s month) and date_add(fi,interval + %s month))" % (embaras,variables,ind,agr,vmin,tmin,tmax))
                  else:
                     if ind==ta and taula=="variables":
                        c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=%s+1 where agrupador='%s' and val_num between %s and %s and (dat between date_add(inici,interval + %s week) and date_add(fi,interval + %s week))" %(embaras,variables,ind,ind,agr,vmin,vmax,tmin,tmax))
                     else:
                        c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and val_num between %s and %s and (dat between date_add(inici,interval - %s month) and date_add(fi,interval + %s month))" % (embaras,variables,ind,agr,vmin,vmax,tmin,tmax))   
               else:
                  if taula=="variables" or taula == "laboratori":
                     c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and dat between inici and fi" % (embaras,variables,ind,agr))
                  elif taula=="vacunes":
                     c.execute("select date_add(date_add(data_ext, interval - '%s' year), interval +1 day),date_add(date_add(data_ext, interval - '%s' year), interval +1 day) from %s" % (tmin,tmax,dext))
                     d=c.fetchone()
                     fatemps,ara = d[0],d[1]
                     if agr == '49':
                        c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and datamax between '%s' and '%s'" % (embaras,vacunes,ind,agr,fatemps,ara))
                     elif agr == '631':
                        c.execute("update {} a inner join {} b on a.id_cip_sec = b.id_cip_sec set {} = 1 where agrupador = {} and datamax between inici and fi".format(embaras, vacunes, ind, agr))
                  elif taula.startswith("odn"):
                     c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and dat between inici and fi" % (embaras,odn,ind,agr))

c.execute("update %s set %s=if(%s=2,1,0)" % (embaras,ta,ta))

                     
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="etancat":
         c.execute("drop table if exists %s" % ind)
         c.execute("create table %s (id_cip_sec int, %s double)" % (ind,ind))
         c.execute("insert into %s select id_cip_sec, %s from %s where inici < date_add('%s',interval - 24 week) and temps >= 168" % (ind,ind,embaras,avui))
         

              
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")