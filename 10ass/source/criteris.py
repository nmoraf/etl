from sisapUtils import *

path="./dades_noesb/ass_relacio.txt"
agrupadors= {}
master= "select taula from {} where agrupador='{}'"
loc= 'ass_criteris_var'
glob= 'eqa_criteris'
new= 'ass_criteris'

db= 'nodrizas'
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind,tipus,agr,f,tmin,tmax,vmin,vmax in p:
        try:
            taula,= getOne(master.format(loc,agr),db)
            taula= 'variables'
        except:
            try:
                taula,= getOne(master.format(glob,agr),db)
            except:
                continue
        agrupadors[agr]= taula
        
db= 'ass'
execute('drop table if exists {}'.format(new),db)
execute('create table {} (agrupador int,taula varchar(50))'.format(new),db)
for agr,taula in agrupadors.items():
    execute("insert into {} values ({},'{}')".format(new,agr,taula),db)