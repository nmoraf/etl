from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random

nod="nodrizas"
imp="import"
validation=False
limit=""
codi_indicador="EQA9223"
alt="ass"


class AssEQA(object):
    def __init__(self):
        self.ps=('N87.2', 'N87.0', 'N87.1', 'N87.9', 'N93.0','B97.7','C01-N87.2', 'C01-N87.0', 'C01-N87.1', 'C01-N87.9', 'C01-N93.0','C01-B97.7')
        self.agr=400
        self.current_date=self.get_date_dextraccio()
        self.dones_ates=self.get_dones_ates_ultim_any()
        
        
        

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]

    def get_dones_ates_ultim_any(self):
        sql="select id_cip_sec from {}.ass_poblacio where edat < 25".format(nod)
        return {id for id, in getAll(sql,nod)}


    def get_problemes(self,table):
        id_prob=defaultdict(set)
        sql="select id_cip_sec,pr_dde,pr_dba from {} where pr_data_baixa =0 and pr_cod_ps in {}".format(table,self.ps)
        print sql
        for id, dde,pr_dba in getAll(sql,imp):
            id_prob[id].add(dde)
        return id_prob

    def get_variables(self):
        id_var=defaultdict(set)
        sql='select id_cip_sec,dat,agrupador from {}.ass_variables where agrupador = {}'.format(nod,self.agr)
        for id, data, agr in getAll(sql,nod):
            if monthsBetween(data,self.current_date) < 12:
                id_var[id].add(data)
        return id_var

    def get_multiprocess(self):
        id_var_total=defaultdict(set)
        for id_var in multiprocess(self.get_problemes,getSubTables('problemes')):
            for id in id_var:
                id_var_total[id] |= id_var[id]
        printTime('Variables')
        return id_var_total


    def get_indicador (self):
        """
        """
        uba_rows=[]


        numerador= self.get_variables()
        printTime('Numerador',len(numerador))
        exclusions=self.get_multiprocess()
        printTime('Excl',len(exclusions))
        

        for id in self.dones_ates:
            den = 1
            num = 0
            if id in numerador:
                if id in exclusions and any( [ dde <= data_var for data_var in numerador[id] for dde in exclusions[id]]):
                    num=0
                else:
                    num=1

            uba_rows.append((id,num,den,0))

        return uba_rows

        
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

if __name__ == '__main__':

    printTime('inici')
    
    ass_23=AssEQA()
    uba_rows=ass_23.get_indicador()
    table_name="ass_indicador_{}".format(codi_indicador)
    columns="(id_cip_sec int, num int, den int)"
    export_table(table_name,columns,alt,uba_rows)
    printTime("Exxxxported!")
    
   