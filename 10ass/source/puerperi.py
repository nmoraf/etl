from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="ass"
conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()

path="./dades_noesb/ass_relacio.txt"
variables="nodrizas.ass_variables"
embaras ="nodrizas.ass_embaras"
puerperi="ass_puerperi"
agrupador_puerperi=389
criteris="ass_criteris"

c.execute("drop table if exists %s" % puerperi)
c.execute("create table %s (id_cip_sec int, dat date)" % puerperi)
c.execute("insert into %s select id_cip_sec,fi from %s where puerperi=1" % (puerperi,embaras))
c.execute("alter table %s add index (id_cip_sec,dat)" % puerperi)

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="p":
         indDEn = ind + '_den'
         c.execute("alter table %s add column %s int" % (puerperi,ind))
         c.execute("update %s set %s=0" % (puerperi,ind))
         c.execute("alter table %s add column %s int" % (puerperi,indDEn))
         c.execute("update %s set %s=0" % (puerperi,indDEn))
         c.execute("update %s,nodrizas.dextraccio set %s=1 where dat < date_add(data_ext,interval - %s day) " % (puerperi,indDEn,tmin))
      elif tipus=="num_p":
         c.execute("select taula from %s where agrupador='%s' " % (criteris,agr))
         pres=c.fetchall()
         for a in pres:
            taula= a[0]
            if taula== "variables":
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador=%s and (b.dat between date_add(a.dat,interval + %s day) and  date_add(a.dat,interval + %s day))" % (puerperi,variables,ind,agr, tmin, tmax))
  

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")