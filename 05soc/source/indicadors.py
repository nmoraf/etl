# coding: latin1

"""
.
"""

import simplejson as j

import sisapUtils as u


TABLE = "mst_indicadors_pacient"
DATABASE = "social"


class Social(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.indicadors = self.get_indicadors()
        self.pacients = {}
        for codi, specs in self.indicadors.items():
            if specs["do"]:
                self.pacients[codi] = {}
                for concepte in ("numerador", "denominador", "exclusio"):
                    if specs.get(concepte):
                        pacients = Social.get_component(specs[concepte])
                    else:
                        pacients = set()
                    self.pacients[codi][concepte] = pacients
        self.get_master()
        self.upload_master()

    def create_table(self):
        """."""
        cols = "(id_cip_sec int, up varchar(5), edat int, sexe varchar(1), \
                 ates int, instit int, indicador varchar(10), compleix int, \
                 exclos int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)

    def get_indicadors(self):
        """."""
        file = 'dades_noesb/indicadors.json'
        try:
            stream = open(file)
        except IOError:
            stream = open('../' + file)
        return j.load(stream, encoding="latin1")

    def get_master(self):
        """."""
        self.master = []
        edats = {cod: spec["edat"] for (cod, spec) in self.indicadors.items()}
        sql = "select id_cip_sec, uporigen, edat, sexe, \
                      ates, institucionalitzat \
               from assignada_tot"
        for id, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            for ind, pacients in self.pacients.items():
                if edats[ind][0] <= edat <= edats[ind][1]:
                    if id in pacients["denominador"]:
                        good = 1 * (id in pacients["numerador"])
                        excl = 1 * (id in pacients["exclusio"])
                        this = (id, up, edat, sexe, ates, instit, ind, good, excl)  # noqa
                        self.master.append(this)

    def upload_master(self):
        """."""
        u.listToTable(self.master, TABLE, DATABASE)

    @staticmethod
    def get_specs(specs):
        """."""
        if isinstance(specs, dict):
            relacio = "o"
            specs = [specs]
        else:
            relacio = specs[0]
            specs = specs[1:]
        return (specs, relacio)

    @staticmethod
    def get_component(specs):
        """."""
        pacients = set()
        specs, relacio = Social.get_specs(specs)
        first = True
        for spec in specs:
            if isinstance(spec, dict):
                params = Social.get_sql(spec)
                component = set([id for id, in u.getAll(*params)])
            else:
                component = Social.get_component(spec)
            if relacio == "o" or first:
                pacients |= component
                first = False
            else:
                pacients &= component
        return pacients

    @staticmethod
    def get_tuple(codis):
        """."""
        if len(codis) == 1:
            return "('{}')".format(codis[0])
        else:
            return tuple(codis)

    @staticmethod
    def get_sql(spec):
        """."""
        params = {("ts_variables", "cod"): "agrupador",
                  ("ts_variables", "dat"): "dat",
                  ("ts_variables", "val"): "convert(val, int)",
                  ("ts_variables", "last"): "last",
                  ("ts_visites", "cod"): "domi",
                  ("ts_valoracio", "dat"): "dat",
                  ("eqa_problemes", "cod"): "ps",
                  ("eqa_problemes", "dat"): "dde",
                  ("estats", "cod"): "es_cod",
                  ("estats", "dat"): "es_dde",
                  ("estats", "db"): "import",
                  ("gma", "cod"): "gma_pniv",
                  ("gma", "db"): "import",
                  ("assignada_tot", "val"): "edat",
                  ("ass_variables", "cod"): "agrupador",
                  ("ass_variables", "dat"): "dat",
                  ("ass_variables", "val"): "val_num",
                  ("xml_detall_cb", "cod"): "camp_codi",
                  ("xml_detall_cb", "dat"): "xml_data_alta",
                  ("xml_detall_cb", "val"): "convert(camp_valor, int)",
                  ("xml_detall_cb", "db"): "import"}
        taula = spec["taula"]
        codis = spec.get("agrupadors")
        temps = spec.get("temps")
        desde = spec.get("desde")
        fins = spec.get("fins")
        last = spec.get("last")
        valors = spec.get("valors")
        sql = "select id_cip_sec \
               from {}, nodrizas.dextraccio".format(taula)
        where = []
        if codis:
            where.append("{} in {}".format(params[(taula, "cod")],
                                           Social.get_tuple(codis)))
        if temps:
            if not isinstance(temps, list):
                temps = [temps, "0 day"]
            where.append("{} between adddate(adddate(data_ext, \
                                             interval - {}), \
                                     interval +1 day) and \
                                     adddate(data_ext, \
                                             interval - {})".format(
                                                params[(taula, "dat")],
                                                temps[0],
                                                temps[1]))
        if desde:
            where.append("{} >= {}".format(params[(taula, "dat")], desde))
        if fins:
            where.append("{} <= {}".format(params[(taula, "dat")], fins))
        if last:
            where.append("{} = {}".format(params[(taula, "last")], last))
        if valors:
            where.append("{} between {} and {}".format(
                                                    params[(taula, 'val')],
                                                    valors[0], valors[1]))
        if taula == "ts_valoracio":
            where.append("id_cip_sec in (select id_cip_sec \
                                         from ts_valoracio \
                                         where obs = 0 \
                                         group by id_cip_sec \
                                         having count(distinct tip) = 4)")
        if where:
            sql += " where "
            sql += " and ".join(where)
        db = params.get((taula, "db"), "nodrizas")
        return (sql, db)


if __name__ == "__main__":
    Social()
