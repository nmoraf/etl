# coding: latin1

"""
.
"""

import collections as c
import simplejson as j

import sisapUtils as u


BASE = "mst_indicadors_pacient"
KHALIX_IND = "exp_khalix_up_ind"
KHALIX_POB = "exp_khalix_up_pob"
PROFESSIONALS = "mst_professionals"
UBAS = "mst_ubas"
ECAP_IND = "exp_ecap_uba"
ECAP_PAC = "exp_ecap_pacient"
CAT_IND = "exp_ecap_cataleg"
CAT_PARE = "exp_ecap_catalegPare"
CAT_PUNTS = "exp_ecap_catalegPunts"
CAT_EXC = "exp_ecap_catalegExclosos"
CAT_KLX = "exp_khalix_cataleg"

DATABASE = "social"


class Export(object):
    """."""

    def __init__(self):
        """."""
        self.set_khalix_ind()
        self.set_khalix_pob()
        self.get_professionals()
        self.get_metes()
        self.get_ponderacio()
        self.set_ecap_indicadors()
        self.get_id_to_hash()
        self.set_ecap_pacients()
        self.get_cataleg()

    def set_khalix_ind(self):
        """."""
        dades = c.Counter()
        sql = "select id_cip_sec, up, indicador, edat, sexe, ates, \
                      instit, compleix \
               from {} \
               where exclos = 0".format(BASE)
        for id, up, ind, edat, sexe, ates, ins, num in u.getAll(sql, DATABASE):
            pobs = ["{}INSASS".format("" if ins else "NO")]
            if ates:
                pobs.append("{}INSAT".format("" if ins else "NO"))
            edat_k = u.ageConverter(int(edat))
            sexe_k = u.sexConverter(sexe)
            for pob in pobs:
                dades[(up, edat_k, sexe_k, pob, ind, "DEN")] += 1
                if num:
                    dades[(up, edat_k, sexe_k, pob, ind, "NUM")] += 1
        upload = [k + (v,) for (k, v) in dades.items()]
        cols = "(up varchar(10), edat varchar(10), sexe varchar(10), \
                 comb varchar(10), indicador varchar(10), conc varchar(10), \
                 n int)"
        u.createTable(KHALIX_IND, cols, DATABASE, rm=True)
        u.listToTable(upload, KHALIX_IND, DATABASE)

    def set_khalix_pob(self):
        """."""
        dades = c.Counter()
        sql = "select id_cip_sec, uporigen, edat, sexe, \
                      ates, institucionalitzat \
               from assignada_tot"
        for id, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            pobs = ["{}INSASS".format("" if instit else "NO")]
            if ates:
                pobs.append("{}INSAT".format("" if instit else "NO"))
            for pob in pobs:
                key = (up, u.ageConverter(edat), u.sexConverter(sexe), pob)
                dades[key] += 1
        upload = [k + (v,) for (k, v) in dades.items()]
        cols = "(up varchar(10), edat varchar(10), sexe varchar(10), \
                 comb varchar(10), n int)"
        u.createTable(KHALIX_POB, cols, DATABASE, rm=True)
        u.listToTable(upload, KHALIX_POB, DATABASE)

    def get_professionals(self):
        """."""
        self.professionals = c.defaultdict(set)
        upload = []
        sql = "select distinct a.up, left(a.ide_dni, 8) \
              from cat_profvisual a \
              where a.visualitzacio = 1 \
              and exists (select 1 from cat_professionals b \
                          where a.ide_usuari = b.ide_usuari and b.tipus = 'T')"
        for up, dni in u.getAll(sql, "import"):
            self.professionals[up].add(dni)
            upload.append((up, dni))
        u.createTable(PROFESSIONALS, '(up varchar(5), dni varchar(20))',
                      DATABASE, rm=True)
        u.listToTable(upload, PROFESSIONALS, DATABASE)

    def get_metes(self):
        """."""
        file = "dades_noesb/eqats_metesres_subind_anual.txt"
        self.metes = {(ind, meta): float(valor) / 100.0
                      for (ind, _r, _r, meta, _r, pob, _r, valor)
                      in u.readCSV(file, sep="{")
                      if pob == "NOINSAT"}

    def get_ponderacio(self):
        """."""
        file = "dades_noesb/ponderacio.txt"
        self.ponderacio = {ind: float(val) for (ind, val)
                           in u.readCSV(file, sep="@")}
        cols = "(indicador varchar(10), tipus varchar(1), valor double)"
        u.createTable(CAT_PUNTS, cols, DATABASE, rm=True)
        u.listToTable([(k, "T", v) for k, v in self.ponderacio.items()],
                      CAT_PUNTS, DATABASE)

    def set_ecap_indicadors(self):
        """."""
        dades = []
        ubas = set()
        sql = "select up, '0', indicador, sum(compleix), count(1), \
                      sum(compleix) / count(1) \
               from {} \
               where instit = 0 and \
                     ates = 1 and \
                     exclos = 0 \
               group by up, indicador".format(BASE)
        sql += " union select 'UP', dni, indicador, sum(compleix), count(1), \
                      sum(compleix) / count(1) \
               from {} a \
               inner join mst_professionals b on a.up = b.up \
               where instit = 0 and \
                     ates = 1 and \
                     exclos = 0 \
               group by dni, indicador".format(BASE)
        for up, uba, ind, num, den, res in u.getAll(sql, DATABASE):
            mmin = self.metes.get((ind, "AGMMINRES"), 0)
            mmax = self.metes.get((ind, "AGMMAXRES"), 0)
            pond = self.ponderacio[ind]
            punts = pond if res >= mmax else 0 if res < mmin else pond * ((float(res) - mmin) / (mmax - mmin))  # noqa
            this = (up, uba, "T", ind, 0, den, num, 1, 1, res, res,
                    res, punts, den - num, res, 0, mmin, mmax)
            dades.append(this)
            ubas.add((up, uba, "T"))
        cols = "(up varchar(5), uba varchar(10), tipus varchar(1), \
                 indicador varchar(8), esperats double, detectats int, \
                 resolts int, deteccio double, deteccioPerResultat double, \
                 resolucio double, resultat double, resultatPerPunts double, \
                 punts double, llistat int, resolucioPerPunts double, \
                 invers int, mmin_p double, mmax_p double)"
        u.createTable(ECAP_IND, cols, DATABASE, rm=True)
        u.listToTable(dades, ECAP_IND, DATABASE)
        cols = "(up varchar(5), uba varchar(10), tipus varchar(1))"
        u.createTable(UBAS, cols, DATABASE, rm=True)
        u.listToTable(list(ubas), UBAS, DATABASE)

    def get_id_to_hash(self):
        """."""
        ids = set([int(id) for id,
                   in u.getAll("select id_cip_sec from {}".format(BASE),
                               DATABASE)])
        sql = "select id_cip_sec, hash_d, codi_sector from u11 \
               where id_cip_sec in {}".format(tuple(ids))
        self.id_to_hash = {row[0]: row[1:] for row in u.getAll(sql, "import")}

    def set_ecap_pacients(self):
        """."""
        sql = "select id_cip_sec, up, indicador, \
                      if(instit = 1, 2, \
                         if(ates = 0, 5, \
                            if(exclos = 1, 4, 0))) \
               from {} \
               where compleix = 0".format(BASE)
        dades = [row + self.id_to_hash[row[0]] for row
                 in u.getAll(sql, DATABASE)]
        cols = "(id_cip_sec int, up varchar(5), indicador varchar(10), \
                 exclos int, hash_d varchar(40), sector varchar(4))"
        u.createTable(ECAP_PAC, cols, DATABASE, rm=True)
        u.listToTable(dades, ECAP_PAC, DATABASE)
        cataleg = [(0, "Pacients que formen part de l&#39;indicador", 0),
                   (2, "Pacients institucionalitzats", 1),
                   (4, "Pacients exclosos per motius cl&iacute;nics", 2),
                   (5, "Pacients no atesos el darrer any", 3)]
        cols = "(codi int, descripcio varchar(255), ordre int)"
        u.createTable(CAT_EXC, cols, DATABASE, rm=True)
        u.listToTable(cataleg, CAT_EXC, DATABASE)

    def get_cataleg(self):
        """."""
        upload = []
        khalix = []
        file = 'dades_noesb/indicadors.json'
        cataleg = j.load(open(file), encoding="latin1")
        pare = "EQAT01"
        pare_lit = "Indicadors treball social"
        umi = "http://sisap-umi.eines.portalics/indicador/codi/"
        for codi, dades in cataleg.items():
            this = (codi, dades["literal"].encode("latin1"), int(codi[-2:]),
                    pare, 1, 0, 0, self.metes.get((codi, "AGMMINRES"), 0),
                    self.metes.get((codi, "AGMINTRES"), 0),
                    self.metes.get((codi, "AGMMAXRES"), 0),
                    1, umi + codi, "", "GENERAL")
            upload.append(this)
            khalix.append((codi, dades["literal"].encode("latin1"),
                           pare, pare_lit))
        cols = "(indicador varchar(8), literal varchar(255), ordre int, \
                 pare varchar(8), llistat int, invers int, mdet double, \
                 mmin double, mint double, mmax double, toShow int, \
                 wiki varchar(255), curt varchar(80), pantalla varchar(50))"
        u.createTable(CAT_IND, cols, DATABASE, rm=True)
        u.listToTable(upload, CAT_IND, DATABASE)
        cols = "(pare varchar(10), literal varchar(255), ordre int, \
                 curt varchar(80), proces varchar(50))"
        u.createTable(CAT_PARE, cols, DATABASE, rm=True)
        dades = [(pare, pare_lit, 1, "Treball social", "")]
        u.listToTable(dades, CAT_PARE, DATABASE)
        cols = "(indicador varchar(10), desc_ind varchar(255), \
                 grup varchar(10), grup_desc varchar(255))"
        u.createTable(CAT_KLX, cols, DATABASE, rm=True)
        u.listToTable(khalix, CAT_KLX, DATABASE)


if __name__ == "__main__":
    Export()
