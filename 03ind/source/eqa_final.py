# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict
import csv, os


bd = "eqa_ind"
conn = connect((bd,'aux'))
c = conn.cursor()
d = conn.cursor()

c.execute("select grip,grip_ind from nodrizas.dextraccio")
gripp,gripInd,=c.fetchone()
grip= True if gripp==1 else False
hideInd = ('EQA0217', 'EQA0313', 'EQD024204')

edats ="eqa_edats"
ind = "eqa_ind"
relacio ="eqa_relacio"

pac = "mst_indicadors_pacient"
ecap_pac_pre = "eqa_ecap_pacient_pre"
exclHide="eqa_exclusions_hide"
ecap_pac = "exp_ecap_pacient"
#condicio_ecap_llistat = "ates=1 and excl_edat=0 and institucionalitzat=0 and maca=0 and excl=0 and llistat=1"
condicio_ecap_llistat = "llistat=1 and excl=0 and ci=0"
c.execute("select withMaca_ind from nodrizas.dextraccio")
withMACA,=c.fetchone()
ecap_marca_exclosos = "if(institucionalitzat=1,2,if(excl_edat=1,3,if(clin=1,4,if(ates=0,5,if(ci=1,6,if(maca=1 and a.grup_codi not in {},1,0))))))".format(withMACA)
ecap_cataleg_exclosos = [[0,'Pacients que formen part de l&#39;indicador',0,0],[1,'Pacients MACA',2,0],[2,'Pacients institucionalitzats',3,0],[3,'Pacients exclosos per edat',4,0],[4,'Pacients exclosos per motius cl&iacute;nics',5,0],[5,'Pacients no atesos el darrer any',1,0],[6,'Pacients exclosos per contraindicacions',6,0]]

u11 = "mst_u11"
u11nod = "nodrizas.eqa_u11_with_jail"

up = "eqa_khalix_up_pre"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
khalix_pre = "eqa_khalix_up_ind"
khalix = "exp_khalix_up_ind"
condicio_khalix = "excl_edat=0 and excl=0 and ci=0 and clin=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
condicio_khalixMACA = "excl_edat=0 and excl=0 and ci=0 and clin=0 and (institucionalitzat=0 and maca=1)"
if not grip:
    condicio_khalix = condicio_khalix + " and left(ind_codi,7) not in %s" % gripInd
edats_k = "nodrizas.khx_edats5a"

pobk = "eqa_poblacio_khx"
condicio_khalix_pob = "institucionalitzat=1 or (institucionalitzat=0 and maca=0)"
pobOrig = "assignada_tot"   #aqui ja hi ha filtre edat >14a
khalix_pob = "exp_khalix_up_pob"
conceptes=[['NUM','num'],['DEN','den']]

pob = "eqa_poblacio"
condicio_poblacio = "ates=1 and institucionalitzat=0 and maca=0"
uba_in = "mst_ubas"
uba1 = "eqa_uba_1"
medea = "nodrizas.cat_centres_with_jail"  #si es '' (o no existis a centres) posem 2U (compte que khalix no ho fa, i per tant no tindran agregat de medea en el detall)
minPobEdat = 'EC1519'
minPob = 50
tipus=[['M','up','uba'],['I','upinf','ubainf']]
uba2 = "eqa_uba_2"
uba3 = "eqa_uba_3"
esp = "eqa_prevalences_anual"
uba4 = "eqa_uba_4"
uba5 = "eqa_uba_5"
condicio_ecap_ind = "ates=1 and excl_edat=0 and institucionalitzat=0 and maca=0 and excl=0 and ci=0 and clin=0"
condicio_ecap_indMACA = "ates=1 and excl_edat=0 and institucionalitzat=0 and excl=0 and ci=0 and clin=0"
ecap_ind = "exp_ecap_uba"   #no hi poso poblacio perque tal com esta ara seria erronia (es per grups de 5 anys, i molts indicadors es tallen per exemple a 80 i el grup va de 80 a 84); per esperats no passa res perque ja demanem menor prevalenca en els grups "tallats"
esperadesOrig = "eqa_poresper_anual"
esperades = "eqa_esperades"
esperadesFactor = 0.8
esperadesCond = "grup = 'NOINSAT'"
pobCond = 'NOINSAT'
c.execute("select atdom_ind from nodrizas.dextraccio")
atdom,=c.fetchone()
metesOrig = "eqa_metes_subind_anual"
metesOrigres = "eqa_metesres_subind_anual"
metes = "eqa_metes"
metesres = "eqa_metesres"
ponderacioOrig = "eqa_ponderacio_anual"
ponderacio = "eqa_ponderacio"
inversos = "eqa_inversos"

khalix_uba = "exp_khalix_uba_ind"
khalix_ubaEQD = "exp_khalix_uba_indEQD"
dimensions = [['NUM','resolts', 1],['DEN','detectats', 1],['DENESPER','esperats', 1],['AGNORESOL','llistat', 1],['AGDETECTR','deteccio', 100],['AGDETECT','deteccioPerResultat', 100],['AGRESOLTR','resolucio', 100],['AGRESOLT','resolucioPerPunts', 100],['AGRESULT','resultatPerPunts', 100],['AGASSOLP','resultatPerPunts', 100],['AGASSOL','punts', 1],['AGMMINRES','mmin_p', 100],['AGMMAXRES','mmax_p', 100]]
condicio_khalix_uba=""
if not grip:
    condicio_khalix_uba = condicio_khalix_uba + " where indicador not in %s" % gripInd

cataleg = "exp_ecap_cataleg"
catalegPare = "exp_ecap_catalegPare"
catalegPunts = "exp_ecap_catalegPunts"
catalegExclosos = "exp_ecap_catalegExclosos"
catalegKhx = "exp_khalix_cataleg"

try:
    c.execute("create table ctl_girats (done int)")
    c.execute("select distinct ind_codi from %s where baixa_=0 and girar=1" % ind)
    r2=c.fetchall()
    for t2 in r2:
       indicador = t2[0]
       taula2 ="%s_ind2" % (indicador)
       c.execute("update %s set num=1-num" % taula2)
except:
    pass

c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,ind_codi varchar(10) not null default'',grup_codi varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double, trasplantat double,excl_edat double,seccio_censal varchar(10) not null default'',up_residencia varchar(13) not null default'',institucionalitzat double,maca double,num double,den double,excl double,ci double,clin double,llistat double)" % pac)
c.execute("select distinct a.ind_codi,grup_codi,llistats,invers,minim,maxim,tipus from %s a inner join %s b on a.ind_codi=b.ind_codi where baixa_=0" % (ind,edats))
r=c.fetchall()
for t in r:
   indicador,grup,llistat,minim,maxim,aguda = t[0],t[1],-1 if t[2]==0 else t[3],t[4],t[5],t[6]==3
   taula ="%s_ind2" % (indicador)
   #print [indicador,grup,llistat,minim,maxim,aguda]
   c.execute("insert into %s select id_cip_sec,'%s','%s',up,uba,upinf,ubainf,edat,sexe,ates,trasplantat,if(edat between %d and %d,0,1) excl_edat,seccio_censal,up_residencia,institucionalitzat,maca,num,%s den,excl,ci,clin,%s from %s" % (pac,indicador,grup,minim,maxim,"den" if aguda else 1,'if(num{0},1,0)'.format('<den' if llistat==0 else '>0') if aguda and llistat> -1 else 'if(num={0},1,0)'.format(llistat),taula))

c.execute("update %s a inner join %s b on a.ind_codi=b.ind_codi set excl=1 where trasplantat=1 and (grup2_codi not in ('EQAG08', 'EQAG09', 'EQAG10', 'EQAG11', 'EQAG19') and b.ind_codi not like ('EQD%%'))" % (pac,ind))

c.execute("drop table if exists %s" % ecap_pac_pre)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',maca int,institucionalitzat int,excl_edat int,excl int,ci int, clin int,exclos int,toShow int)" % ecap_pac_pre)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,grup_codi,maca,institucionalitzat,excl_edat,excl,ci,clin,%s,1 from %s a where %s" % (ecap_pac_pre,ecap_marca_exclosos,pac,condicio_ecap_llistat))
c.execute("delete a.* from %s a inner join %s b on a.grup_codi=b.grup_codi where nets=1" % (ecap_pac_pre,ind))
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,a.ind_codi,maca,institucionalitzat,excl_edat,excl,ci,clin,%s,1 from %s a inner join eqa_ind b on a.ind_codi=b.ind_codi where %s and nets=1" % (ecap_pac_pre,ecap_marca_exclosos,pac,condicio_ecap_llistat))
c.execute("update %s a inner join %s b on a.grup_codi=b.indicador set toShow=0 where a.excl_edat=1 and b.excl_edat=0" % (ecap_pac_pre,exclHide))
c.execute("alter table %s add index(id_cip_sec)" % ecap_pac_pre)

c.execute("drop table if exists %s" % ecap_pac)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("drop table if exists %s" % u11)
c.execute("create table %s as select * from %s where 1=0" % (u11,u11nod))
c.execute("insert into %s select * from %s a where exists (select 1 from %s b where toShow=1 and a.id_cip_sec=b.id_cip_sec)" % (u11,u11nod,ecap_pac_pre))

c.execute("alter table %s add unique(id_cip_sec,hash_d,codi_sector)" % u11)
c.execute("insert into %s select a.id_cip_sec,up,uba,upinf,ubainf,grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1 group by a.id_cip_sec,up,uba,upinf,ubainf,grup_codi,exclos,hash_d,codi_sector" % (ecap_pac,ecap_pac_pre,u11))

c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',ind_codi varchar(10) not null default'',num double,den double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,ind_codi,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,comb,pac,edats_k,condicio_khalix))
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,ind_codi,num,den from %s a inner join %s b on a.edat=b.edat where %s and left(ind_codi,7) in %s" % (up,sexe,comb,pac,edats_k,condicio_khalixMACA,withMACA))
c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',ind_codi varchar(10) not null default'',num double,den double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,ind_codi,sum(num) num,sum(den) den from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,ind_codi indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),ind_codi indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))

c.execute("drop table if exists %s" % pobk)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '')" % pobk)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb from %s a inner join %s b on a.edat=b.edat where %s" % (pobk,sexe,comb,pobOrig,edats_k,condicio_khalix_pob))
c.execute("drop table if exists %s" % khalix_pob)
c.execute("create table %s (up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',n int)" % khalix_pob)
c.execute("insert into %s select up,edat,sexe,comb,count(1) n from %s where comb like '%%AT%%' group by 1,2,3,4" % (khalix_pob,pobk))
c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),count(1) n from %s group by 1,2,3,4" % (khalix_pob,pobk))

c.execute("drop table if exists %s" % pob)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',uba varchar(5) not null default '',upinf varchar(5) not null default '',ubainf varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '')" % pob)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,khalix edat,%s sexe from %s a inner join %s b on a.edat=b.edat where %s" % (pob,sexe,pobOrig,edats_k,condicio_poblacio))
c.execute("drop table if exists %s" % uba_in)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',primary key(up,uba,tipus))" % uba_in)
c.execute("drop table if exists %s" % uba1)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',n int)" % uba1)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s up,%s uba,'%s' tipus from %s where %s <> '' and %s <> '' group by %s,%s having sum(if(edat='%s',0,1))>=%d" % (uba_in,up,uba,tip,pob,up,uba,up,uba,minPobEdat,minPob))
    c.execute("insert into %s select %s up,%s uba,'%s' tipus,'TIPUS2U2' medea,edat,sexe,count(1) n from %s group by %s,%s,edat,sexe" % (uba1,up,uba,tip,pob,up,uba))
c.execute("delete from %(uba1)s where not exists (select 1 from %(uba_in)s where %(uba1)s.up=%(uba_in)s.up and %(uba1)s.uba=%(uba_in)s.uba and %(uba1)s.tipus=%(uba_in)s.tipus)" % {'uba1': uba1,'uba_in':uba_in})
c.execute("update %s a inner join %s b on a.up=b.scs_codi set a.medea=concat('TIPUS',b.medea,'2') where b.medea<>''" % (uba1,medea))
c.execute("drop table if exists %s" % uba2)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',indicador varchar(10) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',poblacio int)" % uba2)
c.execute("insert into %s select up,uba,tipus,grup_codi indicador,medea,edat,sexe,n poblacio from %s,(select distinct grup_codi from %s where baixa_=0) a" % (uba2,uba1,ind))
c.execute("drop table if exists %s" % uba3)
c.execute("create table %s like %s" % (uba3,uba2))
c.execute("alter table %s add column esperats double null" % uba3)
c.execute("insert into %s select a.*,poblacio*prev esperats from %s a inner join %s b on a.indicador=b.indicador and a.medea=b.medea and a.edat=b.edat and a.sexe=b.sexe where pob = '%s'" % (uba3,uba2,esp, pobCond))
c.execute("drop table if exists %s" % uba4)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',indicador varchar(10) not null default '',poblacio int,esperats double)" % uba4)
c.execute("insert into %s select up,uba,tipus,indicador,sum(poblacio) poblacio,sum(esperats) esperats from %s group by 1,2,3,4" % (uba4,uba3))
c.execute("drop table if exists %s" % uba5)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(10) not null default '',detectats int,resolts int)" % uba5)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,grup_codi indicador,sum(den) detectats,sum(num) resolts from %s where %s and grup_codi not in %s group by 1,2,4" % (uba5,up,uba,tip,pac,condicio_ecap_ind,withMACA))
    c.execute("insert into %s select %s,%s,'%s' tipus,grup_codi indicador,sum(den) detectats,sum(num) resolts from %s where %s and grup_codi in %s group by 1,2,4" % (uba5,up,uba,tip,pac,condicio_ecap_indMACA,withMACA))
    c.execute("insert into %s select %s,%s,'%s' tipus,a.ind_codi indicador,sum(den) detectats,sum(num) resolts from %s a inner join %s b on a.ind_codi=b.ind_codi where nets=1 and %s group by 1,2,4" % (uba5,up,uba,tip,pac,ind,condicio_ecap_ind))
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % uba4)
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % uba5)

c.execute("drop table if exists %s" % ecap_ind)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(10) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int,resolucioPerPunts double, invers int, mmin_p double, mmax_p double)" % ecap_ind)
c.execute("insert into %s select a.up,a.uba,a.tipus,a.indicador,esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from %s a left join %s b on a.up=b.up and a.uba=b.uba and a.tipus=b.tipus and a.indicador=b.indicador" % (ecap_ind,uba4,uba5))
c.execute("update %s set deteccio=detectats/esperats,resolucio=if(detectats=0,0,resolts/detectats)" % ecap_ind)
c.execute("drop table if exists %s" % esperades)
c.execute("create table %s (indicador varchar(10) not null default '',prof double,primary key (indicador),unique (indicador,prof))" % esperades)
c.execute("insert into %s select indicador,%s*eap/100 prof from %s where %s" % (esperades,esperadesFactor,esperadesOrig,esperadesCond))
c.execute("insert ignore into %s select distinct grup_codi indicador,0 prof from %s where baixa_=0" % (esperades,ind))
c.execute("update %s set prof=0 where indicador in %s" % (esperades,atdom))
c.execute("update %s a inner join %s b on a.indicador=b.indicador set deteccioPerResultat=if(deteccio>=prof or prof=0,1,deteccio/prof)" % (ecap_ind,esperades))
c.execute("update %s set resultat=resolucio" % ecap_ind)
c.execute("drop table if exists %s" % inversos)
c.execute("create table %s (indicador varchar(10) not null default '',invers int,unique(indicador,invers))" % inversos)
c.execute("insert into %s select distinct grup_codi indicador,invers from %s where baixa_=0" % (inversos,ind))
c.execute("insert into %s select distinct ind_codi indicador,invers from %s where baixa_=0 and nets=1" % (inversos,ind))

c.execute("update {} a inner join {} b on a.indicador=b.indicador set a.invers = b.invers, llistat=if(b.invers=1,resolts,detectats-resolts)".format(ecap_ind, inversos))
c.execute("drop table if exists %s" % metesres)
c.execute("create table %s (indicador varchar(10) not null default '',mmin double,mint double,mmax double,unique (indicador,mmin,mmax))" % metesres)
c.execute("insert into %s select a.indicador,a.valor/100,b.valor/100,c.valor/100 from (select indicador,valor from %s where meta='AGMMINRES' and z4 = '%s') a inner join (select indicador,valor from %s where meta='AGMINTRES' and z4 = '%s') b on a.indicador=b.indicador inner join (select indicador,valor from %s where meta='AGMMAXRES' and z4 = '%s') c on a.indicador=c.indicador" % (metesres,metesOrigres,pobCond,metesOrigres,pobCond,metesOrigres,pobCond))
c.execute('update {} set mmin = mmax * 0.9 where mmin > mmax * 0.9'.format(metesres))
c.execute('update {} set mint = (mmax + mmin) / 2'.format(metesres))
c.execute("update {} a inner join {} b on a.indicador = b.indicador set mmin_p = if(invers = 0, floor(mmin * detectats), ceil(mmin * detectats)), mmax_p = if(invers = 0, floor(mmax * detectats), ceil(mmax * detectats))".format(ecap_ind, metesres))
c.execute('update {} set mmin_p = mmin_p - 1 where invers = 0 and mmin_p = mmax_p and mmax_p > 0'.format(ecap_ind))
c.execute('update {} set mmax_p = mmax_p + 1 where invers = 1 and mmin_p = mmax_p'.format(ecap_ind))
c.execute("update %s set resolucioPerPunts=if(invers = 0, if(resolts>=mmax_p,1,if(resolts<mmin_p,0,(resolts-mmin_p)/(mmax_p-mmin_p))), \
                                                      1 - if(resolts>=mmax_p,1,if(resolts<mmin_p,0,(resolts-mmin_p)/(mmax_p-mmin_p))))" % (ecap_ind))
c.execute('update {} set mmin_p = mmin_p / detectats, mmax_p = mmax_p / detectats where detectats > 0'.format(ecap_ind))
c.execute("update %s a set resultatPerPunts=resolucioPerPunts*deteccioPerResultat" % (ecap_ind))
c.execute("drop table if exists %s" % ponderacio)
c.execute("create table %s (indicador varchar(10) not null default '',tipus varchar(1) not null default '',valor double,unique (indicador,tipus,valor))" % ponderacio)
c.execute("insert into %s select indicador,left(tipus, 1), valor from %s where valor > 0" % (ponderacio,ponderacioOrig))
c.execute("update %s a left join %s b on a.indicador=b.indicador and a.tipus=b.tipus set punts=resolucioPerPunts*deteccioPerResultat*ifnull(valor,0)" % (ecap_ind,ponderacio))
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % ecap_ind)
c.execute("insert ignore into %s select a.up, a.uba, a.tipus, indicador, 0 esperats, detectats,resolts, 1 deteccio,1 deteccioPerResultat, resolts/detectats resolucio,resolts/detectats resultat,0 resultatPerPunts,0 punts,detectats-resolts llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from %s a inner join %s b on a.up=b.up and a.uba=b.uba and a.tipus=b.tipus" % (ecap_ind,uba5,uba_in))
c.execute("insert ignore into %s select up,uba,tipus,indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio, 0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from (select up,uba,tipus from %s) a,(select distinct grup_codi indicador from %s where baixa_=0 and tipus not in (14, 16)) b" % (ecap_ind,uba_in,ind))
c.execute("insert ignore into %s select up,uba,tipus,indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio, 0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from (select up,uba,tipus from %s) a,(select distinct ind_codi indicador from %s where baixa_=0 and nets=1) b" % (ecap_ind,uba_in,ind))
c.execute("insert ignore into %s select up,uba,tipus,indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio, 0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from (select up,uba,tipus from %s) a,(select distinct grup_codi indicador from %s where baixa_=0 and nets=1) b" % (ecap_ind,uba_in,ind))
# ION:36107->  repetir update de linea:207, motivo: corregir flag 'invers' cuando son inversos sin dades_noesb\eqa_prevalencia_anual.txt
# que dejo fuera el LEFT join en linea:193
# y que la linea:225 inserto con  invers=0  
c.execute("update {} a inner join {} b on a.indicador=b.indicador set a.invers = b.invers, llistat=if(b.invers=1,resolts,detectats-resolts)".format(ecap_ind, inversos))

c.execute("drop table if exists %s" % khalix_uba)
c.execute("create table %s (up varchar(5),uba varchar(5),tipus varchar(1),indicador varchar(10),analisis varchar(12),detalle varchar(12),valor double)" % khalix_uba)
for r in dimensions:
    analisis,variable, coeficient =r[0],r[1],r[2]
    c.execute("insert into %s select up,uba,tipus,if(indicador like ('EQA%%'),replace(indicador,'EQA','EQAU'),if(indicador like ('EQD%%'),replace(indicador,'EQD','EQDU'),indicador)) indicador,'%s' analisis,'NOINSAT' detalle,round(%s,4)*%s valor from %s%s" % (khalix_uba,analisis,variable,coeficient,ecap_ind,condicio_khalix_uba))
c.execute("alter table %s add index (indicador)" % khalix_uba)
c.execute("drop table if exists %s" % khalix_ubaEQD)
c.execute("create table %s (up varchar(5),uba varchar(5),tipus varchar(1),indicador varchar(10),analisis varchar(12),detalle varchar(12),valor double)" % khalix_ubaEQD)
c.execute("insert into %s select a.* from %s a inner join %s b on a.indicador=if(ind_codi like ('EQA%%'),replace(ind_codi,'EQA','EQAU'),if(ind_codi like ('EQD%%'),replace(ind_codi,'EQD','EQDU'),ind_codi))  where ind_codi like ('EQD%%')" % (khalix_ubaEQD,khalix_uba,ind))
c.execute("insert into %s select up, uba, a.tipus, replace(ind_codi,'EQD','EQDU'), analisis, detalle, valor  from %s a inner join %s b on a.indicador=if(grup_codi like ('EQA%%'),replace(grup_codi,'EQA','EQAU'),if(grup_codi like ('EQD%%'),replace(grup_codi,'EQD','EQDU'),grup_codi))  where grup_codi like ('EQD%%') and nets =0" % (khalix_ubaEQD,khalix_uba,ind))
c.execute("delete a.* from %s a inner join %s b on a.indicador=if(ind_codi like ('EQA%%'),replace(ind_codi,'EQA','EQAU'),if(ind_codi like ('EQD%%'),replace(ind_codi,'EQD','EQDU'),ind_codi))  where nets=1" % (khalix_uba,ind))

c.execute("drop table if exists %s,%s,%s,%s,%s" % (cataleg,catalegPare,catalegPunts,catalegExclosos,catalegKhx))
c.execute("create table %s (indicador varchar(10),literal varchar(80),ordre int,pare varchar(10),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),curt varchar(80),rsomin double,rsomax double,pantalla varchar(50), primary key(indicador,pantalla))" % cataleg)
c.execute("insert into %s select distinct a.grup_codi indicador,grup_desc literal,ordre_grup,grup2_codi pare,llistats llistat,invers,prof mdet,mmin,mint,mmax,1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',grup_codi) wiki,'' curt,''rsomin,''rsomax,'GENERAL' pantalla from %s a inner join %s b on a.grup_codi=b.indicador inner join %s c on a.grup_codi=c.indicador where baixa_=0" % (cataleg,ind,esperades,metesres))
c.execute("insert ignore into %s select distinct a.grup_codi indicador,grup_desc literal,ordre_grup,grup2_codi pare,llistats llistat,invers, 0 mdet, 0 mmin,0 mint, 0 mmax, 1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',grup_codi) wiki,'' curt,''rsomin,''rsomax,'GENERAL' pantalla from %s a where baixa_=0  and tipus<>16" % (cataleg,ind))
c.execute("insert ignore into %s select distinct a.ind_codi indicador,ind_desc literal,ordre_grup,grup_codi pare,llistats llistat,invers, 0 mdet, 0 mmin,0 mint, 0 mmax, 1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',grup_codi) wiki,'' curt,''rsomin,''rsomax,'GENERAL' pantalla from %s a where baixa_=0 and nets=1  and tipus<>16" % (cataleg,ind))
c.execute("update %s a inner join %s b on indicador=grup_codi set llistat=0 where nets=1" % (cataleg,ind))
#c.execute("update %s set mdet=0" % cataleg)
if not grip:
    c.execute("update %s set toShow=0 where indicador in %s" % (cataleg,gripInd))
c.execute("update %s set toShow=0 where indicador in %s" % (cataleg,hideInd))
c.execute("create table %s (pare varchar(10) not null default '',literal varchar(80) not null default '',ordre int,curt varchar(80),proces varchar(50))" % catalegPare)
c.execute("insert into %s select distinct grup2_codi pare,grup2_desc literal,ordre_grup2 ordre,'' curt,'' proces from %s where baixa_=0  and tipus<>16" % (catalegPare,ind))
c.execute("create table %s like %s" % (catalegPunts,ponderacio))
c.execute("insert into %s select * from %s" % (catalegPunts,ponderacio))
#c.execute("update %s set valor = 0" % catalegPunts)
for tipus in ('M', 'I'):
    c.execute("insert into {0} select indicador, '{1}', 0 from {2} a where not exists (select 1 from {0} b where a.indicador = b.indicador and tipus = '{1}')".format(catalegPunts, tipus, cataleg))
c.execute("create table %s as select ind_codi,ind_desc,grup_codi,grup_desc,grup2_codi,grup2_desc from %s where baixa_=0" % (catalegKhx,ind))
c.execute("create table %s (codi int,descripcio varchar(150),ordre int,valor int)" % catalegExclosos)
for r in ecap_cataleg_exclosos:
    codi,desc,ordre,valor=r[0],r[1],r[2],r[3]
    c.execute("insert into %s VALUES (%d,'%s',%d,%d)" % (catalegExclosos,codi,desc,ordre,valor))

taula="mst_eqa_literals_curts"
sql="drop table if exists {}".format(taula)
execute(sql,bd)
sql="create table {} (indicador varchar(10) not null default'',literal varchar(200) not null default'',curt varchar(80) not null default'')".format(taula)
execute(sql,bd)
path0 = os.path.realpath("../")
fitxer = path0.replace("\\","/") + "/02nod/dades_noesb/literals_curts.txt" 
sql="LOAD DATA LOCAL INFILE '{}' INTO TABLE {} FIELDS TERMINATED BY ';' LINES TERMINATED BY '\r\n'".format(fitxer,taula)
execute(sql,bd)
sql="update {0} a inner join {1} b on a.indicador=b.indicador set a.curt=b.curt".format(cataleg,taula)
execute(sql,bd)
sql="update {0} a inner join {1} b on a.pare=b.indicador set a.curt=b.curt".format(catalegPare,taula)
execute(sql,bd)
sql="update {0} a inner join {1} b  on a.indicador=b.indicador set rsomin=round(valor/100,2) where meta='AGMMINRES'".format(cataleg,metesOrigres)
execute(sql,bd)
sql="update {0} a inner join {1} b  on a.indicador=b.indicador set rsomax=round(valor/100,2) where meta='AGMMAXRES'".format(cataleg,metesOrigres)
execute(sql,bd)

conn.close()