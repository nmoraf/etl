# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


INDICADOR = "EQA0315A"


class Diabetis(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_farmacs()
        self.get_denominador()
        self.get_numerador()
        self.get_master()
        self.upload_data()

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, upinf, ubainf, edat, ates, \
                      trasplantat, sexe, seccio_censal, up_residencia, \
                      institucionalitzat, maca \
               from assignada_tot \
               where edat > 80"
        self.poblacio = {row[0]: row for row in u.getAll(sql, "nodrizas")}

    def get_farmacs(self):
        """."""
        self.farmacs = c.Counter()
        sql = "select id_cip_sec, farmac from eqa_tractaments_ep \
               where farmac = 22"
        for id, codi in u.getAll(sql, "nodrizas"):
            self.farmacs[id] += 1

    def get_denominador(self):
        """."""
        self.denominador = set(self.farmacs.keys())

    def get_numerador(self):
        """."""
        sql = "select id_cip_sec \
               from eqa_variables, dextraccio \
               where agrupador = 20 and \
                     data_var between adddate(data_ext, interval -1 year) and \
                                      data_ext and \
                     valor between 3 and 6.99 and \
                     usar = 1"
        self.numerador = set([id for id, in u.getAll(sql, "nodrizas")
                              if self.farmacs[id] > 1])

    def get_master(self):
        """."""
        self.master = [dades + (INDICADOR, 1 * (id in self.numerador), 0, 0, 0)
                       for id, dades in self.poblacio.items()
                       if id in self.denominador]

    def upload_data(self):
        """."""
        table = "{}_ind2".format(INDICADOR)
        cols = "(id_cip_sec int, up varchar(5), uba varchar(5), \
                 upinf varchar(5), ubainf varchar(5), edat int, ates int, \
                 trasplantat int, sexe varchar(1), seccio_censal varchar(10), \
                 up_residencia varchar(13), institucionalitzat int, maca int, \
                 ind_codi varchar(10), num int, excl int, ci int, clin int)"
        db = "eqa_ind"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.master, table, db)


if __name__ == "__main__":
    Diabetis()
