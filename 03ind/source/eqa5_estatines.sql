#DM2!
use eqa_ind;

drop table if exists eqa_estatines;
create table eqa_estatines (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
estatines double,
segueixen double,
data_e DATE NOT NULL DEFAULT 0,
rcv double,
data_rcv DATE NOT NULL DEFAULT 0,
num double,
num_217 double,
den double,
excl double,
ci double,
clin double,
primary key(id_cip_sec),
index(ind_codi)
)
select
	id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
	,0 as estatines
    ,0 as segueixen
	,0 as data_e
	,0 as rcv
	,0 as data_rcv
	,0 as num
    ,0 as num_217
	,1 as den
	,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a,eqa_ind c
where
	 a.edat between 35 and 74 and c.tipus=5 
group by
	id_cip_sec
;


drop table if exists min_estatines;
create table min_estatines(
id_cip_sec double null,
segueixen double,
data_e DATE NOT NULL DEFAULT 0,
index(id_cip_sec,data_e)
)
select
	id_cip_sec
    ,max(if(farmac in ('82', '658'), 1, 0)) as segueixen
	,min(pres_orig) as data_e
from
	nodrizas.eqa_tractaments
where
	farmac in ('82','216','658','659')
group by
	id_cip_sec
;


drop table if exists max_var;
create table max_var(
id_cip_sec double null,
valor double,
data_var DATE NOT NULL DEFAULT 0,
index(id_cip_sec,valor,data_var)
)
select
	b.id_cip_sec
	,valor
	,data_var
from
	min_estatines a
inner join
	nodrizas.eqa_variables b
on
	a.id_cip_sec=b.id_cip_sec
where
	agrupador=10 and data_var<=data_e
;


drop table if exists max_var2;
create table max_var2(
id_cip_sec double null,
data_var DATE NOT NULL DEFAULT 0,
index(id_cip_sec,data_var)
)
select
	id_cip_sec
	,max(data_var) as data_var
from
	max_var
group by 
	id_cip_sec
;

drop table if exists max_vare;
create table max_vare(
id_cip_sec double null,
valor double,
data_var DATE NOT NULL DEFAULT 0,
index(id_cip_sec,valor,data_var)
)
select
	b.id_cip_sec
	,valor
	,b.data_var
from
	max_var a
inner join
	max_var2 b
on
	a.id_cip_sec=b.id_cip_sec and a.data_var=b.data_var
;



update eqa_estatines a inner join min_estatines b on a.id_cip_sec=b.id_cip_sec,nodrizas.dextraccio
set estatines=1
,a.segueixen=b.segueixen
,a.data_e=b.data_e
where (b.data_e between date_add(data_ext,interval - 1 year) and data_ext) 
;


update eqa_estatines a inner join max_vare b on a.id_cip_sec=b.id_cip_sec
set rcv=valor
,data_rcv=data_var
;

update eqa_estatines a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec,nodrizas.dextraccio
set clin=1 where agrupador=197 and valor>320 and data_var <= data_ext
;

update eqa_estatines a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec,nodrizas.dextraccio
set clin=1 where agrupador=31 and valor>=200 and data_var <= data_ext
;

update eqa_estatines a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec,nodrizas.dextraccio
set clin=1 where agrupador=9 and valor>=240 and data_var <= data_ext
;


update eqa_estatines a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec, nodrizas.dextraccio
set excl=1 where ps =18 and datediff(data_ext,dde)/365 >8 and (rcv between 7 and 10)
;

update eqa_estatines a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in ('1','211','7','212','11','213','430','439','528','224','101')
;

update eqa_estatines a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec
set excl=1 where farmac in (694)
;

update eqa_estatines a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador = 824 and usar=1 and valor between 0.01 and 0.9
;

update eqa_estatines a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec
set excl=1 where agrupador = 9 and valor>190 and (rcv between 7 and 10)
;

drop table if exists excl_microalbuminuria;
create table excl_microalbuminuria(
id_cip_sec int,
ldl double,
index(id_cip_sec))
select id_cip_sec, 0 as ldl from nodrizas.eqa_microalb
;

update excl_microalbuminuria a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec
set ldl = 1 where agrupador=9 and usar = 1 and valor >= 100
;

update eqa_estatines a inner join excl_microalbuminuria b on a.id_cip_sec=b.id_cip_sec
set excl = 1 where ldl=1 and edat <75
;

update eqa_estatines a inner join excl_microalbuminuria b on a.id_cip_sec=b.id_cip_sec
set excl = 1 where rcv between 7 and 10
;

drop table if exists excl_irc;
create table excl_irc(
id_cip_sec int,
fg double,
index(id_cip_sec))
select id_cip_sec, 0 as fg from nodrizas.eqa_problemes where ps = 53
;

update excl_irc a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec
set fg = 1 where agrupador=30 and usar = 1 and valor <45
;

update eqa_estatines a inner join excl_irc b on a.id_cip_sec=b.id_cip_sec
set excl = 1 where rcv between 7 and 10 and fg=1
;

update eqa_estatines
set num=1 where rcv<10 and estatines=1 and segueixen=1
;

update eqa_estatines
set num_217=1 where rcv<10 and estatines=1
;

drop table if exists post_rcv;
create table post_rcv(
id_cip_sec double null,
index(id_cip_sec)
)
select
	b.id_cip_sec
from
	min_estatines a
inner join
	nodrizas.eqa_variables b
on
	a.id_cip_sec=b.id_cip_sec
where
	agrupador=10 and (data_var between data_e and date_add(data_e,interval + 3 month)) and valor >=10 
;

update eqa_estatines a inner join post_rcv b on a.id_cip_sec=b.id_cip_sec
set num = 0, num_217 = 0
;


set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=5);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=5
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2, eqa0217a_ind2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_estatines
where
	den=1
");

set @sql5 = concat("
create table eqa0217a_ind2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num_217 num
	,excl
   ,ci
   ,clin
from
	eqa_estatines
where
	den=1
");


PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s5 FROM @sql5;
EXECUTE s5;

SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();



set @maxg=(select distinct ind_codi from  eqa_ind where tipus=5);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;

drop table min_estatines;

drop table max_vare;
drop table max_var;
drop table max_var2;
drop table eqa_estatines
;

