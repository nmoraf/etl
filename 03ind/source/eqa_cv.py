# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter

nod = 'nodrizas'
eqa = 'eqa_ind'

criteris_khalix = 'ates = 1 and institucionalitzat = 0 and excl = 0 and den = 1'

ind_cv = {
        'propis': ['EQA3000A'],
        'fromEQA': ['EQA3003A', 'EQA3001A','EQA3002A','EQA3004A', 'EQA3005A', 'EQA3006A', 'EQA3007A', 'EQA3008A'],
        'aprofitats': {'EQA0201': 'RESTRAC', 'EQA0202': 'RESTRAC', 'EQA0203': 'RESTRAC', 'EQA0204': 'PREVSEC', 'EQA0205': 'PREVSEC',
                       'EQA0304': 'PREVPRIM', 'EQA0305': 'PREVPRIM', 'EQA0206': 'RESTRAC', 'EQA0207': 'RESTRAC', 'EQD0238': 'QDIAG',
                       'EQA0212': 'PREVPRIM', 'EQA0213': 'PREVPRIM', 'EQA0214': 'PREVPRIM', 'EQA0215': 'SEGUIMENT', 'EQA0216': 'PREVQUAT',
                       'EQA0238': 'PREVQUAT', 'EQA0235': 'PREVPRIM', 'EQD0240': 'QDIAG', 'EQD0241': 'QDIAG'}
        }

indicadors = {
                'default': {
                            'tmin': 24000,
                            'tmax': 0,
                            'where': '',
                            },
                'EQA3000A': {
                            'literal': 'Control de la TAO en AP',
                            'trasplantat': 0,
                            'edat_min': 15,
                            'edat_max': 89,
                            'maca': 0,
                            'den': {
                                    3: {
                                        'table': 'eqa_tractaments',
                                        },
                                    },
                            'num': {
                                    'DSINT': {
                                        'table': 'eqa_variables_tao',
                                        'data': 'data',
                                        'tmin': 11,
                                        'tmax': 0,
                                        'where': 'usar = 6',
                                        },
                                    },
                            'excl': {549: {
                                        'table': 'eqa_variables',
                                        'where': 'usar = 1 and valor = 1',
                                         },
                                    },
                            'dimensio': 'CAPRESOL',
                            'proces': 'MCV',
                          },
                'EQA3003A': {
                            'literal': 'Pacients amb CI/AVC amb ingrés per esdeveniment cardiovascular',
                            'hide': True,
                            'dimensio': 'RESEVEN',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3001A': {
                            'literal': 'Presa de la TA en la malaltia cardiovascular',
                            'hide': True,
                            'dimensio': 'SEGUIMENT',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3002A': {
                            'literal': 'Mesura de LDL en prevenció secundària',
                            'hide': True,
                            'dimensio': 'SEGUIMENT',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3004A': {
                            'literal': 'Pacients sense CI/AVC amb ingrés per e`pi cardiovascular',
                            'hide': True,
                            'dimensio': 'RESEVEN',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3005A': {
                            'literal': 'Pacients amb fibril·lació auricular (FA) amb ingrés per esdeveniment tromboembòlic',
                            'hide': True,
                            'dimensio': 'RESEVEN',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3006A': {
                            'literal': 'Pacients amb fibril·lació auricular (FA) amb ingrés per esdeveniment hemorràgic',
                            'hide': True,
                            'dimensio': 'RESEVEN',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3008A': {
                            'literal': 'Pacients amb insuficiència cardíaca amb ingrés per descompensació',
                            'hide': True,
                            'dimensio': 'RESEVEN',
                            'proces': 'MCV',
                            'invers': True
                          },
                'EQA3007A': {
                            'literal': 'Taxa estandarditzada de mortalitat de pacients amb malaltia cardiovascular',
                            'hide': True,
                            'dimensio': 'RESEVEN',
                            'proces': 'MCV',
                            'invers': True
                          },          
            }

def get_data_ext():
    sql = 'select data_ext from dextraccio'
    dext,= getOne(sql, nod)
    return dext

def get_poblacio():
    pob = {}
    sql = 'select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, institucionalitzat, maca from assignada_tot'
    for id, up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, institucionalitzat, maca in getAll(sql, nod):
        pob[id] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'edat': edat, 'sexe': sexe, 'ates': ates, 'trasplantat': trasplantat, 'sc': seccio_censal, 'up_r': up_residencia, 'insti': institucionalitzat, 'maca': maca}
    return pob
    
def get_indicador(indicador, pob, dext):
    dades = {}
    trasplantats = indicadors[indicador]['trasplantat']
    maques = indicadors[indicador]['maca']
    edat_min = indicadors[indicador]['edat_min']
    edat_max = indicadors[indicador]['edat_max']
    agr_den = indicadors[indicador]['den']
    for den1 in agr_den:
        table =  indicadors[indicador]['den'][den1]['table']
        sql = 'select id_cip_sec from {0} where farmac = {1}'.format(table, den1)
        for id, in getAll(sql, nod):
            try:
                up = pob[id]['up']
                uba = pob[id]['uba']
                upinf = pob[id]['upinf']
                ubainf = pob[id]['ubainf']
                edat = pob[id]['edat']
                ates = pob[id]['ates']
                trasplantat = pob[id]['trasplantat']
                insti = pob[id]['insti']
                maca = pob[id]['maca']
            except KeyError:
                continue
            if edat_min <= edat <= edat_max:
                if trasplantat == trasplantats and maca == maques:
                    dades[id] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'ates': ates, 'insti': insti, 'num': 0, 'excl': 0}
    agr_num = indicadors[indicador]['num']
    for num1 in agr_num:
        table =  indicadors[indicador]['num'][num1]['table']
        t_min =  indicadors[indicador]['num'][num1]['tmin']
        t_max =  indicadors[indicador]['num'][num1]['tmax']
        where =  indicadors[indicador]['num'][num1]['where']
        data =  indicadors[indicador]['num'][num1]['data']
        if indicador == 'EQA3000A':
            sql = "select id_cip_sec, {0} from {1} where agrupador = '{2}' and {3}".format(data, table, num1, where)
        else:
            sql = 'select id_cip_sec, {0} from {1} where agrupador = {2} and {3}'.format(data, table, num1, where)
        for id, dat in getAll(sql, nod):
            mesos = monthsBetween(dat, dext)
            if t_max <= mesos <= t_min:
                try:
                    dades[id]['num'] = 1
                except KeyError:
                    continue
    agr_excl = indicadors[indicador]['excl']
    for excl1 in agr_excl:
        table =  indicadors[indicador]['excl'][excl1]['table']
        where =  indicadors[indicador]['excl'][excl1]['where']
        sql = 'select id_cip_sec from {0} where agrupador ={1} and {2}'.format(table, excl1, where)
        for id, in getAll(sql, nod):
            try:
                dades[id]['excl'] = 1
            except KeyError:
                continue
    upload = []
    for (id), vals in dades.items():
        upload.append([id, indicador, indicadors[indicador]['dimensio'], vals['up'], vals['uba'], vals['upinf'], vals['ubainf'], vals['ates'], vals['insti'], 1, vals['num'], vals['excl']])
    return upload


printTime()
tableMy = 'mst_indicadors_pacient_mcv'
create = "(id_cip_sec double null,ind_codi varchar(10) not null default'',dimensio varchar(10) not null default '',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',ates double, institucionalitzat double, den double,num double,excl double)"
createTable(tableMy,create,eqa, rm=True)        

tb_cat = 'mst_cataleg_mcv'
create = "(indicador varchar(10), literal varchar(255), dimensio varchar(10), is_eqa int, to_show int, is_invers int, has_llistat int)"
createTable(tb_cat, create, eqa, rm=True)

for indicador in ind_cv['propis']:
    dext = get_data_ext()
    pob = get_poblacio()
    upload = get_indicador(indicador, pob, dext)
    listToTable(upload, tableMy, eqa)
    cataleg = [(indicador, indicadors[indicador]['literal'], indicadors[indicador]['dimensio'], 0, 1 - indicadors[indicador].get('hide', False), 1 * indicadors[indicador].get('invers', False), 1 * indicadors[indicador].get('llistat', True))]
    listToTable(cataleg, tb_cat, eqa)

for indicador in ind_cv['fromEQA']:
    taulaI = indicador + '_ind2'
    sql = "delete a.* from {0} a where ind_codi='{1}'".format(tableMy, indicador)
    execute(sql, eqa)
    sql = "insert into {0} select id_cip_sec, ind_codi, '{1}', up, uba, upinf, ubainf, ates, institucionalitzat, 1, num, excl from {2}".format(tableMy, indicadors[indicador]['dimensio'], taulaI)
    execute(sql, eqa)
    cataleg = [(indicador, indicadors[indicador]['literal'], indicadors[indicador]['dimensio'], 0, 1 - indicadors[indicador].get('hide', False), 1 * indicadors[indicador].get('invers', False), 1 * indicadors[indicador].get('llistat', True))]
    listToTable(cataleg, tb_cat, eqa)
    
for indicador, dimensio in ind_cv['aprofitats'].items():
    cataleg = [(indicador, '', dimensio, 1, 9, 9, 9)]
    listToTable(cataleg, tb_cat, eqa)

printTime()
