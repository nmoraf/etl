
#Indicadors
#Aqu� �s quan agafem els criteris de compliment sense tal tractaments
#Es poden tenir indicadors que al denominador o al numerador no tinguin un tractament en concret

#Primer faig taula assignacio amb instit, maca i atesa

drop table if exists assignada_tot;
create table assignada_tot (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
pcc int,
ates double,
trasplantat double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
    ,pcc
	,ates
    ,trasplantat
from
	nodrizas.assignada_tot_with_jail a
where
	edat >14
;


set @eqa='ind';
set @eqa1='num';
set @eqa2='den';
set @eqa3='excl';
set @eqa4='edat';
set @eqa5='ci';
set @eqa6='clin';


set @maxt=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16)) z);


drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16)
;

drop procedure if exists eqa_ind;

delimiter //
CREATE PROCEDURE eqa_ind()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"1
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"1 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
den double,
excl double,
ci double,
clin double,
index(id_cip_sec,ind_codi,num,excl,ci,clin)
)
select
	a.id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,b.ind_codi
	,0 as num
	,1 as den
	,0 as excl
   ,0 as ci
   ,0 as clin
from
   assignada_tot a
inner join
   ",@ind,"_",@eqa2," b
on
   a.id_cip_sec=b.id_cip_sec
group by
   a.id_cip_sec
");

set @sql3 = concat("
update ",@ind,"_",@eqa,"1 a inner join ",@ind,"_",@eqa1," b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set ",@eqa1,"=1
");


set @sql5 = concat("
update ",@ind,"_",@eqa,"1 a inner join ",@ind,"_",@eqa3," b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set ",@eqa3,"=1
");

set @sql51 = concat("
update ",@ind,"_",@eqa,"1 a inner join ",@ind,"_",@eqa5," b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set ",@eqa5,"=1
");

set @sql52 = concat("
update ",@ind,"_",@eqa,"1 a inner join ",@ind,"_",@eqa6," b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set ",@eqa6,"=1
");


set @sql6 = concat("
update ",@ind,"_",@eqa,"1 a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_prescripcions_den c on b.farmac=c.agrupador
set ",@eqa2,"=2 where c.ind_codi='",@ind,"'  and sense=1 
and pres_orig <= final
	and data_fi>= inici
");

set @sql7 = concat("
update ",@ind,"_",@eqa,"1 a inner join eqa_relacio_prescripcions_den c on a.ind_codi=c.ind_codi
set ",@eqa2,"=1 where ",@eqa2,"<>2 and sense=1 and c.ind_codi='",@ind,"' 
");

set @sql8 = concat("
update ",@ind,"_",@eqa,"1 
set ",@eqa2,"=0 where ",@eqa2,"=2
");


set @sql10 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql11 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	",@ind,"_",@eqa,"1
where
	den=1 
");

set @sql12 = concat(" drop table if exists ",@ind,"_",@eqa,"1");

set @sql13 = concat("
update ",@ind,"_",@eqa,"2 a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_prescripcions_num c on b.farmac=c.agrupador
set ",@eqa1,"=2 where c.ind_codi='",@ind,"'  and sense=1 
and pres_orig <= final
	and data_fi>= inici
");

set @sql14 = concat("
update ",@ind,"_",@eqa,"2 a inner join eqa_relacio_prescripcions_num c on a.ind_codi=c.ind_codi
set ",@eqa1,"=1 where ",@eqa1,"<>2 and sense=1 and c.ind_codi='",@ind,"' 
");


set @sql15 = concat("
update ",@ind,"_",@eqa,"2 
set ",@eqa1,"=0 where ",@eqa1,"=2
");


	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s5 FROM @sql5;
EXECUTE s5;

PREPARE s51 FROM @sql51;
EXECUTE s51;

PREPARE s52 FROM @sql52;
EXECUTE s52;

PREPARE s6 FROM @sql6;
EXECUTE s6;

PREPARE s7 FROM @sql7;
EXECUTE s7;

PREPARE s8 FROM @sql8;
EXECUTE s8;

PREPARE s10 FROM @sql10;
EXECUTE s10;

PREPARE s11 FROM @sql11;
EXECUTE s11;

PREPARE s12 FROM @sql12;
EXECUTE s12;

PREPARE s13 FROM @sql13;
EXECUTE s13;

PREPARE s14 FROM @sql14;
EXECUTE s14;

PREPARE s15 FROM @sql15;
EXECUTE s15;



SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ind();


drop table if exists indicadors_eqa1
;

#Hi ha indicadors que els fem per separat perqu� tenen com a compliment criteris tipus A+B o C+D, dif�cils de parametritzar.
#Poso aquests indicadors en una taula i fem un petit proc�s per ajuntar compliments.


set @maxt=(select count(distinct ind_codi) from  eqa_conjunts);

set @eqa='ind';

drop table if exists agrupadorscj;
create table agrupadorscj (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct  ind_codi, 0 usar from eqa_conjunts
;

drop procedure if exists eqa_conjunts;

delimiter //
CREATE PROCEDURE eqa_conjunts()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorscj set usar=0;
update agrupadorscj set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorscj where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2_prov
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2_prov (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec,ind_codi)
)
");


PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_conjunts();


set @maxt=(select count(distinct ind_codi_o) from  eqa_conjunts);

set @eqa='ind';

drop table if exists agrupadorscj;
create table agrupadorscj (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi_o VARCHAR(10) NOT NULL DEFAULT'',
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct  ind_codi_o, ind_codi, 0 usar from eqa_conjunts
;

drop procedure if exists eqa_conjunts;

delimiter //
CREATE PROCEDURE eqa_conjunts()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorscj set usar=0;
update agrupadorscj set usar=1 where id=@num;


set @ind=(select ind_codi_o from agrupadorscj where usar=1);
set @ind2=(select ind_codi from agrupadorscj where usar=1);

set @sql1 = concat("
insert into ",@ind2,"_",@eqa,"2_prov 
select * from ",@ind,"_",@eqa,"2
");

PREPARE s1 FROM @sql1;
EXECUTE s1;

SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_conjunts();



set @maxt=(select count(distinct ind_codi) from  eqa_conjunts);

set @eqa='ind';

drop table if exists agrupadorscj;
create table agrupadorscj (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct  ind_codi, 0 usar from eqa_conjunts
;

drop procedure if exists eqa_conjunts;

delimiter //
CREATE PROCEDURE eqa_conjunts()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorscj set usar=0;
update agrupadorscj set usar=1 where id=@num;

set @ind=(select ind_codi from agrupadorscj where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec,ind_codi)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,'",@ind,"' as ind_codi
	,max(num) as num
	,max(excl) as excl
   ,max(ci) as ci
   ,max(clin) as clin
from
	",@ind,"_",@eqa,"2_prov
group by
	id_cip_sec
");


set @sql3 = concat("
drop table if exists ",@ind,"_",@eqa,"2_prov
");



PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;


SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_conjunts();

delete a.* from eqa_ind a inner join eqa_conjunts b on a.ind_codi=b.ind_codi_o where right(a.ind_codi,1)='B';

