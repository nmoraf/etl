# coding: iso-8859-1
import sisapUtils as u
from collections import defaultdict
import csv
import os


u.printTime('Inici càlcul residències')
db = "eqa_ind"
imp = "import"
eqa = "eqa_ind"
nod = "nodrizas"

path0 = os.path.realpath("../")
pathedat = path0.replace("\\", "/") + "/03ind/dades_noesb/eqa_edats.txt"

pac = "mst_indicadors_pacient"
pac_esp = 'mst_indicadors_pacient_resis_especifics'
condicio_resis = "excl = 0 and ci = 0 and clin = 0 and institucionalitzat = 1"

tableK = "exp_khalix_EQAresis_up"
tableE = "mst_indicadors_pacient_resis"
OutFile = u.tempFolder + 'eqa_residencies.txt'
cataleg = "cat_residencies"  # se vuelve a asignar la variable antes de usarse en linea 153: cataleg = "exp_ecap_resis_cataleg" que es un catalogo de indicadores,  esta linea creo que se puede eliminar
u11 = "import.u11"

indicadors = ('EQA0201', 'EQA0202', 'EQA0203', 'EQA0204', 'EQA0205', 'EQA0206', 'EQA0207', 'EQD0238', 'EQA0208', 'EQA0209', 'EQA0212', 'EQD0239', 'EQA0213', 'EQA0235', 'EQD0240', 'EQD0241', 'EQA0220', 'EQA0221', 'EQD0242',
              'EQA0230', 'EQA0219', 'EQA0227', 'EQA0501', 'EQA0502', 'EQA0308', 'EQA0222', 'EQA0309', 'EQA0310', 'EQA0312', 'EQA0216', 'EQA0238', 'EQA0224', 'EQA0228', 'EQA0229', 'EQA0232', 'EQA0313')
especifics = {
    'EQA0401': {'den': [],
                'num': [(1, 'variables', 87, (0, 11), None), (1, 'variables', 90, (0, 11), None),
                        (2, 'problemes', 86, None, None), (2, 'variables', 88, (0, 11), None), (2, 'variables', 90, (0, 11), None)],
                'excl': None,
                'clin': None,
                'ci': None},
    'EQA0402': {'den': [(1, 'variables', 87, (0, 11), (0, 39))],
                'num': [(1, 'problemes', 93, (0, 11), None), (1, 'variables', 94, (0, 11), None), (1, 'variables', 95, None, None)],
                'excl': None,
                'clin': [(1, 'problemes', 91, (0, 11), None)],
                'ci': [(1, 'problemes', 92, (0, 11), None)]},
}


def get_especifics():
    agrupadors = {'problemes': set(), 'variables': set()}
    criteris_invers = defaultdict(set)
    for indicador, conceptes in especifics.items():
        for concepte, criteris in conceptes.items():
            if criteris:
                for criteri, taula, agrupador, temps, valors in criteris:
                    agrupadors[taula].add(agrupador)
                    criteris_invers[agrupador].add((temps, valors, indicador, concepte, criteri))

    poblacio = {row[0]: row[1:] for row in u.getAll('select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, 0, seccio_censal, up_residencia, institucionalitzat, maca \
                                                   from assignada_tot where edat > 14 and institucionalitzat = 1', nod)}
    poblacio_set = set(poblacio)
    criteri_sets = defaultdict(lambda: defaultdict(set))
    dext, = u.getOne('select data_ext from dextraccio', nod)
    querys = ('select id_cip_sec, agrupador, data_var, valor from eqa_variables where agrupador in {}'.format(tuple(agrupadors['variables'])),
              'select id_cip_sec, ps, dde, null from eqa_problemes where ps in {}'.format(tuple(agrupadors['problemes'])))
    for query in querys:
        for id, agr, dat, val in u.getAll(query, nod):
            if id in poblacio_set:
                for temps, valors, indicador, concepte, criteri in criteris_invers[agr]:
                    if not temps or temps[0] <= u.monthsBetween(dat, dext) <= temps[1]:
                        if not valors or valors[0] <= val <= valors[1]:
                            criteri_sets[(indicador, concepte)][criteri].add(id)

    concepte_sets = {indicador: {concepte: poblacio_set if concepte == 'den' and not especifics[indicador][concepte] else set() for concepte in especifics[indicador]} for indicador in especifics}
    for (indicador, concepte), criteris in criteri_sets.items():
        concepte_sets[indicador][concepte] = set.intersection(*criteris.values())

    u.createTable(pac_esp, 'like {}'.format(pac), db, rm=True)
    for indicador, conceptes in concepte_sets.items():
        upload = ((id, '{}A'.format(indicador), indicador) + poblacio[id] + (int(id in conceptes['num']), 1, int(id in conceptes['excl']), int(id in conceptes['ci']), int(id in conceptes['clin']), int(id not in conceptes['num'])) for id in conceptes['den'])
        u.listToTable(upload, pac_esp, db)


get_especifics()


for gripp, gripInd in u.getAll("select grip,grip_ind from nodrizas.dextraccio", nod):
    grip = True if gripp == 1 else False

edats = {}
with open(pathedat, 'rb') as file:
    p = csv.reader(file, delimiter='@')
    for i in p:
        indicador, emin, emax = i[0], i[1], i[2]
        emax = int(emax)
        emin = int(emin)
        if emax == 89:
            emax = 200
        edats[indicador] = {'emin': emin, 'emax': emax}


eqaResidencies, eqaResidenciesECAP = {}, {}
sql = "select id_cip_sec, up, uba, upinf, ubainf, up_residencia, edat, sexe, ates, ind_codi, grup_codi, llistat, num, den from {0} where {1} and grup_codi in {2} \
       union select id_cip_sec, up, uba, upinf, ubainf, up_residencia, edat, sexe, ates ,ind_codi,grup_codi, llistat, num, den from {3} where {1}".format(pac, condicio_resis, indicadors, pac_esp)
for id, up, uba, upinf, ubainf, resi, edat, sexe, ates, indicador, grup, llistat, num, den in u.getAll(sql, eqa):
    sex = u.sexConverter(sexe)
    ates = int(ates)
    edat = int(edat)
    try:
        emin = edats[indicador]['emin']
        emax = edats[indicador]['emax']
    except KeyError:
        continue
    if emin <= edat <= emax:
        if (up, resi, u.ageConverter(edat, 5), sex, indicador, ates) in eqaResidencies:
            eqaResidencies[(up, resi, u.ageConverter(edat, 5), sex, indicador, ates)]['num'] += num
            eqaResidencies[(up, resi, u.ageConverter(edat, 5), sex, indicador, ates)]['den'] += den
        else:
            eqaResidencies[(up, resi, u.ageConverter(edat, 5), sex, indicador, ates)] = {'num': num, 'den': den}
        eqaResidenciesECAP[(id, up, uba, upinf, ubainf, resi, edat, sexe, ates, indicador, grup, num, den, llistat)] = True
    else:
        continue

u.printTime('export khalix')
with u.openCSV(OutFile) as c:
    for (up, resi, edat, sexe, ind, ates), d in eqaResidencies.items():
        c.writerow([up, resi, edat, sexe, ind, ates, d['num'], d['den']])
u.execute('drop table if exists {}'.format(tableK), db)
u.execute("""
          create table {} (
              up VARCHAR(5)  NOT NULL DEFAULT '',
              up_resi varchar(13) NOT NULL DEFAULT '',
              edat VARCHAR(10) NOT NULL DEFAULT '',
              sexe VARCHAR(5) NOT NULL DEFAULT '',
              indicador VARCHAR(10) NOT NULL DEFAULT '',
              ates int,
              num double null,
              den double null)
    """.format(tableK), db)
u.loadData(OutFile, tableK, db)

u.printTime('export ecap')

with u.openCSV(OutFile) as c:
    for (id, up, uba, upinf, ubainf, resi, edat, sexe, ates, indicador, grup, num, den, llistat), d in eqaResidenciesECAP.items():
        c.writerow([id, up, uba, upinf, ubainf, resi, edat, sexe, ates, indicador, grup, num, den, llistat])
u.execute('drop table if exists {}'.format(tableE), db)
u.execute("create table {} (id_cip_sec int,up VARCHAR(5)  NOT NULL DEFAULT '', uba VARCHAR(5)  NOT NULL DEFAULT '',upinf VARCHAR(5)  NOT NULL DEFAULT '',ubainf VARCHAR(5)  NOT NULL DEFAULT '',\
        up_residencia varchar(13)  NOT NULL DEFAULT '',edat double, sexe VARCHAR(5) NOT NULL DEFAULT '', ates int, ind_codi VARCHAR(10) NOT NULL DEFAULT '',grup_codi varchar(25) not null default'',\
        num double null, den double null,llistat int)".format(tableE), db)
u.loadData(OutFile, tableE, db)

path0 = os.path.realpath("../")
path1 = path0.replace("\\", "/") + "/03ind/dades_noesb/"
file = 'eqa_ind.txt'
eqaInd = file[0:-4]
OutfileInd = path1 + file
u.execute("DROP TABLE IF EXISTS {}".format(eqaInd), db)
u.execute("create table {}(id double,ind_codi VARCHAR(10) NOT NULL DEFAULT'', ind_desc VARCHAR(150) NOT NULL DEFAULT'',grup_codi VARCHAR(7) NOT NULL DEFAULT'',grup_desc VARCHAR(150) NOT NULL DEFAULT'',\
        grup2_codi VARCHAR(7) NOT NULL DEFAULT'',grup2_desc VARCHAR(150) NOT NULL DEFAULT'',codi_antic VARCHAR(10) NOT NULL DEFAULT'',fitxa VARCHAR(200) NOT NULL DEFAULT'',tipus double NULL,llistats int,\
        invers int,girar int,ordre_grup int,ordre_grup2 int,baixa_ int,nets int,primary key(ind_codi),index(grup_codi))".format(eqaInd), db)
u.loadData(OutfileInd, eqaInd, db, rm=False)

ecap_ind = "exp_ecap_resis_uba"
ecap_pac_pre = "pre_exp_ecap_resis_pacient"
ecap_pac = "exp_ecap_resis_pacient"
cataleg = "exp_ecap_resis_cataleg"
catalegPare = "exp_ecap_resis_catalegPare"
inversos = "inversos_resis"

criteri_llistats = "ates=1 and llistat=1"

u.execute("drop table if exists {}".format(ecap_pac_pre), db)
u.execute("create table {} (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',up_residencia varchar(13) not null default'',toShow int)".format(ecap_pac_pre), db)
u.execute("insert into {0} select id_cip_sec,up,uba,upinf,ubainf,grup_codi,up_residencia,1 from {1} where {2}".format(ecap_pac_pre, tableE, criteri_llistats), db)
u.execute("delete a.* from {0} a inner join {1} b on a.grup_codi=b.grup_codi where nets=1".format(ecap_pac_pre, eqaInd), db)
u.execute("insert into {0} select id_cip_sec,up,uba,upinf,ubainf,a.ind_codi,up_residencia,1 from {1} a inner join {2} b on a.ind_codi=b.ind_codi where {3} and nets=1".format(ecap_pac_pre, tableE, eqaInd, criteri_llistats), db)
u.execute("alter table {} add index(id_cip_sec)".format(ecap_pac_pre), db)

u.execute("drop table if exists {}".format(ecap_pac), db)
u.execute("create table {} (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,up_residencia varchar(13) not null default'',hash_d varchar(40) not null default '',sector varchar(4) not null default '')".format(ecap_pac), db)
u.execute("insert into {0} select a.id_cip_sec,up,uba,upinf,ubainf,grup_codi,0 exclos,up_residencia,hash_d,codi_sector sector from {1} a inner join {2} b on a.id_cip_sec=b.id_cip_sec where toShow=1 group by a.id_cip_sec,up,uba,upinf,ubainf,grup_codi,exclos,up_residencia, hash_d,codi_sector".format(ecap_pac, ecap_pac_pre, u11), db)


u.execute("drop table if exists {}".format(ecap_ind), db)
u.execute("create table {} (up varchar(5) not null default '',uba varchar(7) not null default '',up_residencia varchar(13) not null default'',tipus varchar(1) not null default '',indicador varchar(10) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int)" .format(ecap_ind), db)
u.execute("insert into {0} select up,uba,'RES','M',grup_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} where ates=1 group by up,uba,grup_codi".format(ecap_ind, tableE), db)
u.execute("insert into {0} select upinf,ubainf,'RES','I',grup_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} where ates=1  group by upinf,ubainf,grup_codi".format(ecap_ind, tableE), db)
u.execute("insert into {0} select up,uba,'RES','M',a.ind_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} a inner join {2} b on a.ind_codi=b.ind_codi where nets=1 and ates=1 group by up,uba,ind_codi".format(ecap_ind, tableE, eqaInd), db)
u.execute("insert into {0} select upinf,ubainf,'RES','I',a.ind_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} a inner join {2} b on a.ind_codi=b.ind_codi where nets=1 and ates=1  group by upinf,ubainf,ind_codi".format(ecap_ind, tableE, eqaInd), db)
u.execute("insert into {0} select up,uba,up_residencia,'M',grup_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} where ates=1 group by up,uba,up_residencia,grup_codi".format(ecap_ind, tableE), db)
u.execute("insert into {0} select upinf,ubainf,up_residencia,'I',grup_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} where ates=1  group by upinf,ubainf,up_residencia,grup_codi".format(ecap_ind, tableE), db)
u.execute("insert into {0} select up,uba,up_residencia,'M',a.ind_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} a inner join {2} b on a.ind_codi=b.ind_codi where nets=1 and ates=1 group by up,uba,up_residencia,ind_codi".format(ecap_ind, tableE, eqaInd), db)
u.execute("insert into {0} select upinf,ubainf,up_residencia,'I',a.ind_codi,'',sum(den),sum(num),'','',sum(num)/sum(den),sum(num)/sum(den),'','',0 from {1} a inner join {2} b on a.ind_codi=b.ind_codi where nets=1 and ates=1  group by upinf,ubainf,up_residencia,ind_codi".format(ecap_ind, tableE, eqaInd), db)

u.execute("drop table if exists {}".format(inversos), db)
u.execute("create table {} (indicador varchar(10) not null default '',invers int,unique(indicador,invers))".format(inversos), db)
u.execute("insert into {0} select distinct grup_codi indicador,invers from {1} where baixa_=0".format(inversos, eqaInd), db)
u.execute("insert into {0} select distinct ind_codi indicador,invers from {1} where baixa_=0 and nets=1".format(inversos, eqaInd), db)
u.execute("update {0} a inner join {1} b on a.indicador=b.indicador set llistat=if(invers=1,resolts,detectats-resolts)".format(ecap_ind, inversos), db)

u.execute("drop table if exists {0},{1}".format(cataleg, catalegPare), db)
u.execute("create table {}(indicador varchar(10),literal varchar(80),ordre int,pare varchar(10),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),curt varchar(80),rsomin double,rsomax double, primary key(indicador))".format(cataleg), db)
fileOut = 'cataleg.txt'
catalegs, catalegsPare = {}, {}
sql = "select ind_codi,ind_desc,grup_codi,grup_desc,grup2_codi,grup2_desc,llistats,invers,girar,ordre_grup,ordre_grup2,nets from {} where baixa_=0".format(eqaInd)
for ind, desc, grup, grupdesc, grup2, grup2desc, llistat, invers, girar, ordre_grup, ordre_grup2, nets in u.getAll(sql, db):
    if grup in indicadors or grup in especifics:
        wiki = 'http://sisap-umi.eines.portalics/indicador/codi/' + grup
        catalegs[(grup, grupdesc, ordre_grup, grup2, llistat, invers, '', '', '', '', 1, wiki, '', '', '')] = True
        catalegsPare[(grup2, grup2desc, ordre_grup2, '')] = True
        if nets == 1:
            catalegs[(ind, desc, ordre_grup, grup, 0, invers, '', '', '', '', 1, wiki, '', '', '')] = True
with u.openCSV(fileOut) as c:
    for (ind, desc, ordre_grup, grup, llistat, invers, a1, a2, a3, a4, toshow, wiki, a5, a6, a7), d in catalegs.items():
        c.writerow([ind, desc, ordre_grup, grup, llistat, invers, a1, a2, a3, a4, toshow, wiki, a5, a6, a7])
u.loadData(fileOut, cataleg, db)
if not grip:
    u.execute("update {0} set toShow=0 where indicador in {1}".format(cataleg, gripInd), db)
u.execute("create table {0} (pare varchar(8) not null default '',literal varchar(80) not null default '',ordre int,curt varchar(80))".format(catalegPare), db)
with u.openCSV(fileOut) as c:
    for (grup2, grup2desc, ordre_grup2, curt), d in catalegsPare.items():
        c.writerow([grup2, grup2desc, ordre_grup2, curt])
u.loadData(fileOut, catalegPare, db)

u.printTime('Fi càlcul residències')
