# coding: iso-8859-1
from sisapUtils import *
import csv
import os
import sys
from time import strftime
from collections import defaultdict, Counter

imp = "import"
eqa = "eqa_ind"
familiars = "familiars"
taulaDesti = '_ind2'
OutFile = tempFolder + 'familiars.txt'
TableEQA = 'eqa_familiars'

indicadors = ['EQA0313A']

FamiliarsDict = {
        'EQA0313A': {
            'codis': "('C61','D07.5','C01-C61','C01-D07.5')",
            'antecedent': "af_pare",
            'edatmin': 1,
            'edatmax': 59,
            'tipus': 'excl'
        },
}

for ind in indicadors:
    columna = FamiliarsDict[ind]['antecedent'] + '_edat'
    taula = ind + taulaDesti
    with open(OutFile, 'wb') as file:
        w = csv.writer(file, delimiter='@', quotechar='|')
        sql = "select id_cip_sec, af_data_a from {0} where af_cod_ps in {1} and {2} between {3} and {4} and af_data_b=0".format(familiars, FamiliarsDict[ind]['codis'], columna, FamiliarsDict[ind]['edatmin'], FamiliarsDict[ind]['edatmax'])
        for id, data in getAll(sql, imp):
            w.writerow((id, data))
    execute('drop table if exists {}'.format(TableEQA), eqa)
    execute('create table {} (id_cip_sec int, data date, index(id_cip_sec))'.format(TableEQA), eqa)
    loadData(OutFile, TableEQA, eqa)
    sql = "update {0} a inner join {1} b on a.id_cip_sec=b.id_cip_sec set {2} = 1".format(taula, TableEQA, FamiliarsDict[ind]['tipus'])
    execute(sql, eqa)
