#Aquest tipus d indicador �s el d indicadors que el numerador es relaciona amb data amb el denominador
#per q�estions d eficiencia i tenint en compte com s�n els indicadors d EQA, es t� en compte nom�s:
#pel denominador: problemes de salut; o sexe.
#Pel numerador: tractaments o variables (inclou laboratori, proves, actuacions, etc.). Nom�s un criteri de compliment tipus A o B no tipus A+B
#Per exclusions: tractaments o problemes. Nom�s un criteri de exlcusi� tipus A o B no tipus A+B

use eqa_ind;

#necessito fer taula dels codis de problemes incidents de les agudes per quedar-me nom�s amb ells!

drop table if exists psi_agudes;
create table psi_agudes(
ind_codi varchar(10) not null default'',
agrupador double,
index(ind_codi,agrupador)
)
select a.ind_codi,agrupador from eqa_relacio_agudes_problemes_den a where  data_min<>2000
;

set @eqaag='den';

set @maxag=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3) z);

drop table if exists agrupadorsag;
create table agrupadorsag (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3
;


drop table if exists recompte_ag;
create table recompte_ag(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqaag group by ind_codi
;

drop procedure if exists eqa_ag;

delimiter //
CREATE PROCEDURE eqa_ag()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num1=p;


update agrupadorsag set usar=0;
update agrupadorsag set usar=1 where id=@num1;


set @indag=(select ind_codi from agrupadorsag where usar=1);

set @sql1 = concat("
drop table if exists ",@indag,"_",@eqaag,"1
");

set @sql2 = concat("
create table ",@indag,"_",@eqaag,"1 (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
agrupador double,
data DATE NOT NULL DEFAULT 0
)
");


set @sql3 = concat("
insert  into ",@indag,"_",@eqaag,"1
select
	id_cip_sec
	,'",@indag,"' as ind_codi
	,ps as agrupador
	,dde as data
from
	nodrizas.eqa_problemes_incid a
inner join
	eqa_relacio_agudes_problemes_den b
on
	a.ps=b.agrupador,nodrizas.dextraccio
where
b.ind_codi='",@indag,"' and (valor_min is null or valor_max is null)
and dde  between date_add(data_ext,interval - data_min month) and date_add(data_ext,interval - data_max month)
");


set @sql3_2 = concat("
insert  into ",@indag,"_",@eqaag,"1
select
	id_cip_sec
	,'",@indag,"' as ind_codi
	,ps as agrupador
	,dde as data
from
	nodrizas.eqa_problemes_incid a
inner join
	eqa_relacio_agudes_problemes_den b
on
	a.ps=b.agrupador,nodrizas.dextraccio
where
 b.ind_codi='",@indag,"' and valor_min is not null and (pr_gra between valor_min and valor_max) 
and dde  between date_add(data_ext,interval - data_min month) and date_add(data_ext,interval - data_max month)
");
	

set @sql4 = concat("
insert into ",@indag,"_",@eqaag,"1
select
	id_cip_sec
	,'",@indag,"' as ind_codi
	,agrupador
	,0 as data
from
	nodrizas.assignada_tot_with_jail a ,eqa_relacio_agudes_assignacio_den b
where
	ind_codi='",@indag,"' and (if(sexe='H',0,if(sexe='D',1,'9999')) between valor_min and valor_max)
");

set @sql6 = concat("
drop table if exists ",@indag,"_",@eqaag,"2
");

set @sql7 = concat("
create table ",@indag,"_",@eqaag,"2 (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
agrupador double,
data DATE NOT NULL DEFAULT 0,
numero_criteri double
)
select
	id_cip_sec
	,a.ind_codi
	,a.agrupador
	,data
	,numero_criteri
from
	",@indag,"_",@eqaag,"1 a
inner join
	eqa_relacio b
on
	a.ind_codi=b.ind_codi and a.agrupador=b.agrupador
where
	tipus='",@eqaag,"' and baixa=0
");


set @sql8 = concat("
drop table ",@indag,"_",@eqaag,"1
")
;

set @sql9 = concat("
drop table if exists ",@indag,"_",@eqaag,"3
");

set @sql10 = concat("
create table ",@indag,"_",@eqaag,"3 (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
recompte_den double
)
select
	id_cip_sec
	,ind_codi
	,count(distinct numero_criteri) as recompte_den
from
	",@indag,"_",@eqaag,"2 a inner join eqa_agrupadors_desc b on a.agrupador=b.agrupador
group by
	id_cip_sec
");


set @sql11 = concat("
drop table if exists ",@indag,"_",@eqaag,"4
");

set @sql12 = concat("
create table ",@indag,"_",@eqaag,"4 (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
primary key(id_cip_sec,ind_codi),
index(id_cip_sec,ind_codi)
)
select
	id_cip_sec
	,a.ind_codi
from
	",@indag,"_",@eqaag,"3 a inner join recompte_ag b on a.ind_codi=b.ind_codi  where recompte_den>=recompte
");

set @sql13 = concat("
drop table if exists ",@indag,"_",@eqaag,"3
");

set @sql14 = concat("
drop table if exists ",@indag,"_",@eqaag,"5
");

set @sql15 = concat("
create table ",@indag,"_",@eqaag,"5 (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
data DATE NOT NULL DEFAULT 0,
agrupador double,
num double,
excl double,
ci double,
clin double,
recompte_num double,
recompte_excl double,
recompte_ci double,
recompte_clin double
)
select
	a.id_cip_sec
	,a.ind_codi
	,data
	,agrupador 
	,0 as num
	,0 as excl
   ,0 as ci
   ,0 as clin
	,0 as recompte_num
	,0 as recompte_excl
   ,0 as recompte_ci
   ,0 as recompte_clin
from
	",@indag,"_",@eqaag,"2 a
inner join
	",@indag,"_",@eqaag,"4 b
on
	a.id_cip_sec=b.id_cip_sec
");


set @sql16 = concat("
drop table if exists ",@indag,"_",@eqaag,"2
");


set @sql17 = concat("
drop table ",@indag,"_",@eqaag,"4
");


set @sql18 = concat("
drop table if exists ",@indag,"_",@eqaag,"
");

set @sql19 = concat("
create table ",@indag,"_",@eqaag," (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
data DATE NOT NULL DEFAULT 0,
num double,
excl double,
ci double,
clin double,
recompte_num double,
recompte_excl double,
recompte_ci double,
recompte_clin double,
ramP double,
index(id_cip_sec,ind_codi,data)
)
select
	id_cip_sec
	,a.ind_codi
	,data
	,0 as num
	,0 as excl
   ,0 as ci
   ,0 as clin
	,0 as recompte_num
	,0 as recompte_excl
   ,0 as recompte_ci
   ,0 as recompte_clin
   ,0 as ramP
from
	",@indag,"_",@eqaag,"5 a
inner join
	psi_agudes b
on
	a.agrupador=b.agrupador and a.ind_codi=b.ind_codi
");


set @sql20 = concat("
drop table ",@indag,"_",@eqaag,"5
");

set @sql21 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_ram b on a.id_cip_sec=b.id_cip_sec 
set ramP = 1 where b.agr=505
");

set @sql22 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec 
set ramP = 1 where b.ps=163
");
	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s3_2 FROM @sql3_2;
EXECUTE s3_2;

PREPARE s4 FROM @sql4;
EXECUTE s4;

PREPARE s6 FROM @sql6;
EXECUTE s6;

PREPARE s7 FROM @sql7;
EXECUTE s7;

PREPARE s8 FROM @sql8;
EXECUTE s8;

PREPARE s9 FROM @sql9;
EXECUTE s9;

PREPARE s10 FROM @sql10;
EXECUTE s10;

PREPARE s11 FROM @sql11;
EXECUTE s11;

PREPARE s12 FROM @sql12;
EXECUTE s12;

PREPARE s13 FROM @sql13;
EXECUTE s13;

PREPARE s14 FROM @sql14;
EXECUTE s14;

PREPARE s15 FROM @sql15;
EXECUTE s15;

PREPARE s16 FROM @sql16;
EXECUTE s16;

PREPARE s17 FROM @sql17;
EXECUTE s17;

PREPARE s18 FROM @sql18;
EXECUTE s18;

PREPARE s19 FROM @sql19;
EXECUTE s19;

PREPARE s20 FROM @sql20;
EXECUTE s20;

PREPARE s21 FROM @sql21;
EXECUTE s21;

PREPARE s22 FROM @sql22;
EXECUTE s22;

SET p=p+1;
IF p > @maxag
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ag();


#Numeradors

set @eqaag='den';
set @eqa1ag='num';
set @eqa2ag='ind';

set @maxag=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3) z);

drop table if exists agrupadorsag;
create table agrupadorsag (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3
;


drop table if exists recompte_ag;
create table recompte_ag(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag group by ind_codi
;

drop procedure if exists eqa_ag;

delimiter //
CREATE PROCEDURE eqa_ag()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num1=p;


update agrupadorsag set usar=0;
update agrupadorsag set usar=1 where id=@num1;


set @indag=(select ind_codi from agrupadorsag where usar=1);
set @recompte_ag=(select count(distinct numero_criteri) from eqa_relacio where ind_codi=@indag and tipus=@eqa1ag);
set @temps=(select unitat from eqa_temps_ag where ind_codi=@indag);


set @sql1 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_num c on b.farmac=c.agrupador
set num=1
,recompte_num=recompte_num+1  where c.ind_codi='",@indag,"' and sense is null and valor_min is null
and (pres_orig between date_add(data,interval - data_min ",@temps,") and date_add(data,interval + data_max ",@temps,"))
");

set @sql12 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_num c on b.farmac=c.agrupador
set num=1
,recompte_num=recompte_num+1  where c.ind_codi='",@indag,"' and sense is null and valor_min=505 and ramP=1
and (pres_orig between date_add(data,interval - data_min ",@temps,") and date_add(data,interval + data_max ",@temps,"))
");


set @sql2 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_variables_num c on b.agrupador=c.agrupador
set num=1 ,
 recompte_num=recompte_num+1 where c.ind_codi='",@indag,"' and sense is null 
and (data_var between date_add(data,interval + data_min ",@temps,") and date_add(data,interval + data_max ",@temps,"))
and valor between valor_min and valor_max
");


set @sql3 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_variables_num c on b.agrupador=c.agrupador
set num=1 ,
 recompte_num=recompte_num+1 where  c.ind_codi='",@indag,"' and sense is null 
and (data_var between date_add(data,interval + data_min ",@temps,") and date_add(data,interval + data_max ",@temps,"))
and  valor_min is null
");


set @sql4 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_num c on b.farmac=c.agrupador
set num=2 ,
 recompte_num=recompte_num+1  where c.ind_codi='",@indag,"' and sense =1
and (pres_orig between date_add(data,interval - data_min ",@temps,") and date_add(data,interval + data_max ",@temps,"))
");


set @sql5 = concat("
update ",@indag,"_",@eqaag," a inner join eqa_relacio_agudes_prescripcions_num c on a.ind_codi=c.ind_codi
set num=1 where num<>2  and sense=1 and c.ind_codi='",@indag,"' 
");

set @sql6 = concat("
update ",@indag,"_",@eqaag," a
set num=0 where num=2
");


#(francesc): agost14 passo s4 al primer lloc
PREPARE s4 FROM @sql4;
EXECUTE s4;
	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s12 FROM @sql12;
EXECUTE s12;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s5 FROM @sql5;
EXECUTE s5;

PREPARE s6 FROM @sql6;
EXECUTE s6;


SET p=p+1;
IF p > @maxag
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ag();



#Exclusions


set @eqaag='den';
set @eqa1ag='excl';
set @eqa2ag='ind';

set @maxag=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3  and a.tipus=@eqa1ag ) z);


drop table if exists agrupadorsag;
create table agrupadorsag (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag
;


drop table if exists recompte_ag;
create table recompte_ag(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag group by ind_codi
;

drop procedure if exists eqa_ag;

delimiter //
CREATE PROCEDURE eqa_ag()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num1=p;


update agrupadorsag set usar=0;
update agrupadorsag set usar=1 where id=@num1;


set @indag=(select ind_codi from agrupadorsag where usar=1);
set @recompte_ag=(select count(distinct numero_criteri) from eqa_relacio where ind_codi=@indag and tipus=@eqa1ag);



set @sql1 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_excl c on b.farmac=c.agrupador
set excl=1
,recompte_excl=recompte_excl+1 where c.ind_codi='",@indag,"'  and sense is null
and (pres_orig between date_add(data,interval - data_min day) and date_add(data,interval + data_max day))
");


set @sql2 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_problemes_incid b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_problemes_excl c on b.ps=c.agrupador
set excl=1
,recompte_excl=recompte_excl+1 where  c.ind_codi='",@indag,"' 
and (dde between date_add(data,interval - data_min day) and date_add(data,interval + data_max day))
");


set @sql3 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_excl c on b.farmac=c.agrupador
set excl=2
,recompte_excl=recompte_excl+1 where c.ind_codi='",@indag,"' and sense =1
and (pres_orig between date_add(data,interval - data_min day) and date_add(data,interval + data_max day))
");


set @sql5 = concat("
update ",@indag,"_",@eqaag," a inner join eqa_relacio_agudes_prescripcions_excl c on a.ind_codi=c.ind_codi
set excl=1 where excl<>2  and sense=1 and c.ind_codi='",@indag,"' 
");

set @sql6 = concat("
update ",@indag,"_",@eqaag," a
set excl=0 where excl=2
");

set @sql7 = concat("
update ",@indag,"_",@eqaag," a
set excl=1 where recompte_excl>=",@recompte_ag,"
");

set @sql8 = concat("
update ",@indag,"_",@eqaag," a
set excl=0 where excl<>1
");


	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s5 FROM @sql5;
EXECUTE s5;

PREPARE s6 FROM @sql6;
EXECUTE s6;

PREPARE s7 FROM @sql7;
EXECUTE s7;

PREPARE s8 FROM @sql8;
EXECUTE s8;


SET p=p+1;
IF p > @maxag
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ag();


#Contraindicacions



set @eqaag='den';
set @eqa1ag='ci';
set @eqa2ag='ind';

set @maxag=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3  and a.tipus=@eqa1ag ) z);


drop table if exists agrupadorsag;
create table agrupadorsag (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag
;


drop table if exists recompte_ag;
create table recompte_ag(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag group by ind_codi
;

drop procedure if exists eqa_ag;

delimiter //
CREATE PROCEDURE eqa_ag()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num1=p;


update agrupadorsag set usar=0;
update agrupadorsag set usar=1 where id=@num1;


set @indag=(select ind_codi from agrupadorsag where usar=1);
set @recompte_ag=(select count(distinct numero_criteri) from eqa_relacio where ind_codi=@indag and tipus=@eqa1ag);



set @sql1 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_ci c on b.farmac=c.agrupador
set ci=1
,recompte_ci=recompte_ci+1 where c.ind_codi='",@indag,"'  and sense is null
and (pres_orig between date_add(data,interval - data_min month) and date_add(data,interval + data_max month))
");


set @sql2 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_problemes_ci c on b.ps=c.agrupador
set ci=1
,recompte_ci=recompte_ci+1 where  c.ind_codi='",@indag,"'  and sense is null 
and (dde between date_add(data,interval - data_min month) and date_add(data,interval + data_max month))
");

set @sql21 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_ram b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_ram_ci c on b.agr=c.agrupador
set ci=1
,recompte_ci=recompte_ci+1 where  c.ind_codi='",@indag,"'  and sense is null 
");


set @sql3 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_ci c on b.farmac=c.agrupador
set ci=2
,recompte_ci=recompte_ci+1 where c.ind_codi='",@indag,"' and sense =1 and ci=0
and (pres_orig between date_add(data,interval - data_min month) and date_add(data,interval + data_max month))
");


set @sql5 = concat("
update ",@indag,"_",@eqaag," a inner join eqa_relacio_agudes_prescripcions_ci c on a.ind_codi=c.ind_codi
set ci=1 where ci<>2  and sense=1 and c.ind_codi='",@indag,"' 
");

set @sql6 = concat("
update ",@indag,"_",@eqaag," a
set ci=0 where ci=2
");

set @sql7 = concat("
update ",@indag,"_",@eqaag," a
set ci=1 where recompte_ci>=",@recompte_ag,"
");

set @sql8 = concat("
update ",@indag,"_",@eqaag," a
set ci=0 where ci<>1
");


	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s21 FROM @sql21;
EXECUTE s21;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s5 FROM @sql5;
EXECUTE s5;

PREPARE s6 FROM @sql6;
EXECUTE s6;

PREPARE s7 FROM @sql7;
EXECUTE s7;

PREPARE s8 FROM @sql8;
EXECUTE s8;


SET p=p+1;
IF p > @maxag
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ag();


#exclusions cliniques

/*

set @eqaag='den';
set @eqa1ag='clin';
set @eqa2ag='ind';

set @maxag=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3  and a.tipus=@eqa1ag ) z);


drop table if exists agrupadorsag;
create table agrupadorsag (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag
;


drop table if exists recompte_ag;
create table recompte_ag(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3 and a.tipus=@eqa1ag group by ind_codi
;

drop procedure if exists eqa_ag;

delimiter //
CREATE PROCEDURE eqa_ag()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num1=p;


update agrupadorsag set usar=0;
update agrupadorsag set usar=1 where id=@num1;


set @indag=(select ind_codi from agrupadorsag where usar=1);
set @recompte_ag=(select count(distinct numero_criteri) from eqa_relacio where ind_codi=@indag and tipus=@eqa1ag);



set @sql1 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_clin c on b.farmac=c.agrupador
set clin=1
,recompte_clin=recompte_clin+1 where c.ind_codi='",@indag,"'  and sense is null
and (pres_orig between date_add(data,interval - data_min month) and date_add(data,interval + data_max month))
");


set @sql2 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_problemes_clin c on b.ps=c.agrupador
set clin=1
,recompte_clin=recompte_clin+1 where  c.ind_codi='",@indag,"'  and sense is null 
and (dde between date_add(data,interval - data_min month) and date_add(data,interval + data_max month))
");


set @sql3 = concat("
update ",@indag,"_",@eqaag," a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec inner join eqa_relacio_agudes_prescripcions_clin c on b.farmac=c.agrupador
set clin=2
,recompte_clin=recompte_clin+1 where c.ind_codi='",@indag,"' and sense =1
and (pres_orig between date_add(data,interval - data_min month) and date_add(data,interval + data_max month))
");


set @sql5 = concat("
update ",@indag,"_",@eqaag," a inner join eqa_relacio_agudes_prescripcions_clin c on a.ind_codi=c.ind_codi
set clin=1 where clin<>2  and sense=1 and c.ind_codi='",@indag,"' 
");

set @sql6 = concat("
update ",@indag,"_",@eqaag," a
set clin=0 where clin=2
");

set @sql7 = concat("
update ",@indag,"_",@eqaag," a
set clin=1 where recompte_clin>=",@recompte_ag,"
");

set @sql8 = concat("
update ",@indag,"_",@eqaag," a
set clin=0 where clin<>1
");


	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s5 FROM @sql5;
EXECUTE s5;

PREPARE s6 FROM @sql6;
EXECUTE s6;

PREPARE s7 FROM @sql7;
EXECUTE s7;

PREPARE s8 FROM @sql8;
EXECUTE s8;


SET p=p+1;
IF p > @maxag
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ag();

*/
#Indicadors


set @eqaag='ind';
set @eqa1ag='num';
set @eqa2ag='den';
set @eqa3ag='excl';
set @eqa4ag='edat';
set @eqa5ag='ci';
set @eqa6ag='clin';

set @maxtag=(select count(*) from (select distinct a.ind_codi from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3) z);

drop table if exists agrupadorsag;
create table agrupadorsag (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct a.ind_codi, 0 usar from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and b.tipus=3
;

drop procedure if exists eqa_ag;

delimiter //
CREATE PROCEDURE eqa_ag()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num1=p;


update agrupadorsag set usar=0;
update agrupadorsag set usar=1 where id=@num1;


set @indag=(select ind_codi from agrupadorsag where usar=1);

set @sql1 = concat("
drop table if exists ",@indag,"_",@eqa2ag,"final
");

set @sql2 = concat("
create table ",@indag,"_",@eqa2ag,"final (
id_cip_sec double null,
ind_codi varchar(10) not null default'',
data DATE NOT NULL DEFAULT 0,
num double,
excl double,
ci double,
clin double,
den double
)
select
	id_cip_sec
	,a.ind_codi
	,data
	,sum(num) as num
	,max(excl) as excl
   ,max(ci) as ci
   ,max(clin) as clin
	,count(*) as den
from
	",@indag,"_",@eqa2ag," a
group by
	id_cip_sec
");


set @sql3 = concat("
drop table if exists ",@indag,"_",@eqa2ag,"
");

set @sql4 = concat("
drop table if exists ",@indag,"_",@eqaag,"1
");

set @sql5 = concat("
create table ",@indag,"_",@eqaag,"1 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
den double,
excl double,
ci double,
clin double,
index(id_cip_sec, ind_codi)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,'",@indag,"' as ind_codi
	,0 as num
	,0 as den
	,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a
");


set @sql6 = concat("
update ",@indag,"_",@eqaag,"1 a inner join ",@indag,"_",@eqa2ag,"final b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set a.",@eqa1ag,"=b.num
");



set @sql7 = concat("
update ",@indag,"_",@eqaag,"1 a inner join ",@indag,"_",@eqa2ag,"final b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set a.",@eqa2ag,"=b.den
");


set @sql8 = concat("
update ",@indag,"_",@eqaag,"1 a inner join ",@indag,"_",@eqa2ag,"final b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set a.",@eqa3ag,"=b.excl
");


/*
set @sql82 = concat("
update ",@indag,"_",@eqaag,"1 a inner join ",@indag,"_",@eqa2ag,"final b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set a.",@eqa6ag,"=b.clin
");
*/

set @sql81 = concat("
update ",@indag,"_",@eqaag,"1 a inner join ",@indag,"_",@eqa2ag,"final b on a.id_cip_sec=b.id_cip_sec and a.ind_codi=b.ind_codi
set a.",@eqa5ag,"=b.ci
");

set @sql9 = concat("
drop table if exists ",@indag,"_",@eqaag,"2
");

set @sql10= concat("
create table ",@indag,"_",@eqaag,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
den double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
   ,den
from
	",@indag,"_",@eqaag,"1
where
	den>=1
");


set @sql13 = concat("
drop table ",@indag,"_",@eqaag,"1
");


	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s3 FROM @sql3;
EXECUTE s3;

PREPARE s4 FROM @sql4;
EXECUTE s4;

PREPARE s5 FROM @sql5;
EXECUTE s5;

PREPARE s6 FROM @sql6;
EXECUTE s6;

PREPARE s7 FROM @sql7;
EXECUTE s7;

PREPARE s8 FROM @sql8;
EXECUTE s8;
/*
PREPARE s82 FROM @sql82;
EXECUTE s82;
*/
PREPARE s81 FROM @sql81;
EXECUTE s81;

PREPARE s9 FROM @sql9;
EXECUTE s9;

PREPARE s10 FROM @sql10;
EXECUTE s10;

PREPARE s13 FROM @sql13;
EXECUTE s13;

SET p=p+1;
IF p > @maxtag
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_ag();


