# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter

nod = 'nodrizas'

eqa = 'eqa_ind'

indicador = 'EQA3004A'

criteris_khalix = 'ates = 1 and institucionalitzat = 0 and excl = 0 and den = 1'

def get_data_ext():
    sql = 'select data_ext from dextraccio'
    dext,= getOne(sql, nod)
    return dext

def get_problemes():
    problemes = {}
    sql = 'select id_cip_sec, dde from eqa_problemes where ps in (1, 7)'
    for id, dde in getAll(sql, nod):
        problemes[id] = dde
    return problemes
    
def get_cmbdh():
    ingressos = {}
    sql = 'select id_cip_sec, dat from eqa_ingressos, dextraccio where agr in (619, 620, 621, 622, 623) and dat between date_add(date_add(data_ext,interval - 18 month),interval + 1 day) and date_add(date_add(data_ext,interval - 6 month),interval + 1 day) order by dat asc'
    for id, dat in getAll(sql, nod):
        ingressos[id] = dat
    return ingressos

dext = get_data_ext()
problemes = get_problemes()
ingressos = get_cmbdh()

upload = []
sql = 'select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, institucionalitzat, maca from assignada_tot_with_jail where edat>14'
for id, up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, insti, maca in getAll(sql, nod):
    den = 1
    num = 0
    excl = 0
    datp, dati = dext, dext
    if id in problemes:
        datp = problemes[id]
    if id in ingressos:
        num = 1
        dati = ingressos[id]
    if datp < dati:
        den = 0
    if den == 1:
        upload.append([id, indicador, up, uba, upinf, ubainf, ates, insti, den, num, excl])
        
tableMy = 'EQA3004A_ind2'
create = "(id_cip_sec double null,ind_codi varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',ates double, institucionalitzat double, den double,num double,excl double)"
createTable(tableMy,create,eqa, rm=True)
listToTable(upload, tableMy, eqa)
