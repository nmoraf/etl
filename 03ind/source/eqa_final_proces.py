# coding: latin1

"""
.
"""

import collections as c
import itertools as it

import sisapUtils as u


processos = {"MCV": "Malaltia cardiovascular",
             "DM2": "Diabetis mellitus tipus 2",
             "FA": "Fibril�laci� auricular"}
links = {"EQAG01": "MCV;FA", "EQAG02": "DM2"}
mortalitat = ("EQA3007A", "EQA3111A", "EQA3214A")

tb_klx_up = "exp_khalix_up_ind_proc"
tb_klx_uba = "exp_khalix_uba_ind_proc"
tb_ecap_uba = "exp_ecap_uba_proc"
tb_ecap_pac = "exp_ecap_pacient_proc"
tb_ecap_cat = "exp_ecap_cataleg_proc"
tb_ecap_cat_pare = "exp_ecap_catalegpare_proc"
tb_ecap_cat_proc = "exp_ecap_cataleg_processos"

db = "eqa_ind"


class Proces(object):
    """."""

    def __init__(self):
        """."""
        self.get_cataleg()
        self.get_u11()
        self.get_ubas()
        self.get_poblacio()
        self.get_master()
        self.get_ajustades()
        self.export_klx_up()
        self.export_klx_uba()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()
        self.export_ecap_cataleg_pare()
        self.export_ecap_cataleg_proc()
        self.update_links()

    def get_cataleg(self):
        """."""
        self.cataleg = []
        self.mostrar = c.defaultdict(set)
        self.indicadors = set()
        self.inversos = c.defaultdict(set)
        self.llistats = c.defaultdict(set)
        sql = "select indicador, literal, dimensio, is_eqa, to_show, is_invers, has_llistat \
               from mst_cataleg_{} order by indicador"
        for proces in processos:
            for i, (ind, lit, dim, eqa, show, inv, llist) in enumerate(u.getAll(sql.format(proces), db)):
                if eqa:
                    q = "select literal, toshow, invers, llistat, wiki \
                         from exp_ecap_cataleg \
                         where indicador = '{}'".format(ind)
                    lit, show, inv, llist, wiki = u.getOne(q, db)
                    this = (ind, lit, i + 1, dim, llist, inv, None, None, None, None, 1, wiki, None, None, None, proces)
                    self.cataleg.append(this)
                elif show:
                    wiki = 'http://sisap-umi.eines.portalics/indicador/codi/' + ind[:7]
                    this = (ind[:7], lit, i + 1, dim, llist, inv, None, None, None, None, 1, wiki, None, None, None, proces)
                    self.cataleg.append(this)
                    self.mostrar[proces].add(ind)
                    self.indicadors.add(ind)
                    if inv:
                        self.inversos[proces].add(ind)
                    if llist:
                        self.llistats[proces].add(ind)

    def get_u11(self):
        """."""
        sql = "select id_cip_sec, hash_d, codi_sector from eqa_u11"
        self.u11 = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_ubas(self):
        """."""
        sql = "select up, uba, tipus from mst_ubas"
        self.ubas = set(u.getAll(sql, db))

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        medea = {up: grup for (up, grup)
                 in u.getAll("select scs_codi, medea from cat_centres", "nodrizas")}
        sql = "select id_cip_sec, up, edat, sexe from {}"
        for taula in ("assignada_tot", "rip_assignada"):
            for id, up, edat, sexe in u.getAll(sql.format(taula), "nodrizas"):
                key = (u.ageConverter(edat), u.sexConverter(sexe), medea[up])
                self.poblacio[id] = key

    def get_master(self):
        """."""
        self.dades_pac = []
        self.dades_up = c.Counter()
        self.dades_uba = c.Counter()
        self.dades_aj = c.Counter()
        sql = "select * from mst_indicadors_pacient_{}"
        for proces in processos:
            for (id, ind, dim, up, uba, upinf, ubainf, ates, instit,
                 den, num, excl) in u.getAll(sql.format(proces), db):
                entities = [entity for entity in ((up, uba, "M"), (upinf, ubainf, "I")) if entity in self.ubas]
                if ates and not instit and not excl:
                    self.dades_up[(proces, dim, ind, (up,), "DENZ")] += den
                    self.dades_up[(proces, dim, ind, (up,), "NUMZ")] += num
                    if ind in mortalitat:
                        self.dades_aj[(proces, dim, ind, (up,), "DENZ") + self.poblacio[id]] += den
                        self.dades_aj[(proces, dim, ind, (up,), "NUMZ") + self.poblacio[id]] += num
                    for entity in entities:
                        self.dades_uba[(proces, dim, ind, entity, "DENZ")] += den
                        self.dades_uba[(proces, dim, ind, entity, "NUMZ")] += num
                        if ind in mortalitat:
                            self.dades_aj[(proces, dim, ind, entity, "DENZ") + self.poblacio[id]] += den
                            self.dades_aj[(proces, dim, ind, entity, "NUMZ") + self.poblacio[id]] += num
                if ind in self.mostrar[proces].intersection(self.llistats[proces]):
                    inv = 1 * (ind in self.inversos[proces])
                    sit = 2 if instit else 4 if excl else 5 if not ates else 0 if num == inv else 9
                    for entity in entities:
                        if sit < 9 and entity in self.ubas:
                            self.dades_pac.append(entity + (ind[:7],) + self.u11[id] + (sit,))

    def get_ajustades(self):
        """."""
        factors = c.defaultdict(c.Counter)
        denominadors = c.Counter()
        for (proc, dim, ind, entity, analysis, edat, sexe, grup), n in self.dades_aj.items():
            if len(entity) == 1:
                factors[(ind, edat, sexe, grup)][analysis] += n
        pesos = {key: values["NUMZ"] / float(values["DENZ"])
                 for (key, values) in factors.items()}
        for (proc, dim, ind, entity, analysis, edat, sexe, grup), n in self.dades_aj.items():
            if analysis == "DENZ":
                pes = pesos[(ind, edat, sexe, grup)]
                denominadors[(proc, dim, ind, entity, analysis)] += n * pes
        for key, value in denominadors.items():
            object = self.dades_up if len(key[3]) == 1 else self.dades_uba
            object[key] = value

    def export_klx_up(self):
        """."""
        columns = [column + " varchar(10)"
                   for column in ("indicador", "up", "analisis", "dimensio", "proces")] + ["n double"]
        u.createTable(tb_klx_up, "({})".format(", ".join(columns)), db, rm=True)
        export = [(ind, up[0], analisis, dim, proc, n)
                  for (proc, dim, ind, up, analisis), n
                  in self.dades_up.items()]
        u.listToTable(export, tb_klx_up, db)

    def export_klx_uba(self):
        """."""
        columns = [column + " varchar(10)"
                   for column in ("indicador", "up", "uba", "tipus", "analisis", "dimensio", "proces")] + ["n double"]
        u.createTable(tb_klx_uba, "({})".format(", ".join(columns)), db, rm=True)
        export = [(ind, up, uba, tipus, analisis, dim, proc, n)
                  for (proc, dim, ind, (up, uba, tipus), analisis), n
                  in self.dades_uba.items()]
        u.listToTable(export, tb_klx_uba, db)

    def export_ecap_uba(self):
        """."""
        u.createTable(tb_ecap_uba, " like {}".format(tb_ecap_uba.replace("_proc", "")), db, rm=True)
        export = []
        fets = set()
        for (proc, dim, ind, entity, analisis), den in self.dades_uba.items():
            if ind in self.mostrar[proc] and analisis == "DENZ":
                num = self.dades_uba[(proc, dim, ind, entity, "NUMZ")]
                resul = float(num) / den
                no_resolts = num if ind in self.inversos[proc] else (den - num)
                this = entity + (ind[:7], None, den, num, None, None, resul, resul, None, None, no_resolts, None, None, None, None)
                export.append(this)
                fets.add((entity, ind))
        for entity, ind in it.product(self.ubas, self.indicadors):
            if (entity, ind) not in fets:
                this = entity + (ind[:7],) + (None,) * 14
                export.append(this)
        u.listToTable(export, tb_ecap_uba, db)

    def export_ecap_pacient(self):
        """."""
        columns = ("up varchar(5)", "uba varchar(5)", "tipus varchar(1)", "indicador varchar(10)", "hash varchar(40)", "sector varchar(4)", "exclos int")
        u.createTable(tb_ecap_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.dades_pac, tb_ecap_pac, db)

    def export_ecap_cataleg(self):
        """."""
        u.createTable(tb_ecap_cat, " like {}".format(tb_ecap_cat.replace("_proc", "")), db, rm=True)
        u.listToTable(self.cataleg, tb_ecap_cat, db)

    def export_ecap_cataleg_pare(self):
        """."""
        u.createTable(tb_ecap_cat_pare, " like {}".format(tb_ecap_cat_pare.replace("_proc", "")), db, rm=True)
        pares = [("QDIAG", "Qualitat diagn�stica", 1, ""),
                 ("PREVPRIM", "Prevenci� prim�ria", 2, ""),
                 ("PREVSEC", "Prevenci� secund�ria", 3, ""),
                 ("PREVQUAT", "Prevenci� quatern�ria", 4, ""),
                 ("CAPRESOL", "Capacitat resolutiva", 5, ""),
                 ("SEGUIMENT", "Seguiment", 6, ""),
                 ("RESTRAC", "Resultats: tracament i grau de control", 7, ""),
                 ("RESENV", "Resultats: esdeveniments", 8, "")]
        u.listToTable(pares, tb_ecap_cat_pare, db)

    def export_ecap_cataleg_proc(self):
        """."""
        u.createTable(tb_ecap_cat_proc, "(codi varchar(10), descripcio varchar(255))", db, rm=True)
        u.listToTable(processos.items(), tb_ecap_cat_proc, db)

    def update_links(self):
        """."""
        tb = tb_ecap_cat_pare.replace("_proc", "")
        u.execute("update {} set proces = ''".format(tb), db)
        sql = "update {} set proces = '{}' where pare = '{}'"
        for grup, text in links.items():
            u.execute(sql.format(tb, text, grup), db)


if __name__ == "__main__":
    Proces()
