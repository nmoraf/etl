# coding: latin1

"""
Proc�s de c�lcul de l'EQA del proc�s FA.

Es distingeixen 3 tipus d'indicadors:
1-Els que s�nn id�ntics que a l'EQA d'adults -> per ecap els duplicarem
  al cat�leg, i klx far� el que hagi de fer
2-Els que s�n iguals que a l'EQA d'adults per� filtrats per poblaci�
  amb FA -> ho fem aqu� (en el moment de la creaci� no n'hi ha cap)
3-Nous indicadors -> ho fem aqu�
"""

import collections as c
import simplejson as j

import sisapUtils as u


tb_pac = 'mst_indicadors_pacient_fa'
tb_cat = 'mst_cataleg_fa'
db = 'eqa_ind'
proc = 'FA'

no_llistat = ['EQA3210A', 'EQA3211A', 'EQA3212A']


class FA(object):
    """."""

    def __init__(self):
        """."""
        self.create_tables()
        self.get_poblacio()
        self.get_problemes()
        self.get_exitus()
        self.get_indicadors()
        for codi, especificacio in self.indicadors.items():
            if especificacio['do']:
                Indicador(codi, especificacio,
                          self.poblacio, self.prevalents, self.incidents,
                          self.dde, self.exitus)

    def create_tables(self):
        """."""
        for tb in (tb_pac, tb_cat):
            orig = tb.replace('fa', 'mcv')
            u.createTable(tb, 'like {}'.format(orig), db, rm=True)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, edat, up, uba, upinf, ubainf, \
                      ates, institucionalitzat from assignada_tot \
                      where maca = 0 and trasplantat = 0"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, 'nodrizas')}

    def get_problemes(self):
        """."""
        self.prevalents, self.incidents, self.dde = {}, {}, {}
        sql = "select id_cip_sec, dde, incident from eqa_problemes \
               where ps = 180"
        for id, dde, incident in u.getAll(sql, 'nodrizas'):
            if id in self.poblacio:
                self.prevalents[id] = self.poblacio[id]
                self.dde[id] = dde
                if incident:
                    self.incidents[id] = self.poblacio[id]

    def get_exitus(self):
        """."""
        sql = "select id_cip_sec from rip_problemes where ps in (180, 768)"
        fa = set([id for id, in u.getAll(sql, 'nodrizas')])
        self.exitus = {k: v for (k, v) in self.prevalents.items()}
        sql = "select id_cip_sec, edat, up, uba, '', '', 1, 0 \
              from rip_assignada"
        for row in u.getAll(sql, 'nodrizas'):
            if row[0] in fa:
                self.exitus[row[0]] = row[1:]

    def get_indicadors(self):
        """."""
        file = 'dades_noesb/eqa_fa.json'
        try:
            stream = open(file)
        except IOError:
            stream = open('../' + file)
        self.indicadors = j.load(stream)


class Indicador(object):
    """."""

    def __init__(self, codi, especificacions, poblacio, prevalents,
                 incidents, dde, exitus):
        """."""
        self.codi = codi
        self.especificacions = especificacions
        self.poblacio_all = poblacio
        self.prevalents = prevalents
        self.incidents = incidents
        self.dde = dde
        self.exitus = exitus
        self.insert_cataleg()
        if 'eqa' not in self.especificacions:
            if self.especificacions.get('filtrar'):
                self.filtrar_indicador()
            else:
                self.get_poblacio()
                self.get_denominador()
                self.get_numerador()
                self.get_exclusions()
                self.get_pacients()
            self.upload_pacients()

    def insert_cataleg(self):
        """."""
        is_eqa = 1 * ('eqa' in self.especificacions)
        cataleg = (self.codi,
                   self.especificacions.get('literal', '').encode('latin1'),
                   self.especificacions['dimensio'],
                   is_eqa,
                   9 if is_eqa else 1 - self.especificacions.get('hide', False),
                   9 if is_eqa else 1 * self.especificacions.get('invers', False),
                   9 if is_eqa else 0 if self.codi in no_llistat else 1 * self.especificacions.get('llistat', True))
        u.listToTable([cataleg], tb_cat, db)

    def filtrar_indicador(self):
        """."""
        original = self.especificacions['filtrar']
        sql = "select id_cip_sec, '{}', '{}', up, uba, upinf, ubainf, \
                      ates, institucionalitzat, den, num, excl \
                      from mst_indicadors_pacient \
                      where ind_codi = '{}' and \
                            maca = 0 and \
                            trasplantat = 0 and \
                            excl_edat = 0 and \
                            ci = 0 and \
                            clin = 0".format(self.codi,
                                             self.especificacions['dimensio'],
                                             original)
        self.pacients = [row for row in u.getAll(sql, 'eqa_ind')
                         if int(row[0]) in self.prevalents]

    def get_poblacio(self):
        """."""
        tipus = self.especificacions['poblacio']
        if tipus == 'prevalents':
            original = self.prevalents
        elif tipus == 'incidents':
            original = self.incidents
        elif tipus == 'exitus':
            original = self.exitus
        elif tipus == 'assignada':
            original = self.poblacio_all
        emin, emax = self.especificacions['edat']
        self.poblacio = {id: valors[1:] for (id, valors)
                         in original.items()
                         if emin <= valors[0] <= emax}

    @staticmethod
    def get_tuple(codis):
        """."""
        if len(codis) == 1:
            return "('{}')".format(codis[0])
        else:
            return tuple(codis)

    @staticmethod
    def get_sql(especificacions):
        """."""
        conceptes = {('eqa_variables', 'codi'): 'agrupador',
                     ('eqa_variables', 'data'): 'data_var',
                     ('eqa_variables', 'val'): 'valor',
                     ('eqa_problemes', 'codi'): 'ps',
                     ('eqa_problemes', 'data'): 'dde',
                     ('rip_problemes', 'codi'): 'ps',
                     ('rip_problemes', 'data'): 'dde',
                     ('eqa_tractaments', 'codi'): 'farmac',
                     ('eqa_tractaments', 'data'): 'pres_orig',
                     ('eqa_proves', 'codi'): 'agrupador',
                     ('eqa_proves', 'data'): 'data',
                     ('assignada_tot', 'codi'): 'edat',
                     ('assignada_tot', 'data'): 'data_ext',
                     ('eqa_acfa', 'codi'): 'sexe',
                     ('eqa_acfa', 'data'): 'data_ext',
                     ('eqa_acfa', 'val'): 'chads2',
                     ('eqa_acfa', 'db'): 'eqa_ind',
                     ('nod_trt', 'id'): 'id',
                     ('nod_trt', 'codi'): 'is_calculable',
                     ('nod_trt', 'data'): 'dat',
                     ('nod_trt', 'val'): 'trt'}
        if isinstance(especificacions, dict):
            relacio = "o"
            especificacions = [especificacions]
        else:
            relacio = especificacions[0]
            especificacions = especificacions[1:]
        sqls = []
        for especificacio in especificacions:
            taula = especificacio['taula']
            codis = Indicador.get_tuple(especificacio['agrupadors'])
            temps = especificacio.get('temps')
            last = especificacio.get('last')
            valors = especificacio.get('valors')
            sql = "select {}, {} \
                   from {}, nodrizas.dextraccio \
                   where {} in {}".format(
                                    conceptes.get((taula, 'id'), 'id_cip_sec'),
                                    conceptes[(taula, 'data')],
                                    taula,
                                    conceptes[(taula, 'codi')],
                                    codis)
            if temps:
                if not isinstance(temps, list):
                    temps = [temps, "0 day"]
                sql += " and {} between adddate(adddate(data_ext, \
                                                interval - {}), \
                                        interval +1 day) and \
                                        adddate(data_ext, \
                                                interval - {})".format(
                                                    conceptes[(taula, 'data')],
                                                    temps[0],
                                                    temps[1])
            if last:
                sql += " and usar = {}".format(last)
            if valors:
                sql += " and {} between {} and {}".format(
                                                conceptes[(taula, 'val')],
                                                valors[0], valors[1])
            db = conceptes.get((taula, 'db'), 'nodrizas')
            sqls.append((sql, especificacio.get('not'), db, relacio))
        return sqls

    def get_denominador(self):
        """."""
        if self.especificacions.get('denominador'):
            self.denominador = {}
            numero = c.Counter()
            sqls = Indicador.get_sql(self.especificacions['denominador'])
            for sql, invertir, db, relacio in sqls:
                component = {id: self.poblacio[id]
                             for (id, data) in u.getAll(sql, db)
                             if id in self.poblacio}
                if invertir:
                    component = {k: v for (k, v) in self.poblacio.items()
                                 if k not in component}
                if relacio == 'o':
                    for k, v in component.items():
                        self.denominador[k] = v
                elif relacio == 'i':
                    if len(self.denominador) > 0:
                        self.denominador = {k: v for (k, v)
                                            in self.denominador.items()
                                            if k in component}
                    else:
                        for k, v in component.items():
                            self.denominador[k] = v
                else:
                    for id in component:
                        numero[id] += 1
            if numero:
                self.denominador = {id: self.poblacio[id] for (id, n)
                                    in numero.items() if n >= relacio}
        else:
            self.denominador = self.poblacio

    def get_numerador(self):
        """."""
        self.numerador = set()
        numero = c.Counter()
        sqls = Indicador.get_sql(self.especificacions['numerador'])
        dies = self.especificacions.get('dies')
        for sql, invertir, db, relacio in sqls:
            component = set()
            for id, data in u.getAll(sql, db):
                if id in self.denominador:
                    if dies:
                        real = (data - self.dde[id]).days
                        if dies[0] <= real <= dies[1]:
                            component.add(id)
                    else:
                        component.add(id)
            if invertir:
                component = set(self.denominador) - component
            if relacio == 'o':
                self.numerador |= component
            elif relacio == 'i':
                if len(self.numerador) > 0:
                    self.numerador &= component
                else:
                    self.numerador |= component
            else:
                for id in component:
                    numero[id] += 1
        if numero:
            self.numerador = set([id for (id, n) in numero.items()
                                  if n >= relacio])

    def get_exclusions(self):
        """."""
        self.exclusions = set()
        numero = c.Counter()
        if self.especificacions.get('exclusions'):
            sqls = Indicador.get_sql(self.especificacions['exclusions'])
            for sql, invertir, db, relacio in sqls:
                component = set([id for (id, data) in u.getAll(sql, db)
                                 if id in self.denominador])
                if invertir:
                    component = set(self.denominador) - component
                if relacio == 'o':
                    self.exclusions |= component
                elif relacio == 'i':
                    if len(self.exclusions) > 0:
                        self.exclusions &= component
                    else:
                        self.exclusions |= component
                else:
                    for id in component:
                        numero[id] += 1
            if numero:
                self.exclusions = set([id for (id, n) in numero.items()
                                       if n >= relacio])

    def get_pacients(self):
        """."""
        self.pacients = [(id, self.codi, self.especificacions['dimensio']) +
                         valors +
                         (1,
                          1 * (id in self.numerador),
                          1 * (id in self.exclusions))
                         for (id, valors) in self.denominador.items()]

    def upload_pacients(self):
        """."""
        u.listToTable(self.pacients, tb_pac, db)


if __name__ == '__main__':
    FA()
