from sisapUtils import *

bd = "eqa_ind"
base = "eqa_ind"
relacio = "eqa_relacio"
conn = connect(bd)
c = conn.cursor()
c.execute("select distinct ind_codi from %s where (tipus=0 or tipus=14 or tipus=16)" % base)
r=c.fetchall()
for t in r:
   nom = t[0]
   c.execute("select distinct tipus from %s" % relacio)
   r2=c.fetchall()
   for t2 in r2:
      tipus = t2[0]
      taula = "%s_%s" % (nom,tipus)
      c.execute("drop table if exists %s" % taula)
conn.close()