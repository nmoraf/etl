use eqa_ind;


drop table if exists eqa_agrupadors_desc;
create table eqa_agrupadors_desc(
agrupador double,
agrupador_desc varchar(100) not null default'',
index(agrupador,agrupador_desc)
)
select distinct agrupador,agrupador_desc from nodrizas.eqa_criteris;

drop table if exists recompte_den;
create table recompte_den(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  index (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16) and a.tipus='den' group by ind_codi
;

drop table if exists recompte_num;
create table recompte_num(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
 index (ind_codi,recompte)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16) and a.tipus='num' group by ind_codi
;

drop table if exists recompte_excl;
create table recompte_excl(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16) and a.tipus='excl' group by ind_codi
;

drop table if exists recompte_ci;
create table recompte_ci(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16) and a.tipus='ci' group by ind_codi
;

drop table if exists recompte_clin;
create table recompte_clin(
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
recompte double null,
  PRIMARY KEY (ind_codi)
)
select a.ind_codi, count(distinct numero_criteri) as recompte from eqa_relacio a inner join eqa_ind b on a.ind_codi=b.ind_codi where baixa=0 and (b.tipus=0 or b.tipus=14 or b.tipus=16) and a.tipus='clin' group by ind_codi
;
