use eqa_ind;

drop table if exists eqa_acfa;
create table eqa_acfa (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
hasbled double,
chads2 double,
protesi double,
estenosi double,
aco double,
aas double,
clopi double,
ci_aco double,
ci_aas double,
num double,
den double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
    a.id_cip_sec
    ,a.up
    ,a.uba
    ,a.upinf
    ,a.ubainf
    ,a.edat
    ,ates
    ,trasplantat
    ,sexe
    ,seccio_censal
    ,up_residencia
    ,institucionalitzat
    ,maca
    ,ind_codi
    ,0 as hasbled
    ,0 as chads2
    ,0 as protesi
    ,0 as estenosi
    ,0 as aco
    ,0 as aas
    ,0 as clopi
    ,0 as ci_aco
    ,0 as mcv
    ,0 as num
    ,1 as den
    ,0 as excl
    ,0 as ci
    ,0 as clin
from
    assignada_tot a
    inner join nodrizas.eqa_problemes b on a.id_cip_sec=b.id_cip_sec,
    eqa_ind c
where
     ps = 180 and c.tipus = 1
;

update eqa_acfa a inner join nodrizas.eqa_chads2 b on a.id_cip_sec = b.id_cip_sec
set chads2 = valor
;

update eqa_acfa a inner join nodrizas.eqa_variables b on a.id_cip_sec = b.id_cip_sec, nodrizas.dextraccio
set chads2 = valor
where agrupador = 233 and usar = 1 and data_var between date_add(date_add(data_ext, interval - 1 year), interval +1 day) and data_ext
;

update eqa_acfa a inner join nodrizas.eqa_variables b on a.id_cip_sec = b.id_cip_sec
set hasbled = valor
where agrupador = 479 and usar = 1
;

update eqa_acfa a inner join nodrizas.eqa_problemes b on a.id_cip_sec = b.id_cip_sec
set protesi = 1
where ps = 500
;

update eqa_acfa a inner join nodrizas.eqa_problemes b on a.id_cip_sec = b.id_cip_sec
set estenosi = 1
where ps = 660
;

update eqa_acfa a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec
set aco = 1
where farmac in (3, 6)
;

update eqa_acfa a inner join nodrizas.eqa_tractaments b on a.id_cip_sec = b.id_cip_sec
set aas = 1
where farmac = 5
;

update eqa_acfa a inner join nodrizas.eqa_tractaments b on a.id_cip_sec = b.id_cip_sec
set clopi = 1
where farmac = 189
;

update eqa_acfa a inner join nodrizas.eqa_problemes b on a.id_cip_sec = b.id_cip_sec
set ci_aco = 1
where ps in (189, 182, 184, 183, 51, 50, 185, 186, 187, 221, 222, 223, 224, 226, 39, 85, 210, 160, 257, 164)
;

update eqa_acfa a inner join nodrizas.eqa_ram b on a.id_cip_sec = b.id_cip_sec
set ci_aco = 1
where agr in (661, 662)
;


update eqa_acfa a inner join nodrizas.eqa_problemes b on a.id_cip_sec = b.id_cip_sec
set mcv = 1
where ps in (439, 11,213,7,212,1,211)
;


update eqa_acfa
set num = 1
where
    protesi = 1
    or (estenosi = 1 and aco = 1)
    or (chads2 >= 3 and hasbled < 3 and aco = 1 and aas=0)
    or (chads2 >= 3 and hasbled < 3 and aco=1 and (aas = 1 or clopi = 1) and mcv = 1)
    or (chads2 >= 3 and hasbled >= 3 and aas=0)
    or (chads2 >= 3 and hasbled >= 3 and (aas=1 or clopi=1) and mcv=1)
    or (chads2 = 2 and sexe = 'D' and aas = 0)
    or (chads2 = 2 and sexe = 'D' and (aas = 1 or clopi = 1) and mcv=1)
    or (chads2 = 2 and sexe = 'H' and hasbled < 3 and aco = 1 and aas=0)
    or (chads2 = 2 and sexe = 'H' and hasbled < 3 and aco = 1 and (aas = 1 or clopi = 1) and mcv=1)
    or (chads2 = 2 and sexe = 'H' and hasbled >= 3 and aas=0)
    or (chads2 = 2 and sexe = 'H' and hasbled >= 3 and (aas = 1 or clopi= 1) and mcv=1) 
    or (chads2 = 1 and sexe = 'D' and aco = 0 and aas = 0 )
    or (chads2 = 1 and sexe = 'D' and aco = 0 and (aas = 1 or clopi = 1) and mcv=1)
    or (chads2 = 1 and sexe = 'H'  and aas=0)
    or (chads2 = 1 and sexe = 'H' and (aas = 1 or clopi = 1) and mcv=1)
    or (chads2 = 0 and aco = 0 and aas = 0 and clopi = 0 and estenosi = 0)
    or (chads2 = 0 and aco = 0 and (aas = 1 or clopi = 1)and mcv=1)
;

update eqa_acfa
set ci = 1
where
    (ci_aas = 1 and chads2 = 1)
    or (ci_aas = 1 and chads2 = 2 and hasbled >= 3)
    or (ci_aas = 1 and ci_aco = 1 and chads2 > 1)
    or (ci_aas = 1 and ci_aco = 1 and chads2 = 1 and sexe = 'H' and hasbled < 3)
;

update eqa_acfa a inner join nodrizas.eqa_tractaments b on a.id_cip_sec = b.id_cip_sec
set ci = 1
where farmac = 129
;

update eqa_acfa a inner join nodrizas.eqa_problemes b on a.id_cip_sec = b.id_cip_sec
set excl = 1
where ps in (510, 706)
;

update eqa_acfa 
set excl = 1
where ci_aco=1
;

update eqa_acfa a inner join nodrizas.eqa_problemes b on a.id_cip_sec = b.id_cip_sec
set clin = 1
where ps = 839
;

drop table if exists trombocit;
create table trombocit select id_cip_sec, 0 as plaquetes from nodrizas.eqa_problemes where ps = 42;
alter table trombocit add index (id_cip_sec);
update trombocit a inner join nodrizas.eqa_variables b on a.id_cip_sec = b.id_cip_sec set plaquetes = 1 where agrupador = 509 and usar = 1 and valor < 50;

update eqa_acfa a inner join trombocit b on a.id_cip_sec = b.id_cip_sec
set excl = 1
where plaquetes = 1
;


set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=1);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=1
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
    id_cip_sec
    ,up
    ,uba
    ,upinf
    ,ubainf
    ,edat
    ,sexe
    ,seccio_censal
    ,up_residencia
    ,institucionalitzat
    ,maca
    ,ates
    ,trasplantat
    ,ind_codi
    ,num
    ,excl
   ,ci
   ,clin
from
    eqa_acfa
where
    den=1 
");



    
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;




SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=1);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;
