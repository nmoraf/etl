from sisapUtils import *

bd = "eqa_ind"
base = "eqa_relacio"
base2 = "eqa_ind"
taula ="%s_agudes" % (base)
conn = connect(bd)
c = conn.cursor()
c.execute("select distinct taula from %s where taula <>''" % base)
r=c.fetchall()
for t in r:
   nom = t[0]
   c.execute("select distinct tipus from %s where taula <>''" % base)
   r2=c.fetchall()
   for t2 in r2:
      tipus = t2[0]
      taula2 = "%s_%s_%s" % (taula,nom,tipus)
      c.execute("drop table if exists %s" % taula2)
c.execute("drop table if exists %s" % taula)
conn.close()