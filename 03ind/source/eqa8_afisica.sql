use eqa_ind
;

set @fa2anys = (select date_add(date_add(data_ext, interval -2 year), interval +1 day) from nodrizas.dextraccio);
set @fa3mesos = (select date_add(date_add(data_ext, interval -3 month), interval +1 day) from nodrizas.dextraccio);
set @avui = (select data_ext from nodrizas.dextraccio);

#ACTIVITAT F�SICA
drop table if exists eqa_actfisica;
create table eqa_actfisica (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
fr double,
vP1101_1a double,
VA3030_1a double,
CA331_1a double,
EP4012_1a double,
EA3301_1a double,
sedentaris double,
vP1101_ara double,
VA3030_ara double,
CA331_ara double,
EP4012_ara double,
EA3301_ara double,
num double,
den double,
excl double,
ci double,
clin double
)
select
	a.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
   ,0 as fr
   ,null as VP1101_1a
   ,null as VA3030_1a
   ,null as CA331_1a
   ,null as EP4012_1a
   ,null as EA3301_1a
   ,0 as sedentaris
   ,null as VP1101_ara
   ,null as VA3030_ara
   ,null as CA331_ara
   ,null as EP4012_ara
   ,null as EA3301_ara
   ,0 as num
   ,0 as den
   ,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a
   ,eqa_ind c
where
	 c.tipus=8
;

#Haur�em de parametritzar, per� hi ha poc temps aix� que m�s endavant...

drop table if exists ps_actf;
create table ps_actf(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('18','24','55','239','47','267') group by id_cip_sec
;
update eqa_actfisica a inner join ps_actf b on a.id_cip_sec=b.id_cip_sec
set fr=1 
;
drop table if exists ob_actf;
create table ob_actf(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_variables where agrupador=19 and valor>30 and usar=1 group by id_cip_Sec
;
update eqa_actfisica a inner join ob_actf b on a.id_cip_sec=b.id_cip_sec
set fr=1 
;

drop table if exists tabac_actf;
create table tabac_actf(
id_cip_sec double,
index(id_cip_sec)
)
select
   id_cip_sec
from
   nodrizas.eqa_tabac
where
   tab=1 and last=1
;

update eqa_actfisica a inner join tabac_actf b on a.id_cip_sec=b.id_cip_sec
set fr=1 
;

drop table if exists atdom_actf;
create table atdom_actf(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('45','86','428','301','429','788','557') group by id_cip_sec
;
update eqa_actfisica a inner join atdom_actf b on a.id_cip_sec=b.id_cip_sec
set clin=1 
;

drop table if exists exclusions_v;
create table exclusions_v(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_variables where agrupador = 232 and usar = 1 and valor = 4
;
insert into exclusions_v select id_cip_sec from nodrizas.eqa_variables where agrupador = 789 and usar = 1 and valor = -2
;
insert into exclusions_v  select id_cip_sec from nodrizas.eqa_problemes where ps=62 and pr_gra=4
;
update eqa_actfisica a inner join exclusions_v b on a.id_cip_sec=b.id_cip_sec
set excl=1 
;

#Mirem que hagi estat sedentari en els �ltims 2 anys i fins fa 3m

drop table if exists VP1101_1a;
create table VP1101_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =240 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join VP1101_1a b on a.id_cip_sec=b.id_cip_sec
set VP1101_1a=valor; 

drop table if exists VA3030_1a;
create table VA3030_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,min(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =241 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join VA3030_1a b on a.id_cip_sec=b.id_cip_sec
set VA3030_1a=valor;


drop table if exists CA331_1a;
create table CA331_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(if(valor = 2 and data_var > 20190131, 3, valor)) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =242 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join CA331_1a b on a.id_cip_sec=b.id_cip_sec
set CA331_1a=valor;

drop table if exists EP4012_1a;
create table EP4012_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,min(if(valor=0,9999,if(valor=5,0,valor))) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =243 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join EP4012_1a b on a.id_cip_sec=b.id_cip_sec
set EP4012_1a=valor;

drop table if exists EA3301_1a;
create table EA3301_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,min(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador = 859 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join EA3301_1a b on a.id_cip_sec=b.id_cip_sec
set EA3301_1a=valor;


drop table if exists fisica_ara1;
create table fisica_ara1(
id_cip_sec double,
agrupador double,
valor double,
usar double
)
select id_cip_sec,agrupador,if(agrupador = 242 and valor = 2 and data_var > 20190131, 3, valor) valor,usar from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('240','241','242','243','859') and data_var between @fa2anys and @avui
;
drop table if exists fisica_aramax;
create table fisica_aramax(
id_cip_sec double,
agrupador double,
usar double,
primary key(id_cip_sec,agrupador,usar)
)
select id_cip_sec,agrupador,min(usar) as usar from fisica_ara1 group by id_cip_sec,agrupador
;

drop table if exists fisica_ara;
create table fisica_ara(
id_cip_sec double,
agrupador double,
valor double,
index(id_cip_Sec,agrupador,valor)
)
select a.id_cip_sec,a.agrupador,valor from fisica_ara1 a inner join fisica_aramax b on a.id_cip_sec=b.id_cip_sec and a.agrupador=b.agrupador and a.usar=b.usar
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set VP1101_ara=valor where agrupador=240
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set VA3030_ara=valor where agrupador=241
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set CA331_ara=valor where agrupador=242
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set EP4012_ara=if(valor=0,9999,if(valor=5,0,valor)) where agrupador=243
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set EA3301_ara=valor where agrupador=859
;

update eqa_actfisica
set sedentaris=1 where VP1101_1a in ('3','4','5') or  VA3030_1a in ('0','1') or CA331_1a=3 or EP4012_1a in ('1','2','5','0') or EA3301_1a < 4
;

update eqa_actfisica
set den=1 where fr=1 and sedentaris=1
;
update eqa_actfisica
set num=1 where VP1101_ara in ('1','2') or  VA3030_ara in ('3','2','4') or CA331_ara=1 or EP4012_ara in ('3','4') or EA3301_ara > 3
;
update eqa_actfisica
set num=1 where (VP1101_1a >VP1101_ara) or (CA331_1a>CA331_ara) or (VA3030_1a < VA3030_ara) or (EP4012_1a < EP4012_ara) or (EA3301_1a < EA3301_ara)
;


set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=8);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=8
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_actfisica
where
	den=1 
");



PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;


SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=8);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;


drop table if exists ps_actf;
drop table if exists ob_actf;
drop table if exists VP1101_1a;
drop table if exists VA3030_1a;
drop table if exists CA331_1a;
drop table if exists EP4012_1a;
drop table if exists EA3301_1a;
drop table if exists fisica_ara1;
drop table if exists fisica_aramax;
drop table if exists fisica_ara;
drop table if exists atdom_actf;
drop table if exists eqa_actfisica;