use eqa_ind;

set @fa2anys = (select date_add(date_add(data_ext, interval -2 year), interval +1 day) from nodrizas.dextraccio);
set @fa3mesos = (select date_add(date_add(data_ext, interval -3 month), interval +1 day) from nodrizas.dextraccio);
set @avui = (select data_ext from nodrizas.dextraccio);


#reducci� pes en obesitat i sobrepes
drop table if exists eqa_pes;
create table eqa_pes (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
obesitat double,
IMC_1a double,
IMC_ara double,
pes_1a double,
pes_ara double,
num double,
den double,
excl double,
ci double,
clin double
)
select
	a.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
   ,0 as obesitat
   ,0 as IMC_1a
   ,0 as IMC_ara
   ,'' as pes_1a
   ,'' as pes_ara
   ,0 as num
   ,0 as den
   ,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a
   ,eqa_ind c
where
	 c.tipus=9
;

#Haur�em de parametritzar, per� hi ha poc temps aix� que m�s endavant...

drop table if exists ps_obes;
create table ps_obes(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('239','267') group by id_cip_sec
;
update eqa_pes a inner join ps_obes b on a.id_cip_sec=b.id_cip_sec
set obesitat=1
;

#Agafo el m�xim de IMC (>25) dels �ltims 2a i fins a 3m de la data_exr

drop table if exists IMC_1a;
create table IMC_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('19') and valor>25 and data_var between @fa2anys and @fa3mesos group  by id_cip_sec
;

update eqa_pes a inner join IMC_1a b on a.id_cip_sec=b.id_cip_sec
set IMC_1a=valor
;

#Agafo �ltim IMC


drop table if exists IMC_ara;
create table IMC_ara(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('19')  and usar=1 and data_var between @fa2anys and @avui
;

update eqa_pes a inner join IMC_ara b on a.id_cip_sec=b.id_cip_sec
set IMC_ara=valor
;

#Agafo el m�xim de pes (>25) dels �ltims 2a i fins a 3m de la data_ext

drop table if exists pes_1a;
create table pes_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('268') and data_var between @fa2anys and @fa3mesos group  by id_cip_sec
;

update eqa_pes a inner join pes_1a b on a.id_cip_sec=b.id_cip_sec
set pes_1a=valor
;

#Agafo �ltim pes

drop table if exists pes_ara;
create table pes_ara(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('268')  and usar=1 and data_var between @fa2anys and @avui
;

update eqa_pes a inner join pes_ara b on a.id_cip_sec=b.id_cip_sec
set pes_ara=valor
;

update eqa_pes
set den=1 where obesitat=1 or IMC_1a>25
;

update eqa_pes
set num=1 where IMC_ara<26
;

update eqa_pes
set num=1 where pes_ara<=(0.95*pes_1a)
;


drop table if exists ps_ob_excl;
create table ps_ob_excl(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('39', '428') group by id_cip_sec
;

update eqa_pes a inner join ps_ob_excl b on a.id_cip_sec=b.id_cip_sec
set excl=1
;

set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=9);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=9
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_pes
where
	den=1 
");


PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;


SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();



set @maxg=(select distinct ind_codi from  eqa_ind where tipus=9);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;

drop table eqa_pes;
drop table ps_obes;
drop table IMC_1a;
drop table IMC_ara;
drop table pes_1a;
drop table pes_ara;
