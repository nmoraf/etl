from sisapUtils import *

bd = "eqa_ind"
base = "eqa_relacio"
conn = connect(bd)
c = conn.cursor()
c.execute("select distinct taula from %s" % base)
r=c.fetchall()
for t in r:
   nom = t[0]
   c.execute("select distinct tipus from %s" % base)
   r2=c.fetchall()
   for t2 in r2:
      tipus = t2[0]
      taula = "%s_%s_%s" % (base,nom,tipus)
      c.execute("drop table if exists %s" % taula)
conn.close()