# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter

impj = 'import_jail'
eqa = 'eqa_ind'

llistats = 'exp_ecap_pacient'
khalix = 'exp_khalix_up_ind'

indicadors = ['EQD2001', 'EQD2002', 'EQA2000']

jail_particulars = ['EQA0312', 'EQA0308', 'EQA0309']


def get_Hash():
    hashos = {}
    sql = 'select id_cip_sec, codi_sector, hash_d from u11'
    for id, sector, hash in getAll(sql, impj):
        hashos[id] = {'sector': sector, 'hash': hash}
    return hashos


def insert_tables(indicador, hashos):
    expKhx = Counter()
    table = indicador + 'A_ind2'
    execute("delete a.* from {0} a where grup_codi = '{1}'".format(llistats, indicador), eqa) 
    execute("delete a.* from {0} a where left(indicador, length(indicador) - 1) = '{1}'".format(khalix, indicador), eqa)
    sql = 'select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, ind_codi, num from {} where excl=0 and ci=0 and clin=0 and maca=0 and institucionalitzat=0 and id_cip_sec < 0'.format(table)
    for id, up, uba, upinf, ubainf, edat, sexe, ates, ind_codi, num in getAll(sql, eqa):
        grup_codi = ind_codi[:-1]
        if num == 0 and ates == 1:
            try:
                hash = hashos[id]['hash']
                sector = hashos[id]['sector']
            except KeyError:
                continue
            execute("insert into {0} values({1}, '{2}', '{3}', '{4}', '{5}', '{6}', 0, '{7}', '{8}')".format(llistats, id, up, uba, upinf, ubainf, grup_codi, hash, sector), eqa)
        comb = 'NOINSAT'
        if ates == 0:
            comb = 'NOINSASS'
        ed = ageConverter(int(edat), 5)
        sex = sexConverter(sexe)
        expKhx[(up, ed, sex, comb, ind_codi, 'NUM')] += num
        expKhx[(up, ed, sex, comb, ind_codi, 'DEN')] += 1
    for (up, ed, sex, comb, ind_codi, tip), valor in expKhx.items():
        execute("insert into {0} values('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7})".format(khalix, up, ed, sex, comb, ind_codi, tip, valor), eqa)

        
def insert_particulars(indicador, hashos):
    execute("delete a.* from {0} a where grup_codi = '{1}' and id_cip_sec <0".format(llistats, indicador), eqa) 
    sql = "select id_cip_sec,up,uba,upinf,ubainf,grup_codi,maca,institucionalitzat,excl_edat,excl,ci,clin,if(maca=1,1,if(institucionalitzat=1,2,if(excl_edat=1,3,if(clin=1,4,if(ates=0,5,if(ci=1,6,0))))))\
    ,1 from mst_indicadors_pacient where num=0 and excl=0 and ci=0 and grup_codi='{0}' and id_cip_sec <0".format(indicador)
    for id, up, uba, upinf, ubainf, grup, maca, insti, excl_edat,excl,ci,clin,marca, llistat in getAll(sql, eqa):
        try:
            hash = hashos[id]['hash']
            sector = hashos[id]['sector']
        except KeyError:
            continue
        execute("insert into {0} values({1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')".format(llistats, id, up, uba, upinf, ubainf, grup, marca,hash, sector), eqa)
        
      
hashos = get_Hash()
for indicador in indicadors:
    insert_tables(indicador, hashos)

for indicador in jail_particulars:
    insert_particulars(indicador, hashos)
