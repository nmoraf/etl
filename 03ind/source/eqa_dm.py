# coding: latin1

"""
Proc�s de c�lcul de l'EQA del proc�s DM2.

Es distingeixen 3 tipus d'indicadors:
1-Els que s�n id�ntics que a l'EQA d'adults -> per ecap els duplicarem
  al cat�leg, i klx far� el que hagi de fer
2-Els que s�n iguals que a l'EQA d'adults per� filtrats per poblaci�
  amb DM2 -> ho fem aqu�
3-Nous indicadors -> ho fem aqu�
"""

import collections as c
import simplejson as j

import sisapUtils as u


tb_pac = 'mst_indicadors_pacient_dm2'
tb_cat = 'mst_cataleg_dm2'
db = 'eqa_ind'
proc = 'DM2'


class DM2(object):
    """."""

    def __init__(self):
        """."""
        self.create_tables()
        self.get_poblacio()
        self.get_diabetics()
        self.get_exitus()
        self.get_indicadors()
        for codi, especificacio in self.indicadors.items():
            if especificacio['do']:
                Indicador(codi, especificacio,
                          self.poblacio, self.diabetics, self.exitus)

    def create_tables(self):
        """."""
        for tb in (tb_pac, tb_cat):
            orig = tb.replace('dm2', 'mcv')
            u.createTable(tb, 'like {}'.format(orig), db, rm=True)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, edat, up, uba, upinf, ubainf, \
                      ates, institucionalitzat from assignada_tot \
                      where maca = 0 and trasplantat = 0"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, 'nodrizas')}

    def get_diabetics(self):
        """."""
        sql = "select id_cip_sec from eqa_problemes where ps = 18"
        self.diabetics = {id: self.poblacio[id] for id,
                          in u.getAll(sql, 'nodrizas') if id in self.poblacio}

    def get_exitus(self):
        """."""
        sql = "select id_cip_sec from rip_problemes where ps in (18, 700)"
        dm = set([id for id, in u.getAll(sql, 'nodrizas')])
        self.exitus = {k: v for (k, v) in self.diabetics.items()}
        sql = "select id_cip_sec, edat, up, uba, '', '', 1, 0 \
              from rip_assignada"
        for row in u.getAll(sql, 'nodrizas'):
            if row[0] in dm:
                self.exitus[row[0]] = row[1:]

    def get_indicadors(self):
        """."""
        file = 'dades_noesb/eqa_dm.json'
        try:
            stream = open(file)
        except IOError:
            stream = open('../' + file)
        self.indicadors = j.load(stream)


class Indicador(object):
    """."""

    def __init__(self, codi, especificacions, poblacio, diabetics, exitus):
        """."""
        self.codi = codi
        self.especificacions = especificacions
        self.poblacio_all = poblacio
        self.diabetics = diabetics
        self.exitus = exitus
        self.insert_cataleg()
        if 'eqa' not in self.especificacions:
            if self.especificacions.get('filtrar'):
                self.filtrar_indicador()
            else:
                self.get_poblacio()
                self.get_denominador()
                self.get_numerador()
                self.get_exclusions()
                self.get_pacients()
            self.upload_pacients()

    def insert_cataleg(self):
        """."""
        is_eqa = 1 * ('eqa' in self.especificacions)
        cataleg = (self.codi,
                   self.especificacions.get('literal', '').encode('latin1'),
                   self.especificacions['dimensio'],
                   is_eqa,
                   9 if is_eqa else 1 - self.especificacions.get('hide', False),
                   9 if is_eqa else 1 * self.especificacions.get('invers', False),
                   9 if is_eqa else 1 * self.especificacions.get('llistat', True))
        u.listToTable([cataleg], tb_cat, db)

    def filtrar_indicador(self):
        """."""
        original = self.especificacions['filtrar']
        sql = "select id_cip_sec, '{}', '{}', up, uba, upinf, ubainf, \
                      ates, institucionalitzat, den, num, excl \
                      from mst_indicadors_pacient \
                      where ind_codi = '{}' and \
                            maca = 0 and \
                            trasplantat = 0 and \
                            excl_edat = 0 and \
                            ci = 0 and \
                            clin = 0".format(self.codi,
                                             self.especificacions['dimensio'],
                                             original)
        self.pacients = [row for row in u.getAll(sql, 'eqa_ind')
                         if int(row[0]) in self.diabetics]

    def get_poblacio(self):
        """."""
        tipus = self.especificacions['poblacio']
        if tipus == 'dm2':
            original = self.diabetics
        elif tipus == 'rip':
            original = self.exitus
        elif tipus == 'all':
            original = self.poblacio_all
        emin, emax = self.especificacions['edat']
        self.poblacio = {id: valors[1:] for (id, valors)
                         in original.items()
                         if emin <= valors[0] <= emax}

    @staticmethod
    def get_tuple(codis):
        """."""
        if len(codis) == 1:
            return "('{}')".format(codis[0])
        else:
            return tuple(codis)

    @staticmethod
    def get_sql(especificacions):
        """."""
        conceptes = {('eqa_variables', 'codi'): 'agrupador',
                     ('eqa_variables', 'data'): 'data_var',
                     ('eqa_problemes', 'codi'): 'ps',
                     ('eqa_problemes', 'data'): 'dde',
                     ('rip_problemes', 'codi'): 'ps',
                     ('rip_problemes', 'data'): 'dde',
                     ('eqa_microalb', 'codi'): 'agrupador',
                     ('eqa_tractaments', 'codi'): 'farmac',
                     ('eqa_tractaments', 'data'): 'pres_orig',
                     ('gma', 'codi'): 'gma_cod',
                     ('gma', 'db'): 'import',
                     ('eqa_ram', 'codi'): 'agr'}
        if isinstance(especificacions, dict):
            relacio = "o"
            especificacions = [especificacions]
        else:
            relacio = especificacions[0]
            especificacions = especificacions[1:]
        sqls = []
        for especificacio in especificacions:
            taula = especificacio['taula']
            codis = Indicador.get_tuple(especificacio['agrupadors'])
            temps = especificacio.get('temps')
            last = especificacio.get('last')
            valors = especificacio.get('valors')
            sql = "select id_cip_sec \
                   from {}, nodrizas.dextraccio \
                   where {} in {}".format(taula,
                                          conceptes[(taula, 'codi')],
                                          codis)
            if temps:
                sql += " and {} between adddate(adddate(data_ext, \
                                                interval - {}), \
                                        interval +1 day) and data_ext".format(
                                                    conceptes[(taula, 'data')],
                                                    temps)
            if last:
                sql += " and usar = {}".format(last)
            if valors:
                sql += " and valor between {} and {}".format(*valors)
            db = conceptes.get((taula, 'db'), 'nodrizas')
            sqls.append((sql, especificacio.get('not'), db, relacio))
        return sqls

    def get_denominador(self):
        """."""
        if self.especificacions.get('denominador'):
            self.denominador = {}
            sqls = Indicador.get_sql(self.especificacions['denominador'])
            for sql, invertir, db, relacio in sqls:
                component = {id: self.poblacio[id]
                             for id, in u.getAll(sql, db)
                             if id in self.poblacio}
                if invertir:
                    component = {k: v for (k, v) in self.poblacio.items()
                                 if k not in component}
                if relacio == 'o' or len(self.denominador) == 0:
                    for k, v in component.items():
                        self.denominador[k] = v
                elif relacio == 'i':
                    self.denominador = {k: v for (k, v)
                                        in self.denominador.items()
                                        if k in component}
        else:
            self.denominador = self.poblacio

    def get_numerador(self):
        """."""
        self.numerador = set()
        sqls = Indicador.get_sql(self.especificacions['numerador'])
        for sql, invertir, db, relacio in sqls:
            component = set([id for id, in u.getAll(sql, db)
                             if id in self.denominador])
            if invertir:
                component = set(self.denominador) - component
            if relacio == 'o' or len(self.numerador) == 0:
                self.numerador |= component
            elif relacio == 'i':
                self.numerador &= component

    def get_exclusions(self):
        """."""
        self.exclusions = set()
        if self.especificacions.get('exclusions'):
            sqls = Indicador.get_sql(self.especificacions['exclusions'])
            for sql, invertir, db, relacio in sqls:
                component = set([id for id, in u.getAll(sql, db)
                                 if id in self.denominador])
                if invertir:
                    component = set(self.denominador) - component
                if relacio == 'o' or len(self.exclusions) == 0:
                    self.exclusions |= component
                elif relacio == 'i':
                    self.exclusions &= component

    def get_pacients(self):
        """."""
        self.pacients = [(id, self.codi, self.especificacions['dimensio']) +
                         valors +
                         (1,
                          1 * (id in self.numerador),
                          1 * (id in self.exclusions))
                         for (id, valors) in self.denominador.items()]

    def upload_pacients(self):
        """."""
        u.listToTable(self.pacients, tb_pac, db)


if __name__ == '__main__':
    DM2()
