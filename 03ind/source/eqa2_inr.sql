set @fa1any = (select date_add(date_add(data_ext, interval -1 year), interval +1 day) from nodrizas.dextraccio);
set @avui = (select data_ext from nodrizas.dextraccio);
set @dema = (select date_add(data_ext, interval +1 day) from nodrizas.dextraccio);
#si es volgu�s m�s de 18 mesos enrera (ara fem 15), canviar a eqa0.sql de nodrizas
set @fa15mesos = (select date_add(date_add(date_add(data_ext, interval -1 year), interval +1 day), interval -3 month) from nodrizas.dextraccio);
set @marge=0.2;

drop table if exists eqa_inr;
create table eqa_inr (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
aco double,
inr double,
num double,
den double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
    id_cip_sec
    ,up
    ,uba
    ,upinf
    ,ubainf
    ,edat
    ,ates
    ,trasplantat
    ,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
    ,maca
    ,b.ind_codi
    ,0 aco
    ,0 inr
    ,0 num
    ,1 den
    ,0 excl
    ,0 ci
    ,0 clin
from
    eqa_acfa a
    ,eqa_ind b
where
    b.tipus = 2
;

update eqa_inr a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec
set aco=1  where farmac = 3
;

drop table if exists inr0;
create table inr0 like nodrizas.eqa_variables;
insert into inr0 select * from nodrizas.eqa_variables where agrupador in (195,262,263)
;

alter table inr0 add index n(id_cip_sec)
;

update eqa_inr a inner join nodrizas.eqa_variables_tao b on a.id_cip_sec=b.id_cip_sec
set inr = 1
where agrupador='DSINT' and usar=6
;

delete from eqa_inr where aco+inr < 2
;

drop table if exists inrRang;
create table inrRang (
  id_cip_sec double
  ,inf double not null default 2
  ,sup double not null default 3
  ,canviat int
);

insert into inrRang (id_cip_sec)
select id_cip_sec from eqa_inr
;

update inrRang a inner join inr0 b on a.id_cip_sec=b.id_cip_sec
set inf = valor,canviat=1
where agrupador=262 and usar=1
;

update inrRang a inner join inr0 b on a.id_cip_sec=b.id_cip_sec
set sup = valor,canviat=1
where agrupador=263 and usar=1
;

alter table inrRang add unique (id_cip_sec,inf,sup)
;

drop table if exists inr1;
create table inr1 (
  id_cip_sec double
  ,data_var date
  ,valor double
  ,usar int
)
select
   id_cip_sec
   ,data_var
   ,valor
   ,usar
from
    inr0 a
where
    exists (select 1 from eqa_inr b where a.id_cip_sec=b.id_cip_sec)
    and agrupador = 195
;

drop table if exists inr2;
create table inr2 like inr1;
insert into inr2
select 
    a.*
from
    inr1 a
where
    data_var between @fa1any and @avui
;

drop table if exists inr3;
create table inr3 (
  id_cip_sec double
  ,usar int
  ,primary key (id_cip_sec,usar)
)
select
    id_cip_sec
    ,max(usar)+1 usar
from
    inr2
group by
    id_cip_sec
having
    min(data_var) > @fa1any
;

drop table if exists inr4;
create table inr4 like inr2;
insert into inr4
select 
    id_cip_sec
    ,@fa1any data_var
    ,valor
    ,usar
from
    inr1 a
where
    exists (select 1 from inr3 b where a.id_cip_sec=b.id_cip_sec and a.usar=b.usar and data_var between @fa15mesos and @fa1any)
;

drop table if exists inr5;
create table inr5 like inr4;
insert into inr5
select
    id_cip_sec
    ,@dema data_var
    ,-1 valor
    ,0 usar
from
    eqa_inr
;

drop table if exists inr6;
create table inr6 (
  id bigint not null AUTO_INCREMENT
  ,id_cip_sec double
  ,data_var date
  ,valor double
  ,origen int
  ,dies int
  ,ok_a int
  ,ok_e int
  ,primary key (id)
)
select * from (
select id_cip_sec,data_var,valor,1 origen,-1 dies,-1 ok_a,-1 ok_e from inr2
union
select id_cip_sec,data_var,valor,0 origen,-1 dies,-1 ok_a,-1 ok_e from inr4
union
select id_cip_sec,data_var,valor,2 origen,-1 dies,-1 ok_a,-1 ok_e from inr5
) a
order by id_cip_sec,data_var
;

set @max=(select count(1) from inr6);
#set @max=100;
drop procedure if exists inr_days;

delimiter //
CREATE PROCEDURE inr_days()
BEGIN
    DECLARE p INT;
    DECLARE cip_act DOUBLE;
    DECLARE cip_ant DOUBLE;
    DECLARE dat_act DATE;
    DECLARE dat_ant DATE;
    DECLARE rang_i_a DOUBLE;
    DECLARE rang_s_a DOUBLE;
    DECLARE rang_i_e DOUBLE;
    DECLARE rang_s_e DOUBLE;
    
    SET p=2;
    lbl: LOOP
        select id_cip_sec,data_var from inr6 where id=p INTO cip_act,dat_act;
        select id_cip_sec,data_var from inr6 where id=p-1 INTO cip_ant,dat_ant;
        select inf-@marge,sup+@marge,inf,sup from inrRang where id_cip_sec=cip_ant INTO rang_i_a,rang_s_a,rang_i_e,rang_s_e;
        if cip_act = cip_ant then
            update inr6 set dies=datediff(dat_act,dat_ant),ok_a=if(valor between rang_i_a and rang_s_a,1,0),ok_e=if(valor between rang_i_e and rang_s_e,1,0) where id=p-1;
        end if;
        SET p=p+1;
        IF p > @max
            THEN LEAVE lbl;
        END IF;
    END LOOP;
END
//
delimiter ;

call inr_days();

drop table if exists inr7;
create table inr7 (
  id_cip_sec double
  ,num_a double
  ,num_e double
  ,den double
  ,ind_a double
  ,ind_e double
)
select
    id_cip_sec
    ,sum(if(ok_a=1,dies,0)) num_a
    ,sum(if(ok_e=1,dies,0)) num_e
    ,sum(dies) den
    ,sum(if(ok_a=1,dies,0)) / sum(dies) ind_a
    ,sum(if(ok_e=1,dies,0)) / sum(dies) ind_e
from
    inr6
where
    ok_a <> -1
group by
    id_cip_sec
;

update eqa_inr a inner join inr7 b on a.id_cip_sec=b.id_cip_sec
set a.num = 1
where b.ind_a >= 0.6
;
/*
drop table if exists excl_tao;
create table excl_tao as (select id_cip_sec from import.prealt2 where val_var = 'TAO3' and val_hist = 1 and val_val='E')
;
alter table excl_tao add index(id_cip_sec)
;
update eqa_inr a inner join excl_tao b on a.id_cip_sec=b.id_cip_sec
set excl = 1
;
*/
drop table if exists inr0;
drop table if exists inrRang;
drop table if exists inr1;
drop table if exists inr2;
drop table if exists inr3;
drop table if exists inr4;
drop table if exists inr5;
drop table if exists inr6;
drop table if exists inr7;

#proc�s general ermen
set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=2);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=2
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_inr
where
	den=1 
");


	
PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;



SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=2);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;
