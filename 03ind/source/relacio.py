from sisapUtils import *

bd = "eqa_ind"
base = "eqa_relacio"
conn = connect(bd)
c = conn.cursor()
c.execute("select distinct taula from %s" % base)
r=c.fetchall()
for t in r:
   nom = t[0]
   c.execute("select distinct tipus from %s" % base)
   r2=c.fetchall()
   for t2 in r2:
      tipus = t2[0]
      taula = "%s_%s_%s" % (base,nom,tipus)
      c.execute("drop table if exists %s" % taula)
      c.execute("create table %s like %s" % (taula,base))
      c.execute("insert into %s select * from %s where taula='%s' and tipus='%s' and baixa=0" % (taula,base,nom,tipus))
      c.execute("alter table %s add column inici date,add column final date" % taula)
      c.execute("update %s,nodrizas.dextraccio set inici=date_add(date_add(data_ext,interval - data_min month),interval +1 day),final=date_add(data_ext,interval - data_max month)" % taula)
      c.execute("alter table %s add index (agrupador,ind_codi,valor_min,valor_max,sense,inici,final)" % taula)
conn.close()