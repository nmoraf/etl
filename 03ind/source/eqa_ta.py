from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

nod="nodrizas"
validation=True
limit=""
#Output table
db_out="eqa_ind"

#Codis
agr_codi_ta=(55,21,7,212,622,211,1,768,180)
agr_TA=16

agr_codi_ldl=(1,211,7,212,622)
agr_LDL=9

#Indicadores:
indicadores={
           "EQA3001A":(agr_codi_ta,agr_TA,"EQA3001A_ind2"),
           "EQA3002A":(agr_codi_ldl,agr_LDL,"EQA3002A_ind2")
          }

##Functions and clases:
class MesuraInMVC(object):
    """Taking TA in cardiovascular disease"""
    def __init__(self):
        self.date_dextraccio=self.get_date_dextraccio()
        printTime("Dia de hoy y agrupadores") 
      

    def get_agrupador_tancat(self,codis):
        """Get the agrupador from nodizas.eqa_criteris according to the agrupador_dec(siglas enfermedad) with the tancat/obert state.
           Returns a dictionary {agr: 0/1}.
        """
        sql="select distinct agrupador,ps_tancat from nodrizas.eqa_criteris where agrupador in {};".format(codis)
        self.agr_tancat={agr:ps_tancat for agr,ps_tancat in getAll(sql,nod)}

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]
    
    def get_mesure(self,agr):
        """Get patient ids whose mesure has been taken in the last 12 months.
           Returns a set of patient ids.
        """
        sql="select id_cip_sec,data_var from {}.eqa_variables where agrupador={} {};".format(nod,agr,limit)
        return {id for id, data_var in getAll(sql,nod) if monthsBetween(data_var,self.date_dextraccio) < 12}
    
    def get_denominador(self):
        """Get patient ids that have been diagnosed with one of the associated malaties using the agrupadores. If the agrupador refers to a closed case 
           (value 1 in the dictionary {agr:0/1}) then the diagnosis date has to be in the last 5 years to be included in the denominador. 
           Otherwise (value 0 in the dictionary {agr:0/1}), the id is included in the denominador.
           Returns a set of patient ids.
         """
        denominador=set()
        sql="select id_cip_sec, dde,ps from {}.eqa_problemes where ps in {} {};".format(nod,tuple(self.agr_tancat.keys()), limit)
        for id,dde,ps in getAll(sql,nod):
            if self.agr_tancat[ps] == 1 and yearsBetween(dde, self.date_dextraccio) < 5:
                denominador.add(id)
            elif self.agr_tancat[ps]==0:
                denominador.add(id)
        return denominador 

    def get_rows(self,denominador,numerador,codi_indicador):
        """Get rows of the table using the denominador and numerador (both a set of ids).
           Returns a list of tuples (rows), each one with the fields of the new table
        """
        rows=[]
        sql="select id_cip_sec,up,uba,upinf,ubainf,ates,institucionalitzat from {}.assignada_tot {};".format(nod,limit)
        for id,up,uba,upinf,ubainf,ates,insti in getAll(sql,nod):
            if id in denominador:
                num=0
                if id in numerador:
                    num=1
                rows.append((id,codi_indicador,up,uba,upinf,ubainf,ates,insti,num,0))
        return rows
    
    def export_table(self,table,columns,db,rows):
        createTable(table, columns, db, rm=True)
        listToTable(rows, table, db)


if __name__=="__main__":

    for indicador, (agr_codis,agr_mesure,table_name) in indicadores.items():
        print(indicador)
        print(agr_codis,agr_mesure,table_name)
        eqa= MesuraInMVC()
        eqa.get_agrupador_tancat(agr_codis)
        print(eqa.agr_tancat)
        denominador=eqa.get_denominador()
        printTime("Denominador")
        numerador=eqa.get_mesure(agr_mesure)
        printTime("Numerador")
        rows=eqa.get_rows(denominador,numerador,indicador)
        printTime("All rows")
        eqa.export_table(table_name,
                "(id_cip_sec int(11), ind_codi varchar(11), up varchar(11), uba varchar(11),upinf varchar(11), ubainf varchar(11), ates int, institucionalitzat int, num int, excl int)",
                db_out,
                rows)