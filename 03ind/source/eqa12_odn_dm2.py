# coding: iso-8859-1


from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


nod = "nodrizas"
imp = "import"
db = "eqa_ind"

              
indicador, = getOne('select ind_codi from eqa_ind where tipus=12', db)
                
class odn_dm2(object):
    """."""
    
    def __init__(self):
        """."""
        printTime('Inici')
        self.get_date_dextraccio()
        self.get_dm2()
        self.get_exclusions()
        self.get_atdom()
        self.get_glicada()
        self.get_numerador()
        self.get_indicadors()
        self.export_data()
        printTime('Fi')
        
    def get_date_dextraccio(self):
        """"""
        sql="select data_ext from {0}.dextraccio".format(nod)
        self.current_date = getOne(sql,nod)[0]
        print self.current_date
    
    def get_dm2(self):
        """Agafem dm2"""
        
        self.dm2 = set()
        sql = "select id_cip_sec from eqa_problemes where ps=18"
        for id,  in getAll(sql, nod):
            self.dm2.add(int(id))
            
    def get_exclusions(self):
        """Pacients sense dents"""
        
        self.exclusions = Counter()
        sql = "select id_cip_sec, dfd_p_a from odn506"
        for id, dent in getAll(sql, imp):
            exc = 0 if dent == 'A' else 1
            self.exclusions[id] += exc
    
    def get_atdom(self):
        """Agafem atdom"""
        
        self.excl = set()
        sql = "select id_cip_sec from eqa_problemes where ps=45"
        for id,  in getAll(sql, nod):
            self.excl.add(int(id))

    def get_glicada(self):
        """Agafem glicada per sobre de 8 en últim any"""
        
        self.glicada = set()
        sql = "select id_cip_sec, data_var from eqa_variables where agrupador=20 and valor>8"
        for id, dat  in getAll(sql, nod):
            if monthsBetween(dat,self.current_date) < 12:
                self.glicada.add(int(id))
     
    def get_numerador(self):
        """FC"""
        
        self.numerador = set()
        sql = "select id_cip_sec, dat from odn_variables where agrupador=309"
        for id, dat in getAll(sql, nod):
            if monthsBetween(dat,self.current_date) < 24:
                self.numerador.add(int(id))
              
    def get_indicadors(self):
        """."""
        self.upload = []
        sql= 'select id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat, sexe,seccio_censal,up_residencia, institucionalitzat,maca from assignada_tot where edat between 15 and 79'
        for id,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe,seccio_censal,up_residencia,institucionalitzat,maca in getAll(sql, nod):
            dm = 1 if id in self.dm2 else 0
            glic = 1 if id in self.glicada else 0
            den = dm + glic
            num = 1 if id in self.numerador else 0
            ex = 1 if id in self.excl else 0
            if id in self.exclusions:
                dent = self.exclusions[id]
                if dent == 0:
                    ex = 1
            if den == 2:
                den = 1
                self.upload.append([id, up, uba, upinf, ubainf,edat, ates, trasplantat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, indicador, num, ex, 0, 0])
    
    def export_data(self):
        """Taula per a carregar"""
        table = "%s_ind2" % indicador
        columns= "(id_cip_sec double null,up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double \
            ,ates double, trasplantat double, sexe varchar(7) not null default'',seccio_censal varchar(10) not null default'',up_residencia varchar(13) not null default'', institucionalitzat double, maca double,ind_codi varchar(10) not null default'',num double,excl double,ci double,clin double)"
        createTable(table, columns, db, rm=True)
        listToTable(self.upload, table, db)
        
if __name__ == '__main__':
    odn_dm2() 
