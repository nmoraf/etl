# coding: latin1

"""
.
"""

import collections as c
import os

import sisapUtils as u


TABLE = "exp_ecap_pacient_new"
DATABASE = "eqa_ind"


class Avisos(object):
    """."""

    def __init__(self):
        """."""
        self.get_cli_to_ind()
        self.get_avisos()
        self.get_llistats()
        self.upload_data()
        self.set_cataleg()
        self.set_avstb201()

    def get_cli_to_ind(self):
        """."""
        self.cli_to_ind = c.defaultdict(set)
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        file = "../../08alt/dades_noesb/avisos_indicadors.txt"
        for row in u.readCSV(file, sep="@"):
            self.cli_to_ind[row[1]].add(row[0][:7])

    def get_avisos(self):
        """."""
        self.avisos = {}
        self.codis = set()
        sql = "select id_cip_sec, cli, text4, text5 from mst_rec_avisos"
        for id, cli, text4, text5 in u.getAll(sql, "eqa_ind"):
            if not text4 or cli == "CLI1":
                status = 0
                valor = None
            elif text4[:3] == "Cap":
                status = -2
                valor = None
            else:
                status = -1
                if text4[:4] == "Data":
                    valor = text5
                else:
                    valor = "{} ({})".format(text5, text4)
            for ind in self.cli_to_ind[cli]:
                self.avisos[(id, ind)] = (status, valor)
                self.codis.add(ind)

    def get_llistats(self):
        """."""
        self.llistats = []
        sql = "select id_cip_sec, up, uba, upinf, ubainf, \
                      grup_codi, exclos, hash_d, sector \
               from exp_ecap_pacient"
        for id, up, uba, upinf, ubainf, ind, exclos, hash, sector in u.getAll(sql, "eqa_ind"):  # noqa
            if exclos == 0 and (id, ind) in self.avisos:
                status, valor = self.avisos[(id, ind)]
            else:
                status = exclos
                valor = None
            this = (id, up, uba, upinf, ubainf, ind, exclos,
                    hash, sector, status, valor)
            self.llistats.append(this)

    def upload_data(self):
        """."""
        cols = "(id_cip_sec int, up varchar(5), uba varchar(5), \
                 upinf varchar(5), ubainf varchar(5), grup_codi varchar(10), \
                 exclos int, hash_d varchar(40), sector varchar(4), \
                 status int, valor varchar(255))"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.llistats, TABLE, DATABASE)

    def set_cataleg(self):
        """."""
        table = "exp_ecap_catalegexclosos"
        u.execute("delete from {} where codi < 0".format(table), "eqa_ind")
        upload = [(-2, "Cap determinaci&oacute; en el periode", -2, 0),
                  (-1, "Darrera determinaci&oacute; alterada", -1, 1)]
        u.listToTable(upload, table, "eqa_ind")

    def set_avstb201(self):
        """."""
        table = "avstb201"
        upload = [(codi, 1) for codi in self.codis]
        u.execute("truncate table {}".format(table), "pdp")
        u.listToTable(upload, table, "pdp")


if __name__ == "__main__":
    Avisos()
