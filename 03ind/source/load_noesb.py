from sisapUtils import *
import os, os.path
path0 = os.path.realpath("./")
path1 = path0.replace("\\","/") + "/dades_noesb/" 

bd = "eqa_ind"
conn = connect((bd,'aux'));
c = conn.cursor()
for file in os.listdir(path1):
   if file.startswith("eqa_") and not file.endswith('.json'):
      fitxer= path1 + file
      taula=file[0:-4]
      symbol = "{" if "anual" in file else "@"
      c.execute("truncate table %s;" % taula)
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '%s' LINES TERMINATED BY '\r\n';" % (fitxer,taula,symbol))
path0 = os.path.realpath("../")
path1 = path0.replace("\\","/") + "/02nod/dades_noesb/" 
for file in os.listdir(path1):
   if file.startswith("eqa_"):
      fitxer= path1 + file
      taula=file[0:-4]
      c.execute("truncate table %s;" % taula)
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n' (ind_codi,taula,tipus,numero_criteri,agrupador,@data1,@data2,@valor,data_baixa,@valor1,@valor2,@valor3,@valor4,@valor5) set data_min=if(@data1 ='',24000,@data1), data_max=if(@data2 ='',0,@data2),baixa=if(@valor='',0,@valor),valor_min=if(@valor1 ='',null,if(@valor1='H',0,if(@valor1='D',1,@valor1))), valor_max=if(@valor2 ='',null,if(@valor2='H',0,if(@valor2='D',1,@valor2))), var_ult=if(@valor3='',null,@valor3),var_num=if(@valor4='',null,@valor4),sense=if(@valor5='',null,@valor5);" % (fitxer,taula))
conn.close()
