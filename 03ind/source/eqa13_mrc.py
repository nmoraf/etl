# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

printTime('Inici')

debug = False

db = 'eqa_ind'
nod = 'nodrizas'
FG = 30
QAC = 513999  # eliminar 999 si realment es vol introduir QAC
variables = 'eqa_variables'
tipusInd = 13

OutFile = tempFolder + 'adequacioMRC.txt'


assig = {}
sql = 'select id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe,seccio_censal,up_residencia,institucionalitzat,maca from assignada_tot'
for id_cip_sec, up, uba, upinf, ubainf, edat, ates, trasplantat,sexe, seccio_censal, up_residencia, institucionalitzat, maca in getAll(sql, db):
    assig[(id_cip_sec)] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'edat': edat, 'ates': ates, 'trasplantat': trasplantat, 'sexe': sexe, 'seccio_censal': seccio_censal, 'up_residencia': up_residencia, 'institucionalitzat': institucionalitzat, 'maca': maca}

MDRDp = {}
sql = "select id_cip_sec, agrupador, data_var, valor, usar from {0} where agrupador in ({1}, {2}) and usar < 3 {3}".format(variables, FG, QAC, ' limit 10' if debug else '')
for id, agr, data_var, valor, usar in getAll(sql, nod):
    if (agr == FG and 0 < valor < 59.99) or (agr == QAC and valor > 30):
        key = (id, agr)
        if key in MDRDp:
            data = MDRDp[key]['data']
            dies = daysBetween(data_var, data)
            if (agr == FG and abs(dies) > 90) or (agr == QAC and abs(dies) > 180):
                usar1 = MDRDp[key]['usar']
                difusar = usar - usar1
                if abs(difusar) == 1:
                    MDRDp[key]['num'] += 1
                    MDRDp[key]['data'] = data_var
                    MDRDp[key]['usar'] = usar
                else:
                    num = MDRDp[key]['num']
                    if num > 1:
                        continue
                    else:
                        MDRDp[key]['data'] = data_var
                        MDRDp[key]['usar'] = usar
        else:
            MDRDp[key] = {'data': data_var, 'num': 1, 'usar': usar, 'numerador': 0}

MDRD = {}
for (id, agr), dades in MDRDp.items():
    if agr == FG:
        MDRD[id] = {'num': max([dades['num'], MDRDp[(id, QAC)]['num'] if (id, QAC) in MDRDp else 0]), 'numerador': 0}

            
indicadors = []
sql = 'select ind_codi from eqa_ind where tipus = {}'.format(tipusInd)
for ind_codi, in getAll(sql, db):
    indicadors.append(ind_codi)

for indica in indicadors:
    sql = "select taula, agrupador, data_min, data_max from eqa_relacio where ind_codi in ('{}')".format(indica)
    for taula, agr, dmin, dmax in getAll(sql, nod):
        sql = "select date_add(date_add(data_ext,interval - {0} month),interval + 1 day),date_add(data_ext,interval - {1} month) from dextraccio".format(dmin, dmax)
        for fat, ar in getAll(sql, nod):
            fatemps = fat
            ara = ar
        if taula == 'problemes':
            sql = "select id_cip_sec from eqa_problemes where ps={0} and dde between '{1}' and '{2}' {3}".format(agr, fatemps, ara, ' limit 10' if debug else '')
            for id, in getAll(sql, nod):
                if (id) in MDRD:
                    MDRD[(id)]['numerador'] = 1

    tableMy = indica + '_ind2'

    with openCSV(OutFile) as c:
        for (id), d in MDRD.items():
            n = d['num']
            numerador = d['numerador']
            if n > 1:
                try:
                    up = assig[(id)]['up']
                except KeyError:
                    continue
                c.writerow([id, assig[id]['up'], assig[id]['uba'], assig[id]['upinf'], assig[id]['ubainf'], assig[id]['edat'], assig[id]['ates'], assig[id]['trasplantat'],assig[id]['sexe'], assig[id]['seccio_censal'], assig[id]['up_residencia'], assig[id]['institucionalitzat'], assig[id]['maca'], indica, numerador, 0, 0, 0])

    execute("drop table if exists {}".format(tableMy), db)
    execute("create table {} (id_cip_sec int, up varchar(5) not null default'', uba varchar(7) not null default'', upinf varchar(5) not null default'', ubainf varchar(7) not null default'',\
        edat double, ates double, trasplantat double, sexe varchar(7) not null default'', seccio_censal varchar(10) not null default'', up_residencia varchar(13) not null default'', institucionalitzat double, \
        maca double, ind_codi varchar(10) not null default'', num double, excl double, ci double, clin double, index(id_cip_sec))".format(tableMy), db)
    loadData(OutFile, tableMy, db)

printTime('Fi')
