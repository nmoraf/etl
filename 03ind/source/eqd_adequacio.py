from sisapUtils import *
from collections import defaultdict
import csv, os

#el tipus indicador de eqd es 11


debug = False

nod= 'nodrizas'
eqa= 'eqa_ind'
OutFile= tempFolder + 'eqd_adequacio.txt'
path0 = os.path.realpath("../")
path1 = path0.replace("\\","/") + "/02nod/dades_noesb/eqa_relacio.txt" 

      
indicadors= {}
sql= "select ind_codi from eqa_ind where tipus=11"
for indc, in getAll(sql,eqa):
    indicadors[indc]= True
    
def isIndicadors(indc):
    return indc in indicadors

def is_number(x):
    try:
        float(x)
        return True
    except ValueError:
        return False

def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe, seccio_censal,up_residencia,institucionalitzat,maca from assignada_tot_with_jail'
    for id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe,seccio_censal,up_residencia,institucionalitzat,maca in getAll(sql,nod):
         assig[int(id_cip_sec)]= {'up':up,'uba':uba,'upinf':upinf,'ubainf':ubainf,'edat':int(edat),'ates':int(ates),'trasplantat':int(trasplantat),'sexe':sexe,'sec_cens':seccio_censal,'up_res':up_residencia,'insti':int(institucionalitzat),'maca':int(maca)}
    return assig      

hta = {}
sql = 'select id_cip_sec,dde from eqa_problemes where incident=1 and ps=55'
for id,dde in getAll(sql,nod):
    hta[id] = {'dde':dde}

dataTA =  {}
sql = 'select id_cip_sec, agrupador, data_var,valor from eqa_variables where agrupador in (16,17) order by data_var desc{}'.format(' limit 20' if debug else '')
for id,agr,data,valor in getAll(sql,nod):
    try:
        dde = hta[id]['dde']
    except KeyError:
        continue
    b=monthsBetween(dde,data)
    fatemps=-12
    entemps=0
    if fatemps <=b<=entemps and data<=dde:
        if (id,agr) in dataTA:
            n = dataTA[(id,agr)]['n']
            n2 = n + 1 
            if n2 <= 3:
                dataTA[(id,agr)]['vals'] += valor
                dataTA[(id,agr)]['n'] += 1
        else:
            dataTA[(id,agr)] = {'vals':valor,'n':1}
MitjanaTA = {}
for (id,agr),valors in dataTA.items():
    val = valors['vals']
    n = valors['n']
    mitjana = float(val/3)
    MitjanaTA[(id,agr)] = {'mitjana':mitjana}

OFile =  tempFolder + 'proves.txt'
with open(OFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|') 
    for (id,agr),mit in MitjanaTA.items():
        mitjana = mit['mitjana']
        w.writerow([id,agr,mitjana])
table="hta_mitjanes"
sql= 'drop table if exists {}'.format(table)
execute(sql,eqa)      
sql= "create table {} (id_cip_sec double null,agr double,mitjana double)".format(table)
execute(sql,eqa)
loadData(OFile,table,eqa)


 
sql='drop table if exists eqd_num_ad'
execute(sql,eqa)

sql="create table eqd_num_ad(indicador varchar(10) not null default'',n int,agr int,taula varchar(20) not null default'',dmin double, dmax double,vmin double,vmax double,vnum double)"
execute(sql,eqa)

sql='drop table if exists eqd_excl_ad'
execute(sql,eqa)

sql="create table eqd_excl_ad(indicador varchar(10) not null default'',tipus varchar(10) not null default'',n int,agr int,taula varchar(20) not null default'')"
execute(sql,eqa)

den={}
with open(path1, 'rb') as file:
    p=csv.reader(file, delimiter='@', quotechar='|')
    for ind in p:
        i,taula,tip,n,agr,dmin,dmax,baixa,dbaixa,vmin,vmax,vult,vnum,sense = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8],ind[9],ind[10],ind[11],ind[12],ind[13]
        if isIndicadors(i):
            if tip=='den':
                if taula=='problemes':
                    den[int(agr)]= {'indicador':i}
                else:
                    print 'error'
            elif tip=='num':
                sql="insert into eqd_num_ad values('%s',%s,%s,'%s','%s','%s','%s','%s','%s')"
                execute(sql %(i,n,agr,taula,dmin,dmax,vmin,vmax,vnum),eqa)
            else:
                sql="insert into eqd_excl_ad values('%s','%s',%s,'%s','%s')"
                execute(sql %(i,tip,n,agr,taula),eqa)
printTime('inici calcul')            
for ind in indicadors:
    denominadors={}
    sql="select distinct id_cip_sec,ps,dde from eqa_problemes where incident=1 {}".format(' limit 1000' if debug else '')
    for cip,ps,dde in getAll(sql,nod):
        try:
            if den[int(ps)]['indicador'] == ind:
                if (int(cip)) in denominadors:
                    ok=1
                else:
                    denominadors[(int(cip))]= {'data':dde,'den':1,'num':0,'excl':0,'ci':0,'clin':0}
        except:
            continue
    sql="select * from eqd_num_ad where indicador='%s'"
    for id,n,agr,taula,dmin,dmax,vmin,vmax,vnum in getAll(sql % (ind),eqa):
        t="eqa_%s" % taula
        sql="select max(n) from eqd_num_ad where indicador='%s'"
        nmax,= getOne(sql % (ind),eqa)
        nmax=int(nmax)
        n=int(n)
        if taula=="variables":
            nvariables={}
            sql="select * from %s where agrupador='%s'"
            for idcip,var,data_var,valor,usar in getAll(sql % (t,agr),nod):
                idcip=int(idcip)
                if (idcip) in denominadors:
                    dde=denominadors[(idcip)]['data']
                    b=monthsBetween(dde,data_var)
                    fatemps=-12
                    entemps=6
                    if int(dmax)==12:
                        entemps=12
                    if  ind=='EQD024001':
                        if agr == 16 or agr == 17:
                            fatemps=1
                            entemps=90
                            b=daysBetween(dde,data_var)
                        else:
                            fatemps=-12
                            entemps=3
                    if fatemps<=b<=entemps:
                        if int(vmin)==0 and int(vmax)==0:
                            if denominadors[(idcip)]['num'] <> n:
                                denominadors[(idcip)]['num']+=1    
                        else:
                            if vmin<=valor<=vmax:
                                if (idcip) in nvariables:
                                    nvariables[idcip]+=1
                                else:   
                                    nvariables[idcip]=1              
            try:
                if int(vmin)==0 and int(vmax)==0:
                    ok=1
                else:
                    for key,y in nvariables.items():
                        if y >= int(vnum):
                            if denominadors[(key)]['num'] <> n:
                                denominadors[(key)]['num']+=1  
            except:
                ok=1
            nvariables.clear()
        elif taula=="proves":
            sql="select * from %s where agrupador=%s"
            for idcip,var,data_var in getAll(sql % (t,agr),nod):
                idcip=int(idcip)
                if (idcip) in denominadors:
                    dde=denominadors[(idcip)]['data']
                    fatemps=-12
                    entemps=6
                    if int(dmax)==12:
                        entemps=12
                    b=monthsBetween(dde,data_var)
                    if fatemps<=b<=entemps:
                        if  denominadors[(idcip)]['num']<> n:
                            denominadors[(idcip)]['num']+=1
        elif taula=="problemes":
            sql="select id_cip_sec,ps,dde from eqa_problemes where ps=%s"
            for idcip,ps,dde in getAll(sql % (agr),nod):
                idcip=int(idcip)
                if (idcip) in denominadors:
                    if  denominadors[(idcip)]['num']<> n:
                        denominadors[(idcip)]['num']+=1
    sql="select * from eqd_excl_ad where indicador='%s'"
    for id,tipus,n,agr, taula in getAll(sql % (ind),eqa):
        sql="select max(n) from eqd_excl_ad where indicador='%s'"
        nmaxe,= getOne(sql % (ind),eqa)
        nmaxe=int(nmaxe)
        if taula == 'problemes':
            sql="select id_cip_sec,dde from eqa_problemes where ps='%s'"
            for idcip,dde in getAll(sql % (agr),nod):
                idcip=int(idcip)
                if (idcip) in denominadors:
                    if  denominadors[(idcip)]['excl']<> n:
                        denominadors[(idcip)]['excl']+=1 
        elif taula == 'assignacio':
            sql="select id_cip_sec, institucionalitzat from nodrizas.assignada_tot_with_jail where institucionalitzat=1" 
            for idcip, institut in getAll(sql, nod):
                idcip=int(idcip)
                if (idcip) in denominadors:
                    if  denominadors[(idcip)]['excl']<> n:
                        denominadors[(idcip)]['excl']+=1     
    if ind=='EQD024001':
        for (id,agr),mit in MitjanaTA.items():
            mitjana = mit['mitjana']
            if (agr == 16 and mitjana >=140) or (agr==17 and mitjana >=90):
                try:
                    denominadors[(id)]['num'] += 1
                except KeyError:
                    ok=1
    ass = getPacients()
    with open(OutFile,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        for (id),d in denominadors.items():   
            if denominadors[id]['num']>=nmax:
                denominadors[id]['num']=1
            else:
                denominadors[id]['num']=0
            try:
                if denominadors[id]['excl']>=nmaxe:
                    denominadors[id]['excl']=1
                else:
                    denominadors[id]['excl']=0
            except:
                denominadors[id]['excl']=0
            try:
                 w.writerow([id,ass[id]['up'],ass[id]['uba'],ass[id]['upinf'],ass[id]['ubainf'],ass[id]['edat'],ass[id]['ates'],ass[id]['trasplantat'],ass[id]['sexe'],ass[id]['sec_cens'],ass[id]['up_res'],ass[id]['insti'],ass[id]['maca'],ind,d['num'],d['excl'],d['ci'],d['clin']])
            except KeyError:
                continue
    table="%s_ind2" % ind
    sql= 'drop table if exists {}'.format(table)
    execute(sql,eqa)
    sql= "create table {} (id_cip_sec double null,up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double\
            ,ates double, trasplantat double, sexe varchar(7) not null default'',seccio_censal varchar(10) not null default'',up_residencia varchar(13) not null default'',institucionalitzat double,maca double,ind_codi varchar(10) not null default'',num double,excl double,ci double,clin double)".format(table)
    execute(sql,eqa)
    loadData(OutFile,table,eqa)
    denominadors.clear()
den.clear()  
printTime('fi calcul')            