from sisapUtils import *
import csv,os
from time import strftime

printTime('Inici') 

db="eqa_ind"
nod="nodrizas"



OutFile= tempFolder + 'eqa_odn.txt'

indicadors= {}
sql= "select ind_codi from eqa_ind where tipus=12"
for indc, in getAll(sql,db):
    indicadors[indc]= True
    
def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat, sexe,seccio_censal,up_residencia, institucionalitzat,maca from assignada_tot'
    for id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe,seccio_censal,up_residencia,institucionalitzat,maca in getAll(sql,db):
         assig[int(id_cip_sec)]= {'up':up,'uba':uba,'upinf':upinf,'ubainf':ubainf,'edat':int(edat),'ates':int(ates),'trasplantat':int(trasplantat),'sexe':sexe,'sec_cens':seccio_censal,'up_res':up_residencia, 'insti':int(institucionalitzat),'maca':int(maca)}
    return assig 

    
sql="select data_ext from dextraccio"
for d in getOne(sql,nod):
    dext=d

printTime('Inici calcul') 
    
for i in indicadors:
    den={}
    sql="select taula,tipus,numero_criteri,agrupador,data_min,data_max from eqa_relacio where ind_codi='{}'".format(i)
    for taula,tipus,n,agr,dmin,dmax in getAll(sql,db):
        if tipus=="den":
            t="eqa_%s" % taula
            if taula=="problemes":
                sql="select id_cip_sec from {0} where ps='{1}'".format(t,agr)
                for id_cip_sec, in getAll(sql,nod):
                    id_cip_sec=int(id_cip_sec)
                    if id_cip_sec in den:
                        continue
                    else:
                        den[id_cip_sec]={'num':0,'excl':0,'ci':0,'clin':0}
        elif tipus=="num":
            if taula.startswith("odn"):
                t="odn_variables"
                sql="select id_cip_sec from {0} where agrupador='{1}' and (dat between date_add('{2}',interval - {3} month) and date_add('{2}',interval + {4} month))".format(t,agr,dext,dmin,dmax)
                for id, in getAll(sql,nod):
                    id=int(id)
                    if id in den:
                        den[id]['num']=1
        elif tipus=="excl":
            t="eqa_%s" % taula
            if taula=="problemes":
                sql="select id_cip_sec from {0} where ps='{1}'".format(t,agr)
                for id_cip_sec, in getAll(sql,nod):
                    id=int(id_cip_sec)
                    if id in den:
                        den[id]['excl']=1
    ass = getPacients()
    with open(OutFile,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        for (id),d in den.items():   
            try:
                 w.writerow([id,ass[id]['up'],ass[id]['uba'],ass[id]['upinf'],ass[id]['ubainf'],ass[id]['edat'],ass[id]['ates'],ass[id]['trasplantat'],ass[id]['sexe'],ass[id]['sec_cens'],ass[id]['up_res'],ass[id]['insti'],ass[id]['maca'],i,d['num'],d['excl'],d['ci'],d['clin']])
            except KeyError:
                continue
    table="%s_ind2" % i
    sql= 'drop table if exists {}'.format(table)
    execute(sql,db)
    sql= "create table {} (id_cip_sec double null,up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double\
            ,ates double, trasplantat double, sexe varchar(7) not null default'',seccio_censal varchar(10) not null default'',up_residencia varchar(13) not null default'', institucionalitzat double,maca double,ind_codi varchar(10) not null default'',num double,excl double,ci double,clin double)".format(table)
    execute(sql,db)
    loadData(OutFile,table,db)
    den.clear()
printTime('fi calcul')                
     