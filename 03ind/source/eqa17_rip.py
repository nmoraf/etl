from sisapUtils import createTable, getAll, listToTable


nod = 'nodrizas'
eqa = 'eqa_ind'
ind = 'EQA3007A'
tb = '{}_ind2'.format(ind)


def get_problemes(i):
    rip = bool(i)
    sql = 'select id_cip_sec from {}_problemes where ps in {}'.format('rip' if rip else 'eqa', '(1, 7, 11, 211, 212, 213)' if rip else '(1, 7, 11)')
    ps = set([id for id, in getAll(sql, nod)])
    return ps


def get_poblacio(i):
    sql = ('select id_cip_sec, up, uba, upinf, ubainf, ates, institucionalitzat from assignada_tot where edat between 15 and 200',
           "select id_cip_sec, up, uba, '', '', 1, 0 from rip_assignada where edat between 15 and 200")
    poblacio = {row[0]: row[1:] for row in getAll(sql[i], nod) if row[0] in ps}
    return poblacio


def play_with_it(i):
    upload = [(id, ind) + info + (1, i, 0) for id, info in poblacio.items()]
    listToTable(upload, tb, eqa)


if __name__ == '__main__':
    createTable(tb, '(id_cip_sec int, ind_codi varchar(10), up varchar(5), uba varchar(7), upinf varchar(5), ubainf varchar(5), ates int, institucionalitzat int, den int, num int, excl int)', eqa, rm=True)
    for i in range(2):
        ps = get_problemes(i)
        poblacio = get_poblacio(i)
        play_with_it(i)
