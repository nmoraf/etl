# coding: iso-8859-1
from sisapUtils import *
import csv
import os
import sys
from time import strftime
from collections import defaultdict, Counter

debug = False

db = "eqa_ind"
nod = "nodrizas"
eqa = "eqa_ind"
imp = "import"
redics = 'redics'

path0 = os.path.realpath("../")
path = path0.replace("\\", "/") + "/08alt/dades_noesb/avisos_indicadors.txt"
OutFile = "rec_avisos"
TaulaMy = "mst_rec_avisos"

origen = 'mst_indicadors_pacient'

printTime('Inici Conversions')

sql = "select data_ext from dextraccio"
dext, = getOne(sql, nod)

sql = "select grip from dextraccio"
for gripp, in getAll(sql, nod):
    grip = True if gripp == 1 else False

CliConverter = {}
IndicadorsAvisos = {}
with open(path, 'rb') as file:
    p = csv.reader(file, delimiter='@')
    for i in p:
        ind_codi, cli, text1, text2, text3, text6, text8, grup = i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7]
        if not grip:
            if cli == 'CLI9':
                continue
            else:
                IndicadorsAvisos[ind_codi] = True
                CliConverter[ind_codi] = {'cli': cli, 'text1': text1, 'text2': text2, 'text3': text3, 'text6': text6, 'text8': text8, 'grup': grup}
        else:
                IndicadorsAvisos[ind_codi] = True
                CliConverter[ind_codi] = {'cli': cli, 'text1': text1, 'text2': text2, 'text3': text3, 'text6': text6, 'text8': text8, 'grup': grup}

ValorsVariables = {}
Valors16 = {}
Valors17 = {}
for indicador in IndicadorsAvisos:
    if CliConverter[indicador]['grup'] == 'Variable':
        sql = "select agrupador,data_min,data_max,valor_min,valor_max from eqa_relacio where tipus='num' and ind_codi='{0}'".format(indicador)
        for agr, dmin, dmax, vmin, vmax in getAll(sql, nod):
            if CliConverter[indicador]['cli'] == 'CLI1':
                sql = "select id_cip_sec,data_var,valor from eqa_variables,dextraccio where usar=1 and agrupador={0}{1}".format(agr, ' limit 10' if debug else '')
                for id, data, valor in getAll(sql, nod):
                    ValorsVariables[(id, indicador)] = {'data': data, 'valor': valor}
            else:
                sql = "select id_cip_sec,data_var,valor from eqa_variables,dextraccio where usar=1 and agrupador={0} and (data_var between date_add(data_ext,interval - {1} month) and date_add(data_ext,interval - {2} month)){3}".format(agr, dmin, dmax, ' limit 10' if debug else '')
                for id, data, valor in getAll(sql, nod):
                    if agr == 197:
                        continue
                    if agr in (16, 674):
                        data = 'Data c�lcul EQA ' + str(data)
                        Valors16[(id, indicador)] = {'data': data, 'valor': valor}
                    elif agr in (17, 675):
                        Valors17[(id, indicador)] = {'data': data, 'valor': valor}
                    else:
                        ValorsVariables[(id, indicador)] = {'data': data, 'valor': valor}

hashConverter = {}
sql = 'select id_cip_sec,codi_sector,hash_d from u11withjail'
for id, sector, hash in getAll(sql, imp):
    hashConverter[id] = {'sector': sector, 'hash': hash}

printTime('Inici Avisos')

preAvisos = []
sql = "select id_cip_sec,ind_codi,up from {0} where llistat = 1 and excl = 0 and ci = 0 and clin =0 and excl_edat = 0{1}".format(origen, ' limit 1000' if debug else '')
for id, ind_codi, up in getAll(sql, eqa):
    try:
        cli = CliConverter[ind_codi]['cli']
        text1 = CliConverter[ind_codi]['text1']
        text2 = CliConverter[ind_codi]['text2']
        text3 = CliConverter[ind_codi]['text3']
        text6 = CliConverter[ind_codi]['text6']
        text8 = CliConverter[ind_codi]['text8']
        grup = CliConverter[ind_codi]['grup']
    except KeyError:
        continue
    if grup == 'Variable':
        try:
            text4 = ValorsVariables[(id, ind_codi)]['data']
            pre = ValorsVariables[(id, ind_codi)]['valor']
            text5 = round(pre, 1) if cli == "CLI2" else int(pre)
        except KeyError:
            try:
                text4 = Valors16[(id, ind_codi)]['data']
                text516 = int(Valors16[(id, ind_codi)]['valor'])
                text517 = int(Valors17[(id, ind_codi)]['valor'])
                text5 = str(text516) + "/" + str(text517)
                text5 =  text5 + " (mitjana 3 �ltimes)"
            except KeyError:
                text5 = None
                if cli == 'CLI1':
                    text4 = "Cap en els �ltims 2 anys"
                else:
                    text4 = "Cap en l'�ltim any"
    else:
        text4 = None
        text5 = None
    preAvisos.append([hashConverter[id]['sector'], id, hashConverter[id]['hash'], up, cli, text1, text2, text3, text4, text5, text6, text8, grup])

with openCSV(OutFile) as c:
    for sector, id, hash, up, cli, text1, text2, text3, text4, text5, text6, text8, grup in preAvisos:
        c.writerow([sector, id, hash, up, cli, text1, text2, text3, text4, text5, text6, text8, grup])

execute('drop table if exists {}'.format(TaulaMy), db)
execute('create table {} (sector varchar(4),id_cip_sec int,hash varchar(40),up varchar(5),cli varchar(5),text1 varchar(200), text2 int,text3 varchar(200)\
    ,text4 varchar(100), text5 varchar(50),text6 varchar(20), text8 varchar(20), grup varchar(20))'.format(TaulaMy), db)
loadData(OutFile, TaulaMy, db)

printTime('Fi Avisos')
