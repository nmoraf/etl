# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import sys
import datetime
from time import strftime

printTime('Inici')
# db
eqa = 'eqa_ind'
nod = 'nodrizas'
imp = 'import'

# tables
relacio = 'eqa_relacio'
problemes = 'urg_problemes'
probGeneral = 'eqa_problemes'
assignada = 'urg_poblacio'
tractaments = 'urg_tractaments'
dext = 'dextraccio'
OutFile = tempFolder + 'eqa_urgencies.txt'
TableMy = 'mst_eqa_urgencies_pacient'
TableKhx = 'exp_khalix_eqa_urgencies_up'

# querys
sqlRelacio = "select agrupador, data_min, data_max, valor_min, valor_max, sense from {0} where tipus = '{1}' and ind_codi='{2}' and taula='{3}'"
sqlproblemes = "select id_cip_sec, ps, dde, up from {0} where ps in {1}"
sqlTto = 'select id_cip_sec, farmac, pres_orig, data_fi, up from {0} where farmac in {1}'
sqlPsGen = 'select id_cip_sec, ps from {0} where ps in {1}'

# taules previes

sql = 'select data_ext from {}'.format(dext)
for d in getOne(sql, nod):
    dara = d

assig, tempsAg, maxCrit, rangEdat = {}, {}, {}, {}
sql = "select id_cip_sec, usua_sexe, usua_data_naixement from assignada"
for id, sexe, naix in getAll(sql, imp):
    edat = yearsBetween(naix, dara)
    assig[id] = {'sexe': sexe, 'edat': edat}
sql = 'select ind_codi, unitat from eqa_temps_ag'
for ind, unitat in getAll(sql, eqa):
    tempsAg[(ind)] = unitat
sql = 'select ind_codi, minim, maxim from eqa_edats'
for ind, emin, emax in getAll(sql, eqa):
    rangEdat[(ind)] = {'min': emin, 'max': emax}
sql = "select b.ind_codi, b.tipus, max(numero_criteri) from eqa_ind a inner join nodrizas.eqa_relacio b on a.ind_codi=b.ind_codi where baixa_=0 and a.tipus=3 and grup2_codi = 'EQAG06' group by b.ind_codi, b.tipus"
for ind, tipus, nmax in getAll(sql, eqa):
    maxCrit[(ind, tipus)] = nmax


def getUnitats(ind):
    u = tempsAg[(ind)]
    if u == 'day':
        unitat = 1
    elif u == 'week':
        unitat = 7
    return unitat

# agrupadors i taules

agrs = []
relAgrs = defaultdict(list)
inass = {}
sql = "select b.ind_codi, taula, agrupador, valor_min from eqa_ind a inner join nodrizas.eqa_relacio b on a.ind_codi=b.ind_codi where baixa_=0 and a.tipus=3 and grup2_codi = 'EQAG06' and b.tipus='den'"
for indcodi, taula, agr, valor in getAll(sql, eqa):
    if taula == 'problemes':
        agrs.append(int(agr))
        relAgrs[(agr)].append(indcodi)
    elif taula == 'assignacio':
        inass[indcodi] = valor
in_crit = tuple(agrs)
agrsF = []
relAgrsF = defaultdict(list)
Nothing, dates, res = {}, {}, {}
sql = "select b.ind_codi, agrupador, data_min, data_max, sense from eqa_ind a inner join nodrizas.eqa_relacio b on a.ind_codi=b.ind_codi where baixa_=0 and a.tipus=3 and grup2_codi = 'EQAG06' and b.tipus='num'"
for indcodi, agr, dmin, dmax, sense in getAll(sql, eqa):
    agrsF.append(int(agr))
    relAgrsF[(agr)].append(indcodi)
    Nothing[(agr, indcodi)] = sense
    if sense == 1:
        res[(indcodi)] = True
    dates[(indcodi)] = {'dmin': dmin, 'dmax': dmax}
in_critF = tuple(agrsF)
agrsCI = []
relAgrsCI = defaultdict(list)
sql = "select b.ind_codi, agrupador, valor_min from eqa_ind a inner join nodrizas.eqa_relacio b on a.ind_codi=b.ind_codi where baixa_=0 and a.tipus=3 and grup2_codi = 'EQAG06' and b.tipus='ci' and taula='problemes'"
for indcodi, agr, valor in getAll(sql, eqa):
    agrsCI.append(int(agr))
    relAgrsCI[(agr)].append(indcodi)
agrsCI.append(0)
in_critCI = tuple(agrsCI)

# denominadors
indAgudes = {}
for id, ps, dde, up in getAll(sqlproblemes.format(problemes, in_crit), nod):
    b = monthsBetween(dde, dara)
    if 0 <= b <= 11:
        if (ps) in relAgrs:
            for indcodi in relAgrs[(ps)]:
                indAgudes[(id, up, indcodi, dde)] = {'num': 0, 'den': 1, 'ci': 0, 'sense': 0, 'edat': None, 'sexe': None, 'excl_edat': 1}

# numeradors
numAgudes = {}
for id, farmac, inici, fi, up in getAll(sqlTto.format(tractaments, in_critF), nod):
    if (farmac) in relAgrsF:
        for indcodi in relAgrsF[(farmac)]:
            sense = Nothing[(farmac, indcodi)]
            numAgudes[(id, up, indcodi, inici)] = sense

# Contraindicacions
Contraindicacions = {}
for id, ps in getAll(sqlPsGen.format(probGeneral, in_critCI), nod):
    if (ps) in relAgrsCI:
        for indcodi in relAgrsCI[(ps)]:
            Contraindicacions[(id, indcodi)] = 1

# indicadors
for (id, up, indicador, dde), d in indAgudes.items():
    try:
        sexe = assig[id]['sexe']
        edat = assig[id]['edat']
    except KeyError:
        continue
    indAgudes[(id, up, indicador, dde)]['edat'] = edat
    indAgudes[(id, up, indicador, dde)]['sexe'] = sexe
    edatmin = rangEdat[(indicador)]['min']
    edatmax = rangEdat[(indicador)]['max']
    if edatmin <= edat <= edatmax:
        indAgudes[(id, up, indicador, dde)]['excl_edat'] = 0
    try:
        valor = inass[indicador]
    except KeyError:
        valor = 9999
    if valor != 9999:
        if valor == 0:
            valor = 'H'
        else:
            valor = 'D'
        if valor == sexe:
            indAgudes[(id, up, indicador, dde)]['den'] += 1
    unitat = getUnitats(indicador)
    dmin = (dates[(indicador)]['dmin']) * unitat
    dmax = (dates[(indicador)]['dmax']) * unitat
    iperiode = dde - datetime.timedelta(days=dmin)
    fperiode = dde + datetime.timedelta(days=dmax)
    for single_date in dateRange(iperiode, fperiode):
        try:
            sense = numAgudes[(id, up, indicador, single_date)]
        except KeyError:
            continue
        if sense == 1:
            indAgudes[(id, up, indicador, dde)]['sense'] = 1
        else:
            indAgudes[(id, up, indicador, dde)]['num'] = 1
    try:
        excl = Contraindicacions[(id, indicador)]
    except KeyError:
        excl = 0
    indAgudes[(id, up, indicador, dde)]['ci'] = excl

with openCSV(OutFile) as c:
    for (id, up, indicador, dde), d in indAgudes.items():
        if indicador in res:
            if d['sense'] == 0:
                num = 1
            else:
                num = d['num']
        else:
            num = d['num']
        den = d['den']
        maxden = maxCrit[(indicador, 'den')]
        maxnum = maxCrit[(indicador, 'num')]
        if den >= maxden:
            den = 1
        else:
            den = 0
        if num >= maxnum:
            num = 1
        else:
            num = 0
        if den == 1:
            c.writerow([id, up, indicador, d['edat'], d['sexe'], num, den, d['ci'], d['excl_edat']])

execute("drop table if exists {}".format(TableMy), eqa)
execute("create table {} (id_cip_sec int, up varchar(5) not null default'', indicador varchar(20) not null default'', edat int, sexe varchar(1) not null default'', num double, den double, ci double, excl_edat double)".format(TableMy), eqa)
loadData(OutFile, TableMy, eqa)

eqaUrg = Counter()
sql = 'select up, indicador, edat, sexe, num, den, ci, excl_edat from {}'.format(TableMy)
for up, ind, edat, sexe, num, den, ci, excl in getAll(sql, eqa):
    if den == 1 and ci == 0 and excl == 0:
        eqaUrg[up, ageConverter(edat, 5), sexConverter(sexe), 'NOINSAT', ind, 'NUM'] += num
        eqaUrg[up, ageConverter(edat, 5), sexConverter(sexe), 'NOINSAT', ind, 'DEN'] += den

with openCSV(OutFile) as c:
    for (up, edat, sexe, comb, indicador, conc), d in eqaUrg.items():
        c.writerow([up, edat, sexe, comb, indicador, conc, d])
execute("drop table if exists {}".format(TableKhx), eqa)
execute("create table {} (up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(20), conc varchar(10), valor double)".format(TableKhx), eqa)
loadData(OutFile, TableKhx, eqa)

printTime()
