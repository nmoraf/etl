use eqa_ind
;

set @fa2anys = (select date_add(date_add(data_ext, interval -2 year), interval +1 day) from nodrizas.dextraccio);
set @fa3mesos = (select date_add(date_add(data_ext, interval -3 month), interval +1 day) from nodrizas.dextraccio);
set @avui = (select data_ext from nodrizas.dextraccio);

#REDUCCI� ALCOHOL
drop table if exists eqa_oh;
create table eqa_oh (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
fr double,
alset_1a double,
cp201_1a double,
vp201_1a double,
alset_ara double,
cp201_ara double,
vp201_ara double,
num double,
den double,
excl double,
ci double,
clin double
)
select
	a.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
   ,0 as fr
   ,null as alset_1a
   ,null as cp201_1a
   ,null as vp201_1a
   ,null as alset_ara
   ,null as cp201_ara
   ,null as vp201_ara
   ,0 as num
   ,0 as den
   ,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a
   ,eqa_ind c
where
	 c.tipus=10
;

#Haur�em de parametritzar, per� hi ha poc temps aix� que m�s endavant...
#Mirem que hagi consum de risc en els �ltims 2 anys i fins fa 3m

drop table if exists ALSET_1a;
create table ALSET_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =264 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_OH a inner join ALSET_1a b on a.id_cip_sec=b.id_cip_sec
set ALSET_1a=valor; 

drop table if exists cp201_1a;
create table cp201_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =265 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_OH a inner join cp201_1a b on a.id_cip_sec=b.id_cip_sec
set cp201_1a=valor; 

drop table if exists VP201_1a;
create table VP201_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,min(if(valor=0,9999,if(valor=7,0,valor))) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =266 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_OH a inner join VP201_1a b on a.id_cip_sec=b.id_cip_sec
set VP201_1a=valor;

drop table if exists oh_ara1;
create table oh_ara1(
id_cip_sec double,
agrupador double,
valor double,
usar double
)
select id_cip_sec,agrupador,valor,usar from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('264','265','266') and data_var between @fa2anys and @avui
;
drop table if exists oh_aramax;
create table oh_aramax(
id_cip_sec double,
agrupador double,
usar double,
primary key(id_cip_sec,agrupador,usar)
)
select id_cip_sec,agrupador,min(usar) as usar from oh_ara1 group by id_cip_sec,agrupador
;

drop table if exists oh_ara;
create table oh_ara(
id_cip_sec double,
agrupador double,
valor double,
index(id_cip_Sec,agrupador,valor)
)
select a.id_cip_sec,a.agrupador,valor from oh_ara1 a inner join oh_aramax b on a.id_cip_sec=b.id_cip_sec and a.agrupador=b.agrupador and a.usar=b.usar
;

update eqa_OH a inner join oh_ara b on a.id_cip_sec=b.id_cip_sec
set ALSET_ara=valor where agrupador=264
;

update eqa_OH a inner join oh_ara b on a.id_cip_sec=b.id_cip_sec
set CP201_ara=valor where agrupador=265
;

update eqa_OH a inner join oh_ara b on a.id_cip_sec=b.id_cip_sec
set vp201_ara=if(valor=0,9999,if(valor=7,0,valor)) where agrupador=266
;

update eqa_OH 
set den=1 where (alset_1a>28 and sexe='H') or (alset_1a>17 and sexe='D') or (cp201_1a in ('5','4','3')) or (vp201_1a in ('7','4','3','2','1','0'))
;

update eqa_OH
set num=1 where (alset_1a > alset_ara) or (cp201_1a > cp201_ara) or (vp201_1a < vp201_ara)
;

update eqa_OH
set num=1 where cp201_ara in ('1','2') or vp201_ara in ('6','5')
;

set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=10);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=10
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_oh
where
	den=1 
");


PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;



SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=10);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;

drop table if exists ALSET_1a;
drop table if exists cp201_1a;
drop table if exists VP201_1a;
drop table if exists oh_aramax;
drop table if exists oh_ara1;
drop table if exists oh_ara;
drop table if exists eqa_oh;