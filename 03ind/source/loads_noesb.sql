#Faig importaci� de cat�legs i altres taules
#EQA RELACI� EST� A NOESB DE NODRIZAS. AGAFAR-LO 

DROP TABLE IF EXISTS eqa_ind;
create table eqa_ind(
id double,
ind_codi VARCHAR(10) NOT NULL DEFAULT'',
ind_desc VARCHAR(150) NOT NULL DEFAULT'',
grup_codi VARCHAR(7) NOT NULL DEFAULT'',
grup_desc VARCHAR(150) NOT NULL DEFAULT'',
grup2_codi VARCHAR(7) NOT NULL DEFAULT'',
grup2_desc VARCHAR(150) NOT NULL DEFAULT'',
codi_antic VARCHAR(10) NOT NULL DEFAULT'',
fitxa VARCHAR(200) NOT NULL DEFAULT'',
tipus double NULL,
llistats int,
invers int,
girar int,
ordre_grup int,
ordre_grup2 int,
baixa_ int,
nets int,
primary key(ind_codi),
index(grup_codi)
); 

drop table if exists eqa_relacio;
create table eqa_relacio like nodrizas.eqa_relacio;

DROP TABLE IF EXISTS eqa_temps_ag;
create table eqa_temps_ag(
ind_codi VARCHAR(10) NOT NULL DEFAULT'',
unitat VARCHAR(150) NOT NULL DEFAULT'',
primary key(ind_codi,unitat)
); 

DROP TABLE IF EXISTS eqa_edats;
create table eqa_edats(
ind_codi VARCHAR(10) NOT NULL DEFAULT'',
minim double,
maxim double,
index(ind_codi,minim,maxim)
); 

DELIMITER |
CREATE TRIGGER eqa_edats BEFORE INSERT ON eqa_edats
  FOR EACH ROW BEGIN
    set new.maxim=if(new.maxim > 1800, (select year(data_ext) from nodrizas.dextraccio) - new.maxim, new.maxim);
  END
|
DELIMITER ;

DROP TABLE IF EXISTS eqa_conjunts;
create table eqa_conjunts(
ind_codi_o VARCHAR(10) NOT NULL DEFAULT'',
ind_codi VARCHAR(10) NOT NULL DEFAULT'',
index(ind_codi)
); 

DROP TABLE IF EXISTS eqa_prevalences_anual;
create table eqa_prevalences_anual(
indicador VARCHAR(8) NOT NULL DEFAULT'',
periode VARCHAR(7) NOT NULL DEFAULT'',
medea VARCHAR(10) NOT NULL DEFAULT'',
ct VARCHAR(7) NOT NULL DEFAULT'',
edat VARCHAR(7) NOT NULL DEFAULT'',
pob VARCHAR(7) NOT NULL DEFAULT'',
sexe VARCHAR(4) NOT NULL DEFAULT'',
prev double,
index(indicador,medea,edat,sexe,prev)
); 

drop table if exists eqa_poresper_anual;
create table eqa_poresper_anual(
indicador varchar(8) not null default ''
,z1 varchar(10) not null default ''
,z2 varchar(10) not null default ''
,z3 varchar(10) not null default ''
,z4 varchar(10) not null default ''
,grup varchar(8) not null default ''
,z5 varchar(10) not null default ''
,eap double
);

drop table if exists eqa_metes_subind_anual;
create table eqa_metes_subind_anual(
indicador varchar(8) not null default ''
,z1 varchar(10) not null default ''
,z2 varchar(10) not null default ''
,meta varchar(8) not null default ''
,z3 varchar(10) not null default ''
,z4 varchar(10) not null default ''
,z5 varchar(10) not null default ''
,valor double
,unique(indicador,meta,valor)
);

drop table if exists eqa_ponderacio_anual;
create table eqa_ponderacio_anual(
indicador varchar(10) not null default ''
,z1 varchar(10) not null default ''
,z2 varchar(10) not null default ''
,z3 varchar(10) not null default ''
,tipus varchar(10) not null default ''
,z4 varchar(10) not null default ''
,z5 varchar(10) not null default ''
,valor double
,unique(indicador,tipus,valor)
);

drop table if exists eqa_exclusions_hide;
create table eqa_exclusions_hide (
indicador varchar(8)
,excl_edat int
,excl int
,primary key (indicador,excl_edat,excl)
);


drop table if exists eqa_metesres_subind_anual;
create table eqa_metesres_subind_anual(
indicador varchar(8) not null default ''
,z1 varchar(10) not null default ''
,z2 varchar(10) not null default ''
,meta varchar(10) not null default ''
,z3 varchar(10) not null default ''
,z4 varchar(10) not null default ''
,z5 varchar(10) not null default ''
,valor double
,unique(indicador,meta,z4,valor)
);