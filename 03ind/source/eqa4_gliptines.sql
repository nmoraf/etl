#DM2!
use eqa_ind;

drop table if exists eqa_gliptines1;
create table eqa_gliptines1 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default''
)
select
	a.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
from
	assignada_tot a
inner join
	nodrizas.eqa_problemes b
on
	a.id_cip_sec=b.id_cip_sec,eqa_ind c
where
	ps=18 and c.tipus=4
;



drop table if exists eqa_gliptines;
create table eqa_gliptines (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
gliptines double,
data_g DATE NOT NULL DEFAULT 0,
metformina double,
data_m DATE NOT NULL DEFAULT 0,
sulfonil double,
data_s DATE NOT NULL DEFAULT 0,
num double,
den double,
excl double,
ci double,
clin double,
primary key(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
	,0 as gliptines
	,0 as data_g
	,0 as metformina
	,0 as data_m
	,0 as sulfonil
	,0 as data_s
	,0 as num
	,1 as den
	,0 as excl
   ,0 as ci
   ,0 as clin
from
	eqa_gliptines1
group by
	id_cip_sec
;


update eqa_gliptines a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec,nodrizas.dextraccio
set gliptines=1
,data_g=pres_orig where (pres_orig between date_add(data_ext,interval - 2 year) and data_ext) and farmac=175
;


drop table if exists metformina;
create table metformina (
id_cip_sec double null,
pres_orig DATE NOT NULL DEFAULT 0,
index(id_cip_sec,pres_orig)
)
select
	id_cip_sec
	,min(pres_orig) as pres_orig
from
	nodrizas.eqa_tractaments
where 
	farmac in ('173','206')
group by
	id_cip_sec
;

update eqa_gliptines a inner join metformina b on a.id_cip_sec=b.id_cip_sec
set metformina=1
,data_m=pres_orig
;

drop table metformina;

drop table if exists sulfonil;
create table sulfonil (
id_cip_sec double null,
pres_orig DATE NOT NULL DEFAULT 0,
index(id_cip_sec,pres_orig)
)
select
	id_cip_sec
	,min(pres_orig) as pres_orig
from
	nodrizas.eqa_tractaments
where 
	farmac in ('174','207')
group by
	id_cip_sec
;

update eqa_gliptines a inner join sulfonil b on a.id_cip_sec=b.id_cip_sec
set sulfonil=1
,data_s=pres_orig
;

drop table sulfonil
;

update eqa_gliptines
set num=1 where data_m<data_g and gliptines=1 and metformina=1
;

update eqa_gliptines
set num=1 where data_s<data_g and gliptines=1 and sulfonil=1
;

update eqa_gliptines
set num=2 where num=1
;

update eqa_gliptines
set num=1 where num=0 and gliptines=1
;

update eqa_gliptines
set num=0 where num=2
;

update eqa_gliptines a inner join nodrizas.eqa_variables b on a.id_cip_sec=b.id_cip_sec
set ci=1 where agrupador=30 and valor<30 and usar=1
;


drop table eqa_gliptines1;

set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=4);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=4
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_gliptines
where
	den=1 
");


set @sql5 = concat("
delete a.* from ",@ind,"_",@eqa,"2 a inner join eqa_gliptines b on a.id_cip_sec=b.id_cip_sec
where gliptines=0
");

PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;

PREPARE s5 FROM @sql5;
EXECUTE s5;

SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


drop table eqa_gliptines
;


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=4);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;