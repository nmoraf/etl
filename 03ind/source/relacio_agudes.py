from sisapUtils import *

bd = "eqa_ind"
base = "eqa_relacio"
base2 = "eqa_ind"
taula = "%s_agudes" % (base)
conn = connect(bd)
c = conn.cursor()
c.execute("drop table if exists %s" % taula)
c.execute("create table %s like %s" % (taula, base))
c.execute("insert into %s select a.* from %s a inner join %s b on a.ind_codi=b.ind_codi where b.tipus=3 and baixa=0" % (taula, base, base2))
c.execute("select distinct taula from %s where taula <>''" % base)
r = c.fetchall()
for t in r:
    nom = t[0]
    c.execute("select distinct tipus from %s where taula <>''" % base)
    r2 = c.fetchall()
    for t2 in r2:
        tipus = t2[0]
        taula2 = "%s_%s_%s" % (taula, nom, tipus)
        c.execute("drop table if exists %s" % taula2)
        c.execute("create table %s like %s" % (taula2, taula))
        c.execute("insert into %s select * from %s where taula='%s' and tipus='%s'" % (taula2, taula, nom, tipus))
        c.execute("alter table %s add index (agrupador,ind_codi,valor_min,valor_max,data_min,data_max,sense)" % taula2)
conn.close()
