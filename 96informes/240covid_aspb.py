# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u



class Coronavirus_ind(object):
    """."""

    def __init__(self):
        """."""
        self.get_abs()
        self.get_aspb()
        self.get_r0()
        self.export_mail()
        self.put_files()

        
    def get_abs(self):
        """agafem ABS cataleg nou Francesc"""
        self.abs_bcn = {}
        
        sql = "select up, abs, abs_c from sisap_covid_cat_territori where localitat='BARCELONA'"
        for up, abs, abs_c in u.getAll(sql, 'redics'):
            self.abs_bcn[str(abs_c)] = True
        
    
    def get_aspb(self):
        """indicadors demanats"""
        upC = c.Counter()        
        sql = "select data,abs,grup,sexe,sum(casos_confirmat),sum(poblacio) from sisap_covid_web_master group by data,abs,grup,sexe"
        for data, abs, grup, sexe, casos, poblacio in u.getAll(sql, 'redics'):
            if str(abs) in self.abs_bcn:
                upC[(data, abs,grup, sexe, 'Casos')] += casos
                upC[(data, abs,grup, sexe, 'Pob')] += poblacio

        self.upload = []
        for (data, abs,grup, sexe, tip),n in upC.items():
            if tip == 'Pob':
                num = upC[(data, abs,grup, sexe, 'Casos')] 
                self.upload.append([data, abs, grup,sexe,num, n])   
                
    def get_r0(self):
        """R0"""
        self. uplo_r0 = []
        sql = "select data, codi, round(R0_confirmat_M,2) from sisap_covid_agr_risc_ili where nivell='ABS'"
        for dat, abs, r0 in u.getAll(sql, 'redics'):
            if str(abs) in self.abs_bcn:
               self.uplo_r0.append([dat, abs, r0])
        sql = "select data, codi, round(R0_confirmat_M,2) from sisap_covid_agr_risc_ili where nivell='MUNICIPI' and codi='08019'"
        for dat, abs, r0 in u.getAll(sql, 'redics'):
            self.uplo_r0.append([dat, 'BARCELONA', r0])

    def export_mail(self):
        """Enviem fitxer"""
        
        file = u.tempFolder + "indicadors_sisap.csv"
        u.writeCSV(file, self.upload, sep=';')
        text= "Et fem arribar els indicadors actualitzats."
        #u.sendPolite('mmari@aspb.cat','Indicadors sisap',text,file)
        
        fileR0 = u.tempFolder + "R0_sisap.csv"
        u.writeCSV(fileR0, self.uplo_r0, sep=';')
        
    def put_files(self):
        "SFTP files"
        file = "indicadors_sisap.csv"
        u.sshPutFile(file, 'export','aspb_sftp')
        filer0 = "R0_sisap.csv"
        u.sshPutFile(filer0, 'export','aspb_sftp')
        text = "s'han carregat els indicadors actualitzats al vostre SFTP."
        u.sendPolite('mmari@aspb.cat', 'SFTP: Indicadors sisap', text)
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    
    Coronavirus_ind()
    
    u.printTime("Final")