from sisapUtils import *
from collections import defaultdict, Counter

nod = 'nodrizas'

centres = {}
sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc from cat_centres"
for up, br, desc, sap, ambit in getAll(sql, nod):
    centres[up] = {'eap': desc, 'sap': sap, 'amb': ambit}
  
pob = Counter()
a = 0  
sql = 'select id_cip_sec, up from assignada_tot where edat>14 and ates=1'
for id, up in getAll(sql, nod):
    try:
        eap = centres[up]['eap']
        sap = centres[up]['sap']
        ambit = centres[up]['amb']
    except KeyError:
        a += 1
    pob[(up, eap, sap, ambit)] += 1

upload = []
for (up, eap, sap, ambit), d in pob.items():
    upload.append([up, eap, sap, ambit, d])

file = tempFolder + 'atesa.csv'
writeCSV(file, upload, sep=';') 
 
print a