from sisapUtils import getSubTables, getOne, multiprocess, getAll, createTable, listToTable
from collections import Counter


tb = 'variables_pedia'
db = 'test'


def include_table(table):
    y, = getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
    return y == 2015


def get_data(param):
    table, poblacio = param
    data = Counter()
    for id, sec, var in getAll('select id_cip_sec, codi_sector, vu_cod_vs from {}'.format(table), 'import'):
        if id in poblacio:
            data[(sec, var)] += 1
    return data


if __name__ == '__main__':
    poblacio = set([id for id, in getAll('select id_cip_sec from ped_assignada', 'nodrizas')])
    mega_data = multiprocess(get_data, [(table, poblacio) for table in getSubTables('variables') if include_table(table)])
    upload = Counter()
    for data in mega_data:
        for k, v in data.items():
            upload[k] += v
    createTable(tb, '(sector varchar(4), variable varchar(8), n int)', db, rm=True)
    listToTable([k + (n,) for (k, n) in upload.items()], tb, db)
