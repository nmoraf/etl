import sisapUtils as u
from datetime import datetime as d
import pandas as pd
import pprint as pp

print(d.now())

# Bitacora
"""
Cribatge de càncer de pulmó (Pla director d'Oncologia)
LMB: 2020-03-04
card# 369 https://trello.com/c/53vXP3an

# Pomodoros:
2020-03-04 11:30 - 13:30  (4p) # Inici: (trelloLink, PetiLink, definició, meta, descTable)
2020-03-04 17:30 - 19:00  (5p) # objectivize
2020-03-10 11:40 - 13:40  (2p) # df.append()
2020-03-10 15:00 - 17:30  (5p) # traigo u11 y hasheo
2020-03-11 15:00 - 18:30  (7p) # upload (aqui estuve peleandome con typeErrors en la carga)


# Petició:
Població: tota.
Període: tall actual de la població. Totes les variables des de l’inici. 
Destinatari: PADRIS
Objectiu: exploració de les dades disponibles per planificar el cribratge poblacional del càncer de pulmó.

# Definició:
## Taules
### Població: (de nodrizas)
    Identificador (HASH o ID, no cal CIP ni NIA ara per ara)
    UP d’assingació
    ABS de residència
    Edat
    Sexe
    Data de la última vista a l’EAP

### Estat de tabac: (la taula que tenim a nodrizas.eqa_tabac sense la variable dbaixa. )
    tabac_db, tabac_tb = "nodrizas", "eqa_tabac"

### Variables de tabac
    Identificador
    Data
    Variable: (nodrizas.eqa_tabac.vu_val = [CIG-D3, QFUM?, PAQANY, PAQANY2],
                import.xml_detall.xml_tipus_orig = 'IP2702') # 11 registros por formulario, posibles tipos de tabaco, (0/1) según si está marcado o no
    Valor
        ### mapeos de xml a variables:
            [xml:: variables]
            id_cip_sec      :: id_cip_sec
            xml_tipus_orig  :: vu_cod_vs
            xml_data_alta   :: vu_dat_act
            camp_valor      :: vu_val

# datos codificados:
    poblacio: (up, abs)
    tabac: (tab)
    variables: (vu_cod_vs)
        vu_val|vu_cod_vs='IP2702': (-2:alguna, 0:ninguna, 1:10)
"""

# objeto principal:
## diccionario que define los parametros de ETL,
## y almacena los dataframes de carga en meta[table]["tb_df"]
meta = {
    "poblacio":{
        "db": "nodrizas",
        "tb": "assignada_tot",
        "colset": "id_cip_sec, up, abs, edat, sexe, last_visit"},
    "tabac":{
        "db": "nodrizas",
        "tb": "eqa_tabac",
        "colset": "id_cip_sec, tab, dalta, dbaixa, last"},
    "variables":{
        "db": "import",
        "tb": "variables",
        "colset": "id_cip_sec, vu_cod_vs, vu_dat_act, vu_val",
        "flt": "WHERE vu_cod_vs in ('IP2702', 'CIG-D3', 'QFUM?', 'PAQANY', 'PAQANY2')"},
    "xml":{
        "db": "import",
        "tb": "xml_detall",
        "colset": "id_cip_sec, xml_tipus_orig, xml_data_alta, camp_codi",
        "flt": "WHERE xml_tipus_orig = 'IP2702' and camp_valor = '1'",
        "rename": {'xml_tipus_orig': 'vu_cod_vs', 'xml_data_alta': 'vu_dat_act', 'camp_codi': 'vu_val'}},
    "u11":{
        "db": "import",
        "tb": "u11",
        "colset": "id_cip_sec, hash_d, codi_sector"}
    }

def descTable(targetTable, n=3):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"]
    print("\n {} <- {}.{} \n".format(targetTable, meta[targetTable]["db"], meta[targetTable]["tb"]))
    print("{} ->\n".format([col for col in u.getTableColumns(tb, db)]))
    print(cols)
    # flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    # sql = """select {} from {} {} limit {}""".format(cols, tb, flt, n)
    # for row in u.getAll(sql, db):
    #    print(row)

def objectify(targetTable):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"]
    flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    sql = """select {} from {} {}""".format(cols, tb, flt)
    print(sql)
    tb_df = pd.DataFrame.from_records(data = [row for row in u.getAll(sql, db)], columns = cols.split(', '))
    meta[targetTable]["tb_df"] = tb_df
    if "rename" in meta[targetTable]:
        meta[targetTable]["tb_df"].rename(columns = meta[targetTable]["rename"], inplace= True)
    print(tb_df.info())
    print("{} size: {}".format(targetTable, meta[targetTable]['tb_df'].shape))

def apendify(targetTable, sourceTable, ignore_index = True, sort= False):
    meta[targetTable]["tb_df"] = meta[targetTable]["tb_df"].append(meta[sourceTable]["tb_df"], ignore_index = ignore_index, sort= sort)
    
def hashify(targetTable, hashTable, index = 'id_cip_sec'):
    meta[targetTable]["tb_df"].set_index('id_cip_sec', inplace = True)
    if targetTable != hashTable:
        meta[targetTable]["tb_df"]["hash"] = meta[hashTable]["tb_df"]["hash_d"]
        meta[targetTable]["tb_df"] = meta[targetTable]["tb_df"].reindex(columns =(['hash'] + list([col for col in meta[targetTable]["tb_df"].columns if col != 'hash'])))

#[QA]: """Describe TARGET <- SOURCE"""
"""
pp.pprint(meta)
"""

# CONTROL DEL PROCESO
## para describir tablas y almacenar dataFrames en meta[table]["tb_df"]
for table in meta:
    inici = d.now()
    descTable(table, 2)
    objectify(table)
    final = d.now()
    print("{}: {}".format(final - inici, table))

## vuelco xml a variables, reindexo por el autonumerico
apendify('variables','xml')

###[QA]: comprobar volcado (debe aparecer valores -2 y 0)
"""
meta["variables"]['tb_df'][meta['variables']['tb_df']['vu_cod_vs'] == 'IP2702'].groupby(['vu_val']).count()
"""


###[QA]: backup pre_hash: en otro objeto por si lo destruyo en el proceso principal
"""
variables_df = meta["variables"]['tb_df'][:].copy()
xml_df = meta["xml"]['tb_df'][:].copy()
u11_df = meta["u11"]['tb_df'][:].copy()
poblacio_df = meta["poblacio"]['tb_df'][:].copy()
tabac_df = meta["tabac"]['tb_df'][:].copy()
"""

## indexo por id_cip_sec y paso a hash
hashtable = "u11"
for table in ["u11", "poblacio", "tabac", "variables"]:
    hashify(table, hashtable, index = 'id_cip_sec')
    table
    meta[table]["tb_df"].head()

## arreglo tipos
meta["variables"]["tb_df"]["vu_val"] = pd.to_numeric(meta["variables"]["tb_df"]["vu_val"])
## descarto los registros 'tabac' sin hash
meta["tabac"]["tb_df"] = meta["tabac"]["tb_df"][pd.notna(meta["tabac"]["tb_df"]["hash"])]


## upload
prefijo = "SISAP_PDONCO_"
db_desti = "redics"
for table in ["poblacio", "tabac", "variables"]:
    tb_desti = prefijo + table
    cols = ['hash varchar(40)'] + \
            [u.getColumnInformation(col, meta[table]["tb"], meta[table]["db"], desti='ora')['create'] \
            for col in list(meta[table]["tb_df"].columns) if col != 'hash']
    dflist = list(meta[table]["tb_df"].itertuples(index= False, name=None))
    u.createTable(tb_desti, "({})".format(", ".join(cols)), db_desti, rm=True)
    u.listToTable(dflist, tb_desti, db_desti)
    u.grantSelect(tb_desti, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUMBO"), db_desti)

###[QA]: debug upload
"""
i = 0
j = 0
for row in dflist:
    i += 1
    if type(row[0]) is not str:
        j +=1
        if j < 100:
            print(type(row[0]), i, j, row)
"""
