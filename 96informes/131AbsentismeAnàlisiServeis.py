# coding: utf8
import urllib as w
import os
from datetime import *
from sisapUtils import *
from collections import defaultdict,Counter

taula = 'sisap_absentisme'
db = 'test'

def get_servei(servei):
    if servei == 'MG':
        serdesc = "('MG')"
    elif servei == 'PED':
        serdesc = "('PED')"
    elif servei == 'INFP':
        serdesc = "('INFP', 'INFPD')"
    elif servei == 'INFA':
        serdesc = "('INF','INFMG','ENF','INFG','ATS','GCAS')"
    
    return serdesc
  
upload, uploadp =[], []
upload3 =[]
diesvisita = {}
serveis = ['MG', 'INFA', 'INFP', 'PED']
for servei in serveis:
    print servei
    codis = get_servei(servei)
    sql = "select temps_peti_visita,sum(if(visita_feta=0,1,0))/count(*),sum(visita_feta)/count(*) from {0} where servei in {1} and data_visita <>'2017-10-03'  group by temps_peti_visita".format(taula,codis)
    for temps, abs, rel in getAll(sql, db):
        upload3.append([servei, temps, abs, rel])
'''
    sql = "select sum(visita_feta),count(*),sum(visita_feta)/count(*) from {0} where servei in {1} and data_visita<>'2017-10-03'".format(taula,codis)
    for suma,total,perc in getAll(sql, db):
        p2 = 1 - perc
        upload.append([servei, 'Absentisme: ', suma, total, p2])
    sql = "select count(*) from {0} where servei in {1} and data_visita<>'2017-10-03' and sexe = 'D' ".format(taula,codis)
    for dones, in getAll(sql, db):
        p2 = dones/total
        upload.append([servei, 'Dones: ', dones, total, p2])
    sql = "select avg(edat) from {0} where servei in {1} and data_visita<>'2017-10-03' ".format(taula,codis)
    for edat, in getAll(sql, db):
        upload.append([servei, 'Mitjana edat: ', edat]) 
    sql = "select avg(gma_num_croniques) from {0} where servei in {1} and data_visita<>'2017-10-03' ".format(taula,codis)
    for croniques, in getAll(sql, db):
        upload.append([servei, 'Mitjana malalties cròniques: ', croniques])  
    sql = "select count(*) from {0} where servei in {1} and data_visita<>'2017-10-03' and ruralitat = 'U' ".format(taula,codis)
    for rural, in getAll(sql, db):
        p2 = rural/total
        upload.append([servei, 'Ruralitat: ', rural, total, p2])
    sql = "select sum(immigracio_baixa_renta),count(*),sum(immigracio_baixa_renta)/count(*) from {0} where servei in {1} and data_visita<>'2017-10-03'".format(taula,codis)
    for suma,total,perc in getAll(sql, db):
        upload.append([servei, 'Immigracio baixa renda: ', suma, total, perc])
    sql = "select avg(frequentacio_pacient) from {0} where servei in {1} and data_visita<>'2017-10-03' ".format(taula,codis)
    for freq, in getAll(sql, db):
        upload.append([servei, 'Freqüentació pacient: ', freq]) 
    sql = "select avg(medea_seccio) from {0} where servei in {1} and data_visita<>'2017-10-03' ".format(taula,codis)
    for medea, in getAll(sql, db):
        upload.append([servei, 'Mitjana índex medea: ', medea]) 
    sql = "select sum(visita_feta),count(*)from {0} where servei in {1} and data_visita<>'2017-10-03'".format(taula,codis)
    for absencies,total in getAll(sql, db):
        realitzades = total - absencies
    sql = "select visita_feta,sum(if(sexe='D',1,0))/count(*) from {0} where servei in {1} and data_visita<>'2017-10-03' group by visita_feta".format(taula,codis)
    for visi, dones in getAll(sql, db):
        uploadp.append([servei, visi, 'Dones: ',  dones])
    sql = "select visita_feta,avg(edat) from {0} where servei in {1} and data_visita<>'2017-10-03' group by visita_feta ".format(taula,codis)
    for visi, edat in getAll(sql, db):
        uploadp.append([servei, visi, 'Mitjana edat: ',  edat])
    sql = "select visita_feta,avg(gma_num_croniques) from {0} where servei in {1} and data_visita<>'2017-10-03' group by visita_feta ".format(taula,codis)
    for visi, croniques in getAll(sql, db):
        uploadp.append([servei, visi, 'Mitjana malalties cròniques: ',  croniques])
    sql = "select visita_feta,sum(if(ruralitat='R',1,0))/count(*) from {0} where servei in {1} and data_visita<>'2017-10-03'  group by visita_feta".format(taula,codis)
    for visi, rur in getAll(sql, db):
       uploadp.append([servei, visi, 'Ruralitat: ',  rur])
    sql = "select visita_feta,sum(immigracio_baixa_renta),count(*),sum(immigracio_baixa_renta)/count(*) from {0} where servei in {1} and data_visita<>'2017-10-03' group by visita_feta".format(taula,codis)
    for visi,suma,total,perc in getAll(sql, db):   
        uploadp.append([servei, visi, 'Immigració renda baixa: ',  perc])
    sql = "select visita_feta,avg(frequentacio_pacient) from {0} where servei in {1} and data_visita<>'2017-10-03' group by visita_feta ".format(taula,codis)
    for visi, freq in getAll(sql, db):
        uploadp.append([servei, visi, 'Frequentació pacient: ',  freq])
    sql = "select visita_feta,avg(medea_seccio) from {0} where servei in {1} and data_visita<>'2017-10-03' group by visita_feta".format(taula,codis)
    for visi,medea in getAll(sql, db):
        uploadp.append([servei, visi, 'Mitjana índex medea: ',  medea]) 
    sql = "select temps_peti_visita,sum(if(visita_feta=0,1,0))/count(*),sum(visita_feta)/count(*) from {0} where servei in {1} and data_visita <>'2017-10-03'  group by temps_peti_visita".format(taula,codis)
    for temps, abs, rel in getAll(sql, db):
        upload3.append([temps, abs, rel])
    sql = "select ambit, sum(visita_feta),count(*) from  {0} where servei in {1} and data_visita<>'2017-10-03'  group by ambit".format(taula,codis)
    for ambit, fetes, total in getAll(sql,db):
        percf = fetes/total
        abs = 1 - percf
        upload.append([servei, ambit, abs]) 
    sql = "select ruralitat, sum(visita_feta),count(*) from  {0} where servei in {1} and data_visita<>'2017-10-03'  group by ruralitat".format(taula,codis)
    for ambit, fetes, total in getAll(sql,db):
        percf = fetes/total
        abs = 1 - percf
        upload.append([servei, ambit, abs]) 
        
file = tempFolder + 'Analisi_absentisme_serveis.txt'
writeCSV(file, upload, sep=';')
file = tempFolder + 'Analisi_absentisme_serveis_Perc.txt'
writeCSV(file, uploadp, sep=';')
'''
file = tempFolder + 'Analisi_absentisme_serveis_dies.txt'
writeCSV(file, upload3, sep=';')
