# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re


nod="nodrizas"
imp="import"
alt="altres"

periode=range(2004,2019)
codis_grip=('J11','J11.1','J11.8','J10','J10.1','J10.8')

def get_up_ambit():
    """Asocia a cada up el ambito al que pertenece. Filtra por las up donde ep= '0208'.
       Devuelve un diccionario {up:amb_desc}
    """
    sql="select scs_codi,amb_desc from {}.cat_centres where ep = '0208'".format(nod)
    return {up:amb_desc for up,amb_desc in getAll(sql,nod)}


def get_baixes_grip(up_ambit,list_any_mes):
    baixes_grip=defaultdict(set)
    baixes_grip={anymes: set() for anymes in list_any_mes}
    sql='select id_cip_sec,ilt_up_b,ilt_data_baixa from {}.baixes where ilt_prob_salut in {}'.format(imp,codis_grip)
    for id,up,data in getAll(sql,imp):
        if up in up_ambit and data.year in periode:
            baixes_grip[data.strftime('%Y%m')].add((id,data))
    return baixes_grip

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>35}" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

if __name__ == '__main__':

    printTime('inici')
    up_ambit=get_up_ambit()
    printTime('up_ambit')
    

    anymes = []
    current=datetime.date(2004,1,1)
    end=datetime.date(2019,1,1)

    while current < end:
        anymes.append(current)
        current += relativedelta(months=1)

    list_any_mes=[data.strftime('%Y%m') for data in anymes]
    baixes_grip=get_baixes_grip(up_ambit,list_any_mes)
    printTime('baixes')

    rows= [ (periode, len(baixes_grip[periode])) for periode in list_any_mes  ]
    printTime('rows')

    header=['Periode',"Baixes"]
    name='baixes_grip.txt'

    export_txt_file(name,header,rows)