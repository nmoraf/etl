from sisapUtils import getAll, writeCSV, tempFolder


imp = 'import'
nod = 'nodrizas'
out = tempFolder + 'hipoglucemies.csv'


def edat_conversor(edat):
    if edat < 5:
        conv = 'de 0 a 4'
    elif edat < 11:
        conv = 'de 5 a 10'
    elif edat < 19:
        conv = 'de 11 a 18'
    else:
        conv = 'major de 18'
    return conv


def get_poblacio():
    sql = 'select id_cip_sec from eqa_problemes where ps = 24'
    diabetics = set([id for id, in getAll(sql, nod)])
    sql = "select scs_codi, amb_desc, sap_desc, ics_desc \
           from cat_centres where ep = '0208'"
    centres = {up: (amb, sap, eap) for (up, amb, sap, eap) in getAll(sql, nod)}
    sql = 'select id_cip_sec, up, edat from assignada_tot where ates = 1'
    poblacio = {id: {
                    'ambit': centres[up][0],
                    'sap': centres[up][1],
                    'eap': centres[up][2],
                    'edat': edat_conversor(edat),
                    'glucemies_n': 0,
                    'glucemies_hipo': 0,
                    'episodis_hipo': 0,
                    'controls_setmana': 0}
                for (id, up, edat) in getAll(sql, nod)
                if id in diabetics and up in centres}
    return poblacio


def get_glucemies():
    glucemies = {}
    limits = {458: 50, 459: 50, 460: 60, 461: 60}
    sql = 'select id_cip_sec, data_var, agrupador, valor \
           from eqa_variables, dextraccio \
           where agrupador in {} \
           and data_var between \
            date_add(date_add(data_ext, interval -1 year), interval +1 day) \
            and data_ext'.format(tuple(limits))
    for id, dat, codi, valor in getAll(sql, nod):
        if id in poblacio:
            key = (id, dat)
            if key not in glucemies:
                glucemies[key] = False
            if valor < limits[codi]:
                glucemies[key] = True
    for (id, dat), hipo in glucemies.items():
        poblacio[id]['glucemies_n'] += 1
        if hipo:
            poblacio[id]['glucemies_hipo'] += 1


def get_episodis():
    episodis = {}
    cim10 = ('E15', 'E16.0', 'E16.1', 'E16.2')
    sql = 'select id_cip_sec, pr_dde from problemes, nodrizas.dextraccio \
           where pr_cod_ps in {} \
           and pr_dde between \
            date_add(date_add(data_ext, interval -1 year), interval +1 day) \
            and data_ext'.format(cim10)
    for id, dat in getAll(sql, imp):
        if id in poblacio:
            if id not in episodis:
                episodis[id] = set()
            episodis[id].add(dat)
    for id, dates in episodis.items():
        poblacio[id]['episodis_hipo'] = len(dates)


def get_tires():
    sql = 'select id_cip_sec, tires from eqa_tires'
    for id, tires in getAll(sql, nod):
        if id in poblacio:
            poblacio[id]['controls_setmana'] = str(tires).replace('.', ',')


if __name__ == '__main__':
    poblacio = get_poblacio()
    get_glucemies()
    get_episodis()
    get_tires()
    keys = ('ambit', 'sap', 'eap', 'edat', 'glucemies_n',
            'glucemies_hipo', 'episodis_hipo', 'controls_setmana')
    resultat = [('id',) + keys]
    for id, data in poblacio.items():
        resultat.append([id] + [data[key] for key in keys])
    writeCSV(out, resultat, sep=';')
