# coding: iso-8859-1
import collections as c
import sys,datetime
from time import strftime

import sisapUtils as u


nod="nodrizas"
imp="import"
alt="altres"


def edat_dict(edat):

    if 0 <= edat <= 9:
        return '< 10 anys'
    elif edat >= 45:
        return '> 45 anys'
    else:
        inici = str(edat // 10) + ('0' if edat % 10 < 5 else '5')
        final = str(int(inici) + 4)
        return 'Entre ' + inici + ' - ' + final + ' anys'
           


class VACUNES_VPH(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_pob()
        self.get_dosis()
        self.export_data()

    def get_pob(self):
        self.pob = {}
        sql = "select id_cip_sec, usua_data_naixement, usua_sexe from assignada"
        for id, naix, sexe in u.getAll(sql, imp):
            self.pob[id] = {'naix': naix, 'sex': sexe}
    
    def get_dosis(self):
        self.vacunes = c.Counter()
        sql = "select id_cip_sec, date_format(va_u_data_vac, '%Y%m'), va_u_data_vac from  vacunes, nodrizas.dextraccio  \
        where va_u_cod in ('VPH2-S','VPH2','VPH4','VPH4-S','P00123','P00125','P00126','P00193','P00122','P00234','P00124','P00185', 'P00233','P00236','P00231','P00232','P00235') \
            and va_u_data_alta <= data_ext and (va_u_data_baixa=0 or va_u_data_baixa > data_ext)"
        for id,  periode, dat in u.getAll(sql,imp):
            if dat.year in (2017, 2018):
                if id in self.pob:
                    naix, sexe = self.pob[id]['naix'], self.pob[id]['sex']
                    edat = u.yearsBetween(naix, dat)
                    self.vacunes[(periode, edat_dict(edat), sexe)] += 1
   
    def export_data(self):
        """."""
        upload = []
        for (periode, edat, sexe), vals in self.vacunes.items():
            upload.append([periode, edat, sexe, vals])
        
        u.writeCSV(u.tempFolder + 'VPH_2017_2018.txt', upload)

if __name__ == '__main__':
    VACUNES_VPH()
