# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

#Abans de tirar repassar codis i mirar si cal afegir alguna residencia més a exclusió o hi ha canvis en la relacio de les ups!

db = 'redics'
eqa = 'eqa_ind'
imp = 'import'
nod = 'nodrizas'
table = 'residencies'

path = tempFolder + 'RESIDENCIES2015.csv'

noInclos = ['00052','03432','00158','00305','00115','00396','00068','00282']

upload,khalix = [],[]
catOthers, catOurs = {},{}
with open(path, 'rb') as file:
    p=csv.reader(file, delimiter=';')
    for i in p:
        territori,up,desc,places= i[0],i[1],i[2],i[3]
        if up == '':
            continue
        else:
            catOthers[up] = {'territori':territori,'desc':desc,'places':places}
sql = "select gu_up_residencia,gu_desc from cat_ppftb011_def"
for up,desc in getAll(sql,imp):
    catOurs[up] = {'desc':desc}

centres = {}
sql = "select scs_codi,ics_codi,ics_desc,amb_codi,amb_desc,sap_codi,sap_desc from cat_centres"
for up,br,desc,amb,ambdesc,sap,sap_desc in getAll(sql,nod):
    centres[up] = {'br':br,'desc':desc,'amb':amb,'ambdesc':ambdesc,'sap':sap,'sapdesc':sap_desc}
    
pobres = Counter()
sql = "select up,up_residencia from nodrizas.assignada_tot where institucionalitzat = 1"
for up, resi in getAll(sql, eqa):
    try:
        dOthers = catOthers[resi]['desc']
    except KeyError:
        dOthers = None
    try:
        dOurs = catOurs[resi]['desc']
    except KeyError:
        dOurs = None
    if (up,resi,dOthers,dOurs) in pobres:
        pobres[(up,resi,dOthers,dOurs)] += 1
    else:
        pobres[(up,resi,dOthers,dOurs)] = 1
catResis = {}
for (up,resi,dOthers,dOurs),count in pobres.items():
    if resi in catResis:
        if catResis[(resi)]['x'] < count:
            catResis[(resi)]['up'] = up
            catResis[(resi)]['x'] = count
            catResis[(resi)]['tot'] += count
        else:
            catResis[(resi)]['tot'] += count
    else:
        catResis[(resi)] = {'x':count,'up':up,'dOthers':dOthers,'dOurs':dOurs,'tot':count}

file = tempFolder + "pob_resis.txt"
try:
    remove(file)
except:
    pass
with openCSV(file) as c:
    for (resi),d in catResis.items():
        try:
            territori = catOthers[resi]['territori']
        except KeyError:
            territori = None
        try:
            places = catOthers[resi]['places']
        except KeyError:
            places = None
        c.writerow([resi,d['up'],d['dOthers'],d['dOurs'],d['tot'],territori,places])        

with open(file, 'rb') as fitxer:
    p=csv.reader(fitxer, delimiter='@')
    for ind in p:
        residencia,up,descOthers,desc,recompte,territori,places = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6]
        inclos = 1
        if descOthers == None or descOthers == '':
            descripcio = desc
        else:
            descripcio = descOthers
        try:
            if residencia in noInclos:
                inclos = 0
        except KeyError:
            ok = 0   
        descup = centres[up]['desc']
        ambit = centres[up]['amb']
        ambdesc = centres[up]['ambdesc']
        sap = centres[up]['sap']
        sapdesc = centres[up]['sapdesc']
        br = centres[up]['br']
        reskhalix = 'R' + residencia
        upload.append([territori,residencia,descOthers,desc,places,recompte,inclos,descripcio,up,descup,sap,sapdesc,ambit,ambdesc])
        khalix.append([reskhalix,inclos,descripcio,br,descup,sapdesc,ambdesc])
        
filekhalix = tempFolder + "cataleg_resis_khalix.txt"
try:
    remove(filekhalix)
except:
    pass

with openCSV(filekhalix) as c:
    for reskhalix,inclos,descripcio,br,descup,sapdesc,ambdesc in khalix:
        c.writerow([reskhalix,inclos,descripcio,br,descup,sapdesc,ambdesc])     
try:
    execute('drop table {}'.format(table),db)
except:
    pass

sql = "create table {}(territori_ext varchar2(100),residencia varchar2(5),residencia_desc_ext varchar2(400),residencia_desc varchar2(400),places number, pacients number,inclos number,\
         descripcio_sisap varchar2(400), up varchar2(5), up_desc varchar2(400), sap varchar2(4), sap_desc varchar2(400),ambit varchar2(4), ambit_desc varchar2(400))".format(table)
execute(sql,db)
uploadOra(upload,table,db)

users= ['PREDUECR','PREDUMMP','PREDUPRP']
for user in users:
    execute("grant select on {} to {}".format(table,user),db)
