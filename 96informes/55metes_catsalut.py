from sisapUtils import tempFolder, readCSV, writeCSV
from collections import defaultdict
from numpy import std, mean, percentile


file_data = tempFolder + 'CATSALUT_INDICADORS.TXT'
file_eap = tempFolder + 'CATALEGEAP.TXT'
file_excel = tempFolder + 'METES_CATSALUT.CSV'
file_klx = tempFolder + 'METES_CATSALUT_KLX.TXT'
pedia = ('IAP01',)


def get_eap():
    ics = set([row[2] for row in readCSV(file_eap, sep='{') if row[0] == 'AMBDESC' and 'Xarxa Concertada' not in row[8]])
    tip = {row[2]: row[8][0] for row in readCSV(file_eap, sep='{') if row[0] == 'POBTIP' and row[2] in ics}
    return tip


def get_indicadors():
    data = defaultdict(dict)
    for ind, _r, br, con, _r, _r, _r, _r, val in readCSV(file_data, sep='{'):
        if br in eap:
            tip_eap = eap[br]
            tip_ind = 'N' if ind in pedia else 'A'
            if tip_eap == 'M' or tip_eap == tip_ind:
                data[(ind, br)][con] = float(val)
    indicadors = defaultdict(list)
    for (ind, br), d in data.items():
        indicadors[ind].append((d['NUM'] if 'NUM' in d else 0.0) / d['DEN'])
    return indicadors


def get_statistics():
    resul = []
    for indicador, valors in indicadors.items():
        n = len(valors)
        cv = std(valors) / mean(valors)
        pmin = 20
        pmax = get_pmax(cv)
        mmin = percentile(valors, pmin)
        mmax = percentile(valors, pmax)
        mint = mean([mmin, mmax])
        resul.append([indicador, n, 100 * cv, pmin, pmax, 100 * mmin, 100 * mint, 100 * mmax])
    return resul


def get_pmax(coef):
    return 50 if coef < 0.1 else 60 if coef < 0.2 else 70 if coef < 0.3 else 80


def export_files():
    resul_excel = [['ind', 'eap', 'cv', 'pmin', 'pmax', 'mmin', 'mint', 'mmax']] + sorted([map(format_number, r) for r in resultat])
    writeCSV(file_excel, resul_excel, sep=';')
    metes = {'AGMMIN': 5, 'AGMINT': 6, 'AGMMAX': 7}
    resul_klx = []
    for meta, posicio in metes.items():
        resul_klx.extend([(row[0], 'DEF2016', 'NOENT', meta, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', row[posicio]) for row in resultat])
    writeCSV(file_klx, resul_klx, sep='{')


def format_number(num):
    if isinstance(num, float):
        num = str(round(num, 2)).replace('.', ',')
    return num


eap = get_eap()
indicadors = get_indicadors()
resultat = get_statistics()
export_files()
