# coding: latin1

"""
Assir Hta i Diabetes.
"""

import sisapUtils as u
from datetime import timedelta

#Llistes de codis d'hipertensi�, diabetes gestacional i DM2
hipertensio=set(['O13', 'O15.9', 'O14.9', 'C01-O13.9', 'C01-O14.90', 'C01-O15.9'])
diabetes=set(['O24.4', 'C01-O24.419'])
dm2=set(['E11','E12','E13','C01-E11.9','C01-E13.8','C01-E13.9'])
hta=set(['I10', 'C01-I10'])

class AssirHtaDiab(object):
    """."""

    def __init__(self):
        """."""
        self.get_embarassos()
        self.get_hta()
        self.get_dm2()
        self.get_problemes()
        self.get_pressio()
        self.get_glucosa_HbA1c()
        self.get_TTOG()
        self.taula_final()
        self.export_taula()

    def get_embarassos(self):
        """ Obtenim embarassos de l'any 2017
        """
        self.embarassos = {}
        sql = "select id_cip_sec, emb_dur, emb_d_tanca, emb_c_tanca, emb_durada \
               from embaras where emb_d_tanca >= 20170701 and emb_d_tanca < 20180701\
               and emb_c_tanca in ('P','C','Pr') and emb_durada > 0"
        for id, data_ini, data_tanc, c_tanc, durada in u.getAll(sql, "import"):
            if data_ini == None:
                data_ini = data_tanc + timedelta(days=-365)
            self.embarassos[id] = (data_ini, data_tanc, c_tanc, durada)

    def get_hta(self):
        self.hta_tot = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes \
               where pr_dde>20170701 and pr_cod_ps in ('I10','C01-I10')"
        for id, prob, data_prob in u.getAll(sql, "import"):
            if id in self.embarassos:
                self.hta_tot[id]=prob,data_prob
                
    def get_dm2(self):
        self.dm2_tot = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes \
               where pr_dde>20170701 and pr_cod_ps in ('E11','E12','E13','C01-E11.9','C01-E13.8','C01-E13.9')"
        for id, prob, data_prob in u.getAll(sql, "import"):
            if id in self.embarassos:
                self.dm2_tot[id]=prob,data_prob
    
    def get_problemes(self):
        """ De les embarassades del entre 1 de juliol de 2017 i 1 de juliol de 
        2018 mirem quines tenen hipertensi� durant
        el part i quines diabetes gestacional
        """
        self.hipertensio = {}
        self.diabetes = {}
        self.hta = {}
        self.dm2 = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes \
               where pr_dde>20170701"
        for id, prob, data_prob in u.getAll(sql, "import"):
            if id in self.embarassos:
                #Hipertensi� durant el part
                if prob in hipertensio and u.daysBetween(self.embarassos[id][0], data_prob) >= 0 \
                and u.daysBetween(data_prob, self.embarassos[id][1]) >= 0:
                    self.hipertensio[id] = prob
                
                #Diabetes gestacional durant el part
                if prob in diabetes and u.daysBetween(self.embarassos[id][0], data_prob) >= 0 \
                and u.daysBetween(data_prob, self.embarassos[id][1]) >= 0:
                    self.diabetes[id] = prob

        for id, prob, data_prob in u.getAll(sql, "import"):
            if id in self.hipertensio:
                #hta posterior al part (90 dies despr�s tancament??)
                if id in self.hta_tot and u.daysBetween(self.embarassos[id][1], data_prob) >= 90:
                    self.hta[id] = prob
            
            if id in self.diabetes:
                #dm2 posterior al part (90 dies despr�s tancament??)
                if id in self.dm2_tot and u.daysBetween(self.embarassos[id][1], data_prob) >= 90:
                    self.dm2[id] = prob
                    
    def get_pressio(self):
        """ Obtenim les embarassades de 2017 amb hipertensi� durant el part i
        valor registrat de presi� arterial entre 90 dies i any +90 dies de final
        del part
        """
        self.pressio = {}
        sql = "select id_cip_sec, vu_cod_vs, vu_dat_act from variables \
               where vu_cod_vs ='EK201' and vu_dat_act > 20170701"
        for id, codi, data in u.getAll(sql, "import"):
            if id in self.hipertensio:
                if u.daysBetween(self.embarassos[id][1], data) > 90 and \
                u.daysBetween(self.embarassos[id][1], data) <= 455:
                    self.pressio[id] = codi
                
    def get_glucosa_HbA1c(self):
        """ Obtenim les embarassades de 2017 amb diabetes gestacional durant el 
        part i determinacio de glucosa i/o HbA1c despr�s dels 12 mesos de part
        agrupadors d'eqa_variables 20 i 460
        """
        self.glucosa = {}
        sql = "select id_cip_sec, agrupador, data_var from eqa_variables \
               where agrupador in (20,460) and data_var > 20170701"
        for id, agrupador, data in u.getAll(sql, "nodrizas"):
            if id in self.diabetes:
                if u.daysBetween(self.embarassos[id][1], data) > 455:
                    self.glucosa[id] = agrupador
    
    def get_TTOG(self):
        """ Obtenim les embarassades de 2017 amb diabetes gestacional durant el 
        part i toler�ncia oral a la glucosa als 4/6 mesos despr�s dels part o 
        12 mesos despr�s agrupador d'eqa_variables 475
        """
        self.ttog4 = {}
        self.ttog12 = {}
        sql = "select id_cip_sec, agrupador, data_var from eqa_variables \
               where agrupador = 475 and data_var > 20170701"
        for id, agrupador, data in u.getAll(sql, "nodrizas"):
            if id in self.diabetes: 
                if u.daysBetween(self.embarassos[id][1], data) >= 210 and \
                u.daysBetween(self.embarassos[id][1], data) <= 270:
                    self.ttog4[id] = agrupador
                elif u.daysBetween(self.embarassos[id][1], data) >= 455:
                    self.ttog12[id] = agrupador
                    
    #Registrar data puerperi per a cada embarassada i controlar els successos 
    #amb posterioritat al puerperi EMB_TANC_PUER (en abs�ncia puerperi, 90 dies despr�s del part)
    #Agafem 90 dies post_part

    def taula_final(self):
        """."""
        self.final_table = {}

        for id in self.embarassos:
            hipertensio = 0
            diab = 0
            pressio = 0
            hta = 0
            ttog4 = 0
            ttog12 = 0
            hba1c = 0
            dm2 = 0
            if id in self.hipertensio:
                hipertensio = 1
                if id in self.hta:
                    hta = 1
                if id in self.pressio:
                    pressio = 1
            if id in self.diabetes:
                diab = 1
                if id in self.dm2:
                    dm2 = 1
                if id in self.glucosa:
                    hba1c = 1
                if id in self.ttog4:
                    ttog4 = 1
                if id in self.ttog12:
                    ttog12 = 1
                    
            self.final_table[id] = (self.embarassos[id], hipertensio, diab, pressio, hta, dm2, ttog4, ttog12, hba1c)

    def export_taula(self):
        """."""
        upload = self.final_table.items()
        u.writeCSV(u.tempFolder + "assirHtaDiab.csv", upload, sep=";")

if __name__ == "__main__":
    AssirHtaDiab()
