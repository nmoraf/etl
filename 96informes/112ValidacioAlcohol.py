
# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

sql='select data_ext from dextraccio'
for d in getOne(sql, 'nodrizas'):
    dext=d
    
centres = {}

sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, 'nodrizas'):
    centres[up] = True

codis = [
        ('activitats', ['VP201', 'CP201', 'ALRIS', 'ALHAB', 'ALDIA', 'ALSET', '832', '1280C', 'AUDIT', 'CAGE', 'CAGEC']),
        ('variables', ['ALRIS', 'ALSET', 'ALHAB', 'ALDIA', 'CP202', 'A-PCT', 'AUDIT', 'CAGE', 'CAGEC']),
        ('problemes', ['F10', 'F10.0', 'F10.1', 'F10.2', 'F10.3', 'F10.4', 'F10.5', 'F10.6', 'F10.7', 'F10.8', 'F10.9', 'Z72.1']),
        ]
        
origens = {'variables': 'select id_cip_sec, vu_cod_vs, vu_val, vu_dat_act from variables where vu_cod_vs in {}',
           'activitats': 'select id_cip_sec, au_cod_ac, au_val, au_dat_act from activitats where au_cod_ac in {}',
           'problemes': "select id_cip_sec, pr_cod_ps, 1, pr_dde from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) and (pr_dba is null or pr_dba > data_ext) and pr_cod_ps in {}",
        }

alcohol = {}
for taula, codi in codis:
    cod = tuple(codi)
    sql = origens[taula]
    for id, act, val, dat in getAll(sql.format(cod), 'import'):
        b = monthsBetween(dat, dext)
        if taula == 'problemes':
            alcohol[(id, taula, act)] = {'data': dat, 'valor': val}
        else:
            if 0 <= b <= 11:
                if (id, taula, act) in alcohol:
                    dat2 = alcohol[(id, taula, act)]['data']
                    if dat>= dat2:
                        alcohol[(id, taula, act)]['data'] = dat
                        alcohol[(id, taula, act)]['valor'] = val
                else:
                    alcohol[(id, taula, act)] = {'data': dat, 'valor': val}

recomptes = Counter()                
sql = 'select id_cip_sec, up from assignada_tot where edat>14'
for id, up in getAll(sql, 'nodrizas'):
    if up in centres:
        varVP201, varCP201, varALRIS, varALHAB, varALDIA, varALSET, var832, var1280C, varAUDIT, varCAGE, varCAGEC = 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre'
        actALRIS, actALSET, actALHAB, actALDIA, actCP202, actAPCT, actAUDIT, actCAGE, actCAGEC = 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre', 'No registre'
        F10, F100, F101, F102, F103, F104, F105, F106, F107, F108, F109, Z721 = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        if (id, 'activitats', 'VP201') in alcohol:
            varVP201 = alcohol[(id, 'activitats', 'VP201')]['valor']
        if (id, 'activitats', 'CP201') in alcohol:
            varCP201 = alcohol[(id, 'activitats', 'CP201')]['valor']
        if (id, 'activitats', 'ALRIS') in alcohol:
            varALRIS = 'S�'
        if (id, 'activitats', 'ALHAB') in alcohol:
            varALHAB = 'S�'
        if (id, 'activitats', 'ALDIA') in alcohol:
            varALDIA = 'S�'
        if (id, 'activitats', 'ALSET') in alcohol:
            varALSET = 'S�'
        if (id, 'activitats', '832') in alcohol:
            var832 = 'S�'
        if (id, 'activitats', '1280C') in alcohol:
            var1280C = 'S�'
        if (id, 'activitats', 'AUDIT') in alcohol:
            varAUDIT = 'S�'   
        if (id, 'activitats', 'CAGE') in alcohol:
            varCAGE = 'S�'
        if (id, 'activitats', 'CAGEC') in alcohol:
            varCAGEC = 'S�'
        if (id, 'variables', 'ALRIS') in alcohol:
            actALRIS = alcohol[(id, 'variables', 'ALRIS')]['valor']
        if (id, 'variables', 'ALHAB') in alcohol:
            actALHAB = alcohol[(id, 'variables', 'ALHAB')]['valor']
        if (id, 'variables', 'ALDIA') in alcohol:
            actALDIA = alcohol[(id, 'variables', 'ALDIA')]['valor']
        if (id, 'variables', 'ALSET') in alcohol:
            actALSET = alcohol[(id, 'variables', 'ALSET')]['valor']
        if (id, 'variables', 'CP202') in alcohol:
            actCP202 = alcohol[(id, 'variables', 'CP202')]['valor']
        if (id, 'variables', 'AUDIT') in alcohol:
            actAUDIT = alcohol[(id, 'variables', 'AUDIT')]['valor'] 
        if (id, 'variables', 'A-PCT') in alcohol:
            actAPCT = alcohol[(id, 'variables', 'A-PCT')]['valor']  
        if (id, 'variables', 'CAGE') in alcohol:
            actCAGE = alcohol[(id, 'variables', 'CAGE')]['valor']  
        if (id, 'variables', 'CAGEC') in alcohol:
            actCAGEC = alcohol[(id, 'variables', 'CAGEC')]['valor']
        if (id, 'problemes', 'F10') in alcohol:
            F10 = alcohol[(id, 'problemes', 'F10')]['valor']
        if (id, 'problemes', 'F10.0') in alcohol:
            F100 = alcohol[(id, 'problemes', 'F10.0')]['valor']
        if (id, 'problemes', 'F10.1') in alcohol:
            F101 = alcohol[(id, 'problemes', 'F10.1')]['valor']
        if (id, 'problemes', 'F10.2') in alcohol:
            F102 = alcohol[(id, 'problemes', 'F10.2')]['valor']
        if (id, 'problemes', 'F10.3') in alcohol:
            F103 = alcohol[(id, 'problemes', 'F10.3')]['valor']
        if (id, 'problemes', 'F10.4') in alcohol:
            F104 = alcohol[(id, 'problemes', 'F10.4')]['valor']
        if (id, 'problemes', 'F10.5') in alcohol:
            F105 = alcohol[(id, 'problemes', 'F10.5')]['valor']
        if (id, 'problemes', 'F10.6') in alcohol:
            F106 = alcohol[(id, 'problemes', 'F10.6')]['valor']
        if (id, 'problemes', 'F10.7') in alcohol:
            F107 = alcohol[(id, 'problemes', 'F10.7')]['valor']
        if (id, 'problemes', 'F10.8') in alcohol:
            F108 = alcohol[(id, 'problemes', 'F10.8')]['valor']
        if (id, 'problemes', 'F10.9') in alcohol:
            F109 = alcohol[(id, 'problemes', 'F10.9')]['valor']
        if (id, 'problemes', 'Z72.1') in alcohol:
            Z721 = alcohol[(id, 'problemes', 'Z72.1')]['valor']
        recomptes[(varVP201, varCP201, varALRIS, varALHAB, varALDIA, varALSET, var832, var1280C, varAUDIT, varCAGE, varCAGEC, actALRIS, actALSET, actALHAB, actALDIA, actCP202, actAPCT, actAUDIT, actCAGE, actCAGEC,F10, F100, F101, F102, F103, F104, F105, F106, F107, F108, F109, Z721)] += 1
upload = []
upload.append(['Actuaci� VP201', 'Actuaci� CP201', 'Activitat ALRIS', 'Activitat ALHAB', 'Activitat ALDIA', 'Activitat ALSET', 'Activitat 832', 'Activitat 1280C', 'Activitat AUDIT', 'Activitat CAGE', 'Activitat CAGEC', 'Variable ALRIS', 'Variable ALSET', 'Variable ALHAB', 'Variable ALDIA', 'Variable CP202', 'Variable APCT', 'Variable AUDIT', 'Variable CAGE', 'Variable CAGEC', 'F10', 'F10.0', 'F10.1', 'F10.2', 'F10.3', 'F10.4', 'F10.5', 'F10.6', 'F10.7', 'F10.8', 'F10.9', 'Z72.1', 'recompte'])
for (varVP201, varCP201, varALRIS, varALHAB, varALDIA, varALSET, var832, var1280C, varAUDIT, varCAGE, varCAGEC, actALRIS, actALSET, actALHAB, actALDIA, actCP202, actAPCT, actAUDIT, actCAGE, actCAGEC,F10, F100, F101, F102, F103, F104, F105, F106, F107, F108, F109, Z721), d in recomptes.items():
    upload.append([varVP201, varCP201, varALRIS, varALHAB, varALDIA, varALSET, var832, var1280C, varAUDIT, varCAGE, varCAGEC, actALRIS, actALSET, actALHAB, actALDIA, actCP202, actAPCT, actAUDIT, actCAGE, actCAGEC,F10, F100, F101, F102, F103, F104, F105, F106, F107, F108, F109, Z721, d])
    
    
file = tempFolder + 'Variables_alcohol.txt'
writeCSV(file, upload, sep=';')    
