# coding: utf8

"""
Procés per posar les visites 9E a una taula de redics.
Cada cop omplim tota la taula
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in u.getAll(sql, 'nodrizas'):
    centres[up] = True

resultats = {}
sql = 'select up, uba, tipus, sum(punts), count(distinct indicador) from exp_ecap_uba_back group by up, uba, tipus'
for up, uba, tipus, punts, indic in u.getAll(sql, 'eqa_ind'):
    if up in centres:
        resultats[(up, uba, tipus)] = {'puntsa': punts, 'nina': indic, 'puntsd': None, 'nind': None}
    
sql = 'select up, uba, tipus, sum(punts), count(distinct indicador) from exp_ecap_uba group by up, uba, tipus'
for up, uba, tipus, punts, indic in u.getAll(sql, 'eqa_ind'):
    if (up, uba, tipus) in resultats:
        resultats[(up, uba, tipus)]['puntsd'] = punts
        resultats[(up, uba, tipus)]['nind'] = indic
        
upload = []
for (up, uba, tipus), d in resultats.items():
    upload.append([up, uba, tipus, d['puntsa'],d['puntsd'],d['nina'],d['nind']])


file = u.tempFolder + 'simulacio.txt'
u.writeCSV(file, upload, sep='|')
    

