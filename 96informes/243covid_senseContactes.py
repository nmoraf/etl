# coding: utf-8

import sisapUtils as u
from datetime import timedelta, datetime as d

dt_now = d.now().date()

"""
listado (unos 22k) para que revisen de casos covid sin contactos, en cip13 con filiación
"""

WATCHERS = ("PDP","PREDULMB","PREDUMMP")

MDL = ("SISAP_COVID_NOCONTACTES",
       "(cip varchar2(13), up varchar(5), uba varchar(5), inf varchar(5))",
       'redics')

sql_fltUp = """
    SELECT up_cod FROM sisap_covid_cat_up WHERE amb_cod = '04' AND up_tip ='EAP'
"""
filtre_up = set([row[0] for row in u.getAll(sql_fltUp,'redics')])

sql_casUP = """
    SELECT cip, up, uba, inf
    FROM sisap_covid_ecap_llistat
"""
cas_up = {cip: (up, uba, inf) for cip, up, uba, inf in u.getAll(sql_casUP,'redics') if up in filtre_up}

sql_woContacts = """
    SELECT substr(cip, 1, 13) as cip
    ,to_DATE(COVIDSTATUSDATE, 'YYYY-MM-DD') AS CAS_DATA
    FROM dwaquas.media_ccpatients
    WHERE to_number(TOTALCONTACTS) = 0
"""
upload = []
for cip, cas_data in u.getAll(sql_woContacts,'exadata'):
    if cip in cas_up and cas_data.date() >= dt_now - timedelta(days=35):
        up, uba, inf = cas_up[cip]
        upload.append((cip, up, uba, inf))
print(len(upload))

table, cols, db = MDL
u.createTable(table, cols, db, rm=True)
u.listToTable(upload, table, db)
u.grantSelect(table, WATCHERS, db)
