# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import csv,os,sys
import pandas as pd

import sisapUtils as u

path = u.tempFolder + "fitxes/" 
EXCEL_SHEETS = 1



class Genere(object):
    """."""
    
    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_genere()
        self.get_professionals()
        self.get_tractaments()
        self.get_poblacio()
        self.export_data()
        
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_genere(self):
        """."""
        self.genprof = {}
        sheets = []
        for file in os.listdir(path):
            print file
            xls = pd.ExcelFile(path + file)
            for sheet in range(EXCEL_SHEETS):
                sheets.append(xls.parse(sheet))
            fitxes = pd.concat(sheets)
            for row in fitxes.itertuples():
                nif, lloc, edat, sexe = row.NIF, row.LLOC, row.edat, row.SEXE
                self.genprof[nif] = {'edat': edat, 'sexe': sexe}
    
    def get_professionals(self):
        """."""
        self.professionals = {}
        sql = "select ide_dni, up, uab,  adults, nens from cat_professionals \
               where tipus = 'M'"
        for dni, up, uba, adults, nens in u.getAll(sql, "import"):
            nif = u.getNif(dni)
            self.professionals[(up, uba)] = nif
    
    def get_tractaments(self):
        """."""
        self.tractats = {}
        self.prescs = c.Counter()
        sql = "select id_cip_sec, pf_cod_atc, left(pf_cod_atc,3), date_format(ppfmc_pmc_data_ini,'%Y%m%d'), \
                    date_format(ppfmc_data_fi,'%Y%m%d'), if(ppfmc_data_fi > data_ext, 0, 1), ppfmc_pmc_amb_cod_up, ppfmc_pmc_amb_num_col \
                    from tractaments, nodrizas.dextraccio where ppfmc_pmc_data_ini<=data_ext and (left(pf_cod_atc,3)='N05' or left(pf_cod_atc,3)='N06' or left(pf_cod_atc, 5)='A02BC')"
        for id, atc, atc3, ini, fi, tancat, up, numcol in u.getAll(sql, 'import'):
            if tancat == 0:
                self.tractats[(id, atc3)] =  True
                self.prescs[(id, atc3)] += 1
      
    def get_poblacio(self):
        """."""
        self.pacients = c.Counter()
        sql = "select id_cip_sec, up, uba, sexe from assignada_tot where edat>14"
        for id, up, uba, sexe in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                ans, dep, omp = 0, 0, 0
                ansc, depc, ompc = 0, 0, 0
                if (id, 'N05') in self.tractats:
                    ans = 1
                    ansc = self.prescs[(id, 'N05')]
                if (id, 'N06') in self.tractats:
                    dep = 1
                    depc = self.prescs[(id, 'N06')]
                if (id, 'A02') in self.tractats:
                    omp = 1
                    ompc = self.prescs[(id, 'A02')]
                self.pacients[(up, uba, sexe, 'ASS')] += 1
                self.pacients[(up, uba,  sexe, 'ans')] += ans
                self.pacients[(up, uba,  sexe, 'dep')] += dep
                self.pacients[(up, uba,  sexe, 'omp')] += omp
                self.pacients[(up, uba,  sexe, 'ansc')] += ansc
                self.pacients[(up, uba,  sexe, 'depc')] += depc
                self.pacients[(up, uba,  sexe, 'ompc')] += ompc
                    
                    
    def export_data(self):
        """."""
        upload = []
        for (up, uba,  sexe, tipus),d in self.pacients.items():
            if sexe == 'D' and tipus == 'ASS':
                hass = self.pacients[(up, uba,  'H', 'ASS')]
                dans = self.pacients[(up, uba,  'D', 'ans')]
                ddep = self.pacients[(up, uba,  'D', 'dep')]
                domp = self.pacients[(up, uba,  'D', 'omp')]
                hans = self.pacients[(up, uba,  'H', 'ans')]
                hdep = self.pacients[(up, uba,  'H', 'dep')]
                homp = self.pacients[(up, uba,  'H', 'omp')]
                dansc = self.pacients[(up, uba,  'D', 'ansc')]
                ddepc = self.pacients[(up, uba,  'D', 'depc')]
                dompc = self.pacients[(up, uba,  'D', 'ompc')]
                hansc = self.pacients[(up, uba,  'H', 'ansc')]
                hdepc = self.pacients[(up, uba,  'H', 'depc')]
                hompc = self.pacients[(up, uba,  'H', 'ompc')]
                if (up, uba) in self.professionals:
                    nif = self.professionals[(up, uba)]
                    if nif in self.genprof:
                        sexep, edatp = self.genprof[nif]['sexe'], self.genprof[nif]['edat']
                upload.append([up, uba, edatp, sexep, d, hass, dans, dansc, hans, hansc, ddep, ddepc,  hdep, hdepc, domp, dompc, homp, hompc])
        file = u.tempFolder + 'Prescriptors_genere.csv'
        u.writeCSV(file, upload)

      
if __name__ == '__main__':
    Genere()