# coding: iso-8859-1
#Alejandro Guarga, Catsalut (ion 3489), febrer15, Ermengol
from sisapUtils import *
import csv,os,sys
from time import strftime


nod="nodrizas"
imp="import"

OutFile= tempFolder + 'PetiDerivacions.txt'

der="nod_derivacions"
dext="dextraccio"
u11="u11"
catcen="cat_centres"

ics="0208"

printTime('Pob')

centres={}
c=0
sql="select scs_codi,ics_codi from {0} where ep={1}".format(catcen,ics)
for up,br in getAll(sql,nod):
    centres[up]={'br':br}


def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,edat,sexe from assignada_tot'
    for id_cip_sec,up,edat,sexe in getAll(sql,nod):
         try:
            br=centres[up]['br']
            assig[int(id_cip_sec)]= {'up':up,'br':br,'edat':int(edat),'sex':sexe}
         except KeyError:
            continue
    return assig  

ass = getPacients()

uhash={}
sql="select id_cip_sec,hash_d from {}".format(u11)
for id_cip_sec,hash in getAll(sql,imp):
    id_cip_sec=int(id_cip_sec)
    uhash[id_cip_sec]={'hash':hash}
printTime('Derivacions')
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    sql="select id_cip_sec,oc_data,oc_servei_ori,homol_origen,inf_servei_d_codi,homol_desti, motiu,oc_up_ori,inf_prioritat,der_up_des from {0},{1} where oc_data between date_add(data_ext,interval - 12 month) and data_ext".format(der,dext)
    for id,dat,serveio,hserveio,serveid,hserveid,mot,upori,prioritat,updes in getAll(sql,nod):
        id=int(id)
        try:
            up=ass[id]['up']
            br=ass[id]['br']
            e=ass[id]['edat']
            s=ass[id]['sex']
            h=uhash[id]['hash']
        except KeyError:
            continue
        w.writerow((h,s,e,mot,hserveio,up,br,dat,serveid,upori,prioritat,updes)) 

printTime('Fi derivacions')