from sisapUtils import multiprocess, getSubTables, getAll, tempFolder, writeCSV, printTime, createTable, listToTable
from collections import defaultdict



proves = "('S20785','S39685','S39785','006764','SC193','SC086')"

db = 'test'

tableMy = 'serologiaChagas'
createTable(tableMy, '(id_cip_sec int, cr_codi_lab varchar(20), cr_data_reg date, cr_codi_prova_ics varchar(20), cr_res_lab varchar(100), cr_ref_max_lab varchar(100))', db, rm=True)


def get_data(taula):
    data = {}
    sql = "select id_cip_sec, cr_codi_lab, cr_data_reg, cr_codi_prova_ics, cr_res_lab, cr_ref_max_lab  from {} where cr_codi_prova_ics in {} and cr_data_reg between '20100101' and '20161231'".format(taula, proves)
    for id, cod, date, prova, resul, max in getAll(sql, 'import'):
        data[(id, cod, date, prova, resul, max)] = True
    
    return data
    
def export_data(data):
    upload = []
    for set in data:
        for (id, cod, date, prova, resul, max), count in set.items():
            upload.append([id, cod, date, prova, resul, max])
    listToTable(upload, tableMy, db)
    
if __name__ == '__main__':
    printTime()
    jobs = sorted(getSubTables('laboratori'), key=getSubTables('laboratori').get, reverse=True)
    data = multiprocess(get_data, jobs, 12)
    export_data(data)
    printTime()
