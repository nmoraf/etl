# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"

def get_edat(edat):

    ed = None
    if 15 <= edat <= 44:
        ed = 'Entre 15 i 44'
    elif 45 <= edat <= 64:
        ed = 'Entre 45 i 64'
    elif 65 <= edat <= 74:
        ed = 'Entre 65 i 74'
    elif edat >74:
        ed = 'Més de 74'
    else:
        ed = 'error'
        
    return ed

centres = {}
sql = "select scs_codi from nodrizas.cat_centres where ep='0208'"
for up, in getAll(sql, nod):
    centres[up] = True
    
tabac = {}

sql = 'select id_cip_sec, tab from eqa_tabac where last=1'
for id, tab in getAll(sql, nod):
    tabac[id] = tab
    
mpoc = {}
sql = 'select id_cip_sec from eqa_problemes where ps = 62'
for id, in getAll(sql, nod):
    mpoc[id] = True
    
denfuma, numfuma = 0, 0
dennofuma,numnofuma = 0, 0
recomptes = Counter()
sql = 'select id_cip_sec, up, edat from assignada_tot where edat >14'
for id, up, edat in getAll(sql, nod):
    if up in centres:
        try:
            tab = tabac[id]
        except KeyError:
            tab = 0
        if tab == 0:
            dennofuma += 1
            if id in mpoc:
                numnofuma += 1
        else:
            denfuma += 1
            if id in mpoc:
                numfuma += 1
        ed = get_edat(edat)
        recomptes[(ed, 'DEN')] += 1
        if id in mpoc:
            recomptes[(ed, 'NUM')] += 1
            if tab == 0:
                recomptes[(ed, 'NOFUM')] += 1
            else:
                recomptes[(ed, 'FUMA')] += 1
            
upload = []
for (edat, tip), r in recomptes.items():
    if tip == 'DEN':
        num = recomptes[(edat, 'NUM')]
        fuma = recomptes[(edat, 'FUMA')]
        nofuma = recomptes[(edat, 'NOFUM')]
        upload.append([edat,num, r, fuma, nofuma])
        
file = tempFolder + 'Visitats_en_5 anys.txt'
writeCSV(file, upload, sep=';') 

        
print 'Fumadors: ', numfuma, denfuma
print 'No Fumadors: ', numnofuma, dennofuma
        
        
