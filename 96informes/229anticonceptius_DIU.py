# coding: latin1

"""
DIUs excloent embarassades
"""

import sisapUtils as u
import numpy as np
from datetime import timedelta, datetime

VALIDATION = False
VALIDATION_ASSIR = ('01422','01415','05916','01695') # Esquerra, Dreta, Muntanya, LaMina
VALIDATION_N = 5
VISITES2_BROKEN = False

def ageConverter(edat):
    if 15 <= edat <= 19:
        return '15-19'
    elif 20 <= edat <= 24:
        return '20-24'
    elif 25 <= edat <= 29:
        return '25-29'
    elif 30 <= edat <= 39:
        return '30-39'
    elif 40 <= edat <= 49:
        return '40-49'
        
def esperaConverter(dies):
    if dies <= 7:
        return '7 o menys dies'
    elif 8 <= dies <= 30:
        return 'entre 8 i 30 dies'
    elif 31 <= dies <= 60:
        return 'entre 31 i 60 dies'
    else:
        return 'superior a 60 dies'
        
def codiProblemesConverter(codi):
    if codi in ('Z30.46', 'C01-Z30.46'):
        return 'implants'
    elif codi in ('Z30.42', 'C01-Z30.42'):
        return 'injectables'
    elif codi in ('Z30.430', 'C01-Z30.430'):
        return 'DIU'
    elif codi in ('Z30.433', 'C01-Z30.433'):
        return 'retirada i reinsercio DIU'
        
def codiVariablesConverter(codi):
    if codi in ('6'):
        return 'implants'
    elif codi in ('5'):
        return 'injectables'
    elif codi in ('9'):
        return 'DIU'
    elif codi in ('10'):
        return 'DIU LNG'
 
codis_assistencia=set(['Z30.46', 'C01-Z30.46', 'Z30.42', 'C01-Z30.42', 'Z30.430', 'C01-Z30.430', 'Z30.433', 'C01-Z30.433'])

codis_interrupcio=set(['Z33.2','C01-Z33.2','Z30.430','C01-Z30.430','Z30.46','C01-Z30.46','Z30.42','C01-Z30.42'])

class DIU(object):
    """."""

    def __init__(self):
        """."""
        self.get_embarassos()
        self.get_ids()
        self.get_assir()
        self.get_metodes()
        self.get_dies_espera()
        self.get_temps_ass()
        self.gine_lleva()
        self.interrupcio_embaras()
        self.validation() if VALIDATION else None

    def get_cip_dict(self):
        """ hash: cip """
        sql = """select usua_cip_cod, usua_cip from pdptb101 """
        return {hashd: cip for (hashd, cip) in u.getAll(sql, "pdp")}

    def get_hashd_dict(self):
        sql = "select id_cip_sec, hash_d from u11"
        return {id_cip_sec: hashd for (id_cip_sec, hashd) in u.getAll(sql, "import")}

    def validation(self):
        """ sacar VALIDATION_N pacients x VALIDATION_ASSIR  """
        print(VALIDATION_ASSIR)
        
        self.hashd_dict = self.get_hashd_dict()
        self.cip_dict = self.get_cip_dict()
        
        output = []
        for assir in self.validation_problemes.keys():
            for i in range(VALIDATION_N):
                row = self.validation_problemes[assir][i]
                id_cip_sec = row[0]
                hashd = self.hashd_dict[id_cip_sec]
                cip = self.cip_dict[hashd]
                dispositiu = row[1]
                dde = row[2].strftime("%Y-%m-%d")
                demora = row[3]
                output.append((assir, cip, dispositiu, dde, demora))
        for assir in self.validation_variables.keys():
            for i in range(VALIDATION_N):
                row = self.validation_variables[assir][i]
                id_cip_sec = row[0]
                hashd = self.hashd_dict[id_cip_sec]
                cip = self.cip_dict[hashd]
                dispositiu = row[1]
                dde = row[2].strftime("%Y-%m-%d")
                demora = row[3]
                output.append((assir, cip, dispositiu, dde, demora))
        
        u.writeCSV(u.tempFolder + "assir_validation.csv", output, sep=";")
        
        
        # print(assir, self.validation_problemes[assir][0:VALIDATION_N])
        print(output)

        # print(self.validation_problemes)
    
    def get_embarassos(self):
        """ Obtenim embarassos d'1 de gener a 31 de desembre 2020
        """
        self.embarassos = set()
        sql = "select id_cip_sec from embaras where \
               emb_d_fi >=20200101 and emb_d_ini <=20201231 and emb_c_tanca in \
               ('P','C','Pr') and emb_durada > 0"
        for id, in u.getAll(sql, "import"):
            self.embarassos.add(id)
        print(len(self.embarassos))
    
    def get_ids(self):
        """."""
        self.edat = {}
        self.ups = {}
        sql = "select id_cip_sec, edat from assignada_tot where \
              sexe = 'D' and ates=1 and edat >=15 and edat <50"
        for id, edat in u.getAll(sql, "nodrizas"):
            #Excloure embarassades
            if id not in self.embarassos:
                self.edat[id] = ageConverter(edat)
        print("Total de dones assignades i ateses no embarassades")
        print(len(self.edat))
        
        sql = "select id_cip_sec, visi_up from ass_imputacio_up"
        for id, up in u.getAll(sql, "nodrizas"):
            if id in self.edat:
                self.ups[id] = up
        print("Total de dones assignades i atesses amb up imputat per assir")
        print(len(self.ups))
   
    def get_assir(self):
        """Relaciona dones amb assir"""
        self.assir = {}
        self.ids = {}
        
        sql = "select up, assir from ass_centres {}".format("where up_assir in {}".format(VALIDATION_ASSIR) if VALIDATION else '')        
        for up, assir in u.getAll(sql, "nodrizas"):
            self.assir[up] = assir
        
        sql = "select id_cip_sec, visi_up from ass_imputacio_up"
        for id, up in u.getAll(sql, "nodrizas"):
            if id in self.edat:
                if up in self.assir:
                    self.ids[id] = (up, self.assir[up])
                else:
                    if not VALIDATION:
                        self.ids[id] = (up, "No t� assir")
        upload = self.ids.items()
        u.writeCSV(u.tempFolder + "assir.csv", upload, sep=";")
   
    def get_metodes(self):
        """."""
        self.DIU_previ = set()
        self.DIU_actual = set()
        self.DIU_nou = set()
        
        #Miro les dones que han usat DIU abans d'1 de gener de 2020
        sql = "select id_cip_sec, xml_data_alta, camp_codi from xml_detall \
              where xml_tipus_orig = 'EW2003' and camp_valor=1 \
              and xml_data_alta <20200101"
        for id, dummy_data, codi in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                if codi=='9' or codi=='10':
                    self.DIU_previ.add(id)
        print(len(self.DIU_previ))
        
        #Miro les dones que fan servir DIU a partir d'1 de gener de 2020
        sql = "select id_cip_sec, xml_data_alta, camp_codi from xml_detall \
              where xml_tipus_orig = 'EW2003' and camp_valor=1 \
              and xml_data_alta >=20200101 and xml_data_alta <=20201231"
        for id, dummy_data, codi in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                if codi=='9' or codi=='10':
                    if id in self.DIU_previ:
                        self.DIU_actual.add(id)
                    else:
                        self.DIU_nou.add(id)
        print(len(self.DIU_actual))
        print(len(self.DIU_nou))
        
        #Registro insercions i reinsercions DIU entre 1 de gener i 31 de desembre
        self.insercio_DIU = set()
        self.reinsercio_DIU = set()
        sql = "select id_cip_sec, pr_cod_ps from problemes where pr_cod_ps \
               in ('Z30.430', 'Z30.433', 'C01-Z30.430', 'C01-Z30.433') \
               and pr_dde >=20200101 and pr_dde <=20201231"
        for id, codi in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                if codi in ('Z30.430', 'C01-Z30.430'):
                    self.insercio_DIU.add(id)
                else:
                    self.reinsercio_DIU.add(id)
        print("Nombre de dones amb insercio DIU 2020")
        print(len(self.insercio_DIU))
        print("Nombre de dones amb reinsercio DIU 2020")
        print(len(self.reinsercio_DIU))
        
        self.total_assir = {}
        self.DIU_antic_assir_var = {}
        self.DIU_nou_assir_var = {}
        self.insercio_DIU_assir_codi = {}
        self.reinsercio_DIU_assir_codi = {}
        
        for id in self.ids:
            assir = self.ids[id][1]
            
            if assir not in self.total_assir:
                self.total_assir[assir]=1
            else:
                self.total_assir[assir]+=1
            
            if id in self.DIU_actual:
                if assir not in self.DIU_antic_assir_var:
                    self.DIU_antic_assir_var[assir]=1
                else:
                    self.DIU_antic_assir_var[assir]+=1
            
            if id in self.DIU_nou:
                if assir not in self.DIU_nou_assir_var:
                    self.DIU_nou_assir_var[assir]=1
                else:
                    self.DIU_nou_assir_var[assir]+=1
                    
            if id in self.insercio_DIU:
                if assir not in self.insercio_DIU_assir_codi:
                    self.insercio_DIU_assir_codi[assir]=1
                else:
                    self.insercio_DIU_assir_codi[assir]+=1
            
            if id in self.reinsercio_DIU:
                if assir not in self.reinsercio_DIU_assir_codi:
                    self.reinsercio_DIU_assir_codi[assir]=1
                else:
                    self.reinsercio_DIU_assir_codi[assir]+=1
                    
        for assir in self.total_assir:
            #Control d'ASSIRs sense casos 
            antic=insercio=nou=reinsercio=0
            if assir in self.DIU_antic_assir_var:
                antic=self.DIU_antic_assir_var[assir]
            if assir in self.insercio_DIU_assir_codi:
                insercio=self.insercio_DIU_assir_codi[assir]
            if assir in self.DIU_nou_assir_var:
                nou=self.DIU_nou_assir_var[assir]
            if assir in self.reinsercio_DIU_assir_codi:
                reinsercio=self.reinsercio_DIU_assir_codi[assir]
            self.total_assir[assir] = (self.total_assir[assir], antic, insercio, \
            nou, reinsercio)
            
        upload = self.total_assir.items()
        u.writeCSV(u.tempFolder + "total_assir.csv", upload, sep=";")
                    
    def get_dies_espera(self):
        """."""
        #Es compten tots els registres de DIU, siguin nous o canvis,
        #tant per problemes com per variables
        
        #Miro darrer registre d'inserci� o reinserci� DIU per a cada id
        #entre 1 de gener i 31 de desembre i guardo la data
        self.data_problemes_DIU = {}
        sql = "select id_cip_sec, pr_dde from problemes where pr_cod_ps \
               in ('Z30.430', 'Z30.433', 'C01-Z30.430', 'C01-Z30.433') \
               and pr_dde >=20200101 and pr_dde <=20201231 order by \
               id_cip_sec, pr_dde asc"
        for id, data in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                self.data_problemes_DIU[id] = data
        print("Nombre persones amb registre DIU 1-gen a 31-des de 2020 a problemes")
        print(len(self.data_problemes_DIU))
        
        #Miro darrer registre de variable DIU per a cada id
        #entre 1 de gener i 31-des i guardo les dates
        self.data_variables_DIU = {}
        sql = "select id_cip_sec, xml_data_alta from xml_detall \
               where xml_tipus_orig = 'EW2003' and camp_valor=1 \
               and xml_data_alta >=20200101 and xml_data_alta <=20201231 \
               and camp_codi in (9,10) order by id_cip_sec, xml_data_alta asc"
        for id, data in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                self.data_variables_DIU[id] = data
        print("Nombre de persones amb registre DIU a variables 1-gen a 31-des de 2020")
        print(len(self.data_variables_DIU))
        
        self.dies_problemes = {}
        self.dies_variables = {}
        self.validation_problemes = {}
        self.validation_variables = {}
        id_actual_probs = id_actual_vars = 0
        
        # Guardo nom�s la visita m�s propera en temps a problema o variable
        sql = """select
                    id_cip_sec,
                    visi_dia_peticio
                from {}
                where
                    visi_tipus_visita in ('VGIN', 'IEF', 'PUER')
                    and visi_dia_peticio between date '2019-12-01' and date '2020-12-31'
                order by
                    id_cip_sec,
                    visi_dia_peticio desc""".format('visites2' if not VISITES2_BROKEN else 'visites')
        for id, data in u.getAll(sql, "import"):
            if id in self.ids:
                assir = self.ids[id][1]
                if id in self.data_problemes_DIU and id not in self.dies_problemes:
                    if data <= self.data_problemes_DIU[id]:
                        if id != id_actual_probs:
                            if assir not in self.dies_problemes:
                                self.dies_problemes[assir]=[]
                                self.validation_problemes[assir] = []
                            self.dies_problemes[assir].append(u.daysBetween(data, self.data_problemes_DIU[id]))
                            self.validation_problemes[assir].append((id, 'DIUp', data, u.daysBetween(data, self.data_problemes_DIU[id])))
                if id in self.data_variables_DIU and id not in self.dies_variables:
                    if data <= self.data_variables_DIU[id]:
                        if id != id_actual_vars:
                            if assir not in self.dies_variables:
                                self.dies_variables[assir]=[]
                                self.validation_variables[assir] = []
                            self.dies_variables[assir].append(u.daysBetween(data, self.data_variables_DIU[id]))
                            self.validation_variables[assir].append((id, 'DIUv', data, u.daysBetween(data, self.data_variables_DIU[id])))
        
        self.mean_median_probs={}
        self.mean_median_vars={}
        print("Mitjana i mediana de dies d'espera problemes")
        for assir in self.dies_problemes:
            self.mean_median_probs[assir] = (np.mean(self.dies_problemes[assir]), \
            np.median(self.dies_problemes[assir]) )
            
        print("Mitjana i mediana de dies d'espera variables")
        for assir in self.dies_variables:
            self.mean_median_vars[assir] = (np.mean(self.dies_variables[assir]), \
            np.median(self.dies_variables[assir]) )
            
        upload = self.mean_median_probs.items()
        u.writeCSV(u.tempFolder + "mean_median_probs.csv", upload, sep=";")
        upload = self.mean_median_vars.items()
        u.writeCSV(u.tempFolder + "mean_median_vars.csv", upload, sep=";")

    def get_temps_ass(self):
        """."""
        #Miro primer assist�ncia entre 1 de gener i 31-des
        #Al set codis_assistencia tenim els codis:
        #'Z30.46', 'C01-Z30.46', 'Z30.42', 'C01-Z30.42',  
        #'Z30.430', 'C01-Z30.430', 'Z30.433', 'C01-Z30.433'
        self.metode_problemes = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes where pr_cod_ps \
               in {} and pr_dde >=20200101 and pr_dde <=20201231 order by \
               id_cip_sec, pr_dde asc".format(tuple(codis_assistencia))
        for id, codi, data in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                #codiProblemesConverter transforma els codis en la seva descripci� literal
                self.metode_problemes[id] = (codiProblemesConverter(codi), data)
        print("Nombre de dones amb contraconceptiu registrat a problemes")
        print(len(self.metode_problemes))
        
        #Dies dif entre petici� visita i assist�ncia
        id_actual=0
        self.temps_ass_problemes = {}
        sql = "select id_cip_sec, visi_dia_peticio from visites2 where \
               visi_tipus_visita in ('VGIN', 'IEF', 'PUER') order by \
               id_cip_sec, visi_dia_peticio desc"
        for id, data in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                if id != id_actual and id in self.metode_problemes:
                    if data <= self.metode_problemes[id][1]:
                        id_actual = id
                        self.temps_ass_problemes[id] = self.metode_problemes[id][0], \
                        esperaConverter(u.daysBetween(data, self.metode_problemes[id][1]))
                    
        #Miro registre a variables entre 1 de gener i 31-des
        #als codis 5,6,9,10 (DIU, implants, injectables)
        self.metode_variables = {}
        sql = "select id_cip_sec, camp_codi, xml_data_alta from xml_detall \
               where xml_tipus_orig = 'EW2003' and camp_valor=1 \
               and xml_data_alta >=20200101 and xml_data_alta <=20201231 \
               and camp_codi in (5,6,9,10)"
        for id, codi, data in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                #codiVariablesConverter transforma els codis en la seva descripci� literal
                self.metode_variables[id] = (codiVariablesConverter(codi), data)
        print("Nombre de dones amb contraconceptiu registrat a variables")
        print(len(self.metode_variables))
                    
        #Dies dif entre petici� visita i registre variables
        id_actual=0
        self.temps_ass_variables = {}
        sql = "select id_cip_sec, visi_dia_peticio from visites2 where \
               visi_tipus_visita in ('VGIN', 'IEF', 'PUER') order by \
               id_cip_sec, visi_dia_peticio desc"
        for id, data in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                if id != id_actual and id in self.metode_variables:
                    if data <= self.metode_variables[id][1]:
                        id_actual = id
                        self.temps_ass_variables[id] = self.metode_variables[id][0], \
                        esperaConverter(u.daysBetween(data, self.metode_variables[id][1]))        

        self.recompte_temps_ass_problemes={}
        self.recompte_temps_ass_variables={}
        self.recompte_met_temps_ass_problemes={}
        self.recompte_met_temps_ass_variables={}
        
        for id in self.temps_ass_problemes:
            assir=self.ids[id][1]
            
            if (self.temps_ass_problemes[id][0], assir) not in self.recompte_met_temps_ass_problemes:
                self.recompte_met_temps_ass_problemes[self.temps_ass_problemes[id][0], assir]=1
            else:
                self.recompte_met_temps_ass_problemes[self.temps_ass_problemes[id][0], assir]+=1
            if (self.temps_ass_problemes[id][0], self.temps_ass_problemes[id][1], assir) not in self.recompte_temps_ass_problemes:
                self.recompte_temps_ass_problemes[self.temps_ass_problemes[id][0], self.temps_ass_problemes[id][1], assir]=1
            else:
                self.recompte_temps_ass_problemes[self.temps_ass_problemes[id][0], self.temps_ass_problemes[id][1], assir]+=1
        
        for id in self.recompte_temps_ass_problemes:
            self.recompte_temps_ass_problemes[id[0], id[1], id[2]]= \
            100*(self.recompte_temps_ass_problemes[id[0], id[1], id[2]])/float(self.recompte_met_temps_ass_problemes[id[0],id[2]])
                
        for id in self.temps_ass_variables:
            assir=self.ids[id][1]
            
            if (self.temps_ass_variables[id][0], assir) not in self.recompte_met_temps_ass_variables:
                self.recompte_met_temps_ass_variables[self.temps_ass_variables[id][0], assir]=1
            else:
                self.recompte_met_temps_ass_variables[self.temps_ass_variables[id][0], assir]+=1
            if (self.temps_ass_variables[id][0], self.temps_ass_variables[id][1], assir) not in self.recompte_temps_ass_variables:
                self.recompte_temps_ass_variables[self.temps_ass_variables[id][0], self.temps_ass_variables[id][1], assir]=1
            else:
                self.recompte_temps_ass_variables[self.temps_ass_variables[id][0], self.temps_ass_variables[id][1], assir]+=1
                
        for id in self.recompte_temps_ass_variables:
            self.recompte_temps_ass_variables[id[0], id[1], id[2]]= \
            100*(self.recompte_temps_ass_variables[id[0], id[1], id[2]])/ \
            float(self.recompte_met_temps_ass_variables[id[0],id[2]])
             
        upload = self.recompte_temps_ass_problemes.items()
        u.writeCSV(u.tempFolder + "recompte_temps_ass_problemes.csv", upload, sep=";")
        
        upload = self.recompte_temps_ass_variables.items()
        u.writeCSV(u.tempFolder + "recompte_temps_ass_variables.csv", upload, sep=";")
    
    def gine_lleva(self):
        """."""
        self.gine_usu=set()
        self.lleva_usu=set()
        
        #Registro el tipus de professional
        sql = "select IDE_USUARI, IDE_CATEG_PROF from cat_pritb992 where \
        IDE_CATEG_PROF like '%GINE%' or IDE_CATEG_PROF like '%LLEV%'"
        for usu, prof in u.getAll(sql, "import"):
            if prof in ('LLEVADORA', 'LLEVADORA APD'):
                self.lleva_usu.add(usu)
            else:
                self.gine_usu.add(usu)
        
        self.DIU_gine = set()
        self.DIU_lleva = set()
        self.implant_gine = set()
        self.implant_lleva = set()
        #Guardo darrer registre d'inserci� DIU o implant per a cada id
        #entre 1 de gener i 31-des i guardo la data i professional
        self.data_problemes_DIU = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_usu from problemes \
               where pr_cod_ps in ('Z30.430', 'Z30.433', 'C01-Z30.430', \
               'C01-Z30.433', 'Z30.46', 'C01-Z30.46') and pr_dde >=20200101 \
               and pr_dde <=20201231 order by id_cip_sec, pr_dde asc"
        for id, codi, usu in u.getAll(sql, "import"):
            #Miro que aquella dona estigui assignada
            if id in self.ids:
                if codi in ('Z30.430', 'Z30.433', 'C01-Z30.430', 'C01-Z30.433'):
                    if usu in self.lleva_usu:
                        self.DIU_lleva.add(id)
                    if usu in self.gine_usu:
                        self.DIU_gine.add(id)
                else:
                    if usu in self.lleva_usu:
                        self.implant_lleva.add(id)
                    if usu in self.gine_usu:
                        self.implant_gine.add(id)
                        
        #A aquests diccionaris guardar� nombre de dones per ASSIR
        #amb DIU i amb implant
        self.total_assir_DIU = {}
        self.total_assir_implant = {}         
        self.total_assir_DIU_implant = {}        
        self.DIU_gine_tot = {}
        self.DIU_lleva_tot = {}
        self.implant_gine_tot = {}
        self.implant_lleva_tot = {}
        
        for id in self.ids:
            assir = self.ids[id][1]
            
            if id in self.DIU_gine:
                if assir not in self.total_assir_DIU:
                    self.total_assir_DIU[assir]=1
                else:
                    self.total_assir_DIU[assir]+=1
                if assir not in self.DIU_gine_tot:
                    self.DIU_gine_tot[assir]=1
                else:
                    self.DIU_gine_tot[assir]+=1
            
            if id in self.DIU_lleva:
                if assir not in self.total_assir_DIU:
                    self.total_assir_DIU[assir]=1
                else:
                    self.total_assir_DIU[assir]+=1
                if assir not in self.DIU_lleva_tot:
                    self.DIU_lleva_tot[assir]=1
                else:
                    self.DIU_lleva_tot[assir]+=1
                    
            if id in self.implant_gine:
                if assir not in self.total_assir_implant:
                    self.total_assir_implant[assir]=1
                else:
                    self.total_assir_implant[assir]+=1
                if assir not in self.implant_gine_tot:
                    self.implant_gine_tot[assir]=1
                else:
                    self.implant_gine_tot[assir]+=1
            
            if id in self.implant_lleva:
                if assir not in self.total_assir_implant:
                    self.total_assir_implant[assir]=1
                else:
                    self.total_assir_implant[assir]+=1
                if assir not in self.implant_lleva_tot:
                    self.implant_lleva_tot[assir]=1
                else:
                    self.implant_lleva_tot[assir]+=1
                    
        for assir in self.total_assir_DIU:
            #Control d'ASSIRs sense casos 
            DIU_gine=DIU_lleva=implant_gine=implant_lleva=0
            if assir in self.DIU_gine_tot:
                DIU_gine=self.DIU_gine_tot[assir]
            if assir in self.DIU_lleva_tot:
                DIU_lleva=self.DIU_lleva_tot[assir]
            if assir in self.implant_gine_tot:
                implant_gine=self.implant_gine_tot[assir]
            if assir in self.implant_lleva_tot:
                implant_lleva=self.implant_lleva_tot[assir]
            if assir not in self.total_assir_implant:
                self.total_assir_implant[assir]=0                
            self.total_assir_DIU_implant[assir] = (self.total_assir_DIU[assir], \
            self.total_assir_implant[assir], DIU_gine, DIU_lleva, implant_gine, \
            implant_lleva)
            
        upload = self.total_assir_DIU_implant.items()
        u.writeCSV(u.tempFolder + "total_assir_DIU_implant.csv", upload, sep=";")

    def interrupcio_embaras(self):
        """."""
        #Miro primer interrupci� embar�s entre 1 de gener i 31-des
        self.int = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes where pr_cod_ps \
               in {} and pr_dde >=20200101 and pr_dde <=20201231 order by \
               id_cip_sec, pr_dde asc".format(tuple(codis_interrupcio))
        for id, codi, data in u.getAll(sql, "import"):
            if id in self.ids:
                self.int[id] = codi, data
        print("Nombre de dones amb interrupcio voluntaria de l'embaras")
        print(len(self.int))
        
        self.int_DIU = set()
        self.int_implant = set()
        self.int_injectable = set()
        
        for id in self.int:
            if self.int[id][0] in ('Z30.430','C01-Z30.430'):
                self.int_DIU.add(id)
            elif self.int[id][0] in ('Z30.46','C01-Z30.46'):
                self.int_implant.add(id)
            elif self.int[id][0] in ('Z30.42','C01-Z30.42'):
                self.int_injectable.add(id)
                
        self.total_assir_int = {}
        self.int_DIU_tot = {}
        self.int_implant_tot = {}
        self.int_injectable_tot = {}
        
        for id in self.ids:
            assir = self.ids[id][1]
            
            if id in self.int_DIU:
                if assir not in self.int_DIU_tot:
                    self.int_DIU_tot[assir]=1
                else:
                    self.int_DIU_tot[assir]+=1
            
            if id in self.int_implant:
                if assir not in self.int_implant_tot:
                    self.int_implant_tot[assir]=1
                else:
                    self.int_implant_tot[assir]+=1
                    
            if id in self.int_injectable:
                if assir not in self.int_injectable_tot:
                    self.int_injectable_tot[assir]=1
                else:
                    self.int_injectable_tot[assir]+=1
                    
        for assir in self.total_assir:
            #Control d'ASSIRs sense casos 
            DIU=implant=injectable=0
            if assir in self.int_DIU_tot:
                DIU=self.int_DIU_tot[assir]
            if assir in self.int_implant_tot:
                implant=self.int_implant_tot[assir]
            if assir in self.int_injectable_tot:
                injectable=self.int_injectable_tot[assir]
            self.total_assir_int[assir] = (self.total_assir[assir][0], DIU,\
            implant, injectable)
            
        upload = self.total_assir_int.items()
        u.writeCSV(u.tempFolder + "total_assir_int.csv", upload, sep=";")
 
if __name__ == "__main__":
    now = datetime.now()
    print(now)
    DIU()
    end = datetime.now()
    print(end)
    print(end - now)
