# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


DEBUG = False
DATES = ('2019-07-01', '2019-09-30')

TABLE = "lab_piramide"
DATABASE = "test"


class Piramide(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_labs()
        self.get_peticions()
        self.get_piramide()
        self.upload_data()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_desc from cat_centres"
        self.centres = {cod: des for (cod, des) in u.getAll(sql, "nodrizas")}

    def get_labs(self):
        """."""
        sql = "select cla_codi, cla_desc from labtb111"
        self.labs = {cod: des for (cod, des) in u.getAll(sql, "redics")}

    def get_peticions(self):
        """."""
        self.peticions = c.defaultdict(c.Counter)
        if DEBUG:
            data = [worker("6734")]
        else:
            data = u.multiprocess(worker, u.sectors)
        for sector in data:
            for (up, lab), n in sector:
                if up in self.centres:
                    self.peticions[up][lab] += n

    def get_piramide(self):
        """."""
        self.piramide = []
        for up, dades in self.peticions.items():
            lab = sorted(dades.items(), key=lambda x: x[1], reverse=True)[0][0]
            perc = round(100 * dades[lab] / float(sum(dades.values())), 2)
            this = (up, self.centres.get(up), lab, self.labs.get(lab), perc)
            self.piramide.append(this)

    def upload_data(self):
        """."""
        cols = "(up_cod varchar(5), up_des varchar(255), lab_cod varchar(5), \
                 lab_des varchar(255), percentatge double)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.piramide, TABLE, DATABASE)


def worker(sector):
    """."""
    peticions = c.Counter()
    sql = "select pet_up, pet_codi_lab \
           from labtb200 \
           where pet_data_reg between date '{}' and date '{}'".format(*DATES)
    for row in u.getAll(sql, sector):
        peticions[row] += 1
    return peticions.items()


if __name__ == "__main__":
    Piramide()
