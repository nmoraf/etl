# coding: iso-8859-1

"""
Envien fitxers de LMS des de HC3 per a carregar els nia però alguns els envien en més un full excel!
"""

from sisapUtils import *
from datetime import *
import csv,os,sys
import pandas as pd
from time import strftime

tb = "SISAP_LMS_AUTOREGISTRATS"
db = "redics"

EXCEL_SHEETS = 1

path = tempFolder + "Nias_autor/" 

class LMS(object):
    """."""
         
    def __init__(self):
        """."""
        self.upload = []
        self.get_nia()
        self.load_file()
        self.upload_data()
        
    def get_nia(self):
        """."""
        
        self.nies = {}
        sql = "select usua_nia, usua_cip, codi_sector from md_poblacio"
        for nia, hash, sector in getAll(sql, "redics"):
            self.nies[nia] = {'hash': hash, 'sector': sector}
        
    def load_file(self):
        """agafo dades dels arxius que ens han passat i les pujo a redics"""
    
        sheets = []
        for file in os.listdir(path):
            print file
            xls = pd.ExcelFile(path + file)
            for sheet in range(EXCEL_SHEETS):
                sheets.append(xls.parse(sheet))
        fitxes = pd.concat(sheets)
        file = tempFolder + "prova_lms.txt"
        a = 0
        for row in fitxes.itertuples():
            niaExcel, data_acredExcel, first, last, num = row.nia, row.data_acred, row.data_primer_autor, row.data_last_autor, row.n_autor
            if niaExcel in self.nies:
                hash, sector = self.nies[niaExcel]['hash'], self.nies[niaExcel]['sector']
                self.upload.append([sector, hash, str(data_acredExcel), str(first), str(last), num])
            else:
                a += 1
        print a

    def upload_data(self):
        """."""
        columns = ["codi_sector varchar2(4)", "hash varchar2(40)",  "data_acreditacio varchar2(100)", "data_primer_autoregistre varchar2(100)", "data_ultim_autoregistre varchar2(100)", "n_autoregistres number"]
        createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        listToTable(self.upload, tb, db)
        grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUECR"), db)
        
if __name__ == "__main__":
    LMS()