# coding: iso-8859-1
#Pregunta parlament: hepatitis C, Ermengol, Novembre 2016
from sisapUtils import *
from collections import defaultdict, Counter

imp = 'import_jail'

far_VHC = "('J05AE14', 'J05AX15', 'J05AX65', 'J05AX14', 'J05AX16', 'J05AX67')"

dx_VHC = '(12, 29, 550, 553)'

periodes = {'PA': {
                    'dini': '20150101',
                    'dfi': '20151031',
                    },
            'PB': {
                    'dini': '20160101',
                    'dfi': '20161031',
                    },
            'PC': {
                    'dini': '20150101',
                    'dfi': '20151231',
                    },
    }


centres = {}
sql =  'select scs_codi, ics_desc from nodrizas.jail_centres'
for up, desc in getAll(sql, imp):
    centres[up] = desc

hepatitisC = {}

sql = "select id_cip_sec, date_format(dde,'%Y%m%d') from eqa_problemes where ps in {} and id_cip_sec<0".format(dx_VHC)
for id, dde in getAll(sql, 'nodrizas'):
    if id in hepatitisC:
        dde_ant = hepatitisC[id]['dde']
        if dde < dde_ant:
            hepatitisC[id]['dde'] = dde
    else:
        hepatitisC[id] = {'dde':dde}
    
pob = {}
sql = "select id_cip_sec,(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe,usua_uab_up, if(usua_nacionalitat='',724,usua_nacionalitat) from assignada, nodrizas.dextraccio"
for id, edat, sexe, up, nac in getAll(sql, imp):
    if nac == '724':
        nacionalitat = 'AUTÒCTONS'
    else:
        nacionalitat = 'ESTRANGERS'
    pob[id] = {'sexe': sexe, 'edat': edat, 'up': up, 'nac': nacionalitat}
    
last_up = {}
sql = "select id_cip_sec, huab_data_ass, huab_up_codi from moviments order by huab_data_ass desc"
for id, data, up in getAll(sql, imp):
    if id in last_up:
        continue
    else:
        last_up[id] = up 
            
pacients = {} 

ingressats = {}
sql = 'select id_cip_sec from assignada_tot_with_jail where id_cip_sec<0'
for id, in getAll(sql, 'nodrizas'):
    ingressats[id] = True
  
sql = "select id_cip_sec, date_format(ppfmc_pmc_data_ini,'%Y%m%d'),date_format(ppfmc_data_fi,'%Y%m%d') from tractaments where pf_cod_atc in {}".format(far_VHC)
for id, inici, final in getAll(sql, imp):
    edat = pob[id]['edat']
    sexe = pob[id]['sexe']
    nacio = pob[id]['nac']
    up = pob[id]['up']
    if up == '':
        try:
            up = last_up[id]
        except KeyError:
            up = 'INDETERMINADA'
    try:
        desc = centres[up]
    except KeyError:
        desc = up
    pa, pb, pc, ha, hb, hc = 0,0,0,0,0,0
    try:
        dde_vhc = hepatitisC[id]['dde']
    except KeyError:
        dde_vhc = False
    dini,dfi = periodes['PA']['dini'], periodes['PA']['dfi']
    if (dini <= inici <= dfi) or (inici <dini and final>= dini):
        pa = 1
    if dde_vhc <> False:
        if (dini <= dde_vhc <= dfi) or (dde_vhc <dini):
            ha = 1
    dini,dfi = periodes['PB']['dini'], periodes['PB']['dfi']
    if (dini <= inici <= dfi) or (inici <dini and final>= dini):
        pb = 1
    if dde_vhc <> False:
        if (dini <= dde_vhc <= dfi) or (dde_vhc <dini):
            hb = 1
    dini,dfi = periodes['PC']['dini'], periodes['PC']['dfi']
    if (dini <= inici <= dfi) or (inici <dini and final>= dini):
        pc = 1
    if dde_vhc <> False:
        if (dini <= dde_vhc <= dfi) or (dde_vhc <dini):
            hc = 1
    in_prison = 0
    if id in ingressats:
        in_prison=1
    if id in pacients:
        pacients[id]['pa'] += pa
        pacients[id]['pb'] += pb
        pacients[id]['pc'] += pc
    else:
        pacients[id] = {'edat': edat, 'sexe': sexe, 'up': desc, 'nacio': nacio, 'pres': in_prison, 'pa': pa, 'pb': pb, 'pc':pc, 'ha':ha, 'hb': hb, 'hc': hc}

    
upload = []
for id, valors in pacients.items():
    pa = valors['pa']
    pb = valors['pb']
    pc = valors['pc']
    if pa > 1:
        pa = 1
    if pb > 1:
        pb = 1
    if pc > 1:
        pc = 1
    upload.append([id, valors['edat'],  valors['sexe'],  valors['up'], valors['nacio'], valors['pres'], pa, pb, pc, valors['ha'],valors['hb'],valors['hc']])
    
table = 'jail_far_VHC'
create = "(id_cip_sec int,  edat double, sexe varchar(10), up varchar(150), nacionalitat varchar(20), in_prison int, PA int, PB int, PC int, VHC_PA int, VHC_PB int, VHC_PC int)"
createTable(table,create,'test', rm=True)

listToTable(upload, table, 'test')

file = tempFolder + 'Tractament_VHC.txt'
writeCSV(file, upload, sep=';')
