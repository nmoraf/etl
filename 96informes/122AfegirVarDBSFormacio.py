# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
'''
variables = ['op_obt_def', 'oc_risc', 'oe_seg_def', 'oe_seg_temp', 'oe_ex_temp', 'oe_ex_def', 'oe_obt_temp', 'oe_ca_def', 'oe_ca_temp', 'oe_obt_def']

variables = ['oe_seg_temp', 'oe_ex_temp', 'oe_ex_def', 'oe_obt_temp', 'oe_ca_def', 'oe_ca_temp', 'oe_obt_def']


for camp in variables:
    to_change = {}
    sql ="select distinct a.c_cip, b.{0} from dbsform a,dbs b where b.{0} is not null and a.c_edat_anys=b.c_edat_anys and rownum<1300".format(camp)
    for cip, dada in getAll(sql, 'redics'):
        to_change[cip] = dada
        
      
    for cip, dada in to_change.items():
        sql = "update dbsform set {0} = '{1}' where c_cip = '{2}'".format(camp, dada, cip)
        execute(sql, 'redics')

    sql = 'commit'
    execute(sql, 'redics')
'''
#risc de càries segons caod

variables = ['oc_risc']


for camp in variables:
    to_change = {}
    sql ="select distinct c_cip, oc_caod from dbsform where  oc_caod is not null"
    for cip, caod in getAll(sql, 'redics'):
        if int(caod) > 0:
            dada = 1
            to_change[cip] = dada
    ''' 
    sql ="select distinct c_cip, oc_cao from dbsform where  rownum<1300 and oc_cao is not null"
    for cip,  co in getAll(sql, 'redics'):
        if int(co) > 1:
            dada = 1
            to_change[cip] = dada
    '''  
    for cip, dada in to_change.items():
        sql = "update dbsform set {0} = '{1}' where c_cip = '{2}'".format(camp, dada, cip)
        execute(sql, 'redics')

    sql = 'commit'
    execute(sql, 'redics')