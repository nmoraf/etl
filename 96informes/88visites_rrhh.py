# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


tb = 'sisap_visites_rrhh'
ori = 'sisap_cmbdap'
db = 'redics'
inici = '20181001'
fi = '20190930'
categories = ('10999', '10888', '10117')

debug = False


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        if debug:
            self.visites = [self.get_visites('s6734')]
        else:
            self.visites = u.multiprocess(self.get_visites,
                                          u.getTablePartitions(ori, db))
        self.get_centres()
        self.get_professionals()
        self.get_poblacio()
        self.get_resultat()
        self.upload_table()

    def create_table(self):
        """."""
        u.createTable(tb, '(col varchar2(8), dni varchar2(8), \
                           nom varchar2(255), categoria varchar2(5), \
                           up varchar2(5), cupo int, ambit varchar2(255), \
                           eap varchar2(255), dies int, centre int, \
                           domicili int, telefonica int, virtual int)',
                      db, rm=True)

    def get_visites(self, partition):
        """."""
        resul = c.Counter()
        dies = c.defaultdict(set)
        sql = "select substr(cp_ecap_col, 0, 8), cp_up_ecap, \
               substr(cp_t_act, 0, 2), cp_ecap_dvis \
               from {} partition ({}) \
               where cp_ecap_dvis between to_date('{}', 'YYYYMMDD') \
                and to_date('{}', 'YYYYMMDD') \
               and cp_ecap_categoria in {}".format(ori, partition,
                                                   inici, fi, categories)
        for col, up, tip, dat in u.getAll(sql, db):
            resul[(col, up, tip)] += 1
            dies[(col, up)].add(dat)
        for (col, up), d in dies.items():
            resul[(col, up, 'd')] = len(d)
        return resul

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_desc, ics_desc \
               from cat_centres where ep = '0208'"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, 'nodrizas')}

    def get_professionals(self):
        """."""
        sql = "select left(ide_numcol, 8), max(left(ide_dni, 8)), \
               max(ide_nom_usuari), max(ide_categ_prof_c) \
               from cat_pritb992 \
               where ide_categ_prof_c in {} \
               group by 1".format(categories)
        self.prof = {row[0]: row[1:] for row in u.getAll(sql, 'import')}

    def get_poblacio(self):
        """."""
        sql = "select distinct(left(ide_numcol, 8)), up, uab \
               from cat_professionals where uab <> '' and tipus = 'M'"
        self.ubas = c.defaultdict(set)
        for col, up, uba in u.getAll(sql, 'import'):
            self.ubas[(up, uba)].add(col)
        self.poblacio = c.Counter()
        sql = 'select up, uba from assignada_tot'
        if debug:
            sql += " where up = '00371'"
        for up, uba in u.getAll(sql, 'nodrizas'):
            if (up, uba) in self.ubas:
                cols = self.ubas[(up, uba)]
                for col in cols:
                    self.poblacio[(col, up)] += 1

    def get_resultat(self):
        """."""
        self.resultat = []
        fets = set()
        for pt in self.visites:
            for (col, up, tip), n in pt.items():
                if (col, up) not in fets and up in self.centres:
                    ambit, eap = self.centres[up]
                    dni, nom, categ = (self.prof[col] if col in self.prof
                                       else (None,) * 3)
                    categoria = (None if not categ
                                 else 'PED' if categ == '10888' else 'MF')
                    cupo = (self.poblacio[(col, up)]
                            if (col, up) in self.poblacio else None)
                    this = (col, dni, nom, categoria, up, cupo, ambit, eap,
                            pt[(col, up, 'd')],
                            pt[(col, up, '41')], pt[(col, up, '42')],
                            pt[(col, up, '43')], pt[(col, up, '44')])
                    self.resultat.append(this)
                    fets.add((col, up))

    def upload_table(self):
        """."""
        u.listToTable(self.resultat, tb, db)
        u.grantSelect(tb, 'PREDUMMP', db)


if __name__ == '__main__':
    Visites()
