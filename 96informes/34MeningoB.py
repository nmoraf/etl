# coding: iso-8859-1
# Carmen Cabezas,  (ion 7504), gener16, Ermengol
# Demana vacunes meningitis tipo B

from sisapUtils import *
import csv
import os
import sys
from time import strftime
from collections import defaultdict, Counter

printTime('Inici')

debug = False

meningoB = 'A00035'
anyCalcul = '2015'

imp = 'import'
db = 'test'
nod = 'nodrizas'
OutFile = tempFolder + 'vacunaMB.txt'
tableMy = 'MeningoB'

def edatMConverter(edatm, edata):

    if edatm < 6:
        return 'Lactants 0 - 5 mesos'
    elif 6 <= edatm <=11:
        return 'Lactants 6 - 11 mesos'
    elif 12 <= edatm <=23:
        return 'Lactants 12 - 23 mesos'
    elif 24 <= edatm and edata <=10:
        return 'Nens 2 - 10 anys'
    elif edata > 10:
        return 'Adolescents (>=11 anys) i adults'
    else:
        return 'No grup edat'
        
    
assig = {}
sql = 'select id_cip_sec,up, edat from assignada_tot where edat >14'
for id, up, edat in getAll(sql, nod):
    assig[id] = {'edat_a': edat, 'edat_m': 9999}
sql = 'select id_cip_sec,up, edat_a,edat_m from ped_assignada'
for id, up, edata,edatm in getAll(sql, nod):
    assig[id] = {'edat_a': edata, 'edat_m': edatm}
    

Antigen = {}
sql = "select vacuna from cat_prstb040_new where antigen='{0}'".format(meningoB)
for vac, in getAll(sql, imp):
    Antigen[vac] = True

execute("drop table if exists {}".format(tableMy), db)
execute("create table {} (id_cip_sec int, data date, periode varchar(20), edat varchar(300))".format(tableMy), db)

sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    vacunes = {}
    sql = "select id_cip_sec,va_u_cod,year(va_u_data_vac), date_format(va_u_data_vac, '%m'), va_u_data_vac from {0} where va_u_data_baixa=0 {1}".format(particio, ' limit 10' if debug else '')
    for id, vac, anyV, mesV, dat in getAll(sql, imp):
        try:
            Antigen[vac]
        except KeyError:
            continue
        try:
            edatanys = assig[id]['edat_a']
            edatmesos = assig[id]['edat_m']
        except KeyError:
            continue
        if str(anyV) == anyCalcul:
            grup_edat = edatMConverter(edatmesos, edatanys)
            periode = str(anyV) + str(mesV)
            vacunes[id, dat] = {'periode': periode, 'gedat': grup_edat}

    with openCSV(OutFile) as c:
        for (id, data), d in vacunes.items():
            c.writerow([id, data, d['periode'], d['gedat']])
    loadData(OutFile, tableMy, db)

print 'Dosis administrades per mes'
sql = "select periode, count(*) from {} group by periode".format(tableMy)
for periode, recompte in getAll(sql, db):
    print periode, recompte
print "Persones vacunades per grup d'edat"
sql = "select edat, count(distinct id_cip_sec) from {} group by edat".format(tableMy)
for edat, recompte in getAll(sql, db):
    print edat, recompte
    
printTime('Fi')