#Maria Jesus Pueyo (ion 3359), desembre14, Ermengol

from sisapUtils import *
import time,csv
from time import strftime

conn= connect('import')
cursor= conn.cursor()


dext="nodrizas.dextraccio"
cursor.execute("select date_add(date_add(data_ext, interval -1 year), interval +1 day),data_ext from %s" % dext)
for a,b in cursor:
    fa1any=a
    ara=b

path="03op.txt"
#######

print 'pob: ' + strftime("%Y-%m-%d %H:%M:%S")
poblacio,ambit,desc_eap= {},{},{}

cursor.execute("select distinct scs_codi,amb_desc,ics_desc from nodrizas.cat_centres where ep='0208'")
for up,a,d in cursor:
    ambit[up]= a
    desc_eap[up]=d
cursor.execute("select id_cip_sec,if(edat between 50 and 64,'Edat entre 50 i 64',if(edat between 65 and 74,'Edat entre 65 i 74',if(edat between 75 and 84,'Edat entre 75 i 84','85 i mes'))),sexe,up from nodrizas.assignada_tot where edat >49 ")
for id,edat,sexe,up in cursor:
    try:
        poblacio[int(id)]= {'edat':edat,'sexe':sexe,'ambit':ambit[up],'up':up,'desc':desc_eap[up]}
    except KeyError:
        pass

print 'pacients: ' + strftime("%Y-%m-%d %H:%M:%S")
pacients= {}
with open(path, 'rb') as file:
    d=csv.reader(file, delimiter='@', quotechar='|')
    for cat in d:
        dx,grup = cat[0],cat[1]
        cursor.execute("select id_cip_sec,pr_cod_ps from import.problemes where pr_cod_ps='%s' and pr_cod_o_ps='C' and pr_dde between '%s' and '%s' and pr_hist=1" %(dx,fa1any,ara))
        for key,ps in cursor:
            try:
                if (ps,poblacio[key]['ambit'],poblacio[key]['up'],poblacio[key]['desc'],poblacio[key]['edat'],poblacio[key]['sexe']) in pacients:
                    pacients[ps,poblacio[key]['ambit'],poblacio[key]['up'],poblacio[key]['desc'],poblacio[key]['edat'],poblacio[key]['sexe']]+= 1
                else:
                    pacients[ps,poblacio[key]['ambit'],poblacio[key]['up'],poblacio[key]['desc'],poblacio[key]['edat'],poblacio[key]['sexe']]=1
            except KeyError:
				continue

with open(tempFolder +'op.txt', 'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for key in pacients:
        ps,ambit,up,desc,edat,sexe= key
        w.writerow([ps,ambit,up,desc,edat,sexe,pacients[key]])


pacients.clear()        
poblacio.clear()


cursor.close()
conn.close()
print 'fi: ' + strftime("%Y-%m-%d %H:%M:%S")