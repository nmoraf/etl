#necessitem que a test hi hagi mst_corbes
from sisapUtils import *
from collections import defaultdict
from numpy import *
from CorbesUtils import *


def getEdatsAny(temps):
    edatsA = {}
    sql = "select {0},sexe from {1} where {0} between {2} and {3} group by {0},sexe".format(tempsDict[temps]['columna'],table,tempsDict[temps]['edatmin'],tempsDict[temps]['edatmax'])
    for edat_a, sexe in getAll(sql, test):
        edatsA[edat_a, sexe] = True
    return edatsA
    
def getValorsA(temps):
    nensA = defaultdict(list)
    sql = "select val, {0}, sexe, agrupador from {1}".format(tempsDict[temps]['columna'],table)
    for val, edat_a, sexe, agr in getAll(sql, test):
        if val != 0:
            nensA[(edat_a, sexe, agr)].append(val)
    return nensA
    
def getValueConvert(variable, temps, valor):
    valC = 0
    if temps == 'anys' and variable == 'TT102':
        if valor > 300:
            valC = 1
    if temps == 'anys' and variable == 'TT101':
        if valor < 10:
            valC = 1
    return valC


def getdades(temps, edatsA, nensA):
    dades = []
    for edats, sexe in edatsA:
        for variable in variables:
            agr = getAgrupador(variable)
            recomp = 0
            suma = 0
            conv = 0
            valors = nensA[(edats, sexe, variable)]
            try:
                p00001 = getPercentil(valors, 0.01)
                p0001 = getPercentil(valors, 0.05)
                p001 = getPercentil(valors, 0.1)
                p3 = getPercentil(valors, 3)
                p25 = getPercentil(valors, 25)
                p50 = getPercentil(valors, 50)
                p75 = getPercentil(valors, 75)
                p97 = getPercentil(valors, 97)
                p9999 = getPercentil(valors, 99.9)
                p99999 = getPercentil(valors, 99.95)
                p999999 = getPercentil(valors, 99.99)
                minim = round(min(valors), 2)
                maxim = round(max(valors), 2)
            except ValueError:
                continue
            for val in valors:
                recomp += 1
                suma += val
                valC = getValueConvert(variable, temps, val)
                conv += valC
            mitjana = round(suma/recomp, 2)
            dades.append([agr, temps, edats, sexe, minim, p00001, p0001, p001, p3, p25, p50, p75, p97, p9999, p99999, p999999, maxim, recomp, conv])
    return dades

   
printTime()

   
tableMy = 'percentils_corbes'
createTable(tableMy, '(variable varchar(10), unitat varchar(10), edats double, sexe varchar(1), minim double, p001 double, p005 double, p01 double, p3 double, p25 double, p50 double, \
        p75 double, p97 double, p999 double, p9995 double, p9999 double, maxim double, recompte double, conversio double)', test, rm=True)
for temps in tempsDict:
    edatsA = getEdatsAny(temps)
    nensA = getValorsA(temps)
    upload = getdades(temps, edatsA, nensA)    
    listToTable(upload, tableMy, test)
    
fitxer = []
 
sql = "select variable, unitat, edats, sexe, minim, p001, p005, p01, p3, p25, p50, p75, p97, p999, p9995, p9999, maxim, recompte from {} where unitat in ('anys', 'mesos', 'dies')".format(tableMy)
for variable, unitat, edats, sexe, minim, p001, p005, p01, p3, p25, p50, p75, p97, p999, p9995, p9999, maxim, recompte in getAll(sql, test): 
    fitxer.append([variable, unitat, edats, sexe, minim, p001, p005, p01, p3, p25, p50, p75, p97, p999, p9995, p9999, maxim, recompte])
 
try:
    remove(tempFolder + file_data)
except:
    pass  
    
writeCSV(tempFolder + file_data, fitxer, sep=';')

printTime()
