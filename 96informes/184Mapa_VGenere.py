# coding: iso-8859-1


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

anys = [2015, 2016, 2017, 2018]
mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']

codis = "('N80','N80.0','N80.1','N80.2','N80.3','N80.4','N80.5','N80.6','N80.8','N80.9')"


class Violencia(object):
    """."""
    
    def __init__(self):
        """."""
        self.dades = Counter()
        self.dext, = getOne("select data_ext from dextraccio", "nodrizas")
        self.get_descripcio()
        self.get_poblacio()
        self.get_vgenere()
        self.get_dades()
        self.get_prevalenca()
        self.export_data()        
        
        
    def get_descripcio(self):
        """."""
        sql = "select pst_th, pst_des_norm from cat_prstb305 where pst_cod='T74.9'"
        self.descripcionsTH = {th: des for (th, des) in getAll(sql, 'import')}
    
    def get_poblacio(self):
        """Agafem sexe i edat de import assignada"""
        self.pob = {}
        sql = "select id_cip_sec, usua_sexe, year(usua_data_naixement) from assignada where usua_sexe='D'"
        for id, sexe, naix in getAll(sql, "import"):
            self.pob[id] = {'sexe':sexe, 'naix': int(naix)}
            
    def get_vgenere(self):
        """."""
        self.casos=defaultdict(lambda: defaultdict(set))
        self.prevalents = set()
        sql="select id_cip_sec,date_format(pr_dde,'%Y%m'),pr_cod_ps, pr_th, pr_dde, if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) from problemes, nodrizas.dextraccio \
            where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0 and pr_cod_ps ='T74.9' and pr_th in ('5481', '5482')".format(codis)
        for id, dde, ps, th, inici, tancat in getAll(sql, "import"):
            desc = self.descripcionsTH[th]
            self.casos[int(dde)][desc].add(id)
            if inici <= self.dext and tancat == 0:
                self.prevalents.add(id)
        
    def get_dades(self):
        """.""" 
        for anyc in anys:
            for mes in mesos:
                periode = int(str(anyc) + mes)
                if periode in self.casos:
                    for ps in self.casos[periode]:
                        for id in self.casos[periode][ps]:
                            if id in self.pob:
                                anynaix = self.pob[id]['naix'] 
                                edat = int(str(periode)[:4]) - int(anynaix) 
                                self.dades[(periode, ps, ageConverter(edat,5))] += 1
                            
    def get_prevalenca(self):
        """."""
        self.prevalenca = Counter()
        sql = "select id_cip_sec, edat, sexe from assignada_tot where ates = 1 and sexe = 'D'"
        for id, edat, sexe in getAll(sql, "nodrizas"):
            ed, sex = ageConverter(edat, 5), sexConverter(sexe)
            self.prevalenca[(ed, 'den')] += 1
            if id in self.prevalents:
                self.prevalenca[(ed, 'num')] += 1
                
            
    def export_data(self):
        """."""
        rec = []
        for (periode, ps, edat), n in self.dades.items():
            rec.append([periode, ps, edat, n])
        writeCSV(tempFolder + 'casos_violencia.txt', rec)
        
        rec = []
        for (edat, tipus), n in self.prevalenca.items():
            if tipus == 'den':
                num = self.prevalenca[(edat, 'num')]
                rec.append([edat, num, n])
        writeCSV(tempFolder + 'prevalenca_violencia.txt', rec)
        
if __name__ == '__main__':
    Violencia()