# coding: utf8

"""
Procés per explorar les dades dels paciens institucionalitzats
"""

import urllib as w
import os

import sisapUtils as u

virtuals_fins = '20180531'

test_dict = {87: 'Barthel', 88: 'Pfeiffer', 94: 'Braden', 'TUGT': 'caigudes',
            'VT4001': 'MNA_crib', 'VT4002': 'MNA_ava', 'VMEDG': 'Yessavage','VMEAN': 'Goldberg'}

class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dext, self.dext1a = u.getOne("select date_format(data_ext,'%Y%m%d'), date_format(date_add(date_add(data_ext, interval - 1 year),interval + 1 day), '%Y%m%d') from dextraccio", "nodrizas")
        self.get_centres()
        self.get_numcol()
        self.get_poblacio()
        self.get_test()
        self.get_hash()
        self.get_gma()
        self.get_medea()
        self.get_visites()
        self.get_virtuals()
        self.export_dades_pacient()


    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_numcol(self):
        """Agafem numcols de mf"""
        
        self.numcols = {}
        self.numsllocs = {}
        
        sql = "select codi_sector, lloc_codi_lloc_de_treball, lloc_numcol, lloc_especialit_codi_especiali from cat_pritb025 where lloc_especialit_codi_especiali in ('10999', '10117')"
        for sector, lloc, numcol, espe in u.getAll(sql, "import"):
            self.numsllocs[lloc, sector] = numcol
        
        sql = " select codi_sector, uab_codi_up,uab_codi_uab,uab_lloc_de_tr_codi_lloc_de_tr from import.cat_vistb039 where uab_servei_codi_servei='MG'"
        for sector, up, uba, lloctr in u.getAll(sql, "import"):
            if up in self.centres:
                if (lloctr, sector) in self.numsllocs:
                    numcol = self.numsllocs[lloctr, sector]
                    self.numcols[sector, numcol] = {'up': up, 'uba': uba}
              
    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tacament
           - majors de 14 anys a l'inici del període
        """
        self.dades = {}
        self.ambulatoris = set()
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, codi_sector, edat, sexe, \
                       up_residencia, nacionalitat, nivell_cobertura, \
                       up, centre_codi, centre_classe, uba \
                from assignada_tot \
                where edat > 14 and institucionalitzat = 1", "nodrizas")
        for id, sec, edt, sex, residencia, nac, cob, up, cod, cla, uba in u.getAll(*sql):
            if up in self.centres:
                pensio = 1 if cob == 'PN' else 0
                self.dades[id] = {
                              'hash': None, 'sector': sec, 'edat': edt,
                              'sex': sex, 'gma_cod': None, 'gma_cmplx': None,
                              'gma_num': None, 'medea': None, 'residencia': residencia,
                              'renta': nac in renta, 
                              'pensio': pensio, 'up': up, 'c_codi': cod,
                              'c_classe': cla, 'uba': uba, 'cp': None, '9C': 0, '9D': 0,
                              '9T': 0, '9R': 0, '9E':0, 'C': 0, 'D': 0, 'V': 0, 
                              'Barthel': 0, 'Pfeiffer':0, 'Braden': 0, 'caigudes': 0,
                              'MNA_crib': 0, 'MNA_ava': 0, 'Yessavage': 0, 'Goldberg': 0}

    def get_test(self):
        """Busquem els tests"""
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if dat == 2018 or dat== 2017:
                print table
                sql = "select id_cip_sec, date_format(vu_dat_act,'%Y%m%d'), vu_cod_vs from {} where vu_cod_vs in ('TUGT', 'VT4001', 'VT4002', 'VMEDG', 'VMEAN')".format(table)
                for id, dat, var in u.getAll(sql, "import"):
                    if id in self.dades:
                        if self.dext1a <= dat <= self.dext:
                            test_desc = test_dict[var]
                            self.dades[id][test_desc] = 1
                            
        sql = "select id_cip_sec, date_format(data_var,'%Y%m%d'), agrupador from eqa_variables where agrupador in (87,88,94)"
        for id, dat, var in u.getAll(sql, "nodrizas"):
            if id in self.dades:
                if self.dext1a <= dat <= self.dext:
                    test_desc = test_dict[var]
                    self.dades[id][test_desc] = 1
    
    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        for id, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash

    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]



    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        """
        sql = ("select id_cip_sec, visi_up, visi_tipus_visita, \
                       visi_lloc_visita, date_format(visi_data_visita,'%Y%m%d') \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                     visi_servei_codi_servei = 'MG'", "import")
        for id, up, tipus, lloc, dat in u.getAll(*sql):
            if  self.dext1a <= dat <= self.dext:
                if id in self.dades and up in self.centres:
                    if tipus in ('9C', '9D', '9T', '9R', '9E'):
                        self.dades[id][tipus] += 1
                    elif lloc in ('C', 'D'):
                        self.dades[id][lloc] += 1

    def get_virtuals(self):
        """
        Agafem les visites virtuals fins a finals de maig quan les vvirtuals van passar a ser una altra cosa
        amb l'entrada de la 9E
        """
        sql = ("select codi_sector, id_cip_sec, vvi_num_col\
                from vvirtuals \
                where vvi_situacio='R' and \
                      vvi_data_alta between '{}' and '{}'".format(self.dext1a, virtuals_fins),
               "import")
        for sector, id, numcol in u.getAll(*sql):
            if (sector, numcol) in self.numcols:
                up, uba = self.numcols[sector, numcol]['up'], self.numcols[sector, numcol]['uba']
                if id in self.dades:
                    up2, uba2 = self.dades[id]['up'], self.dades[id]['uba']
                    if up == up2 and uba == uba2:
                        self.dades[id]['V'] += 1    

    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules."""
        """Mètode per crear i omplir taules."""
        table = 'SISAP_residencies_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "redics"
        u.createTable(table, columns_str, db, rm=True)
        u.grantSelect(table, ('PREDUMMP', 'PREDUPRP',
                              'PREDUECR'), db)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        dades = [(d['hash'], d['edat'], d['sex'], d['gma_cod'], d['gma_cmplx'],
                  d['gma_num'], d['medea'],  d['renta'],
                  d['up'], d['uba'], d['residencia'],
                  d['9C'], d['9D'], d['9T'], d['9R'], d['9E'] ,d['C'], d['D'], d['V'],
                  d['Barthel'], d['Pfeiffer'], d['Braden'], d['caigudes'], d['MNA_crib'],
                  d['MNA_ava'], d['Yessavage'], d['Goldberg'])
                 for id, d in self.dades.items()]
        columns = ('id varchar2(40)', 'edat number', 'sexe varchar2(1)',
                   'gma_codi varchar2(3)', 'gma_complexitat number',
                   'gma_num_croniques number', 'medea_seccio number',
                    'immigracio_baixa_renta number',
                   'up varchar2(5)','uba varchar2(5)','residencia varchar2(5)',
                    'visites_9c number','visites_9d number', 'visites_9t number',
                   'visites_9r number', 'visites_9e number', 'v_sequencials_centre number',
                   'v_sequencials_domi number', 'visites_virtuals number',
                   'Barthel number', 'Pfeiffer number', 'Braden_nafres number', 'risc_caigudes number',
                   'MNA_cribratge number', 'MNA_avaluacio number', 'Yessavage_depressio number', 'Goldberg_ansietat number')
        Frequentacio.export_dades("PACIENT", columns, dades)


if __name__ == '__main__':
    Frequentacio()
