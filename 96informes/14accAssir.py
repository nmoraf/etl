# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'altres'
nod = "nodrizas"
path = ecapFolder

sql="select concat(date_format(date_add(data_ext,interval + 1 month),'%m'),date_format(data_ext,'%y'),'.txt')  from dextraccio"
for d in getOne(sql, nod):
    dcalcul=d
    
centres = {}
sql = "select up,br_assir from ass_centres"
for up,br in getAll(sql,nod):
    centres[up] = {'br':br}

printTime('Inici')

isFile = []
accAssir = {}
for file in os.listdir(path):
    if  file.startswith("demora_assir_trans") and file.endswith(dcalcul):
        isFile.append('yes')
        pathFile = path + file
        with open(pathFile, 'rb') as fitxer:
            p=csv.reader(fitxer, delimiter=';')
            for ind in p:
                sector,up,desc,cen,classe,nom,servei,dservei,mod,desmod,visita,dies,n = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8],ind[9],ind[10],ind[11],ind[12]
                try:
                    br = centres[up]['br']
                except KeyError:
                    continue
                if dies == '-':
                    dies = 0
                else:
                    dies=int(dies)
                if n == '-':
                    n = 0
                else:
                    n=int(n)    
                if (br,visita) in accAssir:
                    accAssir[(br,visita)]['num1'] += dies
                    accAssir[(br,visita)]['den'] += 1
                    accAssir[(br,visita)]['num2'] += n
                else:
                    accAssir[(br,visita)] = {'num1':dies,'num2':n,'den':1}
taula = "exp_khalix_acc_assir" 
OutFile = tempFolder + taula + '.txt'
with openCSV(OutFile) as c:
    for (br,visita),d in accAssir.items():
        c.writerow([br,visita,d['num1'],d['num2'],d['den']])
execute('drop table if exists {}'.format(taula),db)
execute("create table {} (up VARCHAR(5) NOT NULL DEFAULT '',visita VARCHAR(10) NOT NULL DEFAULT '',  num1 int, num2 int,den int)".format(taula),db)
loadData(OutFile,taula,db)

error = []

sql = "select up,'ACCASS1',concat('T',visita),'NUM', num1 from {0}.{1} union select up, 'ACCASS1',concat('T',visita),'DEN',den from {0}.{1}\
        union select up,'ACCASS2',concat('T',visita),'NUM', num2 from {0}.{1} union select up, 'ACCASS2',concat('T',visita),'DEN',den from {0}.{1}".format(db,taula)
file = 'ACC_ASSIR'
error.append(exportKhalix(sql,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

if any(isFile):
    pass
else:
    text = "No he trobat al FTP el fitxer d'aquest mes de l'accessibilitat de l'assir"
    sendSISAP('ecomaredon@gencat.cat','No hi ha fitxer acc assir','Ermengol',text)

printTime('Fi')