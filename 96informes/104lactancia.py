# -*- coding: utf8 -*-

"""
.
"""

import sisapUtils as u


class Lactancia(object):
    """."""

    def __init__(self):
        """."""
        self.get_denominador()
        self.get_poblacio()
        self.get_lactancia()
        self.upload_data()

    def get_denominador(self):
        """."""
        sql = "select id_cip_sec from mst_indicadors_pacient \
               where indicador = 'EQA0706' and ates = 1 and excl = 0"
        self.denominador = set([id for id, in u.getAll(sql, 'pedia')])

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, data_naix \
               from ped_assignada \
               where edat_a < 2"
        self.poblacio = {id: naix for (id, naix) in u.getAll(sql, 'nodrizas')
                         if id in self.denominador}

    def get_lactancia(self):
        """."""
        sql = "select id_cip_sec, val_var, val_val, \
                      val_data, val_d_v, val_data_mod \
               from {} \
               where val_var in ('PEPAL01', 'PENDE051', 'PENDE052') and \
                     val_hist = 1"
        self.lactancia = []
        for t in ('nen11', 'nen12'):
            for (id, var, val,
                 dvar, dvis, dmod) in u.getAll(sql.format(t), 'import'):
                if id in self.poblacio:
                    dnaix = self.poblacio[id]
                    this = (id, dnaix, var, val,
                            dvar, u.monthsBetween(dnaix, dvar),
                            dvis, u.monthsBetween(dnaix, dvis),
                            dmod, u.monthsBetween(dnaix, dmod))
                    self.lactancia.append(this)

    def upload_data(self):
        """."""
        tb = 'lactancia'
        db = 'test'
        u.createTable(tb, '(id int, dnaix date, var varchar(8), val int, \
                            dvar date, edat_dvar int, \
                            dvis date, edat_dvis int, \
                            dmod date, edat_dmod int)', db, rm=True)
        u.listToTable(self.lactancia, tb, db)


if __name__ == '__main__':
    Lactancia()
