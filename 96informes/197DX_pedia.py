# coding: iso-8859-1
import collections as c
import sys,datetime
from time import strftime

import sisapUtils as u

imp = 'import' 


class cim10(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_cim10()
        self.get_thesaurus()
        self.get_ciap()
        self.get_centres()
        self.get_pob()
        self.get_recomptes()
        self.export_data()


    def get_cim10(self):
        """."""
        self.cim10desc = {}
        sql = 'select ps_cod, ps_des from cat_prstb001'
        for cod, des in u.getAll(sql, imp):
            self.cim10desc[cod] = des
   
    def get_thesaurus(self):
        """."""    
        self.thesaurusDesc = {}
        sql = 'select pst_th, pst_des_norm from cat_prstb305'
        for cod, des in u.getAll(sql, imp):
                self.thesaurusDesc[cod] = des
    
    def get_ciap(self):
        """."""
        self.ciap2 = {}
        sql = 'select codi_cim10,codi_ciap,desc_ciap from cat_md_ct_cim10_ciap'
        for cim, ciap, desc in u.getAll(sql, 'import'):
            self.ciap2[cim] = {'ciap': ciap, 'desc': desc}
            
    
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}
    
    def get_pob(self):
        """."""
        self.pob={}
        
        sql = "select id_cip_sec, up from assignada_tot where edat <15"
        for id, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                self.pob[id] = True

    def get_recomptes(self):
        """Recomptes per cim10 i thesaurus combinats"""
        self.prevalenca = c.Counter()
        sql = "select id_cip_sec, pr_cod_ps, pr_th, date_format(pr_dde,'%Y%m%d'), pr_dde, date_format(pr_dba, '%Y%m%d')\
                ,if(pr_dde > date_add(data_ext, interval -1 year), 1, 0) \
                ,if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) \
                from problemes, nodrizas.dextraccio \
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa=0 or pr_data_baixa > data_ext)"
        for id, cim10, th, dde, pr_dde, dba, incident, tancat in u.getAll(sql, imp):
            if id in self.pob:
                descCim10 = self.cim10desc[cim10] if cim10 in self.cim10desc else ''
                descTh = self.thesaurusDesc[th] if th in self.thesaurusDesc else ''
                if cim10 in self.ciap2:
                    codiciap = self.ciap2[cim10]['ciap']
                    descciap = self.ciap2[cim10]['desc']
                    if tancat == 0:
                        self.prevalenca[(cim10, th,codiciap, descCim10, descTh, descciap)] += 1

    def export_data(self):
        """."""
        upload = []
        for (cim10, th,codiciap, descCim10, descTh, descciap), vals in self.prevalenca.items():
            upload.append([cim10, th,codiciap, descCim10, descTh, descciap, vals])
        
        u.writeCSV(u.tempFolder + 'Diagnòstics_pedia.txt', upload)

if __name__ == '__main__':
    cim10()