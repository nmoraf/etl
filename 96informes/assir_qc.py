import sisapUtils as u
import pandas as pd

cols = []
# sql = "select columns from all_tab_columns where table_name='PRSTB241'"
# cols.append

cols = u.getTableColumns('PRSTB241', '6844')
cols = ['sector'] + cols

print (cols)
rows = []
for sector in u.sectors:
    sql = 'select {}, p.* from prstb241 p'.format(sector)
    for row in u.getAll(sql,sector):
        rows.append(row)

print(len(rows))
print(len(rows[0]))


ccc = pd.DataFrame(rows, columns = cols)

cols = [x for x in u.getTableColumns('cat_pritb008','import')]
print(cols)

print(len(ccc))


resources = {
    'visites': {
        'sourceDb': 'import',
        'sourceTbNAme': 'cat_vistb039',
        'sourceFlt': '',
        'sourceIdx': ['id_cip'],
        'sourceAttributes': ['CIP', 'DATA'],
        'encodig': ['CIP']
        },
}


# import.assignada tiene por cada paciente: up_uba, centre_uba, clase_uba, abs_paciente, localitat_paciente
# responsable corporativa de assir tiene mapeo up_eap-> centre_eap -> up_assir -> desc_centre_assir (punt_atenció)
# se deberia poder mapear población a partir de up_eap -> up_assir
# se deberia poder mapear población a partir de up_uba|centre_uba+clase_uba|abs|abs+localitat -> centre_assir -> up_assir 

# centres de les UPs ASSIR
# listado de 41 ups sacado de ass.exp_khalix_up_ind
'''
select CENT_CODI_UP, cent_codi_centre, cent_classe_centre, group_concat(distinct CENT_NOM_CENTRE)
from import.cat_pritb010
where cent_codi_up in
('00034','00075','00076','00232','00233','00234','00250','00251','00252','00253'
,'00254','00318','00319','00406','00407','00408','00409','00410','00411','00412'
,'00530','01094','01272','01314','01315','01405','01412','01413','01415','01422'
,'01695','01884','04093','04094','04390','05916','06754','07073','07262','07263'
,'07733') and cent_data_baixa > now()
group by CENT_CODI_UP, cent_codi_centre, cent_classe_centre;
'''

# catalogo sectorizado de UPs ASSIR
'''
select *
from import.cat_pritb008
where u_pr_cod_up  in
('00034','00075','00076','00232','00233','00234','00250','00251','00252','00253'
,'00254','00318','00319','00406','00407','00408','00409','00410','00411','00412'
,'00530','01094','01272','01314','01315','01405','01412','01413','01415','01422'
,'01695','01884','04093','04094','04390','05916','06754','07073','07262','07263'
,'07733')
group by u_pr_cod_up
;
'''