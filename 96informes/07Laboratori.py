# coding: iso-8859-1
#Laboratori
#Manolo demana unes quantes dades

from sisapUtils import *
import csv,os,sys
from time import strftime

debug=False

db="zaltres"
nod="znodrizas"
imp="zimport"

lab="lab_anual"
laburv="cat_sisap_catlab_urv"

OutFile= tempFolder + 'paclab.txt'
OutFile2= tempFolder + 'prolab.txt'
OutFile3= tempFolder + 'urvlab.txt'

periodes= [1,2,3,4,5,6,7,8,9,10,11,12]

urv={}
sql="select cr_codi_prova_ics,agrupador,urv from {}".format(laburv)
for prova,agr,u in getAll(sql,imp):
    urv[prova] = {'gp':agr,'urv':u}
printTime('inici pob')     
def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,edat from assignada_tot'
    for id_cip_sec,up,edat in getAll(sql,nod):
         assig[int(id_cip_sec)]= {'up':up,'edat':int(edat)}
    return assig 

ass = getPacients()

table="labpacanual" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int, mes double,n double)".format(table)
execute(sql,db)
table="labproanual" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int,  mes double,n double)".format(table)
execute(sql,db)
table="laburvanual" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int,  mes double,n double)".format(table)
execute(sql,db)

printTime('inici calcul') 
for a in periodes:
    paclab={}    
    sql="select id_cip_sec,cr_codi_lab,cr_codi_prova_ics,cr_data_reg, codi_up from {0} where month(cr_data_reg)={1} {2}".format(lab,a,' limit 10' if debug else '')
    for id_cip_sec,clab,cprova,data,uplab in getAll(sql,nod):
        id_cip_sec=int(id_cip_sec)
        try:
            grup=urv[cprova]['gp']
            urvs=urv[cprova]['urv']
        except KeyError:
            ok=1
        if (id_cip_sec,data,grup,urvs,uplab) in paclab:
            ok=1
        else:
            try:
                paclab[(id_cip_sec,data,grup,urvs,uplab)]=1
            except KeyError:
                ok=1
    pacients,plab,purv={},{},{}
    for (id_cip_sec,data,grup,urvs,uplab),d in paclab.items():    
        try:
            up=ass[id_cip_sec]['up']
            edat=ass[id_cip_sec]['edat']
        except KeyError:
            continue
        if (id_cip_sec,data,uplab,up,edat) in pacients:
            ok=1
        else:
            try:
                pacients[(id_cip_sec,data,uplab,up,edat)]=1
            except KeyError:
                ok=1
        if (uplab,up,edat) in plab:
            plab[(uplab,up,edat)]+=1
        else:
            try:
                plab[(uplab,up,edat)]=1
            except KeyError:
                ok=1
        if (uplab,up,edat) in purv:
            purv[(uplab,up,edat)]+=urvs
        else:
            try:
                purv[(uplab,up,edat)]=urvs
            except KeyError:
                ok=1
    paclab.clear()
    paclabs={}
    for (id_cip_sec,data,uplab,up,edat),d in pacients.items():
        if (uplab,up,edat) in paclabs:
            paclabs[(uplab,up,edat)]+=1
        else:
            try:
                paclabs[(uplab,up,edat)]=1
            except KeyError:
                ok=1
    with open(OutFile,'wb') as file:
            w= csv.writer(file, delimiter='@', quotechar='|')
            for (ul,u,e),d in paclabs.items():
                try:
                   w.writerow([ul,u,e,a,d])
                except KeyError:
                    continue
    table="labpac1" 
    sql= 'drop table if exists {}'.format(table)
    execute(sql,db)
    sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int,  mes double,n double)".format(table)
    execute(sql,db)
    loadData(OutFile,table,db)
    paclabs.clear()   
    pacients.clear()   
    with open(OutFile2,'wb') as file:
            w= csv.writer(file, delimiter='@', quotechar='|')
            for (ul,u,e),d in plab.items():
                try:
                    w.writerow([ul,u,e,a,d])
                except KeyError:
                    continue
    table="labpro1" 
    sql= 'drop table if exists {}'.format(table)
    execute(sql,db)
    sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int,  mes double,n double)".format(table)
    execute(sql,db)
    loadData(OutFile2,table,db)
    plab.clear() 
    with open(OutFile3,'wb') as file:
            w= csv.writer(file, delimiter='@', quotechar='|')
            for (ul,u,e),d in purv.items():
                try:
                    w.writerow([ul,u,e,a,d])
                except KeyError:
                    continue
    table="laburv1" 
    sql= 'drop table if exists {}'.format(table)
    execute(sql,db)
    sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int,  mes double,n double)".format(table)
    execute(sql,db)
    loadData(OutFile3,table,db)
    purv.clear() 
    sql="insert into labpacanual select * from labpac1"
    execute(sql,db)
    sql="insert into labproanual select * from labpro1"
    execute(sql,db)
    sql="insert into laburvanual select * from laburv1"
    execute(sql,db)
    

table="labpac" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int, n double)".format(table)
execute(sql,db)
sql= "insert into {} select uplab,up,edat,sum(n) from labpacanual group by uplab,up,edat".format(table)
execute(sql,db)
table="labpro" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int, n double)".format(table)
execute(sql,db)
sql= "insert into {} select uplab,up,edat,sum(n) from labproanual group by uplab,up,edat".format(table)
execute(sql,db)
table="laburv" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (uplab varchar(5) not null default'',up varchar(5) not null default'',edat int, n double)".format(table)
execute(sql,db)
sql= "insert into {} select uplab,up,edat,round(sum(n),2) from laburvanual group by uplab,up,edat".format(table)
execute(sql,db)

with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    sql="select * from labpac"
    for uplab,up,edat,n in getAll(sql,db):
        n=int(n)
        w.writerow([uplab,up,edat,n])

with open(OutFile2,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    sql="select * from labpro"
    for uplab,up,edat,n in getAll(sql,db):
        n=int(n)
        w.writerow([uplab,up,edat,n])      

with open(OutFile3,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    sql="select * from laburv"
    for uplab,up,edat,n in getAll(sql,db):
        w.writerow([uplab,up,edat,n])          

printTime('fi calcul')     

