# coding: iso-8859-1
#Carmen Cabezas,  (ion 3490, 7619), febrer16, Ermengol
#Lluis Urbuzondo,  (ion 7765), marc16, Ermengol
#omplir excel
from sisapUtils import *
import csv,os,sys
from time import strftime

db = "pedia"
table = "mst_vacsist"

def getPerc(num, den):
    percentatge = round((num/den) * 100, 2)
    return percentatge
    
def getVariable(vacuna):
    if vacuna == '1po':
        return po, po1
    elif vacuna == '2dtp':
        return dtp, dtp1
    elif vacuna == '3hib':
        return hib, hib1
    elif vacuna == '5mcc':
        return mcc, mcc1
    elif vacuna == '4vhb':
        return vhb, vhb1
    elif vacuna == '6varice':
        return varice, varice1
    elif vacuna == '7xrp':
        return xrp, xrp1
    elif vacuna == '8vha':
        return vha, vha1
        
def getNames(vacuna):
    if vacuna == '1po':
        return 'Polio: '
    elif vacuna == '2dtp':
        return 'DTPa: '
    elif vacuna == '3hib':
        return 'Hib: '
    elif vacuna == '5mcc':
        return 'Meningitis C: '
    elif vacuna == '4vhb':
        return 'Hepatitis B: '
    elif vacuna == '6varice':
        return 'Varicel·la: '
    elif vacuna == '7xrp':
        return 'Triple vírica: '
    elif vacuna == '8vha':
        return 'VHA: '
    
sql = "select {0}, count(*) from {1} a where ates=1 and {2}"

parametres = {'0any' : {'edat': 'edat_a = 0', 'text': 'Primovacunació: nens vacunats de 0 a 1 any', 'vacunes': {'1po', '2dtp', '3hib', '4vhb', '5mcc'}},
              '1any': {'edat': 'edat_a = 1', 'text': 'Vacunació reforç: nens vacunats de 1 a 2 anys','vacunes': {'1po', '2dtp', '3hib', '5mcc', '6varice','7xrp', '8vha'}},
              '6any': {'edat': 'edat_a = 6', 'text': 'Vacunació reforç: nens vacunats de 6 anys','vacunes': {'2dtp','8vha'}},
              '7any': {'edat': 'edat_a = 7', 'text': 'Vacunació reforç: nens vacunats de 7 anys','vacunes': {'2dtp','8vha'}},
              '14any': {'edat': 'edat_a = 14', 'text': 'Vacunació adolescents: nens 14 anys','vacunes': {'2dtp'}},
              '10any': {'edat': 'edat_a between 10 and 14', 'text': 'Vacunació adolescents: nens entre 10 i 14 anys','vacunes': {'5mcc','6varice'}},
              '3_4anys': {'edat': 'edat_a between 3 and 4', 'text': 'Cobertura de vacunación de triple vírica 3-4 anys','vacunes': {'6varice','7xrp'}},               
               }
                     
select = "sum(PO), sum(DTP), sum(HIB),sum(MCC), sum(vhb), sum(varice), sum(xrp), sum(vha)"
for parametre in parametres:
    for po, dtp, hib, mcc, vhb, varice, xrp, vha, nens in getAll(sql.format(select, table, parametres[parametre]['edat']), db):
        po1 = getPerc(po, nens)
        dtp1 = getPerc(dtp, nens)
        hib1 = getPerc(hib, nens)
        mcc1 = getPerc(mcc, nens)
        vhb1 = getPerc(vhb, nens)
        varice1 = getPerc(varice, nens)
        xrp1 = getPerc(xrp, nens)
        vha1 = getPerc(vha, nens)
        print parametres[parametre]['text']
        for vacuna in sorted(parametres[parametre]['vacunes']):
            vac, vac1 = getVariable(vacuna)
            textvac = getNames(vacuna)
            print textvac, nens, vac, vac1
