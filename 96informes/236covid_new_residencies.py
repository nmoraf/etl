import sisapUtils as u
from datetime import datetime as d
from dateutil import relativedelta
import collections as c
import numpy as np
import statistics as st
import pandas as pd


# VARS
RUPS_FILE = 'rups/ruprs110_29683857_original.csv'
rups_file = u.tempFolder + RUPS_FILE

PAQUI, LEO = "PREDUPRP", "PREDULMB"
# WRITERS = ("PREDUPRP", "PREDULMB")
WATCHERS = ("PREDULMB", "PREDUPRP", "PREDUMMP")
DB_DESTI = 'redics'

# FUNCTIONS
def birthday(date):
    # Get the current date
    now = d.utcnow()
    now = now.date()
    # Get the difference between the current date and the birthday
    age = relativedelta.relativedelta(now, date)
    age = age.years
    return age

# ACTUALITZACIO DIARIA
# [DIARI: 0: como mantener los que desaparezcan sin a�adir basura? (ug_dba y gu_dba )]
# paso a usar el de francesc de validacion?: SISAP_COVID_RES_VALIDACIO
""" Cataleg de grups d'usuaris """

tb_desti = 'SISAP_CAT_GRUPSUSUARIS'
cols = ['sector_cod varchar2(4)',
        'grup_cod NUMBER(6,0)',
        'grup_des VARCHAR2(30)',
        'grup_up varchar2(5)',
        'grup_n NUMBER',
        'grup_edat NUMBER',
        'eap1 varchar2(5)',
        'eap1_n NUMBER',
        'eaps NUMBER',
        ]

sql = """
    select
      '{}',
      cip_usuari_cip,
      ug_codi_grup,
      gu_up_residencia,
      gu_desc,
      to_date(usua_data_naixement, 'J'),
      usua_uab_up
    from ppftb012,
      ppftb011,
      usutb011,
      usutb040
    where
      ug_usua_cip = cip_cip_anterior
      and ug_codi_grup = gu_codi_grup
      and usua_cip = cip_usuari_cip
      and ug_dba is null
      and gu_dba is null
    """

# sectors = ['6838',]

grups = c.defaultdict(list)
for sector in u.sectors:
    print(sector)
    for sec, cip, grup, grup_up, grup_des, dnaix, eap in u.getAll(sql.format(sector), sector + 'sb'):
        edat = birthday(dnaix)
        # eap = 'NaN' if eap is None else eap 
        grups[(sec, grup, grup_des, grup_up), 'edats'].append(edat)
        grups[(sec, grup, grup_des, grup_up), 'eaps'].append(eap)

grups_agr = c.defaultdict(dict)
for k in grups:
    if k[1] == 'edats':
      grups_agr[k[0]]["grup_n"] =len(grups[k])
      grups_agr[k[0]]["grup_edat"] = int(np.mean(grups[k]))
    elif k[1] == 'eaps':
      grups_agr[k[0]]["eaps"] = len(set(grups[k]))
      try: 
        grups_agr[k[0]]["eap1"] = eap1 = st.mode(grups[k])
        grups_agr[k[0]]["eap1_n"] = len([eap for eap in grups[k] if eap == eap1])
      except st.StatisticsError:
        grups_agr[k[0]]["eap1"] = None
        grups_agr[k[0]]["eap1_n"] = None
        continue

      # grups_agr[k[0]]["eap1_n"] =

upload = []  
for k, v in grups_agr.items():
    sec, grup, grup_des, grup_up = k[0], k[1], k[2], k[3]
    grup_n = v["grup_n"]
    grup_edat = v["grup_edat"]
    eap1 = v["eap1"]
    eap1_n = v["eap1_n"]
    eaps = v["eaps"]
    upload.append((sec, grup, grup_des, grup_up, grup_n, grup_edat, eap1, eap1_n, eaps))

u.createTable(tb_desti, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.listToTable(upload, tb_desti, DB_DESTI)
u.grantSelect(tb_desti, WATCHERS, DB_DESTI)

# JUST ONCE
## tabla de mapeos (vacia) """

tb_desti = 'SISAP_MAP_GRUP_RUP'

# a�adir aqui source_code_type, source_code, target_code
# source_code_type: 'user_grups', source_code: '6844-133', target_code_type: 'RUP', target_code: '02707'
# source_code_type: 'up_residencia', source_code: '02707', target_code_type: 'RUP', target_code: '02707'

# necesito TABLA!!!! de _CODE_TYPE: (up_residencia, RUP, user_grups, hotels)
# necesito TABLA!!!! de source_code y target_code (CODE_TYPE, CODE, DESC) DESC puede ser el descriptor de user_grups, de hotels, de up_residencia o de RUP
cols = ['sec varchar2(4)',
        'grup varchar2(8)',
        'sisap_class NUMBER',
        'src_code_type VARCHAR(10)',  # ej: UP_RESIDENCIA, SEC_GRUP (paqui llama GRUP_USUARIS a grup_up y SISAP_CAT_GRUP a rup_up)
        'src_code  VARCHAR(13)', 
        'target_code_type VARCHAR(10)', #ej: RUP
        'target_code VARCHAR(10)',
        'sisap_grup_desc VARCHAR(50)',  # Nuevo toponimo sisap.
        'up_rup varchar2(5)',           # agregador siempre a RUP
        ]

u.createTable(tb_desti, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
u.execute("grant all on {} to {}".format(tb_desti, PAQUI), DB_DESTI)
u.execute("grant all on {} to {}".format(tb_desti, LEO), DB_DESTI)

xlsx_paqui = "grups_usuaris_revisats.xlsx" 
grups_revisats = pd.read_excel(u.tempFolder + xlsx_paqui)

col_names = ["SEC", "GRUP", "INCLOS_2", "src_code_type", "src_code"]
extra_names = ["target_code_type", "target_code", "sisap_grup_desc", "up_rup"]
grups_revisats_curated = grups_revisats[col_names].copy()

replacements = {"GRUP_USUARIS":"grup_up", "SISAP_CAT_RUP":"rup_up", "sisap_cat_rup":"rup_up", "SEC_GRUP":"sec_grup"}
grups_revisats_curated.replace(replacements, inplace = True)
grups_revisats_curated["SEC"] = grups_revisats_curated["SEC"].astype(str)
grups_revisats_curated["src_code"] = grups_revisats_curated["src_code"].astype(str)
grups_revisats_curated["target_code_type"] = None
grups_revisats_curated["target_code"] = None
grups_revisats_curated["sisap_grup_desc"]  = None
grups_revisats_curated["up_rup"]  = None

grups_revisats_curated2 = grups_revisats_curated.copy()

grups_revisats_curated2["src_code"][grups_revisats_curated2["src_code_type"] == "rup_up"] = grups_revisats_curated["src_code"].str.zfill(5)[grups_revisats_curated["src_code_type"] == "rup_up"]


grups_revisats_curated2[grups_revisats_curated2["src_code_type"] == "rup_up"]


dflist = list(grups_revisats_curated[col_names + extra_names].itertuples(index= False, name=None))
u.listToTable(dflist, tb_desti, DB_DESTI)


u.execute("UPDATE SISAP_MAP_GRUP_RUP SET  src_code = '' WHERE src_code = 'nan'", "redics")
u.execute("UPDATE SISAP_MAP_GRUP_RUP SET  src_code = lpad(src_code, 5, 0) WHERE length(src_CODE) <5", "redics")


# grups_revisats.fillna(-1).groupby(["INCLOS_2","INCLOS"]).agg({"SEC":"count", "N_PACIENTS":"sum"})
grups_revisats_curated.fillna(-1).groupby(["INCLOS_2","src_code_type", "src_code"]).agg({"SEC":"count"})


"""



# JUST ONCE
## Catalogo de Clasificaci�n
"""
tb_desti = 'SISAP_CLASS_GRUPSUSUARIS'

  # (0,'No institucio'),
  # (1, 'Residencia geriatrica'),
  # (2, 'altres institucions'),
  # (3, 'Hotels COVID19'),
  # (4, 'PADES'),
  # (5, 'Treballadors'),
  # (99, 'No classificats'),

upload = [(0,'No institucio', 0),
          (1, 'Residencia geriatrica', 1),
          (2, 'Altres instit. residencials', 1),
          (3, 'Hotels COVID19', 0),
          (4, 'PADES', 0),
          (5, 'Treballadors', 0),
          (6, 'Escola', 0),
          (7, 'Centre per discapacitat', 0),
          (8, 'Centre per salut mental', 0),
          (9, 'Centre per addiccions', 0),
          (10, 'Convent', 0),
          (11, 'Residencia infantil', 0),
          (12, 'Centre per risc exclusio', 0),
          (13, 'Centre sociosanitari', 0),
          (99, 'No classificats', 0),
          ]

cols = ['sisap_class NUMBER', 'sisap_class_des VARCHAR(30)', 'sisap_web NUMBER']

u.createTable(tb_desti, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.listToTable(upload, tb_desti, DB_DESTI)
u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
u.execute("grant all on {} to {}".format(tb_desti, PAQUI), DB_DESTI)
"""

# JUST ONCE
## Catalogo de RUPs (Registre de Unitats Productives: Lluis Villardel)
"""
tb_desti = 'SISAP_CAT_RUP'
cols = [
    'CODI_EP varchar(30)', 'DESC_EP varchar(50)',
    'CODI_GRUP_UP VARCHAR(10)', 'DESC_GRUP_UP varchar(50)',
    'CODI_SUBGRUP_UP VARCHAR(10)', 'DESC_SUBGRUP_UP varchar(50)',
    'CODI_UP VARCHAR(10)', 'NOM_CURT_UP varchar(30)', 'NOM_COMPLET_UP varchar(50)',
    'CODI_TIPUS_VIA varchar(30)', 'NOM_VIA varchar(50)', 'NUM_VIA varchar(30)',
    'CODI_POSTAL NUMBER',
    'CODI_COMARCA NUMBER',
    'CODI_LOCALITAT VARCHAR(10)', 'DESC_LOCALITAT varchar(50)',
    'CODI_ABS VARCHAR(10)', 'DESC_ABS varchar(50)',
    'CODI_SECTOR NUMBER', 'DESC_SECTOR varchar(50)',
    'CODI_REGIO NUMBER', 'DESC_REGIO varchar(50)'
]

rups_df = pd.read_csv(rups_file, sep=';', encoding='windows-1252', index_col=False, dtype={'CODI_EP': object}, decimal=',')

### imprime los dtypes de columnas
# rups_df.apply(lambda x: pd.api.types.infer_dtype(x.values))

### Campos completos, de aqui selecciono por problemas de unicode
# cols = ['CODI_EP varchar(30)', 'DESC_EP varchar(30)',
# 'CIF_NIF varchar(30)','CODI_TIPUS_TIT NUMBER','DESC_TIPUS_TIT varchar(30)',
# 'CODI_GRUP_UP NUMBER','DESC_GRUP_UP varchar(30)','CODI_SUBGRUP_UP NUMBER','DESC_SUBGRUP_UP varchar(30)',
# 'CODI_UP NUMBER','NOM_CURT_UP varchar(30)','NOM_COMPLET_UP varchar(30)',
# 'DATA_INICI_VIG_UP varchar(30)','DATA_FI_VIG_UP varchar(30)','DATA_INICI_VIG_DADES varchar(30)','DATA_FI_VIG_DADES varchar(30)',
# 'CODI_TIPUS_VIA varchar(30)','DESC_TIPUS_VIA varchar(30)','NOM_VIA varchar(30)','NUM_VIA varchar(30)',
# 'PIS varchar(30)','PORTA varchar(30)','ESCALA varchar(30)','PORTAL varchar(30)','BLOC varchar(30)','QUILOMETRE varchar(30)',
# 'CODI_LOCALITAT NUMBER','DESC_LOCALITAT varchar(30)',
# 'CODI_POSTAL NUMBER','CODI_COMARCA NUMBER','DESC_COMARCA varchar(30)',
# 'CODI_ABS NUMBER','DESC_ABS varchar(30)','CODI_SECTOR NUMBER','DESC_SECTOR varchar(30)','CODI_REGIO NUMBER','DESC_REGIO varchar(30)',
# 'LATITUD double','LONGITUD double','COORDENADA_X double','COORDENADA_Y double',
# 'INDX_EXACTITUD double','INDX_CONFIANZA varchar(30)','MODE_RESOLUCIO varchar(30)','DATA_GEOREF varchar(30)','ERROR varchar(30)',
# 'TELEFON varchar(30)','FAX varchar(30)','CORREU_ELECTRONIC varchar(30)',
# 'CODI_OFIC_CENTRE varchar(30)','DESC_OFIC_CENTRE varchar(30)','DATA_INICI_VIG_REL varchar(30)','DATA_FI_VIG_REL varchar(30)']

col_names = [col.split(" ")[0] for col in cols]

dflist = list(rups_df[col_names].itertuples(index= False, name=None))

u.createTable(tb_desti, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.listToTable(dflist, tb_desti, DB_DESTI)
u.execute("UPDATE SISAP_CAT_RUP SET  codi_up = lpad(codi_up, 5, 0) WHERE length(codi_up)<5", "redics")
u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
"""

# SCRATCH ZONE

# sql_pob = """select '{}', usua_cip,
#                   usua_uab_up,
#                   to_date(usua_data_naixement, 'J')
#            from usutb040 where rownum <10 """
grups_revisats_curated.apply(lambda x: pd.api.types.infer_dtype(x.values))

view = """
CREATE VIEW SISAP_VW_RESIDENCIES AS
SELECT
  sec, grup, sisap_class, src_code_type, src_code, grup_des, grup_n, grup_edat,
  eap1_n, eap1, grup_up, codi_up, codi_abs, desc_abs, desc_sector, desc_regio,
  nom_curt_up, nom_complet_up ,c.SCS_CODI, c.ICS_DESC, c.SAP_CODI, c.SAP_DESC,
  c.AMB_CODI, c.AMB_DESC
FROM
  sisap_map_grup_rup
  INNER JOIN SISAP_CAT_GRUPSUSUARIS
    ON sector_cod = sec AND grup_cod = grup
    AND sisap_class in (1,2)
  LEFT JOIN sisap_cat_rup
    ON src_code = codi_up
  LEFT JOIN PREDULMB.SISAP_CAT_CENTRES c
    ON codi_abs=ABS
ORDER BY
  amb_Desc DESC,
  codi_abs,
  eap1
"""

u.execute(view, 'redics')

u.execute("DROP VIEW SISAP_VW_RESIDENCIES",'redics')

u.grantSelect("SISAP_VW_RESIDENCIES","PREDUPRP",'redics')

for table in ['SISAP_CAT_CENTRES', 'SISAP_CAT_RUP', 'SISAP_MAP_GRUP_RUP', 'SISAP_CAT_GRUPSUSUARIS']:
  print table
  print([row for row in u.getAll("select * from PREDULMB.{}".format(table),'redics')])
  

redics.reptbcup (UPs total)
sectors.pritb128 (UPs total)
sectors.pritb028 (UP i UP_PARE)

# [DIARI: 1. SISAP_CAT_GRUP_SRC_CODES, el grup_up tengo que cambiar a coger pritb128 de standby]
sql = """
    SELECT
      DISTINCT 'sec_grup' AS src_code_type
      , sector_cod||'-'||grup_cod AS src_code
      , grup_des AS src_code_desc
      , sector_cod AS src_type
    FROM
      SISAP_CAT_GRUPSUSUARIS gu
    -- WHERE
      -- grup_up IS NULL
    UNION
    SELECT
      DISTINCT 'grup_up' AS src_code_type
      , grup_up AS src_code
      , cup_nup AS src_code_desc
      , cup_tup AS src_type
    FROM
      SISAP_CAT_GRUPSUSUARIS gu
      LEFT JOIN reptbcup cup ON gu.grup_up = cup.cup_cup 
    WHERE
      grup_up IS NOT NULL
    UNION
    SELECT distinct
      'rup_up' AS src_code_type
      , cup_cup AS src_code
      , cup_nup AS src_code_desc
      , cup_tup AS src_type
    FROM
      reptbcup cup
    WHERE
      cup_tup IN ('7071', '7075', '7076')
"""

upload = []
for row in u.getAll(sql, 'redics'):
  upload.append(row)

cols = [
  'src_code_type VARCHAR(10)',
  'src_code  VARCHAR(10)',
  'src_code_desc varchar(50)',
  'src_type varchar(10)'
  ]

u.createTable('SISAP_CAT_GRUP_SRC_CODES',"({})".format(", ".join(cols)), 'redics', rm=True)
u.listToTable(upload, 'SISAP_CAT_GRUP_SRC_CODES', 'redics')

u.grantSelect("SISAP_CAT_GRUP_SRC_CODES","PREDULMB",'redics')

u.grantSelect("SISAP_CAT_GRUP_SRC_CODES","PREDUPRP",'redics')

u.grantSelect("SISAP_CAT_GRUP_SRC_CODES","PREDUMMP",'redics')

u.grantSelect("SISAP_CAT_GRUP_SRC_CODES","PREDUECR",'redics')

# [NOOOO: DIARI: 2. SISAP_CAT_GRUP_TGT_CODES.py?]
sql = """
  SELECT grup_eap.* FROM
  (SELECT DISTINCT
    '{}' AS sec
    , ug_codi_grup * 1
    ,CASE WHEN ude_up_pare IS NOT NULL THEN 'PARE' ELSE CASE WHEN usua_uab_up IS NOT NULL THEN 'EAP1' ELSE 'NONE' END END AS target_code_type
    ,CASE WHEN ude_up_pare IS NOT NULL THEN ude_up_pare ELSE CASE WHEN usua_uab_up IS NOT NULL THEN usua_uab_up ELSE '00000' END END AS target_code
  FROM
    ppftb012 p12
    INNER JOIN usutb011 u11
      ON p12.ug_usua_cip = u11.cip_cip_anterior 
      AND p12.ug_dba IS NULL
    INNER JOIN ppftb011 p11
      ON p12.ug_codi_grup = p11.gu_codi_grup
      AND p11.gu_dba is NULL
    INNER JOIN usutb040 u40
      ON u11.cip_usuari_cip = u40.usua_cip 
    LEFT JOIN pritb028 pares
      ON p11.gu_up_residencia = pares.ude_up_scs
    LEFT JOIN pritb128 rups
      ON pares.ude_up_pare = rups.cup_cup 
  ) grup_eap
  LEFT JOIN pritb128 rups
    ON grup_eap.target_code = rups.cup_cup
  WHERE cup_tup LIKE '20%'
  """
  
upload = []
for sector in u.sectors:
  print(sector)
  for row in u.getAll(sql.format(sector), sector + 'sb'):
    upload.append(row)


cols = [
  'sector_cod varchar2(4)',
  'grup_cod NUMBER(6,0)',
  'target_code_type VARCHAR(10)',
  'target_code  VARCHAR(10)',
  ]

u.createTable('SISAP_CAT_GRUP_TGT_CODES',"({})".format(", ".join(cols)), 'redics', rm=True)
u.listToTable(upload, 'SISAP_CAT_GRUP_TGT_CODES', 'redics')

u.grantSelect("SISAP_CAT_GRUP_TGT_CODES","PREDULMB",'redics')


view =  """
  CREATE VIEW SISAP_VW_RESIDENCIES AS
  SELECT
    gu.sector_cod AS sector,
    gu.grup_cod AS grup,
    m.sisap_class,
    m.src_code_type AS resi_cod_type,
    m.src_code AS resi_cod,
    c.src_code_desc AS resi_des,
    c.src_type aS resi_type,
    m.target_code_type,
    m.target_code AS eap_up
  FROM
    SISAP_CAT_GRUPSUSUARIS gu
      inner join
    SISAP_MAP_GRUP_RUP m
        on gu.sector_cod = m.sec
        and gu.grup_cod = m.grup
      inner join
    SISAP_CAT_GRUP_SRC_CODES c
        on m.src_code_type = c.src_code_type
        and m.src_code = c.src_code
    """

try:
  u.execute("DROP VIEW SISAP_VW_RESIDENCIES",'redics')
except:
  pass

u.execute(view, 'redics')

u.grantSelect("SISAP_VW_RESIDENCIES","PREDUPRP",'redics')
u.grantSelect("SISAP_VW_RESIDENCIES","PREDULMB",'redics')
u.grantSelect("SISAP_VW_RESIDENCIES","PREDUMMP",'redics')

upsert_grups = 'upsert_grups.txt'

import sisapUtils as u
import re

i = 0
for row in u.readCSV(u.tempFolder + upsert_grups, sep='|'):
  i +=1
  row = [re.sub(r"\s", "", field) for field in row]
  sec, grup = row[0], row[1]
  print i, tuple(row)
  u.execute("delete from {} where sec = '{}' and grup = '{}'".format('SISAP_MAP_GRUP_RUP', sec, grup), 'redics')
  u.execute("insert into {} {} values {}".format('SISAP_MAP_GRUP_RUP','(sec, grup, sisap_class, src_code_type, src_code, target_code_type, target_code)', tuple(row) ), 'redics')

# FIX SISAP_COVID_RES_HISTORIC
sql = """
    select
      '{}',
      cip_usuari_cip,
      ug_codi_grup,
      gu_up_residencia,
      gu_desc,
      to_date(usua_data_naixement, 'J'),
      usua_uab_up
    from ppftb012,
      ppftb011,
      usutb011,
      usutb040
    where
      ug_usua_cip = cip_cip_anterior
      and ug_codi_grup = gu_codi_grup
      and gu_codi_grup =  {}
      and usua_cip = cip_usuari_cip
    """

deletable = {
  # '6211': [(5324, '20052'), (784, '05131')],
  # '6626': [(7189, '06380')],
  # '6837': [(143, '20535')],
  # '6626': [(3086, '20315', '20313')],
  # '6626':[(8610, '6626-8610','6626-8609')],
  '6102':[(2567, '08026','13201')],
  }

del_users, upd_users = [], []
for sector in deletable:
    print(sector)
    for pack in deletable[sector]:
      del_grup = pack[0]
      del_resi = pack[1]
      print(sector, del_grup, del_resi)
      print(sql.format(sector, del_grup))
      for sec, cip, grup, grup_up, grup_des, dnaix, eap in u.getAll(sql.format(sector, del_grup), sector + 'sb'):
        if len(pack) == 3:
          upd_resi = pack[2]
          upd_users.append((cip, del_resi, upd_resi))
        else:
          del_users.append((cip, del_resi))

sql = "delete from SISAP_COVID_RES_HISTORIC where CIP = '{}' and RESIDENCIA = '{}'"
for row in del_users:
  cip = row[0]
  resi = row[1]
  print(cip, resi)
  print(sql.format(cip, resi))
  u.execute(sql.format(cip, resi), 'redics')

sql = "update SISAP_COVID_RES_HISTORIC set RESIDENCIA = '{}' where CIP = '{}' and RESIDENCIA = '{}'"
for row in upd_users:
  cip, old_resi, new_resi = row[0], row[1], row[2]
  print(cip, old_resi, new_resi)
  print(sql.format(new_resi, cip, old_resi)) 
  u.execute(sql.format(new_resi, cip, old_resi), 'redics')

sql = """
INSERT INTO sisap_covid_res_historic
SELECT
  DISTINCT DATE '2020-05-20' AS DATA,
  r.CIP,
  r.HASH,
  r.RESIDENCIA,
  r.CATALEG
FROM
  sisap_covid_res_historic r
  INNER JOIN SISAP_MAP_GRUP_RUP m on r.RESIDENCIA = m.SRC_CODE
where
  r.DATA = DATE '2020-05-25'
  and m.sec = '6102' and m.SRC_CODE = '08026'
"""

u.execute(sql, 'redics')