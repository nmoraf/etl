# coding: utf8


import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u


class VisitesDia(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_moduls()
        self.get_visites()
        self.export_dades_visita()
        self.export_file_resum()

    def get_centres(self):
        """EAP ICS i no ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_moduls(self):
        """Moduls de MG lligats a UBA"""
        sql = ("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul \
                from cat_vistb027 \
                where modu_servei_codi_servei='MG'", "import")
        self.moduls = {(sec, cen, cla, ser, cod): (True) for (sec, cen, cla, ser, cod)
                        in u.getAll(*sql)}
    
        
   
    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        Si l'agenda és per capes, separem les visites segons el seu tipus (9C o 9R).
        En el cas de les agendes seqüencials,agafem visites del centre
        segons el lloc de realització.
        """
        self.dades = []
        visitesMG = 0
        lligadesUBA = 0
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), "import")
            if dat == 2017:
                sql ="select  codi_sector, id_cip_sec, visi_up, visi_centre_codi_centre, visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul,\
                        visi_tipus_visita, visi_lloc_visita, visi_situacio_visita, \
                        visi_data_visita,visi_dia_peticio, extract(year_month from visi_data_visita),\
                        visi_forcada_s_n, if(visi_bloc_codi_bloc ='', 'N', 'S'), visi_tipus_citacio \
                        from {} \
                        where visi_servei_codi_servei = 'MG' and visi_data_baixa = 0 and visi_situacio_visita='R' ".format(table)
                for sector, id, up, centre, classe, servei, modul, tipus, lloc, sit, datvisi, datpeti, periode, forcada, forcadaprof, cita in u.getAll(sql, "import"):
                    if up in self.centres:
                        br = self.centres[up][0]
                        visitesMG +=1
                        if (sector, centre, classe, servei, modul) in self.moduls:
                            lligadesUBA += 1
                            self.dades.append([id, centre,classe,servei,modul,up, br, datvisi, 
                                                    forcada,tipus])
        print "Visites MG toal any: ", visitesMG
        print "Visites MG lligades a UBA. ", lligadesUBA
        
    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules de My a test."""
        table = 'SISAP_VISITES_DIA_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db , rm=True)
        u.listToTable(dades, table,  db)

    def export_dades_visita(self):
        """Exporta les dades de nivell visita."""
        columns = ('id_cip_sec int',  'centre varchar(20)', 'classe varchar(3)','servei varchar(10)','modul varchar(10)','up varchar(5)', 'br varchar(5)',
                   'data_visita date', 'forcada varchar(2)', 'tipus_visita varchar(10)')
        VisitesDia.export_dades("MG", columns, self.dades)                                                

    def export_file_resum(self):
        """Treu txt amb les dades."""
        upload = []
        sql = 'select br, modul, count(*), count(distinct data_visita) from test.sisap_visites_dia_mg  group by br, modul'
        for br, agenda, visites, dies in u.getAll(sql, "test"):
             upload.append([br, agenda, visites, dies])
             
        
        file = u.tempFolder + 'VisitesAgenda.txt'
        u.writeCSV(file, upload, sep='|')
        
if __name__ == '__main__':
    VisitesDia()
    