from sisapUtils import getAll, multiprocess, writeCSV, tempFolder, sectors
from os import remove
from zipfile import ZipFile
from os.path import basename


imp = 'import'
nod = 'nodrizas'
inici, final = '20150601', '20160531'  # compte que tirem contra visites1 i per tant en un futur no funcionaria


info = {
    'poblacio': ("select codi_sector, id_cip_sec, up, centre_codi, centre_classe, date_format(data_naix, '%Y-%m-%d'), sexe, institucionalitzat from assignada_tot", nod),
    'visites': ("select codi_sector, id_cip_sec, date_format(visi_data_visita, '%Y-%m-%d'), visi_up, visi_servei_codi_servei, visi_tipus_visita, visi_tipus_citacio from visites1 \
                where visi_data_visita between {} and {} and visi_situacio_visita = 'R'".format(inici, final), imp),
    'gma': ('select codi_sector, id_cip_sec, gma_cod, round(gma_ind_cmplx, 3) from gma', imp),
    'virtuals': ('select codi_sector, id_cip_sec, cp_ecap_dvis, cp_up from cmbdap where cp_t_act = 44 and cp_ecap_dvis between {} and {}'.format(inici, final), imp)
}


def get_id_to_hash():
    centres = set([up for up, in getAll("select scs_codi from cat_centres where ep = '0208'", nod)])
    pacients = set([pacient for (pacient, up) in getAll('select id_cip_sec, up from assignada_tot', nod) if up in centres])
    id_to_hash = {id: hash for (id, hash) in getAll('select id_cip_sec, hash_d from u11', imp) if id in pacients}
    return id_to_hash


def get_data(param):
    domini, sql, db, id_to_hash = param
    data = [(row[0], id_to_hash[row[1]]) + row[2:] for row in getAll(sql, db) if row[1] in id_to_hash]
    file = '{}{}.txt'.format(tempFolder, domini)
    writeCSV(file, data)

    
def get_cp(sector):
    pdp = {cip: hash for (cip, hash) in getAll('select usua_cip, usua_cip_cod from pdptb101', '{}a'.format(sector))}
    cp = [(sector, pdp[cip], codi) for (cip, codi) in getAll('select usua_cip, usua_codi_postal from usutb040', sector) if cip in pdp]
    return cp


if __name__ == '__main__':
    '''
    # Primera entrega
    id_to_hash = get_id_to_hash()
    jobs = [(domini, sql, db, id_to_hash) for domini, (sql, db) in info.items()]
    multiprocess(get_data, jobs)
    with ZipFile(tempFolder + 'nemesio_moreno.zip', 'w', allowZip64=True) as zip:
        for domini in info:
            file = '{}{}.txt'.format(tempFolder, domini)
            zip.write(file, basename(file))
            remove(file)

    # Segons entrega
    resul = multiprocess(get_cp, sectors)
    file = tempFolder + 'codi_postal.txt'
    writeCSV(file, [])
    for sector in resul:
        writeCSV(file, sector, mode='a')
    '''
