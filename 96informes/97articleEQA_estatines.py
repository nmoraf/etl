# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
imp = 'import'
farmacs = '(82, 658, 216, 659)'

problems ='(726,727,728,729)'

exclusions_p = '(1,211,7,212,728,729,11,213,430,439,528,224,101,382)'

rcv = '(10)'

def farmconverter(farmac):
    
    if farmac in (82, 658, 216, 659):
        far = 'Estatines'
    elif farmac in (713, 714):
        far = 'MSRE'
    elif farmac in (715, 716):
        far = 'Denosumab'
    elif farmac in (717, 718):
        far = 'Bifosfonats'
    elif farmac in (719, 720):
        far = 'Altres aOP'
    else:
        far = 'error'
    return far

def psconverter(ps):
    
    if ps in (726,727):
        prob = 'CI'
    elif ps in (728,729):
        prob = 'AVC'
    elif ps in (79, 710, 711, 712):
        prob = 'Fractura fragilitat'
    else:
        prob = 'error'
    return prob

anys = [2017, 2018]
anys1 = [2017, 2018]
#mesos = ['01']
mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']

u11 = {}
all_cips = {}
sql = 'select id_cip, id_cip_sec,hash_d from u11{0}'.format( ' limit 10' if debug else '')
for id, idsec,hash_d in getAll(sql, imp):
    u11[hash_d] = id
    all_cips[idsec] = id
    
id_sidiap = {}
sql = 'select id, id_persistent from poblacio {}'.format( ' limit 10' if debug else '')
for id, idP in getAll(sql, ('SISAP_estatines','nym_proj')):
    a = tuple(idP.split(':'))
    idP = a[0]
    id_sidiap[int(id)] = idP


centres = {}

sql = "select sha1(concat(scs_codi,'EQA')), medea from cat_centres where ep='0208'"
for up, medea in getAll(sql, nod):
    centres[up] = medea
    
#dates naixement tota la pob

all_pob = {}
sql = 'select id_cip, usua_sexe, year(usua_data_naixement) from assignada {0}'. format(' limit 10' if debug else '')
for id,  sexe, naix in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix}

#farmacs

minfarmac = {}
prevfarmac = {}
sql = 'select id, dat, agr from farmacs_facturats where agr in {0} {1}'.format(farmacs, ' limit 10' if debug else '')
for id, dat, farmac in getAll(sql, ('SISAP_estatines','nym_proj')):
    far = farmconverter(int(farmac))
    try:
        hashP = id_sidiap[int(id)]
    except KeyError:
        continue
    try:
        id = u11[hashP]
    except KeyError:
        continue
    if (id, far) in minfarmac:
        inici2 = minfarmac[(id, far)]
        if float(dat) < float(inici2):
            minfarmac[(id, far)] = str(dat)
    else:
        minfarmac[(id, far)] = str(dat)
    if str(dat)[4:] == '12':
        unmesabans = int(dat) + 89
        dosmesosbans = int(dat) + 90
    elif str(dat)[4:] == '11':
        unmesabans = int(dat) + 1
        dosmesosbans = int(dat) + 90
    else:
        unmesabans = int(dat) + 1
        dosmesosbans = int(dat) + 2
    dat = str(dat)
    unmesabans = str(unmesabans)
    dosmesosbans = str(dosmesosbans)
    prevfarmac[(id, far, dat)] = True
    prevfarmac[(id, far, unmesabans)] = True
    prevfarmac[(id, far, dosmesosbans)] = True

    

#problemes salut:

agrs = []
conv = {}
sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in {0}{1}".format(problems, ' limit 10' if debug else '')
for agrupador, criteri in getAll(sql, nod):
    agrs.append(criteri)
    conv[criteri] = agrupador
in_crit = tuple(agrs)

incidents = defaultdict(list)
sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps1, dde, Ydde in getAll(sql, imp):
    ps = conv[ps1]
    prob = psconverter(ps)
    Ydde = str(Ydde)
    incidents[id, prob].append(Ydde)


#rcv

risccv =  defaultdict(list)
sql = 'select id_cip_sec, usar, extract(year_month from data_var), valor from eqa_variables where agrupador in {0} {1}'.format(rcv, ' limit 20' if debug else '')
for idsec, usar, Ydde, valor  in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    Ydde = str(Ydde)
    valor = float(valor)
    risccv[id].append([Ydde, usar, valor])

#exclusions


agrs = []
sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in {0}".format(exclusions_p)
for agrupador, criteri in getAll(sql, nod):
    agrs.append(criteri)
in_crit = tuple(agrs)

exclusions = defaultdict(list)
sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps, dde, Ydde in getAll(sql, imp):
    Ydde = str(Ydde)
    exclusions[(id, 'N')] = {Ydde}
    
sql = 'select id_cip_sec, extract(year_month from data_var) from eqa_variables where agrupador=197 and valor>320 {0}'.format( ' limit 20' if debug else '')
for idsec, Ydde in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    Ydde = str(Ydde)
    exclusions[(id, 'N')] = {Ydde}
    
sql = 'select id_cip_sec, extract(year_month from data_var) from eqa_variables where agrupador=31 and valor>=200 {0}'.format( ' limit 1' if debug else '')
for idsec, Ydde in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    Ydde = str(Ydde)
    exclusions[(id, '12m')] = {Ydde}

sql = 'select id_cip_sec, extract(year_month from data_var) from eqa_variables where agrupador=9 and valor>=240 {0}'.format( ' limit 1' if debug else '')
for idsec, Ydde in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    Ydde = str(Ydde)
    exclusions[(id, '12m')] = {Ydde}
    
sql = 'select id_cip_sec, farmac, pres_orig, data_fi, extract(year_month from pres_orig), extract(year_month from data_fi) from eqa_tractaments where farmac = 694 {0}'.format( ' limit 10' if debug else '')
for idsec, farmac, inici, fi, iniciYM, fiYM in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    for dat in range(iniciYM, fiYM + 1):
        dat = str(dat)
        if '00' < dat[4:] < '13':
            exclusions[(id, 'far')] = {dat}



visites = {}
'''
for ane in anys:
    for table in getSubTables('visites'):
        if ane < 2014:
            dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat == ane:
                for id, up in getAll("select id_cip,  visi_up  from {} where visi_situacio_visita = 'R'".format(table), 'import'):
                    visites[(id, ane)] = True
'''

for ane in anys:
    resultat1, resultat2, resultat3, resultat4, resultat5, resultat6, resultat7, resultat8, resultat9, resultat10, resultat11, resultat12 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat13, resultat14, resultat15, resultat16, resultat17, resultat18, resultat19, resultat20, resultat21, resultat22, resultat23, resultat24,resultat25,resultat26,resultat27  = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat28, resultat29,resultat30,resultat31,resultat32,resultat33,resultat34,resultat35,resultat36,resultat37,resultat38,resultat39 = Counter(), Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),
    resultat40,resultat41, resultat42,resultat43,resultat44 ,resultat45,resultat46 ,resultat47,resultat48,resultat49 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    assignada = {}
    sql = "select id_cip, sha1(concat(up,'EQA')), ates from assignadahistorica where dataany={0} {1}".format(ane, ' limit 1' if debug else '')
    for id, up, ates in getAll(sql, imp):
        if up in centres:
            medea = centres[up]
            assig = 1
            if ane < 2014:
                if (id, ane) in visites:
                    ates = 1
                else:
                    ates = 0
            sexe = 'NS'
            edat, edat5 = 'NS', 'NS'
            try:
                sexe = all_pob[id]['sexe']
                naix = all_pob[id]['naix']
                edat = ane - naix
            except KeyError:
                pass
            if edat < 15:
                continue
            if edat <>'NS':
                edat5 = ageConverter(edat)
            for mes in mesos:
                periode = str(ane) + mes
                unany = []
                for dat in range(int(periode) - 99, int(periode) + 1):
                    dat = str(dat)
                    if '00' < dat[4:] < '13':
                        unany.append(dat)
                excl = 0
                if (id, 'N') in exclusions:
                    for data_excl in exclusions[id, 'N']:
                        if data_excl < periode:
                            excl = 1
                if (id, '12m') in exclusions:
                    for data_excl in exclusions[id ,'12m']:
                        if data_excl in unany:
                            excl = 1               
                if (id, 'far') in exclusions:
                    for data_excl in exclusions[id ,'far']:
                        if data_excl == periode:
                            excl = 1           
                if excl == 0:
                    estatinesP = 0
                    if (id, 'Estatines', periode) in prevfarmac:
                        estatinesP = 1
                    estatinesI = 0
                    if (id, 'Estatines') in minfarmac:
                        iniciest = minfarmac[(id, 'Estatines')]
                        if iniciest == periode:
                            estatinesI = 1    
                    ciI, avcI = 0,0
                    if (id, 'CI') in incidents:
                        for inicialM in incidents[id, 'CI']:
                            if inicialM == periode:
                                ciI = 1
                    if (id, 'AVC') in incidents:
                        for inicialM in incidents[id, 'AVC']:
                            if inicialM == periode:
                                avcI = 1
                    rcv999est, rcv510est, rcv10est, riscv5est = 0,0,0,0
                    rcv999estP, rcv510estP, rcv10estP, riscv5estP = 0,0,0,0
                    rcv999NOe, rcv510NOe, rcv10NOe, riscv5NOe = 0,0,0,0
                    rcv999eveCI, rcv510eveCI, rcv10eveCI, riscv5eveCI = 0,0,0,0
                    rcv999eveAVC, rcv510eveAVC, rcv10eveAVC, riscv5eveAVC = 0,0,0,0
                    rcv999ass, rcv510ass, rcv10ass, riscv5ass = 0,0,0,0
                    rcv999at, rcv510at, rcv10at, riscv5at = 0,0,0,0
                    if id in risccv:
                        ultimrcv = {}
                        rows = risccv[id]
                        for drcv, usar, valorrcv in rows:
                            if drcv <= periode:
                                if id in ultimrcv:
                                    lastrcv = ultimrcv[id]['dat']
                                    if lastrcv < drcv:
                                        ultimrcv[id]['dat'] = drcv
                                        ultimrcv[id]['valor'] = valorrcv
                                else:
                                    ultimrcv[id] = {'dat': drcv, 'valor':valorrcv}
                        if (id) in ultimrcv:
                            vrcv = ultimrcv[id]['valor']
                            if vrcv <5:
                                riscv5ass = 1
                                if ates == 1:
                                    riscv5at = 1
                                if estatinesI == 1:
                                    riscv5est = 1
                                if estatinesP == 1:
                                    riscv5estP = 1
                                else:
                                    riscv5NOe = 1
                                if ciI == 1:
                                    riscv5eveCI = 1
                                if avcI == 1:
                                    riscv5eveAVC = 1
                            elif 5 <= vrcv <10:
                                rcv510ass = 1
                                if ates == 1:
                                    rcv510at = 1
                                if estatinesI == 1:
                                    rcv510est = 1
                                if estatinesP == 1:
                                    rcv510estP = 1
                                else:
                                    rcv510NOe = 1
                                if ciI == 1:
                                    rcv510eveCI = 1
                                if avcI == 1:
                                    rcv510eveAVC = 1
                            elif vrcv >= 10:
                                rcv10ass = 1
                                if ates == 1:
                                    rcv10at = 1
                                if estatinesI == 1:
                                    rcv10est = 1
                                if estatinesP == 1:
                                    rcv10estP = 1
                                else:
                                    rcv10NOe = 1
                                if ciI == 1:
                                    rcv10eveCI = 1
                                if avcI == 1:
                                    rcv10eveAVC = 1
                        else:
                            rcv999ass = 1
                            if ates == 1:
                                rcv999at = 1
                            if estatinesI == 1:
                                rcv999est = 1
                            if estatinesP == 1:
                                rcv999estP = 1
                            else:
                                rcv999NOe = 1
                            if ciI == 1:
                                rcv999eveCI = 1
                            if avcI == 1:
                                rcv999eveAVC = 1
                    else:
                        rcv999ass = 1
                        if ates == 1:
                            rcv999at = 1
                        if estatinesI == 1:
                            rcv999est = 1
                        if estatinesP == 1:
                            rcv999estP = 1
                        else:
                            rcv999NOe = 1
                        if ciI == 1:
                            rcv999eveCI = 1
                        if avcI == 1:
                            rcv999eveAVC = 1
                    resultat1[(periode, up, medea, edat5, sexe)] += estatinesP
                    resultat2[(periode, up, medea, edat5, sexe)] += estatinesI
                    resultat9[(periode, up, medea, edat5, sexe)] += ciI
                    resultat27[(periode, up, medea, edat5, sexe)] += avcI
                    resultat11[(periode, up, medea, edat5, sexe)] += assig
                    resultat12[(periode, up, medea, edat5, sexe)] += ates
                    resultat13[(periode, up, medea, edat5, sexe)] += rcv999est
                    resultat14[(periode, up, medea, edat5, sexe)] += rcv510est
                    resultat15[(periode, up, medea, edat5, sexe)] += rcv10est
                    resultat16[(periode, up, medea, edat5, sexe)] += riscv5est
                    resultat40[(periode, up, medea, edat5, sexe)] += rcv999estP
                    resultat41[(periode, up, medea, edat5, sexe)] += rcv510estP
                    resultat42[(periode, up, medea, edat5, sexe)] += rcv10estP
                    resultat43[(periode, up, medea, edat5, sexe)] += riscv5estP
                    resultat17[(periode, up, medea, edat5, sexe)] += rcv999NOe
                    resultat18[(periode, up, medea, edat5, sexe)] += rcv510NOe
                    resultat19[(periode, up, medea,  edat5, sexe)] += rcv10NOe
                    resultat20[(periode, up, medea, edat5, sexe)] += riscv5NOe
                    resultat21[(periode, up, medea, edat5, sexe)] += rcv999eveCI
                    resultat22[(periode, up, medea, edat5, sexe)] += rcv510eveCI
                    resultat23[(periode, up, medea, edat5, sexe)] += rcv10eveCI
                    resultat24[(periode, up, medea, edat5, sexe)] += riscv5eveCI
                    resultat28[(periode, up, medea, edat5, sexe)] += rcv999eveAVC
                    resultat29[(periode, up, medea, edat5, sexe)] += rcv510eveAVC
                    resultat30[(periode, up, medea, edat5, sexe)] += rcv10eveAVC
                    resultat31[(periode, up, medea, edat5, sexe)] += riscv5eveAVC
                    resultat32[(periode, up, medea, edat5, sexe)] += rcv999ass
                    resultat33[(periode, up, medea, edat5, sexe)] += rcv510ass
                    resultat34[(periode, up, medea, edat5, sexe)] += rcv10ass
                    resultat35[(periode, up, medea, edat5, sexe)] += riscv5ass
                    resultat36[(periode, up, medea, edat5, sexe)] += rcv999at
                    resultat37[(periode, up, medea, edat5, sexe)] += rcv510at
                    resultat38[(periode, up, medea, edat5, sexe)] += rcv10at
                    resultat39[(periode, up, medea, edat5, sexe)] += riscv5at
                else:
                    resultat44[(periode, up, medea, edat5, sexe)] += excl
                    estatinesPexcl = 0
                    if (id, 'Estatines', periode) in prevfarmac:
                        estatinesPexcl = 1
                    estatinesIexcl = 0
                    if (id, 'Estatines') in minfarmac:
                        iniciest = minfarmac[(id, 'Estatines')]
                        if iniciest == periode:
                            estatinesIexcl = 1    
                    ciIexcl, avcIexcl = 0,0
                    if (id, 'CI') in incidents:
                        for inicialM in incidents[id, 'CI']:
                            if inicialM == periode:
                                ciIexcl = 1
                    if (id, 'AVC') in incidents:
                        for inicialM in incidents[id, 'AVC']:
                            if inicialM == periode:
                                avcIexcl = 1
                    resultat46[(periode, up, medea, edat5, sexe)] += estatinesPexcl
                    resultat47[(periode, up, medea, edat5, sexe)] += estatinesIexcl
                    resultat48[(periode, up, medea, edat5, sexe)] += ciIexcl
                    resultat49[(periode, up, medea, edat5, sexe)] += avcIexcl
                resultat45[(periode, up, medea, edat5, sexe)] += assig
    upload = []
    for (periode, up, medea, edat5, sexe), res45 in resultat45.items():
        res1 = resultat1[(periode, up, medea, edat5, sexe)]
        res2 = resultat2[(periode, up, medea, edat5, sexe)]
        res9 = resultat9[(periode, up, medea, edat5, sexe)]
        res11 = resultat11[(periode, up, medea, edat5, sexe)]
        res12 = resultat12[(periode, up, medea, edat5, sexe)]
        res13 = resultat13[(periode, up, medea, edat5, sexe)]
        res14 = resultat14[(periode, up, medea, edat5, sexe)]
        res15 = resultat15[(periode, up, medea, edat5, sexe)]
        res16 = resultat16[(periode, up, medea, edat5, sexe)]
        res17 = resultat17[(periode, up, medea, edat5, sexe)]
        res18 = resultat18[(periode, up, medea, edat5, sexe)]
        res19 = resultat19[(periode, up, medea, edat5, sexe)]
        res20 = resultat20[(periode, up, medea, edat5, sexe)]
        res21 = resultat21[(periode, up, medea, edat5, sexe)]
        res22 = resultat22[(periode, up, medea, edat5, sexe)]
        res23 = resultat23[(periode, up, medea, edat5, sexe)]
        res24 = resultat24[(periode, up, medea, edat5, sexe)]
        res27  = resultat27[(periode, up, medea, edat5, sexe)]
        res28  = resultat28[(periode, up, medea, edat5, sexe)]
        res29  = resultat29[(periode, up, medea, edat5, sexe)]
        res30  = resultat30[(periode, up, medea, edat5, sexe)]
        res31  = resultat31[(periode, up, medea, edat5, sexe)]
        res32  = resultat32[(periode, up, medea, edat5, sexe)]
        res33  = resultat33[(periode, up, medea, edat5, sexe)]
        res34  = resultat34[(periode, up, medea, edat5, sexe)]
        res35  = resultat35[(periode, up, medea, edat5, sexe)]
        res36  = resultat36[(periode, up, medea, edat5, sexe)]
        res37  = resultat37[(periode, up, medea, edat5, sexe)]
        res38  = resultat38[(periode, up, medea, edat5, sexe)]
        res39  = resultat39[(periode, up, medea, edat5, sexe)]
        res40  = resultat40[(periode, up, medea, edat5, sexe)]
        res41  = resultat41[(periode, up, medea, edat5, sexe)]
        res42  = resultat42[(periode, up, medea, edat5, sexe)]
        res43  = resultat43[(periode, up, medea, edat5, sexe)]
        res44  = resultat44[(periode, up, medea, edat5, sexe)]
        res46  = resultat46[(periode, up, medea, edat5, sexe)]
        res47  = resultat47[(periode, up, medea, edat5, sexe)]
        res48  = resultat48[(periode, up, medea, edat5, sexe)]
        res49  = resultat49[(periode, up, medea, edat5, sexe)]
        upload.append([periode, up, medea, edat5, sexe, res1, res2,res9,res27,res11,res12,res13,res14,res15,res16,res40,res41,res42,res43,res17,res18,res19,res20,res21,res22,res23,res24,res28,res29,res30,res31, res32, res33,res34,res35,res36,res37,res38,res39, res44, res46, res47, res48, res49])
    file = tempFolder + 'projecte_Estatines_' + str(ane) + '.txt'
    writeCSV(file, upload, sep=';')
