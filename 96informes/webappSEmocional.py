# coding: utf-8

import collections as c
import csv
import hashlib as h
import os
from datetime import datetime

import sisapUtils as u

""" Aqui va una descripción """

USERS = ("PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB", "PREDUEHE")
USERSECAP = ("PDP", "PREDUEHE")

class covid19WebAppSEmocional(object):
    def __init__(self):
        """ . """

        print("Inici: ", datetime.now())

        ts = datetime.now()
        self.get_webapp()
        print("Time execution WEB APP: time {} / nrow {}".format(datetime.now() - ts, len(self.webapp)))

        ts = datetime.now()
        self.get_cip()
        print("Time execution CIP: time {} / nrow {}".format(datetime.now() - ts, len(self.ciphash)))

        ts = datetime.now()
        self.get_hash()
        print("Time execution HASH: time {} / nrow {}".format(datetime.now() - ts, len(self.hashidcip)))

        ts = datetime.now()
        self.get_poblacio()
        print("Time execution POBLACIO: time {} / nrow {}".format(datetime.now() - ts, len(self.poblacio)))

        ts = datetime.now()
        self.get_casos()
        print("Time execution CASOS: time {} / nrow {}".format(datetime.now() - ts, len(self.casos)))

        ts = datetime.now()
        self.get_rca()
        print("Time execution RCA: time {} / nrow {}".format(datetime.now() - ts, len(self.rca)))

        ts = datetime.now()
        self.get_vis()
        print("Time execution VISITES: time {} / nrow {}".format(datetime.now() - ts, len(self.vis)))

        ts = datetime.now()
        self.get_visp()
        print("Time execution VISITES P: time {} / nrow {}".format(datetime.now() - ts, len(self.visp)))

        ts = datetime.now()
        self.upload()
        print("Time execution UPLOAD PAC: time {} / nrow {}".format(datetime.now() - ts, len(self.uploadpac)))
        print("Time execution UPLOAD ECAP: time {} / nrow {}".format(datetime.now() - ts, len(self.uploadecap)))





    def get_webapp(self):
        """FLT: NO"""
        print("--------------------------------------------- get_webapp")
        self.webappset = c.defaultdict(set)
        self.webapp = {}
        sql = """SELECT
                    cip
                    ,to_date(data, 'yyyy-mm-dd')
                FROM
                    dwaquas.covid19_casosderivatsap"""
        for cip14, data in u.getAll(sql, "exadata"):
            cip13 = cip14[:13]
            hashcovid = h.sha1(cip13).hexdigest().upper()
            self.webappset[cip13].add((cip14, hashcovid, data))
            for cip13 in self.webappset:
                cip14, hashcovid, data = max(self.webappset[cip13])
                etiqueta = "webapp suport emocional"
                self.webapp[(cip13,etiqueta)]={"cip14": cip14, "hashcovid": hashcovid,"app_data": data}
        # print(self.webapp)

    def get_cip(self):
        """
        Executa 16 workers en paral·lel
        segons el primer dígit del HASH.
        """
        print("--------------------------------------------- get_cip")
        jobs = [format(digit, "x").upper() for digit in range(16)]
        resultat = u.multiprocess(self.get_cip_worker, jobs)
        self.ciphash = {}
        for worker in resultat:
            for cip, hash in worker:
                self.ciphash[(cip)] = hash
        print("N registros (CIP) = HASH:", len(self.ciphash))

    def get_cip_worker(self, subgroup):
        """Worker de get_cip"""
        sql = """SELECT
                    usua_cip
                    , usua_cip_cod
                FROM
                    pdptb101
                WHERE
                    usua_cip_cod like '{}%'""".format(subgroup)
        db = "pdp"
        converter = []
        for cip, hash in u.getAll(sql, db):
            converter.append((cip, hash))
        return converter

    def get_hash(self):
        """."""
        print("--------------------------------------------- get_u11")
        self.hashidcip = {}
        db = "import"
        tb = "u11"
        sql = "SELECT id_cip, hash_d, codi_sector FROM {}.{}".format(db, tb)
        for id_cip, hash_d, sector in u.getAll(sql, db):
            self.hashidcip[hash_d] = {"id_cip": id_cip, "sector": sector}
        #print("Ejemplo HASH:", next(iter( self.hashidcip.items() )) )
        print("N de registros HASH to IDCIP: {}", len(self.hashidcip))

    def get_poblacio(self):
        """."""
        print("--------------------------------------------- get_poblacio")
        self.poblacio={}
        sql = """
                SELECT
                    id_cip
                    , id_cip_sec
                    , up
                    , uba
                    , ubainf
                    , edat
                FROM
                    assignada_tot
            """
        for id_cip, id_cip_sec, up, uba, ubainf, edat in u.getAll(sql, "nodrizas"):
            self.poblacio[id_cip] = {"id_cip_sec": id_cip_sec, "up": up, "uba": uba, "ubainf": ubainf, "edat": edat}
        #print("Ejemplo self.poblacio: ", next(iter( self.poblacio.items() )) )

    def get_rca(self):
        """ RCA N=11.000.000 ¡¡ojo!! tarda 10m -> multiprocesso?? """
        print("--------------------------------------------- get_rca")
        self.rca = {}
        sql = "SELECT hash, decode(eap, 'SES', NULL, eap) FROM preduffa.sisap_covid_pac_rca"
        for hash, up in u.getAll(sql,"redics"):
            self.rca[hash] = {"up": up}
        print("RCA:", len(self.rca))

    def get_casos(self):
        """FLT: NO"""
        print("--------------------------------------------- get_casos")
        self.casos = {}
        sql = """SELECT
                    hash
                    , up
                    , uba
                    , inf
                    , edat
                    , estat
                    , cas_data
                    , dx_sit
                    , pcr_res
                    , pcr_data
                    , fr_numero
                    , vis_data
                FROM
                    preduffa.sisap_covid_pac_master"""
        for hash, up, uba, inf, edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data in u.getAll(sql, "redics"):
            self.casos[hash] = {"up": up, "uba": uba, "inf": inf, "edat": edat,
                                "estat": estat, "cas_data": cas_data, "dx_sit": dx_sit,
                                "pcr_res": pcr_res, "pcr_data": pcr_data, "fr_numero": fr_numero, "vis_data": vis_data}

    def get_vis(self):
        """."""
        print("--------------------------------------------- get_vis")
        self.visdat= {}
        self.vis = c.defaultdict(set)
        db = "redics"
        tb = "sisap_covid_pac_visites"
        sql = "SELECT pacient, data FROM preduffa.{}".format(tb)
        for hash, dat in u.getAll(sql, db):
            self.vis[hash].add(dat)
        for hash in self.vis:
            maxdat = max(self.vis[hash])
            self.visdat[hash]= {"dat": maxdat}

    def get_visp(self):
        """ 11/06/2020 N=425.000 """
        print("--------------------------------------------- get_visp")
        self.vispdat= {}
        self.visp = c.defaultdict(set)
        db = "redics"
        tb = "sisap_covid_pac_visites_p"
        sql = "SELECT hash, data FROM preduffa.{}".format(tb)
        for hash, dat in u.getAll(sql, db):
            self.visp[hash].add(dat)
        for hash in self.visp:
            maxdat = max(self.visp[hash])
            self.vispdat[hash]= {"dat": maxdat}


    def upload(self):
        """ . """
        print("--------------------------------------------- upload")
        self.uploadpac = []
        self.uploadecap = []
        for cip13, etiqueta in self.webapp:
            hashcovid = self.webapp[(cip13,etiqueta)]["hashcovid"]
            up = self.rca[hashcovid]["up"] if hashcovid in self.rca else None
            app_data = self.webapp[(cip13,etiqueta)]["app_data"]
            estat = self.casos[hashcovid]["estat"] if hashcovid in self.casos else None
            cas_data = self.casos[hashcovid]["cas_data"] if hashcovid in self.casos else None
            dx_sit = self.casos[hashcovid]["dx_sit"] if hashcovid in self.casos else None
            pcr_res = self.casos[hashcovid]["pcr_res"] if hashcovid in self.casos else None
            pcr_data = self.casos[hashcovid]["pcr_data"] if hashcovid in self.casos else None
            fr_numero = self.casos[hashcovid]["fr_numero"] if hashcovid in self.casos else None
            vis_data = self.visdat[hashcovid]["dat"] if hashcovid in self.visdat else None
            visp_data = self.vispdat[hashcovid]["dat"] if hashcovid in self.vispdat else None
            hash = self.ciphash[cip13] if cip13 in self.ciphash else None
            id_cip = self.hashidcip[hash]["id_cip"] if hash in self.hashidcip else None
            uba = self.poblacio[id_cip]["uba"] if id_cip in self.poblacio else None
            ubainf = self.poblacio[id_cip]["ubainf"] if id_cip in self.poblacio else None
            edat = self.poblacio[id_cip]["edat"] if id_cip in self.poblacio else None
            self.uploadpac.append((hashcovid, up, uba, ubainf,
                                   etiqueta, app_data,
                                   edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data, visp_data))
            self.uploadecap.append((cip13, up, uba, ubainf,
                                    etiqueta, app_data,
                                    edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data, visp_data))
        print(self.uploadpac[0])

        tb = "SISAP_COVID_PAC_WEBAPP_SE"
        db = "redics"
        columns = ["hash varchar2(40)",
                   "up varchar2(5)",
                   "uba varchar2(5)",
                   "inf varchar2(5)",
                   "etiqueta varchar2(50)",
                   "app_data date",
                   "edat number",
                   "estat varchar2(16)",
                   "cas_data date",
                   "dx_sit varchar2(10)",
                   "pcr_res varchar2(20)",
                   "pcr_data date",
                   "fr_numero number",
                   "vis_data date",
                   "visp_data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.uploadpac, tb, db)
        u.grantSelect(tb, USERS, db)

        tb = "SISAP_COVID_ECAP_WEBAPP_SE"
        db = "redics"
        columns = ["cip varchar2(13)",
                   "up varchar2(5)",
                   "uba varchar2(5)",
                   "inf varchar2(5)",
                   "etiqueta varchar2(50)",
                   "app_data date",
                   "edat number",
                   "estat varchar2(16)",
                   "cas_data date",
                   "dx_sit varchar2(10)",
                   "pcr_res varchar2(20)",
                   "pcr_data date",
                   "fr_numero number",
                   "vis_data date",
                   "visp_data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.uploadecap, tb, db)
        u.grantSelect(tb, USERSECAP, db)





if __name__ == "__main__":

    try:
        clock_in = datetime.now()
        covid19WebAppSEmocional()
        clock_out = datetime.now()
        print("---------------------------------------------- TIME EXECUTION")
        print("START: {}".format(clock_in))
        print("END: {}".format(clock_out))
        print("DELTA: {}".format (clock_out - clock_in))
    except Exception as e:
        subject = "WebApp bad!!!"
        text = str(e)
        # raise  # ############################################################
    else:
        subject = "WebApp good"
        text = ""
    finally:
        print(subject)
        print(text)
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'ehermosilla@idiapjgol.info',
                      'eduboniqueta@gmail.com',
                      subject, text)
