# excel mireia 2016-04-27 10:00 (passar primer a csv, eliminar header i deixar nomes hash i sector)

from sisapUtils import readCSV, tempFolder, listToTable, createTable, getAll, writeCSV

redics = 'redics'
tmp = 'sisap_tabac_tmp'
createTable(tmp, '(hash varchar2(40), sector varchar2(4), unique(hash, sector))', redics, rm=True)
listToTable([row for row in readCSV(tempFolder + 'Exfumador.csv', sep=';')], tmp, redics)
ids = {id: (hash, sector) for (id, hash, sector) in getAll('select b.id_cip_sec, a.hash, a.sector from {} a, sisap_u11 b where a.hash = b.hash_a and a.sector = b.sector'.format(tmp), redics)}
resul = []
for id, dat in getAll("select id_cip_sec, date_format(min(dalta), '%Y-%m-%d') from eqa_tabac where tab = 1 group by id_cip_sec", 'nodrizas'):
    if id in ids:
        resul.append(ids[id] + (dat,))
writeCSV(tempFolder + 'exfumadors_sisap.csv', resul, sep=';')
