# coding: latin1

"""
Implants
"""

import sisapUtils as u

def ageConverter(edat):
    if 15 <= edat <= 19:
        return '15-19'
    elif 20 <= edat <= 24:
        return '20-24'
    elif 25 <= edat <= 29:
        return '25-29'
    elif edat >= 30:
        return '30-40'

class Implants(object):
    """."""

    def __init__(self):
        """."""
        self.get_ids()
        self.get_metodes()
        self.get_assir()
        self.taula_anticonc()
        self.get_recomptes()
        self.export_taula()
        
    def get_ids(self):
        """."""
        self.ids = {}
        sql = "select id_cip_sec, edat, up, ep from assignada_tot where sexe = 'D' \
              and edat >=15 and edat <=40"
        for id, edat, up, ep in u.getAll(sql, "nodrizas"):
			self.ids[id] = ageConverter(edat), up, ep
                
    def get_metodes(self):
        """."""
        self.implant_actual = {}
        self.implant = {}
        actual=0
        trobat=0
        sql = "select id_cip_sec, xml_data_alta, camp_codi from xml_detall \
               where xml_tipus_orig = 'EW2003' and camp_valor=1 order by 1, 2 desc"
        for id, data, codi in u.getAll(sql, "import"):
            if id in self.ids:
                #Si �s la data m�s recent d'aquell ID (m�tode actual)
                if id != actual:
                    actual=id
                    trobat=0
                    if codi=='6':
                        self.implant_actual[id] = (data, codi, self.ids[id][0])
                        trobat=1
                #Si la data no �s la m�s recent de l'ID i encara no he trobat 
                #implant comprobo que el codi no sigui el de l'implant
                elif trobat != 1 and codi=='6':
                    self.implant[id] = (data, codi, self.ids[id][0])
                    trobat=1
        print(len(self.implant))
        print(len(self.implant_actual))
        
        
    def get_assir(self):
        """."""
        self.up = {}
        self.assir = {}
        self.equip = {}
        
        sql = "select up, assir from ass_centres"        
        for up, assir in u.getAll(sql, "nodrizas"):
            self.assir[up] = assir
            
        sql = "select ep, ics_desc from cat_centres"        
        for ep, equip in u.getAll(sql, "nodrizas"):
            self.equip[ep] = equip
        
        sql = "select id_cip_sec, visi_up from ass_imputacio_up"
        for id, up in u.getAll(sql, "nodrizas"):
            if id in self.ids:
                if up in self.assir:
                    self.up[id] = (up, self.assir[up], "assir")
                else:
                    self.up[id] = (up, self.equip[self.ids[id][2]], "equip")

    def taula_anticonc(self):
        """."""
        self.taula_anticonc = {'0': 'No utilitza cap m�tode comentat',\
                               '1': 'Preservatiu mascul�',\
                               '2': 'Preservatiu femen�',\
                               '3': 'AO combinada',\
                               '4': 'AO gestagen',\
                               '5': 'A injectable gestagen',\
                               '6': 'Implants',\
                               '7': 'Anell vaginal',\
                               '8': 'Pegats',\
                               '9': 'DIU',\
                               '10': 'DIU LNG',\
                               '11': 'Diafragma',\
                               '12': 'Espermicides',\
                               '13': 'Esterilitzaci� tub�rica',\
                               '14': 'Vasectomia',\
                               '15': 'Coitus interruptus',\
                               '16': 'Ogino',\
                               '17': 'Billing',\
                               '18': 'Simptot�rmic',\
                               '19': 'MELA (m�tode amenorrea de lact�ncia)',\
                               '20': 'No precisa'}

    def get_recomptes(self):
        """."""
        self.recompte_implant = {}
        self.recompte_implant_actual = {} 
        for id in self.implant:
            edat = self.ids[id][0]
            if id not in self.up:
                if self.ids[id][2] not in self.equip:
                    up = 0
                else:
                    up = self.equip[self.ids[id][2]]
            else:
                up = self.up[id]
            if (up, edat) not in self.recompte_implant:
                self.recompte_implant[up, edat] = 1
            else:
                self.recompte_implant[up, edat] += 1
        for id in self.implant_actual:
            edat = self.ids[id][0]
            if id not in self.up:
                if self.ids[id][2] not in self.equip:
                    up = 0
                else:
                    up = self.equip[self.ids[id][2]]
            else:
                up = self.up[id]
            if (up, edat) not in self.recompte_implant_actual:
                self.recompte_implant_actual[up, edat] = 1
            else:
                self.recompte_implant_actual[up, edat] += 1

    def export_taula(self):
        """."""
        upload = self.up.items()
        u.writeCSV(u.tempFolder + "up.csv", upload, sep=";")
        upload = self.implant.items()
        u.writeCSV(u.tempFolder + "implants.csv", upload, sep=";")
        upload = self.implant_actual.items()
        u.writeCSV(u.tempFolder + "implants_actuals.csv", upload, sep=";")
        upload = self.recompte_implant_actual.items()
        u.writeCSV(u.tempFolder + "recompte_implants_actuals.csv", upload, sep=";")
        upload = self.recompte_implant.items()
        u.writeCSV(u.tempFolder + "recompte_implants.csv", upload, sep=";")
      
if __name__ == "__main__":
    Implants()
