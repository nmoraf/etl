# coding: latin1


"""
.
"""

import collections as c

import sisapUtils as u


PERIOD = ("2018-01-01", "2018-12-31")
FILE = u.tempFolder + "exploracio_decret_{}.csv".format(PERIOD[0][:4])


class Decret(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_dades()
        self.work_it()
        self.get_resultat()
        self.export_resultat()

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_desc, ics_desc from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_dades(self):
        """."""
        sql = "select * from sisap_accessibilitat \
               where servei = 'MG' and \
                     dia between date '{}' and date '{}'".format(*PERIOD)
        self.dades = (row for row in u.getAll(sql, "redics"))

    def work_it(self):
        """."""
        self.resultat = c.Counter()
        for row in self.dades:
            modul, up, uba, servei, dia, peticions = row[:6]
            forats = row[6:]
            mes = dia.month
            demanen = peticions if peticions else 0
            obtenen = min(demanen, sum(forats[:22]))
            self.resultat[(up, mes, "demanen")] += demanen
            self.resultat[(up, mes, "obtenen")] += obtenen

    def get_resultat(self):
        """."""
        self.export = []
        for (up, mes, tipus), n in self.resultat.items():
            if tipus == "demanen":
                demanen = n
                obtenen = self.resultat[(up, mes, "obtenen")]
                ambit, eap = self.centres.get(up, ("deprecated", "deprecated"))
                try:
                    percentatge = obtenen / float(demanen)
                except ZeroDivisionError:
                    percentatge = 0
                pretty = str(round(100 * percentatge, 3)).replace(".", ",")
                self.export.append((ambit, eap, mes, demanen, obtenen, pretty))

    def export_resultat(self):
        """."""
        header = [("ambit", "eap", "mes", "demanen", "obtenen", "percentage")]
        u.writeCSV(FILE, header + sorted(self.export), sep=";")


if __name__ == "__main__":
    Decret()
