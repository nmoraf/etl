# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


DEBUG = False
DATES = ('2019-09-01', '2019-09-30')
NAIX = (u.gcal2jd(2004, 10, 1), u.gcal2jd(2019, 9, 30))
NAIX2 = (u.gcal2jd(2019, 3, 1), u.gcal2jd(2019, 9, 30))


class Alerta(object):
    """."""

    def __init__(self):
        """."""
        self.get_codis()
        self.get_seguiments()
        self.get_problemes()
        self.get_alerta()

    def get_codis(self):
        """."""
        self.codis = {"inc": set(), "exc": set(), "con": set()}
        for key in self.codis:
            file = __file__.replace(".py", "_{}.txt".format(key))
            for cod, in u.readCSV(file):
                if cod[0] == "D" and "A" <= cod[1] <= "Z":
                    cod = cod[1:]
                    if len(cod) > 3:
                        cod = cod[:3] + "." + cod[3:]
                    cod = "C01-" + cod
                    self.codis[key].add(cod)

    def get_seguiments(self):
        """."""
        if DEBUG:
            seguiments = [worker("6837")]
        else:
            seguiments = u.multiprocess(worker, u.sectors)
        self.seguiments = c.defaultdict(lambda: c.defaultdict(set))
        for sector in seguiments:
            for id, dat, cod in sector:
                if cod in self.codis["inc"]:
                    self.seguiments[id]["inc"].add(dat)
                elif cod in self.codis["exc"]:
                    self.seguiments[id]["exc"].add(dat)

    def get_problemes(self):
        """."""
        if DEBUG:
            problemes = [worker2("6837")]
        else:
            problemes = u.multiprocess(worker2, u.sectors)
        self.problemes = set()
        for sector in problemes:
            for id, cod in sector:
                if cod in self.codis["con"]:
                    self.problemes.add(id)

    def get_alerta(self):
        """."""
        self.alerta = {1: set(), 2: self.problemes}
        for id, dades in self.seguiments.items():
            if len(dades["inc"] - dades["exc"]) >= 3:
                self.alerta[1].add(id)
        print {(k, len(v)) for (k, v) in self.alerta.items()}


def worker(sector):
    """."""
    sql = "select usua_cip, usua_uab_up, usua_uab_codi_uab \
           from usutb040 \
           where usua_situacio = 'A' and \
                 usua_uab_up is not null and \
                 usua_uab_codi_uab is not null and \
                 usua_data_naixement between {} and {}".format(*NAIX)
    pob = {row[0]: row[1:] for row in u.getAll(sql, sector)}
    sql = "select cip_usuari_cip, sc_datseg, sc_coddiag \
           from prstb318 a, usutb011 b \
           where sc_cip = cip_cip_anterior and \
                 sc_datseg between date '{}' and date '{}' and \
                 sc_tipus = 'LLIURE' and \
                 sc_v_num is not null".format(*DATES)
    print 1, sector
    return [row for row in u.getAll(sql, sector) if row[0] in pob]


def worker2(sector):
    """."""
    sql = "select usua_cip, usua_uab_up, usua_uab_codi_uab \
           from usutb040 \
           where usua_situacio = 'A' and \
                 usua_uab_up is not null and \
                 usua_uab_codi_uab is not null and \
                 usua_data_naixement between {} and {}".format(*NAIX2)
    pob = {row[0]: row[1:] for row in u.getAll(sql, sector)}
    sql = "select cip_usuari_cip, pr_cod_ps \
           from prstb015 a, usutb011 b \
           where pr_cod_u = cip_cip_anterior and \
                 pr_dde between date '{}' and date '{}' and \
                 pr_data_baixa is null".format(*DATES)
    print 2, sector
    return [row for row in u.getAll(sql, sector) if row[0] in pob]


if __name__ == "__main__":
    Alerta()
