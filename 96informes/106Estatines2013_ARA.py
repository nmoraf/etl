from sisapUtils import *
from collections import defaultdict, Counter


debug = False

nod = 'nodrizas'
imp = 'import'
db = 'test'

sql='select data_ext from dextraccio'
for d in getOne(sql, nod):
    dext=d

all_pob = {}
sql = 'select id_cip, usua_sexe, year(usua_data_naixement), usua_situacio, usua_data_situacio from assignada {0}'. format(' limit 10' if debug else '')
for id,  sexe, naix, sit, datsit in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix, 'sit': sit, 'datsit': datsit}
    
all_cips = {}
sql = 'select id_cip, id_cip_sec,hash_d from u11{0}'.format( ' limit 10' if debug else '')
for id, idsec,hash_d in getAll(sql, imp):
    all_cips[idsec] = id

pob2013 = {}
sql = "select id_cip, ates from assignadahistorica where dataany='2013'"
for id, ates in getAll(sql, imp):
    try:
        naix = all_pob[id]['naix']
        sexe = all_pob[id]['sexe']
        sit = all_pob[id]['sit']
        datsit = all_pob[id]['datsit']
    except KeyError:
        continue
    edat = 2013 - naix
    pob2013[(id)] = {'ates': ates, 'edat': edat, 'sexe': sexe, 'sit':sit, 'datsit':datsit}

pob = {}
sql = 'select id_cip, edat, sexe, ates, institucionalitzat, maca from assignada_tot where edat>14'
for id, edat, sexe, ates, insti, maca in getAll(sql, nod):
    pob[id] = {'edat': edat, 'sexe': sexe, 'ates': ates, 'insti': insti, 'maca': maca}
    
est2013 = {}
sql = "select id_cip_sec, pres_orig from nodrizas.eqa_tractaments where farmac in (82,216) and pres_orig<'20140101' and data_fi>'20140101'"
for idsec, data in getAll(sql,nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    est2013[id] = True

rcv2013 = {}
sql = "select id_cip_sec, data_var, valor from eqa_variables where agrupador=10 and data_var<'20140101'"
for idsec, data, valor in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    if id in rcv2013:
        dat2 = rcv2013[id]['data']
        if data > dat2:
            rcv2013[id]['data'] = data
            rcv2013[id]['valor'] = valor
    else:
        rcv2013[id] = {'data': data, 'valor': valor}


rcvactual = {}
sql = "select id_cip_sec, data_var, valor from eqa_variables where agrupador=10 and usar=1"
for idsec, data, valor  in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    rcvactual[id] = {'data': data, 'valor': valor}

estactual = {}
sql = "select id_cip_sec from nodrizas.eqa_tractaments where farmac = 82"
for idsec,  in getAll(sql,nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    estactual[id] = True

exclusionsARA, exclusions2013 = {}, {}
sql ='select id_cip_sec, data_var from eqa_variables, nodrizas.dextraccio where agrupador=197 and valor>320'
for idsec, dat in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    if str(dat) < '20140101':
        exclusions2013[id] = True
    if dat <= dext:
        exclusionsARA[id] = True
sql ='select id_cip_sec, data_var from eqa_variables, nodrizas.dextraccio where agrupador=9 and valor>=240'
for idsec, dat in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    if str(dat) < '20140101':
        exclusions2013[id] = True
    if dat <= dext:
        exclusionsARA[id] = True
sql = "select id_cip_sec, dde from eqa_problemes where ps in ('1','211','7','212','11','213','430','439','224','101')"
for idsec, dat in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    if str(dat) < '20140101':
        exclusions2013[id] = True
    if dat <= dext:
        exclusionsARA[id] = True
sql = 'select id_cip_sec from eqa_microalb'
for idsec, in getAll(sql, nod):
    try:
        id = all_cips[idsec]
    except KeyError:
        continue
    exclusionsARA[id] = True
        
upload = []
for id, t in est2013.items():
    assigactual, edat, sexe, ates13, atesactual, insti, maca, datsit, sit = 0, None, None, None, None, None, None, None,  'A'
    if id in pob2013:
        edat = pob2013[id]['edat']
        sexe = pob2013[id]['sexe']
        ates13 = pob2013[id]['ates']
        sit = pob2013[id]['sit']
        datsit = pob2013[id]['datsit']
        if id in pob:
            assigactual = 1
            atesactual = pob[id]['ates']
            insti = pob[id]['insti']
            maca = pob[id]['maca']
        rcv13, datrcv13 = 0, None
        if id in rcv2013:
            rcv13 = rcv2013[id]['valor']
            datrcv13 = rcv2013[id]['data']
        rcvARA, datrcvARA = 0, None
        if id in rcvactual:
            rcvARA = rcvactual[id]['valor']
            datrcvARA = rcvactual[id]['data']
        estact = 0
        if id in estactual:
            estact = 1
        excl2013 = 0
        if id in exclusions2013:
            excl2013 = 1
        exclARA = 0
        if id in exclusionsARA:
            exclARA=1
        
        upload.append([id, assigactual, edat, sexe, ates13, atesactual,sit, datsit, insti, maca,  rcv13, datrcv13, rcvARA, datrcvARA, estact, excl2013, exclARA])

TableMy = 'estatines_2013_to_ARA'
create = '(id int, assigARA double, edat double, sexe varchar(10), ates13 double, atesARA double, sit varchar(10),datsit date, insti double, maca double,  rcv13 double, datrcv13 date, rcvARA double, datrcvARA date, estatinaARA double, excl13 double, exclARA double)'
createTable(TableMy,create,db, rm=True)
listToTable(upload, TableMy, db)

