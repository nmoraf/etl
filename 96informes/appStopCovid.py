# coding: utf-8

import collections as c
import sisapUtils as u
import hashlib as h
from datetime import datetime
import time
import csv,os

USERS = ("PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB", "PREDUEHE")
USERSECAP = ("PDP","PREDUEHE")

APP_STOPCOVID = "dwaquas.covid19_appdataorigin"

# descomentar la siguiente linea, y avisar a toni, en caso que se pierdan grants a la tabla buena
# APP_STOPCOVID = "dwaquas.covid19_appdataorigin_1"

def measure_time(func):
    def wrapper(*args):
        t = time.time()
        res = func(*args)
        print("--------------------------------- {}".format(func.__name__))
        print("{} took : ".format(func.__name__) + str(time.time()-t) + " seconds to run")
        print("object size : " + str(res) + " items")
        return res
    return wrapper

class covid19AppStopCovid(object):
    def __init__(self):
        """."""
        print("Inici: {}".format(datetime.now()))
        self.get_app()
        self.get_cip()
        self.get_hash()
        self.get_poblacio()
        self.get_casos()
        self.get_rca()
        self.get_vis()
        self.get_visp()
        self.prepare_upload()
        self.upload()

    @measure_time
    def get_app(self):
        """FLT: NO"""
        self.appset = c.defaultdict(set)
        self.app = {}
        self.hashcovidset = set()
        sql = """SELECT
                    document_id
                    -- , document_id_type
                    -- , id
                    , to_date(substr(created,1,10), 'yyyy/mm/dd')
                    , modified
                    , classification
                    -- , contacted
                    -- , verified
                    , sem_classification
                FROM
                    {}
                WHERE
                    document_id_type = 0 and
                    classification in (1,2) and
                    sem_classification in (1,2,3)""".format(APP_STOPCOVID)
        etiqueta = "App Stop Covid"
        for cip14, created, modified, cla_usu, cla_sem  in u.getAll(sql, "exadata"):
            cip13 = cip14[:13]
            hashcovid = h.sha1(cip13).hexdigest().upper()
            self.hashcovidset.add(hashcovid)
            self.appset[cip13].add((cip14, hashcovid, created, modified, cla_usu, cla_sem))
            for cip13 in self.appset:
                cip14, hashcovid, created, modified, cla_usu, cla_sem = max(self.appset[cip13])
                self.app[(cip13,etiqueta)]={"cip14": cip14, "hashcovid": hashcovid, "app_data": created, "cla_usu": cla_usu, "cla_sem": cla_sem}
        return len(self.app)

    @measure_time
    def get_cip(self):
        """
        Executa 16 workers en paral·lel
        segons el primer dígit del HASH.
        """
        jobs = [format(digit, "x").upper() for digit in range(16)]
        resultat = u.multiprocess(self.get_cip_worker, jobs)
        self.ciphash = {}
        self.hashset = set()
        for worker in resultat:
            for cip, hash in worker:
                if cip in self.appset:
                    self.ciphash[(cip)] = hash
                    self.hashset.add(hash)
        return len(self.ciphash)

    def get_cip_worker(self, subgroup):
        """Worker de get_cip"""
        sql = """SELECT usua_cip, usua_cip_cod from pdptb101
               where substr(usua_cip_cod, 1, 1) = '{}'""".format(subgroup)
        db = "pdp"
        converter = []
        for cip, hash in u.getAll(sql, db):
            converter.append((cip, hash))
        return converter

    @measure_time
    def get_hash(self):
        """."""
        self.hashidcip = {}
        self.id_cipset = set()
        db = "import"
        tb = "u11"
        sql = "select id_cip, hash_d, codi_sector from {}.{}".format(db, tb)
        for id_cip, hash_d, sector in u.getAll(sql, db):
            if hash_d in self.hashset:
                self.id_cipset.add(id_cip)
                self.hashidcip[hash_d] = {"id_cip": id_cip, "sector": sector}
        #print("Ejemplo HASH:", next(iter( self.hashidcip.items() )) )
        #print("N de registros HASH to IDCIP: {}", len(self.hashidcip))
        return len(self.hashidcip)

    @measure_time
    def get_poblacio(self):
        """."""
        self.poblacio={}
        sql = """SELECT id_cip, id_cip_sec, up, uba, ubainf, edat
               FROM assignada_tot"""
        for id_cip, id_cip_sec, up, uba, ubainf, edat in u.getAll(sql, "nodrizas"):
            if id_cip in self.id_cipset:
                self.poblacio[id_cip] = {"id_cip_sec": id_cip_sec, "up": up, "uba": uba, "ubainf": ubainf, "edat": edat}
        #print("Ejemplo self.poblacio: ", next(iter( self.poblacio.items() )) )
        return len(self.poblacio)

    @measure_time
    def get_rca(self):
        """ RCA N=11.000.000 ¡¡ojo!! tarda 10m -> multiprocesso?? """
        self.rca = {}
        sql = "SELECT hash, eap FROM sisap_covid_pac_rca WHERE decode(eap,'', NULL, 'SES', NULL, eap) IS NOT NULL"
        for hash, up in u.getAll(sql,"redics"):
            if hash in self.hashcovidset: 
                self.rca[hash] = up
        return len(self.rca)

    @measure_time
    def get_casos(self):
        """FLT: NO"""
        self.casos = {}
        sql = """select hash, up, uba, inf, edat, 
                      estat, cas_data, dx_sit, 
                      pcr_res, pcr_data, fr_numero, vis_data
               from preduffa.sisap_covid_pac_master"""
        for hash, up, uba, inf, edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data in u.getAll(sql, "redics"):
            self.casos[hash] = {"up": up, "uba": uba, "inf": inf, "edat": edat,
                                "estat": estat, "cas_data": cas_data, "dx_sit": dx_sit,
                                "pcr_res": pcr_res, "pcr_data": pcr_data, "fr_numero": fr_numero, "vis_data": vis_data}
        return len(self.casos)

    @measure_time
    def get_vis(self):
        """."""
        self.visdat= {}
        #self.vis = c.defaultdict(set)
        db = "redics"
        tb = "sisap_covid_pac_visites"
        sql = "select pacient, max(data) from {} group by pacient".format(tb)
        for hash, dat in u.getAll(sql, db):
            if hash in self.hashcovidset:
                self.visdat[hash] = dat
        #     self.vis[hash].add(dat)
        # for hash in self.vis:
        #     maxdat = max(self.vis[hash])
        #     self.visdat[hash]= {"dat": maxdat}
        return len(self.visdat)

    @measure_time
    def get_visp(self):
        """ 11/06/2020 N=425.000 """
        self.vispdat= {}
        # self.visp = c.defaultdict(set)
        db = "redics"
        tb = "sisap_covid_pac_visites_p"
        sql = "select hash, max(data) from {} group by hash".format(tb)
        for hash, dat in u.getAll(sql, db):
            self.vispdat[hash]= dat
        # for hash in self.visp:
        #     maxdat = max(self.visp[hash])
        #     self.vispdat[hash]= {"dat": maxdat}
        return len(self.vispdat)

    @measure_time
    def prepare_upload(self):
        """ . """
        self.uploadpac = []
        self.uploadecap = []
        for cip13, etiqueta in self.app:
            estat = cas_data = dx_sit = pcr_res = pcr_data = fr_numero = uba = ubainf = edat = None 
            hashcovid = self.app[(cip13,etiqueta)]["hashcovid"]
            hash = self.ciphash[cip13] if cip13 in self.ciphash else None
            id_cip = self.hashidcip[hash]["id_cip"] if hash in self.hashidcip else None
            if hashcovid in self.casos:
                estat = self.casos[hashcovid]["estat"]
                cas_data = self.casos[hashcovid]["cas_data"]
                dx_sit = self.casos[hashcovid]["dx_sit"]
                pcr_res = self.casos[hashcovid]["pcr_res"]
                pcr_data = self.casos[hashcovid]["pcr_data"]
                fr_numero = self.casos[hashcovid]["fr_numero"]
            if id_cip in self.poblacio:
                uba = self.poblacio[id_cip]["uba"]
                ubainf = self.poblacio[id_cip]["ubainf"]
                edat = self.poblacio[id_cip]["edat"]
            up = self.rca[hashcovid] if hashcovid in self.rca else None
            app_data = self.app[(cip13,etiqueta)]["app_data"]
            cla_usu = self.app[(cip13,etiqueta)]["cla_usu"]
            cla_sem = self.app[(cip13,etiqueta)]["cla_sem"]
            vis_data = self.visdat[hashcovid] if hashcovid in self.visdat else None
            visp_data = self.vispdat[hashcovid] if hashcovid in self.vispdat else None
            self.uploadpac.append((hashcovid, up, uba, ubainf,
                                   etiqueta, app_data, cla_usu, cla_sem, 
                                   edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data, visp_data))
            self.uploadecap.append((cip13, up, uba, ubainf,
                                    etiqueta, app_data, cla_usu, cla_sem,
                                    edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data, visp_data))
        return len(self.uploadpac)

    @measure_time
    def upload(self):
        tb = "SISAP_COVID_PAC_APP"
        db = "redics"
        columns = ["hash varchar2(40)",
                   "up varchar2(5)",
                   "uba varchar2(5)",
                   "inf varchar2(5)",
                   "etiqueta varchar2(50)",
                   "app_data date",
                   "cla_usu number",
                   "cla_sem number",
                   "edat number",
                   "estat varchar2(16)",
                   "cas_data date",
                   "dx_sit varchar2(10)",
                   "pcr_res varchar2(20)",
                   "pcr_data date",
                   "fr_numero number",
                   "vis_data date",
                   "visp_data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.uploadpac, tb, db)
        u.grantSelect(tb, USERS, db)

        tb = "SISAP_COVID_ECAP_APP"
        db = "redics"
        columns = ["cip varchar2(13)",
                   "up varchar2(5)",
                   "uba varchar2(5)",
                   "inf varchar2(5)",
                   "etiqueta varchar2(50)",
                   "app_data date",
                   "cla_usu number",
                   "cla_sem number",
                   "edat number",
                   "estat varchar2(16)",
                   "cas_data date",
                   "dx_sit varchar2(10)",
                   "pcr_res varchar2(20)",
                   "pcr_data date",
                   "fr_numero number",
                   "vis_data date",
                   "visp_data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.uploadecap, tb, db)
        u.grantSelect(tb, USERSECAP, db)
        return len(self.uploadecap)

if __name__ == "__main__":

    try:
        clock_in = datetime.now()
        covid19AppStopCovid()
        clock_out = datetime.now()
        print("---------------------------------------------- TIME EXECUTION")
        print("START: {}".format(clock_in))
        print("END: {}".format(clock_out))
        print("DELTA: {}".format (clock_out - clock_in))
    except Exception as e:
        subject = "App STOP COVID bad!!!"
        text = str(e)
        # raise  # ############################################################
    else:
        subject = "App STOP COVID good"
        text = ""
    finally:
        print(subject)
        print(text)
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'ehermosilla@idiapjgol.info',
                      'eduboniqueta@gmail.com',
                      subject, text)