import math

import sisapUtils as u


table = 'sisap_simulacio_professionals'

explorar = {
    'EQA0201': (81.84074175075, 90.9341575008333),
    'EQA0202': (83.2835820895522, 92.5373134328358),
    'EQA0203': (85.5, 95),
    'EQA0204': (58.7551652892561, 65.2835169880624),
    'EQA0205': (64.6353135313531, 71.8170150348368),
    'EQA0206': (70.4721459832357, 78.3023844258174),
    'EQA0207': (70.6317497560494, 78.479721951166),
    'EQD0238': (57.4095352684614, 70.7761562140711),
    'EQA0208': (62.8791891891892, 69.8657657657658),
    'EQA0209': (62.7903801107325, 69.767089011925),
    'EQA0210': (71.5672936928542, 79.5192152142825),
    'EQA0212': (73.1757897275139, 81.306433030571),
    'EQD0239': (85.5, 95),
    'EQA0213': (65.2274490121824, 72.4749433468693),
    'EQA0235': (75.6119934282585, 84.0133260313983),
    'EQD0240': (64.766959243383, 72.9127725856698),
    'EQA0214': (64.2857142857142, 71.4285714285714),
    'EQA0215': (84.8838821490468, 94.315424610052),
    'EQD0241': (78.4942084942085, 87.5),
    'EQA0220': (81.6472868217054, 90.7192075796727),
    'EQD0242': (66.3851402436057, 73.7612669373397),
    'EQA0223': (85.1875425459496, 94.6528250510551),
    'EQA0226': (74.0884682248406, 82.3205202498229),
    'EQA0230': (58.2903241750095, 64.9372539370079),
    'EQA0231': (36.0771412365286, 50.3355704697987),
    'EQA0219': (27.863829787234, 38.9673740893253),
    'EQA0227': (64.2857142857142, 71.4285714285714),
    'EQD0247': (74.9371092554541, 83.2634547282823),
    'EQA0301': (67.4897399480351, 74.9885999422612),
    'EQA0302': (44.4131455399061, 52.4968944099379),
    'EQA0303': (30.9922187019474, 34.7741758225169),
    'EQA0304': (68.6213411611175, 76.2459346234639),
    'EQA0306': (36.2752114508783, 48.0051948051948),
    'EQD0313': (79.6169322480798, 88.463258053422),
    'EQA0501': (39.1705736348594, 45.2803762758102),
    'EQA0502': (13.834008694319, 18.8018897637795),
    'EQA0308': (60.3634641337071, 68.3234043617447),
    'EQA0309': (59.4798814871803, 69.2648832212461),
    'EQA0310': (44.5404694011845, 59.6852871754524),
    'EQA0312': (27.2003852427993, 37.3367960717537),
    'EQA0401': (85.5, 95),
    'EQA0402': (85.5, 95),
    'EQA0403': (74.8275862068966, 95),
    'EQA0404': (85.0412846821686, 94.4903163135207),
    'EQA0406': (85.5, 95),
    'EQA0407': (83.9578326414682, 95),
    'EQA0216': (5.5546962707349, 8.02421450554621),
    'EQA0222': (46.6479941291585, 52.3989745316121),
    'EQA0224': (85.5, 95),
    'EQA0228': (1.14491999326189, 1.98424256695722),
    'EQA0229': (85.5, 95),
    'EQA0232': (74.3726037876834, 82.6362264307593),
    'EQA0313': (9.23623128242682, 15.2490211977296),
    'EQA0217': (0.431940830061579, 0.784933383434381)
}

sql = 'select up, uab, a.tipus, a.indicador, literal, detectats, resolts, assoldeteccio, invers, c.punts \
       from eqaindicadors a, eqacataleg b, eqacatalegpunts c \
       where a.dataany = 2017 and a.datames = 0 and \
       b.dataany = 2016 and a.indicador = b.indicador and b.mmax > 0 and \
       c.dataany = 2016 and a.indicador = c.indicador and a.tipus = c.tipus and c.punts > 0'
puntuacio = {}
i = 0
for up, uba, tip, ind, lit, detectats, resolts, as_detec, inv, pond in u.getAll(sql, 'pdp'):
    mmin = explorar[ind][0] / 100.0
    mmax = explorar[ind][1] / 100.0
    if inv:
        resolts = detectats - resolts
        mmin_o = mmin + 0
        mmin = 1 - mmax
        mmax = 1 - mmin_o
    if detectats == 0:
        resolucio = 0
    else:
        resolucio = float(resolts) / detectats
    if resolucio >= mmax:
        as_resol = 1
    elif resolucio < mmin:
        as_resol = 0
    else:
        as_resol = (resolucio - mmin) / (mmax - mmin)
    as_resul = as_resol * as_detec
    punts = as_resul * pond
    puntuacio[(up, uba, tip, ind)] = [lit, pond, inv, detectats, resolts, as_detec, mmin, mmax, as_resol, as_resul, punts]

    mmin_ind = math.floor(mmin * detectats)
    mmax_ind = math.floor(mmax * detectats)
    if mmin_ind == mmax_ind and mmax_ind > 0:
        mmin_ind -= 1
    if resolts >= mmax_ind:
        as_resol = 1
    elif resolts < mmin_ind:
        as_resol = 0
    else:
        as_resol = (resolts - mmin_ind) / (mmax_ind - mmin_ind)
    as_resul = as_resol * as_detec
    punts = as_resul * pond
    puntuacio[(up, uba, tip, ind)].extend((mmin_ind, mmax_ind, as_resol, as_resul, punts))


u.createTable(table, '(up varchar2(5), uab varchar2(5), tipus varchar2(1), indicador varchar2(25), literal varchar2(250), ponderacio int, invers int, detectats int, resolts int, assol_deteccio number, meta_min_general number, meta_max_general number, assol_resol_general number, assol_resul_general number, punts_general number, meta_min_indiv int, meta_max_indiv int, assol_resol_indiv number, assol_resul_indiv number, punts_indiv number)', 'redics', rm=True)
export = [list(k) + v for (k, v) in puntuacio.items()]
u.listToTable(export, table, 'redics')
u.grantSelect(table, 'PREDUMMP', 'redics')
