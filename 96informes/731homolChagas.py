from sisapUtils import getAll, createTable, listToTable, writeCSV, tempFolder, sendSISAP
from collections import Counter
from os import remove


table = 'homol_chagas'
db = 'test'
descartar = 'eliminar'
proves = {'S20785': ['Chagas', 'serologia'],'S39685': ['Chagas', 'serologia'],'S39785': ['Chagas', 'serologia'],'006764': ['Chagas', 'serologia'],'SC193': ['Chagas', 'serologia'],'SC086': ['Chagas', 'serologia']}
file = tempFolder + 'homologacio_chagas.csv'


def homologar(tipus, val, ref):
    if tipus == 'serologia':
        homologat = homologar_serologia(val, ref)
    elif tipus == 'carrega':
        homologat = homologar_carrega(val)
    else:
        homologat = homologar_valor(val)
    return homologat


def homologar_serologia(val, ref):
    val = val.replace(',', '.')
    ref = ref.replace(',', '.')
    hom = 9
    try:
        val = float(val)
    except ValueError:
        if ('NEG' in val.upper() and 'CONEGUT' not in val.upper() and 'NO PROCEDEIX' not in val.upper() and 'POSIT' not in val.upper()) or ('<' in val.upper() and '>' not in val.upper() and 'POSIT' not in val.upper()) or 'NO ES DETECTA' in val.upper() or 'NO ES DETECTEN' in val.upper() or 'NO SE DETECTA' in val.upper() or 'NAGATI' in val.upper() or 'NO REACTI' in val.upper() or (val.upper() == 'N'):
            hom = 0
        elif ('POS' in val.upper() or ('>' in val.upper() and '<' not in val.upper()) or 'REACTIU' in val.upper() or 'CONFIRMADA' in val.upper()) and ('PENDENT' not in val.upper() and 'NO ' not in val.upper() and 'BAIX' not in val.upper() and 'FEBLE' not in val.upper() and 'DUBT' not in val.upper() and 'BIL' not in val.upper() and 'NEGAT' not in val.upper())or (val.upper() == 'P'):
            hom = 1
    else:
        try:
            ref = float(ref)
        except (ValueError, TypeError):
            pass
        else:
            hom = 1 if val > ref else 0
    return hom


def homologar_carrega(val):
    val = val.replace(',', '.').replace('"', '').replace('x10E', ' x10E')
    hom = descartar
    try:
        val = int(float(val.split(' ')[0]))
    except ValueError:
        if len(val) < 70:
            if 'NEG' in val.upper() or 'IND' in val.upper() or '<' in val.upper() or 'INFERIOR' in val.upper() or ('DET' in val.upper() and 'NO' in val.upper()) or ('REACTI' in val.upper() and 'NO' in val.upper()):
                hom = 0
    else:
        hom = val
    return hom


def homologar_valor(val):
    val = val.replace(',', '.')
    try:
        hom = float(val.split(' ')[0])
    except ValueError:
        hom = descartar
    return hom


validacio = Counter()
upload = []
sql = "select id_cip_sec, cr_codi_lab, date_format(cr_data_reg, '%Y%m%d'), cr_codi_prova_ics, cr_res_lab, cr_ref_max_lab from test.serologiaChagas where cr_codi_prova_ics in {}".format(tuple(proves))
for id, lab, dat, prova, resultat, referencia in getAll(sql, 'import'):
    homologat = homologar(proves[prova][1], resultat, referencia)
    validacio[(lab, prova, resultat, referencia, homologat)] += 1
    if homologat != descartar:
        upload.append([id, dat, prova, homologat])

createTable(table, '(id_cip_sec int, dat varchar(8), cod varchar(6), val double)', db, rm=True)
listToTable(upload, table, db)
writeCSV(file, sorted([[laboratori] + proves[prova] + [n, resultat, referencia, homologat] for (laboratori, prova, resultat, referencia, homologat), n in validacio.items()], reverse=True), sep=';')

