# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re


nod="nodrizas"
imp="import"
alt="altres"

cohorts=range(1994,2004)

agr_vph=782

def get_up_ambit():
    """Asocia a cada up el ambito al que pertenece. Filtra por las up donde ep= '0208'.
       Devuelve un diccionario {up:amb_desc}
    """
    sql="select scs_codi,amb_desc from {}.cat_centres where ep = '0208'".format(nod)
    return {up:amb_desc for up,amb_desc in getAll(sql,nod)}


def get_dosis_id():
    dosis_id=defaultdict(set)
    for taula in ['eqa_vacunes','ped_vacunes']:
        sql='select id_cip_sec, dosis from {}.{} where agrupador={}'.format(nod,taula,agr_vph)
        for id, dosis in getAll(sql,nod):
            if dosis > 2:
                dosis == 3
            dosis_id[dosis].add(id)
    return dosis_id

def get_rows(dosis_id,up_ambit):
    rows_dict=defaultdict(lambda: defaultdict(Counter))
    sql='select id_cip_sec,up,data_naix,sexe,ates from {}.assignada_tot'.format(nod)
    for id,up,data_naix,sexe,ates in getAll(sql,nod):
        #if data_naix.year in cohorts and up in up_ambit:
        if data_naix.year in cohorts:
            rows_dict[data_naix.year][sexe]['N']+=1
            if ates==1:
                rows_dict[data_naix.year][sexe]['A']+=1
            for dosis in dosis_id:
                if id in dosis_id[dosis]:
                    rows_dict[data_naix.year][sexe][(dosis,ates)]+=1
    return rows_dict

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>35}" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))


if __name__ == '__main__':

    printTime('inici')
    up_ambit=get_up_ambit()
    printTime('up_ambit')
    dosis_id=get_dosis_id()
    printTime('dosis_id')
    rows_dict=get_rows(dosis_id,up_ambit)
    printTime('rows_dict')

    header=['Any de naixement','Sexe','Pob assignada','Vacunats ASS 1 dosi','Vacunats ASS dues dosis','Vacunats ASS 3 o més dosis','Pob atesa','Vacunats ATES 1 dosi','Vacunats ATES dues dosis','Vacunats ATES 3 o més dosis']
    name='vacunats_vph_all.txt'
    
    rows=[ [year, sexe, rows_dict[year][sexe]['N']]+
           [sum([rows_dict[year][sexe][(dosis,a)] for a in [0,1]]) for dosis in [1,2,3] ]+
           [rows_dict[year][sexe]['A']]+
           [rows_dict[year][sexe][(dosis,1)] for dosis in [1,2,3] ] 
            for year in rows_dict 
                for sexe in rows_dict[year]]

    printTime('rows')
    export_txt_file(name,header,rows)
