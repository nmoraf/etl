# -*- coding: utf8 -*-

"""
Petició Núria Nadal (directora AP LLeida) 7-8-17.
"""

import collections as c

import sisapUtils as u


y = 2015


class CMBD(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_estats()
        self.get_cmbdh()
        self.get_reingressos()
        self.get_resultat()
        self.export()

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_codi from cat_centres where ep = '0208'"
        self.centres = {up: ambit == "01" for (up, ambit)
                        in u.getAll(sql, "nodrizas")}

    def get_estats(self):
        """."""
        self.estats = {}
        sql = "select nia, es_cod, es_up from prstb715 \
               where es_dde <= to_date('{0}1231', 'YYYYMMDD') and \
                     (es_dba is null or \
                      es_dba >= to_date('{0}0101', 'YYYYMMDD'))".format(y)
        for nia, cod, up in u.getAll(sql, "redics"):
            if nia and up in self.centres:
                des = "PCC" if cod == "ER0001" else "MACA"
                lleida = "S" if self.centres[up] else "N"
                if nia not in self.estats or des < self.estats[nia][0]:
                    self.estats[nia] = (des, lleida)

    def get_cmbdh(self):
        """."""
        self.cmbdh = c.defaultdict(set)
        sql = "select nia, d_ingres, d_alta from redics.crg_cmbd_altes \
               where d_alta between to_date('{0}0101', 'YYYYMMDD') and \
                                    to_date('{0}1231', 'YYYYMMDD')".format(y)
        for nia, ingres, alta in u.getAll(sql, "redics"):
            if nia in self.estats:
                self.cmbdh[nia].add((ingres, alta))

    def get_reingressos(self):
        """."""
        self.reingressos = {}
        for nia, ingressos in self.cmbdh.items():
            inicis = set([ingres for (ingres, alta) in ingressos])
            for ingres, alta in ingressos:
                for inici in inicis:
                    if inici >= alta:
                        temps = (inici - alta).days
                        if (nia not in self.reingressos or
                           temps < self.reingressos[nia]):
                            self.reingressos[nia] = temps

    def get_resultat(self):
        """."""
        self.resultat = {}
        for nia, key in self.estats.items():
            if key not in self.resultat:
                self.resultat[key] = [0 for i in range(6)]
            self.resultat[key][0] += 1
        for nia in self.cmbdh:
            key = self.estats[nia]
            self.resultat[key][1] += 1
        for nia, temps in self.reingressos.items():
            key = self.estats[nia]
            if temps == 0:
                categ = 2
            elif temps < 3:
                categ = 3
            elif temps < 31:
                categ = 4
            else:
                categ = 5
            self.resultat[key][categ] += 1

    def export(self):
        """."""
        file = u.tempFolder + "ingressos_pcc_maca.csv"
        header = [("estat", "lleida", "pacients", "ingressen",
                   "reingressen 0d", "reingressen 1-2d", "reingressen 3-30d",
                   "reingressen >30d")]
        dades = sorted([list(k) + v for (k, v) in self.resultat.items()])
        u.writeCSV(file, header + dades, sep=";")


if __name__ == "__main__":
    CMBD()
