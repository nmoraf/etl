# -*- coding: utf-8 -*-
# assumeix que el fitxer està al tempFolder del PC des d'on s'executa
# si es consolida, caldrà afegir a procés general txt2red
from sisapUtils import getAll, tempFolder, readCSV, createTable, listToTable, calcStatistics, grantSelect


fl = 'Sancions i contencions.txt'
tb = 'sisap_eapp_sancions_txt'
db = 'redics'


hash = {}
for id, nis in getAll('select usua_cip, usua_nis from sisap_eapp_usutb040', db):
    hash[str(nis)] = id

upload = []
for nis, centre, situacio, alta, baixa in readCSV(tempFolder + fl):
    if nis in hash:
        upload.append([hash[nis], centre, situacio, alta, baixa])

createTable(tb, '(usua_cip varchar2(40), centre varchar2(4), situacio varchar2(20), d_aplica date, d_suspen date)', db, rm=True)
listToTable(upload, tb, db, 'YYYYMMDDHH24MI')
calcStatistics(tb, db)
grantSelect(tb, ('PREDUMMP', 'PREDUPRP'), db)
