# coding: iso-8859-1

import sisapUtils as u
import csv,os,sys
from time import strftime
from collections import defaultdict, Counter

nod = 'nodrizas'
imp = 'import'
YEAR = 2019

centres = {}
sql =  '''
    select
        a.amb_codi_amb,
        a.amb_desc_amb,
        s.dap_codi_dap,
        s.dap_desc_dap,
        c.up_codi_up_scs,
        u.up_codi_up_ics,
        u.up_desc_up_ics
    from cat_gcctb006 s,
        cat_gcctb008 c,
        cat_gcctb007 u,
        cat_gcctb005 a
    where
        u.dap_codi_dap = s.dap_codi_dap
        and c.up_codi_up_ics = u.up_codi_up_ics
        and s.amb_codi_amb = a.amb_codi_amb
        and c.up_data_baixa = 0
        and dap_data_baixa = 0
        and u.up_data_baixa = 0
        and amb_data_baixa = 0
        and e_p_cod_ep = '0208'
    '''
        

for amb, ambdesc, dap, dapdesc, up, br, desc in u.getAll(sql, 'import'):
    centres[up] = {'ambit':amb, 'ambdesc':ambdesc,'dap':dap,'dapdesc':dapdesc,'br':br,'desc':desc}

sql = 'select scs_codi, ics_codi from cat_centres'
eaps = {up:br for up, br in u.getAll(sql,nod)}

GRUPS = {
    647: {'desc': "Sífilis precoç", 'cim10':set()},
    649: {'desc': "Sífilis inespecífica", 'cim10':set()},
    651: {'desc': "infecció gonocòccica", 'cim10':set()},
    653: {'desc': "Clamídia", 'cim10':set()},
    666: {'desc': "VIH", 'cim10':set()},
    667: {'desc': "SIDA", 'cim10':set()},
    668: {'desc': "Tricomones", 'cim10':set()},
    13:  {'desc': "Hepatitis B", 'cim10':set()},
    495: {'desc': "Condiloma acuminat", 'cim10':set()},
    669: {'desc': "Infecció genital per herpes", 'cim10':set()},
    670: {'desc': "Contacte sexual VIH", 'cim10':set()},
    671: {'desc': "Contacte sexual ITS", 'cim10':set()},
    # 406: {'desc': "ITS inespecífica", 'cim10':set()},
    879: {'desc': "Linfogranuloma veneri", 'cim10':set()},
    881: {'desc': "Uretritis", 'cim10':set()}
}

sql_base = '''select agrupador, criteri_codi from eqa_criteris where agrupador in {}'''

sql = sql_base.format(tuple(set([desc for desc in GRUPS])))
for k, v in u.getAll(sql, 'nodrizas'):
    GRUPS[k]['cim10'].add(v)

CODIS = set()
for k, v in GRUPS.items():
    for cod in v['cim10']:
        CODIS.add(cod)

"""# muchos codigos de ITS inespecifica son compartidos por otros agrupadores (VHB, Gonococcia)"""
""" Comento porque el agrupador 406 es un supraagrupador, y necesitan solo un par de codigos de los que no hay grupo """
# ITS_INESPECIFICA_BRUTA = set()
# for k, v in u.getAll(sql_base.format('(406)'), 'nodrizas'):
#     ITS_INESPECIFICA_BRUTA.add(v) 
# ITS_INESPECIFICA = ITS_INESPECIFICA_BRUTA - CODIS

ITS_INESPECIFICA  = set(['C01-A63.8','C01-A64'])
GRUPS.update({406: {'desc': "ITS inespecífica", 'cim10': ITS_INESPECIFICA}})

print(GRUPS[406])

INVERS = {}
for k in GRUPS.keys():
    for cod in GRUPS[k]['cim10']:
            INVERS[cod] = GRUPS[k]['desc']

# refresco el conjunto de codigos para que incluya los de ITS_INESPECIFICA

CODIS = set()
for k, v in GRUPS.items():
    for cod in v['cim10']:
        CODIS.add(cod)

CODIS = tuple(CODIS)

codis_sero = "('D04885','S21285','S21085','S23085','S22985','S31385','S31285','S31185','008803','003884','003885','003886','003889','003890','003901','003902','S22885','S21385','S38485','007032','006998',\
            '006999','007000','S21585','S21185','S21485','D05485','SC137','SC136','SC138','SC139','SC140','SC201','S20044', 'S20344' ,'S20285', 'S20185', 'S30185', 'S20085', 'S38385', 'S39485', 'S20385', '007026'\
            ,'D01785','D01885', 'SG021', 'S10185','S10285','001994','V01085','V01285','011556','S10085','SC117','D01985','S09985','SC120','SG022','D01885', 'SG021', 'SC118', 'SC119',\
            'B06477','B06484','B06478','B06481','B06423','B06451','B06482','B06500','B06623','B06677','000490','000491','000492','B06476','B06723','B06777','B06422','B06758','001915','006703',\
            '006201','012902','012908','006202','012903','012906','006203','012905','006205','006195','006204','012904','006196','006197','009035','006199','011670',\
            'B01977','D00877','B01923','D00823','D00858','B01800','B01823','B01877','000435','000436','000433','000434','B01958','D00878','D00881','001826','001914','002134',\
            'SG002','SG026','SG027','S04685','012223','012244','012226','012238','006203','012234','006205','006195','012233','006196','006197','012236','006173','006199','11670','12221')"


def get_its(cim10):
    """."""
    # return its.decode('utf8').encode('latin1')
    return INVERS[cim10].decode('utf8').encode('latin1')
    
def get_proves(lab):

    if lab in ('D04885','S21285','S21085','S23085','S22985','S31385','S31285','S31185','008803','003884','003885','003886','003889','003890','003901','003902','S22885','S21385','S38485','007032','006998','006999','007000','S21585','S21185','S21485','D05485','SC137','SC136','SC138','SC139','SC140','SC201'):
        sero = 'Serologia VIH'
    elif lab in ('S20044', 'S20344' ,'S20285', 'S20185', 'S30185', 'S20085', 'S38385', 'S39485', 'S20385', '007026'):
        sero = 'Serologia sífilis'
    elif lab in ('D01785','D01885', 'SG021', 'S10185','S10285','001994','V01085','V01285','011556','S10085','SC117','D01985','S09985','SC120','SG022','D01885', 'SG021', 'SC118', 'SC119'):
        sero = 'Hepatitis B'
    elif lab in ('B06477','B06484','B06478','B06481','B06423','B06451','B06482','B06500','B06623','B06677','000490','000491','000492','B06476','B06723','B06777','B06422','B06758','001915','006703',\
            '006201','012902','012908','006202','012903','012906','006203','012905','006205','006195','006204','012904','006196','006197','009035','006199','011670'):
        sero = 'Prova Gonococ'
    elif lab in ('B01977','D00877','B01923','D00823','D00858','B01800','B01823','B01877','000435','000436','000433','000434','B01958','D00878','D00881','001826','001914','002134',\
            'SG002','SG026','SG027','S04685','012223','012244','012226','012238','006203','012234','006205','006195','012233','006196','006197','012236','006173','006199','11670','12221'):
        sero = 'Proves Clamydia'
    else:
        sero = 'error'
    return sero.decode('utf8').encode('latin1')
    

recomptes = Counter()   

sql = '''
    select
        id_cip_sec,
        pr_cod_ps,
        pr_up
    from
        problemes,
        nodrizas.dextraccio
    where
        pr_cod_o_ps = 'C'
        and pr_hist = 1
        and pr_dde <= data_ext
        and (
            pr_data_baixa = 0
            or year(pr_data_baixa) > '{year}'
        )
        and year(pr_dde) = '{year}'
        and pr_cod_ps in {codis}
        '''.format(year= YEAR, codis= CODIS)

for id, cod, up in u.getAll(sql, imp):
    if up in centres:
        desc = centres[up]['desc']
        ambit = centres[up]['ambdesc']
        sap = centres[up]['dapdesc']
        desc_its = get_its(cod)
        es_eap = 0
        if up in eaps:
            es_eap = 1
        recomptes[(ambit, sap, up, desc, es_eap, desc_its)] += 1
        
upload = []
for (ambit, sap, up, desc, es_eap, desc_its), d in recomptes.items():
    upload.append([ambit, sap, up, desc, es_eap, desc_its, d])


header = [("amb", "sap", "cod", "des", "es_eap", "its", "n")]
file = u.tempFolder + 'ITS_Catalunya_{}.csv'.format(YEAR)
u.writeCSV(file, header + sorted(upload), sep=';')
"""
recomptess = Counter()   
sql = "select id_cip_sec, cr_codi_prova_ics, codi_up \
        from laboratori, nodrizas.dextraccio \
        where  year(cr_data_reg)='{}' and\
        cr_codi_prova_ics in {}".format(YEAR, codis_sero)
for id, cod, up in getAll(sql, imp):
    if up in centres:
        desc = centres[up]['desc']
        ambit = centres[up]['ambdesc']
        sap = centres[up]['dapdesc']
        desc_its = get_proves(cod)
        es_eap = 0
        if up in eaps:
            es_eap = 1
        recomptess[(ambit, sap, up,desc,es_eap,desc_its)] += 1
        
upload = []
for (ambit, dap, up,desc,es_eap,desc_its), d in recomptess.items():
    upload.append([ambit, dap, up,desc,es_eap,desc_its, d])


file = tempFolder + 'ITS_proves_Catalunya.txt'
writeCSV(file, upload, sep=';')
"""
