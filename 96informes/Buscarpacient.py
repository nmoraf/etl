# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

"""
Procés per mirar visites de cips concrets. Informar set de dnis amb les possibilitats diferents i executar
El deixo buit per tema de confidencialitat
Posar a fitxa el codi de la fitxa que envien
"""

printTime("Inici")
"""
cognom1 = ''
cognom2 = ''

for sector in sectors:
    sql = "select usua_dni, usua_nom, usua_cognom1, usua_cognom2, usua_uab_up, usua_situacio, usua_cip from usutb040 where usua_cognom1='{0}' and usua_cognom2='{1}'".format(cognom1, cognom2)
    for id, nom, cog1, cog2, up, sit, cip in getAll(sql, sector):
        print id, cip, nom, cog1, cog2
"""
dni = ''
fitxa = ''
hashos = {}
for sector in sectors:
    sql = "select usua_dni, usua_nom, usua_cognom1, usua_cognom2, usua_uab_up, usua_situacio, usua_cip from usutb040 where usua_dni ='{}'".format(dni)
    for id, nom, cog1, cog2, up, sit, cip in getAll(sql, sector):
        print sector, id, nom, cog1, cog2, up, sit
        sql = "select cip_cip_anterior, cip_usuari_cip from usutb011 where cip_usuari_cip = '{}'".format(cip)
        for cipa, cipu in getAll(sql, sector):
            print sector, cip, cipa, cipu
            sql = "select usua_cip, usua_cip_cod from pdptb101 where usua_cip = '{}'".format(cipu)
            for c,h in getAll(sql, 'pdp'):
                hashos[(sector, h)] = True
print hashos

cips = {}
for  (sector, hash), d in hashos.items():
    sql = "select id_cip, id_cip_sec from u11 where codi_sector='{}' and hash_d='{}'".format(sector, hash)
    for id_cip, id_cip_sec in getAll(sql, 'import'):
        cips[id_cip] = True

print cips


centres = {}
sql = "select	a.amb_codi_amb	,a.amb_desc_amb	,s.dap_codi_dap	,s.dap_desc_dap	,c.up_codi_up_scs	,u.up_codi_up_ics,u.up_desc_up_ics, e_p_cod_ep \
        from	cat_gcctb006 s,cat_gcctb008 c,cat_gcctb007 u,cat_gcctb005 a\
        where u.dap_codi_dap = s.dap_codi_dap\
        and c.up_codi_up_ics = u.up_codi_up_ics\
		and s.amb_codi_amb=a.amb_codi_amb\
        and c.up_data_baixa = 0 \
        and dap_data_baixa = 0 \
        and u.up_data_baixa = 0 \
        and amb_data_baixa = 0 "
for amb, ambdesc,dap,dapdesc,up,br,desc, ep in getAll(sql, 'import'):
    centres[up] = {'ambit':amb, 'ambdesc':ambdesc,'dap':dap,'dapdesc':dapdesc,'br':br,'desc':desc, 'ep':ep}

sql = "select espe_codi_especialitat, espe_especialitat \
               from cat_pritb023"
especialitats = {cod: des for (cod, des) in getAll(sql, 'import')}
 
#Visites 
upload = [] 
for cip, d in cips.items():
    print cip
    sql = "select visi_data_visita, visi_up, s_espe_codi_especialitat from visites where id_cip = '{}' and visi_situacio_visita='R'".format(cip)
    for data, up, espe in getAll(sql, 'import'):
        if up in centres:
            desc = centres[up]['desc']
            descespe = especialitats[espe]
            ep = centres[up]['ep']
            upload.append([data, up, desc, descespe])
            print data, up, ep, desc
    

file = tempFolder + 'Visites_' + fitxa + '.txt'
writeCSV(file, upload, sep=';')

#Problemes

cataleg_ps = {}
sql = "select ps_cod, ps_des from cat_prstb001"
for ps, psdes in getAll(sql, 'import'):
    cataleg_ps[ps] = psdes
    

upload = [] 
for cip, d in cips.items():
    print cip
    sql = "select pr_cod_ps, pr_dde, pr_dba from problemes, nodrizas.dextraccio where id_cip = '{}' and  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext)".format(cip)
    for ps, dde, dba in getAll(sql, 'import'):
        if ps in cataleg_ps:
            desc = cataleg_ps[ps]
            upload.append([dde, ps, desc, dba])
            print dde, ps, desc, dba
    

file = tempFolder + 'Problemes_' + fitxa + '.txt'
writeCSV(file, upload, sep=';')

#Derivacions
servei_d = {}

sql = 'select servei, desc_servei from cat_ct_der_serveid'
for servei, desc in getAll(sql, 'import'):
    servei_d[servei] = desc

upload = [] 
for cip, d in cips.items():
    print cip
    sql = "select oc_data, inf_servei_d_codi, motiu from nod_derivacions where id_cip = '{}'".format(cip)
    for dat, serv, motiu in getAll(sql, 'nodrizas'):
        descS = servei_d[serv]
        try:
            descps = cataleg_ps[motiu]
        except KeyError:
            descps = None
        upload.append([dat, descS, descps])
        print data, descS, descps
    

file = tempFolder + 'Derivacions_' + fitxa + '.txt'
writeCSV(file, upload, sep=';')

#Proves


upload = [] 
for cip, d in cips.items():
    print cip
    sql = "select codi_sector, oc_data, inf_codi_prova from nod_proves where id_cip = '{}'".format(cip)
    for sector, dat, motiu in getAll(sql, 'nodrizas'):
        print sector
        sql = "select pro_desc_prova from gcctb003 where pro_codi_prova='{}'".format(motiu)
        desc_prova, = getOne(sql, str(sector))
        upload.append([dat, motiu, desc_prova])
        print dat, motiu, desc_prova
    

file = tempFolder + 'Proves_' + fitxa + '.txt'
writeCSV(file, upload, sep=';')



printTime("Fi")