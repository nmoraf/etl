# -*- coding: utf8 -*-

"""
Refredats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'sisap_covid_agr_pneum_urg'
db = 'redics'

codis = "('J189', 'J1289','J1289', 'J189','J189', 'J189','J129','J129','J159', 'J129', 'J129', 'J189','J159', 'J129','J1281', 'J180', 'J181', 'J1289','J159', 'J181', \
'J180', 'J1000','J181', 'J168', 'J13', 'J1000', 'J188','J158', 'J151', 'J1000', 'J1000', 'J1000','J122', 'J157', 'J121', 'J156','J1008', 'J1100', \
'J120', 'J123','J14', 'J15211', 'J1529', 'J154', 'J182', 'J168')"

class Pneumonies(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_refredats()
        self.dona_grants()
        
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        

        pneumo = c.Counter()
        sql = """select hash, u_data, u_condicio
                from sisap_covid_pac_urgencies where 
                 (substr(u_motiu, 0,5) in {0} or substr(u_dd1, 0,5) in {0} or substr(u_dd2, 0,5) in {0} or substr(u_dd3, 0,5) in {0} or substr(u_dd4, 0,5) in {0})""".format(codis)
        for hash, data, condicio in u.getAll(sql, db):
            pneumo[(data, condicio)] += 1      
            
           
        cols = ("data date", "condicio varchar2(100)", "recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (data, condicio), n in pneumo.items():
            n = int (n)
            upload.append([data, condicio, n])
        u.listToTable(upload, tb, db)       
      
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db) 
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Pneumonies()
    
    u.printTime("Final")      