from sisapUtils import *
import random
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
#Validation
write=False
validation=True
up_codi=("00380",)

alt = "altres"
nod = "nodrizas"
assig=nod+".assignada_tot"
odn=nod+".odn_variables"
peces="import.odn508"
#limit=2000
codi="iad0019"
#Agrupador indicador de ARC

def get_tabac_dictionaries():
    sql="select id_cip_sec,tab from {}.eqa_tabac where id_cip_sec <0 and last=1 ".format(nod)
    tabac_id=defaultdict(set)
    for id,tab in getAll(sql,nod):
        tabac_id[tab].add(id)
    return tabac_id



def get_up_rows(tabac_id):
    up_rows=[]
    indicador=defaultdict(Counter)
    all_id=set()
    sql = "select id_cip_sec, up from {}.jail_assignada where ates=1".format(nod)
    for id, up in getAll(sql,nod):
        for state,ids in tabac_id.items():
            if id in ids:
                indicador[up][state]+=1
        indicador[up][3]+=1
        all_id.add(id)
       
    up_rows = [(up,  indicador[up][0], indicador[up][1], indicador[up][2],indicador[up][3] )
                          for up in indicador.keys()]
    return up_rows,all_id

###
if __name__ == '__main__':
# comencem!
    printTime('inici')
    non=set()
    tabac_id=get_tabac_dictionaries()
    for key in tabac_id.keys():
        print(key)
        print(random.sample(tabac_id[key],10))
    printTime("Registros de tabaco OK")
    
    up_rows,all_id=get_up_rows(tabac_id)
    printTime("All ups OK")
    #tabac_values={id for lista in tabac_id.values() for id in lista}
    #print(all_id- tabac_values)

    if write:
        with open("tabac_informe.txt","wb") as out_file:
            row_format ="{:>20}" * len(up_rows[0])+"\n"
            header=["UP","NO_FUMADORES","FUMADORES","EX_FUMADORES", "POBLACION TOTAL"]
            out_file.write(row_format.format(*header))
            for row in up_rows:
                out_file.write(row_format.format(*row))