# -*- coding: utf-8 -*-
import sisapUtils as u
from datetime import datetime as d
import pandas as pd
import pprint as pp

print(d.now())

# Bitacora
"""
Pacients vulnerables de presons:
LMB: 2020-03-13
card# 377 https://trello.com/c/UxHCHHtz


# Pomodoros:
2020-03-13 11:07:28.625000 Start

# Petició:
Penjar una taula a REDICS amb CIP i nom i cognoms per passar a presons.

Objectiu: Detectar els malalts vulnerables de les presons per a prendre mesures preventives front el coronavirus. 
Població: Població actualment assignada a una presó que tinguin alguna de les condicions marcades amb *
Període: actual
Destinatari: Pressons


Variables
    Poblacio: (assignada_tot_with_jail)
        CIP
        Nom i cognoms
        Edat
        Presó
        *PCC (ER0001): data d’inclusió en el programa
            http://sisap-umi.eines.portalics/indicador/concepte/4162/ver/
        *MACA: data d’inclusió en el programa
            http://sisap-umi.eines.portalics/indicador/concepte/4068/ver/
    gma:
        *GMA4 (agrupador 3): si tenim data, sinó posa un 1.
            http://sisap-umi.eines.portalics/indicador/concepte/4022/ver/
    Problemes:
        *Neoplàsia maligna: data de diagnòstic de la neoplàsia maligna activa.
            http://sisap-umi.eines.portalics/indicador/concepte/3347/ver/
            agrupador = 722
        *Malaltia respiratòria crònica: data del primer diagnòstic que aparegui de MPOC o enfisema
            agrupador: 62
            http://sisap-umi.eines.portalics/indicador/concepte/4650/ver/
    Variables/laboratorio:
        *CD4 <200, data de la última determinació amb els CD4 <200
            http://sisap-umi.eines.portalics/indicador/concepte/3787/ver/
    Tractaments:
        principis actius ATC(descripcions) vigents:


Notes:
   1) els GMA diu la Isabel que no estàn a presons,
        no sé si es pot fer per id_cip i posar qualsevol dada sigui de quin sector.
        Tant de GMA com de la resta de variables.
        Si no es pot fer així, fes-ho només amb dades del sector.
"""
# objetos principales

sql_nombres = """SELECT
                    usua_cip,
                    concat(concat(ltrim(rtrim(usua_c1)), CASE WHEN ltrim(rtrim(usua_c2)) IS null  THEN '' ELSE concat(' ',ltrim(rtrim(usua_c2))) END), ', ')||ltrim(rtrim(usua_nom)) as nom
                    -- usua_n as nom,
                    -- usua_c1 as cognom1,
                    -- usua_c2 as cognom2,
                    -- usua_cip_rca,
                    -- usua_cip_sns,
                    -- usua_dni
                FROM USUTB040"""
cip_to_name = {cip: nom for (cip, nom) in u.getAll(sql_nombres, '6951')}


sql_cips = """SELECT
                usua_cip as cip,
                usua_cip_cod as hash
            FROM PDPTB101"""
hash_to_cip = {hash: cip for (cip, hash) in u.getAll(sql_cips, '6951a') if cip in cip_to_name}

ids_sql = """
        select
            left(group_concat(distinct id_cip_sec order by id_cip_sec),locate(',',group_concat(distinct id_cip_sec order by id_cip_sec))-1) as id_cip_sec_pressons,
            replace(left(group_concat(distinct id_cip_sec order by id_cip desc),locate(',',group_concat(distinct id_cip_sec order by id_cip_sec desc))-1), ',', '') as id_cip_primaria
        from import.u11withjail
        group by hash_d
        having
            left(group_concat(distinct id_cip_sec order by id_cip_sec),1)='-'
            and left(group_concat(distinct id_cip_sec order by id_cip_sec desc),1)<>'-'
        """

ids = {row[0]: row[1] for row in u.getAll(ids_sql, 'import')}


cd4_sql = """
    select
        distinct s.id_cip_sec, s.dat
    from
        nodrizas.jail_serologies s
    inner join (
        select
            id_cip_sec,
            max(dat) as dat_max
        from
            nodrizas.jail_serologies
        where
            cod = 'I89372'
        group by
            id_cip_sec ) x on
        s.id_cip_sec = x.id_cip_sec
        and s.dat = x.dat_max
        and s.cod = 'I89372'
        and s.val < 200
    """

cd4 = {row[0]: row[1] for row in u.getAll(cd4_sql,'nodrizas')}

meta ={
    "poblacio":{
        "db": "nodrizas",
        "tb": "jail_assignada",
        "colset": "id_cip_sec, up, edat, sexe, pcc, maca",
        "flt": "WHERE id_cip_sec < 0"},
    "problemes":{
        "db": "nodrizas",
        "tb": "eqa_problemes",
        "colset": "id_cip_sec, ps, dde",
        "flt": "WHERE id_cip_sec < 0 and ps in (62, 722)"}, # agrupador 62: MPOC, 722: neo maligna
    # "cd4":{
    #     "db": "nodrizas",
    #     "tb": "jail_serologies",
    #     "colset": "",
    #     "flt": ""},
    "tractaments":{
        "db": "import_jail",
        "tb": "tractaments",
        "colset": "id_cip_sec, ppfmc_atccodi",
        "flt": "where ppfmc_data_fi > (select data_ext from nodrizas.dextraccio) and ppfmc_pf_codi <> 0"},
    "gma":{
        "db": "import",
        "tb": "gma",
        "colset": "id_cip, id_cip_sec, codi_sector, gma_pniv",
        "flt":"where gma_pniv = 4"},
    "u11":{
        "db": "nodrizas",
        "tb": "jail_u11",
        "colset": "id_cip_sec, hash_d, codi_sector"}
}

def descTable(targetTable, n=3):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"]
    print("\n {} <- {}.{} \n".format(targetTable, meta[targetTable]["db"], meta[targetTable]["tb"]))
    print("{} ->\n".format([col for col in u.getTableColumns(tb, db)]))
    print(cols)
    # flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    # sql = """select {} from {} {} limit {}""".format(cols, tb, flt, n)
    # for row in u.getAll(sql, db):
    #    print(row)

def objectify(targetTable):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"]
    flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    sql = """select distinct {} from {} {}""".format(cols, tb, flt)
    print(sql)
    tb_df = pd.DataFrame.from_records(data = [row for row in u.getAll(sql, db)], columns = cols.split(', '))
    meta[targetTable]["tb_df"] = tb_df
    if "rename" in meta[targetTable]:
        meta[targetTable]["tb_df"].rename(columns = meta[targetTable]["rename"], inplace= True)
    print(tb_df.info())
    print("{} size: {}".format(targetTable, meta[targetTable]['tb_df'].shape))

def hashify(targetTable, hashTable, index = 'id_cip_sec'):
    if targetTable == hashtable or targetTable == 'tractaments':
        meta[targetTable]["tb_df"].set_index('id_cip_sec', inplace = True)
    if targetTable != hashTable:
        meta[targetTable]["tb_df"]["hash"] = meta[hashTable]["tb_df"]["hash_d"]
        meta[targetTable]["tb_df"] = meta[targetTable]["tb_df"].reindex(columns =(['hash'] + list([col for col in meta[targetTable]["tb_df"].columns if col != 'hash'])))

# CONTROL DEL PROCESO
## para describir tablas y almacenar dataFrames en meta[table]["tb_df"]
for table in meta:
    inici = d.now()
    descTable(table, 2)
    objectify(table)
    final = d.now()
    print("{}: {}".format(final - inici, table))

### GMA4
meta["gma"]["tb_df"].rename(columns = {'id_cip_sec':'id_primaria'}, inplace= True)
meta["gma"]["tb_df"].set_index('id_primaria', inplace = True)

meta["poblacio"]["tb_df"].set_index('id_cip_sec', inplace = True)
meta["poblacio"]["tb_df"][meta["poblacio"]["tb_df"].index.duplicated()]

ids_df = pd.DataFrame.from_dict(ids, orient='index', columns=["id_primaria"])
ids_df[ids_df.index.duplicated()]
ids_df.head()
ids_df.info()

ids_df.reset_index(inplace= True)
ids_df.rename(columns = {'index':'id_cip_sec'}, inplace= True)
ids_df['id_cip_sec'] = ids_df['id_cip_sec'].astype('int64')
ids_df['id_primaria'] = ids_df['id_primaria'].astype('int64')

ids_df.set_index('id_cip_sec', inplace = True)

meta["poblacio"]["tb_df"]["id_primaria"] = ids_df


pob_gma_df = meta["poblacio"]["tb_df"][pd.notna(meta["poblacio"]["tb_df"]["id_primaria"])].copy()
pob_gma_df[pob_gma_df.index.duplicated()]
pob_gma_df.info()
pob_gma_df.head()
pob_gma_df['id_primaria'] = pob_gma_df['id_primaria'].astype('int64')

pob_gma_df.reset_index(inplace = True)
pob_gma_df.set_index('id_primaria', inplace = True)


meta["gma"]["tb_df"]["id_cip_sec"] = pob_gma_df["id_cip_sec"]
meta["gma"]["tb_df"].info()
meta["gma"]["tb_df"].head()

# meta["gma"]["tb_df"][pd.notna(meta["gma"]["tb_df"]["id_cip_sec"])]

pob_gma_df["gma"] = meta["gma"]["tb_df"]["gma_pniv"][meta["gma"]["tb_df"]["gma_pniv"]==4].copy()
pob_gma_df = pob_gma_df[pob_gma_df["gma"] == 4]

pob_gma_df.reset_index(inplace = True)
pob_gma_df.set_index('id_cip_sec', inplace = True)

## aqui pongo gma a la población 
meta["poblacio"]["tb_df"]["gma"] = pob_gma_df["gma"]

###### CD4
cd4_df = pd.DataFrame.from_dict(cd4, orient='index', columns=["cd4_data"])
cd4_df.reset_index(inplace=True)
cd4_df.rename(columns= {'index':'id_cip_sec'}, inplace=True)
cd4_df.index.name = 'id_cip_sec'

# neo maligna
meta["problemes"]["tb_df"].head()


# pob_tup = [tuple(x) for x in meta["problemes"]["tb_df"].values]
prob_tup = [tuple(x) for x in meta["problemes"]["tb_df"].values]
cd4_tup = [tuple(x) for x in cd4_df.values]


neo_first = {}
for row in prob_tup:
    id_cip_sec = row[0]
    prob = row[1]
    dde = row[2]
    if prob == 722:
        if id_cip_sec not in neo_first:
            neo_first[id_cip_sec] = dde
        else:
            if dde <= neo_first[id_cip_sec]:
                neo_first[id_cip_sec] = dde

neo_first_df = pd.DataFrame.from_dict(neo_first, orient='index', columns =['neo'])
neo_first_df.index.name = 'id_cip_sec'
meta["poblacio"]["tb_df"]['neo'] = neo_first_df

mpoc_first = {}
for row in prob_tup:
    id_cip_sec = row[0]
    prob = row[1]
    dde = row[2]
    if prob == 62:
        if id_cip_sec not in mpoc_first:
            mpoc_first[id_cip_sec] = dde
        else:
            if dde <= mpoc_first[id_cip_sec]:
                mpoc_first[id_cip_sec] = dde

mpoc_first_df = pd.DataFrame.from_dict(mpoc_first, orient='index', columns =['mpoc'])
mpoc_first_df.index.name = 'id_cip_sec'
meta["poblacio"]["tb_df"]['mpoc'] = mpoc_first_df

cd4_last = {}
for row in cd4_tup:
    id_cip_sec = row[0]
    dde = row[1]
    if id_cip_sec not in cd4_last:
        cd4_last[id_cip_sec] = dde
    else:
        if dde >= cd4_last[id_cip_sec]:
            cd4_last[id_cip_sec] = dde

cd4_last_df = pd.DataFrame.from_dict(cd4_last, orient='index', columns =['cd4'])
cd4_last_df.index.name = 'id_cip_sec'
meta["poblacio"]["tb_df"]['cd4'] = cd4_last_df

# consigo hashes
hashtable = "u11"
for table in ["u11", "poblacio", "tractaments"]:
    hashify(table, hashtable, index = 'id_cip_sec')
    table
    meta[table]["tb_df"].head()

# para francesc,

## estos son los objetos para la salida
meta["poblacio"]["tb_df"] # falta eliminar columna id_primaria, y añadir CIP_abierto y nombre
meta["tractaments"]["tb_df"] # falta concatenar las descripciones de los ATC, y añadir CIP_abierto y nombre.

meta["poblacio"]["tb_df"]["cip"] = meta["poblacio"]["tb_df"]["hash"].map(hash_to_cip)
meta["poblacio"]["tb_df"]["nom"] = meta["poblacio"]["tb_df"]["cip"].map(cip_to_name)
meta["poblacio"]["tb_df"]["cip"] = meta["poblacio"]["tb_df"]["cip"].str.decode('iso-8859-1').str.encode('utf-8')
meta["poblacio"]["tb_df"]["nom"] = meta["poblacio"]["tb_df"]["nom"].str.decode('iso-8859-1').str.encode('utf-8')

meta["tractaments"]["tb_df"]["cip"] = meta["tractaments"]["tb_df"]["hash"].map(hash_to_cip)
meta["tractaments"]["tb_df"]["nom"] = meta["tractaments"]["tb_df"]["cip"].map(cip_to_name)
meta["tractaments"]["tb_df"]["cip"] = meta["tractaments"]["tb_df"]["cip"].str.decode('iso-8859-1').str.encode('utf-8')
meta["tractaments"]["tb_df"]["nom"] = meta["tractaments"]["tb_df"]["nom"].str.decode('iso-8859-1').str.encode('utf-8')


#OUTPUT XLS POBLACIO
pob_columns = ['cip', 'nom', 'up', 'edat', 'sexe', 'pcc', 'maca', 'gma', 'neo', 'mpoc', 'cd4']
meta["poblacio"]["tb_df"][pob_columns][pd.notna(meta["poblacio"]["tb_df"]["cip"])].to_excel(u.tempFolder + "pob_vulnerable_def.xlsx", index = False)

# OUTPUT XLS TRACTAMENTS
tto_columns = ['cip', 'nom', 'ppfmc_atccodi']
meta["tractaments"]["tb_df"][tto_columns][pd.notna(meta["tractaments"]["tb_df"]["cip"])].to_excel(u.tempFolder + "pob_vulnerable_ttos_def.xlsx", index = False)

# TRACTAMENTS CONCATENATS
meta["tractaments"]["tb_df"].groupby(["cip"])['ppfmc_atccodi'].apply(lambda x: ','.join(x)).reset_index()
