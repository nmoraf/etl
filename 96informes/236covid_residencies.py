import sisapUtils as u
import pandas as pd
import numpy as np
import pickle

from datetime import datetime as d
# from dateutil.relativedelta import relativedelta

import pprint as pp

xlsx_contel = "MapaResidencies.xlsx"
adreces = pd.read_excel(u.tempFolder + xlsx_contel)
adreces_cols = ['id', 'nom', 'abs', 'comarca', 'municipi', 'localitat', 'nom_via', 'portal', 'llogaret', 'nom_carretera']
adreces = adreces[adreces_cols]
adreces_abs = adreces[pd.notna(adreces["abs"])]
adreces["abs_code"] = adreces_abs["abs"].str.split("-").str.get(0).str.replace(" ","").astype(dtype=np.int64)


# adreces.merge(centres, left_on="abs_code", right_on = "abs" )

adreces_notabs  = adreces[pd.isna(adreces["abs"])]

# adreces_notabs.head()
# adreces_abs.head()
# SELECT * FROM dwrcds.ft_residencies
test_xlsx_out = u.tempFolder + 'test_xlsx_out.xlsx'
exa_resis = "ft_residencies"
exa_owner = "DWRCDS"
exa_resis_df = pd.DataFrame([row for row in u.getAll("select * from {}.{}".format(exa_owner,exa_resis), 'exadata')], 
                            columns= [col for col in u.getTableColumns(exa_resis, 'exadata')])


len([row for row in u.getAll("select * from {}.{}".format(exa_owner,exa_resis), 'exadata')])
exa_resis_textFields = ['DISCAPACITATS','COB_PROFE','SITUACIO',"ORIGEN","NREG","NOMRECURS","AMBIT_TERRITORIAL","NOMESTABLIMENT","MUNICIPI","COMARCA","TERRITORI","TIPUS_RECURS","TIPUS_RESIDENTS","ADRESA","TIPOLOGIA_SALUT","COD_ABS"]
exa_resis_df.AMBIT_TERRITORIAL.str.decode('iso_8859-1').str.encode('utf-8').to_excel(test_xlsx_out)
exa_resis_df.NOMESTABLIMENT.str.decode('iso_8859-1').str.encode('utf-8').to_excel(test_xlsx_out)
exa_resis_df.MUNICIPI.str.decode('iso_8859-1').str.encode('utf-8').to_excel(test_xlsx_out)
exa_resis_df.COMARCA.str.decode('iso_8859-1').str.encode('utf-8').to_excel(test_xlsx_out)
exa_resis_df.TERRITORI.str.decode('iso_8859-1').str.encode('utf-8').to_excel(test_xlsx_out)
exa_resis_df.TIPUS_RESIDENTS.str.decode('iso_8859-1').str.encode('utf-8').to_excel(test_xlsx_out)

for descField in exa_resis_textFields:
            exa_resis_df[descField] = exa_resis_df[descField].str.decode('iso_8859-1').str.encode('utf-8')

exa_resis_df[["AMBIT_TERRITORIAL","NOMESTABLIMENT","MUNICIPI","COMARCA","TERRITORI","TIPUS_RESIDENTS"]].to_excel(test_xlsx_out)

exa_resis_df.sum()
exa_resis_df.to_excel(test_xlsx_out)
exa_resis.head()

for y in exa_resis_df.columns:
    print(y, exa_resis_df[y].dtype)
    
exa_resis_df.EXITUS.dtype

meta ={
    "gu":{
        "db": [sector for sector in u.sectors],
        "tb": "ppftb011",
        "colset": "gu_codi_grup, gu_desc, gu_up_residencia, gu_dba, gu_comentari",
        "flt": "WHERE upper(gu_desc) like '% COVI%'",},
    "ass_gu":{
        "db": [sector for sector in u.sectors],
        "tb": "ppftb012",
        "colset": "ug_codi_grup, ug_usua_cip, ug_ubic_1, ug_ubic_2",
        "flt": "",},}
    # "abs":{ 
    #     "db": "import",
    #     "tb": "cat_rittb001",
    #     "colset": "ABS_CODI_ABS, ABS_DATA_BAIXA , ABS_DATA_MODI, ABS_NOM_ABS, ABS_SECTOR_CODI_SECTOR, ABS_SECTOR_SIAP, ABS_CODI_UP",
    #     "flt": "",
    #     "encoding": ["ABS_NOM_ABS"]},
    # "residencies":{
    #     "db": "import",
    #     "tb": "cat_residencies",
    #     "colset": "territori_ext, residencia, residencia_desc_ext, residencia_desc, places, pacients, inclos, descripcio_sisap, up, up_desc, sap, sap_desc, ambit, ambit_desc",
    #     "flt": "WHERE inclos = 1",
    #     "encoding": ["territori_ext", "residencia_desc_ext", "residencia_desc", "descripcio_sisap", "up_desc", "sap_desc", "ambit_desc"]
    #     },
    # "residents":{
    #     "db": "import",
    #     "tb": """ institucionalitzats a inner join cat_ppftb011_def b on a.ug_codi_grup = b.gu_codi_grup and a.codi_sector = b.codi_sector""",
    #     "colset": "id_cip_sec, a.codi_sector, gu_up_residencia",
    #     "rename": {"gu_up_residencia": "up_residencia", "a.codi_sector": "codi_sector"},
    # },
    # "u11":{
    #     "db": "import",
    #     "tb": "u11",
    #     "colset": "id_cip_sec, hash_d, codi_sector",
    #     "flt": "",},
    }

gu = []
for sector in meta["gu"]["db"]:
    sql = """select distinct {}, {} from {} {}""".format(sector, meta["gu"]["colset"], meta["gu"]["tb"], meta["gu"]["flt"])
    for row in u.getAll(sql, sector+"sb"):
        gu.append(row)
    print(sql)
gu_df = pd.DataFrame(gu, columns =('sector', 'gu_codi_grup', 'gu_desc', 'gu_up_residencia', 'gu_dba', 'gu_comentari'))

ass_gu = []
for sector in meta["ass_gu"]["db"]:
    sql = """select distinct {}, {} from {} {}""".format(sector, meta["ass_gu"]["colset"], meta["ass_gu"]["tb"], meta["ass_gu"]["flt"])
    for row in u.getAll(sql, sector+"sb"):
        ass_gu.append(row)
    print(sql)
ass_gu_df = pd.DataFrame(ass_gu, columns =('sector', 'gu_codi_grup', 'ug_usua_cip', 'ug_ubic_1', 'ug_ubic_2'))

gu_df["gu_desc"] = gu_df["gu_desc"].str.decode('iso_8859-1').str.encode('utf-8')
gu_df["gu_comentari"] = gu_df["gu_comentari"].str.decode('iso_8859-1').str.encode('utf-8')

gu_df.to_excel(u.tempFolder + "hotels3.xlsx")

meta = pickle.load(open(u.tempFolder + "residencies.p", "rb"))

def descTable(targetTable, n=3):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"] if "colset" in meta[targetTable] else "*"
    print("\n {} <- {}.{} \n".format(targetTable, meta[targetTable]["db"], meta[targetTable]["tb"]))
    print("{} ->\n".format([col for col in u.getTableColumns(tb, db)]))
    print(cols)
    flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    sql = """select {} from {} {} limit {}""".format(cols, tb, flt, n)
    print(sql)
    for row in u.getAll(sql, db):
       print(row)

def objectify(targetTable):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"]
    flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    sql = """select distinct {} from {} {}""".format(cols, tb, flt)
    print(sql)
    tb_df = pd.DataFrame.from_records(data = [row for row in u.getAll(sql, db)], columns = cols.split(', '))
    meta[targetTable]["tb_df"] = tb_df
    if "rename" in meta[targetTable]:
        meta[targetTable]["tb_df"].rename(columns = meta[targetTable]["rename"], inplace= True)
    if "encoding" in meta[targetTable]:
        for descField in meta[targetTable]["encoding"]:
            meta[targetTable]["tb_df"][descField] = meta[targetTable]["tb_df"][descField].str.decode('iso_8859-1').str.encode('utf-8')
    print(tb_df.info())
    print("{} size: {}".format(targetTable, meta[targetTable]['tb_df'].shape))

def hashify(targetTable, hashTable, index = 'id_cip_sec'):
    meta[targetTable]["tb_df"].set_index('id_cip_sec', inplace = True)
    if targetTable != hashTable:
        meta[targetTable]["tb_df"]["hash"] = meta[hashTable]["tb_df"]["hash_d"]
        meta[targetTable]["tb_df"] = meta[targetTable]["tb_df"].reindex(columns =(['hash'] + list([col for col in meta[targetTable]["tb_df"].columns if col != 'hash'])))


# CONTROL DEL PROCESO
## para describir tablas y almacenar dataFrames en meta[table]["tb_df"]
for table in meta:
    inici = d.now()
    descTable(table, 2)
    objectify(table)
    final = d.now()
    print("{}: {}".format(final - inici, table))

for table in meta:
    print("/n", table)
    meta[table]["tb_df"].info()
    meta[table]["tb_df"][:5]
    meta[table]["tb_df"].columns

meta["residents_residencia"] = meta["residents"]
meta["residents_residencia"]["tb_df"] = meta["residents"]["tb_df"].merge(meta["residencies"]["tb_df"], left_on="up_residencia", right_on="residencia", how='inner')
meta["residents_residencia"]["tb_df"] = meta["residents_residencia"]["tb_df"][["id_cip_sec","up_residencia","descripcio_sisap","up"]]

# residents = meta["residents"]["tb_df"].copy()
# residencies = meta["residencies"]["tb_df"].copy()

## indexo por id_cip_sec y paso a hash
hashtable = "u11"
meta[hashtable]["tb_df"].reset_index(inplace=True)
for table in ["u11", "residents_residencia"]:
    hashify(table, hashtable, index = 'id_cip_sec')
    table
    meta[table]["tb_df"].head()


pickle.dump(meta, open(u.tempFolder + "residencies.p", "wb"))

# formato de columnas para crear en oracle
# [u.getColumnInformation(col, meta["residencies"]["tb"], meta["residencies"]["db"], desti = 'ora')['create'] for col in list(meta["residencies"]["tb_df"].columns)]

## upload
prefijo = "SISAP_"
db_desti = "redics"
for table in ["residents_residencia"]:
    tb_desti = prefijo + table
    cols = ['hash varchar(40)'] + ['up_residencia varchar2(5)', 'descripcio_sisap varchar2(400)', 'up varchar2(5)']
    dflist = list(meta[table]["tb_df"].itertuples(index= False, name=None))
    u.createTable(tb_desti, "({})".format(", ".join(cols)), db_desti, rm=True)
    u.listToTable(dflist, tb_desti, db_desti)
    u.grantSelect(tb_desti, ("PREDULMB","PREDUPRP","PREDUMMP"), db_desti)


# 53826 residents tienen residencies.residencia
#  4528 residents cuya up_residencia(n=107) no esta en residencies.residencia (a revisar con Frederic?)
#    51 residencies.residencia sin residentes (han dejado de usarse?, mal-clasificadas?)
# residents_residencia = residents.merge(residencies, left_on="up_residencia", right_on="residencia", how='inner')

# meta["residents_residencia"]["tb_df"].groupby(["descripcio_sisap"]).agg({"id_cip_sec":"count"}).sort_values("id_cip_sec", ascending=False)



# residents_left = residents.merge(residencies, left_on="up_residencia", right_on="residencia", how="left")
# residents_left.to_excel(u.tempFolder + "residents_left.xlsx")
# residents_left.query("residencia == residencia").groupby("up_residencia").agg({"id_cip_sec": "count", "residencia_desc": "min"}).sort_values("id_cip_sec", ascending= False)
# residents_left.iloc[1]


# u11 = meta["u11"]["tb_df"].copy()


# abs_cols = "ABS_CODI_ABS, ABS_DATA_BAIXA , ABS_DATA_MODI, ABS_NOM_ABS, ABS_SECTOR_CODI_SECTOR, ABS_SECTOR_SIAP, ABS_CODI_UP"
# sql = "select {} from cat_rittb001".format(abs_cols)


#### Contel:

abs_df = meta["abs"]["tb_df"].copy()

residencies = meta["residencies"]["tb_df"].copy()
residencies_abs = residencies.merge(abs_df, left_on="up", right_on="ABS_CODI_UP", how= 'left').to_excel(u.tempFolder + "residencies_abs.xlsx")

residencies_abs.merge(adreces_abs, how='left', left_on="ABS_CODI_ABS", right_on="abs_code").to_excel(u.tempFolder + "test.xlsx")

adreces_abs.columns

residencies.columns

abs_df.head()
abs_df.info()
abs_df.to_excel(u.tempFolder + "test.xlsx")
meta["abs"]["tb_df"].to_excel(u.tempFolder + "test.xlsx")

meta["residencies"]["tb_df"].to_excel(u.tempFolder + "test.xlsx")

# residencies_cols = "TERRITORI_EXT,RESIDENCIA,RESIDENCIA_DESC_EXT,RESIDENCIA_DESC,PLACES,PACIENTS,INCLOS,DESCRIPCIO_SISAP,UP,UP_DESC,SAP,SAP_DESC,AMBIT,AMBIT_DESC"
# sql = "select {} from import.cat_residencies where inclos = 1".format(residencies_cols)
# residencies_df = pd.DataFrame.from_records(data = [row for row in u.getAll(sql, "import")], columns = residencies_cols.split(','))
# residencies_df.head()



# residencies_df["AMBIT_DESC"] = residencies_df["AMBIT_DESC"].str.decode('iso_8859-1').str.encode('utf-8')
# residencies_df[["RESIDENCIA", "AMBIT_DESC"]].to_excel(u.tempFolder + "test.xlsx")
