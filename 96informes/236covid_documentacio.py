# encoding: utf8
import pprint as pp
from collections import defaultdict
from datetime import datetime as dt

import markdown as md

import covid_docutils as res
import sisapUtils as u

db = "redics"
today = dt.now().date()
doc = u.tempFolder + "doc_taules_sisap_coronavirus.html"

html_meta = u"""<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>"""
html_head = "\n".join(["<head>",
                       u"<title>Documentació sisap_coronavirus</title>",
                       html_meta,
                       "</head>",])

md_title = u"#Documentació de taules SISAP_CORONAVIRUS a REDICS/Exadata \n*Versió: ({})* \n".format(today)
md_descripcio = u"""Els ​usuaris de Redics/exadata (bbdd) ​poden consultar els objectes que aquí es descriuen, n’hi ha 2 conjunts d’objectes que es diferencien per el prefixe:\n\n
- Prefixe ​**sisap_covid_**​NomDeTaula: Correspon a les taules complertes (adequades per a l\'us central) i existeixen només a redics. \n
> com que no pertanyen als usuaris de la bbdd, els usuaris amb permisos de consulta sobre elles han de der servir el nom de propietari, tal que així: \n
>        select * from PREDUFFA.sisap_covid_NomDeTaula \n\n
- el prefixe ​**sisap_coronavirus_**​NomDeTaula​: \n
> En ​exadata ​correspon 1:1 amb les taules **sisap_covid_** i canvien el seu nom només per motius de compatiblitat \n
> en ​redics​: son sinonims de cada usuari que els hi dona accés a vistes (ja filtrades segons la SAP i/o Àmbit que correspon a cada  usuari de bbdd) \n
> com que cada usuari veu les seves, no cal posar el nom del popietari, ej: \n
>        SELECT * FROM sisap_coronavirus"""
md_tb_header = u"|columna|descripció|"
md_tb_sep = u"|---|---|"

sql = """
    select
        synonym_name,
        lpad(table_name, length(table_name) -2)
    from
        all_synonyms
    where
        owner = 'PREDUXMG'
        and synonym_name like 'SISAP_CORONAVIRUS%'
"""

sql_cols = """
    select
        column_id,
        column_name
    from
        all_tab_columns
    where
        table_name = '{}'
"""

corona_tb = {row[0]: {"tb": row[1]} for row in u.getAll(sql, db)}

for vw in corona_tb:
    tb = corona_tb[vw]["tb"]
    if "_CAT" in vw:
        corona_tb[vw]["class"] = "catalegs"
    elif "_RES" in vw:
        corona_tb[vw]["class"] = "residencies"
    else:
        corona_tb[vw]["class"] = "generals"
    corona_tb[vw]["col_dict"] = defaultdict()
    corona_tb[vw]["col_to_comment"] = defaultdict()
    col_attr = u.getAll(sql_cols.format(tb), db)
    for row in col_attr:
        col_order = row[0]
        col_name = row[1]
        if col_name in res.col_des:
            corona_tb[vw]["col_dict"][col_order] = {col_name: res.col_des[col_name]}
        else:
            corona_tb[vw]["col_dict"][col_order] = {col_name: "---"}
            corona_tb[vw]["col_to_comment"][col_order] = {col_name: ""}

for s in sorted(corona_tb):
    view_des = u"###{}\n{}".format(s, res.tab_des[s])
    tb_name = u"*{}*".format(corona_tb[s]["tb"])
    section_head = "\n\n".join([view_des, tb_name])
    tb_ls = [md_tb_header, md_tb_sep]
    for col in sorted(corona_tb[s]["col_dict"]):
        col_attr = corona_tb[s]["col_dict"][col].items()
        tb_row = u"|**{}**|{}|".format(col_attr[0][0], col_attr[0][1])
        tb_ls.append(tb_row)
    md_tb = "\n".join(tb_ls)
    corona_tb[s]["md_section"] = "\n\n".join([section_head, md_tb])

md_sections = "##Catalegs"
for s in sorted(corona_tb):
    if corona_tb[s]["class"] == "catalegs":
        md_sections = "\n\n".join([md_sections, corona_tb[s]["md_section"]])

md_sections = "\n\n".join([md_sections, "##Taules Generals"])
for s in sorted(corona_tb):
    if corona_tb[s]["class"] == "generals":
        md_sections = "\n\n".join([md_sections, corona_tb[s]["md_section"]])

md_sections = "\n\n".join([md_sections, "##Taules de Residencies"])
for s in sorted(corona_tb):
    if corona_tb[s]["class"] == "residencies":
        md_sections = "\n\n".join([md_sections, corona_tb[s]["md_section"]])

md_mashup = "\n\n".join([md_title,
                    "[TOC]",
                    md_descripcio,
                    md_sections])

html_body = md.markdown(md_mashup, extensions= ['toc','tables'])

html = "\n".join(["<!DOCTYPE html>", "<html>", html_head,
                  "<body>", html_body, "</body>", "</html>"])

with open(doc, "w") as output_file:
    output_file.write(html.encode('utf8'))

# campos pendientes de comentar
"""
for s in sorted(corona_tb):
    print('\n-- s: {}, t: {}'.format(s,corona_tb[s]['tb']))
    for col in corona_tb[s]['col_to_comment']:
        print('{}: {}'.format(col, corona_tb[s]['col_to_comment'][col]))
"""
