# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

#visites realitzades 2016 i 2017

centres = {scs_c: (ics_c, amb_d, sap_d, ics_d) for (scs_c, ics_c, amb_d, sap_d, ics_d) in getAll("select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc from cat_centres where ep='0208'", 'nodrizas')}
moduls = {}
for sec, cen, cla, ser, cod in getAll("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul from cat_vistb027 where modu_codi_uab <> '' ", 'import'):
    moduls[(sec, cen, cla, ser, cod)] = True

programades = Counter()
realitzades = Counter()
realitzades_qc = Counter()
h48 = Counter()
hmes48 = Counter()
h48r = Counter()
hmes48r = Counter()

for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
    if dat == 2016 or dat == 2017:
        sql = "select  codi_sector, visi_centre_codi_centre, visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul, visi_data_visita, extract(year_month from visi_data_visita), visi_dia_peticio,visi_up, visi_situacio_visita \
               from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_data_baixa = 0 {}".format(table, ' limit 10' if debug else '')
        for sec, cen, cla, ser, cod,data, periode,peti, up,sit in getAll(sql, 'import'):
            if up in centres:
                if (sec, cen, cla, ser, cod) in moduls:
                    programades[periode] += 1
                    if sit == 'R':
                        realitzades[periode] += 1
                        realitzades_qc[periode] += 1
                    elif sit == 'P':
                        realitzades_qc[periode] += 1
                    b = daysBetween(peti,data)
                    if 0 <= b <= 2:
                        h48[periode] += 1
                        if sit == 'R':
                            h48r[periode] += 1
                    else:
                        hmes48[periode] += 1
                        if sit == 'R':
                            hmes48r[periode] += 1
            
data_file = []
for id, count in programades.items():
    fetes = realitzades[id]
    fetes_qc = realitzades_qc[id]
    menys48H = h48[id]
    mes48H = hmes48[id]
    menys48Hr = h48r[id]
    mes48Hr = hmes48r[id]
    data_file.append([id, fetes, fetes_qc,count,menys48H, mes48H,menys48Hr, mes48Hr])

if data_file:
    file = tempFolder + 'Visites_realitzades_2017_2016.txt'
    writeCSV(file, data_file, sep=';')