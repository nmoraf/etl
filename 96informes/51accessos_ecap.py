import collections as c

import sisapUtils as u


pantalla = 'SISDAP'

sql = "select cip_pant_usuari, cip_pant_cip, cip_pant_data \
       from pritb700 where cip_pant_pant = '{}'".format(pantalla)
dades = c.defaultdict(set)
for sector in u.sectors:
    print sector
    for usuari, pacient, data in u.getAll(sql, sector):
        year = data.year
        dades[(year, sector, 'usuaris')].add(usuari)
        dades[(year, sector, 'pacients')].add(pacient)

resultat = []
for (year, sector, tipus), conjunt in dades.items():
    if tipus == 'usuaris':
        this = (year, sector, len(conjunt), len(dades[(year, sector, 'pacients')]))
        resultat.append(this)

header = [('any', 'sector', 'professionals', 'pacients')]
u.writeCSV(u.tempFolder + 'accessos_demencia.csv', header + sorted(resultat), sep=';')
