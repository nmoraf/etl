# coding: iso-8859-1
# Pol Perez pla cures, Ermengol, Desembre 2016
# Actualitzar Gener 2018 per anys 2014, 2016, 2017

import sisapUtils as u
from collections import defaultdict, Counter

YEAR = 2019

imp = 'import'
db = 'test'
nod = 'nodrizas'

centres = {}
sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc from cat_centres"
for up, br, desc, sap, ambit in u.getAll(sql, nod):
    centres[up] = {'eap': desc, 'sap': sap, 'amb': ambit}

pob = {}
a = 0
sql = 'select id_cip_sec, up from assignada_tot where edat > 14'
for id, up in u.getAll(sql, nod):
    try:
        eap = centres[up]['eap']
        sap = centres[up]['sap']
        ambit = centres[up]['amb']
    except KeyError:
        a += 1
    pob[id] = {'up': up, 'eap': eap, 'sap': sap, 'ambit': ambit}

tipus_ps = {} 
sql = 'select ps_cod, ps_cat, ps_des from cat_prstb001'
for ps, tipus, desc in u.getAll(sql, imp):
    tipus_ps[ps] = {'tipus': tipus, 'desc': desc}

pla, nandes, cims = Counter(), Counter(), Counter()
cat_nandas, cat_cims = {}, {}

sql = '''
        select
            id_cip_sec,
            date_format(spc_data_alta, '%Y%m'),
            spc_cod_ps,
            '{year}' as year_
        from
            pla1
        where
            year(spc_data_alta) = '{year}'
        '''.format(year= YEAR)

for id, dat, ps, anys in u.getAll(sql, imp):
    try:
        up = pob[id]['up']
        desc = pob[id]['eap']
        sap = pob[id]['sap']
        ambit = pob[id]['ambit']
    except KeyError:
        continue
    try:
        tipus = tipus_ps[ps]['tipus']
    except KeyError:
        continue
    if tipus == 'C' or tipus == 'N':
        pla[(up, desc, sap, ambit, dat, tipus)] += 1
    if tipus == 'N':
        nandes[anys, ps] += 1
        cat_nandas[ps] = tipus_ps[ps]['desc']
    if tipus == 'C':
        cims[anys, ps] += 1
        cat_cims[ps] = tipus_ps[ps]['desc']

upload1 = []
for (up, desc, sap, ambit, dat, tipus), d in pla.items():
    upload1.append([up, desc, sap, ambit, dat, tipus, d])

header = [('up', 'desc', 'sap', 'ambit', 'periode', 'cataleg', 'n')]
file = u.tempFolder + 'Plans_cura_{}.csv'.format(YEAR)
u.writeCSV(file, header + upload1, sep=';')

upload2 = []
for (anys, ps), d in nandes.items():
    desc = cat_nandas[ps]
    upload2.append([anys, ps, desc, d])

header = [('any', 'nanda', 'desc', 'n')]
file = u.tempFolder + 'Top_Nandes_{}.csv'.format(YEAR)
u.writeCSV(file, header + upload2, sep=';')

upload3 = []
for (anys, ps), d in cims.items():
    desc = cat_cims[ps]
    upload3.append([anys, ps, desc, d])

header = [('any', 'cim10', 'desc', 'n')]
file = u.tempFolder + 'Top_CIMs_{}.csv'.format(YEAR)
u.writeCSV(file, header + upload3, sep=';')
