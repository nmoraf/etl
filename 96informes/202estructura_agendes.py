# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import math as m

import sisapUtils as u


MES = d.date.today().month
SERVEI = "MG"

TB = "SISAP_ESTRUCTURA_AGENDES"
DB = "redics"
USERS = ("PREDUMMP", "PREDUECR", "PREDUPRP")


class Estructura(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.get_sectors()

    @staticmethod
    def _get_all_params():
        """."""
        for tipus in ("9C", "9D", "9R", "9T", "9E"):
            for parametre in ("minuts", "forats", "espontanies"):
                yield tipus, parametre

    def create_table(self):
        """."""
        cols = ["{} varchar2(255)".format(col)
                for col in ("br", "ambit", "sap", "eap",
                            "centre", "codi", "agenda", "dni", "dia",
                            "hora_inici", "hora_final")]
        for tipus, parametre in Estructura._get_all_params():
            cols.append("{}_{} number(8,2)".format(parametre, tipus))
        u.createTable(TB, "({})".format(", ".join(cols)), DB, rm=True)
        u.grantSelect(TB, USERS, DB)

    def get_sectors(self):
        """."""
        for sector in u.sectors:
            print("inici ", sector)
            Sector(sector)
            print("fi ", sector)


class Sector(object):
    """."""

    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_eaps()
        self.get_centres()
        self.get_llocs()
        self.get_moduls()
        self.get_blocs()
        self.get_trams()
        self.get_upload()
        self.upload_data()

    def get_eaps(self):
        """."""
        sql = "select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc \
               from cat_centres \
               where sector = '{}' and ep = '0208'".format(self.sector)
        self.eaps = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_centres(self):
        """."""
        sql = "select cent_codi_centre, cent_classe_centre, cent_nom_centre \
               from pritb010"
        self.centres = {row[:2]: row[2] for row in u.getAll(sql, self.sector)}

    def get_llocs(self):
        """."""
        sql = "select lloc_codi_lloc_de_treball, lloc_codi_up, \
                      lloc_proveidor_dni_proveidor \
               from pritb025"
        self.llocs = {row[:2]: row[2] for row in u.getAll(sql, self.sector)}

    def get_moduls(self):
        """."""
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, \
                      modu_codi_up, modu_descripcio, modu_lloc_codi_lloc \
               from vistb027 \
               where modu_data_baixa = 1 and \
                     modu_codi_uab is not null and \
                     modu_servei_codi_servei = '{}' and \
                     modu_model_agenda = 2".format(SERVEI)
        self.moduls = {row[:4]: self.eaps[row[4]] +
                       (self.centres[row[:2]], row[3], row[5],
                        self.llocs.get((row[6], row[4])))
                       for row in u.getAll(sql, self.sector)
                       if row[4] in self.eaps}

    def get_blocs(self):
        """."""
        sql = "select bp_centre_cod, bp_centre_cla, bp_servei, \
                      bp_modul, bp_bloc_p \
               from vistb201 \
               where bp_ddb = 1 and \
                     bp_principal = 'S' and \
                     substr(bp_mes, {}, 1) = 'X'".format(MES)
        self.blocs = set([row for row in u.getAll(sql, self.sector)
                          if row[:4] in self.moduls])

    def get_trams(self):
        """."""
        self.resultat = c.defaultdict(lambda: c.defaultdict(lambda: c.Counter()))  # noqa
        self.horari = c.defaultdict(set)
        self.dies = c.defaultdict(set)
        sql = "select bh_centre_cod, bh_centre_cla, bh_servei, bh_modul, \
                      bh_bloc_p, bh_bloc_d, bh_tipus_visita, \
                      bh_bloc_hi, bh_bloc_hf, \
                      nvl(bh_interval_minim, 0), nvl(bh_maxim_forc, 0) \
               from vistb203 \
               where bh_ddb = 1 and \
                     bh_bloc_d in (1, 2, 3, 4, 5)"
        for row in u.getAll(sql, self.sector):
            if row[:5] in self.blocs:
                minuts = (row[8] - row[7]) / 60.0
                self.resultat[row[:4]][row[5:7]]["minuts"] += minuts
                if row[9]:
                    self.resultat[row[:4]][row[5:7]]["forats"] += m.floor(minuts / row[9])  # noqa
                self.resultat[row[:4]][row[5:7]]["espontanies"] += row[10]
                self.horari[(row[:4], row[5], "hi")].add(row[7])
                self.horari[(row[:4], row[5], "hf")].add(row[8])
                self.dies[row[:4]].add(row[5])

    def _num_to_time(self, num):
        """."""
        mins, secs = divmod(num, 60)
        hours, mins = divmod(mins, 60)
        parts = str(hours).zfill(2), str(mins).zfill(2), str(secs).zfill(2)
        return "{}:{}:{}".format(*parts)

    def get_upload(self):
        """."""
        self.upload = []
        for modul, dades in self.resultat.items():
            for i in range(1, 6):
                if i in self.dies[modul]:
                    this = list(self.moduls[modul] + (i,))
                    this.append(self._num_to_time(min(self.horari[modul, i, "hi"])))  # noqa
                    this.append(self._num_to_time(max(self.horari[modul, i, "hf"])))  # noqa
                    for tipus, parametre in Estructura._get_all_params():
                        this.append(dades[(i, tipus)][parametre])
                    self.upload.append(this)

    def upload_data(self):
        """."""
        if self.sector == "6838":
            for row in self.upload:
                u.listToTable([row], TB, DB)
        else:
            u.listToTable(self.upload, TB, DB)


if __name__ == "__main__":
    Estructura()
