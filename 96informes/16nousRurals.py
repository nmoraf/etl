from sisapUtils import *
from collections import Counter
'''
centres,ups,atesa,edat65 = Counter(),Counter(),Counter(),Counter()
sql = 'select up,centre_codi,ates,edat from assignada_tot'
for up,centre,ates,edat in getAll(sql,'nodrizas'):
    centres[up,centre] += 1
    ups[up] += 1
    if ates == 1:
        atesa[up] += 1
    if edat > 64:
        edat65[up] += 1

averagEdat = {}
sql = "select up,avg(edat) from assignada_tot where edat> 14 group by up"
for up,edat in getAll(sql,'nodrizas'):
    averagEdat[up] = {'edat':edat}
        
urbans = {}
for (up,centre),n in centres.items():
    pobcentre = centres[up,centre]
    if up in urbans:
        if pobcentre > urbans[up]['centre']:
            urbans[up]['centre'] = pobcentre
    else:
        urbans[up] = {'centre':pobcentre,'assig':ups[up]}

try:
    remove(tempFolder + 'rurals.txt')
except:
    pass 
    
with openCSV(tempFolder + 'rurals.txt') as c:
    sql = "select amb_desc,ics_codi,scs_codi from cat_centres"
    for ambit,br,up in getAll(sql,'nodrizas'):
        if up in urbans:
            centre = urbans[up]['centre']
            pob = urbans[up]['assig']
            avgE = averagEdat[up]['edat']
            e65 = edat65[up]
            at = atesa[up]
            c.writerow([ambit,br,centre,pob,at,avgE,e65])
'''
ruralitat = {}
with open(tempFolder + 'nous_rurals.txt', 'rb') as file:
   p=csv.reader(file, delimiter='{', quotechar='|')
   for ind in p:
      br,rural = ind[0],ind[1]
      ruralitat[br] = {'rural': rural}
indicadors = [['EQA0203'],['EQA0209']]
PREVALEN = {}
with open(tempFolder + 'EQARURALS_POBL_BASAL.txt', 'rb') as file:
    p=csv.reader(file, delimiter='{')
    for ind in p:
        a,b,br,cd,edat,pobla,sexe,nn,pob = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8]
        pob = float(pob)
        if br in ruralitat:
            for indica in indicadors:
                indicador = indica[0]
                rural = ruralitat[br]['rural']
                if (indicador,rural,edat,sexe) in PREVALEN:
                    PREVALEN[(indicador,rural,edat,sexe)]['pob'] += pob
                else:
                    PREVALEN[(indicador,rural,edat,sexe)] = {'den':0,'pob':pob}
with open(tempFolder + 'EQARURALS_DEN_BASAL.txt', 'rb') as file:
    p=csv.reader(file, delimiter='{')
    for ind in p:
        indicador,b,br,cd,edat,pobla,sexe,nn,den = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8]
        den = float(den)
        if br in ruralitat:
            rural = ruralitat[br]['rural']
            if (indicador,rural,edat,sexe) in PREVALEN:
                PREVALEN[indicador,rural, edat,sexe]['den'] += den

ESPERADES = {}
with open(tempFolder + 'EQARURALS_POBL_A1509.txt', 'rb') as file:
    p=csv.reader(file, delimiter='{')
    for ind in p:
        a,bb,br,cd,edat,pobla,sexe,nn,pob = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8]
        if br in ruralitat:
            rural = ruralitat[br]['rural']
            for indica in indicadors:
                indicador = indica[0]
                if (indicador,rural, edat,sexe) in PREVALEN:
                    pprev = float(PREVALEN[indicador,rural, edat,sexe]['pob'])
                    dprev = float(PREVALEN[indicador, rural, edat,sexe]['den'])    
                    prev = dprev / pprev
                    esper = prev*float(pob)
                    ESPERADES[(br,indicador,rural,edat,sexe)] =  esper      
try:
    remove(tempFolder + 'esperades_CI_DM2.txt')
except:
    pass 
    
with openCSV(tempFolder + 'esperades_CI_DM2.txt') as c:
    for (br,indicador,rural,edat,sexe),count in ESPERADES.items():
        c.writerow([br,indicador,rural,edat,sexe,count])
