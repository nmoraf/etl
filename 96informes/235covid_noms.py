# coding: latin1

"""
.
"""

import datetime as d
import multiprocessing as m

import sisaptools as u


TABLE = "covid"
DATABASE = ("p2262", "permanent")

DEBUG = False
PROD = d.datetime.now().hour > 22 or d.datetime.now().hour < 6


class Covid(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.get_dades()
        self.indexing()

    def create_table(self):
        """."""
        cols = ("sector varchar(4)", "cip varchar(13)", "dni varchar(10)",
                "nom varchar(255)", "cog1 varchar(255)", "cog2 varchar(255)",
                "naix date", "sexe varchar(1)", "sit varchar(1)",
                "up varchar(5)", "abs int", "rca varchar(5)")
        u.Database(*DATABASE).create_table(TABLE, cols, remove=True)

    def get_dades(self):
        """."""
        if DEBUG:
            self._worker("6734")
        else:
            pool = m.Pool(len(u.constants.SECTORS_ECAP))
            pool.map(self._worker, u.constants.SECTORS_ECAP, chunksize=1)
            pool.close()

    def _worker(self, sector):
        """."""
        sql = "select '{}', usua_cip, usua_dni, usua_nom, usua_c1, usua_c2, \
                      to_date(usua_data_naixement, 'J'), usua_sexe, \
                      usua_situacio, usua_uab_up, usua_abs_codi_abs, \
                      usua_up_rca \
               from usutb040".format(sector)
        dades = list(u.Database(sector, "prod" if PROD else "data").get_all(sql))  # noqa
        u.Database(*DATABASE).list_to_table(dades, TABLE)

    def indexing(self):
        """."""
        indexes = [(TABLE, "cip", "cip"), (TABLE, "cog", "cog1, cog2"),
                   (TABLE, "naix", "naix")]
        with u.Database(*DATABASE) as db:
            for index in indexes:
                sql = "alter table {} add index {} ({})".format(*index)
                db.execute(sql)


if __name__ == "__main__":
    Covid()
