# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

from sisaptools import *

tb_d = 'sisap_alertsacu_inicials'
db = 'redics'
alt = 'altres'

execute('drop table {}'.format(tb_d),db)


upload = []

sql = 'select up, uba, grup_codi,exclos, hash_d,sector from exp_ecap_sacubitril_pacient'
for up, uba, grup_codi, exclos, hash_d, sector in getAll(sql, alt):
    upload.append([up, uba, 'M', grup_codi, hash_d, sector, exclos])

    
#afegim pacients FARM003 encara que no tinguin llistats per veure si això evoluciona o no!

u11 = {}
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, 'import'):
    u11[id] = {'sector': sector, 'hash': hash}

sql = 'select id_cip_sec, up, uba, den, num1, num2, num3, num4 from mst_alerta_sacubitril'
for id, up, uba, den, num1, num2, num3, num4 in getAll(sql, alt):
    try:
        sector = u11[id]['sector']
        hash = u11[id]['hash']
    except KeyError:
        continue
    if num3 == 1:
        upload.append([up, uba, 'M', 'FARM0003', hash, sector, 0])
        

sql = "create table {0}(up varchar2(5)  ,uab varchar2(5)  ,tipus varchar2(1)  ,indicador varchar2(8)  ,idpac varchar2(40)  ,sector varchar2(4)  ,exclos int)".format(tb_d)
execute(sql,db)

uploadOra(upload,tb_d,db)

users= ['PREDUECR','PREDUMMP','PREDUPRP']
for user in users:
    execute("grant select on {} to {}".format(tb_d,user),db)
