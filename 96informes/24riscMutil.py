# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

printTime('Inici')

nod = 'nodrizas'
imp = 'import'

dict_rs = {'RS61': 'LLEIDA','RS62': 'TARRAGONA', 'RS63': "TERRES DE L'EBRE",
            'RS64': 'GIRONA','RS67': 'CATALUNYA CENTRAL','RS71':'ALT PIRINEU - ARAN','RS78':'BARCELONA'}

sql = 'select year(data_ext), month(data_ext) from dextraccio'
for anysd, mes in getAll(sql, nod):
    dcalcul = str(mes) + str(anysd)
    
centres = {}
sql = "select scs_codi, rs from cat_centres"
for up, rs in getAll(sql, nod):
    centres[up] = rs

OutFile = tempFolder + 'recompte_versio' + dcalcul + '.txt'
OutFile2 = tempFolder + 'recompte_anual_versio' + dcalcul + '.txt'
OutFile3 = tempFolder + 'recomptes_perAnys_versio' + dcalcul + '.txt'

codisCim10 = "('N90.81', 'S38.2', 'Z60.81', 'C01-N90.81', 'C01-N90.810', 'C01-N90.811', 'C01-N90.812', 'C01-N90.813', 'C01-N90.818', 'C01-S38.21', 'C01-S38.211', 'C01-S38.211A', 'C01-S38.211D', 'C01-S38.211S', 'C01-S38.212', 'C01-S38.212A', 'C01-S38.212D', 'C01-S38.212S')"

descrip = {}
sql = "select ps_cod,ps_des from cat_prstb001 where ps_cod in  {}".format(codisCim10)
for cod, des in getAll(sql, imp):
    descrip[cod] = des

assig = {}
sql = 'select id_cip_sec, up, edat, sexe from assignada_tot'
for id, up, edat, sexe in getAll(sql, nod):
    if up in centres:
        rs = centres[up]
        assig[id] = {'edat': edat, 'sexe': sexe, 'rs': rs}
        
sql = ("select pst_th, pst_des from cat_prstb305", "import")
d_thes = {(ps_cod): (ps_des) for (ps_cod, ps_des)
                        in getAll(*sql)}

riscMutil, riscMutil2, riscMutilxAnys = Counter(), Counter(), Counter()
sql = "select id_cip_sec, pr_cod_ps, year(pr_dde), pr_th, pr_up from problemes,nodrizas.dextraccio where pr_cod_ps in {} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_dde<=data_ext and (pr_data_baixa =0  or pr_data_baixa>data_ext) and (pr_dba = 0  or pr_dba>data_ext)".format(codisCim10)
for id, ps, anys, thes, pr_up in getAll(sql, imp):
    descps = descrip[ps]
    try:
        edat = assig[id]['edat']
        sexe = assig[id]['sexe']
    except KeyError:
        edat = 'Sense registre'
        sexe = 'Sense registre'
    if int(anys) >= 2010:
        if pr_up in centres:
            rs = centres[pr_up]
            riscMutilxAnys[(rs, dict_rs[rs], ps, descps, anys, thes)] += 1
    if str(anys) == '2019':    
        riscMutil[ps, descps, anys, edat, sexe] += 1
        riscMutil2[ps, descps, anys, thes] += 1

try:
    remove(OutFile)
except:
    pass

with openCSV(OutFile) as c:
    for (ps, descps, anys, edat, sexe), d in riscMutil.items():
        c.writerow([ps, descps, anys, edat, sexe, d])

try:
    remove(OutFile2)
except:
    pass

with openCSV(OutFile2) as c:
    for (ps, descps, anys,thes), d in riscMutil2.items():
        c.writerow([ps, descps, anys, thes, d])
try:
    remove(OutFile3)
except:
    pass

with openCSV(OutFile3) as c:
    for (rs, rs_des, ps, descps, anys, thes), d in riscMutilxAnys.items():
        desc = d_thes[thes] if thes in d_thes else None
        c.writerow([rs, rs_des, ps, descps, anys, thes, desc, d])        

printTime('Fi')