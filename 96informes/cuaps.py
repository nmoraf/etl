from sisapUtils import *

dbResults = 'nodrizas'
tableResults = 'cuaps_20180724'
tableResultsColumns = "(up varchar(5), anymes varchar(7), imp int, n int)"
sql = "select                                                           \
            up,                                                         \
            anymes,                                                     \
            imp,                                                        \
            count(*)                                                    \
       from                                                             \
            (select                                                     \
                iau_up as up,                                           \
                to_char(iau_data_entr, 'YYYY-MM') as anymes,            \
                decode(max(iau_data_imp), NULL, '0', '1') as imp        \
            from                                                        \
                prstb152                                                \
            where                                                       \
                iau_data_entr >= DATE '2017-01-01'                      \
                and iau_lloc_visita='C'                                 \
                and iau_up in ('07207','07111','04021','04554','07208', \
                               '04901','04903','04881','07023','06311', \
                               '04893','07883','04372','00071','04842') \
            group by                                                    \
                iau_up,                                                 \
                to_char(iau_data_entr, 'YYYY-MM'),                      \
                iau_id_visita                                           \
            ) a                                                         \
       group by                                                         \
            up,                                                         \
            anymes,                                                     \
            imp "


sqlResults = []

for sector in sectors:
    for row in getAll(sql, sector):
        sqlResults.append(row)

createTable(tableResults, tableResultsColumns, dbResult, rm=True)

listToTable(sqlResults, tableResults, dbResults)
