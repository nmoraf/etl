# coding: iso-8859-1
#Carmen Cabezas,  (vacuna pneumococ en nens), Ermengol, Març 2017
#omplir excel

from sisapUtils import *
from collections import defaultdict, Counter

nod = 'nodrizas'

#Cobertura actual

pob = {}
sql = 'select id_cip_sec, edat_a, edat_m from ped_assignada where edat_a<1 and ates=1'
for id, edat_a, edat_m in getAll(sql, nod):
    pob[id] = {'edata': edat_a, 'edatm':edat_m}


ndosis = {}    
sql = 'select id_cip_sec, dosis from ped_vacunes where agrupador=698'
for id, dosis in getAll(sql, nod):
    ndosis[id] = {'dosis':dosis}

cobertura = Counter()
for (id), valors in pob.items():
    edat_a = valors['edata']
    edat_m = valors['edatm']
    vacunats = 0
    nd = 0
    if (id) in ndosis:
        nd = ndosis[id]['dosis']
        if edat_m <4:
            vacunats = 1
        elif 4<= edat_m <= 6:
            if nd >0:
                vacunats = 1
        elif 6<= edat_m <= 13:
            if nd >1:
                vacunats = 1
        elif edat_m > 13:
            if nd > 2:
                vacunats = 1
    cobertura[(edat_a, vacunats)] += 1
for (edat, vacunats), d in cobertura.items():
    print edat, vacunats, d
    

