#coding: iso-8859-1
#Rosa Morral, setembre16, Ermengol
#una mica cutre pero ho necessiten rapid

from sisapUtils import *
import csv, os, sys

nod = 'nodrizas'

file_dada = tempFolder + 'consum_tires_pedia.txt'

def get_ICS():
    centres = {}
    sql = "select scs_codi from cat_centres where ep='0208'"
    for up, in getAll(sql, nod):
        centres[up] = True
    return centres
    
def get_pob(centres):
    pob = {}
    sql = 'select id_cip_sec, up, edat from assignada_tot where edat between 4 and 120'
    for id, up, edat in getAll(sql, nod):
        if up in centres:
            pob[id] = edat
    return pob
    
def get_tires():
    posologia = {}
    sql = 'select id_cip_sec, tires from eqa_tires'
    for id, tires in getAll(sql, nod):
        posologia[id] = tires
    return posologia
    
centres = get_ICS()
pob = get_pob(centres)
posologia = get_tires()

resultat = {}
sql = 'select id_cip_sec from eqa_problemes where ps = 24'
for id, in getAll(sql, nod):
    if id in pob:
        edat = pob[id]
        if id in posologia:
            ntires = posologia[id]
            ambtires = 1
        else:
            ntires = 0
            ambtires = 0
        if edat in resultat:
            resultat[edat]['rec'] += 1
            resultat[edat]['ambtires'] += ambtires
            resultat[edat]['tires'] += ntires
            if ntires >= 42:
                    resultat[edat]['t42'] += 1
            if ntires >= 63:
                    resultat[edat]['t63'] += 1
        else:
            if ntires>= 42:
                if ntires>=63:
                    resultat[edat] = {'rec': 1, 'ambtires': ambtires, 'tires': ntires, 't42': 0, 't63': 1}
                else:
                    resultat[edat] = {'rec': 1, 'ambtires': ambtires, 'tires': ntires, 't42': 1, 't63': 0}
            else:
                resultat[edat] = {'rec': 1, 'ambtires': ambtires, 'tires': ntires, 't42': 0, 't63': 0}
exp = []
for edat, values in resultat.items():
    mitjana = values['tires']/values['rec']
    try:
        mitjana2 = values['tires']/values['ambtires']
    except:
        mitjana = 0
    exp.append([edat, values['rec'], mitjana, values['ambtires'], mitjana2, values['t42'],values['t63']])
writeCSV(file_dada, exp, sep=';') 


'''
resultat = []
for edat in range(4, 17):
    sql = "select count(distinct id_cip_sec),round(avg(tires),1), min(tires),max(tires) from eqa_tires where id_cip_sec in (select id_cip_sec from assignada_tot where edat={} and up in (select scs_codi from cat_centres where ep='0208')) and id_cip_sec in (select id_cip_sec from eqa_problemes where ps=24)".format(edat)
    for nens, tires, min, max in getAll(sql, 'nodrizas'):
        resultat.append([edat, nens, tires, min, max])
        
writeCSV(file_dada, resultat, sep=';')  
'''

