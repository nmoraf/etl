# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'test'
nod = 'nodrizas'
imp = 'import'

debug = False
tract3m = False

sql='select concat(year(data_ext),month(data_ext)) from dextraccio'
for d in getOne(sql, nod):
    dcalcul=d


OutFile= tempFolder + '15CITerresEbre.txt' + dcalcul + '.txt'
OutFile2= tempFolder + '15CITerresEbre_tancats.txt' + dcalcul + '.txt'
OutFile3= tempFolder + '15CITerresEbre_Z86.txt' + dcalcul + '.txt'
TaulaMy = "CI_terresEbre"


printTime('inici')
codisCI = {}
sql = "select criteri_codi from eqa_criteris where agrupador=1"
for criteri, in getAll(sql,nod):
    codisCI[criteri] = True

def tEbreConverter(ambit,up):
    if int(ambit) == 11:
        if up =='00043':
            return 'Terres Ebre - Afectats'
        elif up == '00046':
            return 'Terres Ebre - Afectats'
        elif up == '00053':
            return 'Terres Ebre - Afectats'
        else:
            return 'Terres Ebre'
    else:
        return 'Resta Catalunya'

centres = {}
sql = "select amb_codi,amb_desc,medea,scs_codi from cat_centres"
for ambit,desc,medea,up in getAll(sql,nod):
    centres[up] = {'ambit':desc,'grup':tEbreConverter(ambit,up),'medea':medea}

assig = {}
assigToT = Counter()
sql = 'select id_cip_sec,up from assignada_tot'
for id,up in getAll(sql,nod):
    assig[id] = {'up':up,'ambit':centres[up]['ambit'],'grup':centres[up]['grup'],'medea':centres[up]['medea']} 
    assigToT[centres[up]['grup'],centres[up]['medea']] += 1

tractamentsCI = {}
sql = "select id_cip_sec from eqa_tractaments,dextraccio where farmac in (3,4,5,6){}".format(" and pres_orig < date_add(data_ext,interval - 3 month)" if tract3m else '')
for id, in getAll(sql,nod):
    tractamentsCI[id] = True

pacientsCI = {}
sql = "select id_cip_sec,pr_cod_ps,pr_dde,if(pr_dba=0,0,1) from problemes where pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa=0{}".format(" and pr_cod_ps='I24' limit 10" if debug else '')
for  id,ps,dde,tancat in getAll(sql,imp):
    try:
        codisCI[ps]
    except KeyError:
        continue
    if (id) in pacientsCI:
        if tancat < pacientsCI[(id)]['tancat'] :
            pacientsCI[(id)]['tancat'] = tancat
    else:
        pacientsCI[(id)] = {'tancat':tancat}

try:
    remove(OutFile)
except:
    pass      
with openCSV(OutFile) as c:
    for (id),count in pacientsCI.items():
        tancat = count['tancat']
        c.writerow([id,tancat])
execute('drop table if exists {}'.format(TaulaMy),db)
execute('create table {} (id int,tancat int)'.format(TaulaMy),db)
loadData(OutFile,TaulaMy,db)

resultatCI = Counter()
tancatsCI = {}
sql = 'select id,tancat from {}'.format(TaulaMy)
for id,tancat in getAll(sql,db):
    try:
        up = assig[id]['up']
        ambit = assig[id]['ambit']
        grup = assig[id]['grup']
        medea = assig[id]['medea']
    except KeyError:
        continue
    try:
        if tractamentsCI[id]:
            farmac = 1
    except KeyError:
        farmac = 0
    if tancat ==1:
        tancatsCI[id] = {'grup':grup,'medea':medea,'farmac':farmac}
    resultatCI[ambit,grup,medea,up,tancat,farmac] += 1
    
try:
    remove(OutFile)
except:
    pass      
with openCSV(OutFile) as c:
    for (ambit,grup,medea,up,tancat,farmac),count in resultatCI.items():
        c.writerow([ambit,grup,medea,up,tancat,farmac,count])

problemesCI = {}
sql = "select id_cip_sec,pr_cod_ps from problemes where pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa=0 and pr_dba=0"
for id,ps in getAll(sql,imp):
    try:
        grup = tancatsCI[id]['grup']
        medea = tancatsCI[id]['medea']
        farmac = tancatsCI[id]['farmac']
        problemesCI[id,ps] = {'grup':grup,'medea':medea,'farmac':farmac}
    except KeyError:
        continue
            
try:
    remove(OutFile2)
except:
    pass      
with openCSV(OutFile2) as c:
    for (id,ps),count in problemesCI.items():
        grup = count['grup']
        medea = count['medea']
        farmac = count['farmac']
        if farmac == 1:
            c.writerow([ps,grup,medea])

problemesZ86 = Counter()
sql = "select id_cip_sec from problemes where pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa=0 and pr_dba=0 and pr_cod_ps='Z86.70'"
for id, in getAll(sql,imp):
    try:
        grup = assig[id]['grup']
        medea = assig[id]['medea']
    except KeyError:
        continue
    problemesZ86[grup,medea] += 1
   
try:
    remove(OutFile3)
except:
    pass      
with openCSV(OutFile3) as c:
    for (grup,medea),count in problemesZ86.items():
        total = assigToT[grup,medea]
        c.writerow([grup,medea,count,total])
            
printTime('fi')