# coding: latin1

"""
Treure NIfs de metges de taula econsulta
"""

import urllib as w
import os as o
import collections as c
import math as m
import pandas as pd

import sisapUtils as u


tb = "SISAP_ECONSULTA_PROFESSIONALS"
db = "redics"

upload = []
sql = "select distinct dni from {} where tipus='M'".format(tb)
for dni, in u.getAll(sql, db):
    nif = u.getNif(dni)
    upload.append([nif])
    
file = u.tempFolder + 'NIF_econsulta_MF.txt'
u.writeCSV(file, upload, sep='|')