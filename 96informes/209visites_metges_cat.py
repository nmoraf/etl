# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


DEBUG = False

ESPECIALITATS = ("10999", "10117")
PERIODE = (2016, 2018)
FILE = u.tempFolder + "visites_ics.csv"


class Sindicat(object):
    """."""

    def __init__(self):
        """."""
        self.get_cataleg()
        self.get_centres()
        self.get_particions()
        self.get_visites()
        self.get_logins()
        self.get_virtuals()
        self.export()

    def get_cataleg(self):
        """."""
        self.cataleg = {}
        sql = "select codi_sector, tv_tipus, tv_cita, tv_homol, tv_lloc \
               from cat_vistb206"
        for sec, tip, cit, hom, lloc in u.getAll(sql, "import"):
            if tip == "9T" or hom == "TLF":
                visita = 43
            elif tip == "9E":
                visita = 441
            elif lloc == "D":
                visita = 42
            else:
                visita = 41
            self.cataleg[(sec, tip, cit)] = visita

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres where ep = '0208'"
        self.centres = set([up for up, in u.getAll(sql, "nodrizas")])

    def get_particions(self):
        """."""
        self.particions = []
        sql = "select visi_data_visita from {} limit 1"
        for table in u.getSubTables("visites"):
            dat, = u.getOne(sql.format(table), "import")
            y = dat.year
            s = 1 if dat.month < 7 else 2
            if y in range(PERIODE[0], PERIODE[1] + 1):
                self.particions.append((table, (y, s)))

    def get_visites(self):
        """."""
        self.visites = c.Counter()
        if DEBUG:
            visites = [self._get_visites_worker(self.particions[0])]
        else:
            visites = u.multiprocess(self._get_visites_worker, self.particions, 12)  # noqa
        for worker in visites:
            for row in worker:
                self.visites[row[:-1]] += row[-1]

    def _get_visites_worker(self, param):
        """."""
        table, key = param
        visites = c.Counter()
        sql = "select visi_up, codi_sector, \
                      visi_tipus_visita, visi_tipus_citacio \
               from {} \
               where visi_situacio_visita = 'R' and \
                     s_espe_codi_especialitat in {}".format(table, ESPECIALITATS)  # noqa
        for row in u.getAll(sql, "import"):
            if row[0] in self.centres:
                visites[self.cataleg.get(row[1:], 41)] += 1
        return [key + (k, v) for (k, v) in visites.items()]

    def get_logins(self):
        """."""
        self.logins = c.defaultdict(set)
        sql = "select codi_sector, ide_usuari \
               from cat_pritb992 \
               where ide_categ_prof_c in {}".format(ESPECIALITATS)
        for sector, usuari in u.getAll(sql, "import"):
            self.logins[sector].add(usuari)

    def get_virtuals(self):
        """."""
        if DEBUG:
            virtuals = [self._get_virtuals_worker("6734")]
        else:
            virtuals = u.multiprocess(self._get_virtuals_worker, u.sectors, 12)
        for worker in virtuals:
            for row in worker:
                self.visites[row[:-1]] += row[-1]

    def _get_virtuals_worker(self, sector):
        """."""
        virtuals = c.Counter()
        logins = self.logins[sector]
        sql = "select vvi_codi_up, vvi_usu_resolucio, vvi_data_resolucio \
               from vvirtuals_s{} \
               where vvi_situacio = 'R' and \
                     vvi_data_resolucio between {}0101 and {}1231".format(sector, PERIODE[0], PERIODE[1])  # noqa
        for up, usu, dat in u.getAll(sql, "import"):
            if up in self.centres and usu in logins:
                virtuals[(dat.year, 1 if dat.month < 7 else 2)] += 1
        return [k + (442, v) for (k, v) in virtuals.items()]

    def export(self):
        """."""
        result = sorted([k + (v,) for (k, v) in self.visites.items()
                         if v > 99])
        header = [("any", "semestre", "tipus", "numero")]
        u.writeCSV(FILE, header + result, sep=";")


if __name__ == "__main__":
    Sindicat()
