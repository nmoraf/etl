# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re


nod="nodrizas"
imp="import"
alt="altres"


class laboratori(object):
    """."""
    
    def __init__(self):
        """."""
        self.current_date = self.get_date_dextraccio()[1]
        self.past_date = self.get_date_dextraccio()[0]
        self.dext = self.get_date_dextraccio()[2]
        self.get_taules()
        self.id_edat_sexe=self.get_pacients()
        self.get_centres()
        self.get_assir()
        self.assir = self.get_assir()
        self.get_tipus_up()
        self.get_rows()
        

    def get_date_dextraccio(self):
        """ . """
        sql= "select date_format(date_add(date_add(data_ext, interval - 12 month), interval + 1 day),'%Y%m'), date_format(data_ext,'%Y%m'), data_ext  from nodrizas.dextraccio"
        for past, current, dext in getAll(sql,nod):
            return past,current, dext	

    def get_taules(self):
        """."""
        self.tables = []
        for table in getSubTables('laboratori'):
            try:
                dat, = getOne("select date_format(cr_data_reg,'%Y%m') from {} limit 1".format(table), imp)
                if self.past_date <= dat <= self.current_date:
                    print table
                    self.tables[table]
            except TypeError:
                continue
                
    def get_pacients(self):
        """ . """
        sql = "select id_cip_sec,  up, edat, sexe from assignada_tot"
        return {id: (up, ageConverter(edat),sexConverter(sexe)) for id, up,  edat, sexe in getAll(sql, nod)}
        
    def get_centres(self):
        """."""
        sql = "select scs_codi,ics_desc from cat_centres"
        self.centresEAP = {(up): desc for (up, desc) in getAll(sql, nod)} 
        
    def get_assir(self):
        """."""
        sql="select up_assir from ass_centres"
        self.assir = {(up): True for (up), in getAll(sql, nod)}
        
    def get_tipus_up(self):
        """."""
        self.up_tipus = {}
        sql = "select up_codi_up_ics, up_tipus_up_ics from cat_gcctb007"
        for up, tip in getAll(sql, imp):
            if tip == 1:
                tp = 'Hospital'
            else:
                if up in self.centresEAP:
                    tp = 'EAP'
                elif up in self.assir:
                    tp = 'ASSIR'
                else:
                    tp = 'Altres AP'
            self.up_tipus[up] = tp
    
    def get_peticions(self,table):
        """ . """
        recomptes= {}
        sql = "select id_cip_sec,cr_data_reg, date_format(cr_data_reg,'%Y%m') codi_up from {} where cr_codi_lab <> 'RIMAP'".format(table)
        for id, data, periode, up in getAll(sql, imp):
            if monthsBetween(data,self.dext) < 12 and id in self.id_edat_sexe:
                up_ass, edat, sexe=self.id_edat_sexe[id]
                if up in self.up_tipus:
                    tp = self.up_tipus[up]
                    recomptes[(periode, up_ass,edat,sexe, tp, id, data)]= True
        return recomptes
    
    def get_rows(self):
        """ . """
        numerador_total= Counter()
        for numerador in multiprocess(self.get_peticions, self.tables):
            for periode, up_ass,edat,sexe, tp, id, data in numerador:
                numerador_total[(periode, up,edat, sexe, tp)] += 1
      
        self.rows=[]
        for (periode, up, edat, sexe, tp), d in numerador_total.items:
            self.rows.append([periode, up, edat, sexe, tp, d])
        file = u.tempFolder + 'Laboratori_det.csv'
        writeCSV(file, self.rows)
        
if __name__ == '__main__':
    laboratori()
