# coding: iso-8859-1
#Pregunta parlament: metadona, Ermengol, Novembre 2016
from sisapUtils import *
from collections import defaultdict, Counter

imp = 'import_jail'

metadona = "('N02AC52','N07BC02')"

periodes = {'PA': {
                    'dini': '20150101',
                    'dfi': '20151001',
                    },
            'PB': {
                    'dini': '20160101',
                    'dfi': '20161001',
                    },
            'PC': {
                    'dini': '20150101',
                    'dfi': '20151231',
                    },
    }

centres = {}
sql =  'select scs_codi, ics_desc from nodrizas.jail_centres'
for up, desc in getAll(sql, imp):
    centres[up] = desc
    
pob = {}
sql = "select id_cip_sec,(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe,usua_uab_up, if(usua_nacionalitat='',724,usua_nacionalitat) from assignada, nodrizas.dextraccio"
for id, edat, sexe, up, nac in getAll(sql, imp):
    if nac == '724':
        nacionalitat = 'AUTÒCTONS'
    else:
        nacionalitat = 'ESTRANGERS'
    pob[id] = {'sexe': sexe, 'edat': edat, 'up': up, 'nac': nacionalitat}
    
last_up = {}
sql = "select id_cip_sec, huab_data_ass, huab_up_codi from moviments order by huab_data_ass desc"
for id, data, up in getAll(sql, imp):
    if id in last_up:
        continue
    else:
        last_up[id] = up 
            
pacients = {} 
  
sql = "select id_cip_sec, date_format(ppfmc_pmc_data_ini,'%Y%m%d'),date_format(ppfmc_data_fi,'%Y%m%d') from tractaments where pf_cod_atc in {}".format(metadona)
for id, inici, final in getAll(sql, imp):
    edat = pob[id]['edat']
    sexe = pob[id]['sexe']
    nacio = pob[id]['nac']
    up = pob[id]['up']
    if up == '':
        try:
            up = last_up[id]
        except KeyError:
            up = 'INDETERMINADA'
    try:
        desc = centres[up]
    except KeyError:
        desc = up
    pa, pb, pc = 0,0,0
    dini,dfi = periodes['PA']['dini'], periodes['PA']['dfi']
    if (dini <= inici <= dfi) or (inici <dini and final>= dini):
        pa = 1
    dini,dfi = periodes['PB']['dini'], periodes['PB']['dfi']
    if (dini <= inici <= dfi) or (inici <dini and final>= dini):
        pb = 1
    dini,dfi = periodes['PC']['dini'], periodes['PC']['dfi']
    if (dini <= inici <= dfi) or (inici <dini and final>= dini):
        pc = 1
    if id in pacients:
        pacients[id]['pa'] += pa
        pacients[id]['pb'] += pb
        pacients[id]['pc'] += pc
    else:
        pacients[id] = {'edat': edat, 'sexe': sexe, 'up': desc, 'nacio': nacio, 'pa': pa, 'pb': pb, 'pc':pc}

upload = []
for id, valors in pacients.items():
    pa = valors['pa']
    pb = valors['pb']
    pc = valors['pc']
    if pa > 1:
        pa = 1
    if pb > 1:
        pb = 1
    if pc > 1:
        pc = 1
    upload.append([id, valors['edat'],  valors['sexe'],  valors['up'], valors['nacio'], pa, pb, pc])
    
table = 'jail_metadona'
create = "(id_cip_sec int,  edat double, sexe varchar(10), up varchar(150), nacionalitat varchar(20), PA int, PB int, PC int)"
createTable(table,create,'test', rm=True)

listToTable(upload, table, 'test')

file = tempFolder + 'metadona.txt'
writeCSV(file, upload, sep=';')
