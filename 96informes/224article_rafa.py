# coding: utf8

"""
Pel post de l'article del Rafa
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

dext, = u.getOne("select data_ext from dextraccio", 'nodrizas')

class article_estatines(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_problemes()
        self.get_estatines()
        self.get_numeros()        
        
    def get_centres(self):
        """EAP ICS"""
        self.centres = {}
        sql = "select scs_codi from cat_centres where ep='0208'"
        for up, in u.getAll(sql, 'nodrizas'):
            self.centres[up] = True
    
    
    def get_problemes(self):
        """Agafem pacients diabètics i amb malaltia cardiovascular oberta o tancada per a excloure'ls"""
        self.exclusions, self.dm2 = {}, {}
        sql = "select id_cip_sec, ps from eqa_problemes where ps in (1, 7, 11, 211, 212, 213, 18)"
        for id, ps in u.getAll(sql, 'nodrizas'):
            if ps == 18:
                self.dm2[id] = True
            else:
                self.exclusions[id] = True
                
    def get_estatines(self):
        """Mirem pacients amb estatines prevalents i pacients amb estatines incidents dels últims 18 mesos"""
        self.prevalents, self.incidents = {}, {}
        
        sql = "select id_cip_sec, farmac, pres_orig from eqa_tractaments where farmac= 82"
        for id, farmac, origen in u.getAll(sql, 'nodrizas'):
            self.prevalents[id] = True
            b = u.monthsBetween(origen,dext)
            if 0 <= b <= 17:
                self.incidents[id] = True
                
    def get_numeros(self):
        """Treu la n de pacients de l'ICS a tenir en compte"""
        incid, prev, den = 0, 0, 0
        inciddm, prevdm, dendm = 0, 0, 0
        sql = "select id_cip_sec, up from assignada_tot where edat > 74"
        for id, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                if id in self.exclusions:
                    continue
                else:
                    if id in self.dm2:
                        dendm += 1
                        if id in self.prevalents:
                            prevdm += 1
                        if id in self.incidents:
                            inciddm += 1
                    else:
                        den += 1
                        if id in self.prevalents:
                            prev += 1
                        if id in self.incidents:
                            incid += 1
                        
        print 'denominador: ', den
        print 'incidents: ', incid
        print 'prevalents: ', prev
        
        print 'En diabètics: '
        print 'denominador: ', dendm
        print 'incidents: ', inciddm
        print 'prevalents: ', prevdm

if __name__ == '__main__':
    u.printTime("Inici")
    
    article_estatines()
    
    u.printTime("Fi")
        
        