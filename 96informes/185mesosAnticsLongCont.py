# -*- coding: utf8 -*-

"""
Adaptat del procés de long_cont de altres
però per passar dades dels altres anys
"""


import collections as c
import sys,datetime
from time import strftime

import sisapUtils as u


inici = 200901
fi = 201012

periodo = "A1012"
anyCalcul = 2010

db = "altres"
tb = "mst_long_cont_pacient"
tbup = "exp_long_cont_up"
file = 'LONG_CONT'

u.printTime("inici")

class Continuitat(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_edats()
        self.get_pob()
        self.get_visites()
        self.get_taula()
        self.get_calculs()
        self.export_khalix()

        
    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = {up: True for up, in u.getAll(sql, 'nodrizas')}

    
    def get_edats(self):
        """."""
        self.edats = {}
        sql = "select id_cip_sec, (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) from assignada, nodrizas.dextraccio"
        for id, edat in u.getAll(sql, 'import'):
            if edat > 14:
                self.edats[id] = True

    def get_pob(self):
        """."""
        self.pob = {}
        sql = "select id_cip_sec, up from assignadahistorica where dataany={}".format(anyCalcul)
        for id, up in u.getAll(sql, 'import'):
            if id in self.edats:
                self.pob[id] = up

    def get_visites(self):
        """Agafem visites dels dos últims anys"""
        
        self.visitespacients = c.Counter()
        self.pac_inclosos = set()
        for table in u.getSubTables('visites'):
            dat, = u.getOne("select date_format(visi_data_visita,'%Y%m') from {} limit 1".format(table), 'import')
            if inici <= int(dat) <= fi:
                print table
                sql = "select id_cip_sec, visi_up, visi_col_prov_resp \
                        from {} \
                        where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_servei_codi_servei = 'MG' and visi_col_prov_resp <> 0".format(table)
                for id, up, col in u.getAll(sql, "import"):
                    if up in self.centres:
                        self.visitespacients[(id, col)] += 1        
                
    def get_taula(self):
        """Calculem les tres dades necessàries per calcular els indicadors. A nivell de pacient necessitem:
            1) Nombre de professionals diferents
            2) Nombre de visites totals
            3) Nombre visites professional majoritari
        """
        self.pacients = {}
        for (id, col), rec in self.visitespacients.items():
            if id in self.pacients:
                self.pacients[(id)]['totals'] += rec
                self.pacients[(id)]['nprof'] += 1
                majoritari = self.pacients[(id)]['vprof']
                if rec > majoritari:
                    self.pacients[(id)]['vprof'] = rec
            else:
                self.pacients[(id)] = {'totals': rec, 'nprof': 1, 'vprof': rec}

        
    def get_calculs(self):
        """Calculem els indicadors a nivell de pacient
        Aqui filtrem per població assignada i pacients entre 3 i 300 visites"""
        upload = []
        for (id), dad in self.pacients.items():
            totals, nprof, vprof = dad['totals'], dad['nprof'], dad['vprof']
            if 3 <= totals <= 300:
                if id in self.pob:
                    up = self.pob[id]
                    n1 = 1 - (nprof/(totals + 0.1))
                    d1 = 1 - (1/(totals + 0.1))
                    r1 = float(n1/d1)
                    r2 = float(vprof) / float(totals)
                    upload.append([id, up, nprof, vprof, totals, n1, d1, r1, r2])
                
        columns = ["id_cip_sec int", "up varchar(5)", "nprof int", "vprof int", "totals int", "n1 double", "d1 double", "r1 double", "r2 double"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)

    def export_khalix(self):
        """Exportar a khalix els indicadors"""
        sql = "select scs_codi, ics_codi from khx_centres"
        centres = {up: br for up, br in u.getAll(sql, 'export')}
        detalle = "TIPPROF1"
        export = []
        sql = "select up, sum(r1), sum(r2), count(*) from {} group by up".format(tb)
        for up, r1, r2, den in u.getAll(sql, db):
            br = centres[up] if up in centres else False
            if br:
                export.append(['CONT0001', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r1])
                export.append(['CONT0001', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0002', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r2])
                export.append(['CONT0002', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
        columns = ["indicador varchar(10)", "periode varchar(10)", "up varchar(5)","analisi varchar(5)", "nc varchar(10)", "detalle varchar(10)", "d1 varchar(10)", "n varchar(5)", "resultat double"]
        u.createTable(tbup, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(export, tbup, db)  
        sql = "select * from {}.{}".format(db, tbup)
        u.exportKhalix(sql, file)
        
if __name__ == '__main__':
    Continuitat()  
                
u.printTime("Fi")