# coding: latin1

"""
Trapassa les dades de la web de centres de l'Albert a REDICS.
"""

import collections as c
import datetime as d

import sisapUtils as u


tb = "sisap_model_consultoris"
db = "redics"
tb_persist = "sisap_centres_cataleg"
db_persist = "pdp"
albert = ("centres", "x0001")


class Centres(object):
    """."""

    def __init__(self):
        """."""
        self.get_up_to_br()
        self.get_hacked_ecap_to_oficial()
        self.get_ecap_to_oficial()
        self.persist_ecap_to_oficial()
        self.get_poblacio()
        self.get_serveis()
        self.get_horaris()
        self.get_centres()
        self.upload_data()
        self.get_exclosos()
        if d.datetime.today().weekday() == 0:
            self.send_mail()

    def get_up_to_br(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.up_to_br = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_hacked_ecap_to_oficial(self):
        """."""
        file = u.baseFolder + ["96informes", "148sisap_centres_conversio.txt"]
        file = u.SLASH.join(file)
        self.hacked_ecap_to_oficial = {tuple(row[:3]): row[3] for row
                                       in u.readCSV(file, sep="@")}

    def get_ecap_to_oficial(self):
        """."""
        self.ecap_to_oficial = {}
        self.ecap_to_descripcio = {}
        sql = "select codi_sector, cent_codi_centre, cent_classe_centre, \
                      cent_codi_oficial, cent_nom_centre \
               from cat_pritb010"
        for row in u.getAll(sql, "import"):
            key = row[:3]
            oficial = self.hacked_ecap_to_oficial.get(key, row[3])
            self.ecap_to_oficial[key] = oficial
            self.ecap_to_descripcio[key] = row[4]

    def persist_ecap_to_oficial(self):
        """."""
        cols = "(sector varchar(4), codi varchar(9), classe varchar(2), \
                 oficial varchar(10))"
        u.createTable(tb_persist, cols, db_persist, rm=True)
        upload = [k + (v,) for (k, v) in self.ecap_to_oficial.items()]
        u.listToTable(upload, tb_persist, db_persist)

    def get_poblacio(self):
        """."""
        self.poblacio = c.defaultdict(lambda: c.Counter())
        self.poblacio_per_centre = c.Counter()
        sql = "select codi_sector, centre_codi, centre_classe, up, edat \
               from assignada_tot \
               where ep = '0208'"
        for row in u.getAll(sql, "nodrizas"):
            oficial = self.ecap_to_oficial[row[:3]]
            br = self.up_to_br[row[3]]
            grup = "A" if row[4] > 14 else "N"
            self.poblacio[(oficial, br)][grup] += 1
            self.poblacio_per_centre[row[:3]] += 1

    def get_serveis(self):
        """."""
        self.centre_to_servei = c.defaultdict(set)
        sql = "select a.centre_id, b.codi \
               from serveis_ocupacio a \
                    inner join serveis_servei b on a.servei_id = b.id"
        for id, br in u.getAll(sql, albert):
            if br in self.up_to_br.values():
                self.centre_to_servei[id].add(br)

    def get_horaris(self):
        """."""
        self.horaris = c.defaultdict(list)
        sql = "select centre_id, categoria, dia, inici, final \
               from centres_horaricategoria"
        for id, cat, dia, ini, fi in u.getAll(sql, albert):
            if ini and fi:
                temps = (fi - ini).seconds / 3600.0
                self.horaris[(id, cat)].append(temps)

    def get_centres(self):
        """."""
        self.centres = []
        self.centres_albert = set()
        self.centres_sense_br = []
        sql = "select a.id, codi_edi, codi_departament, c.nom, a.nom, \
                      distancia_al_centre_principal, \
                      setmanes_obertura_any, \
                      tipus_id \
               from centres_centre a \
                    inner join centres_sap b on a.sap_id = b.id \
                    inner join centres_ambit c on b.ambit_id = c.id \
               where tipus_id in (3, 5, 15) and donat_de_baixa = 0"
        for id, edi, cod, amb, nom, dis, setm, tipus in u.getAll(sql, albert):
            self.centres_albert.add(cod)
            if tipus == 5:
                this = [amb, edi, nom, cod, dis, setm]
                for cat in range(1, 5):
                    horaris = self.horaris.get((id, cat), [])
                    if horaris:
                        this.extend([len(horaris), sum(horaris)])
                    else:
                        this.extend([None, None])
                poblacions = []
                n = len(self.centre_to_servei[id])
                if n:
                    for br in self.centre_to_servei[id]:
                        poblacions.append([br,
                                           self.poblacio[(cod, br)].get("A"),
                                           self.poblacio[(cod, br)].get("N")])
                else:
                    poblacions.append([None, None, None])
                for poblacio in poblacions:
                    self.centres.append(this + [n] + poblacio)
                if (id, 1) in self.horaris and not n:
                    self.centres_sense_br.append((edi, nom))

    def upload_data(self):
        """."""
        u.createTable(tb, "(ambit varchar2(255), codi_edifici varchar2(40), \
                            nom varchar2(255), codi_departament varchar2(40), \
                            distancia int, setmanes int, \
                            mf_dies int, mf_hores int, \
                            inf_dies int, inf_hores int, \
                            ped_dies int, ped_hores int, \
                            gis_dies int, gis_hores int, \
                            n_br int, br varchar2(5), pob_a int, pob_n int)",
                      db, rm=True)
        u.listToTable(self.centres, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUJCH", "PDP", "PREDUECR"), db)
        u.calcStatistics(tb, db)

    def get_exclosos(self):
        """."""
        file = u.baseFolder + ["96informes", "148sisap_centres_exclosos.txt"]
        file = u.SLASH.join(file)
        self.exclosos = set([tuple(row) for row in u.readCSV(file, sep="@")])

    def send_mail(self):
        """."""
        subject = "Consultoris amb errors"
        text = ""
        falten_albert = [(k[0], k[1].ljust(9), k[2], v,
                          self.ecap_to_oficial[k].ljust(9),
                          self.ecap_to_descripcio[k])
                         for (k, v) in self.poblacio_per_centre.items()
                         if 24 < v < 8000 and
                         self.ecap_to_oficial[k] not in self.centres_albert and
                         k not in self.exclosos]
        if falten_albert:
            text += "Relaci� de centres d'ECAP amb poblaci� assignada que no apareixen al cat�leg d'infraestructures (Albert).\n\n"  # noqa
            text += "Com que no tenim manera de saber quins s�n consultoris locals, nom�s enviem els que tenen assignats entre 25 i 8000 pacients i no els tenim expl�citament exclosos.\n\n"""  # noqa
            text += "Es mostren les columnes SECTOR, CODI, CLASSE, POBLACIO, CODI_OFICIAL (el que ha de creuar amb CODI_DEPARTAMENT de l'Albert) i DESCRIPCI�.\n\n"  # noqa
            text += "\n".join(["\t".join(map(str, row)) for row
                               in sorted(falten_albert, key=lambda x: x[3],
                               reverse=True)])
            text += "\n\n\n"
        if self.centres_sense_br:
            text += "Relaci� de centres d'infraestructures (Albert) sense el seu BR relacionat (taula serveis_servei).\n\n"  # noqa
            text += "Es mostren les columnes CODI i DESCRIPCI�.\n\n"
            text += "\n".join(["\t".join(row) for row
                               in sorted(self.centres_sense_br)])
            text += "\n\n\n"
        if text:
            u.sendPolite(["amercadecosta@gencat.cat", "fmunientep@gencat.cat"],
                         subject, text)


if __name__ == "__main__":
    Centres()
