# coding: utf8

"""
Nombre de visites de llevadora per població assignada
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

imp = "import"

class atesa_ass(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_ass()
        self.get_pob()
        self.export_data()
        
    def get_centres(self):
        """EAP ICS"""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_ass(self):
        """
        Obtenim visites llevadores
        """
        self.visites = Counter()
        sql = "select \
                    id_cip_sec \
                    ,visi_up \
                from \
                    visites1 a \
                where \
                    visi_situacio_visita='R' \
                    and ( \
                        s_espe_codi_especialitat in ('30084','30085') \
                    or \
                        (s_espe_codi_especialitat = '30999' and visi_up in (select up from nodrizas.ass_centres where br like 'SD%' and amb_desc like '%METRO%SUD%')) \
                        ) "
        for id, up in u.getAll(sql, imp):
            self.visites[id] += 1
    
    def get_pob(self):
        """Obtenim Nombre visites per pob assignada"""
        self.pob = Counter()
        sql = "select id_cip_sec, up, edat, sexe from assignada_tot"
        for id, up, edat, sexe in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up][0]
                visita_ass = self.visites[(id)] if (id) in self.visites else 0
                if sexe == 'D':
                    self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'den')] += 1
                    self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'num')] += visita_ass
        
             
    def export_data(self):
        """."""
        self.upload = []
        for (br, edat, sexe, tip), n in self.pob.items():
            if tip == 'den':
                num = self.pob[(br, edat, sexe, 'num')]
                self.upload.append([br, edat, sexe, num, n])
                
        u.writeCSV(u.tempFolder + 'atesa_ass2.txt', self.upload)  
   
if __name__ == '__main__':
    atesa_ass()
    