# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'redics'
table = 'sisap_AssignadaHistorica'

#Per pujar assignada de desembre de cada any

imp = 'import'
nod = 'nodrizas'

sql = 'select year(data_ext) from dextraccio'
for anys in getOne(sql,nod):
    dataAny = anys

try:
    execute("delete from {0} where dataAny={1}".format(table,dataAny),db)
except:
    pass

hashConverter = {}
sql = 'select id_cip_sec,codi_sector,hash_d from u11'
for id, sector, hash in getAll(sql,imp):
    hashConverter[id] = {'sector':sector,'hash':hash}

upload2 = []
sql = 'select id_cip_sec,up,uba,upinf,ubainf,ates from assignada_tot'
for id,up,uba,upinf,ubainf,ates in getAll(sql,nod):
    upload2.append([dataAny,hashConverter[id]['sector'],hashConverter[id]['hash'],up,uba,ubainf,ates])
uploadOra(upload2,table,db)    