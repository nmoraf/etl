# si es repeteix veure comentari a 62bis, ja que estem contant repetides vegades un mateix registre original pel problema de id_cip / id_cip_sec
from sisapUtils import getSubTables, getOne, multiprocess, getAll, writeCSV, tempFolder, readCSV, createTable, listToTable, grantSelect
from collections import defaultdict


db = 'import'
data = 2015
ct = ('sisap_ct_lab', 'redics')


def upload_ct():
    file = tempFolder + 'cat_mfuste.csv'  # convertit de xls enviat per mmedina 2-6-16 12:48h
    upload = []
    i = 0
    for row in readCSV(file, sep=';'):
        if i > 0:
            cod = row[0]
            des = row[2]
            try:
                preu = float(row[8].replace('\x80', '').replace('.', '').replace(',', '.').replace('-', '').strip())
            except:
                preu = None
            if cod == 'Q32666':
                base = preu
            upload.append([cod, des, preu])
        else:
            i = 1
    for row in upload:
        urv = round(row[2] / base, 2) if row[2] else None
        row.append(urv)
    createTable(ct[0], '(cod varchar(20), des varchar(500), preu number, urv number)', ct[1], rm=True)
    listToTable(upload, *ct)
    grantSelect(ct[0], 'PREDUMMP', ct[1])


def get_catalog():
    lab = {cod: (des, cat) for (cod, des, cat) in getAll('select cla_codi, cla_desc, cla_versio_cataleg from redics.labtb111', ct[1])}
    urv = {cod: round(val, 2) for (cod, val) in getAll('select cod, urv from {} where urv is not null'.format(ct[0]), ct[1])}
    prv = {(cataleg, intern): urv[oficial] for (cataleg, intern, oficial) in getAll('select dvc_codi_versio, dvc_codi_prova, dvc_codi_oficial from redics.labtb136', ct[1]) if oficial in urv}
    ups = {up: (amb, sap, br, des) for (up, amb, sap, br, des) in getAll('select scs_codi, amb_desc, sap_desc, ics_codi, ics_desc from cat_centres_with_jail', 'nodrizas')}
    for up, br, des in getAll('select up_codi_up_scs, a.up_codi_up_ics, up_desc_up_ics from gcctb008 a, gcctb007 b where a.up_codi_up_ics = b.up_codi_up_ics', 'redics'):
        if up not in ups:
            ups[up] = (None, None, br, des)
    for up, des in getAll('select u_pr_cod_up, max(u_pr_descripcio) from cat_pritb008 group by u_pr_cod_up', 'import'):
        if up not in ups:
            ups[up] = (None, None, None, des)
    ups[''] = (None, None, None, 'sense up')
    return lab, prv, ups


def get_tables():
    tables = []
    for table in getSubTables('laboratori'):
        dat, = getOne("select cr_data_reg from {} where cr_codi_lab <> 'RIMAP' limit 1".format(table), db)
        if dat.year == data:
            tables.append(table)
    return tables


def get_data(table):
    dades = {}
    sql = "select cr_codi_lab, codi_up, cr_codi_prova_ics from {}".format(table)
    for lab, up, prv in getAll(sql, db):
        key = (lab, up)
        cat = lab_ct[lab][1]
        id = (cat, prv)
        if id in prv_ct:
            if key not in dades:
                dades[key] = [0, 0]
            dades[key][0] += 1
            dades[key][1] += prv_ct[id]
    return dades


lab_ct, prv_ct, up_ct = get_catalog()


if __name__ == '__main__':
    # upload_ct()
    resul = {}
    for dades in multiprocess(get_data, get_tables()):
        for key, (n, urv) in dades.items():
            if key not in resul:
                resul[key] = [0, 0]
            resul[key][0] += n
            resul[key][1] += urv
    upload = []
    for (lab, up), (n, urv) in resul.items():
        up_data = up_ct[up] if up in up_ct else (None, None, None, '{}: up desconeguda'.format(up))
        upload.append(up_data + (lab, lab_ct[lab][0], n, str(urv).replace('.', ',')))
    writeCSV(tempFolder + 'laboratori_2015.csv', upload, sep=';')
