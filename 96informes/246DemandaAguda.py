# coding: iso-8859-1

import sisapUtils as u

class DemandaAguda(object):

    def __init__(self):
        """ . """

        self.get_prstb337()
        self.get_prstb101()
        self.get_u11()
        self.get_upload()
        self.upload_todb()


    def get_prstb337(self):
        """
        TOTS
         ['PI_CODI_SEQ', 'PI_CIP', 'PI_CODI_PC', 'PI_DATA_INICI', 'PI_DATA_FI', 'PI_ALIES', 'PI_ANAGRAMA',
         'PI_LATERALITAT', 'PI_MOT_TANCAMENT', 'PI_MOT_ALTRES', 'PI_SER_DERIVAT', 'PI_SER_DERIVAT_ALTRES',
         'PI_PLA_TERAPEUTIC', 'PI_USU_ALTA', 'PI_DATA_ALTA', 'PI_USU_MOD', 'PI_DATA_MOD', 'PI_USU_BAIXA',
         'PI_DATA_IMP_PLA', 'PI_TXT_PREFIXAT', 'PI_ALTRES_CA', 'PI_V_CEN', 'PI_V_CLA', 'PI_V_SER', 
         'PI_V_MOD', 'PI_V_DAT', 'PI_V_NUM', 'PI_UP', 'PI_SAP', 'PI_EP']
         
        SÍ (s'inclou pi_alies (text))
        ['PI_CODI_SEQ', 'PI_CIP', 'PI_CODI_PC', 'PI_DATA_INICI', 'PI_DATA_FI', 
         'PI_ALIES', 'PI_ANAGRAMA', 'PI_LATERALITAT', 'PI_MOT_TANCAMENT','PI_SER_DERIVAT',
         'PI_USU_ALTA', 'PI_DATA_ALTA', 'PI_USU_MOD', 'PI_DATA_MOD','PI_USU_BAIXA',
         'PI_DATA_IMP_PLA', 
         'PI_V_CEN''PI_V_CLA', 'PI_V_SER', 'PI_V_MOD', 'PI_V_DAT', 'PI_V_NUM',
         'PI_UP', 'PI_SAP', 'PI_EP']

        NO (són texte):
         'PI_MOT_ALTRES' PI_MOT_ALTRES varchar(150)
         'PI_SER_DERIVAT_ALTRES' PI_SER_DERIVAT_ALTRES varchar(200)
         'PI_PLA_TERAPEUTIC' PI_PLA_TERAPEUTIC varchar(2000)
         'PI_TXT_PREFIXAT' PI_TXT_PREFIXAT varchar(2000)
         'PI_ALTRES_CA' PI_ALTRES_CA varchar(2000)
        """
        print("---------------------------------------------- get_prstb337")
        self.prstb337 = []
        sql = "SELECT pi_codi_seq, \
                      cip_usuari_cip, \
                      pi_codi_pc, \
                      pi_data_inici, pi_data_fi, \
                      PI_ALIES, PI_ANAGRAMA, PI_LATERALITAT, PI_MOT_TANCAMENT, PI_SER_DERIVAT, \
                      PI_USU_ALTA, PI_DATA_ALTA, \
                      PI_USU_MOD, PI_DATA_MOD, \
                      PI_USU_BAIXA, PI_DATA_IMP_PLA, \
                      PI_V_CEN, PI_V_CLA, PI_V_SER, PI_V_MOD, PI_V_DAT, PI_V_NUM, \
                      PI_UP, PI_SAP, PI_EP \
               from prstb337, usutb011 where pi_cip=cip_cip_anterior \
                    and PI_CODI_PC='PC1000' \
                    and PI_DATA_INICI between to_date('01/01/2020','dd/mm/yyyy') \
                        and to_date('31/12/2020','dd/mm/yyyy')"
        for sector in u.sectors:
            print(sector)
            db = sector
            #if sector not in ('6625'):
            for codi_seq, cip13, codi_pc, data_inici, data_fi, \
                alies, anagrama, lateritat, mot_tanca, ser_der, \
                usu_alta, data_alta, usu_mod, data_mod, \
                usu_baixa, data_imp_pla, \
                v_cen, v_cla, v_ser, v_mod, v_dat, v_num, \
                up, sap, ep in u.getAll(sql, db):
                self.prstb337.append((codi_seq, cip13, sector, codi_pc, data_inici, data_fi, 
                                      alies, anagrama, lateritat, mot_tanca, ser_der, 
                                      usu_alta, data_alta,
                                      usu_mod, data_mod,
                                      usu_baixa, data_imp_pla, 
                                      v_cen, v_cla, v_ser, v_mod, v_dat, v_num,
                                      up, sap, ep))

    def get_prstb101(self):
        """ . """
        print("---------------------------------------------- get_prstb101")        
        self.prstb101 = {}
        sql = "select usua_cip_cod, usua_cip from pdptb101"
        db = "pdp"
        for hash_d, cip13 in u.getAll(sql, db):
            self.prstb101[cip13] = {'hash_d': hash_d}

    def get_u11(self):
        """ . """
        print("---------------------------------------------- get_u11")
        self.u11 = {}
        sql = "select id_cip, id_cip_sec, codi_sector, hash_d from u11"
        db = "import"
        for id_cip, id_cip_sec, sector, hash_d in u.getAll(sql, db):
            self.u11[(sector,hash_d)] = {'id_cip': id_cip, 'id_cip_sec': id_cip_sec}

    def get_upload(self):
        """ . """
        print("---------------------------------------------- get_upload")
        self.upload = []
        for codi_seq, cip13, sector, codi_pc, data_inici, data_fi, \
            alies, anagrama, lateritat, mot_tanca, ser_der, \
            usu_alta, data_alta, usu_mod, data_mod, \
            usu_baixa, data_imp_pla, \
            v_cen, v_cla, v_ser, v_mod, v_dat, v_num, \
            up, sap, ep in self.prstb337:
            hash_d = self.prstb101[cip13]['hash_d'] if cip13 in self.prstb101 else None
            id_cip = self.u11[(sector,hash_d)]['id_cip'] if (sector,hash_d) in self.u11 else None
            id_cip_sec = self.u11[(sector,hash_d)]['id_cip_sec'] if (sector,hash_d) in self.u11 else None
            self.upload.append((codi_seq, id_cip, id_cip_sec, sector, 
                                codi_pc, data_inici, data_fi, 
                                alies, anagrama, lateritat, mot_tanca, ser_der, 
                                usu_alta, data_alta,
                                usu_mod, data_mod,
                                usu_baixa, data_imp_pla, 
                                v_cen, v_cla, v_ser, v_mod, v_dat, v_num,
                                up, sap, ep))

    def upload_todb(self):
        """."""
        print("---------------------------------------------- upload_todb")
        tb = "prstb337"
        db = "test"
        columns = ["pi_cod_seq varchar(14)",
                   "id_cip int",
                   "id_cip_sec int",
                   "sector varchar(4)",                   
                   "codi_pc varchar(12)",
                   "pi_data_inici date",
                   "pi_data_fi date",
                   "pi_alies varchar(200)",
                   "pi_anagrama varchar(10)",
                   "pi_lateritat varchar(1)",
                   "pi_mot_tancament varchar(2)",
                   "pi_ser_derivat varchar(5)",
                   "pi_usu_alta varchar(30)",
                   "pi_data_alta date",
                   "pi_usu_mod varchar(30)",
                   "pi_data_mod date",
                   "pi_usu_baixa varchar(30)",
                   "pi_data_imp_pla date",
                   "pi_v_cen varchar(9)",
                   "pi_v_cla varchar(2)",
                   "pi_v_ser varchar(5)",
                   "pi_v_mod varchar(5)",
                   "pi_v_dat date",
                   "pi_v_num int",
                   "pi_up varchar(5)",
                   "pi_sap varchar(2)",
                   "pi_ep varchar(4)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)

if __name__ == "__main__":
    DemandaAguda()




        



""" cols = ["TOTAL_ACO number" if col == "TOTAL_ACO" else
        u.getColumnInformation(col, TABLE_O, DATABASE_O, desti="ora")["create"]
        for col in u.getTableColumns(TABLE_O, DATABASE_O)]
cols_s = "({})".format(", ".join(cols)) """