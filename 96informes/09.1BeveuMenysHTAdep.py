# coding: iso-8859-1
#Estela, Departament (ion 3558), juliol15, Ermengol
from sisapUtils import *
import csv,os,sys
from time import strftime

#faig taula de variables pq sino triga molt el proces!

debug = False

anys = [['2010'],['2011'],['2012'],['2013'],['2014'],['2015'],['2016'],['2017'],['2018'],['2019'],['2020']]

nod="nodrizas"
imp="import"
db="test"
eqa="eqa_ind"

dext="dextraccio"
catcen="cat_centres"
variables="variables"
teqa="mst_indicadors_pacient"

OutFile= tempFolder + '09PetiBeveuMenys2.txt'
ics='where ep=0208'
varOH = "('ALRIS','ALSET','ALHAB','ALDIA','CP202')"
psOH = "('F10','F10.1','F10.2','F10.0','F10.3','F10.4','F10.5','F10.6','F10.7','F10.8','F10.9','T51.9','Z50.2','Z71.4','Z72.1')"
htaoDepre = "('I10','I11','I11.0','I11.9','I12','I12.0','I12.9','I13','I13.0','I13.1','I13.2','I13.9','I15','I15.0','I15.1','I15.2','I15.8','I15.9','F32','F32.0','F32.1','F32.2','F32.3','F32.8','F32.9','F33','F33.0','F33.1','F33.2','F33.3','F33.4','F33.8','F33.9','F34','F34.0','F34.1','F34.8','F34.9','F38','F38.0','F38.1','F38.8','F43','F43.0','F43.1','F43.2','F43.8','F43.9')"
varEQA = "('832','8321','A-PCT','ALCOHO','ALDIA','ALHAB','ALRIS','ALSET','ANTIAL','AUDIT','CAGE','CAGEC','CALCO','CP202','EDU10','EDU11','EDU25','NALCO')"
problemEQA = "('F10','F10.0','F10.1','F10.2','F10.3','F10.4','F10.5','F10.6','F10.7','F10.8','F10.9','Z72.1')"

printTime('inici construccio variables')
sql = "drop table if exists variables"
execute(sql,db)
sql = "create table variables (id_cip int, id_cip_sec int,vu_cod_vs varchar(20),vu_val double,vu_dat_act date,vu_v_dat date)"
execute(sql,db)
sql = "insert into variables select * from import.variables where vu_cod_vs in {}".format(varOH)
execute(sql,db)

sql = "drop table if exists variablesEQA"
execute(sql,db)
sql = "create table variablesEQA (id_cip int, id_cip_sec int,vu_cod_vs varchar(20),vu_val double,vu_dat_act date,vu_v_dat date)"
execute(sql,db)
sql = "insert into variablesEQA select * from import.variables where vu_cod_vs in {}".format(varEQA)
execute(sql,db)

printTime('final construccio variables')

printTime('Inici procés')
def PsConverter(agr):

    if agr.startswith("F"):
        return 'DEPRESSIO'
    elif agr.startswith("I"):
        return 'HTA' 
    else:
        'ERROR' 
 
sql='select data_ext,year(data_ext),month(data_ext) from {}'.format(dext)
for d,year,mes in getAll(sql, nod):
    dataActual = d
    anyActual = int(year)
    mesActual = mes

centres={}
sql="select scs_codi,ics_codi,ics_desc,amb_desc from {0} {1}".format(catcen,ics)
for up,br,ics_desc,amb_desc in getAll(sql,nod):
    centres[up]={'br':br,'desc':ics_desc, 'amb':amb_desc}

table="alcohol_htadep" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (id_cip_sec int,periode varchar(10) not null default'',regio varchar(500) not null default'',up varchar(5) not null default'',updesc varchar(500) not null default'',edat varchar(50) not null default'',sexe varchar(1) not null default'',\
        hta double, depressio double,eqa double,ates double,ALRISd date not null default 0,ALRISv double,ALSETd date not null default 0,\
            ALSETv double,ALHABd date not null default 0,ALHABv double,ALDIAd date not null default 0,ALDIAv double,CP202d date not null default 0,CP202v double)".format(table)
execute(sql,db)
        
for aC in anys:
    anyCalcul = int(aC[0])
    if anyCalcul > anyActual:
        continue
    else:
        print anyCalcul
        assig= {}
        if anyCalcul == anyActual:
            dataCalcul = dataActual
            mesCalcul = mesActual
            sql= "select id_cip_sec,up,edat,sexe,ates from assignada_tot"
            for id_cip_sec,up,edat,sexe,ates in getAll(sql,nod):
                edat=int(edat)
                if edat > 14:
                    if 15 <= edat <= 29:
                        gedat='Entre 15 i 29'
                    if 30<=edat<=45:
                        gedat='Entre 30 i 45'
                    if 46<=edat<=64:
                        gedat='Entre 46 i 64'
                    if 65<=edat<=79:
                        gedat='Entre 65 i 79'
                    if edat >79:
                        gedat='80 o +'
                    try:
                        br=centres[up]['br']
                        idesc=centres[up]['desc']
                        ambit=centres[up]['amb']
                        assig[int(id_cip_sec)]= {'up':up,'br':br,'desc':idesc,'amb':ambit,'edat':edat,'sex':sexe,'GrupEdat':gedat,'ates':ates}
                    except KeyError:
                        ok=1
        else:
            dataCalcul = str(anyCalcul) +'-12-31'
            mesCalcul = dataCalcul[5:-3]
            dadesHist = {}
            sql = "select id_cip_sec,(year('{0}')-year(usua_data_naixement))-(date_format('{0}','%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe from assignada,nodrizas.dextraccio".format(dataCalcul)
            for id,edat,sexe in getAll(sql,imp):
                edat = int(edat)
                dadesHist[id] = {'edat':edat,'sexe':sexe}
            sql= "select id_cip_sec,up,ates from assignadaHistorica where dataany = '{}' {}".format(anyCalcul,' limit 10' if debug else '')
            for id_cip_sec,up,ates in getAll(sql,imp):
                try:
                    edat = dadesHist[id_cip_sec]['edat']
                    sexe = dadesHist[id_cip_sec]['sexe']
                except KeyError:
                    ok=1
                edat=int(edat)
                if edat > 14:
                    if 15 <= edat <= 29:
                        gedat='Entre 15 i 29'
                    if 30<=edat<=45:
                        gedat='Entre 30 i 45'
                    if 46<=edat<=64:
                        gedat='Entre 46 i 64'
                    if 65<=edat<=79:
                        gedat='Entre 65 i 79'
                    if edat >79:
                        gedat='80 o +'
                    try:
                        br=centres[up]['br']
                        idesc=centres[up]['desc']
                        ambit=centres[up]['amb']
                        assig[int(id_cip_sec)]= {'up':up,'br':br,'desc':idesc,'amb':ambit,'edat':edat,'sex':sexe,'GrupEdat':gedat,'ates':ates}
                    except KeyError:
                        ok=1
        periode = str(anyCalcul) + str(mesCalcul)
        alris={}
        sql="select id_cip_sec,vu_cod_vs,vu_val,vu_dat_act from {0} where vu_dat_act between date_add(date_add('{1}',interval - 24 month),interval + 1 day) and date_add('{1}',interval - 0 month) {2}".format(variables,dataCalcul,' limit 10' if debug else '')
        for id_cip_sec,agr,val,data in getAll(sql,db):
            id_cip_sec=int(id_cip_sec)
            if agr=='ALRIS':
                if id_cip_sec in alris:
                    if alris[id_cip_sec]['ALRISd']==0:
                        alris[id_cip_sec]['ALRISd']=data
                        alris[id_cip_sec]['ALRISv']=val
                    elif data>alris[id_cip_sec]['ALRISd']:
                        alris[id_cip_sec]['ALRISd']=data
                        alris[id_cip_sec]['ALRISv']=val
                else:
                    try:
                        alris[id_cip_sec]={'ALRISd':data,'ALRISv':val,'ALSETd':0,'ALSETv':'\N','ALHABd':0,'ALHABv':'\N','ALDIAd':0,'ALDIAv':'\N','CP202d':0,'CP202v':'\N'}
                    except KeyError:
                        ok=1
            elif agr=='ALSET':
                if id_cip_sec in alris:
                    if alris[id_cip_sec]['ALSETd']==0:
                        alris[id_cip_sec]['ALSETd']=data
                        alris[id_cip_sec]['ALSETv']=val
                    elif data>alris[id_cip_sec]['ALSETd']:
                        alris[id_cip_sec]['ALSETd']=data
                        alris[id_cip_sec]['ALSETv']=val
                else:
                    try:
                        alris[id_cip_sec]={'ALRISd':0,'ALRISv':'\N','ALSETd':data,'ALSETv':val,'ALHABd':0,'ALHABv':'\N','ALDIAd':0,'ALDIAv':'\N','CP202d':0,'CP202v':'\N'}
                    except KeyError:
                        ok=1
            elif agr=='ALHAB':
                if id_cip_sec in alris:
                    if alris[id_cip_sec]['ALHABd']==0:
                        alris[id_cip_sec]['ALHABd']=data
                        alris[id_cip_sec]['ALHABv']=val
                    elif data>alris[id_cip_sec]['ALHABd']:
                        alris[id_cip_sec]['ALHABd']=data
                        alris[id_cip_sec]['ALHABv']=val
                else:
                    try:
                        alris[id_cip_sec]={'ALRISd':0,'ALRISv':'\N','ALSETd':0,'ALSETv':'\N','ALHABd':data,'ALHABv':val,'ALDIAd':0,'ALDIAv':'\N','CP202d':0,'CP202v':'\N'}
                    except KeyError:
                        ok=1
            elif agr=='ALDIA':
                if id_cip_sec in alris:
                    if alris[id_cip_sec]['ALDIAd']==0:
                        alris[id_cip_sec]['ALDIAd']=data
                        alris[id_cip_sec]['ALDIAv']=val
                    elif data>alris[id_cip_sec]['ALDIAd']:
                        alris[id_cip_sec]['ALDIAd']=data
                        alris[id_cip_sec]['ALDIAv']=val
                else:
                    try:
                        alris[id_cip_sec]={'ALRISd':0,'ALRISv':'\N','ALSETd':0,'ALSETv':'\N','ALHABd':0,'ALHABv':'\N','ALDIAd':data,'ALDIAv':val,'CP202d':0,'CP202v':'\N'}
                    except KeyError:
                        ok=1
            elif agr=='CP202':
                if id_cip_sec in alris:
                    if alris[id_cip_sec]['CP202d']==0:
                        alris[id_cip_sec]['CP202d']=data
                        alris[id_cip_sec]['CP202v']=val
                    elif data>alris[id_cip_sec]['CP202d']:
                        alris[id_cip_sec]['CP202d']=data
                        alris[id_cip_sec]['CP202v']=val
                else:
                    try:
                        alris[id_cip_sec]={'ALRISd':0,'ALRISv':'\N','ALSETd':0,'ALSETv':'\N','ALHABd':0,'ALHABv':'\N','ALDIAd':0,'ALDIAv':'\N','CP202d':data,'CP202v':val}
                    except KeyError:
                        ok=1
        varEQA = {}
        sql="select id_cip_sec,vu_cod_vs from variables EQA where vu_dat_act between date_add(date_add('{0}',interval - 24 month),interval + 1 day) and date_add('{0}',interval - 0 month) {1}".format(dataCalcul,' limit 10' if debug else '')
        for id,agr in getAll(sql,db):
            varEQA[int(id)] = True
        sql= "select id_cip_sec,pr_cod_ps from problemes,nodrizas.dextraccio \
            where pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{0}' and (pr_data_baixa is null or pr_data_baixa>'{0}') and pr_cod_ps in {1}".format(dataCalcul,problemEQA)
        for id,ps in getAll(sql,imp):
            varEQA[int(id)] = True
        PsEQA={}
        sql= "select id_cip_sec,pr_cod_ps from problemes,nodrizas.dextraccio \
            where pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{0}' and (pr_data_baixa is null or pr_data_baixa>'{0}') and pr_cod_ps in {1}".format(dataCalcul,htaoDepre)
        for id,ps in getAll(sql,imp):
            id=int(id)
            agr = PsConverter(ps)
            if agr == 'HTA':
                if id in PsEQA:
                    PsEQA[id]['HTA'] = 1
                else:
                    PsEQA[id] = {'HTA':1,'DEP':0}
            elif agr=='DEPRESSIO':
                if id in PsEQA:
                    PsEQA[id]['DEP'] = 1
                else:
                    PsEQA[id] = {'HTA':0,'DEP':1}
        with open(OutFile,'wb') as file:
            w= csv.writer(file, delimiter='@', quotechar='|')
            for (id),ps in PsEQA.items():
                id = int(id)
                hta = ps['HTA']
                depre = ps['DEP']
                indEQA = 0
                try:
                    up = assig[id]['br']
                    desc = assig[id]['desc']
                    amb = assig[id]['amb']
                    edat = assig[id]['GrupEdat']
                    sex = assig[id]['sex']
                    ates = assig[id]['ates']
                except KeyError:
                    continue
                try:    
                    if varEQA[id]:
                        indEQA = 1
                except KeyError:
                    indEQA = 0
                try:
                    ALRISd = alris[id]['ALRISd']
                except KeyError:
                    ALRISd = '\N'
                try:
                    ALRISv = alris[id]['ALRISv']
                except KeyError:
                    ALRISv = '\N'
                try: 
                    ALSETd = alris[id]['ALSETd']
                except KeyError:
                    ALSETd = '\N'
                try:
                    ALSETv = alris[id]['ALSETv']
                except KeyError:
                    ALSETv = '\N'
                try:
                    ALHABd = alris[id]['ALHABd']
                except KeyError:
                    ALHABd = '\N'
                try:
                    ALHABv = alris[id]['ALHABv']
                except KeyError:
                    ALHABv = '\N'
                try:
                    ALDIAd = alris[id]['ALDIAd']
                except KeyError:
                    ALDIAd = '\N'
                try:
                    ALDIAv = alris[id]['ALDIAv']
                except KeyError:
                    ALDIAv = '\N'
                try:
                    CP202d = alris[id]['CP202d']
                except KeyError:
                    CP202d = '\N'
                try:
                    CP202v = alris[id]['CP202V']
                except KeyError:
                    CP202v = '\N'
                w.writerow([id,periode,amb,up,desc,edat,sex,hta,depre,indEQA,ates,ALRISd,ALRISv,ALSETd,ALSETv,ALHABd,ALHABv,ALDIAd,ALDIAv,CP202d,CP202v])
        loadData(OutFile,table,db)
        

printTime('Inici Agrupar')
bMenysV={}
sql="select id_cip_sec,periode,regio,up,updesc,edat,sexe,HTA, depressio,eqa,ates,alrisd,alrisv,alsetd,alsetv,alhabd,alhabv,aldiad,aldiav,cp202d,cp202v from {}".format(table)
for id_cip_sec,periode,regio,up,updesc,edat,sexe,hta,depre,eqa,ates,alrisd,alrisv,alsetd,alsetv,alhabd,alhabv,aldiad,aldiav,cp202d,cp202v in getAll(sql,db):
    if alsetv==None:
        alsetv=0
    if alhabv==None:
        alhabv=0
    if aldiav==None:
        aldiav=0
    try:    
        alrisv=int(alrisv)
    except TypeError:
        ok=1
    try:
        cp202v=int(cp202v)
    except TypeError:
        ok=1        
    if (up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v) in bMenysV:
        bMenysV[(up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v)]['n']+=1
        bMenysV[(up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v)]['Alset']+=alsetv
        bMenysV[(up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v)]['Alhab']+=alhabv
        bMenysV[(up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v)]['Aldia']+=aldiav
    else:
        try:
            bMenysV[(up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v)]={'n':1,'Alset':alsetv,'Alhab':alhabv,'Aldia':aldiav}
        except KeyError:
            continue
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,periode,regio,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v),d in bMenysV.items():
        try:
            w.writerow([periode,regio,up,updesc,edat,sexe,hta,depre,ates,alrisv,cp202v,d['n'],d['Alset'],d['Alhab'],d['Aldia']])
        except KeyError:
            continue   
bMenysV.clear()            

printTime('Fi procés')
