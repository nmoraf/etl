from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from taulesKhalix_utils import *

path = ecapFolder

redics = 'redics'
imp = 'import'
nod = 'nodrizas'

taula = 'sisap_khx_dades'

centresBr, centresUp = {},{}
sql = "select if(up_codi_up_ics='04374','BR366',if(up_codi_up_ics like '0%',concat('B',right(up_codi_up_ics,4)),up_codi_up_ics)),up_codi_up_scs from cat_gcctb008 where up_data_baixa = 0"
for br,up in getAll(sql,imp):
    centresBr[br] = {'up':up}
    centresUp[up] = {'br':br}

    
eap = {}
sql = "select ics_codi from cat_centres"
for br, in getAll(sql,nod):
    eap[br] = True
    
NotinKhx = {}
sql = "select if(up_codi_up_ics='04374',0,if(up_codi_up_ics like '0%',1,0)),if(up_codi_up_ics='04374','BR366',if(up_codi_up_ics like '0%',concat('B',right(up_codi_up_ics,4)),up_codi_up_ics)) from cat_gcctb008 where up_data_baixa = 0"
for inkhx, brmodi in getAll(sql,imp):
    if inkhx == 1:
        if brmodi in eap:
            ok = 1
        else:
            NotinKhx[brmodi] = True
    

upFromAbs = {}
sql = "select abs_codi_abs,abs_codi_up from cat_rittb001"
for abs, up in getAll(sql,imp):
    upFromAbs[str(abs)] = {'up':up}

fitxersKhx = {}
for fitxer in fitxers:
    fit = fitxer[0]
    fitxersKhx[fit] = True
    

periodesExport = {}
resumFitxers = []
for file in os.listdir(path):
    file = file.upper()
    fitxer = getKhxFitxer(file)
    pathFile = path + file
    if fitxer in fitxersKhx:
        print fitxer
        upload = []
        sumaValors = Counter()
        tipusDada = getKhxTipDada(fitxer)
        mes = getKhxmes(file)
        anys = getKhxdataany(file)
        anysKhx = anys[2:]
        print tipusDada, anys, mes, anysKhx
        if mes <> '12MES':
            periode = 'A' + anysKhx + mes
            periodesExport[periode,tipusDada, anysKhx] = {'mes':mes,'anys':anys}
        needSum = getNeedSum(fitxer,tipusDada)     
        with open(pathFile, 'rb') as fitxerKhx:
            p=csv.reader(fitxerKhx, delimiter='{')
            for ind in p:
                entitat = ind[getKhxUP(fitxer)]
                fromUp = getUpFromAbs(fitxer,tipusDada)
                calcUp = CalculUP(fitxer,tipusDada)
                if fromUp:
                    descABS = ind[getDescAbs(fitxer,tipusDada)]
                    if  'LLEIDA 7' in descABS:
                        continue
                    else:
                        try:
                            entitat = upFromAbs[entitat]['up']
                        except KeyError:
                            continue
                if calcUp:
                    try:
                        if centresBr[entitat]:
                            entitat = entitat
                    except KeyError:
                        try:
                            if centresUp[entitat]:
                                entitat = centresUp[entitat]['br']
                        except KeyError:
                            ok=1
                if entitat == 'ICS':
                    entitat = 'AMBITOS'
                try:
                    compte = ind[getCompte(fitxer,tipusDada)]
                except:
                    compte = getCompte(fitxer,tipusDada)               
                if tipusDada == 'MADS':
                    compte = 'Q' + compte.replace('-','').upper()
                try:
                    tipus = ind[getTipus(fitxer,tipusDada)]
                except:
                    tipus = getTipus(fitxer,tipusDada)
                if tipusDada == 'MADS':
				    tipus = tipus.upper()
                detalls = getDetalls(fitxer,tipusDada)
                controls = getControls(fitxer,tipusDada)
                inKhalix = getInKhalix(file,tipusDada)
                if inKhalix == 0:
                    try:
                        if eap[entitat]:
                            inKhalix = 1
                    except KeyError:
                        inKhalix = 0
                NotinKhalix = getInKhalixExt(file,tipusDada)
                if NotinKhalix == 0:
                    try:
                        if NotinKhx[entitat]:
                            inKhalix = 0
                    except KeyError:
                        ok=1
                nAnalis = {}
                cAnalis = CalcAnalisi(fitxer,tipusDada)
                if cAnalis:
                    for dim in dimensions:
                        dimensio = dim[0]
                        cValor = getCalcValor(fitxer,dimensio)
                        if cValor:
                            try:
                                valor = ind[getValor(fitxer,dimensio)].replace(',','.')
                                nAnalis[(dimensio,periode)] ={'valor':valor}
                            except:
                                ok = 1
                else:
                    try:
                        analisiclass = ind[getAnalisiClassic(fitxer,tipusDada)]
                    except:
                        analisiclass = getAnalisiClassic(fitxer,tipusDada)
                    if mes == '12MES':
                        for perio in period:
                            mesos = perio[0]
                            periode = 'A' + anysKhx + mesos
                            periodesExport['periode',tipusDada, anysKhx] = {'mes':mesos,'anys':anys}
                            try:
                                valor12m = ind[getValor12m(fitxer,tipusDada,mesos)].replace(',','.')
                            except:
                                valor12m = getValor12m(fitxer,tipusDada,mesos)
                            nAnalis[(analisiclass,periode)] ={'valor':valor12m}
                    else:
                        valorclass = ind[getValorclassic(fitxer,tipusDada)].replace(',','.')
                        if tipusDada == 'MADS':
                            if valorclass == 'NULL':
                                valorclass = 0
                            elif valorclass == 'n/a':
                                valorclass = -1  
                        nAnalis[(analisiclass,periode)] = {'valor':valorclass}
                for (analisi,periode),val in nAnalis.items():
                    try:
                        valor = float(val['valor'])
                    except ValueError:
                        valor = 0
                    if needSum:
                        sumaValors[(compte,periode,entitat,analisi,tipus,detalls,controls,file,tipusDada,inKhalix)] += valor
                    else:
                        if analisi == '':
                            analisi = " "
                        upload.append([compte,periode,entitat,analisi,tipus,detalls,controls,valor,file,tipusDada,inKhalix])
        if needSum:
            for (compte,periode,entitat,analisi,tipus,detalls,controls,file,tipusDada,inKhalix),resultat in sumaValors.items():
                valor = resultat
                upload.append([compte,periode,entitat,analisi,tipus,detalls,controls,valor,file,tipusDada,inKhalix])
        print 'ok', taula, file
        sql = "select * from {} where rownum<1".format(taula)
        execute(sql,redics)
        sql = "delete from {0} where fitxer='{1}'".format(taula,file)
        execute(sql,redics)
        uploadOra(upload,taula,redics)
        resumFitxers.append([file])
        folder = getFolder(tipusDada)
        moveToKhalix(file,path,folder,originals=True)


conn= connect('redics')
c= conn.cursor()

for (periodes,tipusDada, anysKhx),d in periodesExport.items():
    mes = d['mes']
    anys = d['anys']
    folder = 'Cargas_pendientes_ultimo_mes'
    Export = {}
    Name12 = getName12m(tipusDada)
    exp = 'A' + anysKhx
    if Name12:
        nameFile = getNameFile(tipusDada) + anys + '.txt'
    else:
        nameFile = getNameFile(tipusDada) + mes + '_' + anys + '.txt'
    path = tempFolder + nameFile
    with openCSV(path,sep='{') as w:
        if Name12:
            sql = "select compte,periode,entitat,analisi,tipus,detalls,controls,valor from {0} where tipus_dada='{1}' and inKhalix = 1 and periode like ('{2}%%')".format(taula,tipusDada,exp)
        else:
            sql = "select compte,periode,entitat,analisi,tipus,detalls,controls,valor from {0} where periode='{1}' and tipus_dada='{2}' and inKhalix = 1".format(taula,periodes,tipusDada)
        for compte,periode,entitat,analisi,tipus,detalls,controls,valor in c.execute(sql):
            valor = round(valor,2)
            w.writerow([compte,periode,entitat,analisi,tipus,detalls,controls,'N',valor])
    moveToKhalix(nameFile,tempFolder,folder)

if resumFitxers:
    text= "\r\n\r\nFitxers generats avui:\r\n\r\n" + '\r\n'.join(map(str,resumFitxers))
