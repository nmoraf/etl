#mireia fabregas, agost14, francesc

from sisapUtils import *
import time,csv
from time import strftime

debug= False
limit= ' limit 10000' if debug else ''

conn= connect('import')
cursor= conn.cursor()

print 'th: ' + strftime("%Y-%m-%d %H:%M:%S")
switch= {0:0,1240:1,276:0,1239:0,1241:9,1242:9}
pacients= {}
cursor.execute("select id_cip_sec,pr_th from import.problemes,nodrizas.dextraccio where pr_cod_ps='I48' and pr_cod_o_ps='C' and pr_dde <= data_ext and (pr_dba = 0 or pr_dba > data_ext) and pr_hist=1{0}".format(limit))
for id,th in cursor:
    try:
        existent= pacients[int(id)]
    except KeyError:
        existent= -1
    try:
        if switch[int(th)]> existent:
            pacients[int(id)]= switch[int(th)]
    except KeyError:
        print 'thesaurus no classificat: ' + str(int(th))

print 'pob: ' + strftime("%Y-%m-%d %H:%M:%S")
poblacio,acfa,ambit,sap,medea= {},{},{},{},{}
cursor.execute("select distinct scs_codi,amb_codi,sap_codi,medea from nodrizas.cat_centres where ep='0208'")
for up,a,s,m in cursor:
    ambit[up]= a
    sap[up]= s
    medea[up]= m
cursor.execute("select id_cip_sec,edat,sexe,up from nodrizas.assignada_tot where edat between 15 and 90 and ates=1 and maca=0 and institucionalitzat=0{0}".format(limit))
for id,edat,sexe,up in cursor:
    try:
        poblacio[int(id)]= [int(edat),sexe,ambit[up],sap[up],up,medea[up]]
    except KeyError:
        pass
for key,value in pacients.iteritems():
    try:
        if value< 9:
            acfa[key]= {'tipus':value,'edat':poblacio[key][0],'sexe':poblacio[key][1],'ambit':poblacio[key][2],'sap':poblacio[key][3],'eap':poblacio[key][4],'medea':poblacio[key][5]}
    except KeyError:
        pass
poblacio.clear()
pacients.clear()

print 'tx: ' + strftime("%Y-%m-%d %H:%M:%S")
cursor.execute("select id_cip_sec,pf_cod_atc from import.tractaments,nodrizas.dextraccio where pf_cod_atc in ('B01AA07','B01AA03','B01AE07','B01AF02','B01AF01','B01AC06','B01AC04') and ppfmc_pmc_data_ini<=data_ext and ppfmc_data_fi>data_ext{0}".format(limit))
for id,atc in cursor:
    try:
        acfa[int(id)][atc]= 1
    except KeyError:
        pass

print 'ps: ' + strftime("%Y-%m-%d %H:%M:%S")
cursor.execute("select id_cip_sec, if(ps in (7, 212), 'AVC', if(ps = 21, 'ICC', 'error')) from nodrizas.eqa_problemes where ps in (7,212,21){0}".format(limit))
for id,ps in cursor:
    try:
        acfa[int(id)][ps]= 1
    except KeyError:
        pass        

print 'var: ' + strftime("%Y-%m-%d %H:%M:%S")
#compte 1a, variable nova
dat= {}
cursor.execute("select id_cip_sec,vu_val,date_format(vu_dat_act,'%Y%m%d') from import.variables1,nodrizas.dextraccio where vu_cod_vs='5844' and vu_dat_act<=data_ext{0}".format(limit))
for id,val,dat in cursor:
    try:
        existent= dat[int(id)]
    except IndexError:
        existent= '19990101'
    if dat> existent:
        try:
            acfa[int(id)]['hasbled']= int(val)
        except KeyError:
            pass
        
print 'ind: ' + strftime("%Y-%m-%d %H:%M:%S")
#si falla, cal tirar indicador acfa
cursor.execute("select id_cip_sec,num,ci,chads2 from eqa_ind.eqa_acfa")
for id,num,ci,chads2 in cursor:
    try:
        acfa[int(id)]['num']= int(num)
        acfa[int(id)]['ci']= int(ci)
        acfa[int(id)]['chads2']= int(chads2)
    except KeyError:
        pass

print 'inr: ' + strftime("%Y-%m-%d %H:%M:%S")
#cal tirar primer indicador inr, comentant els drop table corresponents
cursor.execute("select id_cip_sec,ind_a,ind_e from eqa_ind.inr7")
for id,ind_a,ind_e in cursor:
    try:
        acfa[int(id)]['temps_a']= float(ind_a)
        acfa[int(id)]['temps_e']= float(ind_e)
    except KeyError:
        pass
cursor.execute("select id_cip_sec,valor,concat('inr',usar) from eqa_ind.inr2 where usar<4")
for id,val,num in cursor:
    try:
        acfa[int(id)][num]= float(val)
    except KeyError:
        pass
cursor.execute("select id_cip_sec,inf,sup from eqa_ind.inrRang")
for id,inf,sup in cursor:
    try:
        acfa[int(id)]['inf']= float(inf)
        acfa[int(id)]['sup']= float(sup)
    except KeyError:
        pass
        
print 'txt: ' + strftime("%Y-%m-%d %H:%M:%S")
vars= ['tipus','edat','sexe','ambit','sap','eap','medea','chads2','hasbled','AVC','ICC','B01AA07','B01AA03','B01AE07','B01AF02','B01AF01','B01AC06','B01AC04','num','ci','temps_a','temps_e','inr1','inr2','inr3','inf','sup']
default= {'tipus':-1,'edat':-1,'sexe':'','ambit':'','sap':'','eap':'','medea':'','chads2':-1,'hasbled':-1,'AVC':0,'ICC':0,'B01AA07':0,'B01AA03':0,'B01AE07':0,'B01AF02':0,'B01AF01':0,'B01AC06':0,'B01AC04':0,'num':-1,'ci':-1,'temps_a':-1,'temps_e':-1,'inr1':-1,'inr2':-1,'inr3':-1,'inf':-1,'sup':-1}
with open(tempFolder + 'acfa.txt', 'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    header= ['id']+vars
    w.writerow(header)
    for key in acfa.keys():
        row= [key]
        for var in vars:
            try:
                val= acfa[key][var]
            except KeyError:
                val= default[var]
            row.append(val)
        w.writerow(row)

print 'my: ' + strftime("%Y-%m-%d %H:%M:%S")      
cursor.execute("drop table if exists test.acfa")
cursor.execute("create table test.acfa (id_cip_sec int,tipus int,edat int,sexe varchar(1),ambit varchar(2),sap varchar(2),eap varchar(5),medea varchar(2),chads2 int,hasbled int,avc int,icc int,b01aa07 int,b01aa03 int,b01ae07 int,b01af02 int,b01af01 int,b01ac06 int,b01ac04 int,num int,ci int,temps_a double,temps_e double,inr1 double,inr2 double,inr3 double,inf double,sup double)")
cursor.execute("LOAD DATA LOCAL INFILE '{0}acfa.txt' INTO TABLE test.acfa FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n' IGNORE 1 LINES".format(tempFolder))

cursor.close()
conn.close()

print 'fi: ' + strftime("%Y-%m-%d %H:%M:%S")
