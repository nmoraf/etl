# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


debug = False

nod = 'nodrizas'
imp = 'import'

anys = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]

cdemencia = ('F00', 'F00.0', 'F00.1', 'F00.2', 'F00.9', 'F01', 'F01.0', 'F01.1', 'F01.2', 'F01.3', 'F01.8', 'F01.9', 'F02', 'F02.0', 'F02.1', 'F02.2', 'F02.3', 'F02.4', 'F02.8', 'F03', 'G30', 'G30.0', 'G30.1', 'G30.8', 'G30.9', 'G31.8')
cAlzheimer = "('G30', 'G30.0', 'G30.1', 'G30.8','G30.9','F00', 'F00.0', 'F00.1','F00.2','F00.9')"
ccog = "('F06.7')"

centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, nod):
    centres[up] = True

all_pob = {}
sql = 'select id_cip_sec, usua_sexe, year(usua_data_naixement) from assignada {0}'. format(' limit 10' if debug else '')
for id,  sexe, naix in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix}
   
dAlzheimer = defaultdict(list)
sql = "select  id_cip_sec, extract(year from pr_dde) from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa is null  and pr_dba is null".format(cAlzheimer)
for id, Ydde in getAll(sql, imp):
    Ydde = str(Ydde)
    dAlzheimer[id].append(Ydde)
    
ddemencia = defaultdict(list)
sql = "select  id_cip_sec, extract(year from pr_dde) from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa is null  and pr_dba is null".format(cdemencia)
for id, Ydde in getAll(sql, imp):
    Ydde = str(Ydde)
    ddemencia[id].append(Ydde)   
    
dcog = defaultdict(list)
sql = "select  id_cip_sec, extract(year from pr_dde) from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa is null  and pr_dba is null".format(ccog)
for id, Ydde in getAll(sql, imp):
    Ydde = str(Ydde)
    dcog[id].append(Ydde)

def grup_edat(edat):

    if edat < 65:
        grupedat = 'Menors de 65 anys'
    elif 65<= edat <75:
        grupedat = 'Entre 65 i 74'
    elif 75<= edat <85:
        grupedat = 'Entre 75 i 84'
    elif 85<= edat:
        grupedat = 'Majors de 84 anys'
    else:
        grupedat = 'error'
    
    return grupedat
    
visites = {}
for ane in anys:
    for table in getSubTables('visites'):
        if ane < 2014:
            dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat == ane:
                for id, up in getAll("select id_cip_sec,  visi_up  from {} where visi_situacio_visita = 'R'".format(table), 'import'):
                    visites[(id, ane)] = True    

resultatdc, resultatal, resultatdem, resultatat = Counter(), Counter(), Counter(), Counter()
for ane in anys:
    sql = "select id_cip_sec, up, ates from assignadahistorica where dataany={0} {1}".format(ane, ' limit 1' if debug else '')
    for id, up, ates in getAll(sql, imp):
        if up in centres:
            assig = 1
            if ane < 2014:
                if (id, ane) in visites:
                    ates = 1
                else:
                    ates = 0
            sexe = 'NS'
            edat, edat5 = 'NS', 'NS'
            try:
                sexe = all_pob[id]['sexe']
                naix = all_pob[id]['naix']
                edat = ane - naix
            except KeyError:
                pass
            if edat < 15:
                continue
            if edat <>'NS':
                edat5 = grup_edat(edat)
            periode = str(ane)
            cog, dem, alzh = 0, 0, 0
            if id in dcog:
                for data_ps in dcog[id]:
                    if data_ps <= periode:
                        cog = 1
            if id in ddemencia:
                for data_ps in ddemencia[id]:
                    if data_ps <= periode:
                        dem = 1
            if id in dAlzheimer:
                for data_ps in dAlzheimer[id]:
                    if data_ps <= periode:
                        alzh = 1
            if dem == 1:
                cog = 0
            if ates == 1:
                resultatat[(periode, edat5)] += ates
                resultatdc[(periode, edat5)] += cog
                resultatdem[(periode, edat5)] += dem
                resultatal[(periode, edat5)] += alzh

sql = "select id_cip_sec, up, edat, ates from assignada_tot "
for id, up, edat, ates in getAll(sql, nod):
    if up in centres:
        assig = 1
        if edat < 15:
            continue
        edat5 = grup_edat(edat)
        periode = str(2017)
        cog, dem, alzh = 0, 0, 0
        if id in dcog:
            for data_ps in dcog[id]:
                if data_ps <= periode:
                    cog = 1
        if id in ddemencia:
            for data_ps in ddemencia[id]:
                if data_ps <= periode:
                    dem = 1
        if id in dAlzheimer:
            for data_ps in dAlzheimer[id]:
                if data_ps <= periode:
                    alzh = 1
        if dem == 1:
            cog = 0
        if ates == 1:
            resultatat[(periode, edat5)] += ates
            resultatdc[(periode, edat5)] += cog
            resultatdem[(periode, edat5)] += dem
            resultatal[(periode, edat5)] += alzh
                
upload = []                
for (periode,  edat5), res in resultatat.items():
    res1 = resultatdc[(periode, edat5)]
    res2 = resultatdem[(periode, edat5)]
    res3 = resultatal[(periode, edat5)]
    upload.append([periode, edat5, res, res1, res2, res3])
    
table = 'evol_alzh'      
createTable(table, '(periode varchar(4), edat varchar(100), ates int, det_cog int, demencia int, alzheimer int)', 'test', rm=True)
listToTable(upload, table, 'test')

    