# -*- coding: utf8 -*-

#NECESSITEM TENIR ASS_VISITES I ASS_EMBARAS PER ID_CIP I NO ID_CIP_SEC (A BBDD TEST)

"""
.
"""

import sisapUtils as u
import collections as c
import os

from numpy import percentile

test = "test"
nod = "nodrizas"
imp = "import"



class Edimburgh(object):
    """."""

    def __init__(self):
        """."""
        self.year, = u.getOne("select year(data_ext) from dextraccio", nod)
        self.dext, = u.getOne("select data_ext from dextraccio", nod)
        self.get_visites()
        self.get_embaras()
        self.get_test()
        self.get_centres()
        self.get_dones()
        self.get_indicadors()
        self.upload_results()
        self.export_results()
        self.get_percentils()
        

    def get_visites(self):
        """."""
        self.visites = {}
        sql = "select id_cip, visi_data_visita, visi_up from ass_visites \
               where year(visi_data_visita) = {}".format(self.year)
        for id, data, up in u.getAll(sql, test):
            if id not in self.visites or data > self.visites[id][0]:
                self.visites[id] = (data, up)

    def get_embaras(self):
        """."""
        self.embaras = {}
        sql = "select id_cip, cod, inici, fi, puerperi from ass_embaras where year(fi) = {} and fi < '{}'".format(self.year, self.dext)
        for id, cod, inici, fi, puerperi in u.getAll(sql, test):
            if id not in self.embaras or fi > self.embaras[id][2]:
                self.embaras[id] = (cod, inici, fi, puerperi)
    
    def get_test(self):
        """."""
        self.test = c.defaultdict(set)
        self.primerTest = {}
        sql = "select id_cip, concat(codi_sector, ':', val_cod), val_data, val_val from assir216 where year(val_data)={} and val_var='PAPI019'".format(self.year)
        for id, cod, data, val in u.getAll(sql, imp):
            self.test[(id, cod)].add((data, int(val)))
            if id not in self.primerTest or data < self.primerTest[id, cod][0]:
                self.primerTest[(id, cod)] = (data, int(val))
    
    def get_centres(self):
        """."""
        sql = "select up, br_assir, assir from ass_centres"
        self.centres = {up: (br, des) for (up, br, des) in u.getAll(sql, nod)}

    def get_dones(self):
        """."""
        self.dones = {}
        
        self.dies = []
        for (id), (data, up) in self.visites.items():
            ind0, ind00, ind1, ind2, ind3, ind4, ind5, ind6 = 0,0,0,0,0, 0, 0, 0
            if up in self.centres:
                br, des = self.centres[up]
                if id in self.embaras:
                    ind0 = 1
                    cod, inici, fi, puerperi = self.embaras[id][0], self.embaras[id][1], self.embaras[id][2], self.embaras[id][3]
                    if puerperi == 1:
                        ind00 = 1
                    if (id, cod) in self.primerTest:
                        ind1 = 1
                        data, val = self.primerTest[id, cod][0], self.primerTest[id, cod][1]
                        ind4 = val
                        ind2 = u.daysBetween(fi,data)
                        w = int(ind2/7)
                        if 6 <= w:
                           ind3 = 1
                        if 9 <= val <= 11:
                            if (id, cod) in self.test:
                                for data2, val2 in self.test[id, cod]:
                                    if data2 > data:
                                        if ind3 == 1:
                                            ind5 = 1
                                            ind6 = val2
                                        else:
                                            d2 = u.daysBetween(fi, data2)
                                            w2 = int(d2/7)
                                            if 6<= w:
                                                ind5 = 1
                                                ind6 = val2
            if ind0 == 1:
                self.dones[id] = (br, des, ind0, ind00, ind1, ind2, ind3, ind4,ind5,ind6)
                if ind1 == 1:
                    self.dies.append(ind2)
                    
    def get_indicadors(self):
        """."""
        self.upload = []
        self.indicadors = c.Counter()
        
        for (id), (br, des, ind0, ind00, ind1, ind2, ind3, ind4,ind5,ind6) in self.dones.items():
            self.indicadors[(br, des, 'ind0')] += ind0
            if ind00 == 1:
                self.indicadors[(br, des, 'ind00')] += ind00
                self.indicadors[(br, des, 'ind1')] += ind1
                if ind1 == 1:
                    self.indicadors[(br, des, 'ind2')] += ind2
                    self.indicadors[(br, des, 'ind3')] += ind3
                    if ind3 == 0:
                        self.indicadors[(br, des, 'ind5.1')] += ind5
                        if ind4 < 9:
                            self.indicadors[(br, des, 'ind4.1')] += 1
                        elif 9 <= ind4 <= 11:
                            self.indicadors[(br, des, 'ind4.2')] += 1
                        elif ind4 > 11:
                            self.indicadors[(br, des, 'ind4.3')] += 1
                        if ind5 == 1:
                            if ind6 < 9:
                                self.indicadors[(br, des, 'ind6.1')] += 1
                            elif 9 <= ind6 <= 11:
                                self.indicadors[(br, des, 'ind6.2')] += 1
                            elif ind6 > 11:
                                self.indicadors[(br, des, 'ind6.3')] += 1
                    else:
                        self.indicadors[(br, des, 'ind5.2')] += ind5
                        if ind4 < 9:
                            self.indicadors[(br, des, 'ind4.4')] += 1
                        elif 9 <= ind4 <= 11:
                            self.indicadors[(br, des, 'ind4.5')] += 1
                        elif ind4 > 11:
                            self.indicadors[(br, des, 'ind4.6')] += 1
                        if ind5 == 1:
                            if ind6 < 9:
                                self.indicadors[(br, des, 'ind6.4')] += 1
                            elif 9 <= ind6 <= 11:
                                self.indicadors[(br, des, 'ind6.5')] += 1
                            elif ind6 > 11:
                                self.indicadors[(br, des, 'ind6.6')] += 1
            self.upload.append([id, br, des, ind0, ind00, ind1, ind2, ind3, ind4,ind5,ind6])
            
    def upload_results(self):
        """."""
        TableMy = 'test_Edimburgh_' + str(self.year)
        create = '(id int, br varchar(5), des varchar(150), embaras int, puerperi int, test int, diesEntre int, mes6week int, valor int,posterior int, valPost int)'
        u.createTable(TableMy,create,test, rm=True)
        u.listToTable(self.upload, TableMy, test)
    
    def export_results(self):
        """."""
        file = u.tempFolder + "test_edimburgh.csv"
        header = [("br", "assir", "indicador" ,"n")]
        dades = [k + (n,) for (k, n) in self.indicadors.items()]
        u.writeCSV(file, header + sorted(dades), sep=';')
    
    def get_percentils(self):
        """."""
        p20 = round(percentile(self.dies, 20), 2)
        p50 = round(percentile(self.dies, 50), 2)
        p80 = round(percentile(self.dies, 80), 2)
        print p20, p50, p80
        
if __name__ == '__main__':
    Edimburgh()


    