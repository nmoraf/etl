import sisapUtils as u

tables = ["SISAP_CORONAVIRUS_ACTIVITAT",
          "SISAP_CORONAVIRUS_ACTIVITAT_A",
          "SISAP_CORONAVIRUS_ACTIVITAT_P",
          "SISAP_CORONAVIRUS_CORBA",
          "SISAP_CORONAVIRUS_XML",
          "SISAP_CORONAVIRUS_XML_SEG",
          "SISAP_CORONAVIRUS_RESIDENCIES"]

grantees = [# interns
            "PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB",
            "PREDUEHE",
            # externs
            "PREDURAP", "PREDUJVG", "PREDUMPP", "PREDUJCD", "PREDUMER",
            "PREDUCAM", "PREDUNNC", "DDIAZA",   "PREDUJMS", "PREDUDDA",
            "PREDUMAG", "PREDUXMG", "PREDUXGM", "PREDUSLA", "PREDUADT",
            "PREDUXCP", "PREDUABO", "PREDUMAA",
            ]

for table in tables:
    for user in grantees:
        u.grantSelect(table, user, 'redics')

# DDIAZA    sembla ser també: PREDUDDA
# JMUÑOZS   sembla ser també: PREDUJMS

"""
TABLE = "sisap_coronavirus"
AGRUPAT = "sisap_coronavirus_recompte"
LLISTATS = "sisap_coronavirus_llistat"
RESIDENCIES = "sisap_coronavirus_residencies"
DATA = "sisap_coronavirus_llistat_data"
CORBA = "sisap_coronavirus_corba"
PCR = "sisap_coronavirus_pcr"
VISITES = "sisap_coronavirus_visites"
ACTIVITAT = "sisap_coronavirus_activitat"
ACTIVITAT_AGR = "sisap_coronavirus_activitat_a"
PROFESSIONALS = "sisap_coronavirus_activitat_p"
XML = "sisap_coronavirus_xml",
XML_SEGUIMENT =  "sisap_coronavirus_xml_seg"
"""