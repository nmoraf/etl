# -*- coding: latin1 -*-

"""
.
"""

import collections as c

import sisapUtils as u


DEBUG = False
PERIODE = ((2017, 1), (2020, 4))
TABLE = "SISAP_CENTRES_VISITES"
DATABASE = "pdp"


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.get_cataleg()
        self.get_jobs()
        self.get_visites()
        self.upload_data()

    def get_cataleg(self):
        """."""
        sql = "select sector, codi, classe, oficial from sisap_centres_cataleg"
        self.cataleg = {row[:3]: row[3] for row in u.getAll(sql, DATABASE)}

    def get_jobs(self):
        """."""
        self.jobs = []
        sql = "select year(visi_data_visita), month(visi_data_visita) from {} limit 1"  # noqa
        for taula in u.getSubTables("visites"):
            this = u.getOne(sql.format(taula), "import")
            if PERIODE[0] <= this <= PERIODE[1]:
                self.jobs.append((taula, this))

    def get_visites(self):
        """."""
        self.visites = []
        if DEBUG:
            dades = [self._get_visites_worker(self.jobs[0])]
        else:
            dades = u.multiprocess(self._get_visites_worker, self.jobs, 12)
        for periode, recompte in dades:
            for row in recompte:
                self.visites.append(periode + row)

    def upload_data(self):
        """."""
        cols = "(exercici int, periode int, centre varchar(10), visites int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.visites, TABLE, DATABASE)

    def _get_visites_worker(self, params):
        """."""
        taula, periode = params
        visites = c.Counter()
        sql = "select codi_sector, visi_centre_codi_centre, \
                      visi_centre_classe_centre \
               from {} \
               where visi_lloc_visita = 'C' and \
                     visi_situacio_visita = 'R'".format(taula)
        for key in u.getAll(sql, "import"):
            if key in self.cataleg:
                visites[self.cataleg[key]] += 1
        return (periode, [row for row in visites.items()])


if __name__ == "__main__":
    Visites()
