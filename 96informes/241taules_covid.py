# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

db = "redics"
tb = "sisap_covid_cat_taules"

file = "taules_covid.txt"

class cataleg_brots(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_file()
        self.export_data()
        self.dona_grants()
        
    
    
    def get_file(self):
        """obtenim arxiu"""
        self.upload = []
        
        for taula, columna, literal_curt, definicio, l1, l2, l3, l4 in u.readCSV(file, sep="@"):
            self.upload.append([taula, columna, literal_curt, definicio, l1, l2, l3, l4])
           
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("taula varchar2(100)", "columna varchar2 (100)", "literal_curt varchar2(500)", "definicio varchar(3000)",
                "llindar1 int", "llindar2 int", "llindar3 int", "llindar4 int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)

if __name__ == '__main__':
    u.printTime("Inici")
    
    cataleg_brots()
    
    u.printTime("Final")