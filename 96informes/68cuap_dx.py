from sisapUtils import getAll, writeCSV, tempFolder
from collections import Counter


ups = tuple([up for up, in getAll('select scs_codi from urg_centres', 'nodrizas')])
des = {c: d for (c, d) in getAll('select ps_cod, ps_des from prstb001', '6734')}

dades = Counter()
sql = "select pr_cod_ps from problemes where pr_dde between '20150101' and '20151231' and pr_up in {}".format(ups)
for ps, in getAll(sql, 'import'):
    dades[ps] += 1

writeCSV(tempFolder + 'diagnostics_cuap.csv', sorted([(cod, des[cod], n) for (cod, n) in dades.items()], key=lambda x: x[2], reverse=True), sep=';')
