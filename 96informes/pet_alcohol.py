from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import string
import random
import operator
import re


nod='nodrizas'
imp='import'
ops = { ">": operator.gt, "=": operator.eq, '>=': operator.ge,'<=':operator.le,'<':operator.lt }
file='codis_oh.txt'
codis_by_table= { "activitats":('CP201','VP201')	,"variables":('VP2000','VP2001','VP2002','ALRIS','VA0301','ALSET','ALHAB',
                                                                  'ALDIA','EP2003','EP2002','EP2004','AUDIT',
                                                                  'EP2001','EP2008')	}

class PetAlcohol(object):
    def __init__(self):
        self.homolog_codis=self.read_file_dict(file)
        self.table_field= {"activitats":("au_cod_ac","au"), "variables":("vu_cod_vs","vu")}
        self.current_date=self.get_date_dextraccio()
        self.tables=self.get_tables_year()

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]

    def read_file_dict(self,file):
        csv=readCSV(file,sep='\t', header=True)
        homolog_codis=defaultdict(lambda: defaultdict(dict))
        for l in csv:
            label,codi,valor,desc=l
        
            if '/' in valor:
                criteris_sexe= valor.split('/')
                for criteri in criteris_sexe:
                    sexe= criteri[-1]
                    oper= "".join(re.findall('[>=<]',criteri))
                    number= int(re.findall(r'\d+',criteri)[0])
                    homolog_codis[codi][sexe][(oper,number)]= label
                    
            else:
                for sexe in ('H','D'):
                    oper='='
                    if len(valor) != 1:
                        if '-' in valor:
                            for number_str,oper in zip(valor.split('-'),[">=","<="]):
                                number=int(number_str)
                                homolog_codis[codi][sexe][(oper,number)]= label
                                
                            
                        else:
                            oper= "".join(re.findall('[>=<]',criteri))
                            number= int(re.findall(r'\d+',valor)[0])
                            homolog_codis[codi][oper][sexe]= (number,label)
                    else:
                        number=int(valor) 
                        homolog_codis[codi][sexe][(oper,number)]= label
        return homolog_codis

    def get_tables_year(self):
        """ Consigue las tablas del any in self.year
            Para ello selecciona una fecha de visita de cada una de las particiones y comprueba que el any de esta fecha sea el any deseado.
            Si es asi, se anyade a una lista.
            Devuelve una lista de tablas.
        """
        tables = defaultdict(list)
        for taula_pare in self.table_field:
            for table in getSubTables(taula_pare):
                dat = [dat for dat, in getAll("select {}u_dat_act from {} limit 1".format(taula_pare[0],table), imp)][0]
                if dat.year in (self.current_date.year, self.current_date.year-1):
                    tables[taula_pare].append(table)
        printTime("Tables")
        return tables


    def get_last_registre(self,dicti,key,label_fecha):
        """ Guarda como valor en un diccionario dicti el registro (tuple label_fecha con los elementos (label, fecha)) que tenga una fecha mas reciente.
            Cuando la llave key no existe en dicti, la crea y la asocia al valor label_fecha.
            Si key ya existe, se comprueba que la fecha en el tuple label_fecha sea mas reciente a la que esta guardada en el diccionari. Si esto es asi, se 
            devuelve el valor nuevo label_fecha. Sino, se devuelve el valor anteriormente guardado.
            Devuelve un tuple (label,ini) donde laber es una str y ini una fecha.
        """
        try: dicti[key]
        except KeyError: 
            return label_fecha
        else: 
            if dicti[key][1] < label_fecha[1]:
                return label_fecha
            else:
                return dicti[key]
        
    def get_id_cod_valor(self,params):
            
        #table,taula_pare=params
        table=params
        taula_pare=table.split('_')[0]
        codi_field,up_field=self.table_field[taula_pare]
        id_cod_valor=defaultdict()

        sql="select id_cip_sec,{0}_val,{0}_dat_act,{2} from {1} where {2} in {3}".format(up_field,table,codi_field,codis_by_table[taula_pare])
        for id,valor,dat,cod in getAll(sql,imp):
            if monthsBetween(dat,self.current_date) < 24:
                val=int(valor) if valor != '' else ''
                id_cod_valor[id]=self.get_last_registre(id_cod_valor,id,(cod,val,dat))
        return id_cod_valor


    def join_act_var(self):
        id_cod_valor_total=defaultdict()
        for taula_pare in self.tables:
            printTime('{} started'.format(taula_pare))
            for table in self.tables[taula_pare]:
                id_cod_valor=self.get_id_cod_valor(table)
                for id in id_cod_valor:
                    id_cod_valor_total[id]=self.get_last_registre(id_cod_valor,id,id_cod_valor[id])
            printTime('{} finished'.format(taula_pare))
        self.id_cod_valor_total=id_cod_valor_total

    def get_rows(self):
        rows=defaultdict(lambda: defaultdict(Counter))
        sql='select id_cip_sec,up,sexe from {}.assignada_tot where edat >= 15 and edat <=65 '.format(nod)
        counter=0
        for id,up,sexe in getAll(sql,nod):
            try: 
                cod, valor,fecha = self.id_cod_valor_total[id]
                if valor =='':
                    rows[up][sexe]['SENSE VALOR']+=1
                    continue

                for (oper,umbral),label in self.homolog_codis[cod][sexe].iteritems():
                    if ops[oper](valor,umbral):
                        rows[up][sexe][label]+=1
                        break
            except:
                rows[up][sexe]['ALTRES']+=1
        return [ (up, sexe, label, n) for up in rows for sexe in rows[up] for (label,n) in rows[up][sexe].iteritems()]
                
def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>35}" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

if __name__ == "__main__":
    
    printTime('inici')
    pet_oh=PetAlcohol()
    
    printTime(pet_oh.tables)
    pet_oh.join_act_var()
    printTime(len(pet_oh.id_cod_valor_total))

    rows=pet_oh.get_rows()
    printTime('Rows')
    name= tempFolder + 'oh_new.txt'
    header=['UP','SEXE','LABEL','N']
    export_txt_file(name,header,rows)