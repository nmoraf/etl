from sisapUtils import printTime, getAll, getSubTables, getOne
from collections import defaultdict
from numpy import percentile, std, mean


printTime()
assignada = {}
for id, sector, up in getAll('select id_cip, codi_sector, up from assignada_tot', 'nodrizas'):
    assignada[id] = {'ics': False, 'sec:{}'.format(sector): False, 'eap:{}'.format(up): False, 'ser': False, 'cmb': False, 'vir': False}

printTime()
for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
    if dat == 2015:
        for id, sector, up, servei in getAll("select id_cip, codi_sector, visi_up, visi_servei_codi_servei from {} where visi_situacio_visita = 'R'".format(table), 'import'):
            if id in assignada:
                assignada[id]['ics'] = True
                if 'sec:{}'.format(sector) in assignada[id]:
                    assignada[id]['sec:{}'.format(sector)] = True
                    if 'eap:{}'.format(up) in assignada[id]:
                        assignada[id]['eap:{}'.format(up)] = True
                        if servei in ('MG', 'PED'):
                            assignada[id]['ser'] = True

printTime()
for id, sector, tipus in getAll('select id_cip, codi_sector, cp_t_act from cmbdap where year(cp_ecap_dvis) = 2015', 'import'):
    if id in assignada:
        if 'sec:{}'.format(sector) in assignada[id]:
            assignada[id]['cmb'] = assignada[id]['cmb'] or tipus != '44'
            assignada[id]['vir'] = True

printTime()
glob = {'ass': 0, 'ics': 0, 'sec': 0, 'eap': 0, 'ser': 0, 'cmb': 0, 'vir': 0}
eaps = {}
for id, data in assignada.items():
    glob['ass'] += 1
    for key in data:
        if data[key]:
            glob[key[:3]] += 1
        if key[:3] == 'eap':
            eap = key[4:]
    if eap not in eaps:
        eaps[eap] = {'ass': 0, 'ics': 0, 'sec': 0, 'eap': 0, 'ser': 0, 'cmb': 0, 'vir': 0}
    eaps[eap]['ass'] += 1
    for key in data:
        if data[key]:
            eaps[eap][key[:3]] += 1

printTime()
result = defaultdict(list)
for eap, data in eaps.items():
    for key in data:
        if key != 'ass':
            result[key].append(float(data[key]) / float(data['ass']))

printTime()
for key, data in result.items():
    print key, 'glob', float(glob[key]) / float(glob['ass']), 'p10', percentile(data, 10), 'p90', percentile(data, 90), 'cv', std(data) / mean(data)
