from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random


nod="nodrizas"
imp="import"
validation=True
limit=""
write=True

aga_codi=("AGA00071","AGA00046", "AGA00047","AGA00070")
years=(2017,2016,2011,2010)
vacuna_dosis= {
    "2016": {
    49:3,
    311:3,
    312:3,
    15:3,
    36:1}, 
    "2010": {
        49:1
    }
}
vacuna_desc= {
    49: "DTPa",
    311:"Polio",
    312:"HIB",
    15:"HB",
    36:"TV"
}

def get_ups_per_amb(aga_codi):
    """Consigue un set de ups que presenten el amb codi determinado y las asocia con la descripcion del ics.
       Devuelve un diccionario de {up:ics_desc}
    """
    sql="select scs_codi,ics_desc from {}.cat_centres where aga in {}".format(nod,aga_codi)
    return { up:ics_desc for up,ics_desc in getAll(sql,nod)}

def get_id_vacune_dosis(agr,dosis,year):
    """Selecciona a los nens que han sido vacunados con una vacuna asociada a un agr y con un numero de dosis determinada. En el caso de ser el any 2010 mira la fecha de la
       ultima dosis, para asegurarse que se dio a los 6 o mas.
       Devuelve un set de ids.
    """
    sql="select id_cip_sec,year(datamax) from {}.ped_vacunes where agrupador={} and dosis>={}".format(nod,agr,dosis)
    if year == "2010":
        return {id for (id,datamax_year) in getAll(sql,nod) if datamax_year >= 2010+6}
    else:
        return {id for (id,datamax_year) in getAll(sql,nod)}

def get_nens_year(years,ups,vacunes_ids):
    """Construye las rows de la output file. De una orden a sql, saca los ninos cuyo any de nacimiento esta dentro de los anys deseados (se saca el id, la up y el any de data_naix). 
       Luego itera sobre estos ids y va construyendo el valor de indicador[up] que va a ser a su vez un diccionario con las siguientes keys:
       la up_desc y los nacidos en cualquiera de los anys especificados. Para cada id, se suma uno a indicador[up][year] donde year es el any que se saca
       de la query a sql. Luego, para sacar los nens con vacunas, se itera sobre vacunes_ids[year] que guarda un diccionario de {agr: set(ids)} en el que para cada agrupador
       esta asociado un set de ids de nens con ese agrupador y el numero de dosis (los cuales dependen del any en el que nacio el nen, por eso se agrupan por ayear) especificadas anteriormente.
       Devuelve una lista de rows (lista con los fields de cada fila)
    """
    indicador=defaultdict(Counter)
    sql="select id_cip_sec,up,year(data_naix) from {}.ped_assignada where year(data_naix) in {}".format(nod,years)
   
    for id,up,year in getAll(sql,nod):
        year=str(year)
        if up in ups:
            indicador[up]["up_desc"]=ups[up]
            indicador[up][year]+=1
            for agr,ids in vacunes_ids[year].iteritems():
                if id in ids:
                    indicador[up][year+vacuna_desc[agr]]+=1
    
    

    rows = [[up,  
             indicador[up]["up_desc"], 
             indicador[up]["2017"], 
             indicador[up]["2016"]] + 
             [indicador[up]["2016"+vacuna_desc[agr]] for agr in vacuna_dosis["2016"]] + 
             [indicador[up]["2011"],indicador[up]["2010"]]+ 
             [indicador[up]["2010"+vacuna_desc[agr]] for agr in vacuna_dosis["2010"]]  
             for up in indicador]
    return rows



if __name__=="__main__":
    printTime("inici")
    ups=get_ups_per_amb(aga_codi)
    printTime("ups")

    vacunes_ids=defaultdict(dict)
    #Crear un diccionario en el que tenga agrupados por any los agrupadores deseados con los ids que tienen esta vacuna y el numero de dosis determinada en vacuna_dosis
    #Agruparlo por any aqui no mira el any de nacimiento todavia, solo es para tener agr:dosis asociada a un any
    for year, agr_dosis in vacuna_dosis.iteritems():
        vacunes_ids[year]= {agr: get_id_vacune_dosis(agr,dosis,year) for agr,dosis in agr_dosis.iteritems() }
    printTime("Ids con vacunas por any y agrupador")

    rows=get_nens_year(years,ups,vacunes_ids)
    printTime("rows")
    
    if write:
        header_vacunas=list("NACIDOS {} {}".format(year,vacuna_desc[agr]) for year in vacuna_dosis for agr in vacuna_dosis[year])
        row_format ="{:>10}"+"{:>40}"+"{:>25}" * len(rows[0][2:])+"\n"
        header=["UP","UP_DESC","NACIDOS 2017", "NACIDOS 2016"]+ header_vacunas[:-1]+ ["NACIDOS 2011","NACIDOS 2010", header_vacunas[-1]]
        with open("vacunes_bcn_informe_assig.txt","wb") as out_file:
            out_file.write(row_format.format(*header))
            for row in rows:
                out_file.write(row_format.format(*row))