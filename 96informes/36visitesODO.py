from sisapUtils import getAll, getSubTables, getOne, writeCSV, tempFolder
from collections import defaultdict, Counter
from numpy import *


dies = 61.3
db = 'odn'

visites = {(dni[:8], up): 0 for dni, up in getAll("select distinct ide_dni, up from cat_professionals where tipus in ('T', 'O')", 'import')}
tipus = {(dni[:8], up): tipus for dni, up, tipus in getAll("select distinct ide_dni, up, tipus from cat_professionals where tipus in ('T', 'O')", 'import')}
nom = {(dni[:8], up): nom for dni, up, nom in getAll("select ide_dni, up, max(concat(cognom1, ' ', cognom2, ' ', nom)) from cat_professionals where tipus in ('T', 'O') group by ide_dni, up", 'import')}
ups = {cod: des for cod, des in getAll('select scs_codi, ics_desc from cat_centres', 'nodrizas')}
for table in getSubTables('visites'):
    try:
        dat, = getOne("select date_format(visi_data_visita, '%Y%m') from {} limit 1".format(table), 'import')
    except TypeError:
        continue
    if dat in ('201510', '201511', '201512'):
        for dni, up in getAll('select visi_dni_prov_resp, visi_up from {}'.format(table), 'import'):
            try:
                visites[(dni[:8], up)] += 1
            except KeyError:
                continue
writeCSV(tempFolder + 'visites_per_professional.csv', [(tipus[(dni, up)], up, ups[up], dni, nom[(dni, up)], n) for (dni, up), n in visites.items()], sep=';')


def get_percentil(valors, perc):
    percentils = round(percentile(valors, perc), 2)
    return percentils


def get_indicadors():
    indicadors = {}
    sql = 'select distinct indicador from exp_ecap_uba'
    for indic, in getAll(sql, db):
        indicadors[indic] = True
    return indicadors


def get_Odontos(visites):
    odontolegs = defaultdict(list)
    n_odo = Counter()
    for (dni, up), recompte in visites.items():
        visites_dia = round(recompte/dies, 0)
        if visites_dia > 9:
            odontolegs[up].append(dni)
            n_odo[up] += 1
    return odontolegs, n_odo


def get_eqa(odontolegs, n_odo, indic):
    resul_eqa = {}
    sql = "select up,  detectats from exp_ecap_uba where indicador = '{}' and detectats > 0".format(indic)
    for up, detectats in getAll(sql, db):
        odns = float(n_odo[up])
        dnis = odontolegs[up]
        try:
            detect_per_odo = round(detectats/odns, 0)
        except ZeroDivisionError:
            continue
        for dni in dnis:
            if (dni, indic) in resul_eqa:
                resul_eqa[(dni, indic)] += detect_per_odo
            else:
                resul_eqa[(dni, indic)] = detect_per_odo
    return resul_eqa


def do_simulation(resul_eqa):
    valors = []
    for (dni, indicador), val in resul_eqa.items():
        valors.append(val)
    p80 = get_percentil(valors, 80)
    p50 = get_percentil(valors, 50)
    p20 = get_percentil(valors, 20)
    mitjana = mean(valors, 0)
    desv_std = std(valors, 0)
    minim = min(valors)
    maxim = max(valors)
    return p80, p50, p20, mitjana, desv_std, minim, maxim

odontolegs, n_odo = get_Odontos(visites)
indicadors = get_indicadors()
for indicador in indicadors:
    resul_eqa = get_eqa(odontolegs, n_odo, indicador)
    p80, p50, p20, mitjana, desv_std, minim, maxim = do_simulation(resul_eqa)
    print indicador, p80, p50, p20, mitjana, desv_std, minim, maxim
