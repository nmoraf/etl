# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

file = u.tempFolder + "actualitzacio.txt"

class Coronavirus_ind(object):
    """."""

    def __init__(self):
        """."""
        self.get_comarca()
        self.get_centres()
        self.get_acumulats()
        self.export_ind()
        
    def get_comarca(self):
        """agafem comarca de explotacio"""
        self.comarca = {}
        sql = "select distinct cent_codi_abs,cent_codi_comarca, coma_nom_comarca from pritb125, rittb012 where cent_codi_comarca=coma_codi_comarca"
        for abs, comarca, nom in u.getAll(sql, '6735'):
            abs = int(abs)
            self.comarca[(abs)] = {'nom': nom}
    
    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, abs from cat_centres"
        for up, abs in u.getAll(sql, 'nodrizas'):
            abs = int(abs)
            try: 
                com = self.comarca[(abs)]['nom']
            except KeyError:
                print abs
                continue
            self.centres[up] = {'abs': abs, 'comarca': com}
             
    def get_acumulats(self):
        """Acumulats setmanals"""
        self.acumulats = c.Counter()
        sql = "select up, edat, sexe, cas_data, estat from sisap_covid_pac_master where estat in ('Possible', 'Confirmat')"
        for up, edat, sexe, dat, estat in u.getAll(sql, 'redics'):
            abs = self.centres[up]['abs'] if up in self.centres else None
            comarca = self.centres[up]['comarca'] if up in self.centres else None
            self.acumulats[(dat, estat, comarca,  edat, sexe)] += 1

    def export_ind(self):
        """fem export dels indicadors"""
        upload = []
        for (dat, estat, comarca,  edat, sexe), n in self.acumulats.items():
            upload.append([dat, estat, comarca,  edat, sexe, n])
        u.writeCSV(file, upload, sep=';')
                                    
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    Coronavirus_ind()
    
    u.printTime("Final")