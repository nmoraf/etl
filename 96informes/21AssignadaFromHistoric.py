from sisapUtils import *
from collections import Counter

imp= 'import'
nod= 'nodrizas'
file= tempFolder + 'poblacio.txt'
table= 'assignada_tot'

query = 'select year(data_ext) from dextraccio'
for d, in getAll(query, nod):
    anyCalcul = d

mst= {
    'sql': "select id_cip_sec,id_cip{0} from assignadahistorica where dataany={1}"
    ,'create': 'create table {} (id_cip_sec int,id_cip int{},primary key(id_cip_sec))'
    }

pob= {
    'sql': mst['sql'].format(",codi_sector, up,uab,up,uabinf",anyCalcul)
    ,'create': ',codi_sector varchar(5),up varchar(5),uba varchar(5),upinf varchar(5),ubainf varchar(5)'
    }
  

   
components= {
     '0pobs': {
        'sql': "select id_cip_sec,usua_uab_servei_centre,usua_uab_servei_centre_classe,date_format(usua_data_naixement,'%Y%m%d'),(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe,usua_nacionalitat,cart_tipus,usua_relacio from assignada,nodrizas.dextraccio where (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) >=0"
        ,'db': imp
        ,'creua': 'id'
        ,'default': ('','','1900-01-01','115','','','','',)
        ,'create': ',centre_codi varchar(9),centre_classe varchar(2),data_naix date,edat int,sexe varchar(1),nacionalitat varchar(3),nivell_cobertura varchar(2),relacio varchar(1)'
        }
    ,'1origen': {
        'sql': "select distinct lloc_codi_up up,lloc_uab_codi_uab_assignat__a uba,lloc_up_rca rca from cat_pritb025_def where lloc_codi_up in (select scs_codi from nodrizas.cat_centres where ics_codi like 'BN%') and lloc_codi_up<>lloc_up_rca"
        ,'db': imp
        ,'creua': 'up'
        ,'default': ('',)
        ,'create': ',upOrigen varchar(5)'
        }
    ,'2seccio': {
        'sql': "select id_cip_sec,sector_censal from crg"
        ,'db': imp
        ,'creua': 'id'
        ,'default': ('',)
        ,'create':',seccio_censal varchar(10)'
        }
    ,'3visites': {
        'sql': "select id_cip_sec,date_format(last_visit,'%Y%m%d') from eqa_last_visit,dextraccio"
        ,'db': nod
        ,'creua': 'id'
        ,'default': ('',)
        ,'create':',last_visit date'
        }
     ,'4ates': {
        'sql': "select id_cip_sec,ates from assignadahistorica where dataany={}".format(anyCalcul)
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,)
        ,'create':',ates int'
        }    
    ,'5institucionalizats': {
        'sql': "select id_cip_sec,gu_up_residencia,1 from institucionalitzats a inner join cat_ppftb011_def b on a.ug_codi_grup=b.gu_codi_grup and a.codi_sector=b.codi_sector"
        ,'db': imp
        ,'creua': 'id'
        ,'default': ('',0)
        ,'create': ',up_residencia varchar(13),institucionalitzat int'
        }
    ,'6institucionalitzats_ps': {
        'sql': "select id_cip_sec,1 from problemes,nodrizas.dextraccio where pr_cod_ps='Z59.3' and pr_dde<=data_ext and (pr_dba=0 or pr_dba>data_ext) and (pr_data_baixa=0 or pr_data_baixa>data_ext) and pr_hist=1"
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,)
        ,'create': ',institucionalitzat_ps int'
        }
    ,'7maca': {
        'sql': "select id_cip_sec,1 from estats,nodrizas.dextraccio where es_cod='ER0002' and es_dde<=data_ext \
            union select id_cip_sec,1 from problemes,nodrizas.dextraccio where pr_cod_ps='Z51.5' and pr_dde<=data_ext and (pr_dba=0 or pr_dba>data_ext) and (pr_data_baixa=0 or pr_data_baixa>data_ext) and pr_hist=1"
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,)
        ,'create': ',maca int'
        }
    ,'8pcc': {
        'sql': "select id_cip_sec,1 from estats,nodrizas.dextraccio where es_cod='ER0001' and es_dde<=data_ext"
        ,'db': imp
        ,'creua': 'id'
        ,'default': (0,)
        ,'create': ',pcc int'
        }    
    }

post= ("update {} set upOrigen=if(upOrigen='',up,upOrigen),pcc=if(maca=1,0,pcc)".format(table),)
    
def addComponent(sql,db,creua):

    component= {}
    for row in getAll(sql,db):
        if creua== 'id':
            id= row[0]
            data= row[1:]
            if ('s',id) not in pac:
                continue
        elif creua== 'up':
            id= (row[0],row[1])
            data= row[2:]
        component[id]= data
    return component

def checkComponent(id,up,uba,component):
    
    if components[component]['creua']== 'up':
        id= (up,uba)
    try:
        resul= components[component]['data'][id]
    except KeyError:
        resul= components[component]['default']
    return resul
        
printTime('inici pob')
pac= Counter()
sql= mst['sql'].format('', anyCalcul)
for cipsec,cip in getAll(sql,imp):
    pac['s',cipsec]+= 1
    pac['c',cip]+= 1
printTime('fi pob')
    
for component in sorted(components.keys()):
    printTime('inici',component)
    components[component]['data']= addComponent(components[component]['sql'],components[component]['db'],components[component]['creua'])
    printTime('fi',component)
    
printTime('inici join')
with openCSV(file) as c:
    for row in getAll(pob['sql'],imp):
        cipsec,cip,up,uba= row[:4]
        if pac['s',cipsec]== 1 and pac['c',cip]== 1:
            moreData= ()
            for component in sorted(components.keys()):
                moreData= moreData + checkComponent(cipsec,up,uba,component)
            c.writerow(row + moreData)
printTime('fi join')

printTime('inici up')
execute('drop table if exists {}'.format(table),nod)
create= mst['create'].format(table,pob['create'] + ''.join([value['create'] for key,value in sorted(components.items())]))
execute(create,nod)
loadData(file,table,nod)
printTime('fi up')

printTime('inici post')
for sql in post:
    execute(sql,nod)
printTime('fi post')

