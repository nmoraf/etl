#necessitem que a test hi hagi mst_corbes
from sisapUtils import *
from collections import defaultdict, Counter


patologia = '55'

nod = 'nodrizas'

def get_centres():
    centres = {}
    sql = "select scs_codi,ics_codi, ics_desc, medea from cat_centres where ep='0208'"
    for up,br, ics_desc, medea in getAll(sql,nod):
        centres[(up)] = medea
    return centres

def get_descripcions():
    descrip = {}
    sql = "select agrupador, agrupador_desc from eqa_criteris where taula='problemes'"
    for agr, desc in getAll(sql, nod):
        descrip[agr] = {'desc': desc}
    return descrip

def get_patologia():
    pato = {}
    sql = "select id_cip_sec, dde from eqa_problemes where ps in ('{0}')".format(patologia)
    for id, dde in getAll(sql, nod):
        pato[id] = True
    return pato
    
    
def get_pob(pato, centres):
    prevalenca = Counter()
    sql = "select id_cip_sec, up, sexe from assignada_tot where id_cip_sec >0 and edat>14"
    for id, up, sexe in getAll(sql, nod):
        try:
            medea = centres[(up)]
        except KeyError:
            continue
        prevalenca[medea, sexe, 'POB'] += 1
        if id in pato:
            prevalenca[medea, sexe, 'PATOLOGIA'] += 1
  
    return prevalenca

def get_comorbiditat(pato, descrip):
    comor = Counter()
    sql = 'select id_cip_sec, ps from eqa_problemes'
    for id, ps in getAll(sql, nod):
        try:
            pato[id]
            try:
                desc = descrip[ps]['desc']
            except KeyError:
                desc = 'Altres'
            comor[desc] += 1
        except KeyError:
            continue
    return comor
    
printTime()

centres = get_centres()
pato = get_patologia()
prevalenca = get_pob(pato, centres)
descrip = get_descripcions()
comor = get_comorbiditat(pato, descrip)

print prevalenca

for (ps), recompte in comor.items():
    print ps,';', recompte


printTime()