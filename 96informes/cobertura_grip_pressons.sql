/* interns amb estan�a minima de 5 dies a un modul, durant la campanya vacunal de grip temporada 2017-2018*/
/* desde el segundo lunes de Octubre hasta 16 semanas despues: (ultimo lunes de enero)*/

set @campaign_offset_year = if(extract(month from now()) > 10, extract(year from now()) ,extract(year from now())-1);
-- set @campaign_offset_year= '2017';
set @campaign_offset = concat(@campaign_offset_year, '-10-01');
set @campaign_start = adddate(@campaign_offset, MOD(9-DAYOFWEEK(@campaign_offset),14));	-- 2nd monday of October, of this year if November or December, else: last year 
set @campaign_end = date_add(@campaign_start, interval 16 week);

select @campaign_offset_year;
select @campaign_offset;
select @campaign_start;
select @campaign_end;


drop table if exists test.grip_jail;

create table if not exists test.grip_jail (
	id_cip_sec int(11)
	,P_G_AR int(1) not null default 0
	,P_G_NE int(1) not null default 0
	,P_G_AD int(1) not null default 0
	,P_G_60	int(1) not null default 0
	, primary key (id_cip_sec)
)
;

insert into
	test.grip_jail(id_cip_sec)
select
	distinct id_cip_sec
from
	import_jail.moviments
where 																			-- (A) seleccionamos los que su Estancia_Durante_Campa�a sea superior a 4 dias
	datediff(if(huab_data_final = 0, now(),huab_data_final), huab_data_ass) + 						-- EDC calcul: A la estancia total, le restamos...
	if(datediff(@campaign_end, @campaign_start) 												
		- datediff(if(huab_data_final is null, now(),huab_data_final),@campaign_start ) >0,0					--  [identificats porque esta resta sea negativa]
		,datediff(@campaign_end, @campaign_start)												  
		- datediff(if(huab_data_final is null, now(),huab_data_final),@campaign_start )				-- ... los dias de estancia previa al inicio_de_campa�a. 
		) +
	if(datediff(@campaign_end, @campaign_start)														-- y tambien le restaremos...
		- datediff(@campaign_end,  huab_data_ass) >0,0															--  [identificats porque esta resta sea negativa]    
		, datediff(@campaign_end, @campaign_start) 						
		- datediff(@campaign_end,  huab_data_ass)													-- ... los dias de estancia posterior al fin_de_campa�a. 
		) >=5																	
;


drop table if exists test.grip_jail_vacunats;
create table if not exists test.grip_jail_vacunats as
	select id_cip_sec, va_u_cod
	from import_jail.vacunes
	where va_u_cod in ('P-G-AR', 'P-G-NE', 'P-G-AD', 'P-G-60')
	and va_u_data_vac between @campaign_start and @campaign_end
;

alter table test.grip_jail_vacunats add index (id_cip_sec, va_u_cod)
;

update	test.grip_jail p set P_G_AR =1 	where exists
	(select id_cip_sec from test.grip_jail_vacunats v where p.id_cip_sec = v.id_cip_sec and va_u_cod =	'P-G-AR')
;

update	test.grip_jail p set P_G_NE =1 	where exists
	(select id_cip_sec from test.grip_jail_vacunats v where p.id_cip_sec = v.id_cip_sec and va_u_cod =	'P-G-NE')
;

update	test.grip_jail p set P_G_AD =1 	where exists
	(select id_cip_sec from test.grip_jail_vacunats v where p.id_cip_sec = v.id_cip_sec and va_u_cod =	'P-G-AD')
;

update	test.grip_jail p set P_G_60 =1 	where exists
	(select id_cip_sec from test.grip_jail_vacunats v where p.id_cip_sec = v.id_cip_sec and va_u_cod =	'P-G-60')
;

/* sortida */
select
	count(*) as poblacio
	,sum(P_G_AR + P_G_NE + P_G_AD + P_G_60) as vacunats_grip
	,avg(P_G_AR + P_G_NE + P_G_AD + P_G_60)	as cobertura
	,sum(P_G_AR)							as Pauta_arbitraria
	,sum(P_G_AD)							as Pauta_adults_factors_risc_9_a_60a
	,sum(P_G_60)							as Pauta_adults_majors_60a
	,sum(P_G_NE) 							as Pauta_nens_6m_a_9a
from
	test.grip_jail
;

drop table if exists test.grip_jail_vacunats;