# coding: latin1

"""
Basat en informe 209visites_metges_cat.py
"""

import collections as c

import sisapUtils as u


DEBUG = False

ESPECIALITATS = {"30999": "I", "10117": "M", "10999": "M", "10888": "P",
                 "10116": "M", "10777": "O", "05999": "T", "10106": "O"}
PERIODE = (2018, 2019)
TABLE = "sisap_visites_categ_tipus"
DATABASE = "redics"
USERS = "PREDUMMP"


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.get_cataleg()
        self.get_centres()
        self.get_particions()
        self.get_visites()
        self.get_logins()
        self.get_virtuals()
        self.export()

    def get_cataleg(self):
        """."""
        self.cataleg = {}
        sql = "select codi_sector, tv_tipus, tv_cita, tv_homol, tv_lloc \
               from cat_vistb206"
        for sec, tip, cit, hom, lloc in u.getAll(sql, "import"):
            if tip == "9T" or hom == "TLF":
                visita = 43
            elif tip == "9E":
                visita = 442
            elif lloc == "D":
                visita = 42
            else:
                visita = 41
            self.cataleg[(sec, tip, cit)] = visita

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = set([up for up, in u.getAll(sql, "nodrizas")])

    def get_particions(self):
        """."""
        self.particions = []
        sql = "select visi_data_visita from {} limit 1"
        for table in u.getSubTables("visites"):
            dat, = u.getOne(sql.format(table), "import")
            y = dat.year
            m = dat.month
            if y in range(PERIODE[0], PERIODE[1] + 1):
                self.particions.append((table, (y, m)))

    def get_visites(self):
        """."""
        self.visites = c.Counter()
        if DEBUG:
            visites = [self._get_visites_worker(self.particions[0])]
        else:
            visites = u.multiprocess(self._get_visites_worker, self.particions, 12)  # noqa
        for worker in visites:
            for row in worker:
                self.visites[row[:-1]] += row[-1]

    def _get_visites_worker(self, param):
        """."""
        table, key = param
        visites = c.Counter()
        sql = "select visi_up, s_espe_codi_especialitat, codi_sector, \
                      visi_tipus_visita, visi_tipus_citacio, visi_etiqueta \
               from {} \
               where visi_situacio_visita = 'R' and \
                     s_espe_codi_especialitat in {}".format(table, tuple(ESPECIALITATS.keys()))  # noqa
        for row in u.getAll(sql, "import"):
            if row[0] in self.centres:
                tipus = self.cataleg.get(row[2:5], 41)
                if tipus == 442 and row[5] == "ECTA":
                    tipus = 441
                visites[(row[0], ESPECIALITATS[row[1]], tipus)] += 1
        return [key + k + (v,) for (k, v) in visites.items()]

    def get_logins(self):
        """."""
        self.logins = c.defaultdict(dict)
        sql = "select codi_sector, ide_usuari, ide_categ_prof_c \
               from cat_pritb992 \
               where ide_categ_prof_c in {}".format(tuple(ESPECIALITATS.keys()))  # noqa
        for sector, usuari, espe in u.getAll(sql, "import"):
            self.logins[sector][usuari] = ESPECIALITATS[espe]

    def get_virtuals(self):
        """."""
        if DEBUG:
            virtuals = [self._get_virtuals_worker("6734")]
        else:
            virtuals = u.multiprocess(self._get_virtuals_worker, u.sectors, 12)
        for worker in virtuals:
            for row in worker:
                self.visites[row[:-1]] += row[-1]

    def _get_virtuals_worker(self, sector):
        """."""
        virtuals = c.Counter()
        logins = self.logins[sector]
        sql = "select vvi_codi_up, vvi_usu_resolucio, vvi_data_resolucio, \
                      if(vvi_tipus = 'ECONSULTA', 451, 452) \
               from vvirtuals_s{} \
               where vvi_situacio = 'R' and \
                     vvi_data_resolucio between {}0101 and {}1231".format(sector, PERIODE[0], PERIODE[1])  # noqa
        for up, usu, dat, tip in u.getAll(sql, "import"):
            if up in self.centres and usu in logins:
                virtuals[(dat.year, dat.month, up, logins[usu], tip)] += 1
        return [k + (v,) for (k, v) in virtuals.items()]

    def export(self):
        """."""
        result = sorted([k + (v,) for (k, v) in self.visites.items()])
        cols = "(exercici int, mes int, up varchar2(5), espe varchar2(1), \
                 tipus varchar2(3), numero int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(result, TABLE, DATABASE)
        u.grantSelect(TABLE, USERS, DATABASE)


if __name__ == "__main__":
    Visites()
