# coding: utf8

"""
Passem les dades necessàries per EAP per calcular el model assignació adults
"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter

upload = []
sql = "select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc from cat_centres where ep='0208'"
for up, br, ambit, sap, desc in u.getAll(sql, "nodrizas"):
    upload.append([up, br, ambit, sap, desc])
upload2 = []
sql = "select amb_desc, sap_Desc,up,ics_codi, ics_desc, upOrigen,count(*) from nodrizas.assignada_tot a inner join nodrizas.cat_centres b on up=scs_codi where up<>uporigen and b.ep='0208' and ics_codi like ('BN%') and edat <12 group by up, uporigen"
for a, b, c, d, e,f,g in u.getAll(sql, "nodrizas"):
    upload2.append([ a, b, c, d, e,f,g])
    
u.writeCSV(u.tempFolder + 'visites.txt', upload)
u.writeCSV(u.tempFolder + 'linies.txt', upload2)