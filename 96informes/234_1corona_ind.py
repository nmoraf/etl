# coding: latin1

"""
.
"""


import collections as c
from datetime import timedelta

import sisapUtils as u

sex = ['H', 'D']
grups = [0, 1, 2]
tipus = ['casos', 'pneumo', 'rx', 'casosP', 'pneumoP', 'rxP', 'casosC',
         'pneumoC', 'pneumoR', 'rxC', 'xmlseg', 'xmldx', 'rxN', 'pneumoN']
taxes = {
  'COVC001': {
    'num': 'casos',
    'den': 'pob',
    'lit': 'Taxa bruta de casos totals de COVID-19 per 100.000 habitants',
    'multi': 100000,
    'acumula': False,
    'remove': True,
    'grup': 'Casos',
    'lit_curt': 'Taxa bruta setmanal de COVID-19 totals'},
  'COVC001A': {
    'num': 'casosP',
    'den': 'pob',
    'lit': 'Taxa bruta de casos possibles de COVID-19 per 100.000 habitants',
    'multi': 100000,
    'acumula': False,
    'remove': True,
    'grup': 'Casos',
    'lit_curt': 'Taxa bruta setmanal de COVID-19 possibles'},
  'COVC001B': {
    'num': 'casosC',
    'den': 'pob',
    'lit': 'Taxa bruta de casos confirmats de COVID-19 per 100.000 habitants',
    'multi': 100000,
    'acumula': False,
    'remove': True,
    'grup': 'Casos',
    'lit_curt': 'Taxa bruta setmanal de COVID-19 confirmats'},
  'COVC002': {
    'num': 'casos',
    'den': 'pob',
    'lit': 'Taxa bruta de casos totals acumulats de COVID-19 '
           + 'per 100.000 habitants',
    'multi': 100000,
    'acumula': True,
    'remove': True,
    'grup': 'Casos',
    'lit_curt': 'Taxa bruta acumulada de COVID-19 totals'},
  'COVC002A': {
    'num': 'casosP',
    'den': 'pob',
    'lit': 'Taxa bruta de casos possibles acumulats de COVID-19 '
           + 'per 100.000 habitants',
    'multi': 100000,
    'acumula': True,
    'remove': True,
    'grup': 'Casos',
    'lit_curt': 'Taxa bruta acumulada de COVID-19 possibles'},
  'COVC002B': {
    'num': 'casosC',
    'den': 'pob',
    'lit': 'Taxa bruta de casos confirmats acumulats de COVID-19 '
           + 'per 100.000 habitants',
    'multi': 100000,
    'acumula': True,
    'remove': True,
    'grup': 'Casos',
    'lit_curt': 'Taxa bruta acumulada de COVID-19 confirmats'},
  'COVR001': {
    'num': 'rxN',
    'den': 'pob',
    'lit': 'Taxa bruta de radiografies en pacients COVID-19 '
           + 'per 100.000 habitants',
    'multi': 100000,
    'acumula': False,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Taxa bruta poblacional de radiografies en COVID-19'},
  'COVR002': {
    'num': 'rx',
    'den': 'casos',
    'lit': 'Percentatge de pacients COVID-19 totals amb radiografia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 amb radiografia demanada'},
  'COVR002A': {
    'num': 'rxP',
    'den': 'casosP',
    'lit': 'Percentatge de pacients COVID-19 possibles amb radiografia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 possibles amb radiografia demanada'},
  'COVR002B': {
    'num': 'rxC',
    'den': 'casosC',
    'lit': 'Percentatge de pacients COVID-19 confirmats amb radiografia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 confirmats amb radiografia demanada'},
  'COVP001': {
    'num': 'pneumo',
    'den': 'pob',
    'lit': 'Taxa bruta de pneumonies en pacients COVID-19 '
           + 'per 100.000 habitants',
    'multi': 100000,
    'acumula': False,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Taxa bruta de pneumonies en COVID-19'},
  'COVP002': {
    'num': 'pneumo',
    'den': 'casos',
    'lit': 'Percentatge de pacients COVID-19 totals amb pneumonia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 amb pneumonia'},
  'COVP002A': {
    'num': 'pneumoP',
    'den': 'casosP',
    'lit': 'Percentatge de pacients COVID-19 possibles amb pneumonia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 possibles amb pneumonia'},
  'COVP002B': {
    'num': 'pneumoC',
    'den': 'casosC',
    'lit': 'Percentatge de pacients COVID-19 confirmats amb pneumonia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 confirmats amb pneumonia'},
  'COVP003': {
    'num': 'pneumoR',
    'den': 'rx',
    'lit': 'Percentatge de pacients amb radiografies demanada amb pneumonia',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'Pneumonies i Radiologia',
    'lit_curt': 'Percentatge de COVID-19 amb radiografia amb pneumonia'},
  'COVX001': {
    'num': 'xmldx',
    'den': 'casos',
    'lit': 'Percentatge de pacients amb COVID-19 '
           + 'amb xml de diagnostic realitzat',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'XML i visites',
    'lit_curt': 'Percentatge de COVID-19 amb xml de diagnòstic fet'},
  'COVX002': {
    'num': 'xmlseg',
    'den': 'casos',
    'lit': 'Percentatge de pacients amb COVID-19 '
           + 'amb xml de seguiment realitzat',
    'multi': 100,
    'acumula': True,
    'remove': True,
    'grup': 'XML i visites',
    'lit_curt': 'Percentatge de COVID-19 amb xml de seguiment fet'},
}

ind_visites = {
  'COVV001': {
    'lit': 'Percentatge de pacients amb COVID-19 actiu '
           + 'visitats els últims 7 dies',
    'multi': 100,
    'grup': 'XML i visites',
    'lit_curt': 'Percentatge de COVID-19 actius visitats (7 dies)'},
  'COVV002': {
    'lit': 'Mitjana de visites dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites en COVID-19 actius (7 dies)'},
  'COVV0029C': {
    'lit': 'Mitjana de visites 9C dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites 9C en COVID-19 actius (7 dies)'},
  'COVV0029D': {
    'lit': 'Mitjana de visites 9D dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites 9D en COVID-19 actius (7 dies)'},
  'COVV0029R': {
    'lit': 'Mitjana de visites 9R dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites 9R en COVID-19 actius (7 dies)'},
  'COVV0029T': {
    'lit': 'Mitjana de visites 9T dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites 9T en COVID-19 actius (7 dies)'},
  'COVV0029E': {
    'lit': 'Mitjana de visites 9E dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites 9E en COVID-19 actius (7 dies)'},
  'COVV0029Ec': {
    'lit': 'Mitjana de visites 9Ec dels ultims 7 dies '
           + 'en pacients amb COVID-19 actiu',
    'multi': 1,
    'grup': 'XML i visites',
    'lit_curt': 'Mitjana de visites 9Ec en COVID-19 actius (7 dies)'},
}

tb_ind = "sisap_covid_ind_indicadors"
tb_cat = "sisap_covid_ind_cataleg"
tb_back = "sisap_covid_ind_back"
db = 'redics'


class Coronavirus_ind(object):
    """."""

    def __init__(self):
        """."""
        self.indicadors = []
        self.get_pob()
        self.get_setmanals()
        self.get_indicadors()
        self.get_visites()
        self.export_ind()
        self.get_cataleg()
        self.dona_grants()

    def get_pob(self):
        """ Agafem les poblacions de la taula sisap_coronavirus_poblacio"""
        self.pob, self.cataleg = c.Counter(), {}
        sql = "SELECT up, grup, sexe, recompte FROM sisap_covid_agr_poblacio"
        for up, edat, sexe, n in u.getAll(sql, db):
            self.pob[up, edat, sexe] += n
            self.cataleg[up] = True

    def get_setmanals(self):
        """Acumulats setmanals"""
        sql = "SELECT min(cas_data), max(cas_data) FROM sisap_covid_pac_master"
        for mind, maxd in u.getAll(sql, 'redics'):
            self.ini = mind
            self.fi = maxd + timedelta(days=1)
            self.calcul_avui = maxd

        ind_1 = c.Counter()
        self.setmanals = c.Counter()
        self.acumulats = c.Counter()
        self.ind_vis = c.Counter()
        self.pac_visites = {}
        sql = """
            SELECT
                hash, up, grup, sexe, cas_data,
                pneumonia, rx, xml_dx, xml_seg,
                estat, dx_sit, vis_data, vis_set
            FROM sisap_covid_pac_master
            WHERE estat in ('Possible', 'Confirmat')
        """
        for id, up, edat, sexe, dat, pneumo, rx, xml_dx, xml_seg, estat, sit, vis_data, nvis in u.getAll(sql, db):  # noqa
            ind_1[(dat, up, edat, sexe,  'casos')] += 1
            if estat == "Possible":
                ind_1[(dat, up, edat, sexe,   'casosP')] += 1
            if estat == "Confirmat":
                ind_1[(dat, up, edat, sexe,  'casosC')] += 1
            if pneumo is not None:
                pneumo = dat if dat > pneumo else pneumo
                ind_1[(pneumo, up, edat, sexe, 'pneumo')] += 1
                if estat == "Possible":
                    ind_1[(pneumo, up, edat, sexe,  'pneumoP')] += 1
                if estat == "Confirmat":
                    ind_1[(pneumo, up, edat, sexe,  'pneumoC')] += 1
            if rx is not None:
                rx = dat if dat > rx else rx
                ind_1[(rx, up, edat, sexe, 'rx')] += 1
                if pneumo is not None:
                    pneumo = rx if rx > pneumo else pneumo
                    ind_1[(pneumo, up, edat, sexe, 'pneumoR')] += 1
                if estat == "Possible":
                    ind_1[(rx, up, edat, sexe,  'rxP')] += 1
                if estat == "Confirmat":
                    ind_1[(rx, up, edat, sexe,  'rxC')] += 1
            if xml_dx is not None:
                ind_1[(dat, up, edat, sexe, 'xmldx')] += 1
            if xml_seg is not None:
                ind_1[(dat, up, edat, sexe, 'xmlseg')] += 1
            if sit == 'Obert':
                self.ind_vis[(up, edat, sexe, 'den')] += 1
                self.pac_visites[(id, up, edat, sexe)] = True
                if vis_data is not None and nvis > 0:
                    self.ind_vis[(up, edat, sexe, 'num')] += 1
                    self.ind_vis[(up, edat, sexe, 'numN')] += nvis

        sql = """
            SELECT
                up, data, grup, sexe, pneumonia, estat
            FROM
                sisap_covid_pac_master a,
                preduffa.sisap_covid_pac_rx b
            WHERE
                a.hash = b.hash
                and estat in ('Possible', 'Confirmat')
        """
        for up, dat, edat, sexe, _pneumonia, estat in u.getAll(sql, 'redics'):
            ind_1[(dat, up, edat, sexe, 'rxN')] += 1
            if pneumo is not None:
                ind_1[(pneumo, up, edat, sexe, 'pneumoN')] += 1

        for dat in u.dateRange(self.ini, self.fi):
            dat1 = dat - timedelta(days=6)
            for tip in tipus:
                for up in self.cataleg:
                    for sexe in sex:
                        for edat in grups:
                            n = ind_1[(dat, up, edat, sexe, tip)]             \
                                if (dat, up, edat, sexe, tip) in ind_1 else 0
                            self.setmanals[(dat, up, edat, sexe, tip)] += n
                            self.acumulats[(dat, up, edat, sexe, tip)] += n
                            for single_date in u.dateRange(dat1, dat):
                                n2 = ind_1[(single_date, up, edat, sexe, tip)]\
                                    if (single_date, up, edat, sexe, tip)     \
                                    in ind_1 else 0
                                self.setmanals[(dat, up, edat,
                                                sexe, tip)] += n2
                            for single_date in u.dateRange(self.ini, dat):
                                n3 = ind_1[(single_date, up, edat, sexe, tip)]\
                                    if (single_date, up, edat, sexe,  tip)    \
                                    in ind_1 else 0
                                self.acumulats[(dat, up, edat,
                                                sexe,  tip)] += n3

    def get_indicadors(self):
        """Calculem indicadors diaris"""
        for single_date in u.dateRange(self.ini, self.fi):
            for indica in taxes:
                for up in self.cataleg:
                    for sexe in sex:
                        for edat in grups:
                            acumula = taxes[indica]['acumula']
                            num_tip = taxes[indica]['num']
                            den_tip = taxes[indica]['den']
                            if acumula:
                                try:
                                    num = self.acumulats[(single_date, up,
                                                          edat, sexe, num_tip)]
                                except KeyError:
                                    num = 0
                            else:
                                try:
                                    num = self.setmanals[(single_date, up,
                                                          edat, sexe, num_tip)]
                                except KeyError:
                                    num = 0
                            if den_tip == 'pob':
                                den = self.pob[up, edat, sexe, ]
                            else:
                                if acumula:
                                    try:
                                        den = self.acumulats[(single_date, up,
                                                              edat, sexe,
                                                              den_tip)]
                                    except KeyError:
                                        den = 0
                                else:
                                    try:
                                        den = self.setmanals[(single_date, up,
                                                              edat, sexe,
                                                              den_tip)]
                                    except KeyError:
                                        den = 0
                            self.indicadors.append([single_date, up, edat,
                                                    sexe, indica, num, den])

    def get_visites(self):
        """Indicadors de visites els fem separats pq no cal acumular res"""
        tipus_visita = ['9C', '9D', '9R', '9T', '9E', '9Ec']
        dat1 = self.calcul_avui - timedelta(days=6)
        visitesT = c.Counter()
        sql = "SELECT pacient, data, tipus FROM sisap_covid_pac_visites"
        for id, data, tipus in u.getAll(sql, 'redics'):
            if tipus in tipus_visita:
                visitesT[(id, data, tipus)] += 1
        print dat1, self.calcul_avui, self.fi

        rec_visites = c.Counter()
        for (id, up, edat, sexe), n in self.pac_visites.items():
            for tip in tipus_visita:
                ind = 'COVV002' + str(tip)
                rec_visites[(up, edat, sexe, ind, 'den')] += 1
                for single_date in u.dateRange(dat1, self.fi):
                    if (id, single_date, tip) in visitesT:
                        rec_visites[(up, edat, sexe, ind, 'num')] += 1

        for (up, edat, sexe, ind, tipus), n in rec_visites.items():
            if tipus == 'den':
                num = rec_visites[(up, edat, sexe, ind, 'num')]               \
                    if (up, edat, sexe, ind, 'num') in rec_visites else 0
                self.indicadors.append([self.calcul_avui, up, edat, sexe,
                                        ind, num, n])

        for (up, edat, sexe, tipus), n in self.ind_vis.items():
            if tipus == 'den':
                num = self.ind_vis[(up, edat, sexe, 'num')]                   \
                    if (up, edat, sexe, 'num') in self.ind_vis else 0
                numN = self.ind_vis[(up, edat, sexe, 'numN')]                 \
                    if (up, edat, sexe, 'numN') in self.ind_vis else 0
                self.indicadors.append([self.calcul_avui, up, edat, sexe,
                                        'COVV001', num, n])
                self.indicadors.append([self.calcul_avui, up, edat, sexe,
                                        'COVV002', numN, n])

    def export_ind(self):
        """fem export dels indicadors"""

        columns = ["data date", "up varchar2(10)", "grup int",
                   "sexe varchar2(300)", "indicador varchar2(20)",
                   "num int", "den int"]
        u.createTable(tb_back, "({})".format(", ".join(columns)), db, rm=True)
        u.execute("""
                  INSERT INTO {0}
                  SELECT * FROM {1}
                  """.format(tb_back, tb_ind), db)
        for indica in taxes:
            remove = taxes[indica]['remove']
            if remove:
                u.execute("""
                          DELETE FROM {0}
                          WHERE indicador = '{1}'
                          """.format(tb_ind, indica), db)
            else:
                try:
                    u.execute(
                        """
                        DELETE FROM {0}
                        WHERE indicador = '{1}'
                              AND to_char(data, 'DD-MM-YYYY') = '{2}'
                        """.format(tb_ind, indica,
                                   self.calcul_avui.strftime("%d-%m-%Y")), db)
                except Exception as e:
                    print(str(e))
                    pass

        for indica in ind_visites:
            try:
                u.execute(
                    """
                    DELETE from {0}
                    WHERE indicador='{1}'
                    AND to_char(data, 'DD-MM-YYYY') = '{2}'
                    """.format(tb_ind, indica,
                               self.calcul_avui.strftime("%d-%m-%Y")), db)
            except Exception as e:
                print(str(e))
                pass

        # u.createTable(tb_ind, "({})".format(", ".join(columns)), db, rm=False)  # noqa
        u.listToTable(self.indicadors, tb_ind, db)

    def get_cataleg(self):
        """Cataleg"""
        upload = []
        for indica in taxes:
            literal = taxes[indica]['lit'].decode("utf8").encode("latin1")
            multi = taxes[indica]['multi']
            grup = taxes[indica]['grup']
            litc = taxes[indica]['lit_curt'].decode("utf8").encode("latin1")
            upload.append([indica, literal, multi, grup, litc])
        for indica in ind_visites:
            literal = ind_visites[indica]['lit']
            multi = ind_visites[indica]['multi']
            grup = ind_visites[indica]['grup']
            litc = ind_visites[indica]['lit_curt']
            upload.append([indica, literal, multi, grup, litc])
        # columns = ["indicador varchar2(10)", "literal varchar2(300)",
        #            "multiplica int", "agrupador varchar2(100)",
        #            "literal_curt varchar2(100)"]
        # u.createTable(tb_cat, "({})".format(", ".join(columns)), db, rm=True)
        u.execute("TRUNCATE table {0}".format(tb_cat), db)
        u.listToTable(upload, tb_cat, db)

    def dona_grants(self):
        """."""
        users = ["PREDUFFA", "PREDUMMP", "PREDUPRP",
                 "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            # u.execute("GRANT SELECT on {} to {}".format(tb_ind, user), db)
            # u.execute("GRANT SELECT on {} to {}".format(tb_cat, user), db)
            u.execute("GRANT SELECT on {} to {}".format(tb_back, user), db)


if __name__ == '__main__':
    u.printTime("Inici")
    Coronavirus_ind()
    u.printTime("Final")
