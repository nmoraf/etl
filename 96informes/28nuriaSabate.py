# -*- coding: utf-8 -*-
# va enviar un excel que cal pretractar per deixar txt nom�s amb cip14
# s'ha de tirar des de p2262
# genera dos arxius a carpeta ftp, i un arxiu a tempFolder que cal enviar a MMP
from sisapUtils import readCSV, getAll, writeCSV, tempFolder


pth = 'D:\\FileTransfers\\altres\\nsabate_altres\\'
file_in = 'Consulta_FxFemur.txt'
file_ou_px = 'Consulta_FxFemur_pacients.txt'
file_ou_dx = 'Consulta_FxFemur_diagnostics.txt'
file_ou_tx = 'Consulta_FxFemur_prescripcions.txt'


cips = set()
for cip, in readCSV(pth + file_in):
    cips.add(cip)

hashs = {}
sql = 'select usua_cip, usua_cip_cod from pdptb101 where usua_cip in ({})'.format(','.join(['\'{}\''.format(cip[:13]) for cip in cips]))
for cip, hash in getAll(sql, 'pdp'):
    hashs[cip] = hash

ids = {}
sql = 'select hash_a, id_cip from sisap_u11 where hash_a in ({})'.format(','.join(['\'{}\''.format(hash) for cip, hash in hashs.items()]))
for hash, id in getAll(sql, 'redics'):
    ids[hash] = id

data = []
for hash1, id in ids.items():
    for cip1, hash2 in hashs.items():
        if hash1 == hash2:
            for cip2 in cips:
                if cip1 == cip2[:13]:
                    data.append([cip2, id])
writeCSV(tempFolder + file_ou_px, data)

print len(cips), len(hashs), len(ids), len(data)

data = []
sql = "select id_cip, pr_cod_ps, date_format(pr_dde, '%Y%m%d'), if(pr_dba = 0, '', date_format(pr_dba, '%Y%m%d')) from problemes where id_cip in ({})".format(','.join([str(id) for hash, id in ids.items()]))
for row in getAll(sql, 'import'):
    data.append(row)
writeCSV(pth + file_ou_dx, data)

data = []
sql = "select id_cip, ppfmc_pf_codi, pf_cod_atc, date_format(ppfmc_pmc_data_ini, '%Y%m%d'), date_format(ppfmc_data_fi, '%Y%m%d') from tractaments where id_cip in ({})".format(','.join([str(id) for hash, id in ids.items()]))
for row in getAll(sql, 'import'):
    data.append(row)
writeCSV(pth + file_ou_tx, data)
