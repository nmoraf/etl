# coding: iso-8859-1
import collections as c
import sys,datetime
from time import strftime

import sisapUtils as u


def dict_edat(edat):
    if 15 <= edat <= 44:
        ed = 'Entre 15 - 44 anys'
    elif 45 <= edat <= 64:
        ed = 'Entre 45 - 64 anys'
    elif 65 <= edat <= 74:
        ed = 'Entre 65 - 74 anys'
    elif edat >=75:
        ed = 'Majors de 74 anys'
    else:
        ed = 'error'
    return ed


class visites(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_poblacio()
        self.get_visites()
        self.export_dades_pacient()
  
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tancament
           - majors de 14 anys a l'inici del període
        """
        self.pob = {}
        sql = ("select id_cip_sec, codi_sector, edat,  up \
                from assignada_tot \
                where edat > 14", "nodrizas")
        for id, sec, edat, up in u.getAll(*sql):
            if up in self.centres:
                edt = dict_edat(edat)
                self.pob[id] = {'edat': edt}
               
    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        """
        self.dades = c.Counter()
        sql = ("select id_cip_sec, date_format(visi_data_visita, '%Y%m'), visi_up, visi_tipus_visita, \
                       visi_lloc_visita \
               from visites2 \
               where visi_situacio_visita = 'R' and \
                     visi_servei_codi_servei = 'MG'", "import")
        for id, data, up, tipus, lloc in u.getAll(*sql):
            if id in self.pob and up in self.centres:
                edat = self.pob[id]['edat']
                if tipus in ('9C', '9D', '9T', '9E'):
                    tipus = tipus
                elif tipus == '9R':
                    tipus = '9C'    
                elif lloc == 'C':
                    tipus = '9C'   
                elif lloc == 'D':
                    tipus = '9D' 
                self.dades[(data, edat, tipus)] += 1
                
    
    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        upload = []
        for (data, edat, tipus), recompte in self.dades.items():
            upload.append([data, edat, tipus, recompte])
        columns = ('periode varchar(6)',  'edat varchar(100)', 'tipus_visita varchar(10)',
                   'n int'
                    )
        table = 'SISAP_visites_9e'
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db, rm=True)
        u.listToTable(upload, table, db)
        u.writeCSV(u.tempFolder + 'visites_9e_2018.txt', upload)

if __name__ == '__main__':
    visites()
