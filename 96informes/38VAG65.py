# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict

grip = "('EQA0501A', 'EQA0502A')"

nod = 'nodrizas'
db = 'eqa_ind'

def getCentres():
    centres = {}
    sql = "select scs_codi, ics_codi from cat_centres"
    for up, br in getAll(sql, nod):
        centres[up] = br
    return centres
    
def getIndicador(indicador, edat):
    if indicador == 'EQA0502A':
        return "Menors de 60 anys"
    elif indicador == 'EQA0501A':
        if edat <65:
            return 'Entre 60 i 64 anys'
        elif edat >= 65:
            return "65 o més"
        else:
            return "err"
    else:
        return "err"
 
def getDades(centres):
    dades = {}
    uploads = []
    sql = "select ind_codi, up, edat, num, den from mst_indicadors_pacient where ind_codi in {}".format(grip)
    for indicador, up, edat, num, den in getAll(sql, db):
        indica = getIndicador(indicador, edat)
        if up in centres:
            if (up, indica) in dades:
                dades[(up, indica)]['num'] += num
                dades[(up, indica)]['den'] += den
            else:
                dades[(up, indica)] = {'num': num, 'den': den}
    for (up, indica), d in dades.items():
        num = d['num']
        den = d['den']
        ind = d['num']/d['den']
        uploads.append([up, indica, int(num), int(den), ind])
    return uploads
        
centres = getCentres()
upload = getDades(centres)
file_data = "vag.txt"

try:
    remove(tempFolder + file_data)
except:
    pass

writeCSV(tempFolder + file_data, upload, sep='@')
