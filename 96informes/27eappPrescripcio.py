# -*- coding: utf-8 -*-
# assumeix que el fitxer està al tempFolder del PC des d'on s'executa
# si es consolida, caldrà afegir a procés general txt2red
from sisapUtils import getAll, tempFolder, readCSV, createTable, listToTable, calcStatistics, grantSelect


tb = 'sisap_eapp_prescripcio_txt'
db = 'redics'


def format_data(data):
    return data[6:10] + data[3:5] + data[:2]

hash = {}
for id, nis in getAll('select usua_cip, usua_nis from sisap_eapp_usutb040', db):
    hash[str(nis)] = id

upload = []
for nis, pf, desc, ini, fi in readCSV(tempFolder + 'tract.txt', sep='|'):
    if nis in hash:
        upload.append([hash[nis], pf, desc, format_data(ini), format_data(fi)])

createTable(tb, '(usua_cip varchar2(40), pf int, pf_desc varchar2(250), ini date, fi date)', db, rm=True)
listToTable(upload, tb, db, 'YYYYMMDD')
calcStatistics(tb, db)
grantSelect(tb, ('PREDUMMP', 'PREDUPRP'), db)
