# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class Coronavirus(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_edat()
        
        
    def get_centres(self):
        """trec rs"""
        self.centres = {}
        sql = 'select scs_codi, rs from cat_centres'
        for up, rs in u.getAll(sql, 'nodrizas'):
            self.centres[up] = rs
    
    
    def get_edat(self):
        """agafem casos sospitosos per edat"""
        rec, totals = {}, c.Counter()
        sql = "select pac_ambit, dx_up_cod, pac_up_cod, pac_hash, dx_data, trunc((dx_data - pac_naix)/365) \
                from sisap_coronavirus \
                where dx_grup='M' and  pac_hash not in (select pacient from sisap_coronavirus_pcr where resultat='Positiu')"
        for ambit, updx, uppac, hash, data, edat in u.getAll(sql, 'redics'):
            edat = int(edat)
            ed = '0'
            if 0 <= edat < 15:
                ed = 'Menors de 15 anys'
            elif 15 <= edat < 65:
                ed = 'Entre 15 i 64 anys'
            elif edat > 64:
                ed = 'Majors de 64 anys'
            else:
                ed = '0'
            rs = 'No'
            if updx in self.centres:
                rs = self.centres[updx]
            elif uppac in self.centres:
                rs = self.centres[uppac]
            else:
                rs = ambit
            rec[(hash)] = {'rs': rs, 'a': ambit, 'dat': data, 'edat':ed}

        for (hash), r in rec.items():
            totals[(r['dat'], r['rs'], r['a'], r['edat'])] += 1
        
        upload = []
        for (data, rs, amb, ed), n in totals.items():
            upload.append([data, rs, amb, ed, n])
        
        file = u.tempFolder + 'sospita_coronavirus_edat_territori.txt'
        u.writeCSV(file, upload, sep='@')
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    Coronavirus()
    
    u.printTime("Final")       