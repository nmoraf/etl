from sisapUtils import getSubTables, getOne, getAll, multiprocess, writeCSV, tempFolder
from collections import Counter


periode = ('201601', '201605')
ups = ('07676', '07677', '07681')
centres = {cod: des for (cod, des) in getAll("select scs_codi, ics_desc from jail_centres where scs_codi in {}".format(ups), 'nodrizas')}
laboratoris = {cod: cat for (cod, cat) in getAll('select cla_codi, cla_versio_cataleg from redics.labtb111', 'redics')}
proves = {(cataleg, intern): oficial for (cataleg, intern, oficial) in getAll('select dvc_codi_versio, dvc_codi_prova, dvc_codi_oficial from redics.labtb136', 'redics')}
file = tempFolder + 'activitat_laboratoris.csv'


def get_taules():
    taules = []
    for taula in getSubTables('laboratori'):
        dat, = getOne("select date_format(cr_data_reg, '%Y%m') from {} where cr_codi_lab <> 'RIMAP' limit 1".format(taula), 'import')
        if periode[0] <= dat <= periode[1]:
            taules.append([taula, dat])
    return taules


def get_data(params):
    taula, dat = params
    sql = "select cr_codi_lab, codi_up, cr_codi_prova_ics from {} where codi_up in {} and cr_codi_lab <> 'RIMAP'".format(taula, tuple(centres))
    data = Counter()
    for lab, up, prova in getAll(sql, 'import'):
        cataleg = laboratoris[lab]
        oficial = proves[(cataleg, prova)]
        data[(dat, up, oficial)] += 1
    resul = [(dat, centres[up], prova, n) for (dat, up, prova), n in data.items()]
    return resul


if __name__ == '__main__':
    jobs = get_taules()
    mega_resul = multiprocess(get_data, jobs)
    export = []
    for resul in mega_resul:
        export.extend(resul)
    writeCSV(file, export, sep=';')
