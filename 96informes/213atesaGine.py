# coding: utf8

"""
Atesa per ginecòlegs i nombre de visites per ginecòlegs
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

imp = "import"

class atesa_gine(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_ass()
        self.get_pob()
        self.export_data()
        
    def get_centres(self):
        """EAP ICS"""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_ass(self):
        """
        Obtenim visites ginecòlegs
        """
        self.visites = Counter()
        sql = "select id_cip_sec, \
                      visi_up \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                     s_espe_codi_especialitat in ('10099','10098')"
        for id, up in u.getAll(sql, imp):
            self.visites[id] += 1
    
    def get_pob(self):
        """Obtenim Nombre visites per pob assignada"""
        self.pob = Counter()
        sql = "select id_cip_sec, up, edat, sexe from assignada_tot"
        for id, up, edat, sexe in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up][0]
                nvisites = self.visites[(id)] if (id) in self.visites else 0
                atesGINE = 1 if (id) in self.visites else 0
                if sexe == 'D':
                    self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'den')] += 1
                    self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'num')] += nvisites
                    self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'atesa')] += atesGINE
        
             
    def export_data(self):
        """."""
        self.upload, self.upload2 = [], []
        for (br, edat, sexe, tip), n in self.pob.items():
            if tip == 'den':
                num = self.pob[(br, edat, sexe, 'num')]
                ates = self.pob[(br, edat, sexe, 'atesa')]
                self.upload.append([br, edat, sexe, num, n])
                self.upload2.append([br, edat, sexe, ates, n])
                
        u.writeCSV(u.tempFolder + 'Visites_gine.txt', self.upload)  
        u.writeCSV(u.tempFolder + 'Atesa_gine.txt', self.upload2) 
   
if __name__ == '__main__':
    atesa_gine()
    