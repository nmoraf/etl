# -*- coding: utf8 -*-

"""
Refredats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

def get_nivell(edat):
    """."""
    nivell = 'INF1C' if edat < 3 else  'INF2C' if edat <6 else 'PRI' if edat < 12 else 'ESO' if edat <16 else 'BAX' if edat <18 else 'ADULTS'
    return (nivell)

tb = 'sisap_covid_agr_ili_historic'
db = 'redics'

codis = "('B25.0', 'B30.2','J00','J09','J10','J10.0','J10.1','J10.8','J11','J11.0','J11.1','J11.8', \
'J12','J12.0','J12.1','J12.2','J12.3','J12.8','J12.9','J17.1','J20.3','J20.4','J20.5','J20.6','J20.7','J21.0', \
'J21.1','P23.0','U04','U04.9','C01-B25.0','C01-B30.2','C01-B33.4','C01-B97.21','C01-J00','C01-J09','C01-J09.X', \
'C01-J09.X1','C01-J09.X2','C01-J09.X3','C01-J09.X9','C01-J10','C01-J10.0','C01-J10.00','C01-J10.01','C01-J10.08', \
'C01-J10.1','C01-J10.2','C01-J10.8','C01-J10.81','C01-J10.82','C01-J10.83','C01-J10.89','C01-J11','C01-J11.0','C01-J11.00','C01-J11.08', \
'C01-J11.1','C01-J11.2','C01-J11.8','C01-J11.81','C01-J11.82','C01-J11.83','C01-J11.89','C01-J12','C01-J12.0','C01-J12.1','C01-J12.2','C01-J12.3','C01-J12.8', \
'C01-J12.81','C01-J12.89','C01-J12.9','C01-J20.3','C01-J20.4','C01-J20.5','C01-J20.6','C01-J20.7','C01-J21.0','C01-J21.1','C01-P23.0','C01-J20.7','C01-J21.0', \
'C01-J21.1','C01-P23.0')"

grips = ['J09','J10','J10.0','J10.1','J10.8','J11','J11.0','J11.1','J11.8','C01-J09','C01-J09.X', 
'C01-J09.X1','C01-J09.X2','C01-J09.X3','C01-J09.X9','C01-J10','C01-J10.0','C01-J10.00','C01-J10.01','C01-J10.08', 
'C01-J10.1','C01-J10.2','C01-J10.8','C01-J10.81','C01-J10.82','C01-J10.83','C01-J10.89','C01-J11','C01-J11.0','C01-J11.00','C01-J11.08', 
'C01-J11.1','C01-J11.2','C01-J11.8','C01-J11.81','C01-J11.82','C01-J11.83','C01-J11.89']

exclou = ['J12','J12.0','J12.1','J12.2','J12.3','J12.8','J12.9','J17.1','C01-J12','C01-J12.0','C01-J12.1','C01-J12.2','C01-J12.3','C01-J12.8', 
'C01-J12.81','C01-J12.89','C01-J12.9', 'C01-P23.0', 'C01-P23.0', 'P23.0']

class Refredats(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_assignada()
        self.get_refredats()
        self.dona_grants()

    def get_assignada(self):
        """."""
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement from assignada'
        for id, sexe, naix in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        refredatsn = c.Counter()
        sql = "select id_cip, pr_dde,  pr_cod_ps, pr_up, count(*) \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and \
                            pr_cod_ps in {}  \
                            and extract(year_month from pr_dde) >'200908' and \
                            extract(year_month from pr_dde) <'202003' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde, pr_cod_ps, pr_up".format(codis)
                        
        for id, dat,codi, up, recompte in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None
            grup = None
            nivell = 'ADULTS'  
            if ed != None:
                if int(ed) < 15:
                    grup = 0
                elif 15 <= int(ed) < 65:
                    grup = 1
                elif 64 < int(ed) < 75:
                    grup = 2
                elif int(ed)>74:
                    grup = 3
                else:
                    grup = 9
                nivell = get_nivell(ed) 
            tip = 1 if codi in grips else 0
            refredatsn[(dat, up, tip, sexe, ed, grup, nivell)] += recompte
        
       
        cols = ("up varchar2(5)", "data date", "grip int", "sexe varchar2(10)","edat varchar2(10)","grup_edat int","nivell varchar(10)","recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (dat, up, tip, sexe, ed, grup, nivell), n in refredatsn.items():
            n = int (n)
            upload.append([up, dat, tip, sexe, ed, grup, nivell, n])
        u.listToTable(upload, tb, db)

    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db) 
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Refredats()
    
    u.printTime("Final")