# coding: utf8

"""
Vacunes segons any de naixement
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

inici = 2002
final = 2018

codis_vacunes = { 'Triple virica': 38,
                  'Tetanus': 49,
                  'Polio': 311,
                  'Haemophilus influenzae (Hib)': 312,
                  'Hepatitis B': 15,
                  'MCC': 313,
                  'VNC13': 698,
                  'VVZ': 314,
                  'VPH': 782,
                  'Hepatitis A': 34,
                  'Rotavirus': 790,
                  'Tos ferina': 631,
                  'VNC23': 48,
                  'Meningo B': 548,
                  }   

class vacunesCohorts(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_vacunes()
        self.get_pob()        
        self.export_data()
        
    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
    
    def get_vacunes(self):
        """aconseguim vacunes"""
        self.vacunats = {}
        sql = 'select id_cip_sec, agrupador, dosis from ped_vacunes'
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi

        sql = 'select id_cip_sec, agrupador, dosis from eqa_vacunes'
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi
                
    def get_pob(self):
        """agafem població segons criteris edat"""
        self.resultats = Counter()
        sql = "select id_cip_sec, year(data_naix), up from assignada_tot where ates=1 and year(data_naix) between {} and {}".format(inici, final)
        for id, any_naix, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                for c in codis_vacunes:
                    agr = codis_vacunes[c]
                    self.resultats[(any_naix, c, 'Total població')] += 1
                    if (id, agr) in self.vacunats:
                        dosi = self.vacunats[(id, agr)]
                        num1d, num2d, num3d, num4d, num5d = 0, 0, 0, 0, 0
                        self.resultats[(any_naix,c,'1 dosi')] += 1 if dosi > 0 else 0
                        self.resultats[(any_naix,c,'2 dosis')] += 1 if dosi > 1 else 0
                        self.resultats[(any_naix,c,'3 dosis')] += 1 if dosi > 2 else 0
                        self.resultats[(any_naix,c,'4 dosis')] += 1 if dosi > 3 else 0
                        self.resultats[(any_naix,c,'5 dosis')] += 1 if dosi > 4 else 0
                            
    def export_data(self):
        """."""
        self.upload = []
        for (any_naix, agr, tip), n in self.resultats.items():
            self.upload.append([any_naix, agr, tip, n])
                
        u.writeCSV(u.tempFolder + 'vacunes_departament.txt', self.upload)            
   
if __name__ == '__main__':
    vacunesCohorts()