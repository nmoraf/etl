# coding: utf8

debug = False

from sisapUtils import *
from collections import defaultdict,Counter
import datetime


nod = 'nodrizas'
imp = 'import'

dini ='20170901'
dfi = '20180430'


centres = {}
sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc from cat_centres"
for up, br, desc, sap, ambit in getAll(sql, nod):
    centres[up] = {'eap': desc, 'sap': sap, 'amb': ambit}
    
pob = {}
sql = 'select id_cip_sec, up, edat, sexe from assignada_tot'
for id, up, edat,sexe in getAll(sql, nod):
    gedat = 0
    if edat <15:
        gedat = 'Menors de 15'
    elif 15 <= edat <=44:
        gedat = 'Entre 15 i 44'
    elif 45 <= edat <=64:
        gedat = 'Entre 45 i 64'
    elif edat >64:
        gedat = 'Majors de 64'
    else:
        gedat = 'error'
    try:
        eap = centres[up]['eap']
        sap = centres[up]['sap']
        ambit = centres[up]['amb']
    except KeyError:
        continue
    pob[id] = {'up': up, 'sexe': sexe, 'edat': gedat, 'eap': eap, 'sap': sap, 'ambit': ambit}
    
Chiroflu = {}
sql = "select pf_codi from import.cat_cpftb006 where pf_desc_gen like ('CHIROFLU%')"
for pf, in getAll(sql, imp):
    Chiroflu[pf] = True
    

nVacunes = Counter()
vac_admin = 0
sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}_%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    printTime(particio)
    vacunes = {}
    sql = "select id_cip_sec,va_u_cod,date_format(va_u_data_vac,'%Y%m'),date_format(va_u_data_vac,'%Y%m%d'), va_u_pf_cod from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, anys, dat, pf in getAll(sql, imp):
        if dini <= dat <= dfi:
            try:
                Chiroflu[pf]
            except KeyError:
                continue
            vac_admin += 1
            try:
                up = pob[id]['up']
                edat = pob[id]['edat']
                sexe = pob[id]['sexe']
                eap = pob[id]['eap']
                sap = pob[id]['sap']
                ambit = pob[id]['ambit']
            except KeyError:
                continue
            nVacunes[(up, eap, sap, ambit, edat, sexe, anys)] += 1

upload = []
for (up, eap, sap, ambit, edat, sexe, anys), d in nVacunes.items():
    upload.append([up, eap, sap, ambit, edat, sexe, anys, d])

file = tempFolder + 'Chiroflu.txt'
writeCSV(file, upload, sep=';')

print vac_admin

