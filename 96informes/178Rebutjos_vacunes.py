"""
Peticio cop de calor. extreure dades de produccio/explotacio
"""
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import datetime



class Rebutjos(object):
    """."""
    
    def __init__(self):
        """."""
        self.dades = Counter()
        self.get_catalegs()
        self.get_exclusions()
        self.export_data()
        
    def get_catalegs(self):
        """."""
        self.cim10 = {}
        sql = "select if(ps_cod ='Z28.1',2,3), ps_def from cat_prstb001 where ps_cod in ('Z28.1' ,'Z28.2')"
        for ps, desc in getAll(sql, "import"):
            self.cim10[int(ps)] = desc
            
        self.vacunes = {}
        sql = "select vacuna, des from cat_prstb040_new"
        for vac, des in getAll(sql, "import"):
            self.vacunes[vac] = des
    
    def get_exclusions(self):
        """."""
        sql = "select id_cip_sec, ief_cod_v, year(ief_data_alta), ief_exc from exclusions2 \
            where ief_inc_exc = 'E' and ief_data_baixa=0 and ief_exc in (2,3)"
        for id, vac, dat, mot in getAll(sql, "import"):
            motdesc = self.cim10[int(mot)]
            vacdesc = self.vacunes[vac]
            self.dades[(dat, vac,vacdesc, mot, motdesc)] += 1
            
    def export_data(self):
        """."""
        upload = [] 
        for (dat, vac,vacdesc, mot, motdesc),d in self.dades.items():
            upload.append([dat, vac,vacdesc, mot, motdesc, d])
        writeCSV(tempFolder + 'Rebutjos_vacunes.txt', upload)

        
if __name__ == '__main__':
    Rebutjos()