import sisapUtils as u
import hashlib as h
from datetime import timedelta, datetime as d
from collections import defaultdict
import string
import pprint as pp

def isDecimal(n):
   n = n.strip()
   try:
      n = int(n)
      return True
   except:
      return False

def isLetter(l):
    l = l.strip()
    letters = string.ascii_uppercase + 'Ñ'
    try:
        l in letters
        return True
    except:
        return False

def isBoleanNum(b):
    b = b.strip()
    try:
        b = int(b)
        b in range(2)
        return True
    except:
        return False

def looksLikeCip(cip):
    if cip != None:
        l = cip[:4]
        b = cip[4:5]
        n = cip[5:11]
        if all([isLetter(l) for l in l]) and isBoleanNum(b) and all([isDecimal(n) for n in n]):
            return True
        else:
            return False
    else:
        return False

pcr_dict = {0: 'Positiu',
            1: 'No concloent',
            3: 'Negatiu',
            4: 'No valorable',
            9: 'Descartar'}

pcrs = defaultdict(set)
def get_pcr():
    """."""
    db = 'redics'
    sql = """
        SELECT hash, DATA, estat_cod
        FROM SISAP_COVID_PAC_PRV_EXT
        WHERE prova= 'PCR' AND estat_cod <> 9
    """
    for hcovid, data, result in u.getAll(sql, db):
        data = data.date()
        pcrs[hcovid].add((data, result))
get_pcr()

contactes_hist = {}
def get_contactes_hist():
    """ viene por hash_ics """
    db = u.sectors
    tb = "prstb015"
    sql = """
        SELECT u11.cip_usuari_cip, pr_dde, pr_th, pr_hist
        FROM {} p inner join usutb011 u11 on p.pr_cod_u = u11.cip_cip_anterior
        WHERE pr_cod_ps = 'C01-Z20.828'
        -- and pr_th <> 16185 and pr_hist = '1'
        """.format(tb)
    for db in db:
        print(db)
        for cip, data, th, hist in u.getAll(sql, db):
            data = data.date()
            if data >= d(2020,3,1).date() and th <> 16185 and hist == '1':
                hcovid = h.sha1(cip).hexdigest().upper()
                contactes_hist[hcovid] = data
get_contactes_hist()
print(len(contactes_hist))

contactes_mediador = {}
no_contactes_mediador = {}
# limit = "where rownum <200"
limit = ""
def get_mediador():
    db = "exadata"
    sql = """
        SELECT
            substr(regexp_replace(upper(c.CIP),'[^0-9A-Za-z]',''),1,13) AS CIP_CONTACTE
            ,to_date(s.DATACONTACTE,'YYYY-MM-DD') AS CONTACTE_DATA
            ,to_date(p.COVIDSTATUSDATE, 'YYYY-MM-DD') AS CAS_DATA
            ,substr(p.cip,1,13) AS CIP_CAS
            ,substr(s.PCRREALITZADA,1,1)
            ,s.PCRRESULTAT
        FROM 
            dwaquas.media_ccpatientcontacts c LEFT JOIN dwaquas.media_ccpatients p
                ON c.PATIENTCIP = p.CIP
            LEFT JOIN dwaquas.MEDIA_SEGUIMENT_CONTACTES s
                ON c.cip = s.CIP
        {}
    """
    for cip, data, cas_data, cip_cas, pcr_sn, val in u.getAll(sql.format(limit), db):
        hcovid_cas = h.sha1(cip_cas).hexdigest().upper()
        cas_data = cas_data.date() if cas_data else None
        data = data.date() if data else None
        if looksLikeCip(cip):
            hcovid = h.sha1(cip).hexdigest().upper()
            contactes_mediador[hcovid] = {'data': data, 'cas_data':cas_data, 'cas_id':hcovid_cas, 'pcr_sn':pcr_sn, 'val':val}
        else:
            no_contactes_mediador[hcovid_cas] = {'cip':cip, 'data':data, 'cas_data':cas_data, 'pcr_sn':pcr_sn, 'val':val}

get_mediador()
print(len(contactes_mediador),len(no_contactes_mediador))


"""
S C R A T H    Z O N E
"""

[row for row in u.getAll("select pr_cod_u from prstb015_t where pr_cod_ps = 'C01-Z20.828' AND PR_TH <>  16185 AND pr_dde > DATE '2020-03-01'",'redics')]
[col for col in u.getTableColumns('PDPTB101','pdp')]
[row for row in u.getAll("select * from sisap_covid_pac_id where rownum <10",'redics')]
[col for col in u.getTableColumns("sisap_covid_pac_id",'redics')]

hashcovid = h.sha1(None).hexdigest().upper()

"""
select cip_usuari_cip, pr_cod_ps, pr_th, pr_dde, pr_dba
               from prstb015, usutb011
               where pr_cod_u = cip_cip_anterior and
                     pr_cod_o_ps = 'C' and
                     pr_cod_ps in ('C01-Z11.59')"""
"""
last_hics = {}
def get_last_hics():
    db = 'redics'
    tb = 'usutb011'
    sql = "select cip_cip_anterior, cip_usuari_cip from {} where cip_cip_anterior in {}"
    sql.format(tb, tuple(contactes_err.keys()))
    for oldhics, hics in u.getAll(sql, db):
        # if oldhics in contactes_err:
            last_hcovid[oldhics] =  hics
"""


"""
last_hcovid = {}
def get_last_hcovid():
    db = "pdp"
    tb = "pdptb101"
    sql = "select usua_cip_cod, usua_cip from {}".format(tb)
    for hics, cip in u.getAll(sql, db):
        if hics in set(last_hics.values()):
            hcovid = h.sha1(cip).hexdigest().upper()
            last_hcovid[hics] = hcovid
"""


hics_hcovid_map, hcovid_hics_map = {}, {}
def get_covid_map():
    """
    mapeo Hash_ics a Hash_covid: hics_hcovid_map
    mapeo Hash_covid a Hash_ics: hcovid_hics_map
    """
    sql = """
        SELECT distinct hash_ics, hash
        FROM preduffa.sisap_covid_pac_id
        """
    for hics, hcovid in u.getAll(sql,'redics'):
        hics_hcovid_map[hics] = hcovid
        hcovid_hics_map[hcovid] = hics
get_covid_map()
print(len(hics_hcovid_map), len(hcovid_hics_map))

tope=3
curs = 0
for i in pcrs:
    if curs < tope:
        curs +=1
        print(i, pcrs[i])

curs = 0
for i in contactes_hist:
    if curs < tope:
        curs +=1
        print(i, contactes_hist[i])

curs = 0
for i in contactes_mediador:
    if curs < tope:
        curs +=1
        print(i)
        pp.pprint(contactes_mediador[i])

for k in contactes_mediador:
    if k not in contactes_hist:
        pp.pprint(k)
        pp.pprint(contactes_mediador[k])

for k in contactes_hist:
    if k in contactes_mediador:
        pp.pprint(k)
        pp.pprint(contactes_hist[k])
        pp.pprint(pcrs[k])
        pp.pprint(contactes_mediador[k])

pp.pprint('a', pp.pprint(contactes_hist['33E756EFF25B6EB70E21E13AD15E9AE1857C5C4F']))
pp.pprint(pcrs['33E756EFF25B6EB70E21E13AD15E9AE1857C5C4F'])
