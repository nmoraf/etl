# coding: iso-8859-1

"""Petició Elisabet per a Girona. Ho necessita ràpid
"""

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

printTime('Inici')

pacients = {}
sql = 'select id_cip_sec, up from assignada_tot where edat>14'
for id, up in getAll(sql, 'nodrizas'):
    pacients[id] = up
    
    
recomptes = Counter()
sql = "select id_cip_sec, pr_cod_ps\
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext) \
        and (pr_cod_ps like ('H90%') or pr_cod_ps like ('H91%'))"
for id, ps in getAll(sql, 'import'):
    if id in pacients:
        up = pacients[id]
        recomptes[(up, ps)] += 1
        
upload = []
for (up, ps), n in recomptes.items():
    upload.append([up, ps, n])
    
file = tempFolder + 'hipoacusia.txt'
writeCSV(file, upload, sep=';')
        
printTime('Fi')