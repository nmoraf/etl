# -*- coding: utf8 -*-

"""
Visites ACUT per dia setmana i hora
"""
import math, datetime

import collections as c

import sisaptools as u


year = '2016'
debug = False
db = ('redics', 'data')
tb = 'sisap_visites_acut'
tmp = tb + '_tmp'


class Vespre(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        with u.Database(*db) as self.db:
            self.create_tables()
            self.get_centres()
            self.download_visites()
            self.get_result()
            self.upload_result()
            if not debug:
                self.db.drop_table(tmp)

    def create_tables(self):
        """Crea taula temporal."""
        self.db.create_table(tmp, ('up varchar2(5)', 'hores int',
                                   'dies varchar2(150)'), remove=True)
        self.db.create_table(tb, ('eap varchar2(150)','hores int',
                                  'dies varchar2(150)', 'visites int'),
                             remove=True)
        self.db.set_grants('select', tb, 'PREDUMMP')

    def get_centres(self):
        """Centres ICS."""
        sql = "select scs_codi, ics_desc \
               from urg_centres"
        self.centres = {row[0]: row[1:] for row
                        in u.Database('p2262', 'nodrizas').get_all(sql)}

    def download_visites(self):
        """Descarrega les visites instanciant Particio per cada partició."""
        partitions = self.db.get_table_partitions('vistb043')
        if debug:
            Particio(partitions.keys()[23], set(self.centres))
        else:
            pool = u.NoDaemonPool(len(partitions), maxtasksperchild=1)
            for partition in partitions:
                pool.apply_async(Particio, args=(partition, set(self.centres)))
            pool.close()
            pool.join()

    def get_result(self):
        """Recomptes a partir de taula temporal."""
        self.result = c.Counter()
        sql = 'select up, hores, dies from {}'.format(tmp)
        for id in self.db.get_all(sql):
            self.result[id] += 1

    def upload_result(self):
        """Puja resultat a taula definitiva."""
        self.upload = [self.centres[up] + (hores, dies, n)
                       for (up, hores, dies), n in self.result.items()]
        self.db.list_to_table(self.upload, tb)


class Particio(object):
    """Classe a instanciar per cada partició."""

    def __init__(self, partition, centres):
        """Execució seqüencial."""
        self.partition = partition
        self.centres = centres
        with u.Database(*db) as self.db:
            self.get_year()
            if self.year == year or debug:
                self.get_visites()
                self.upload_visites()

    def get_year(self):
        """Any de la partició."""
        sql = "select to_char(visi_data_visita, 'YYYY') \
               from vistb043 partition ({}) \
               where rownum < 2".format(self.partition)
        self.year = self.db.get_one(sql)[0]

    def get_visites(self):
        """Visites de la partició."""
        sql = "select visi_data_visita, visi_up,visi_hora_visita \
               from vistb043 partition ({}) \
               where  visi_situacio_visita = 'R'".format(self.partition)
        self.visites = [(up, math.floor(hora/3600), data.weekday()) for (data, up, hora)
                        in self.db.get_all(sql)
                        if up in self.centres]


    def upload_visites(self):
        """Puja visites a taula temporal."""
        u.Database(*db).list_to_table(self.visites, tmp)

    def upload_my(self):
        listToTable(self.visites, tmp, 'test')
        
if __name__ == '__main__':
    Vespre()
