# coding: latin1

"""
Diagnòstics incidents per Xarxa vigilància hospitalaria
"""

import sys,datetime, re

import sisapUtils as u

dext, = u.getOne("select data_ext from dextraccio", 'nodrizas')

respiratori=set(['C01-J00', 'J00', 'C01-J01', 'C01-J01.0', 'C01-J01.00', 'C01-J01.01', 'C01-J01.1', 'C01-J01.10', 'C01-J01.11', 'C01-J01.2', 'C01-J01.20', 'C01-J01.21', 'C01-J01.3', 'C01-J01.30', 'C01-J01.31', 'C01-J01.4', 'C01-J01.40', 'C01-J01.41', 'C01-J01.8', 'C01-J01.80', 'C01-J01.81', 'C01-J01.9', 'C01-J01.90', 'C01-J01.91', 'J01', 'J01.0', 'J01.1', 'J01.2', 'J01.3', 'J01.4', 'J01.8', 'J01.9', 'C01-J02', 'C01-J02.8', 'C01-J02.9', 'J02', 'J02.8', 'J02.9', 'C01-J03', 'C01-J03.8', 'C01-J03.80', 'C01-J03.9', 'C01-J03.90', 'J03', 'J03.8', 'J03.9', 'C01-J04', 'C01-J04.0', 'C01-J04.1', 'C01-J04.10', 'C01-J04.11', 'C01-J04.2', 'C01-J04.3', 'C01-J04.30', 'C01-J04.31', 'J04', 'J04.0', 'J04.1', 'J04.2', 'C01-J05', 'C01-J05.0', 'C01-J05.1', 'C01-J05.10', 'C01-J05.11', 'J05', 'J05.0', 'J05.1', 'C01-J06', 'C01-J06.0', 'C01-J06.9', 'J06', 'J06.0', 'J06.8', 'J06.9', 'C01-J12', 'C01-J12.0', 'C01-J12.1', 'C01-J12.2', 'C01-J12.3', 'C01-J12.8', 'C01-J12.81', 'C01-J12.89', 'C01-J12.9', 'J12', 'J12.0', 'J12.1', 'J12.2', 'J12.8', 'J12.9', 'C01-J16', 'C01-J16.8', 'J16', 'J16.8', 'C01-J17', 'J17', 'J17.1', 'J17.8', 'C01-J18', 'C01-J18.0', 'C01-J18.1', 'C01-J18.8', 'C01-J18.9', 'J18', 'J18.0', 'J18.1', 'J18.8', 'J18.9', 'C01-J20', 'C01-J20.3', 'C01-J20.4', 'C01-J20.5', 'C01-J20.6', 'C01-J20.7', 'C01-J20.8', 'C01-J20.9', 'J20', 'J20.3', 'J20.4', 'J20.5', 'J20.6', 'J20.7', 'J20.8', 'J20.9', 'C01-J40', 'J40', 'C01-J21', 'C01-J21.0', 'C01-J21.1', 'C01-J21.8', 'C01-J21.9', 'J21', 'J21.0', 'J21.8', 'J21.9', 'C01-J22', 'J22', 'B97.4', 'C01-B97.4', 'C01-J09', 'C01-J09.X', 'C01-J09.X1', 'C01-J09.X2', 'C01-J09.X3', 'C01-J09.X9', 'J09', 'C01-J10', 'C01-J10.0', 'C01-J10.00', 'C01-J10.01', 'C01-J10.08', 'C01-J10.1', 'C01-J10.2', 'C01-J10.8', 'C01-J10.81', 'C01-J10.82', 'C01-J10.83', 'C01-J10.89', 'J10', 'J10.0', 'J10.1', 'J10.8', 'C01-J11', 'C01-J11.0', 'C01-J11.00', 'C01-J11.08', 'C01-J11.1', 'C01-J11.2', 'C01-J11.8', 'C01-J11.81', 'C01-J11.82', 'C01-J11.83', 'C01-J11.89', 'J11', 'J11.0', 'J11.1', 'J11.1', 'J11.8', 'B34.2', 'C01-B34.2', 'B34.0', 'C01-B34.0', 'B97.0', 'C01-B97.0', 'C01-B97.11', 'C01-B97.12', 'C01-B97.19', 'B97.2', 'C01-B97.2', 'C01-B97.21', 'C01-B97.29', 'C01-B97.81'])
enterovirus=set(['B97.1', 'C01-B97.1', 'C01-B97.10', 'C01-B97.11', 'C01-B97.12', 'C01-B97.19', 'B08.4', 'C01-B08.4', 'B08.5', 'C01-B08.5', 'C01-B08.8', 'A85.0', 'C01-A85.0', 'B30.3', 'C01-B30.3', 'B34.1', 'C01-B34.1', 'A88.0', 'C01-A88.0', 'A87.0', 'C01-A87.0'])
resp_ente=respiratori.union(enterovirus)
resp_and_ente=respiratori.intersection(enterovirus)

class diag_incidents(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_poblacio()
        self.rel_codi_desc()
        self.rel_codi_desc_th()
        self.get_problemes()
        self.set_master()
        self.write_CSV()
        
        
    def get_poblacio(self):
        """
        Obtenim població ECAP de 0 a 14 anys a data extracció i la 
        """
        self.id={}
        sql = "select id_cip_sec, edat from assignada_tot where edat <=14"
        for id, edat in u.getAll(sql, 'nodrizas'):
            if id not in self.id:
                self.id[id]=edat
        print "id's saved"
    
        print(len(self.id))
        u.writeCSV(u.tempFolder + 'id.csv', self.id.items()) 
   
    def rel_codi_desc(self):
        """
        Creem una taula que relaciona els problemes de salut 
        amb la seva descripció
        """
        self.codi_desc={}
        sql = "select ps_cod, ps_des from cat_prstb001"
        for codi, desc in u.getAll(sql, 'import'):
            if codi in resp_ente:
                if codi not in self.codi_desc:
                    self.codi_desc[codi]=u.latin2utf(desc)
        for codi in resp_ente:
            if codi not in self.codi_desc:
                self.codi_desc[codi]='CODI SENSE DESCRIPCIÓ'
        u.writeCSV(u.tempFolder + 'codi_desc.csv', self.codi_desc.items()) 
        
    def rel_codi_desc_th(self):
        """
        Creem una taula que relaciona els codis thesaurus 
        amb la seva descripció
        """
        self.codi_desc_th={}
        sql = "SELECT pst_th, pst_des FROM prstb305"
        for codi, desc in u.getAll(sql, '6837'):
            if codi not in self.codi_desc_th:
                self.codi_desc_th[codi]=u.latin2utf(desc)
                
        self.codi_desc_th[0]='CODI THESAURUS SENSE DESCRIPCIÓ'
        print(len(self.codi_desc_th))
        
    def get_problemes(self):
        """
        Obtenim els diagnòstics de la llista pels pacients fins a 14 anys
        """
        print "get problemes"
        self.problemes={}
        date_ini=datetime.datetime.strptime('2018-10-01','%Y-%m-%d').date()
        date_fi=datetime.datetime.strptime('2019-05-19','%Y-%m-%d').date()
        i=0
        sql = "select id_cip_sec, pr_cod_ps, pr_th, pr_dde from problemes"
        for id, codi, codi_th, data in u.getAll(sql, 'import'):
            if data >= date_ini and data <= date_fi:
                if id in self.id and codi in resp_ente:
                    i+=1
                    if(i%10000==0):
                        print(i)
                    self.problemes[id, codi, codi_th, data, self.id[id]] = data

    def set_master(self):
        """
        Creació taula mestra
        """
        self.recompte={}
        for row in self.problemes:
            id=row[0]
            codi=row[1]
            codi_th=row[2]
            data=row[3]
            edat=row[4]
            if (codi, codi_th, data, edat) not in self.recompte:
                self.recompte[codi, codi_th, data, edat]=1
            else:
                self.recompte[codi, codi_th, data, edat]+=1
        u.writeCSV(u.tempFolder + 'recompte.csv', self.recompte.items())
        
        self.master={}
        for row in self.recompte:
            codi=row[0]
            codi_th=row[1]
            data=row[2]
            edat=row[3]
            if codi in resp_and_ente:
                    tipus='respiratori i enterovirus'
            else: 
                if codi in respiratori:
                    tipus='respiratori'
                else:
                    tipus='enterovirus'
            
            self.master[row]=(row[0], row[1], row[2], row[3], \
            self.codi_desc[codi], self.codi_desc_th[codi_th], tipus, \
            self.recompte[row])
        u.writeCSV(u.tempFolder + 'res_master.csv', self.master.values(), sep=";")

    def write_CSV(self):
        print("creating table in test.xarxa_vig_vrs")
        cols = "(codi varchar(12), codi_th int, data date, edat int, \
        desc_codi varchar(60), desc_th varchar(60), tipus varchar(25), recompte int)"
        u.createTable("xarxa_vig_vrs", cols, "test", rm=True)
        self.list=u.readCSV(u.tempFolder + "res_master.csv", sep=";")
        u.listToTable(self.list, "xarxa_vig_vrs", "test")   
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    diag_incidents()
    
    u.printTime("Fi")
        
# des de la Xarxa de Vigilància Hospitalaria de VRS a Catalunya, ens demanen, ara puntualment (després un cop explorades les dades serà un enviament setmanal) el següent: una sèrie de diagnòstics incidents

# codi diagnòstic: els codis els tens a l'excel, es divideixen en 2 respiratoris i enterovirus. ojo que estan filtrats, no treguis el filtre (els pintats de groc no entren). són els incidents del període
# codi del thesaurus si el té
# descripció del ps
# descripció del thesaurus si el té
# tipus: respiratori o enterovirus
# data del diagnòstic
# edat del pacient, la que et sigui més fàcil o en el moment del diagnòstic o a final de període (al final serà igual perquè farem períodes setmanals)
# recompte de casos
# període: 01/10/2018 a 19/05/2019
# població ecap de 0 a 14 anys   