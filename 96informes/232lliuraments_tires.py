# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


PERIODE = ("20200101", "20200831")
TIPUS = {1124: "B", 478: "A", 1114: "A", 1113: "A", 1066: "A", 1040: "A",
         1083: "B", 1102: "A", 1072: "K", 1046: "K", 1085: "K", 1105: "A",
         1106: "A", 1074: "A", 1116: "B", 1081: "B", 1101: "B", 1104: "A",
         1103: "A", 1080: "A", 1120: "K", 1123: "A", 1065: "A", 1090: "K",
         1095: "K", 477: "A", 1070: "A", 1014: "A", 1084: "A", 1112: "A",
         1111: "A", 1110: "A", 1013: "A", 1094: "A", 1093: "A", 1117: "B",
         1118: "B", 1119: "B", 1077: "K", 1042: "K", 1045: "K", 1044: "K",
         1122: "K", 1097: "K", 1043: "K", 1041: "K", 1075: "A", 475: "A",
         1022: "A", 1020: "A", 1109: "A", 1108: "A", 1079: "A", 1107: "A",
         1011: "A", 1068: "K", 1067: "A", 1071: "A", 1100: "B", 1099: "B",
         1098: "B", 1115: "I", 1023: "A", 1073: "A", 1076: "B", 1078: "B",
         1091: "A", 1121: "A",
         1087: "A", 1005: "A", 1006: "A"}  # ABBOT/ROCHE sin clasificar: "A"

ABBOT = set([1023, 1065, 1067, 1071, 1080, 1081, 1087, 1101, 1103, 1104, 1116, 1123])  # noqa

ROCHE = set([478, 1005, 1006, 1040, 1066, 1083, 1098, 1099, 1100, 1113, 1114, 1124])  # noqa


class Tires(object):
    """."""

    def __init__(self):
        """."""
        self.get_insulines()
        self.get_pacients()
        self.get_cataleg()
        self.get_fitxes()
        self.get_lliuraments()
        self.export_data()

    def get_insulines(self):
        """."""
        sql = "select id_cip_sec from eqa_tractaments where farmac = 587"
        self.insulines = set([id for id, in u.getAll(sql, "nodrizas")])

    def get_pacients(self):
        """."""
        self.pacients = {}
        sql = "select id_cip_sec, indicador from mst_tir_pacient"
        for id, ind in u.getAll(sql, "altres"):
            if ind == "TIR004" and id not in self.insulines:
                ind += "int"
            self.pacients[id] = ind

    def get_cataleg(self):
        """."""
        sql = "select pd_codi from cat_cpftb025 where pd_tipus = 'T'"
        self.cataleg = {cod: TIPUS.get(cod, "D")
                        for cod, in u.getAll(sql, "import")}

    def get_fitxes(self):
        """."""
        sql = "select id_cip_sec, fit_cod from tires"
        self.fitxes = {cod: id for (id, cod) in u.getAll(sql, "import")}

    def get_lliuraments(self):
        """."""
        self.lliurades = c.Counter()
        self.desconeguts = c.Counter()
        self.no_trobats = set()
        sql = "select llipr_cod_pac, llipr_pd_cod_lli, llipr_quant_pd_lli, \
                      llipr_up,  llipr_data_lli \
               from tires3 \
               where llipr_data_lli between {} and {}".format(*PERIODE)
        for pac, cod, n, up, dat in u.getAll(sql, "import"):
            if pac in self.fitxes and cod in self.cataleg:
                month = dat.month
                tipus = self.cataleg[cod]
                lab = 'ABBOT' if cod in ABBOT else 'ROCHE' if cod in ROCHE \
                    else 'ALTRES'
                id = self.fitxes[pac]
                grup = self.pacients.get(id, "TIR999")
                # self.lliurades[(grup, tipus)] += n
                self.lliurades[(grup, lab, tipus, month)] += n
                if tipus == "D":
                    self.desconeguts[cod] += n
                if grup == "TIR999" and up == "00441":
                    self.no_trobats.add((up, id))

    def export_data(self):
        """."""
        u.writeCSV(u.tempFolder + "lliuraments.csv",
                   [('grup', 'lab', 'tipus', 'month', 'n')]
                   + sorted([k + (v,) for (k, v) in self.lliurades.items()]),
                   sep=";")
        u.writeCSV(u.tempFolder + "desconeguts.csv",
                   sorted(self.desconeguts.items()), sep=";")
        u.writeCSV(u.tempFolder + "no_poblacio.csv",
                   sorted(self.no_trobats), sep=";")


if __name__ == "__main__":
    Tires()
