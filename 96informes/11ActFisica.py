# coding: iso-8859-1
#Carmen Cabezas,  (ion 7766), marc16, Ermengol
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

nod="nodrizas"
imp="import"

var = "('AL110','EXFICN','EXFIPR','VP1101','VA3030','EP4012')"

act = "('CA331')"

sql='select concat(year(data_ext),month(data_ext)) from dextraccio'
for d in getOne(sql, nod):
    dcalcul=d

OutFile= tempFolder + '11PetiActFisica_' + dcalcul + '.txt'

printTime('Pob')

centres = {}
sql = 'select amb_desc,sap_desc,scs_codi,ics_desc from cat_centres'
for a,s,up,d in getAll(sql,nod):
    centres[up] = {'ambit':a,'sap':s,'desc':d}
    
assig = {}
sql = 'select id_cip_sec,up from assignada_tot'
for id,up in getAll(sql,nod):
    assig[id] = {'up':up,'ambit':centres[up]['ambit'],'sap':centres[up]['sap'],'desc':centres[up]['desc']} 


printTime('Inici càlcul')

afisica = Counter()
sql="select date_format(vu_dat_act,'%Y%m'),id_cip_sec, vu_cod_vs, vu_val from variables where vu_cod_vs in {}".format(var)
for anys,id,var,val in getAll(sql, imp):
    val=int(val)
    try:
        afisica[anys,assig[id]['ambit'],assig[id]['sap'],assig[id]['up'],assig[id]['desc'],var,val] += 1
    except KeyError:
        continue
        
sql="select date_format(au_dat_act,'%Y%m'),id_cip_sec, au_cod_ac, au_val from activitats where au_cod_ac in {}".format(act)
for anys,id,var,val in getAll(sql, imp):
    try:
        afisica[anys,assig[id]['ambit'],assig[id]['sap'],assig[id]['up'],assig[id]['desc'],var,val] += 1
    except KeyError:
        continue
        
with openCSV(OutFile) as c:
    for (anys,ambit,sap,up,desc,var,val),count in afisica.items():
        c.writerow([anys,ambit,sap,up,desc,var,val,count])
        
printTime('Inici càlcul')