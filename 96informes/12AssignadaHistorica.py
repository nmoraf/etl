# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'redics'
table = 'sisap_AssignadaHistorica'

path = tempFolder + 'pob_ass_2009_2014.txt'
#path = tempFolder + 'test.txt'

upload = []
with open(path, 'rb') as file:
    p=csv.reader(file, delimiter='|')
    for i in p:
        anys,sector,hash,up,uba,visita= i[0],i[1],i[2],i[3],i[4],i[5]
        if anys == visita:
            ates = 1
        else:
            ates = 0
        ubainf = ''
        upload.append([anys,sector,hash,up,uba,ubainf,ates])

anys = list(range(2009, 2050))
partitions= ','.join(["partition s{0} values ('{0}')".format(dany) for dany in anys]) + ',partition sError values (default)'
print partitions

try:
    execute('drop table {}'.format(table),db)
except:
    pass

sql = "create table {0}(dataAny number,codi_sector varchar2(4),idpac varchar2(40),up varchar2(5),uab varchar2(5),uabinf varchar2(5),ates int) partition by list (dataAny) ({1})".format(table,partitions)
execute(sql,db)
uploadOra(upload,table,db)

users= ['PREDUECR','PREDUMMP','PREDUPRP']
for user in users:
    execute("grant select on {} to {}".format(table,user),db)

