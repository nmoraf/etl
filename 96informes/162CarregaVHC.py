# coding: latin1

"""
Càrrega viral de VHC de nod_serologies
codis de l'umi
"""

import collections as c

import sisapUtils as u


tb = "SISAP_VHC_CARREGA"
db = "redics"

codis_vhc = "('001995', 'V01485', 'V01385')"

class VHC(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.get_up()
        self.get_valorscarrega()
        self.get_carrega()
        self.upload_data()
        
    def get_hash(self):
        """Obtenim els hashos dels pacients"""
        self.hashos = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, 'import'):
            self.hashos[id] = {'s': sec, 'h': hash}

    def get_up(self):
        """."""
        self.ups = {}
        sql = 'select id_cip_sec, up from assignada_tot'
        for id, up in u.getAll(sql, 'nodrizas'):
            self.ups[id] = up
    
    def get_valorscarrega(self):
        """."""
        self.valorsc = {}
        sql = "select id_cip_sec, cr_codi_lab, cr_codi_prova_ics, date_format(cr_data_reg, '%Y%m%d'), cr_res_lab,cr_unitats_lab from laboratori where cr_codi_prova_ics in {}".format(codis_vhc)
        for id, lab, prova, dat, resul, unit in u.getAll(sql, 'import'):
            self.valorsc[(id, prova, dat)] = {'lab': lab, 'resul': resul, 'unit': unit}
       
    def get_carrega(self):
        """carrega viral de nod_serologies"""
        self.pacients = {}
        self.upload = []
        sql = "select id_cip_sec, dat,cod, val from nod_serologies where cod in {}".format(codis_vhc)
        for id, dat, cod, val in u.getAll(sql, 'nodrizas'):
            if id in self.ups:
                up = self.ups[id]
                if id in self.hashos:
                    sec = self.hashos[id]['s']
                    hash = self.hashos[id]['h']
                    lab = self.valorsc[(id, cod, dat)]['lab']
                    resul = self.valorsc[(id, cod, dat)]['resul']
                    unit = self.valorsc[(id, cod, dat)]['unit']
                    if (id, sec, hash) in self.pacients:
                        dat1 = self.pacients[(id, sec, hash)]['dat']
                        if dat > dat1:
                            self.pacients[(id, sec, hash)]['dat'] = dat
                            self.pacients[(id, sec, hash)]['val'] = val
                            self.pacients[(id, sec, hash)]['lab'] = lab
                            self.pacients[(id, sec, hash)]['resul'] = resul
                            self.pacients[(id, sec, hash)]['unit'] = unit
                    else:
                        self.pacients[(id, sec, hash)] = {'dat': dat, 'val': val, 'up': up, 'lab': lab, 'resul': resul, 'unit':unit}
        for (id, sec, hash), dades in self.pacients.items():
            if dades['val'] != 0:
                self.upload.append([sec, hash, dades['up'], dades['dat'],dades['val'],dades['lab'],dades['resul'],dades['unit']])
    
    def upload_data(self):
        """."""
        columns = ["codi_sector varchar2(4)", "hash varchar2(40)", "up varchar2(5)", "data int", "valor number", "lab varchar2(400)", "valor_origen varchar2(400)", "unitat varchar2(400)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUECR"), db)


if __name__ == "__main__":
    VHC()
