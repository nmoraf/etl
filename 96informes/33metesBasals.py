# coding: iso-8859-1
# necessitem arxiu de metes a Tempfolder
import os
import os.path
from datetime import *
from sisapUtils import *
from collections import defaultdict

printTime()

eqa = 'eqa_ind'
db = 'test'

tableOrigen = 'exp_ecap_uba'
table = 'test_metes_eqa'
file_data = 'simulacio_detectats.txt'

path = tempFolder + 'meteseqa2016.txt'

metes = {}
with open(path, 'rb') as file:
    p = csv.reader(file, delimiter=';', quotechar='|')
    for ind in p:
        pob, indicador, neap, coef, minp, maxp, mmin, mint, mmax = ind[0], ind[1], ind[2], ind[3], ind[4], ind[5], ind[6], ind[7], ind[8]
        if pob == 'NOINSAT':
            mmin = float(mmin)/100
            mmax = float(mmax)/100
            metes[indicador] = {'mmin': mmin, 'mmax': mmax}

upload = []
sql = 'select up, uba, tipus, indicador, detectats from {}'.format(tableOrigen)
for up, uba, tipus, indicador, detectats in getAll(sql, eqa):
    try:
        mmin = metes[indicador]['mmin']
        mmax = metes[indicador]['mmax']
    except KeyError:
        continue
    dmin = detectats * mmin
    dmax = detectats * mmax
    upload.append([up, uba, tipus, indicador, detectats, dmin, dmax])

createTable(table, '(up varchar(5), uba varchar(5), tipus varchar(2), indicador varchar(10), detectats double, min_detectats double, max_detectats double)', db, rm=True)
listToTable(upload, table, db)

try:
    remove(tempFolder + file_data)
except:
    pass

writeCSV(tempFolder + file_data, upload, sep=';')

printTime()
