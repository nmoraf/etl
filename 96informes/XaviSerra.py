# coding: utf-8

"""
 - TIME EXECUTION 15m
 
 
* 2020/09/02 Actualización: solicitan GLICADA / CKD-EPI / FEV1 (o en su defecto alguna variable respiratoria)

Notas:
 - GLICADA: Agrupador EQA 20 / Laboratori BB127 Q32036 Q32136 Variables LBB127
 - CKD-EPI: Agrupador EQA No existe Existe FG con MDRD / Laboratori W18161 W18261 Variables LUF002
 - FEV1/FVC: Agrupador EQA 447 / Variables EF/F TR302B
"""

import sisapUtils as u
import csv, os, sys
from time import strftime
import collections as c
from datetime import datetime
import hashlib as h

ROWNUM_CONDITION = " where rownum <= 10"
LIMIT_CONDITION = " limit 100"

ROWNUM_CONDITION = ""
LIMIT_CONDITION = ""

class XaviSerra(object):
    """.""" 
    def __init__(self):
        """."""
        self.get_covid_pac_master()
        self.get_usutb040()
        self.get_covid_pac_id()
        self.get_u11()
        self.get_tabac()
        self.get_charlson()
        self.get_lab_glicada()
        self.get_lab_ckdepi()
        self.get_var_fev1fvc()
        self.get_upload()
        self.export_data()
        self.upload_table()

    def get_covid_pac_master(self):
        """."""
        print("--------------------------------------------- get_covid_pac_master")
        self.covid_pac_master = {}
        sql = "select hash, edat from sisap_covid_pac_master{}".format(ROWNUM_CONDITION)
        for hash_covid, edat in u.getAll(sql,'redics'):
            self.covid_pac_master[hash_covid]={'edat': edat}
        sql = "select hash, edat from sisap_covid_pac_master_h{}".format(ROWNUM_CONDITION)
        for hash_covid, edat in u.getAll(sql,'redics'):
            self.covid_pac_master[hash_covid]={'edat': edat}            
        print("COVID_PAC_MASTER: N de registros:", len(self.covid_pac_master))

    def get_usutb040(self):
        """."""
        print("--------------------------------------------- get_usutb040")
        ts = datetime.now()
        resultat = u.multiprocess(self.get_usutb040_worker, u.sectors)
        self.usutb040 = {}
        for sector in resultat:
            for cip13, hash_covid, indf, subindf in sector:
                if hash_covid in self.covid_pac_master:
                    #telf = None
                    #if len(str(telf1)) == 9 and str(telf1[0]) == "6":
                    #    telf = telf1
                    #if len(str(telf2)) == 9 and str(telf2[0]) == "6" and telf is None:
                    #    telf = telf2
                    self.usutb040[hash_covid]= {'cip13': cip13, 'indf': indf, 'subindf': subindf}
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.usutb040)))

    def get_usutb040_worker(self, sector):
        """Worker de usutb040"""
        sql = "select usua_cip, \
                      USUA_IND_FARMACIA, USUA_SUBIND_FARMACIA \
               from usutb040"
        db = sector
        converter = []
        for cip13, indf, subindf in u.getAll(sql, db):
            hash_covid = h.sha1(cip13).hexdigest().upper()
            converter.append((cip13, hash_covid, indf, subindf))
        print(sector)
        return converter
        
    def get_covid_pac_id(self):
        """."""
        print("--------------------------------------------- get_covid_pac_id")
        self.covid_pac_id = {}
        sql = "select hash, cip, hash_ics from sisap_covid_pac_id"
        for hash_covid, cip13, hash_ics in u.getAll(sql, "redics"):
            self.covid_pac_id[hash_covid]={'cip13': cip13, 'hash_ics': hash_ics}
        print("COVID_PAC_ID: N de registros:", len(self.covid_pac_id))

    def get_u11(self):
        '''.'''
        print("--------------------------------------------- get_u11")
        print(datetime.now())
        ts = datetime.now()
        self.u11 = {}
        db = 'import'
        tb = 'u11'
        sql = 'select id_cip, hash_d, codi_sector from {}.{}{}'.format(db, tb, LIMIT_CONDITION)
        for id_cip, hash_ics, sector in u.getAll(sql, db):
            self.u11[hash_ics] = {'id_cip': id_cip, 'sector': sector}
        print('Time execution time {} / nrow {}'.format(datetime.now() - ts, len(self.u11)))

    def get_tabac(self):
        """Taula TABAC de SIDIAP traspasada per Leo a la bbdd PERMANENT"""
        print("--------------------------------------------- get_tabac")
        self.tabac_pac = {}
        self.tabac = c.defaultdict(set)
        sql = "select * from tabac{}".format(LIMIT_CONDITION)
        for hash_ics, _sector, val, dat, dbaixa, _dlast, _is_last in u.getAll(sql, "permanent"):
            dat = str(dat)
            dbaixa = str(dbaixa)
            self.tabac[hash_ics].add((dbaixa, dat, val))
        for hash_ics in self.tabac:
            maxtupla = max(self.tabac[hash_ics])
            self.tabac_pac[hash_ics]= {'val': maxtupla[2], 'dat': maxtupla[1], 'dbaixa': maxtupla[0]}
        print("TABAC: N de pacientes:", len(self.tabac))

    def get_charlson(self):
        """Taula CHARLSON de SIDIAP traspasada per Leo (20200831) a la bbdd PERMANET"""
        print("--------------------------------------------- get_charlson")
        self.charlson_pac = {}
        self.charlson = c.defaultdict(set)
        sql = "select id_persistent, str_to_date(dat,'%Y%m%d'), str_to_date(datfi,'%Y%m%d'), chix from 20202t_charlson{}".format(LIMIT_CONDITION)
        for hash_ics, dat, dbaixa, val in u.getAll(sql, "permanent"):
            self.charlson[hash_ics].add((dbaixa,dat,val))
        for hash_ics in self.charlson:
            maxtupla = max(self.charlson[hash_ics])
            self.charlson_pac[hash_ics]= {'val': maxtupla[2], 'dat': maxtupla[1], 'dbaixa': maxtupla[0]}
        print("CHARLSON: N de pacientes:", len(self.charlson))

    def get_lab_glicada(self):
        '''
        ejemplo de manipular cr_res_lab: 731homolChagas.py
        '''
        print("--------------------------------------------- get_lab_glicada")
        print(datetime.now())
        ts = datetime.now()
        self.lab_glicada_pac = {}
        self.lab_glicada = c.defaultdict(set)
        db = 'import'
        tb = 'laboratori2'
        sql = "select id_cip, cr_codi_prova_ics, cr_data_reg, cr_res_lab from {}.{} \
               where cr_codi_prova_ics in ('BB127','Q32036','Q32136') {}".format(db, tb, LIMIT_CONDITION)
        for id_cip, _codi, dat, val in u.getAll(sql, db):
            val1 = val.replace(',','.').replace('"', '').replace('*', '').lstrip()
            try:
                val_n = float(val1.split(' ')[0])
            except:
                val_n = None
            self.lab_glicada[(id_cip)].add((dat,val_n))
        for id_cip in self.lab_glicada:
            maxtupla = max(self.lab_glicada[id_cip])
            self.lab_glicada_pac[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}
        print('Time execution time {} / nrow {}'.format(datetime.now() - ts, len(self.lab_glicada_pac)))

    def get_lab_ckdepi(self):
        '''.'''
        print("--------------------------------------------- get_lab_ckdepi")
        print(datetime.now())
        ts = datetime.now()
        self.lab_ckdepi_pac = {}
        self.lab_ckdepi = c.defaultdict(set)
        db = 'import'
        tb = 'laboratori2'
        sql = "select id_cip, cr_codi_prova_ics, cr_data_reg, cr_res_lab from {}.{} \
               where cr_codi_prova_ics in ('W18161','W18261') {}".format(db, tb, LIMIT_CONDITION)
        for id_cip, _codi, dat, val in u.getAll(sql, db):
            if val == '>90,00':
                val = '>90'
            if val == '>90 mL/min/1,73m2':
                val = '>90'
            if val == '>90,0':
                val = '>90'
            if val == '> 90':
                val = '>90'
            val = val.replace(',','.').replace('*','').lstrip()
            if len(val) > 5:
                val = ''
            self.lab_ckdepi[(id_cip)].add((dat,val))
        for id_cip in self.lab_ckdepi:
            maxtupla = max(self.lab_ckdepi[id_cip])
            self.lab_ckdepi_pac[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}
        print('Time execution time {} / nrow {}'.format(datetime.now() - ts, len(self.lab_ckdepi_pac)))

    def get_var_fev1fvc(self):
        """ Codigos: EF/F TR302B 
            - Si existen 2 mediones el mismo dia selecciona la mayor
        """
        print("--------------------------------------------- get_var_fev1fvc")
        print(datetime.now())
        ts = datetime.now()
        self.var_fev1fvc_pac = {}
        self.var_fev1fvc = c.defaultdict(set)
        db = 'import'
        tb = 'variables2'
        sql = "select id_cip, vu_cod_vs, vu_dat_act, vu_val from {}.{} \
               where vu_cod_vs in ('EF/F','TR302B') {}".format(db, tb, LIMIT_CONDITION)
        for id_cip, _codi, dat, val in u.getAll(sql, db):
            self.var_fev1fvc[(id_cip)].add((dat,val))
        for id_cip in self.var_fev1fvc:
            maxtupla = max(self.var_fev1fvc[id_cip])
            self.var_fev1fvc_pac[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}
        print('Time execution time {} / nrow {}'.format(datetime.now() - ts, len(self.var_fev1fvc_pac)))

    def get_upload(self):
        """ . """
        print("--------------------------------------------- get_upload")
        self.upload=[]
        for hash_covid in self.covid_pac_master:
            cip13 = self.covid_pac_id[hash_covid]['cip13'] if hash_covid in self.covid_pac_id else None
            hash_ics = self.covid_pac_id[hash_covid]['hash_ics'] if hash_covid in self.covid_pac_id else None
            id_cip = self.u11[hash_ics]['id_cip'] if hash_ics in self.u11 else None
            tab_dat = str(self.tabac_pac[hash_ics]['dat']) if hash_ics in self.tabac_pac else ''
            tab_val = str(self.tabac_pac[hash_ics]['val']) if hash_ics in self.tabac_pac else ''
            charlson_dat = str(self.charlson_pac[hash_ics]['dat']) if hash_ics in self.charlson_pac else ''
            charlson_val = str(self.charlson_pac[hash_ics]['val']) if hash_ics in self.charlson_pac else ''
            glicada_dat = self.lab_glicada_pac[id_cip]['dat'] if id_cip in self.lab_glicada_pac else None
            glicada_val = str(self.lab_glicada_pac[id_cip]['val']) if id_cip in self.lab_glicada_pac else ''
            ckdepi_dat = self.lab_ckdepi_pac[id_cip]['dat'] if id_cip in self.lab_ckdepi_pac else None
            ckdepi_val = str(self.lab_ckdepi_pac[id_cip]['val']) if id_cip in self.lab_ckdepi_pac else ''
            fev1fvc_dat = self.var_fev1fvc_pac[id_cip]['dat'] if id_cip in self.var_fev1fvc_pac else None
            fev1fvc_val = str(self.var_fev1fvc_pac[id_cip]['val']) if id_cip in self.var_fev1fvc_pac else ''
            indf = self.usutb040[hash_covid]['indf'] if hash_covid in self.usutb040 else ''
            subindf = self.usutb040[hash_covid]['subindf'] if hash_covid in self.usutb040 else ''
            self.upload.append((cip13, hash_covid, 
                                tab_dat, tab_val, charlson_dat, charlson_val,
                                glicada_dat, glicada_val,
                                ckdepi_dat, ckdepi_val,
                                fev1fvc_dat, fev1fvc_val,
                                indf, subindf))

    def export_data(self):
        """."""        
        print("--------------------------------------------- export_data")
        u.writeCSV(u.tempFolder + 'XaviSerra.txt', self.upload)

    def upload_table(self):
        """."""
        print("--------------------------------------------- upload_table")
        table = "sisap_coronavirus_charlson"
        cols = "(cip varchar2(13), hash varchar2(40), tab_dat varchar2(10), tab_val int, charlson_dat varchar2(10), charlson_val int, glicada_dat date, glicada_val varchar2(20), ckdepi_dat date, ckdepi_val varchar2(20), fev1fvc_dat date, fev1fvc_val varchar2(2000), indf varchar2(10), subindf varchar2(10))"
        db = "exadata"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)

if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    XaviSerra()
    print('Time execution {}'.format(datetime.now() - ts))