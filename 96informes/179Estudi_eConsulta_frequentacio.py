# coding: utf8


import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

tb = "SISAP_ECONSULTA_FREQUENTACIO"
db = "redics"

class Frequentacio(object):
    """Classe principal a instanciar."""
    
    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_visites()
        self.get_edats()
        self.get_assignada()
        self.upload_data()        
        
    def get_centres(self):
        """EAP ICS"""
        sql = ("select scs_codi, ics_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: br for (up, br)
                        in u.getAll(*sql)}
                        
    
    def get_visites(self):
        """Agafem visites del 2017"""          
        self.visitespacients = Counter()
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), "import")
            if dat == 2017:
                sql = "select id_cip_sec, visi_up \
                        from {} \
                        where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_servei_codi_servei = 'MG'".format(table)
                for id, up in u.getAll(sql, "import"):
                    if up in self.centres:
                        self.visitespacients[id] += 1
        
    def get_edats(self):
        """Obtenim edat de import assignada"""
        self.all_pob = {}
        sql = 'select id_cip_sec, usua_sexe, year(usua_data_naixement) from assignada'
        for id,  sexe, naix in u.getAll(sql, "import"):
            edat = 2017 - int(naix)
            self.all_pob[id] = edat

    
    def get_assignada(self):
        """Assignada Historica del 2017"""
        self.assig = Counter()
        sql = "select id_cip_sec, up, uab, ates from assignadahistorica where dataany='2017'"
        for id, up, uba, ates in u.getAll(sql, "import"):
            if up in self.centres:
                if id in self.all_pob:
                    edat = self.all_pob[id]
                    if edat > 14:
                        visit = self.visitespacients[id] if id in self.visitespacients else 0
                        self.assig[(up,uba, 'ass')] += 1
                        self.assig[(up, uba, 'visites')] += visit
                        if ates == 1:
                            self.assig[(up,uba, 'atesa')] += 1

    def upload_data(self):
        """."""
        upload = []
        for (up, uba, tipus), recomptes in self.assig.items():
            if tipus == 'ass':
                atesos = self.assig[(up,uba, 'atesa')] 
                visites = self.assig[(up,uba, 'visites')] 
                upload.append([up, uba, visites, atesos, recomptes])
        columns = ["up varchar(5)", "uba varchar(5)", "visites number", "atesa number", "assignada number"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUEHE"), db)
                       
        
if __name__ == '__main__':
    Frequentacio()
    