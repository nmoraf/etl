# coding: iso-8859-1
#Pregunta parlament: VIH sida
from sisapUtils import *
from collections import defaultdict, Counter

nod = 'nodrizas'
imp = 'import_jail'

vihSida = 101

codis_dx = []
sql = 'select criteri_codi from eqa_criteris where agrupador={}'.format(vihSida)
for c, in getAll(sql, nod):
    codis_dx.append(c)
    
in_crit = tuple(codis_dx)    

dx_vihSida = {}
sql = "select id_cip_sec, pr_dde \
        from problemes \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= '2016/12/31' and (pr_data_baixa is null or pr_data_baixa > '2016/12/31') and (pr_dba is null or pr_dba > '2016/12/31') \
        and pr_cod_ps in {}".format(in_crit)
for id, dde in getAll(sql, imp):
    dx_vihSida[id] = True
 

sql = ("select scs_codi, ics_codi, ics_desc from jail_centres",nod)
centres = {up: (br, desc) for (up, br, desc)
                        in getAll(*sql)}

 
pacs = {}
pac = {}
sql = "select id_cip_sec, huab_up_codi, huab_data_ass, huab_data_final from moviments where (huab_data_ass between '2016/01/01' and '2016/12/31') or (huab_data_final between '2016/01/01' and '2016/12/31') \
or (huab_data_ass <'2016/01/01' and huab_data_final>'2016/12/13')"
for id, up, inici, final in getAll(sql, imp):
    if id in dx_vihSida:
        pacs[(id, up)] = True
        pac[(id)] = True

recomptes = Counter()
for (id, up),t in pacs.items():
    recomptes[up] += 1

recompte = 0  
for  (id),t in pac.items():
    recompte += 1
print recompte

data_file = []
for (up), count in recomptes.items():
    try:
        br = centres[up][0]
        dsc = centres[up][1]
    except KeyError:
        br = ''
        dsc = ''
    data_file.append([up,br,dsc,count])

file = tempFolder + 'VIHSIDA_EAPP.txt'
writeCSV(file, data_file, sep=';')