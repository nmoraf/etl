# coding: iso-8859-1
import os, os.path
from subprocess import Popen, PIPE
from datetime import datetime
from MySQLdb import escape_string
from datetime import *
from sisapUtils import *
from collections import defaultdict,Counter

# per tirar anys antics
# cal marcar als sources quins processos es volen tirar i modificar data_extraccio del fitxer de noesb
# marcar quins indicadors es volen tirar
# Rceordar de tirar sec2red afegint farmacs que es vulguin prescripcions historiques
# hi ha problema a vacunes (posar try amb continue a línia 28 de nodriza vacunes)
# posar and baixa_=0 a línia 12 de procés eqa0.py pq no calculi tots els indicadors

indicadorsCalcul = "('EQA0203A', 'EQA0203B', 'EQA0203C', 'EQA0209A', 'EQA0213A', 'EQA0313A', 'EQA0401A', 'EQA0402A')"
conceptes = ['nodrizas','eqa']

_host = sisap_['hst']
_port = str(sisap_['prt'])
_user = sisap_['usr']

printTime()

anys = list(range(2009, 2015))
for danys in anys:
    dataext = str(danys) +'-12-31'
    for concepte, folder, file, db, call, done in readCSV('../00all/components/__components.txt', sep=';'):
        if concepte in conceptes:
            path = "../" + folder + "/"
            path2 = path + "proc/source.txt"
            for file, extension, exe, delayable in readCSV(path2, sep=';'):
                os.chdir(path)
                _call = {'sql': ['mysql', db, '-u', _user, '-h', _host, '--port', _port], 'py': ['c:\\Python27\\python']}
                proceed = False
                if exe == '1':
                    proceed = True
                if proceed:
                    if file == 'nod_assignada' and extension == 'py':
                        ruta = '../96informes/21AssignadafromHistoric.py'
                    else:
                        ruta = './source/{}.{}'.format(file, extension)
                    if extension == 'py':
                        crida, info = _call[extension] + [ruta], None
                    elif extension == 'sql':
                        crida, info = _call[extension], 'source {}'.format(ruta)
                    process = Popen(crida, stdout=PIPE, stdin=PIPE, stderr=PIPE, shell=True)
                    output, errors = process.communicate(info)
                    exit = process.returncode
                    if file == 'load_noesb' and extension == 'py' and db == 'nodrizas':
                        execute("update dextraccio set data_ext = '{}'".format(dataext), db)
                    if file == 'load_noesb' and extension == 'py' and db == 'eqa_ind':
                        execute("update eqa_ind set baixa_ = 1 where ind_codi not in {}".format(indicadorsCalcul), db)
                    if file == 'nod_assignada' and extension == 'py':
                        execute("update assignada_tot set data_naix = '1900-01-01' where data_naix=0", db)

    OutFileF = tempFolder + 'EQA_Historic_' + dataext + '.txt'
    taula = "eqa_ind.exp_khalix_up_ind"
    centres="export.khx_centres"
    with open(OutFileF,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        query="select indicador,ics_codi,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres':centres}
        for indicador, centre, conc, edat, comb, sexe, n, valor in getAll(query, 'eqa_ind'):
            w.writerow([indicador, centre, conc, edat, comb, sexe, n, valor])
    
    # grip
    grip = {}
    sql = "select id_cip_sec from nodrizas.eqa_vacunes a where a.agrupador=99"
    for id_cip, in getAll(sql, 'eqa_ind'):
        grip[id_cip] = True
    indicadorGrip = Counter()
    sql = "select id_cip_sec, up, edat, sexe, if(ates=1, 'ates','no ates'), if(institucionalitzat=1 or maca=1, 'insti','no insti') from assignada_tot"
    for id,up,edat,sexe,pob,comb in getAll(sql, 'nodrizas'):
        if edat > 59:
            if id in grip:
                num = 1
            else:
                num = 0 
            indicadorGrip['EQA0501A', up, ageConverter(edat, 5),sexConverter(sexe),pob, comb, 'den'] += 1
            indicadorGrip['EQA0501A', up, ageConverter(edat, 5),sexConverter(sexe),pob, comb, 'num'] += num
    OutFileG = tempFolder + 'EQA_Historic_GRIP_' + dataext + '.txt'    
    with open(OutFileG,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        for (indicador, up, edat,sexe,pob, comb, tip), d in indicadorGrip.items():
            w.writerow([indicador, up, pob, edat, comb, sexe, 'N', tip, d])
        
printTime()               
