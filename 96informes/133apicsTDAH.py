# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

tdah = {}

sql = "select id_cip_sec\
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) and pr_cod_ps in ('F90','F90.0','F90.1','F90.8','F90.9')"
for id, in getAll(sql, 'import'):
    tdah[(id)] = True

centres = {}

sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, 'nodrizas'):
    centres[up] = True
    
recomptes = Counter()
nascuts = Counter()
sql = 'select id_cip_sec, month(data_naix), up from ped_assignada'
for id, mes, up in getAll(sql, 'nodrizas'):
    if up in centres:
        nascuts[mes] += 1
        if (id) in tdah:
            recomptes[mes] += 1
            
for mes, d in recomptes.items():
    print mes, d
    
for mes, d in nascuts.items():
    print mes, d
        