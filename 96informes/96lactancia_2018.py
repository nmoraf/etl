# coding: iso-8859-1

# Modificació del codi 96lactancia per ampliar lactants fins a 2018
# - 20201014 Actualització fins 20191231

from sisapUtils import *
from collections import defaultdict, Counter

#Llista de codis de lactància (variables agrupador 285 de lactància)
codis_nen11=set('PEPAL01')
codis_nen12=set(['PENDE051','PENDE052'])

sql = "select scs_codi, ics_desc, sap_desc, amb_desc \
       from cat_centres \
       where ep='0208'"
centros = {}
for up, ics_desc, sap_desc, amb_desc in getAll(sql, 'nodrizas'):
    centros[up] = {'ics': ics_desc, 'sap': sap_desc, 'ambit':amb_desc}

sql= 'select id_cip_sec, val_val, val_var \
      from import.nen11'
valormax = {}
i=0
for id, valor, var in getAll(sql, 'nodrizas'):
    if var in codis_nen11:
        i+=1
        if(i%100000==0):
            print(i)
        valor = int(float(valor.replace(',', '.')))
        if valor > 24:
            valor = 24
        if id in valormax:
            valor2 = valormax[id]
            if valor > valor2:
                valormax[id] = valor
        else:
            valormax[id] = valor

i=0
sql= 'select id_cip_sec, val_val, val_var \
      from import.nen12'
for id, valor, var in getAll(sql, 'nodrizas'):
    if var in codis_nen12:
        i+=1
        if(i%100000==0):
            print(i)
        valor = int(float(valor.replace(',', '.')))
        if valor > 24:
            valor = 24
        if id in valormax:
            valor2 = valormax[id]
            if valor > valor2:
                valormax[id] = valor
        else:
            valormax[id] = valor
        
mesosLM = Counter()
sql = "select id_cip_sec, year(data_naix) as any_naix, up \
       from nodrizas.ped_assignada \
       where data_naix between '2018/01/01' and '2019/12/31'"
for id, anys, up in getAll(sql, 'nodrizas'):
    if up in centros:
        try:
            valor = valormax[id]
        except KeyError:
            valor = None
        mesosLM[(anys, valor)] += 1
        

upload = []
for (anys, valor), d in mesosLM.items():
    upload.append([anys, valor, d])

file = tempFolder + 'lm_mes.csv'
writeCSV(file, 
        [('any','lm_mes','n')] + sorted(upload),
        sep=';')
