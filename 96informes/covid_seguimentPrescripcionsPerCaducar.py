# coding: iso-8859-1

"""

--- ACTUALITZACIONS:
    --- 2020/11/06 Alertes són dades incorrectes, està pendent d'un requeriment
        a l'ECAP: UTILITZAR PRESCRIPCIONS
    --- 2020/11/13 Alertes són dades correctes: UTILITZAR ALERTES
-------------------------------------------------------------------------------

--- ALERTES
    Les alertes relacionades amb la caducitat de les prescripcions està
    associat al tipus alv_tipus = 'RES'

    --- SECTORS: Dades: PRSTB550 / Catàleg: prstb551

        --- PRSTB550
            Dades d'alertes
            select * from prstb550;

            Filtres necessari per complir criteris:
                - talv_tipus = 'RES' : Alertes de Recepta Electrònica
                - talv_aub_up != NA: Alertes con UP enregistrada

        --- PRSTB551
            Catàleg tipus d'alerta
                - talv_tipus: RES
                - talv_desc: Recepta electrònica setmanal
                - talv_text1: Número de receptes
                - talv_text2: Data de caducitat mes proxima
                - talv_text3: Nom usuari que revisa
                - talv_text4: Dies que falten avui fins caducitat

    --- IMPORT: Dades: ALERTES / Catàleg: NO
        --- ALERTES
            select * from import.alertes where alv_tipus='RES';
            select alv_data_al, count(1) from import.alertes
            where alv_tipus='RES' group by alv_data_al;
            select alv_tipus, count(1) from import.alertes group by alv_tipus;

    --- Catàleg: prstb551 (sectors)

--- DUBTES
    - Quins centres incorporar?, .. ICS?, .. Tots?
    - NUM > DEN: en denominadores muy pequeños: los elimino

--- NOTES
    - Hi ha nens a Alertes, s'excloen les Línees pediatrìques i els
      pacients < 14 anys

--- MATERIAL
    - Scrip python eqpf para den poblacional: 08alt/source/eqpf_p.py

"""


import sisapUtils as u
import datetime as d
from datetime import datetime
from dateutil.relativedelta import relativedelta
import collections as c
import hashlib as h
from os import remove

GAPPOST10 = relativedelta(days=10)
GAPPOST90 = relativedelta(days=90)
TODAY = d.datetime.now().date()
print(TODAY)
print(TODAY + GAPPOST10)
print(TODAY + GAPPOST90)


class PrescripcionsPerCaducar(object):
    def __init__(self):
        """."""
        self.get_centres()
        self.get_assignada()

        clock_in = d.datetime.now()
        self.get_u11()
        clock_out = d.datetime.now()
        print('''U11: {}'''.format(clock_out - clock_in))

        clock_in = d.datetime.now()
        self.get_pdptb101()
        clock_out = d.datetime.now()
        print('''PDPTB101: {}'''.format(clock_out - clock_in))

        clock_in = d.datetime.now()
        self.get_prstb550()
        clock_out = d.datetime.now()
        print('''ALERTES (PRSTB550): {}'''.format(clock_out - clock_in))

        # clock_in = d.datetime.now()
        # self.get_presc()
        # clock_out = d.datetime.now()
        # print('''Presc: {}'''.format (clock_out - clock_in))

        self.get_upload()
        self.export_upload()

    def get_centres(self):
        """."""
        self.centres = {}
        select = """select amb_desc, sap_desc, scs_codi, ics_desc
                    from cat_centres"""
        db = "nodrizas"
        for amb_desc, sap_desc, up, up_desc in u.getAll(select, db):
            self.centres[up] = {'amb_desc': amb_desc,
                                'sap_desc': sap_desc,
                                'up_desc': up_desc}
        # print("N registres CAT_CENTRES:", len(self.centres))

    def get_assignada(self):
        """."""
        print("------------------------------------------------ get_assignada")
        self.assignada = {}
        self.den = c.Counter()
        select = "select id_cip_sec, up, uba \
                  from assignada_tot \
                  where edat >= 15"
        db = "nodrizas"
        for id_cip_sec, up, uba in u.getAll(select, db):
            self.assignada[id_cip_sec] = {'up': up, 'uba': uba}
            self.den[(up, uba)] += 1
        print("N registres ASSIGNADA:", len(self.assignada))
        print("N UP/UBA ASSIGNADA:", len(self.den))

    def get_u11(self):
        """."""
        print("------------------------------------------------------ get_u11")
        self.u11 = {}
        db = "import"
        tb = "u11"
        sql = "select id_cip_sec, hash_d, codi_sector \
               from {}.{}".format(db, tb)
        for id_cip_sec, hash_d, _sector in u.getAll(sql, db):
            if id_cip_sec in self.assignada:
                self.u11[hash_d] = {"id_cip_sec": id_cip_sec}
        print("N de registros 'filtrados' U11 (ID_CIP_SEC): ", len(self.u11))

    def get_pdptb101(self):
        """ . """
        print("------------------------------------------------- get_pdptb101")
        self.pdptb101 = {}
        db = 'pdp'
        sql = "select usua_cip, usua_cip_cod \
               from pdptb101"
        db = "pdp"
        for cip, hash in u.getAll(sql, db):
            self.pdptb101[cip] = {'hash': hash}
        print("N registros PDPTB101: CIP <-> HASH", len(self.pdptb101))

    def get_prstb550(self):
        """
        Existe una tabla de Alertes en import: alertes pero no tiene las
        columnas que necesitamos: alv_uab_codi_uab, alv_text1, alv_text2,
        alv_text4. Además se necesita la información semanalmente y a fecha de
        hoy (2020/11) no se tira SISAP cada semana por problemas en redICS >>
        Se ataca a la tabla prstb550 de SECTORES
        """
        print("------------------------------------------------- get_prstb550")
        self.alertes = []
        self.alertes_pax = {}
        self.up = c.Counter()
        self.num = c.Counter()
        select = """select alv_cip, alv_data_al, alv_uab_up, alv_uab_codi_uab,
                           alv_text1, alv_text2, alv_text4
                    from prstb550
                    where alv_tipus='RES'"""
        for sector in u.sectors:
            bd = sector
            print(bd)
            # if sector not in ('6523'):
            for cip13, data, up, uba, nreceptes, data_cad, ndies_cad in u.getAll(select, bd): # noqa
                self.data = data.date()
                hash_covid = h.sha1(cip13).hexdigest().upper()
                data_cad = d.datetime.strptime(data_cad, '%d/%m/%Y').date()
                self.up[(up)] += 1
                if up in self.centres:
                    if cip13 in self.pdptb101:
                        hash = self.pdptb101[cip13]['hash']
                        if hash in self.u11:
                            id_cip_sec = self.u11[hash]['id_cip_sec']
                            if id_cip_sec in self.assignada:
                                self.alertes.append((hash_covid, sector, data, up, uba, nreceptes, data_cad, ndies_cad))  # noqa
                                self.alertes_pax[hash_covid] = True
                                self.num[(up, uba, 'Totes')] += 1
                                if TODAY < data_cad <= TODAY + GAPPOST10:
                                    self.num[(up, uba, 'Caduca10d')] += 1
                                elif TODAY + GAPPOST10 < data_cad <= TODAY + GAPPOST90:  # noqa
                                    self.num[(up, uba, 'Caduca90d')] += 1

        print("N registres ALERTES:", len(self.alertes))
        print("N pacients ALERTES:", len(self.alertes_pax))

    def get_presc(self):
        """ Existen prescipcions repetidas por sector: seleccionar
            el paciente de assignada_tot (id_cip_sec) y filtrar prescripción
            por ese id_cip_sec """
        print("---------------------------------------------------- get_presc")
        self.presc = {}
        self.up = c.Counter()
        self.num = c.Counter()
        select = """select id_cip_sec, ppfmc_atccodi, ppfmc_data_fi,
                           ppfmc_durada, ppfmc_pmc_amb_cod_up,
                           ppfmc_pmc_amb_num_col
                    from tractaments
                    where (ppfmc_durada = 365 or ppfmc_durada = 0)"""
        db = "import"
        for id_cip_sec, _atc, data_fi, _durada, _up, _col in u.getAll(select, db):  # noqa
            if id_cip_sec in self.assignada:
                if TODAY < data_fi:
                    self.presc[id_cip_sec] = {'dat': data_fi}
        for id_cip_sec in self.presc:
            up = self.assignada[id_cip_sec]['up']
            uba = self.assignada[id_cip_sec]['uba']
            dat = self.presc[id_cip_sec]['dat']
            if TODAY < dat <= TODAY + GAPPOST10:
                self.num[(up, uba, 'Caduca10d')] += 1
            elif TODAY + GAPPOST10 < dat <= TODAY + GAPPOST90:
                self.num[(up, uba, 'Caduca90d')] += 1

    def get_upload(self):
        """."""
        print("--------------------------------------------------- get_upload")
        self.upload = []
        for (up, uba), den in self.den.items():
            for ind in ('Caduca10d', 'Caduca90d'):
                num = self.num[(up, uba, ind)]
                sap_desc = self.centres[up]['sap_desc'] if up in self.centres else None  # noqa
                amb_desc = self.centres[up]['amb_desc'] if up in self.centres else None  # noqa
                up_desc = self.centres[up]['up_desc'] if up in self.centres else None  # noqa
                tax = format((float(num)/float(den))*10000, '.2f')
                tax = str(tax).replace('.', ',')
                # if num <= den:
                if den >= 50:
                    self.upload.append((self.data, amb_desc, sap_desc, up, up_desc, uba, ind, num, den, tax))  # noqa
        print("N registres UPLOAD", len(self.upload))

    def export_upload(self):
        """."""
        print("------------------------------------------------ export_upload")
        file = u.tempFolder + 'PrescripcionsPerCaducar_Alertes_{}.csv'
        file = file.format(TODAY.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('data',
                     'amb_desc', 'sap_desc',
                     'up', 'up_desc', 'uba',
                     'ind', 'num', 'den', 'tax')] + self.upload,
                   sep=";")
        print("Send mail")
        subject = 'Seguiment Prescripcions per caducar'
        text = 'Arxiu amb el seguiment prescripcions per caducar'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('farmacia.ics@gencat.cat', 'rpdehesa@gencat.cat'),
                      ('cguiriguet.bnm.ics@gencat.cat','ehermosilla@idiapjgol.info'),
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == "__main__":

    clock_in = d.datetime.now()
    PrescripcionsPerCaducar()
    clock_out = d.datetime.now()
    print("--------------------------------------------------- TIME EXECUTION")
    print('''START: {}'''.format(clock_in))
    print('''END: {}'''.format(clock_out))
    print('''DELTA: {}'''.format(clock_out - clock_in))
