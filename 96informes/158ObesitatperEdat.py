# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


imc = {}

centres = {}
sql = "select scs_codi,amb_desc from cat_centres where ep='0208'"
for up, amb in getAll(sql, 'nodrizas'):
    centres[up] = amb

sql = 'select id_cip_sec,valor from eqa_variables where usar=1 and agrupador=19'
for id, valor in getAll(sql, 'nodrizas'):
    if 35 <= valor < 40:
        i = 'IMC 35-39.9'
    elif 40 <= valor < 50:
        i = 'IMC 40-49.9'
    elif 50 <= valor < 60:
        i = 'IMC 50-59.9'
    elif valor>=60:
        i = 'IMC >=60'
    else:
        i = 'IMC < 35'
    imc[id] = i
    
recomptes = Counter()       
sql = 'select id_cip_sec, edat, up from assignada_tot where edat>13 and ates=1'
for id, edat, up in getAll(sql, 'nodrizas'):
    if up in centres:
        amb = centres[up]
        if 14 <= edat <= 18:
            ed = '14-18 anys'
        elif 19 <= edat <= 65:
            ed = '19-65 anys'
        elif edat >65:
            ed = '>65 anys'
        else:
            ed = 'err'
        vimc = 'Sense registre en els últims 2 anys'
        if id in imc:
            vimc = imc[id]
        recomptes[(amb, ed, vimc)] += 1
    
upload = []
for (amb, ed, vimc), d in recomptes.items():
    upload.append([amb, ed, vimc, d])


file = tempFolder + 'Obesitat_.txt'
writeCSV(file, upload, sep='|')
    
    