# coding: iso-8859-1
#Carmen Cabezas,  (ion 3506), febrer15, Ermengol
#Demana vacunes hepatits A
from sisapUtils import *
import csv,os,sys
from time import strftime

znod="znodrizas"
zimp="zimport"
zped="zpedia"

nod="nodrizas"
imp="import"
ped="pedia"

dext="dextraccio"
vac="vacsist"
ped_vac="ped_vacunes"
a_vac="eqa_vacunes"
u11="u11"
embaras="ass_embaras"

vha="34"
dt="49"

sql='select year(data_ext) from {}'.format(dext)
for d in getOne(sql, znod):
    dara=d

any1=dara - 1
any6=dara - 6
any13=dara - 13

pob1=0
pob6=0
pob13=0

printTime('Pob')

def getPacients():

    assig= {}
    sql= 'select id_cip_sec from assignada_tot'
    for id_cip_sec, in getAll(sql,znod):
         assig[int(id_cip_sec)]=True
    return assig 

ass = getPacients()

zuhash={}
sql="select id_cip_sec,hash_d from {}".format(u11)
for id_cip_sec,hash in getAll(sql,zimp):
    id_cip_sec=int(id_cip_sec)
    zuhash[id_cip_sec]={'hash':hash}
    
    
vha1,vha6,vha13={},{},{}
sql="select id_cip_sec,year(data_naix) from {}".format(vac)
for id_cip_sec,naix in getAll(sql,zped):
    naix=int(naix)
    id_cip_sec=int(id_cip_sec)
    try:
        hashz=zuhash[id_cip_sec]['hash']
    except KeyError:
        continue
    if naix==any1:
        vha1[hashz]=True
        pob1+=1
    elif naix==any6:
        vha6[hashz]=True
        pob6+=1
    elif naix==any13:
        vha13[hashz]=True
        pob13+=1
    else:
        ok=1
        
printTime('Vacuna HA')

uhash={}
sql="select id_cip_sec,hash_d from {}".format(u11)
for id_cip_sec,hash in getAll(sql,imp):
    id_cip_sec=int(id_cip_sec)
    uhash[id_cip_sec]={'hash':hash}
    
ha1any=0
ha6any=0
ha13any=0
sql="select id_cip_sec, dosis from {0} where agrupador={1}".format(ped_vac,vha)
for id,dosi in getAll(sql,nod):
    try:
        hasha=uhash[id]['hash']
    except KeyError:
        continue
    if dosi>=1:
        try:
            if vha1[hasha]:
                ha1any+=1
        except KeyError:
            ok=1
    if dosi>=1:
        try:
            if vha6[hasha]:
                ha6any+=1
        except KeyError:
            ok=1
        try:
            if vha13[hasha]:
                ha13any+=1
        except KeyError:
            ok=1
            
print 'HA en 1 any (1 dosi):', ha1any,pob1
print 'HA en 6 anys (1 dosi):', ha6any,pob6
print 'HA en 12 anys (2 dosis):',ha13any,pob13

printTime('dt embarassades')

sql="select date_add(date_add(data_ext, interval - 25 year), interval +1 day),date_add(data_ext,interval - 12 month),data_ext from {}".format(dext)
for z,a,b in getAll(sql,znod):
    fa25anys=z
    fa1any=a
    ara=b
print fa25anys, ara
emb={}
pobe=0
sql="select id_cip_sec from {0} where inici>'{1}' or fi >'{1}'".format(embaras,fa1any)
for id, in getAll(sql,znod):
    id=int(id)
    try:
        if ass[id]:
            emb[id]=True
            pobe+=1
    except KeyError:    
        ok=1
nume=0
sql="select distinct id_cip_sec from {0} where agrupador={1} and (datamax between '{2}' and '{3}')".format(a_vac,dt,fa25anys,ara)
for id, in getAll(sql,znod):
    id=int(id)
    try:
        if emb[id]:
            nume+=1
    except KeyError:
        ok=1
print 'dT en embarassades (1 dosi cada 25 anys):', nume,pobe 

printTime('Fi proces')