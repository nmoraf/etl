from sisapUtils import tempFolder, readCSV, getAll, ageConverter, writeCSV
from collections import defaultdict
from datetime import date


year = date.today().year
file = tempFolder + 'RCA{}.csv'.format(year)  # convertit de xlsx que envia pere a enrique; compte que cal usar pestanya eap, no abs!
klx_eap = 'POBRCA_EAP_{}.TXT'.format(year)
klx_aga = 'POBRCA_AGA_{}.TXT'.format(year)

up_to_br = {up: br for (up, br) in getAll('select scs_codi, ics_codi from cat_centres', 'nodrizas')}
up_to_abs = {up: abs for (up, abs) in getAll("select abs_codi_up, abs_codi_abs from cat_rittb001 where abs_data_baixa = '47120101' and abs_nom_abs <> 'INEXISTENT'", 'import')}
abs_to_aga = {abs: 'AGA{}'.format(str(aga).zfill(5)) for (abs, aga) in getAll('select abs_cod, aga_cod from cat_sisap_agas', 'import')}

pob_eap = defaultdict(int)
pob_aga = defaultdict(int)
pob_falta = defaultdict(int)
for up, up_d, edat, sexe, sexe_d, n in readCSV(file, sep=';', header=True):
    if up != 'SES' and edat:
        n = int(n)
        if up in up_to_br:
            br = up_to_br[up]
            key = ('POBRCA', 'INI{}'.format(year), br, 'NOCLI', ageConverter(int(edat), 1), 'NOIMP', sexe_d.upper(), 'N')
            pob_eap[key] += n
        if up in up_to_abs:
            abs = up_to_abs[up]
            aga = abs_to_aga[abs]
            key = ('POBRCA', 'INI{}'.format(year), aga, 'NOCLI', ageConverter(int(edat), 1), 'NOIMP', sexe_d.upper(), 'N')
            pob_aga[key] += n
        else:
            pob_falta[up] += n

writeCSV(tempFolder + klx_eap, [key + (n,) for (key, n) in pob_eap.items()], sep='{')
writeCSV(tempFolder + klx_aga, [key + (n,) for (key, n) in pob_aga.items()], sep='{')

print pob_falta
