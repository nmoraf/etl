from sisapUtils import *
import csv
import os
import sys
from time import strftime
from collections import defaultdict, Counter
from taulesKhalix_utils import *


pathFile = tempFolder + 'centressegrip.txt'
OutFile = tempFolder + 'afegircatalegcentressegrip.txt'

nod = 'nodrizas'

NOics = {}
NOics['0186'] = 'BARCELONA'
NOics['0182'] = 'GIRONA'
NOics['0128'] = 'BARCELONA'
NOics['0561'] = 'BARCELONA'
NOics['0679'] = 'BARCELONA'


upsSegrip, regioS = {}, {}
with open(pathFile, 'rb') as fitxer:
    p = csv.reader(fitxer, delimiter='@')
    for ind in p:
        up = ind[2]
        codiRS = ind[0]
        Regio = ind[1]
        upsSegrip[up] = True
        regioS[Regio] = codiRS

upsNogrip = {}
sql = "select ep, scs_codi, ics_desc, if(amb_codi='09', 'ALT PIRINEU I ARAN', if(amb_codi='02', 'CAMP DE TARRAGONA',if(amb_codi in ('05','06'),'BARCELONA',amb_desc))) from cat_centres"
for ep, up, desc, ambdesc in getAll(sql, nod):
    try:
        upC = upsSegrip[up]
    except KeyError:
        try:
            codireg = regioS[ambdesc]
            upsNogrip[up] = {'codi': codireg, 'desc': desc, 'amb': ambdesc}
        except KeyError:
            try:
                ambdesc = NOics[ep]
                codireg = regioS[ambdesc]
                upsNogrip[up] = {'codi': codireg, 'desc': desc, 'amb': ambdesc}
            except KeyError:
                print 'Error', ep, up, desc, ambdesc
try:
    remove(OutFile)
except:
    pass


with openCSV(OutFile) as c:
    for (up), d in upsNogrip.items():
        insert = "insert into centres_ecap values('" + d['codi'] + "', '" + d['amb'] + "', '" + up + "', '" + d['desc'] + "');"
        c.writerow([insert])
