from sisapUtils import getAll, writeCSV, tempFolder
from collections import defaultdict

'''
proves = {codi: [c.lower() for c in agrupador.split('_')[1:3]] for (codi, agrupador) in getAll("select codi, agrupador from cat_dbscat where taula = 'serologies'", 'import')}

resul = {'prova': defaultdict(int), 'resultat': defaultdict(int), 'referencia': defaultdict(int)}
sql = "select id_cip_sec, cr_codi_lab, cr_codi_prova_ics, cr_res_lab, cr_ref_max_lab from laboratori, nodrizas.dextraccio \
       where cr_data_reg between date_add(data_ext, interval -1 month) and data_ext and cr_codi_prova_ics in {}".format(tuple(proves))
for id, lab, prova, resultat, referencia in getAll(sql, 'import_jail'):
    concepte = ' '.join(proves[prova])
    resultat = resultat.replace(',', '.').replace('"', '').replace('x10E', ' x10E').replace('>', '').replace('<', '').lstrip()
    try:
        resultat = float(resultat.split(' ')[0])
    except:
        pass
    else:
        resultat = 'numeric'
    resul['prova'][(lab, concepte, prova)] += 1
    resul['resultat'][(lab, concepte, resultat)] += 1
    resul['referencia'][(lab, concepte, referencia)] += 1

for taula, data in resul.items():
    writeCSV('{}{}.csv'.format(tempFolder, taula), [['laboratori', 'concepte', taula, 'recompte']] + sorted([k + (v,) for (k, v) in data.items()]), sep=';')
'''

proves = {codi: agrupador for (codi, agrupador) in getAll("select codi, agrupador from cat_dbscat where taula = 'serologies'", 'import')}
laboratoris = {codi: (descripcio, cataleg) for (codi, descripcio, cataleg) in getAll('select cla_codi, cla_desc, cla_versio_cataleg from redics.labtb111', 'redics')}
codis = {(cataleg, intern): oficial for (cataleg, intern, oficial) in getAll('select dvc_codi_versio, dvc_codi_prova, dvc_codi_oficial from redics.labtb136', 'redics')}

resul = defaultdict(int)
resul_ref = defaultdict(int)
sql = "select id_cip_sec, cr_codi_lab, cr_codi_prova_ics, cr_res_lab, cr_ref_max_lab from laboratori, nodrizas.dextraccio \
       where cr_data_reg between concat(year(data_ext), '0101') and data_ext and cr_codi_prova_ics in {}".format(tuple(proves))
for id, lab, prova, resultat, referencia in getAll(sql, 'import_jail'):
    agrupador = proves[prova]
    laboratori = laboratoris[lab][0]
    oficial = codis[(laboratoris[lab][1], prova)]
    resultat = resultat.replace(',', '.').replace('"', '').replace('x10E', ' x10E').replace('>', '').replace('<', '').lstrip()
    try:
        resultat = float(resultat.split(' ')[0])
    except:
        pass
    else:
        resultat = 'numeric'
        resul_ref[(laboratori, agrupador, oficial, referencia)] += 1
    resul[(laboratori, agrupador, oficial, resultat)] += 1

writeCSV(tempFolder + 'serologies.csv', [['laboratori', 'concepte', 'prova', 'resultat', 'recompte']] + sorted([k + (v,) for (k, v) in resul.items()]), sep=';')
writeCSV(tempFolder + 'serologies_ref.csv', [['laboratori', 'concepte', 'prova', 'referencia', 'recompte']] + sorted([k + (v,) for (k, v) in resul_ref.items()]), sep=';')
