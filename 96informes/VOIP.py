import os
import pandas as pd
from datetime import datetime
from collections import defaultdict, Counter

import sisapUtils as u
# import pprint as pp

OUTFILE = os.path.join(u.tempFolder, 'VOIP_{}.xlsx'.format(datetime.now().date()))
REDICS = 'redics'
IMP = 'import'
TASCAD = 'TASCAD'

UP_ABS_TB = 'SISAP_COVID_CAT_territori'
AGR_ABS_TB = 'SISAP_COVID_CAT_aga'
AGR_UP_TB = 'SISAP_COVID_CAT_UP'
CAT_SERVEIS_TB = 'SISAP_COVID_CAT_SERVEIS'
TASQUES_TB = 'ALERTES'
ASSIGNADA_TB = 'ASSIGNADA'

VISI_PARTS = {
    'visites_sys_p7361': '2020-08',
    'visites_sys_p6601': '2020-07',
    'visites_sys_p6901': '2020-06',
    'visites_sys_p5861': '2020-05',
    'visites_sys_p7021': '2020-04',
    'visites_sys_p6701': '2020-03',
    'visites_sys_p5541': '2020-02',
    'visites_sys_p302': '2020-01',
    'visites_sys_p6442': '2019-12',
    'visites_sys_p303': '2019-11',
    'visites_sys_p5961': '2019-10',
    'visites_sys_p5522': '2019-09',
    'visites_sys_p5002': '2019-08',
    'visites_sys_p5981': '2019-07',
    'visites_sys_p4901': '2019-06',
    'visites_sys_p5761': '2019-05',
    'visites_sys_p5741': '2019-04',
    'visites_sys_p5641': '2019-03',
    'visites_sys_p5521': '2019-02',
    'visites_sys_p5421': '2019-01',
}

VISI_SQL = """
    SELECT
        visi_up,
        visi_data_visita,
        codi_sector,
        visi_servei_codi_servei,
        count(*) as recompte
    FROM
        {}
    WHERE
        visi_tipus_visita = '9T'
        or visi_modul_codi_modul = '4CW'
        -- or s_espe_codi_especialitat = '04104'
    GROUP BY
        visi_up, visi_data_visita, codi_sector, visi_servei_codi_servei
"""

TASQUES_SQL = """
    SELECT
        id_cip_sec,
        alv_data_al
    FROM
        {}
    WHERE
        alv_tipus = '{}'
        and alv_data_al >= date '2019-01-01'
""".format(TASQUES_TB, TASCAD)

"""
petició:
--------
    De: Solans Fernandez, Oscar <osolansf>
    Enviat el: dimarts, 25 d’agost de 2020 18:31
    Per a: Medina Peralta, Manuel
    A/c: Garcia Canela, Margarita
    Tema: Explotacio dades sortints dels EAPs 
    
    Hola Manolo,
    Estem treballant amb el CTTi l’opció de incorporar telefonia IP als centres per
    tal de poder trucar des de l’ECAP i també donar espai de entrada a les centraletes
    dels centres ja que totes les trucades sortints es farien per una plataforma
    externa a la centraleta.

    Necessitaria per tant conèixer totes les trucades sortints que es realitzen des
    del centres d’AP ECAP i també agregades per regió sanitària diàriament i mensualment.

    Per tant per part de les trucades dels assitencials estaríem parlant de les 9T
    i per part de les trucades que es fa des de l’àrea administrativa no ho tenim registrat.
    He parlat amb la Marga que parlarà amb la MN i T.de l’Ebre que sembla que tenen centraletes
    que si les registren i podríem fer una estimació.

    Moltes gràcies
    
    Oscar Solans Fernández
    Responsable funcional de la Oficina eSalut
    Àrea de Sistemes d’Informació

MODIFICAN:
-------------
<2020-08-26>:
    Manolo o Leornado,
    podeu afegir el que proposa la Marga a les dades que ens heu fet arribar? Afegir les 4cw i tasques administratives en els mateixos periodes que em dement de 9T
    Moltes gràcies

    Oscar .



operativització:
----------------
    criteris utilitzats per les dades:
    - visites des de l'1/1/2019
    - visites tipus 9T o modul 4CW o especialitat 04104 (admins)
    - s'ha fet recompte per dies, mesos-any
    - visites tant realitzades com pendents
    - de totes les UP, tant EAP com no EAP
    - de totes les UP ECAP
    - hem afegit el servei, ja que potser alguns serveis interessa filtrar-los. hi ha un 11% aproximadament de visites 9T fetes per la UAU (corresponen a diferents tipus de serveis)
    - dades per up, sap, àmbit, regió i aga

fonts:
------
    visites = import.visites_p{}
    up_abs = 'SISAP_COVID_CAT_territori'
    agr_abs = 'SISAP_COVID_CAT_aga'
    agr_up = 'SISAP_COVID_CAT_UP'
    cat_serveis = 'SISAP_COVID_CAT_SERVEIS '
"""

up_abs = {up: {'abs_': abs_,
               'up_des': des.decode('latin1').encode('utf8')}
          for up, des, abs_ in
            u.getAll('select up, des, abs_c from {}'.format(UP_ABS_TB), REDICS)
          }

agr_abs = {abs_: {'aga': aga_des.decode('latin1').encode('utf8'),
                  'regio': regio_des.decode('latin1').encode('utf8')}
           for abs_, aga_des, regio_des in
            u.getAll('select abs_cod, aga_des, regio_des from {}'.format(AGR_ABS_TB), REDICS)
           }

agr_abs['NoABS'] = {k: 'NoABS' for k in ['aga', 'regio']}

agr_up = {up: {'up_des': up_des.decode('latin1').encode('utf8'),
               'sap_des':sap_des.decode('latin1').encode('utf8'),
               'amb_des': amb_des.decode('latin1').encode('utf8')}
          for up, up_des, sap_des, amb_des in
            u.getAll('select up_cod, up_des, sap_des, amb_des from {}'.format(AGR_UP_TB), REDICS)
          }

cat_serveis = {cod: des.decode('latin1').encode('utf8')
               for cod, des in
                u.getAll("select sector||'-'||cod, des from {}".format(CAT_SERVEIS_TB), REDICS)
            }
cat_serveis[TASCAD] = 'Tasques administratives'

cip_to_up = {cip: up
    for cip, up in u.getAll(
    """
    select
        id_cip_sec,
        case
            when usua_uab_up = '' then usua_up_rca
            else usua_uab_up
            end as up
    from assignada""", IMP)}

upload = defaultdict(int)
cod = servei = TASCAD
i = 0
for cip, data in u.getAll(TASQUES_SQL, IMP):
    try:
        up = cip_to_up[cip]
    except KeyError:
        i +=1
        continue
    servei_des = cat_serveis[cod] if cod in cat_serveis else ''
    year_month = data.strftime("%Y-%m")
    try:
        abs_ = up_abs[up]['abs_']
    except KeyError:
        abs_ = 'NoABS'
    regio_des = agr_abs[abs_]['regio'] if abs_ in agr_abs else 'NoABS'
    aga_des = agr_abs[abs_]['aga'] if abs_ in agr_abs else 'NoABS'
    try:
        up_des = up_abs[up]['up_des']
    except KeyError:
        try:
            up_des = agr_up[up]['up_des']
        except KeyError:
            up_des = ''
    sap_des = agr_up[up]['sap_des'] if up in agr_up else ''
    amb_des = agr_up[up]['amb_des'] if up in agr_up else ''
    row = (up, up_des, year_month, data, servei, servei_des, regio_des, aga_des, sap_des, amb_des)
    upload[row]+=1
print('cips de tasques not in assignada:', i)
counter = Counter(upload)

# upload[('00443', 'EAP Barcelona 1E - Raval Nord-Dr. Say\xc3\xa9', '2020-06', datetime(2020, 6, 2).date(), 'TASCAD', 'Tasques administratives', 'BARCELONA CIUTAT', 'Barcelona Litoral Mar', 'SAP LITORAL DE BARCELONA', 'BARCELONA')]

upload = []
for row in counter.items():
    recompte = row[1]
    l = list(row[0])
    l.append(recompte)
    upload.append(l)

for part in VISI_PARTS:
    ts = datetime.now()
    year_month = VISI_PARTS[part]
    sql = VISI_SQL.format(part)
    for up, data, sector, servei, recompte in u.getAll(sql, IMP):
        cod = '-'.join((sector, servei))
        servei_des = cat_serveis[cod] if cod in cat_serveis else ''
        try:
            abs_ = up_abs[up]['abs_']
        except KeyError:
            abs_ = 'NoABS'
        regio_des = agr_abs[abs_]['regio'] if abs_ in agr_abs else 'NoABS'
        aga_des = agr_abs[abs_]['aga'] if abs_ in agr_abs else 'NoABS'
        try:
            up_des = up_abs[up]['up_des']
        except KeyError:
            try:
                up_des = agr_up[up]['up_des']
            except KeyError:
                up_des = ''
        sap_des = agr_up[up]['sap_des'] if up in agr_up else ''
        amb_des = agr_up[up]['amb_des'] if up in agr_up else ''
        row = [up, up_des, year_month, data, servei, servei_des, regio_des, aga_des, sap_des, amb_des, recompte]
        upload.append(row)
    print(year_month, datetime.now() - ts)

labels = ['up', 'up_des', 'year_month', 'data', 'servei', 'servei_des', 'regio_des', 'aga_des', 'sap_des', 'amb_des', 'recompte']
out_df = pd.DataFrame.from_records(upload, columns = labels)

writer = pd.ExcelWriter(OUTFILE, engine='openpyxl')
out_df.to_excel(writer, index=False)
writer.save()
