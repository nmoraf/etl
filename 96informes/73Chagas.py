# coding: iso-8859-1
#Pilar Ciruela Chagas, Ermengol, Novembre 2016

from sisapUtils import *
from collections import defaultdict, Counter


#Necessitem tirar abans els processos homologacio 73seroChagas.py i 731homolChagas.py

imp = 'import'
db = 'test'
nod = 'nodrizas'

anys = list(range(2010, 2016))

seroChagas = defaultdict(list)
sql = 'select id_cip_sec, dat, val from homol_chagas'
for id, dat, val in getAll(sql, db):
    row = [dat, val]
    seroChagas[id].append(row)
    

embaras = "('O48', 'O30', 'O30.0', 'O30.1', 'O30.2', 'O30.8', 'O30.9', 'O36.9', 'Z32.1', 'Z34', 'Z34.0', 'Z34.8', 'Z34.9', 'Z35', 'Z35.0', 'Z35.1', 'Z35.2', 'Z35.3', 'Z35.4', 'Z35.5', 'Z35.6', 'Z35.7', 'Z35.8', 'Z35.9')"

paisosChagas = {}
with open('paisosChagas.txt', 'rb') as paisos:
    p=csv.reader(paisos, delimiter=';')
    for ind in p:
        paisosChagas[ind[0]] = {'pais': ind[1], 'zona': ind[2]}
        

centres = {}
sql = "select scs_codi,  ics_desc, sap_desc, amb_desc from cat_centres where ep='0208' and ics_codi like 'BR%'"
for up, desc, sap, ambit in getAll(sql, nod):
    centres[up] = {'eap': desc, 'sap':sap, 'ambit':ambit}
    
pob = {}
sql = "select id_cip_sec, data_naix, up, nacionalitat from assignada_tot"
for id, dnaix, up, nac in getAll(sql, nod):
    try:
        desc = centres[up]['eap']
        sap = centres[up]['sap']
        ambit = centres[up]['ambit']
    except KeyError:
        continue
    try:
        zona = paisosChagas[nac]['zona']
    except KeyError:
        zona = 'NC'
    if zona == 'EN' or zona == 'ES':
        pais = paisosChagas[nac]['pais']
    elif zona == 'AL':
        pais = 'Altres'
    else:
        pais = 'NC'
    pob[id] = {'up': up, 'desc': desc, 'sap': sap, 'ambit':ambit, 'dnaix': dnaix, 'pais': pais, 'zona': zona}

#tepal i dur

tepal, dur = defaultdict(list), defaultdict(list)
sql = "select id_cip_sec, if(emb_tepal in ('0000','1001','1011','0010','2002','2012','1021','3003','0020','2022'),emb_tepal, if(emb_tepal='', 'No consta','Altres')), emb_d_ini, \
        if(emb_durcorr=0,emb_dur,emb_durcorr) from embaras"
for id, embtepal, data, embdur in getAll(sql, imp):
    row1 = [data, embtepal]
    row2 = [data, embdur]
    tepal[id].append(row1)
    dur[id].append(row2)

visites = defaultdict(list)
sql = "select id_cip_sec, visi_data_visita from import.visites where visi_situacio_visita='R' and s_espe_codi_especialitat in ('10099','10098','30084','30085')"
for id, data in getAll(sql, imp):
    visites[id].append(data)

upload = []    
for ani in anys:
    datafi = str(ani) + '1231'
    dataini = str(ani) + '0101'
    print datafi, dataini
    sql = "select id_cip_sec, date_format(pr_dde,'%Y%m%d'), pr_dde,  pr_dba, date_add(pr_dde,interval + 9 month), \
    date_add(pr_dde,interval - 4 month) from problemes where pr_cod_ps in {}  and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa is null".format(embaras)
    for id, dde_str, dde,  baixa, mes9,  mes4 in getAll(sql, imp):
        if dataini <= dde_str <= datafi:
            if id in pob:
                up = pob[id]['up']
                desc = pob[id]['desc']
                sap = pob[id]['sap']
                ambit = pob[id]['ambit']
                dnaix = pob[id]['dnaix']
                pais = pob[id]['pais']
                zona = pob[id]['zona']
            else:
                continue
            edat = yearsBetween(dnaix, dde)
            mes13weeks = 1
            if baixa <> None:
                dies = daysBetween(dde, baixa)
                if 0 <= dies <= 91:
                    mes13weeks = 0
            visitada, vtpal, vdur = 0, '0', 0
            if id in visites:
                for datavis in visites[id]:
                    if dde <= datavis <= mes9:
                        visitada=1
            resultatChagas = 'No realitzat'
            if id in seroChagas:
                for row  in seroChagas[id]:
                    datasero, valor = row[0], row[1]
                    if valor == 1 and str(datasero) <= dde_str:
                        resultatChagas = 'Positiu'
                    if str(mes4) <= str(datasero) <= str(mes9):
                        if valor == 1:
                            resultatChagas = 'Positiu'
                        elif valor == 0:
                            resultatChagas = 'Negatiu'
                        elif valor == 9:
                            resultatChagas = 'No valorable'
            resultatTEPAL = 'No consta'
            resultatDUR = 0
            if id in tepal:
                for row in tepal[id]:
                    datatepal, valortepal = row[0], row[1]
                    if str(mes4) <= str(datatepal) <= str(mes9):
                        resultatTEPAL = valortepal
            if id in dur:
                for row in dur[id]:
                    datadur, valordur = row[0], row[1]
                    if str(mes4) <= str(datadur) <= str(mes9):
                        resultatDUR = 1
            upload.append([id, up, desc, sap, ambit, edat, pais, zona, visitada, mes13weeks, resultatTEPAL, resultatDUR, resultatChagas, ani])

table = 'peticioChagas'    
      
createTable(table, '(id_cip_sec int, up varchar(5), descripcio varchar(150), sap varchar(150), ambit varchar(150), edat double, pais varchar(150), zona varchar(150), visitada double, \
mes13weeks double, tepal varchar(10), dur double, chagas varchar(150), anys double)', db, rm=True)
listToTable(upload, table, db)

nchagas = Counter()
sql = 'select id_cip_sec, up, descripcio, sap, ambit, edat, pais, zona, visitada, mes13weeks, tepal, dur, chagas, anys from {}'.format(table)
for id, up, desc, sap, amb, edat, pais, zona, visitada, mes13, tepal, dur, chagas, anys in getAll(sql, db):
    nchagas[(up, desc, sap, amb, int(edat), pais, zona, int(visitada), int(mes13), tepal, int(dur), chagas, int(anys))] += 1

upload = []
for (up, desc, sap, amb, edat, pais, zona, visitada, mes13, tepal, dur, chagas, anys), d in nchagas.items():
    upload.append([up, desc, sap, amb, edat, pais, zona, visitada, mes13, tepal, dur, chagas, anys, d])

file = tempFolder + 'Chagas_2010_2015.txt'
writeCSV(file, upload, sep=';')

