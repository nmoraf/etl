import sisapUtils as u
from datetime import datetime as d
from dateutil import relativedelta as rd
import collections as c
import numpy as np
import statistics as st
import re
import os, sys
# import pandas as pd

PAQUI, LEO = "PREDUPRP","PREDULMB"
WATCHERS = ("PREDULMB","PREDUPRP","PREDUMMP")
DB_DESTI = 'redics'

def birthday(date):
    # Get the current date
    now = d.utcnow().date()
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(now, date)
    age = age.years
    return age

tb_desti = 'SISAP_CAT_GRUPSUSUARIS'
cols = ['sector_cod varchar2(4)',
        'grup_cod varchar2(8)',
        'grup_des VARCHAR2(30)',
        'grup_up varchar2(5)',
        'grup_n NUMBER',
        'grup_edat NUMBER',
        'eap1 varchar2(5)',
        'eap1_n NUMBER',
        'eaps NUMBER',
        ]

sql = """
    select
      '{}',
      cip_usuari_cip,
      ug_codi_grup||'',
      gu_up_residencia,
      gu_desc,
      to_date(usua_data_naixement, 'J'),
      usua_uab_up
    from ppftb012,
      ppftb011,
      usutb011,
      usutb040
    where
      ug_usua_cip = cip_cip_anterior
      and ug_codi_grup = gu_codi_grup
      and usua_cip = cip_usuari_cip
      and ug_dba is null
      and gu_dba is null
    """

grups = c.defaultdict(list)
for sector in u.sectors:
    print(sector)
    for sec, cip, grup, grup_up, grup_des, dnaix, eap in u.getAll(sql.format(sector), sector + 'sb'):
        edat = birthday(dnaix)
        grups[(sec, grup, grup_des, grup_up), 'edats'].append(edat)
        grups[(sec, grup, grup_des, grup_up), 'eaps'].append(eap)

grups_agr = c.defaultdict(dict)
for k in grups:
    if k[1] == 'edats':
      grups_agr[k[0]]["grup_n"] =len(grups[k])
      grups_agr[k[0]]["grup_edat"] = int(np.mean(grups[k]))
    elif k[1] == 'eaps':
      grups_agr[k[0]]["eaps"] = len(set(grups[k]))
      try:
        grups_agr[k[0]]["eap1"] = eap1 = st.mode(grups[k])
        grups_agr[k[0]]["eap1_n"] = len([eap for eap in grups[k] if eap == eap1])
      except st.StatisticsError:
        grups_agr[k[0]]["eap1"] = None
        grups_agr[k[0]]["eap1_n"] = None
        continue

upload = []
for k, v in grups_agr.items():
    sec, grup, grup_des, grup_up = k[0], k[1], k[2], k[3]
    grup_n = v["grup_n"]
    grup_edat = v["grup_edat"]
    eap1 = v["eap1"]
    eap1_n = v["eap1_n"]
    eaps = v["eaps"]
    upload.append((sec, grup, grup_des, grup_up, grup_n, grup_edat, eap1, eap1_n, eaps))
    
print(upload[0])
print(tb_desti, DB_DESTI)
print(", ".join(cols))

u.createTable(tb_desti, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.listToTable(upload, tb_desti, DB_DESTI)
u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
sql = """
    SELECT DISTINCT
      'sec_grup' AS src_code_type
      , sector_cod||'-'||grup_cod AS src_code
      , grup_des AS src_code_desc
      , sector_cod AS src_type
    FROM
      SISAP_CAT_GRUPSUSUARIS gu
    UNION
    SELECT DISTINCT
      'grup_up' AS src_code_type
      , grup_up AS src_code
      , cup_nup AS src_code_desc
      , cup_tup AS src_type
    FROM
      SISAP_CAT_GRUPSUSUARIS gu
      LEFT JOIN REPTBCUP cup ON gu.grup_up = cup.cup_cup
    WHERE
      grup_up IS NOT NULL
    UNION
    SELECT DISTINCT
      'rup_up' AS src_code_type
      , cup_cup AS src_code
      , cup_nup AS src_code_desc
      , cup_tup AS src_type
    FROM
      REPTBCUP cup
    WHERE
      cup_tup IN ('7071', '7075', '7076')
"""

upload = []
for row in u.getAll(sql, 'redics'):
  upload.append(row)

cols = [
  'src_code_type VARCHAR(10)',
  'src_code  VARCHAR(13)',
  'src_code_desc varchar(50)',
  'src_type varchar(10)'
  ]

u.createTable('SISAP_CAT_GRUP_SRC_CODES',"({})".format(", ".join(cols)), 'redics', rm=True)
u.listToTable(upload, 'SISAP_CAT_GRUP_SRC_CODES', 'redics')
u.grantSelect("SISAP_CAT_GRUP_SRC_CODES", WATCHERS, 'redics')

pathname = os.path.dirname(sys.argv[0])
print(pathname)

UPSERT_GRUPS = 'upsert_SISAP_MAP_GRUP_RUP.txt'
UPSERT_GRUPS_FSN = os.path.join(pathname, UPSERT_GRUPS)

i = 0
for row in u.readCSV(UPSERT_GRUPS_FSN, sep='|'):
  i +=1
  row = [re.sub(r"\s", "", field) for field in row]
  sec, grup = row[0], row[1]
  print i, tuple(row)
  u.execute("delete from {} where sec = '{}' and grup = '{}'".format('SISAP_MAP_GRUP_RUP', sec, grup), 'redics')
  u.execute("insert into {} {} values {}".format('SISAP_MAP_GRUP_RUP','(sec, grup, sisap_class, src_code_type, src_code, target_code_type, target_code)', tuple(row) ), 'redics')

u.createTable("SISAP_MAP_RESIDENCIES", " as select * from SISAP_VW_RESIDENCIES", "redics", rm=True)
u.grantSelect("SISAP_MAP_RESIDENCIES", WATCHERS, "redics")
