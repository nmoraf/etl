# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import itertools as i

import sisapUtils as u


DEBUG = False
FILE = u.tempFolder + "ITS_VISITES.CSV"
PERIODE = (2018, 2019)

GRUPS = {
    647: {'desc': "S�filis preco�", 'cim10':set()},
    649: {'desc': "S�filis inespec�fica", 'cim10':set()},
    651: {'desc': "infecci� gonoc�ccica", 'cim10':set()},
    653: {'desc': "Clam�dia", 'cim10':set()},
    666: {'desc': "VIH", 'cim10':set()},
    667: {'desc': "SIDA", 'cim10':set()},
    668: {'desc': "Tricomones", 'cim10':set()},
    13:  {'desc': "Hepatitis B", 'cim10':set()},
    495: {'desc': "Condiloma acuminat", 'cim10':set()},
    669: {'desc': "Infecci� genital per herpes", 'cim10':set()},
    670: {'desc': "Contacte sexual VIH", 'cim10':set()},
    671: {'desc': "Contacte sexual ITS", 'cim10':set()},
    # 406: {'desc': "ITS inespec�fica", 'cim10':set()},
    879: {'desc': "Linfogranuloma veneri", 'cim10':set()},
    881: {'desc': "Uretritis", 'cim10':set()}
}

sql_base = '''select agrupador, criteri_codi from eqa_criteris where agrupador in {}'''

sql = sql_base.format(tuple(set([desc for desc in GRUPS])))
for k, v in u.getAll(sql, 'nodrizas'):
    GRUPS[k]['cim10'].add(v)

CODIS = set()
for k, v in GRUPS.items():
    for cod in v['cim10']:
        CODIS.add(cod)

"""# muchos codigos de ITS inespecifica son compartidos por otros agrupadores (VHB, Gonococcia)"""
""" Comento porque el agrupador 406 es un supraagrupador, y necesitan solo un par de codigos de los que no hay grupo """
# ITS_INESPECIFICA_BRUTA = set()
# for k, v in u.getAll(sql_base.format('(406)'), 'nodrizas'):
#     ITS_INESPECIFICA_BRUTA.add(v) 
# ITS_INESPECIFICA = ITS_INESPECIFICA_BRUTA - CODIS

ITS_INESPECIFICA  = set(['C01-A63.8','C01-A64'])

GRUPS.update({406: {'desc': "ITS inespec�fica", 'cim10': ITS_INESPECIFICA}})

INVERS = {}
for k in GRUPS.keys():
    for cod in GRUPS[k]['cim10']:
            INVERS[cod] = GRUPS[k]['desc']

# refresco el conjunto de codigos para que incluya los de ITS_INESPECIFICA
CODIS = set()
for k, v in GRUPS.items():
    for cod in v['cim10']:
        CODIS.add(cod)

AGRUPADORS = {}
for k, v in GRUPS.items():
    AGRUPADORS[v['desc']] = tuple(v['cim10'])

#
'''
AGRUPADORS = {"S�filis preco�": ("A51.0", "A51.1", "A51.2", "A51.3", "A51.4", "A51.5", "A51.9"),  # noqa
              "S�filis inespec�fica": ("A53", "A53.0", "A53.9"),
              "Gonoc�ccica": ("A54", "A54.0", "A54.1", "A54.2", "A54.3", "A54.4", "A54.5", "A54.6", "A54.8", "A54.9", "O98.2", "K67.1", "N74.3", "M73.0"),  # noqa
              "Clamydia": ("A56", "A56.0", "A56.1", "A56.2", "A56.3", "A56.4", "A56.8", "N74.4"),  # noqa
              "Tricomones": ("A59", "A59.0", "A59.8", "A59.9"),  # noqa
              "VIH": ("Z21",),
              "Hepatitis B": ("B16", "B16.0", "B16.1", "B16.2", "B16.9", "B18.0", "B18.1"),  # noqa
              "Condiloma acuminat": ("A63.0",),
              "Infecci� genital per herpes": ("A60", "A60.0", "A60.1", "A60.9"),  # noqa
              "Linfogranuloma veneri": ("A55",),
              "Uretritis inespec�fica": ("N34", "N34.0", "N34.1", "N34.2", "N34.3"),  # noqa
              "Contacte sexual VIH": ("Z20.6",),
              "Contacte sexual ITS": ("Z20.2",),
              "ITS inespec�fica": ("A64",)}
'''
TABLE = "ITS_VISITES"
DATABASE = "test"


def main():
    """."""
    cols = "(sec varchar(5), amb varchar(255), sap varchar(255), \
             cod varchar(5), des varchar(255), eap int, any int, \
             its varchar(255), vis_n int, vis_p int, seg_n int, seg_p int)"
    u.createTable(TABLE, cols, DATABASE, rm=True)
    if DEBUG:
        Sector(("6734", PERIODE[0]))
    else:
        u.multiprocess(Sector, list(i.product(u.sectors, PERIODE)))
    sql = "select amb, sap, cod, des, eap, any, its, \
                  sum(vis_n), sum(vis_p), sum(seg_n), sum(seg_p) \
           from {} \
           group by 1, 2, 3, 4, 5, 6, 7".format(TABLE)
    dades = u.getAll(sql, DATABASE)
    header = [("ambit", "sap", "codi", "descripci�", "es_eap",
               "any", "grup", "visites total", "visitats total",
               "visites grup", "visitats grup")]
    u.writeCSV(FILE, header + sorted(dades), sep=";")


class Sector(object):
    """."""

    def __init__(self, params):
        """."""
        self.sector, self.any = params
        self.get_seguiment()
        self.get_visites()
        self.get_centres()
        self.get_resultat()
        self.upload_data()
        self.seguiment, self.visites = ({}, {})

    def get_seguiment(self):
        """."""
        self.seguiment = c.defaultdict(set)
        # invers = {}
        # for agrupador, codis in AGRUPADORS.items():
        #     for codi in codis:
        #         invers[codi] = agrupador
        sql = "select sc_cip, sc_cupseg, sc_datseg, sc_coddiag \
               from prstb318 \
               where sc_datseg between date '{0}-01-01' and \
                                       date '{0}-12-31'".format(self.any)
        for id, up, dat, codis in u.getAll(sql, self.sector):
            if codis:
                for codi in codis.split("#"):
                    if codi in INVERS:
                        self.seguiment[(up, INVERS[codi])].add((id, dat))

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(set)
        periode = (u.gcal2jd(self.any, 1, 1), u.gcal2jd(self.any, 12, 31))
        sql = "select visi_usuari_cip, visi_up, visi_data_visita \
               from vistb043 \
               where visi_data_visita between {} and {} and \
                     visi_situacio_visita = 'R'".format(*periode)
        for id, up, dat in u.getAll(sql, self.sector):
            self.visites[up].add((id, dat))

    def get_centres(self):
        """."""
        sql_eaps = "select scs_codi from cat_centres"
        eaps = set([up for up, in u.getAll(sql_eaps, "nodrizas")])

        sql = '''
                select
                    c.up_codi_up_scs,
                    a.amb_desc_amb,
                    s.dap_desc_dap,
                    u.up_codi_up_ics,
                    u.up_desc_up_ics
                from
                    gcctb006 s,
                    gcctb008 c,
                    gcctb007 u,
                    gcctb005 a
                where
                    c.up_codi_up_ics = u.up_codi_up_ics
                    and u.dap_codi_dap = s.dap_codi_dap(+)
                    and s.amb_codi_amb = a.amb_codi_amb
                    and u.up_data_baixa is null
                    and u.e_p_cod_ep = '0208'
                '''
        self.centres = {cod: (amb, sap, br, des, 1 * (cod in eaps))
                        for (cod, amb, sap, br, des)
                        in u.getAll(sql, self.sector)}

    def get_resultat(self):
        """."""
        self.resultat = []
        for up, dades in self.visites.items():
            if up in self.centres:
                vis_n = len(dades)
                vis_p = len(set([id for (id, dummy_dat) in dades]))
                for agr in AGRUPADORS:
                    seg_n = len(self.seguiment[(up, agr)])
                    seg_p = len(set([id for (id, dummy_dat) in self.seguiment[(up, agr)]]))  # noqa
                    this = (self.sector,) + self.centres[up]
                    this += (self.any, agr, vis_n, vis_p, seg_n, seg_p)
                    self.resultat.append(this)

    def upload_data(self):
        """."""
        u.listToTable(self.resultat, TABLE, DATABASE)


if __name__ == "__main__":
    main()
