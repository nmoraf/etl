import sisapUtils as u
import json, datetime
import pandas as pd

imp_jail = 'import_jail'
pfc_json = 'psicofarmacs_pressons_pfc.json'

benzos = ['N03AE','N05BA','N05CD','N05CF']

class psicofarmacs(object):
    def __init__(self):
        self.get_pfcs(json_file = pfc_json)
        # print('111111' in self.pfcs)
        # print(self.pfcs['111111'])
        
        self.get_poblacio()
        # print(self.poblacio[-32021])
        
        self.get_tractaments()
        # print(tractaments)
        
        self.out_file()

    def get_pfcs(self, json_file):
        '''.'''
        with open(json_file) as json_data:
            self.pfcs = json.load(json_data)

    def get_poblacio(self):
        '''.'''
        self.poblacio = {}
        sql = '''
        select
            ingressos.*, TIMESTAMPDIFF(YEAR, a.usua_data_naixement, first_ingress) AS age_ingress_anys, a.usua_data_naixement as dnaix
        from 
            (select 
                cip
                ,min(ingres) as first_ingress -- el primer ingres
                ,left(group_concat(huab_up_codi order by dif desc),5) as up_ppal-- up principal
                ,max(dif) as up_ppal_days -- els dies que va estar en la UP que mas dies va estar (per agafar la UP principal)
                ,sum(dif) as cummulative_days -- el total de dies del pacient
                ,count(*) as distinct_ups
                ,sum(n_moviments) as n_moviments
            from 
                (select
                    id_cip_sec as cip
                    ,huab_up_codi
                    ,min(huab_data_ass) as ingres
                    ,sum(datediff(if(huab_data_final = 0, d.data_ext, huab_data_final), huab_data_ass)) as dif	-- sum(dias amb cada uba)
                    ,count(*) as n_moviments
                from
                    import_jail.moviments
                    ,nodrizas.dextraccio d
                group by
                    id_cip_sec
                    ,huab_up_codi
                ) moviments_vw
            group by
                cip
            having
                min(ingres) between date '2016-01-01' and date '2018-05-31'
                and sum(dif) > 365
            ) ingressos
        left join
            import_jail.assignada a
                on a.id_cip_sec=ingressos.cip
        '''

        for cip, first_ingres, up_ppal, up_ppal_days, cummulative_days, distinct_ups, n_moviments, age_ingress_anys, dnaix in u.getAll(sql,imp_jail):
            self.poblacio[cip] = {'edat': age_ingress_anys, 'up': up_ppal, 'first_ingres': first_ingres, 'dnaix': dnaix}

    def get_tractaments(self):
        '''.'''
        sql = '''
        select
            id_cip
            ,id_cip_sec
            ,ppfmc_pf_codi
            ,ppfmc_pmc_data_ini
            ,ppfmc_data_fi
            ,PPFMC_FREQ
            ,PPFMC_POSOLOGIA
            ,ppfmc_pmc_amb_cod_up
        from
            tractaments
        '''

        self.data = []

        for id_cip, id_cip_sec, pfc, ini, fi, freq, pos, up in u.getAll(sql,imp_jail):
            for delta in (0, 1, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 365):
                if id_cip_sec in self.poblacio and str(pfc) in self.pfcs:
                    first_ingres = self.poblacio[id_cip_sec]['first_ingres']
                    delta_date = first_ingres + datetime.timedelta(days=delta)
                    if ini <= delta_date < fi:
                        tall = delta
                        edat = (delta_date - self.poblacio[id_cip_sec]['dnaix']).days / 365
                        up = self.poblacio[id_cip_sec]['up']
                        s_pfc = str(pfc)
                        atc = self.pfcs[s_pfc]['ATC']
                        if atc == 'N07BC02':
                                atc4 = 'MTDN'
                        elif atc[:5] not in benzos:
                                atc4 = atc[:4]
                        else:
                                atc4 = 'BDZP'
                        # atc4 = atc[:4]
                        atc5 = atc[:5]
                        bdz = atc5 in benzos
                        ddd = self.pfcs[s_pfc]['DDD_MG']
                        quantitat = self.pfcs[s_pfc]['QUANTITAT_MG']
                        ddp = quantitat * pos * (24.0/freq)
                        ddd_pr = ddp / ddd
                        self.data.append([tall, id_cip_sec, edat, up, bdz, atc5, atc4, atc, pfc, pos, freq, quantitat, round(ddp, 7), ddd, round(ddd_pr,7)])
        
        print(self.data[0:5])

        labels = ['tall', 'id_cip_sec', 'edat_tall', 'up', 'bdz', 'atc5', 'atc4', 'atc', 'pfc', 'posol', 'freq', 'quantitat', 'ddp', 'ddd', 'ddd_pr']
        test_df = pd.DataFrame.from_records(self.data, columns = labels)

        '''
        def get_previs(x, field):
            previs = {
                0: {'pre30': None, 'pre60':None},
                1: {'pre30': None, 'pre60':None},
                30: {'pre30': None, 'pre60':None},
                60: {'pre30': 30, 'pre60': 0}, 
                90: {'pre30': 60, 'pre60': 30}, 
                120: {'pre30': 90, 'pre60': 60}, 
                150: {'pre30': 120, 'pre60': 90}, 
                180: {'pre30': 150, 'pre60': 120}, 
                210: {'pre30': 180, 'pre60': 150}, 
                240: {'pre30': 210, 'pre60': 180}, 
                270: {'pre30': 240, 'pre60': 210}, 
                300: {'pre30': 270, 'pre60': 240}, 
                330: {'pre30': 300, 'pre60': 270}, 
                365: {'pre30': 330, 'pre60': 300} 
                }
            return previs[x][field]

        test_df['pre30'] = [get_previs(x,'pre30') for x in test_df['tall']]
        test_df['pre60'] = [get_previs(x,'pre60') for x in test_df['tall']]

        test_df[test_df['tall'] == previs[90]['pre30']]
        '''

        writer = pd.ExcelWriter('psicof_desagregat_v{}.xlsx'.format(datetime.datetime.now().date()), engine='openpyxl')
        test_df.to_excel(writer, index=False)
        writer.save()

        # test_df2 = test_df[test_df['atc'] != 'N07BC02'].groupby(by = ['tall','id_cip_sec','up','atc4'], as_index = False).agg({'pfc':'count', 'ddd_pr':'sum', 'atc':'nunique', 'edat_tall':'min'}).rename(columns={'pfc':'pfc_count', 'ddd_pr':'ddd_pr_sum','atc':'atc_nunique', 'edat_tall':'edat_tall'})
        test_df2 = test_df.groupby(by = ['tall','id_cip_sec','up','atc4'], as_index = False).agg({'pfc':'count', 'ddd_pr':'sum', 'atc':'nunique', 'edat_tall':'min'}).rename(columns={'pfc':'pfc_count', 'ddd_pr':'ddd_pr_sum','atc':'atc_nunique', 'edat_tall':'edat_tall'})
        writer2 = pd.ExcelWriter('psicof_aggregat_v{}.xlsx'.format(datetime.datetime.now().date()), engine='openpyxl')
        print(test_df2.columns.ravel())
        # test_df2.columns = ["_".join(x) if isinstance(x,tuple) else x for x in test_df2.columns.ravel()]
        
        print(test_df2.head())
        print(test_df2[0:5])

        test_df2.to_excel(writer2, index=False)
        writer2.save()

        #bdz

        test_df3 = test_df[test_df['bdz'] == True].groupby(by = ['tall','id_cip_sec','up','bdz'], as_index = False).agg({'pfc':'count', 'ddd_pr':'sum', 'atc':'nunique', 'edat_tall':'min'}).rename(columns={'pfc':'pfc_count', 'ddd_pr':'ddd_pr_sum','atc':'atc_nunique', 'edat_tall':'edat_tall'})
        writer3 = pd.ExcelWriter('psicof_bdz_aggregat_v{}.xlsx'.format(datetime.datetime.now().date()), engine='openpyxl')
        print(test_df3.columns.ravel())
        # test_df3.columns = ["_".join(x) for x in test_df3.columns.ravel()]
        
        print(test_df3.head())
        print(test_df3[0:5])

        test_df3.to_excel(writer3, index=False)
        writer3.save()

        # test_df2 = DataFrame({'count_pfc': test_df.groupby(by = ['tall','id_cip_sec','up','atc4','atc']).count(), 'sum_dddpr': test_df.groupby(by = ['tall','id_cip_sec','up','atc4','atc']).count() })
    
        print(test_df[0:5])
        print(len(test_df))
        
        # grp_cip = test_df.groupby(by = ['id_cip_sec','tall','up','atc4'], as_index=False)

        # print(grp_cip.describe())

        # atc_count = grp_cip['atc'].nunique()
        # pfc_count = grp_cip['pfc'].nunique()
        # ddd_pr_sum = grp_cip['ddd_pr'].sum()
        # ddd_pr_mean = grp_cip['ddd_pr'].mean()
        # print(grp_cip['atc'].nunique())
        # print(pd.DataFrame(grp_cip))
        # print(grp_cip['atc', 'pfc'].count(), grp_cip['atc', 'pfc'].nunique(), grp_cip['ddp'].sum(), grp_cip['ddd_pr'].mean())
        # print(pd.crosstab(grp_cip.tall, test_df.atc4))

        # groupby id_cip_sec, tall, up, atc4, min(edat), count(*), count(distinct atc), count(distinct pfc), sum(ddp), avg(ddp), sum(ddd_pr), avg(ddd_pr)

        # trimestres:
        '''
        talls = [0, 1] + range(30, 360, 30) + [365]

        for tall in talls:
            print(tall)
            tall_ini = tall - 60
            print('tall_ini = {}'.format(tall_ini))
            if tall_ini <= -60:
                tall_ini = 0
            if tall_ini in range(-59, 1):
                tall_ini = 1
            if tall_ini == 305:
                tall_ini = 300
            tall_fi = tall + 1
            print("({} - {}]".format(tall_ini, tall_fi))
        '''

    def out_file(self):
        '''.'''
        dest_filename = "psicof_desagregat_v{}.csv".format(datetime.datetime.now().date())
        labels = ['tall', 'id_cip_sec', 'edat_tall', 'up', 'atc4', 'atc', 'pfc', 'posol', 'freq', 'quantitat', 'ddp', 'ddd', 'ddd_pr']
        self.desagregat = []
        self.desagregat.append(labels)
        self.desagregat.extend(self.data)

        u.writeCSV(file = dest_filename, data = self.desagregat, sep = ';')

if __name__ == "__main__":
    psicofarmacs()
