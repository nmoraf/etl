# -*- coding: utf8 -*-

"""
Petició JCB 30-1-17 8:17h.
"""

import collections as c

import sisapUtils as u


centres = ('00044', '00054', '00055')
year = 2016
sector = '6211'


class ECO(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_professionals()
        self.get_oc()
        self.get_diagnostics()
        self.get_proves()
        self.export_data()

    def get_centres(self):
        """Centres d'interès."""
        sql = "select scs_codi, ics_desc from cat_centres \
               where scs_codi in {}".format(centres)
        self.centres = {cod: des for cod, des in u.getAll(sql, 'nodrizas')}

    def get_professionals(self):
        """Nom dels números de col·legiat."""
        sql = 'select prov_numero_col_legiat, prov_nom, prov_cognom1, \
               prov_cognom2 from pritb031'
        self.professionals = {row[0]: ' '.join([' ' if not item else item
                                               for item in row[1:]])
                              for row in u.getAll(sql, sector)}

    def get_oc(self):
        """Captura capçalera OC."""
        sql = "select oc_numid, oc_up_ori, oc_collegiat, \
               to_char(oc_data, 'DD/MM/YYYY'), oc_motiu2 \
               from gpitb104 where oc_up_ori in {0} \
               and oc_data between to_date('{1}0101', 'YYYYMMDD') \
                and to_date('{1}1231', 'YYYYMMDD')".format(tuple(centres),
                                                           year)
        self.oc = {row[0]: (self.centres[row[1]],
                            self.professionals[row[2]],
                            row[3], row[4]) for row in u.getAll(sql, sector)}

    def get_diagnostics(self):
        """Captura diagnòstics associats."""
        cataleg = {cod: des for cod, des in u.getAll('select ps_cod, ps_des \
                                                      from cat_prstb001',
                                                     'import')}
        sql = 'select oc_numid, ppp_codi_problema \
               from sisap_oc3 partition(s{})'.format(sector)
        self.diagnostics = c.defaultdict(set)
        for id, ps in u.getAll(sql, 'redics'):
            if id in self.oc:
                self.diagnostics[id].add(cataleg[ps])

    def get_proves(self):
        """Captura detall OC."""
        sql = "select inf_numid, inf_des_prova \
               from gpitb004 where inf_des_prova like '%ECOGRAF%'"
        self.proves = set()
        for id, prova in u.getAll(sql, sector):
            if id in self.oc:
                if id in self.diagnostics:
                    dx = ' / '.join(self.diagnostics[id])
                else:
                    dx = ''
                this = self.oc[id] + (prova, dx)
                self.proves.add(this)
        self.proves = list(self.proves)

    def export_data(self):
        """Export a CSV."""
        head = [('EAP', 'Professional', 'Data', 'Motiu',
                 'Prova', 'Diagnostic')]
        u.writeCSV(u.tempFolder + 'jcb_ecos.csv', head + self.proves, sep=';')


if __name__ == '__main__':
    ECO()
