# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


imp = 'import_jail'
nod = 'nodrizas'

Neos = '(708, 730, 521, 731, 732, 733, 258, 560, 734)'

neoplasies = {}
sql = 'select id_cip_sec from eqa_problemes where ps in {} and id_cip_sec<0'.format(Neos)
for id, in getAll(sql, nod):
    neoplasies[id] = True
    
hivPS = {}
sql = 'select id_cip_sec from eqa_problemes where ps = 101  and id_cip_sec<0'
for id, in getAll(sql, nod):
    hivPS[id] = True
    
resultats = Counter()
sql = 'select id_cip_sec, up from assignada_tot_with_jail where id_cip_sec<0'
for id, up in getAll(sql, nod):
    HIV, npls = 0, 0
    if id in hivPS:
        HIV = 1
    if id in neoplasies:
        npls = 1
    resultats[(up, HIV, npls)] += 1
    
upload = []
for (up, HIV, npls), d in resultats.items():
    upload.append([up, HIV, npls, d])

file = tempFolder + 'Neos_VIH_EAPP.txt'
writeCSV(file, upload, sep=';')