# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import simplejson as j

import sisapUtils as u


DEBUG = False

PERIODES = (2017, 2018)
FILE = u.tempFolder + "ITS_LABORATORI.CSV"


class Laboratori(object):
    """."""

    def __init__(self):
        """."""
        self.get_codis()
        self.cataleg = {}
        self.get_cat_cod_des()
        self.get_cat_cod_ofi()
        self.get_cat_lab()
        self.get_cat_up()
        self.get_jobs()
        self.get_data()
        self.export()

    def get_codis(self):
        """."""
        self.codis = {}
        file = "213its_laboratori.json"
        for grup, codis in j.load(open(file)).items():
            for codi in codis:
                self.codis[codi] = grup
        self.codis_sql = tuple(self.codis.keys())

    def get_cat_cod_des(self):
        """."""
        sql = "select cod, des from ( \
                select cpi_codi_prova cod, cpi_desc_prova des from labtb112 \
                union select cpr_codi_prova cod, cpr_desc des from labtb120) a\
                where cod in {}".format(self.codis_sql)
        self.cataleg["cod_des"] = {cod: des for (cod, des)
                                   in u.getAll(sql, "redics")}

    def get_cat_cod_ofi(self):
        """."""
        sql = "select cla_codi, dvc_codi_prova, dvc_codi_oficial \
               from redics.labtb111 a, labtb136 b \
               where a.cla_versio_cataleg = dvc_codi_versio and \
                     dvc_codi_prova in {}".format(self.codis_sql)
        self.cataleg["cod_ofi"] = {(lab, cod): ofi for (lab, cod, ofi)
                                   in u.getAll(sql, "redics")}

    def get_cat_lab(self):
        """."""
        sql = "select cla_codi, cla_desc from labtb111"
        self.cataleg["lab"] = {cod: des for (cod, des)
                               in u.getAll(sql, "redics")}

    def get_cat_up(self):
        """."""
        sql = "select up.up_codi_up_scs, br.e_p_cod_ep, amb.amb_desc_amb, \
                      br.up_codi_up_ics, br.up_desc_up_ics  \
               from cat_gcctb007 br \
               inner join cat_gcctb008 up \
                          on br.up_codi_up_ics = up.up_codi_up_ics \
               left join cat_gcctb006 sap on br.dap_codi_dap=sap.dap_codi_dap \
               left join cat_gcctb005 amb on sap.amb_codi_amb=amb.amb_codi_amb\
               where br.up_data_baixa = 0"
        self.cataleg["up"] = {row[0]: row[1:] for row
                              in u.getAll(sql, "import")}

    def get_jobs(self):
        """."""
        self.jobs = [table for table in u.getSubTables("laboratori")
                     if int(table[12:16]) in PERIODES]

    def get_data(self):
        """."""
        self.data = c.Counter()
        if DEBUG:
            data = [self._worker(self.jobs[0])]
        else:
            data = u.multiprocess(self._worker, self.jobs, 12)
        for worker in data:
            for y, up, lab, prv_c, n in worker:
                if up in self.cataleg["up"]:
                    centre = self.cataleg["up"][up]
                    if centre[0] == "0208":
                        grup = self.codis[prv_c]
                        prv_d = self.cataleg["cod_des"][prv_c]
                        prv_o = self.cataleg["cod_ofi"][(lab, prv_c)]
                        lab_d = self.cataleg["lab"][lab]
                        key = centre[1:] + (lab, lab_d, y, grup, prv_c, prv_d, prv_o)  # noqa
                        self.data[key] += n

    def _worker(self, table):
        """."""
        data = c.defaultdict(set)
        periode = int(table[12:16])
        sql = "select id_cip, cr_data_reg, codi_up, \
                      cr_codi_lab, cr_codi_prova_ics \
               from {} \
               where cr_codi_prova_ics in {} and \
                     cr_codi_lab <> 'RIMAP' and \
                     codi_up <> ''".format(table, self.codis_sql)
        for id, dat, up, lab, cod in u.getAll(sql, "import"):
            data[(up, lab, cod)].add((id, dat))
        return [(periode, up, lab, cod, len(determinacions))
                for (up, lab, cod), determinacions in data.items()]

    def export(self):
        """."""
        upload = [k + (v,) for (k, v) in self.data.items()]
        u.writeCSV(FILE, sorted(upload), sep=";")


if __name__ == "__main__":
    Laboratori()
