# coding: latin1

"""
Necessita a tempFolder un Excel confidencial de RH que envia MMP, i on
després Albert afegeix columnes (compte que això duplica la columna CATEGORIA,
que cal renombrar a CATEGORIA1).
"""

import collections as c
import math as m
import pandas as pd

import sisapUtils as u


tb1 = "sisap_model_rh"
tb2 = "sisap_model_ecap"
db = "redics"
usr = ("PREDUMMP", "PREDUECR", "PREDUPRP")


class Model(object):
    """."""

    def __init__(self):
        """."""
        self.export_excel()
        self.get_centres()
        self.export_professionals()
        self.get_dades()

    def export_excel(self):
        """."""
        xls = pd.ExcelFile(u.tempFolder + "PLANTILLA mf.xlsx").parse(0)
        export = []
        for row in xls.values:
            this = []
            for field in row:
                if isinstance(field, float) and m.isnan(field):
                    field = ""
                elif isinstance(field, long):
                    field = float(field)
                elif isinstance(field, unicode):
                    field = field.encode("latin1")
                this.append(str(field))
            export.append(this)
        columns = [column.encode("latin1") + " varchar2(255)" for column in xls]
        u.createTable(tb1, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(export, tb1, db)
        u.grantSelect(tb1, usr, db)

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres where ep = '0208'"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}
        self.centresb = {br: up for (up, br) in u.getAll(sql, "nodrizas")}

    def export_professionals(self):
        """."""
        professionals = c.Counter()
        sql = "select ide_dni, up, adults, nens from cat_professionals \
               where tipus = 'M'"
        for dni, up, adults, nens in u.getAll(sql, "import"):
            nif = u.getNif(dni)
            br = self.centres.get(up)
            if br:
                professionals[(nif, br, "A")] += adults
                professionals[(nif, br, "N")] += nens
        export = []
        for (nif, br, tipus), adults in professionals.items():
            if tipus == "A":
                nens = professionals[(nif, br, "N")]
                this = (nif, br, adults, nens)
                export.append(this)
        u.createTable(tb2, "(nif varchar2(10), br varchar2(5), adults int, nens int)", db, rm=True)
        u.listToTable(export, tb2, db)
        u.grantSelect(tb2, usr, db)

    def get_dades(self):
        """Dades per RRHH"""
        prof = {}
        sql = "select nif,sum(adults), sum(nens) from {} group by nif".format(tb2)
        for nif, adult, nen in u.getAll(sql, db):
            prof[nif] = {'a': adult, 'n': nen}
        upload = []
        sql = "select nif, cognom1, cognom2, nom, tipus_concepte, br1, br2, br3,br4,br5,br6,br7,br8 from {} where ((br1 like 'BR%' or br1 like 'BN%') \
        or (br2 like 'BR%' or br2 like 'BN%') or (br3 like 'BR%' or br3 like 'BN%')  or (br4 like 'BR%' or br4 like 'BN%')  or (br5 like 'BR%' or br5 like 'BN%')\
         or (br6 like 'BR%' or br6 like 'BN%')  or (br7 like 'BR%' or br7 like 'BN%') or (br8 like 'BR%' or br8 like 'BN%') or (br_original like 'BR%' or br_original like 'BN%')) \
         and codi_categoria in ('1999.0','10117.0')".format(tb1)
        for nif, c1, c2, nom, varsisap, br1, br2, br3, br4, br5, br6, br7, br8 in u.getAll(sql, db):
            if br1 in self.centresb:
                if nif in prof:
                    if prof[nif]['n'] > 20:
                        if prof[nif]['a'] < 100:
                            etiqueta = 'MF NENS'
                        else:
                            etiqueta = 'MF MIXTE'
                    elif prof[nif]['a'] > 0:
                        etiqueta = 'MF AMB CUPO'
                else:
                    if varsisap == 'F':
                        etiqueta = 'FIX SENSE CUPO'
                    elif varsisap == 'V':
                        etiqueta = 'VARIABLE SISAP'
                    else:
                        etiqueta = 'ALTRES'
            else:
                etiqueta = 'NO EAP ICS'
    
            upload.append([nif, c1, c2, nom, prof[nif]['a'] if nif in prof else '', prof[nif]['n'] if nif in prof else '', etiqueta, br1, br2, br3, br4, br5, br6, br7, br8])
            
            file = u.tempFolder + 'mf sisap.txt'
            u.writeCSV(file, upload, sep='|')
            
if __name__ == "__main__":
    Model()
    

