#### versio del 25/07/2016 amb multiprocess i marge de 5 per a la determinacio de patologia a les transes (es a la funcio is_patologic) ###

from sisapUtils import getSubTables, multiprocess, getAll, printTime, getAll, monthsBetween, printTime, writeCSV, tempFolder
import time

### codis laboratori de les transaminases ###
codis_lab = ('Q07574', 'Q07585', 'Q02185', 'Q02166', 'Q07566', 'Q02174') 

#### codis de problema de salut hepatitis, tots o nomes dx de hepatitis viriques actives (actiu) ###
##codis_ps = ('168', '550', '553', '551', '554', '552', '555', '26', '160', '32', '33', '13', '14', '12', '29')
codis_ps = ('550', '553', '551', '554', '552', '555', '32', '13', '14', '12', '29')


### codis de serologies de hepatitis viriques, tots o els de l'estudi per protocol del ICS (actius) ###
## serologies_hepatitis =('9334','9014','9015','9016','9017','9018','9019','9020','9021','9022','9023','9024','6807','6808','8605','8606','7144','D02185','D01985','D01885','D01785','R23185','V01585','V01485','V01385','V01285','V01085','S09785','S09985','S10085','S10185','S10285','S10385','S10485','S10685','S10885','S10985','S23185','6382','6383','3738','3739','3740','3847','3848','3849','V01100','V01685','V01785','V01800','S11185','1994','1995','1996','S10785','D02085','S09885','S10585','S11085','9834')
serologies_HB = ('D01785', 'S10185') 
serologies_HC = ('S10485',)

### crec que no cal despres de pararelitzar ###
## taulalab = 'laboratori'


### aqui va la distancia entre les transaminases ###
mesos = 3



###### per posar condicions al mirar assignada ######
## whereassig = "where ((up='00276' and uba='UAB18') or (up='00108' and uba='UAB-V') or (up='00441' and uba='4T')) and edat>17"
whereassig = "where edat>14 and edat<71"

###### bases #######
imp_db = 'import'
nod_db = 'nodrizas'

###### pera a fer proves:possiblement es pot treure ######
id_prova = ''




###### funcio que treu un set amb tots els id amb dx de hepatitis a eqa_problemes ######

def get_hepatitis():
    sql = 'select id_cip_sec from eqa_problemes where ps in {}'.format(codis_ps)
    hepatitis = set([id for id, in getAll(sql, nod_db)])
    return hepatitis

    

    
###### Treu les serologies, separades per Hep C i B ######
###### Treu tambe totes les transaminases en llista de llistes [ id, data, patologica (true o false) ]   ###### 

def get_serologiesitranses(taula):
    sql = "select id_cip_sec, cr_codi_prova_ics, cr_data_reg, cr_res_lab, cr_ref_max_lab from {} where cr_codi_prova_ics in {}".format(taula, serologies_HB + serologies_HC + codis_lab)
    hep_b = set()
    hep_c = set()
    transes = []
  
    
    for id, codiprova, data, resultat, referencia in getAll(sql, imp_db):
    
        if codiprova in serologies_HB:
            hep_b.add(id)
        elif codiprova in serologies_HC:
            hep_c.add(id)
        elif codiprova in codis_lab:
                patologic = is_patologic(resultat, referencia)
                if patologic is None:
                    continue
                else:
                    transes.append([id,data,patologic])
            
    return (hep_b, hep_c, transes)


###### Arregla resultat i valor de referencia del laboratori, i torna None si no es interpretable o True o False  i el marge que hem posat de 5 respecte el valor de referencia per a no agafar poc patologics######
    
def is_patologic(resultat, referencia):
    resultat = resultat.replace(',', '.').lstrip().rstrip()
    referencia = referencia.replace(',', '.').replace('menor', '').replace(' ', '').lstrip().rstrip()
    marge=5
    try:
        resultat = float(resultat)
    except ValueError:
        patologic = None
    else:
        try:
            referencia = float(referencia)
            referencia = referencia + marge
        except ValueError:
            patologic = None
        if resultat > referencia:
            patologic = True
        else:
            patologic = False
    return patologic
    
###### agafa els pacients que compleixen les condicions de tenir les transaminases altes a partir del dicconari de {ids: dates: transa alta o no} ######

def get_patients(laboratori):
    patients = set()
    for id, dates in laboratori.items():
        if meet_criteria(dates):
            patients.add(id)
    return patients

###### mira si compleixen les condicions: ordena dates, i mira si ha pasat mes de x mesos entre la ultima data i la seguent data true (patologiques) ######
####### mirant que no hi hagi transes normals entre ellles ######


def meet_criteria(dades):
    # arriba {'data1': True/False, 'data2': True/False}
    dates = sorted(dades, reverse=True)
    
    last = dates[0]
    if dades[last] and len(dates) > 1:
        for data in dates[1:]:
            if dades[data]:
                if monthsBetween(data, last) >= mesos:
                    return True
            else:
                return False
    return False
    
    
###### per tots els ids que compleixen criteris mira si tenen les pertinents serologies de B i C fetes i construeix el numerador #####   
def get_numerador (serologies, denominador):
    numerador = set()
    for id in denominador:
        if id not in serologies:
            numerador.add(id)
        else: 
            continue
    return numerador    
    
###### creua denominador amb assignada, retorna diccionari amb el que seran files de la taula {id: [hash(per ara buit), up, uba, ates, numerador )] }   ######  

def get_assignada (whereassig, denominador, numerador):
    sql = "select id_cip_sec, up, uba, ates from assignada_tot {}".format(whereassig)
    assignada = {}       
    for id, up, uba, ates in getAll(sql, nod_db):
        if id in denominador:
            if id in numerador:
                assignada[id]=['',up,uba,ates,1]
            elif id not in numerador:
                assignada[id]=['',up,uba,ates,0]
    return assignada

###### carrefa la taula de id - hash en memoria (hashos)   ######  

def get_hash (assignada):
    sql = "select id_cip_sec, hash_d from eqa_u11"
    hashos = {}
    for id, hash_d  in getAll(sql, nod_db):
        if id in assignada: 
            hashos[id] = hash_d
    return hashos    

###### canvia el camp hash que haviem deixat buit, per el hash corresponent, i crea una llista de llistes  [ [hash(per ara buit), up, uba, ates, numerador ], ]  ######      
    
def busca_hashup(assignada, hashos):
    llista=[]
    for id in assignada:
            fila=list(assignada[id])
            fila[0]=hashos[id]
            llista.append(fila)    
    return llista

    
    
    

    
if __name__ == '__main__':
# comencem!

    printTime('inici')

# agafem els que tenen diagnostic de hepatitis    
    
    hepatitis = get_hepatitis()
    printTime('hepatitis OK')
    print 'Pacients amb hepatitis:', len(hepatitis)
    
# ara anem a agafar les serologies d'hepatitis i les transaminases amb multiprocess
    
    tables = getSubTables('laboratori')
    jobs = sorted(tables, key=tables.get, reverse=True)

    resul = multiprocess(get_serologiesitranses, jobs, 12)
    
    printTime('multiprocess acabat')

# tenim resul que son multiples llistes? de (hep_b, hep_c, transes) (de cada taula de laboratori)
# les hepatitis son sets, i les transes son una llistes  [ id, data, patologica (true o false) ] 
# procedim a juntarles: cada ovella al seu corral!
# fem la interseccio de seros de hep B i C, perque volem que tinguin les dues!

    hep_b = set()
    hep_c = set()
    transes = []
    for (b, c, d) in resul:
        hep_b = hep_b.union(b)
        hep_c = hep_c.union(c)
        transes += d
        
    serologies = hep_b.intersection(hep_c)
    
    printTime('Pacients amb serologies C i B fetes:', len(serologies))
    printTime('Transaminases no nules fetes:', len(transes))

    
# anem a ordenar dates de les transes:
# agafem la llista de transes i les posem en un diccionari amb les id: dates . Si hi ha dos dates iguals, es conta com a una, prioritzant la patologica.

    dades={}    

    for id,data,patologic in transes:
        if id  in hepatitis:
           continue
        if id not in dades:
            dades[id] = {}
        if data not in dades[id] or not dades[id][data]:
            dades[id][data] = patologic

     
    printTime('pacients amb transaminases fetes:', len(dades) )
    
# amb tot ordenadet ja podem mirar si compleix criteris de interval entre transes i que siguin positives: podem construir el denominador 

    denominador = get_patients(dades)
    printTime('pacients que compleixen criteris de transaminitis:', len(denominador) )
    
# quants no tenen fetes les serologies?  

    numerador = get_numerador(serologies, denominador)
    printTime('pacients que no tenen fetes les serologies:', len(numerador))

# creuem amb assignada, i filtrem per edat  
    assignada = get_assignada(whereassig, denominador, numerador)
    printTime('denominador de gent assignada:', len(assignada))

  
# carreguem hashos en memoria
 
    hashos = get_hash(assignada)
    printTime('tots els hashos de tothom:', len(hashos))
    
# de la taula de la ulima funcio la completem amb els hashos         

    llista = busca_hashup(assignada, hashos)
    printTime('denominador de gent assignada (taula hashos):', len(llista))   

# fem la tauleta en txt 
    file = tempFolder + 'transes_totlics.txt'
    writeCSV(file, llista)
    
    ##from os import remove
    ##remove(file)
    
    printTime('Ja hem acabat!!!')
    
    
    
    