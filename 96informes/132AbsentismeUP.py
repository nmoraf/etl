
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

taula = 'sisap_absentisme'
db = 'test'

def get_servei(servei):
    if servei == 'MG':
        serdesc = "('MG')"
    elif servei == 'PED':
        serdesc = "('PED')"
    elif servei == 'INFP':
        serdesc = "('INFP', 'INFPD')"
    elif servei == 'INFA':
        serdesc = "('INF','INFMG','ENF','INFG','ATS','GCAS')"
    
    return serdesc
  
centres = {}
con_cent = {}

sql = 'select scs_codi, medea, ics_codi from cat_centres'
for up, medea, br in getAll(sql, 'nodrizas'):
    centres[up] = medea
    con_cent[br] = up


medeaUP = {}

file = '../02nod/dades_noesb/medea_eap.txt'
for (br, mnum, tip) in readCSV(file):
    try:
        up = con_cent[br]
    except KeyError:
        continue
    medeaUP[up] = mnum

     
upload = []
serveis = ['MG', 'INFA', 'INFP', 'PED']
for servei in serveis:
    print servei
    codis = get_servei(servei)
    sql = "select codi_up, periode, temps_peti_visita, avg(edat),sum(visita_feta),count(*),sum(if(sexe='D',1,0)) from {0} where servei in {1} and data_visita <>'2017-10-03'  group by codi_up, periode,temps_peti_visita".format(taula,codis)
    for up, periode, tempsp, edat, visites, programades, dones in getAll(sql, db):
        try:
            medea = centres[up]
        except KeyError:
            medea = None
        try:
            medeaNUM = medeaUP[up]
        except KeyError:
            medeaNUM = None
        upload.append([up, periode,tempsp, medea, medeaNUM, servei, edat, visites, programades, dones])
        
file = tempFolder + 'Analisi_absentisme_up.txt'
writeCSV(file, upload, sep=';')