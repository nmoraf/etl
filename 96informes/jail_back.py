# -*- coding: utf8 -*-

"""
Quan fem sidics, no tenim accés a taula ppftb016 de presons a producció
Mentre la bústia ecap no ens dona accés, posem dades a sidics des d explotació pq no falli procés de jail... Alfileres 2.0
"""

import itertools as i

import shared_utils as s
import sisapUtils as u

SIDICS_DB = ("sidics", "x0002")

def getCipToHash():
    cipToHash = {}
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    for cip, hash in u.getAll(sql, '6951', anonim=True):
        cipToHash[cip] = hash
    return cipToHash

cipToHash = getCipToHash()    
    
upload = []
up_prova = []
sql = 'select ppfmc_pmc_usuari_cip, ppfmc_pf_codi,ppfmc_atccodi,ppfmc_pmc_data_ini,ppfmc_data_fi,ppfmc_durada, \
   ppfmc_freq,ppfmc_posologia,ppfmc_pmc_amb_cod_up,ppfmc_pmc_amb_num_col, ppfmc_pmc_codi,ppfmc_num_prod,ppfmc_apats, \
   ppfmc_periodicitat,ppfmc_codi_vell,ppfmc_prod_vell from ppftb016'
for row in u.getAll(sql, '6951'):
    hash = cipToHash[row[0]]
    upload.append([hash, '6951', row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13],row[14], row[15]])
    #up_prova.append([hash])

"""
table = 'prova'
db = 'test'
columns = "(hash varchar(40))"
#u.createTable(table, columns, db, rm=True)
u.listToTable(up_prova, table, db)
"""
table = 'ppftb016'
u.listToTable(upload, table, SIDICS_DB)
