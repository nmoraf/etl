# coding: latin1

"""
.
"""

import sisapUtils as u


TABLE = "acut_carregues"
DATABASE = "test"


class Ariadna(object):
    """."""

    def __init__(self):
        """."""
        self.get_informes()
        self.get_visites()
        self.upload_data()

    def get_informes(self):
        """."""
        sql = "select iau_id_visita, iau_ates \
               from urg_informes, nodrizas.dextraccio \
               where iau_data_imp between adddate(data_ext, interval -1 year) and data_ext"  # noqa
        self.informes = {id: cod for (id, cod) in u.getAll(sql, "import")}

    def get_visites(self):
        """."""
        sql = "show create table visites1"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        sql = """select visi_id, visi_up, visi_data_visita,
                        sec_to_time(visi_hora_visita), visi_servei_codi_servei,
                        visi_modul_codi_modul, visi_tipus_visita,
                        visi_tipus_citacio, left(visi_col_prov_resp, 1)
                 from {}, nodrizas.dextraccio
                 where visi_up in (select scs_codi from nodrizas.urg_centres) and
                       visi_data_visita between
                          adddate(data_ext, interval -1 year) and data_ext and
                       visi_situacio_visita = 'R'"""
        jobs = [(sql.format(table), "import") for table in tables]
        self.visites = []
        for visites in u.multiprocess(worker, jobs):
            for id, up, dat, hora, serv, mod, tip, cit, cat in visites:
                inf = self.informes.get(id, -1)
                if inf <= 0 and cat in ("1", "3"):
                    inf = 5 if cat == "3" else 6
                this = (up, dat, hora, dat.isoweekday(),
                        1 - u.isWorkingDay(dat), serv, mod, tip, cit, inf)
                self.visites.append(this)

    def upload_data(self):
        """."""
        cols = "(up varchar(5), data date, hora time, dia int, \
                 festiu int, servei varchar(5), modul varchar(5), \
                 tipus varchar(5), cita varchar(5), informe int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.visites, TABLE, DATABASE)


def worker(params):
    """."""
    return([row for row in u.getAll(*params)])


if __name__ == "__main__":
    Ariadna()
