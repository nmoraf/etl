# coding: latin1

"""
.
"""

import collections as c
from datetime import timedelta

import sisapUtils as u

tb = "sisap_covid_res_indicadors"
tb_cat = "sisap_covid_res_ind_cat"
db = 'redics'

cat_ind = {
    'COVRES01A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR o test rapid fets',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb tests fets',
                'mostra': 1,
                'ordre': 2},
    'COVRES02A': {
                'lit': 'Percentatge de pacients en residencia i PCR HC3 feta',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb PCR HC3 feta',
                'mostra': 1,
                'ordre': 2},
    'COVRES03A': {
                'lit': 'Mitjana de PCR per pacients amb PCR feta',
                'multi': 1,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Mitjana de PCR per pacient',
                'mostra': 1,
                'ordre': 2},
    'COVRES04A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i test rapid fet',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb test rapid',
                'mostra': 1,
                'ordre': 2},
    'COVRES05A': {
                'lit': 'Percentatge de pacients en residencia i pcr positiva',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb pcr positiva',
                'mostra': 1,
                'ordre': 2},
    'COVRES06A': {
                'lit': 'Percentatge de pacients en residencia i pcr positiva',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pcr positiva sobre pacients amb pcr',
                'mostra': 1,
                'ordre': 2},
    'COVRES07A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i visitats en els ultims 7 dies',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge visitats en ultims 7 dies',
                'mostra': 1,
                'ordre': 2},
    'COVRES08A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i test de Barthel realitzat',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb Barthel',
                'mostra': 1,
                'ordre': 2},
    'COVRES09A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i test de Pfeiffer realitzat',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb Pfeiffer',
                'mostra': 1,
                'ordre': 2},
    'COVRES10A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR HC3 i IA feta',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients amb PCR feta',
                'mostra': 1,
                'ordre': 2},
    'COVRES30A': {
                'lit': 'Percentatge de pacients PCC',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients PCC',
                'mostra': 1,
                'ordre': 2},
    'COVRES31A': {
                'lit': 'Percentatge de pacients MACA',
                'multi': 100,
                'grup': 'Poblacio en residencia',
                'lit_curt': 'Percentatge pacients MACA',
                'mostra': 1,
                'ordre': 2},
    'COVRES01B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR o test rapid fets en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb tests fets',
                'mostra': 1,
                'ordre': 1},
    'COVRES02B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR HC3 feta en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb PCR HC3 feta',
                'mostra': 1,
                'ordre': 1},
    'COVRES03B': {
                'lit': 'Mitjana de PCR per pacients '
                       + 'amb PCR feta en pacients exposats',
                'multi': 1,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Mitjana de PCR per pacient',
                'mostra': 1,
                'ordre': 1},
    'COVRES04B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i test rapid fet en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb test rapid',
                'mostra': 1,
                'ordre': 1},
    'COVRES05B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i pcr positiva en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb pcr positiva',
                'mostra': 1,
                'ordre': 1},
    'COVRES06B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i pcr positiva en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pcr positiva sobre pacients amb pcr',
                'mostra': 1,
                'ordre': 1},
    'COVRES07B': {
                'lit': 'Percentatge de pacients en residencia i visitats '
                       + 'en els ultims 7 dies en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge visitats en ultims 7 dies',
                'mostra': 1,
                'ordre': 1},
    'COVRES08B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i test de Barthel realitzat en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb Barthel',
                'mostra': 1,
                'ordre': 1},
    'COVRES09B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i test de Pfeiffer realitzat en pacients exposats',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb Pfeiffer',
                'mostra': 1,
                'ordre': 1},
    'COVRES10B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR HC3 i IA feta',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients amb PCR feta',
                'mostra': 1,
                'ordre': 1},
    'COVRES30B': {
                'lit': 'Percentatge de pacients PCC',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients PCC',
                'mostra': 1,
                'ordre': 1},
    'COVRES31B': {
                'lit': 'Percentatge de pacients MACA',
                'multi': 100,
                'grup': 'Poblacio exposada',
                'lit_curt': 'Percentatge pacients MACA',
                'mostra': 1,
                'ordre': 1},
    'COVRES11A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i PCR o test rapid fets',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb tests fets',
                'mostra': 0,
                'ordre': 3},
    'COVRES12A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i PCR feta',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb PCR HC3 feta',
                'mostra': 0,
                'ordre': 3},
    'COVRES13A': {
                'lit': 'Mitjana de PCR per pacients no actuals amb PCR feta',
                'multi': 1,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Mitjana de PCR per pacient',
                'mostra': 0,
                'ordre': 3},
    'COVRES14A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i test rapid fet',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb test rapid',
                'mostra': 0,
                'ordre': 3},
    'COVRES15A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i pcr positiva',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb pcr positiva',
                'mostra': 0,
                'ordre': 3},
    'COVRES16A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i pcr positiva',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pcr positiva sobre pacients amb pcr',
                'mostra': 0,
                'ordre': 3},
    'COVRES17A': {
                'lit': 'Percentatge de pacients en residencia no actuals i '
                       + 'visitats en els ultims 7 dies',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge visitats en ultims 7 dies',
                'mostra': 0,
                'ordre': 3},
    'COVRES18A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i test de Barthel realitzat',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb Barthel',
                'mostra': 0,
                'ordre': 3},
    'COVRES19A': {
                'lit': 'Percentatge de pacients en residencia no actuals '
                       + 'i test de Pfeiffer realitzat',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb Pfeiffer',
                'mostra': 0,
                'ordre': 3},
    'COVRES20A': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR HC3 i IA feta',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients amb PCR feta',
                'mostra': 0,
                'ordre': 3},
    'COVRES40A': {
                'lit': 'Percentatge de pacients PCC',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients PCC',
                'mostra': 0,
                'ordre': 3},
    'COVRES41A': {
                'lit': 'Percentatge de pacients MACA',
                'multi': 100,
                'grup': 'Poblacio inactiva en residencia',
                'lit_curt': 'Percentatge pacients MACA',
                'mostra': 0,
                'ordre': 3},
    'COVRES11B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR o test rapid fets '
                       + 'en pacients exposats no actuals',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb tests fets',
                'mostra': 0,
                'ordre': 4},
    'COVRES12B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR feta en pacients exposats no actuals',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb PCR HC3 feta',
                'mostra': 0,
                'ordre': 4},
    'COVRES13B': {
                'lit': 'Mitjana de PCR per pacients amb PCR feta '
                       + 'en pacients exposats no actuals',
                'multi': 1,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Mitjana de PCR per pacient',
                'mostra': 0,
                'ordre': 4},
    'COVRES14B': {
                'lit': 'Percentatge de pacients en residencia i test rapid fet'
                       + ' en pacients exposats no actuals',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb test rapid',
                'mostra': 0,
                'ordre': 4},
    'COVRES15B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i pcr positiva en pacients exposats no actuals',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb pcr positiva',
                'mostra': 0,
                'ordre': 4},
    'COVRES16B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i pcr positiva en pacients exposats no actuals',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pcr positiva sobre pacients amb pcr',
                'mostra': 0,
                'ordre': 4},
    'COVRES17B': {
                'lit': 'Percentatge de pacients pacients exposats '
                       + 'no actuals i visitats en els ultims 7 dies',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge visitats en ultims 7 dies',
                'mostra': 0,
                'ordre': 4},
    'COVRES18B': {
                'lit': 'Percentatge de pacients exposats no actuals '
                       + 'i test de Barthel realitzat',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb Barthel',
                'mostra': 0,
                'ordre': 4},
    'COVRES19B': {
                'lit': 'Percentatge de pacients exposats no actuals '
                       + 'i test de Pfeiffer realitzat',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb Pfeiffer',
                'mostra': 0,
                'ordre': 4},
    'COVRES20B': {
                'lit': 'Percentatge de pacients en residencia '
                       + 'i PCR HC3 i IA feta',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients amb PCR feta',
                'mostra': 0,
                'ordre': 4},
    'COVRES40B': {
                'lit': 'Percentatge de pacients PCC',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients PCC',
                'mostra': 0,
                'ordre': 4},
    'COVRES41B': {
                'lit': 'Percentatge de pacients MACA',
                'multi': 100,
                'grup': 'Poblacio inactiva exposada',
                'lit_curt': 'Percentatge pacients MACA',
                'mostra': 0,
                'ordre': 4},
    }


class Coronavirus_ind(object):
    """."""

    def __init__(self):
        """."""

        self.get_data()
        self.get_res_afectades()
        self.get_master()
        self.get_calcul()
        self.get_calcul_no_actual()
        self.export_ind()
        self.get_cataleg()
        # self.dona_grants()

    def get_data(self):
        """agafem data calcul"""
        sql = "select min(data), max(data) from sisap_covid_res_historic"
        for mind, maxd in u.getAll(sql, db):
            self.calcul_avui = maxd
            self.ini = mind
            self.fi = maxd + timedelta(days=1)

    def get_res_afectades(self):
        """."""
        self.afectades = {}
        sql = """
            SELECT residencia, exposicio
            FROM sisap_covid_res_resum
            WHERE exposicio = 1
        """
        for resi, afecta in u.getAll(sql, 'redics'):
            self.afectades[resi] = afecta

    def get_master(self):
        """ Agafem les poblacions """

        self.indicadors = c.Counter()
        self.indicadors2 = c.Counter()
        sql = """
            SELECT
                hash, residencia, actual, pcr_data, pcr_N, pcr_ia_n, test_n,
                test_ia_n, pcr_res, vis_setmana, barthel_dat, pfeifer_dat,
                situacio
            FROM sisap_covid_res_master
        """
        for _hash, resi, actual, _dat_pcr, n_pcr, nia_pcr, test, test_ia, pcr_res, vis_set, barthel, pfeiffer, situacio in u.getAll(sql, 'redics'):  # noqa
            if actual == 1:
                self.indicadors[(resi, self.calcul_avui, 'den')] += 1
                if vis_set > 0:
                    self.indicadors[(resi, self.calcul_avui, 'vis')] += 1
                if barthel is not None:
                    self.indicadors[(resi, self.calcul_avui, 'bart')] += 1
                if pfeiffer is not None:
                    self.indicadors[(resi, self.calcul_avui, 'pfeiffer')] += 1
                if n_pcr > 0 or test > 0 or test_ia > 0 or nia_pcr > 0:
                    self.indicadors[(resi, self.calcul_avui, 'numTOT')] += 1
                if n_pcr > 0:
                    self.indicadors[(resi, self.calcul_avui, 'num')] += 1
                    self.indicadors[(resi, self.calcul_avui,
                                     'numPCR')] += n_pcr
                if n_pcr > 0 or nia_pcr > 0:
                    self.indicadors[(resi, self.calcul_avui, 'numIA')] += 1
                if test > 0 or test_ia > 0:
                    self.indicadors[(resi, self.calcul_avui, 'numt')] += 1
                if pcr_res == 'Positiu':
                    self.indicadors[(resi, self.calcul_avui, 'num+')] += 1
                if situacio == 'PCC':
                    self.indicadors[(resi, self.calcul_avui, 'numPCC')] += 1
                if situacio == 'MACA':
                    self.indicadors[(resi, self.calcul_avui, 'numMACA')] += 1
                if resi in self.afectades:
                    self.indicadors[(resi, self.calcul_avui, 'denA')] += 1
                    if vis_set > 0:
                        self.indicadors[(resi, self.calcul_avui, 'visA')] += 1
                    if barthel is not None:
                        self.indicadors[(resi, self.calcul_avui, 'bartA')] += 1
                    if pfeiffer is not None:
                        self.indicadors[(resi, self.calcul_avui,
                                         'pfeifferA')] += 1
                    if n_pcr > 0 or test > 0 or test_ia > 0 or nia_pcr > 0:
                        self.indicadors[(resi, self.calcul_avui,
                                         'numTOTA')] += 1
                    if n_pcr > 0:
                        self.indicadors[(resi, self.calcul_avui, 'numA')] += 1
                        self.indicadors[(resi, self.calcul_avui,
                                         'numPCRA')] += n_pcr
                    if n_pcr > 0 or nia_pcr > 0:
                        self.indicadors[(resi, self.calcul_avui,
                                         'numIAA')] += 1
                    if test > 0 or test_ia > 0:
                        self.indicadors[(resi, self.calcul_avui, 'numtA')] += 1
                    if pcr_res == 'Positiu':
                        self.indicadors[(resi, self.calcul_avui, 'num+A')] += 1
                    if situacio == 'PCC':
                        self.indicadors[(resi, self.calcul_avui,
                                         'numPCCA')] += 1
                    if situacio == 'MACA':
                        self.indicadors[(resi, self.calcul_avui,
                                         'numMACAA')] += 1
            else:
                self.indicadors2[(resi, self.calcul_avui, 'den')] += 1
                if vis_set > 0:
                    self.indicadors2[(resi, self.calcul_avui, 'vis')] += 1
                if barthel is not None:
                    self.indicadors2[(resi, self.calcul_avui, 'bart')] += 1
                if pfeiffer is not None:
                    self.indicadors2[(resi, self.calcul_avui, 'pfeiffer')] += 1
                if n_pcr > 0 or test > 0 or test_ia > 0 or nia_pcr > 0:
                    self.indicadors2[(resi, self.calcul_avui, 'numTOT')] += 1
                if n_pcr > 0:
                    self.indicadors2[(resi, self.calcul_avui, 'num')] += 1
                    self.indicadors2[(resi, self.calcul_avui,
                                      'numPCR')] += n_pcr
                if n_pcr > 0 or nia_pcr > 0:
                    self.indicadors2[(resi, self.calcul_avui, 'numIA')] += 1
                if test > 0 or test_ia > 0:
                    self.indicadors2[(resi, self.calcul_avui, 'numt')] += 1
                if pcr_res == 'Positiu':
                    self.indicadors2[(resi, self.calcul_avui, 'num+')] += 1
                if situacio == 'PCC':
                    self.indicadors2[(resi, self.calcul_avui, 'numPCC')] += 1
                if situacio == 'MACA':
                    self.indicadors2[(resi, self.calcul_avui, 'numMACA')] += 1
                if resi in self.afectades:
                    self.indicadors2[(resi, self.calcul_avui, 'denA')] += 1
                    if vis_set > 0:
                        self.indicadors2[(resi, self.calcul_avui, 'visA')] += 1
                    if barthel is not None:
                        self.indicadors2[(resi, self.calcul_avui,
                                          'bartA')] += 1
                    if pfeiffer is not None:
                        self.indicadors2[(resi, self.calcul_avui,
                                          'pfeifferA')] += 1
                    if n_pcr > 0 or test > 0 or test_ia > 0 or nia_pcr > 0:
                        self.indicadors2[(resi, self.calcul_avui,
                                          'numTOTA')] += 1
                    if n_pcr > 0:
                        self.indicadors2[(resi, self.calcul_avui, 'numA')] += 1
                        self.indicadors2[(resi, self.calcul_avui,
                                          'numPCRA')] += n_pcr
                    if n_pcr > 0 or nia_pcr > 0:
                        self.indicadors2[(resi, self.calcul_avui,
                                          'numIAA')] += 1
                    if test > 0 or test_ia > 0:
                        self.indicadors2[(resi, self.calcul_avui,
                                          'numtA')] += 1
                    if pcr_res == 'Positiu':
                        self.indicadors2[(resi, self.calcul_avui,
                                          'num+A')] += 1
                    if situacio == 'PCC':
                        self.indicadors2[(resi, self.calcul_avui,
                                          'numPCCA')] += 1
                    if situacio == 'MACA':
                        self.indicadors2[(resi, self.calcul_avui,
                                          'numMACAA')] += 1

    def get_calcul(self):
        """Calcul indicadors"""
        self.upload = []
        for (resi, data, tip), n in self.indicadors.items():
            if tip == 'den':
                num = self.indicadors[(resi, data, 'num')] \
                    if (resi, data, 'num') in self.indicadors else 0
                numPCR = self.indicadors[(resi, data, 'numPCR')] \
                    if (resi, data, 'numPCR') in self.indicadors else 0
                numt = self.indicadors[(resi, data, 'numt')] \
                    if (resi, data, 'numt') in self.indicadors else 0
                numTOT = self.indicadors[(resi, data, 'numTOT')] \
                    if (resi, data, 'numTOT') in self.indicadors else 0
                numP = self.indicadors[(resi, data, 'num+')] \
                    if (resi, data, 'num+') in self.indicadors else 0
                vis = self.indicadors[(resi, data, 'vis')] \
                    if (resi, data, 'vis') in self.indicadors else 0
                bart = self.indicadors[(resi, data, 'bart')] \
                    if (resi, data, 'bart') in self.indicadors else 0
                pfeiffer = self.indicadors[(resi, data, 'pfeiffer')] \
                    if (resi, data, 'pfeiffer') in self.indicadors else 0
                numIA = self.indicadors[(resi, data, 'numIA')] \
                    if (resi, data, 'numIA') in self.indicadors else 0
                numPCC = self.indicadors[(resi, data, 'numPCC')] \
                    if (resi, data, 'numPCC') in self.indicadors else 0
                numMACA = self.indicadors[(resi, data, 'numMACA')] \
                    if (resi, data, 'numMACA') in self.indicadors else 0
                self.upload.append([data, resi, 'COVRES01A', numTOT, n])
                self.upload.append([data, resi, 'COVRES02A', num, n])
                self.upload.append([data, resi, 'COVRES03A', numPCR, num])
                self.upload.append([data, resi, 'COVRES04A', numt, n])
                self.upload.append([data, resi, 'COVRES05A', numP, n])
                self.upload.append([data, resi, 'COVRES06A', numP, num])
                self.upload.append([data, resi, 'COVRES07A', vis, n])
                self.upload.append([data, resi, 'COVRES08A', bart, n])
                self.upload.append([data, resi, 'COVRES09A', pfeiffer, n])
                self.upload.append([data, resi, 'COVRES10A', numIA, n])
                self.upload.append([data, resi, 'COVRES30A', numPCC, n])
                self.upload.append([data, resi, 'COVRES31A', numMACA, n])
            if tip == 'denA':
                num = self.indicadors[(resi, data, 'numA')] \
                    if (resi, data, 'numA') in self.indicadors else 0
                numPCR = self.indicadors[(resi, data, 'numPCRA')] \
                    if (resi, data, 'numPCRA') in self.indicadors else 0
                numt = self.indicadors[(resi, data, 'numtA')] \
                    if (resi, data, 'numtA') in self.indicadors else 0
                numP = self.indicadors[(resi, data, 'num+A')] \
                    if (resi, data, 'num+A') in self.indicadors else 0
                numTOT = self.indicadors[(resi, data, 'numTOTA')] \
                    if (resi, data, 'numTOTA') in self.indicadors else 0
                vis = self.indicadors[(resi, data, 'visA')] \
                    if (resi, data, 'visA') in self.indicadors else 0
                bart = self.indicadors[(resi, data, 'bartA')] \
                    if (resi, data, 'bartA') in self.indicadors else 0
                pfeiffer = self.indicadors[(resi, data, 'pfeifferA')] \
                    if (resi, data, 'pfeifferA') in self.indicadors else 0
                numIA = self.indicadors[(resi, data, 'numIAA')] \
                    if (resi, data, 'numIAA') in self.indicadors else 0
                numPCC = self.indicadors[(resi, data, 'numPCCA')] \
                    if (resi, data, 'numPCCA') in self.indicadors else 0
                numMACA = self.indicadors[(resi, data, 'numMACAA')] \
                    if (resi, data, 'numMACAA') in self.indicadors else 0
                self.upload.append([data, resi, 'COVRES01B', numTOT, n])
                self.upload.append([data, resi, 'COVRES02B', num, n])
                self.upload.append([data, resi, 'COVRES03B', numPCR, num])
                self.upload.append([data, resi, 'COVRES04B', numt, n])
                self.upload.append([data, resi, 'COVRES05B', numP, n])
                self.upload.append([data, resi, 'COVRES06B', numP, num])
                self.upload.append([data, resi, 'COVRES07B', vis, n])
                self.upload.append([data, resi, 'COVRES08B', bart, n])
                self.upload.append([data, resi, 'COVRES09B', pfeiffer, n])
                self.upload.append([data, resi, 'COVRES10B', numIA, n])
                self.upload.append([data, resi, 'COVRES30B', numPCC, n])
                self.upload.append([data, resi, 'COVRES31B', numMACA, n])

    def get_calcul_no_actual(self):
        """Calcul indicadors"""

        for (resi, data, tip), n in self.indicadors2.items():
            if tip == 'den':
                num = self.indicadors2[(resi, data, 'num')] \
                    if (resi, data, 'num') in self.indicadors2 else 0
                numPCR = self.indicadors2[(resi, data, 'numPCR')] \
                    if (resi, data, 'numPCR') in self.indicadors2 else 0
                numt = self.indicadors2[(resi, data, 'numt')] \
                    if (resi, data, 'numt') in self.indicadors2 else 0
                numP = self.indicadors2[(resi, data, 'num+')] \
                    if (resi, data, 'num+') in self.indicadors2 else 0
                numTOT = self.indicadors2[(resi, data, 'numTOT')] \
                    if (resi, data, 'numTOT') in self.indicadors2 else 0
                vis = self.indicadors2[(resi, data, 'vis')] \
                    if (resi, data, 'vis') in self.indicadors2 else 0
                bart = self.indicadors2[(resi, data, 'bart')] \
                    if (resi, data, 'bart') in self.indicadors2 else 0
                pfeiffer = self.indicadors2[(resi, data, 'pfeiffer')] \
                    if (resi, data, 'pfeiffer') in self.indicadors2 else 0
                numIA = self.indicadors2[(resi, data, 'numIA')] \
                    if (resi, data, 'numIA') in self.indicadors2 else 0
                numPCC = self.indicadors2[(resi, data, 'numPCC')] \
                    if (resi, data, 'numPCC') in self.indicadors2 else 0
                numMACA = self.indicadors2[(resi, data, 'numMACA')] \
                    if (resi, data, 'numMACA') in self.indicadors2 else 0
                self.upload.append([data, resi, 'COVRES11A', numTOT, n])
                self.upload.append([data, resi, 'COVRES12A', num, n])
                self.upload.append([data, resi, 'COVRES13A', numPCR, num])
                self.upload.append([data, resi, 'COVRES14A', numt, n])
                self.upload.append([data, resi, 'COVRES15A', numP, n])
                self.upload.append([data, resi, 'COVRES16A', numP, num])
                self.upload.append([data, resi, 'COVRES17A', vis, n])
                self.upload.append([data, resi, 'COVRES18A', bart, n])
                self.upload.append([data, resi, 'COVRES19A', pfeiffer, n])
                self.upload.append([data, resi, 'COVRES20A', numIA, n])
                self.upload.append([data, resi, 'COVRES40A', numPCC, n])
                self.upload.append([data, resi, 'COVRES41A', numMACA, n])
            if tip == 'denA':
                num = self.indicadors2[(resi, data, 'numA')] \
                    if (resi, data, 'numA') in self.indicadors2 else 0
                numPCR = self.indicadors2[(resi, data, 'numPCRA')] \
                    if (resi, data, 'numPCRA') in self.indicadors2 else 0
                numt = self.indicadors2[(resi, data, 'numtA')] \
                    if (resi, data, 'numtA') in self.indicadors2 else 0
                numP = self.indicadors2[(resi, data, 'num+A')] \
                    if (resi, data, 'num+A') in self.indicadors2 else 0
                numTOT = self.indicadors2[(resi, data, 'numTOTA')] \
                    if (resi, data, 'numTOTA') in self.indicadors2 else 0
                vis = self.indicadors2[(resi, data, 'visA')] \
                    if (resi, data, 'visA') in self.indicadors2 else 0
                bart = self.indicadors2[(resi, data, 'bartA')] \
                    if (resi, data, 'bartA') in self.indicadors2 else 0
                pfeiffer = self.indicadors2[(resi, data, 'pfeifferA')] \
                    if (resi, data, 'pfeifferA') in self.indicadors2 else 0
                numIA = self.indicadors2[(resi, data, 'numIAA')] \
                    if (resi, data, 'numIAA') in self.indicadors2 else 0
                numPCC = self.indicadors2[(resi, data, 'numPCCA')] \
                    if (resi, data, 'numPCCA') in self.indicadors2 else 0
                numMACA = self.indicadors2[(resi, data, 'numMACAA')] \
                    if (resi, data, 'numMACAA') in self.indicadors2 else 0
                self.upload.append([data, resi, 'COVRES11B', numTOT, n])
                self.upload.append([data, resi, 'COVRES12B', num, n])
                self.upload.append([data, resi, 'COVRES13B', numPCR, num])
                self.upload.append([data, resi, 'COVRES14B', numt, n])
                self.upload.append([data, resi, 'COVRES15B', numP, n])
                self.upload.append([data, resi, 'COVRES16B', numP, num])
                self.upload.append([data, resi, 'COVRES17B', vis, n])
                self.upload.append([data, resi, 'COVRES18B', bart, n])
                self.upload.append([data, resi, 'COVRES19B', pfeiffer, n])
                self.upload.append([data, resi, 'COVRES20B', numIA, n])
                self.upload.append([data, resi, 'COVRES40B', numPCC, n])
                self.upload.append([data, resi, 'COVRES41B', numMACA, n])

    def export_ind(self):
        """fem export dels indicadors"""

        # columns = ["data date", "resi varchar2(10)",
        #            "indicador varchar2(10)", "num int", "den int"]
        # u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        try:
            u.execute("""
                DELETE FROM {0} WHERE  to_char(data, 'DD-MM-YYYY') = '{1}'
                """.format(tb, self.calcul_avui.strftime("%d-%m-%Y")), db)
        except Exception as e:
            print(str(e))
            pass

        u.listToTable(self.upload, tb, db)

    def get_cataleg(self):
        """Cataleg"""

        upload = []
        for indica in cat_ind:
            literal = cat_ind[indica]['lit']
            multi = cat_ind[indica]['multi']
            grup = cat_ind[indica]['grup']
            litc = cat_ind[indica]['lit_curt']
            ordre = cat_ind[indica]['ordre']
            upload.append([indica, literal, multi, grup, litc, ordre])

        # columns = ["indicador varchar2(10)", "literal varchar2(300)",
        #            "multiplica int", "agrupador varchar2(100)",
        #            "literal_curt varchar2(100)", "ordre int"]
        # u.createTable(tb_cat, "({})".format(", ".join(columns)), db, rm=True)
        u.execute("truncate table {0}".format(tb_cat), db)
        u.listToTable(upload, tb_cat, db)

    def dona_grants(self):
        """."""
        users = ["PREDUFFA", "PREDUMMP", "PREDUPRP",
                 "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb, user), db)
            u.execute("grant select on {} to {}".format(tb_cat, user), db)


if __name__ == '__main__':
    u.printTime("Inici")
    Coronavirus_ind()
    u.printTime("Final")
