# coding: iso-8859-1

import sisapUtils as u
from datetime import datetime
from dateutil.relativedelta import relativedelta
import datetime as d

TODAY = d.datetime.now().date()
YEAR = 2019
DATES = ('2019-01-01','2019-09-30')





class ive(object):

    def __init__(self):
        """."""
        
        self.get_cat_varcli_val()
        self.get_centres_assir()
        #self.get_imputacio_up()
        self.get_ive()
        self.get_upload()
        self.export_csv()


    def get_cat_varcli_val(self):
        """ . """
        self.cat_varcli_val = {}
        select = "select distinct vi_cod, vi_min, vi_des \
                  from cat_prstb106 \
                  where vi_cod in ('FW3001') and vi_min = vi_max"
        db = "import"
        for cod, val, val_desc in u.getAll(select,db):
            self.cat_varcli_val[val]=val_desc
        print(self.cat_varcli_val)

    def get_centres_assir(self):
        """."""
        self.centres_assir={}
        select = "select up, up_desc, up_assir, assir from ass_centres"
        bd = "nodrizas"
        for up, up_desc, assir, assir_desc in u.getAll(select,bd):
            self.centres_assir[up]={'up_desc': up_desc, 
                                    'assir': assir, 'assir_desc': assir_desc}
        print("N UP ASSIR:", len(self.centres_assir))

    def get_imputacio_up(self):
        """."""
        self.imputacio_up={}
        select = "select id_cip_sec, visi_up from ass_imputacio_up"
        bd = "nodrizas"
        for id, up in u.getAll(select, bd):
            self.imputacio_up[id]={'up': up}
        print("N pacients ASSIR",len(self.imputacio_up))

    # def get_ive(self):
        # """."""
        # self.ive = []
        # select = "select id_cip_sec, dat, val_num \
                  # from nodrizas.ass_variables \
                  # where agrupador=636 \
                  # and dat between CAST('{}' AS DATE) and CAST('{}' AS DATE)".format(*DATES)
        # db = "nodrizas"
        # for row in u.getAll(select, db):
            # self.ive.append(row)
        # print("N registres IVE",len(self.ive))

    def get_ive(self):
        """."""
        self.ive = []
        select = "select id_cip_sec, vu_dat_act, vu_val, vu_up \
                  from variables2 \
                  where vu_cod_vs in ('FW3001') \
                  and vu_dat_act between CAST('{}' AS DATE) and CAST('{}' AS DATE)".format(*DATES)
        db = "import"
        for row in u.getAll(select, db):
            self.ive.append(row)
        print("N registres IVE",len(self.ive))
        
    def get_upload(self):
        """."""
        self.upload = []
        for id, dat, val, up in self.ive:
            #if id in self.imputacio_up:
            #up = self.imputacio_up[id]['up']
            if up in self.centres_assir:
                up_desc = self.centres_assir[up]['up_desc']
                assir = self.centres_assir[up]['assir']
                assir_desc = self.centres_assir[up]['assir_desc']
                month = dat.strftime("%b")
                val_text = self.cat_varcli_val[val]
                self.upload.append((up, up_desc, 
                                    assir, assir_desc, 
                                    id, month, dat, val_text))
        print("N registres UPLOAD",len(self.upload))

    def export_csv(self):
        """."""
        u.writeCSV(u.tempFolder + 'ive_{}_{}.csv'.format(YEAR,TODAY.strftime('%Y%m%d')),
                   [('up','up_desc','assir','assir_desc','id','mes','data','valor')] + self.upload, sep=';')



if __name__ == "__main__":
    clock_in = datetime.now()
    ive()
    clock_out = datetime.now()
    print("---------------------------------------------- TIME EXECUTION")
    print('''START: {}'''.format(clock_in))
    print('''END: {}'''.format(clock_out))
    print('''DELTA: {}'''.format (clock_out - clock_in))