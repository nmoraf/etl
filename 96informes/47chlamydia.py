from sisapUtils import multiprocess, getSubTables, getAll, tempFolder, writeCSV, printTime
from collections import defaultdict


proves = {cod: des for (cod, des) in getAll("select cpi_codi_prova, cpi_desc_prova from labtb112 where upper(cpi_desc_prova) like '%CHLAMYDIA%' \
                                             union select cpr_codi_prova, cpr_desc from labtb120 where upper(cpr_desc) like '%CHLAMYDIA%'", 'redics')}


def get_data(taula):
    data = {}
    sql = "select cr_codi_prova_ics, year(cr_data_reg) from {} where cr_codi_prova_ics in {} and cr_data_reg between '20060101' and '20151231'".format(taula, tuple(proves))
    for cod, dat in getAll(sql, 'import'):
        if (cod, dat) not in data:
            data[(cod, dat)] = 1
        else:
            data[(cod, dat)] += 1
    return data


def union_data(data):
    union = {}
    for set in data:
        for id, count in set.items():
            if id not in union:
                union[id] = count
            else:
                union[id] += count
    return union


def export_data(data):
    resul = defaultdict(lambda: defaultdict(int))
    for (cod, dat), count in data.items():
        resul[cod][dat] = count
    dats = range(2006, 2016)
    export = [['prova'] + dats]
    for cod, count in resul.items():
        export.append([proves[cod]] + [count[dat] for dat in dats])
    writeCSV(tempFolder + 'chlamydia.csv', export, sep=';')


if __name__ == '__main__':
    printTime()
    jobs = sorted(getSubTables('laboratori'), key=getSubTables('laboratori').get, reverse=True)
    data = multiprocess(get_data, jobs, 12)
    union = union_data(data)
    export_data(union)
    printTime()
