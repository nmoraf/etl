# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
del model adults per uba (2018).
Es tracta d'una actualització del procés del Francesc.
Les dades es refereixen 12 mesos enrere
"""

import urllib as w
import os

import sisapUtils as u

virtuals_fins = '20180531'

class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dext, self.dext1a = u.getOne("select date_format(data_ext,'%Y%m%d'), date_format(date_add(date_add(data_ext, interval - 1 year),interval + 1 day), '%Y%m%d') from dextraccio", "nodrizas")
        self.get_centres()
        self.get_numcol()
        self.get_poblacio()
        self.get_hash()
        self.get_gma()
        self.get_medea()
        self.get_atdom()
        #self.get_codi_postal()
        self.get_visites()
        self.get_virtuals()
        self.export_dades_pacient()
        self.export_dades_centre()


    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_numcol(self):
        """Agafem numcols de mf"""
        
        self.numcols = {}
        self.numsllocs = {}
        
        sql = "select codi_sector, lloc_codi_lloc_de_treball, lloc_numcol, lloc_especialit_codi_especiali from cat_pritb025 where lloc_especialit_codi_especiali in ('10999', '10117')"
        for sector, lloc, numcol, espe in u.getAll(sql, "import"):
            self.numsllocs[lloc, sector] = numcol
        
        sql = " select codi_sector, uab_codi_up,uab_codi_uab,uab_lloc_de_tr_codi_lloc_de_tr from import.cat_vistb039 where uab_servei_codi_servei='MG'"
        for sector, up, uba, lloctr in u.getAll(sql, "import"):
            if up in self.centres:
                if (lloctr, sector) in self.numsllocs:
                    numcol = self.numsllocs[lloctr, sector]
                    self.numcols[sector, numcol] = {'up': up, 'uba': uba}
            
    
    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tacament
           - majors de 14 anys a l'inici del període
        """
        self.dades = {}
        self.ambulatoris = set()
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, codi_sector, edat, sexe, \
                       institucionalitzat, nacionalitat, nivell_cobertura, \
                       up, centre_codi, centre_classe, uba \
                from assignada_tot \
                where edat > 14", "nodrizas")
        for id, sec, edt, sex, insti, nac, cob, up, cod, cla, uba in u.getAll(*sql):
            if up in self.centres:
                pensio = 1 if cob == 'PN' else 0
                self.dades[id] = {
                              'hash': None, 'sector': sec, 'edat': edt,
                              'sex': sex, 'gma_cod': None, 'gma_cmplx': None,
                              'gma_num': None, 'medea': None, 'insti': insti,
                              'renta': nac in renta, 'atdom': 0,
                              'pensio': pensio, 'up': up, 'c_codi': cod,
                              'c_classe': cla, 'uba': uba, 'cp': None, '9C': 0, '9D': 0,
                              '9T': 0, '9R': 0, '9E':0, 'C': 0, 'D': 0, 'V': 0}
                self.ambulatoris.add((cod, cla))

    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        for id, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash

    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]

    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        sql = ("select id_cip_sec from eqa_problemes where ps = 45",
               "nodrizas")
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['atdom'] = 1

    def get_codi_postal(self):
        """
        Obté el codi postal dels pacients executant en paral·lel un worker
        contra cadascun dels 21 sectors. Els workers tornen la informació
        en HASH, que convertim a ID per poder creuar amb la resta de dades.
        """
        codi_postal = u.multiprocess(get_codi_postal_worker, u.sectors)
        hash_to_id = {(sector, hash): id for (id, sector, hash)
                      in u.getAll("select id_cip_sec, codi_sector, hash_d \
                                   from u11", "import")}
        for worker in codi_postal:
            for sector, hash, codi in worker:
                if (sector, hash) in hash_to_id:
                    id = hash_to_id[(sector, hash)]
                    if id in self.dades:
                        self.dades[id]['cp'] = codi

    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        """
        sql = ("select id_cip_sec, visi_up, visi_tipus_visita, \
                       visi_lloc_visita, date_format(visi_data_visita,'%Y%m%d') \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                     visi_servei_codi_servei = 'MG'", "import")
        for id, up, tipus, lloc, dat in u.getAll(*sql):
            if  self.dext1a <= dat <= self.dext:
                if id in self.dades and up in self.centres:
                    if tipus in ('9C', '9D', '9T', '9R', '9E'):
                        self.dades[id][tipus] += 1
                    elif lloc in ('C', 'D'):
                        self.dades[id][lloc] += 1

    def get_virtuals(self):
        """
        Agafem les visites virtuals fins a finals de maig quan les vvirtuals van passar a ser una altra cosa
        amb l'entrada de la 9E
        """
        sql = ("select codi_sector, id_cip_sec, vvi_num_col\
                from vvirtuals \
                where vvi_situacio='R' and \
                      vvi_data_alta between '{}' and '{}'".format(self.dext1a, virtuals_fins),
               "import")
        for sector, id, numcol in u.getAll(*sql):
            if (sector, numcol) in self.numcols:
                up, uba = self.numcols[sector, numcol]['up'], self.numcols[sector, numcol]['uba']
                if id in self.dades:
                    up2, uba2 = self.dades[id]['up'], self.dades[id]['uba']
                    if up == up2 and uba == uba2:
                        self.dades[id]['V'] += 1    

    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules."""
        """Mètode per crear i omplir taules."""
        table = 'SISAP_MODEL_ADULTS_uba_{}_2018'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db, rm=True)
        #u.grantSelect(table, ('PREDUMMP', 'PREDUPRP',
        #                      'PREDUECR'), db)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        dades = [(d['hash'], d['edat'], d['sex'], d['gma_cod'], d['gma_cmplx'],
                  d['gma_num'], d['medea'], d['insti'], d['renta'], d['atdom'],
                  d['pensio'], d['up'], d['c_codi'], d['c_classe'],  d['uba'], d['cp'],
                  d['9C'], d['9D'], d['9T'], d['9R'],  d['9E'] ,d['C'], d['D'], d['V'])
                 for id, d in self.dades.items()]
        columns = ('id varchar(40)', 'edat int', 'sexe varchar(1)',
                   'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'immigracio_baixa_renta int',
                   'atdom int', 'pensionista int', 'up varchar(5)',
                   'centre_codi varchar(10)', 'centre_classe varchar(2)', 'uba varchar(5)',
                   'codi_postal varchar(5)', 'visites_capes_9c int',
                   'visites_capes_9d int', 'visites_capes_9t int',
                   'visites_capes_9r int', 'visites_capes_9e int', 'visites_sequencials_centre int',
                   'visites_sequencials_domicili int', 'visites_virtuals int')
        Frequentacio.export_dades("PACIENT", columns, dades)

    def export_dades_centre(self):
        """Extreu i exporta les dades de nivell centre."""
        codi_postal = {centre: codi for (centre, codi)
                       in u.getAll("select edce_codi_centre, edce_cpostal \
                                    from cat_pritb120", "import")}
        sql = ("select cent_codi_centre, cent_classe_centre, cent_codi_up \
                from cat_pritb010", "import")
        dades = [(codi, classe, self.centres[up][1], self.centres[up][2],
                  codi_postal[codi], up)
                 for (codi, classe, up)
                 in u.getAll(*sql)
                 if (codi, classe) in self.ambulatoris and up in self.centres]
        columns = ('codi varchar(10)', 'classe varchar(2)',
                   'ambit varchar(2)', 'sap varchar(2)',
                   'codi_postal varchar(5)', 'up varchar(5)')
        Frequentacio.export_dades("CENTRE", columns, dades)

 
def get_codi_postal_worker(sector):
    """Worker per capturar les dades de codi postal d'un sector."""
    c2h = {cip: hash for (cip, hash)
           in u.getAll("select usua_cip, usua_cip_cod from pdptb101",
                       sector + 'a')}
    codi_postal = [(sector, c2h[cip], codi) for (cip, codi)
                   in u.getAll("select usua_cip, usua_codi_postal \
                                from usutb040", sector) if cip in c2h]
    return codi_postal


if __name__ == '__main__':
    Frequentacio()
