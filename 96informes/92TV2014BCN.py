# coding: iso-8859-1
#Mireia Garcia (ASPB), marc 2017, Ermengol
#cobertures TV en nens nascuts al 2014 per up BCN

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = "pedia"
table = "mst_vacsist"

centres = {}
sql = "select scs_codi, ics_desc from cat_centres where amb_codi='03' or scs_codi in ('00467','00465')"
for up, descr in getAll(sql, 'nodrizas'):
    centres[up] = {'desc': descr}
    
tv = Counter()
pob = Counter()
sql = 'select year(data_naix), up, xrp from {} where ates=1'.format(table)
for anys, up, xrp in getAll(sql, db):
    if anys==2014:
        if up in centres:
            descripcio = centres[up]['desc']
            tv[up] += xrp
            pob[up, descripcio] += 1
            
upload = []
for (up, descripcio), v in pob.items():
    if up in tv:
        vacuna = tv[up]
    upload.append([up, descripcio, vacuna, v])
file = tempFolder + 'TV_nascuts2014_BCN.txt'
writeCSV(file, upload, sep=';')
        
