# -*- coding: utf-8 -*-

"""
.
"""

import collections as c


import sisapUtils as u

u.execute("drop table tmp_estiu_loc", 'redics')
sql = "select a.cip, a.data_validacio_mostra, b.localitat_c, c.localitat \
       from dwaquas.covid19_aut_resul_pcr a, \
            sisap_coronavirus_cat_regio b, \
            rca_cip_nia c \
       where a.up_codi = b.up and \
             a.cip = c.cip and \
             a.resultat = 'Positiu' and \
             a.data_validacio_mostra is not null"
dades = c.defaultdict(dict)
for cip, dat, pcr, rca in u.getAll(sql, 'exadata'):

    dades[cip][dat] = (pcr, rca)

upload = []
for cip, dates in dades.items():
    first = min(dates)
    if first.month == 7 or first.month == 8:
        upload.append(((first.month,) + dates[first]))
cols = ("mes int", "localitat_pcr int", "localitat_rca int")

u.createTable("sisap_covid_tmp_estiu_loc", "({})".format(", ".join(cols)), 'redics', rm=True)
u.listToTable(upload, "sisap_covid_tmp_estiu_loc", 'redics')

users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
for user in users:
    u.execute("grant select on {} to {}".format("sisap_covid_tmp_estiu_loc",user),'redics')

"""
select decode(quinzena, 0, '1 a 15', '16 a 31') quinzena, regio_des,
       count(1) positius,
       round(100 * avg(case when regio_pcr = regio_rca then 0
                                                       else 1 end), 2) fora
from preduffa.sisap_covid_tmp_estiu a,
     (select distinct regio_cod, regio_des
      from preduffa.sisap_covid_cat_aga) b
where a.regio_pcr = b.regio_cod
group by quinzena, regio_des
order by quinzena, regio_des

select regio_des, count(1)
from preduffa.sisap_covid_tmp_estiu a,
     (select distinct regio_cod, regio_des
      from preduffa.sisap_covid_cat_aga) b
where a.regio_rca = b.regio_cod and
      a.regio_pcr = 62 and
      a.quinzena = 1
group by regio_des
order by count(1) desc
"""