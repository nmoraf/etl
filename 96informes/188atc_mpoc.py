# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

"""
Procés passar ATCs MPOC d'uns pacientc concrets
"""


nod = "nodrizas"


file = tempFolder + 'mpoc_HUVH.txt'

farmacs = "('R03AC','R03AK','R03AL','R03DA','R03DB','R03DC','R03DX','R03BA','R03BB','R03BC','R03BX','H02AB')"


class atc_mpoc(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_centres()
        self.get_cat_ATC()
        self.get_poblacio()
        self.id_to_hash()
        self.hash_to_cip()
        self.get_cips()
        self.get_tractaments()
        
        
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_desc from cat_centres where ep='0208'"
        self.centres = {up: desc for (up, desc) in getAll(sql, 'nodrizas')}
        
    def get_cat_ATC(self):
        """."""
        sql = "select atc_codi, atc_desc from cat_cpftb010"
        self.catATC = {atc: desc for (atc, desc) in getAll(sql, 'import')}
        
        sql = "select pf_codi, pf_desc_gen from cat_cpftb006"
        self.catPF = {pf: desc for (pf, desc) in getAll(sql, 'import')}
    
    def get_poblacio(self):
        """."""
        self.pob = {}
        sql = "select id_cip, up from assignada_tot"
        for id, up in getAll(sql, "nodrizas"):
            if up in self.centres:
                self.pob[id] = up

    def id_to_hash(self):
        """."""
        self.ids = {}
        sql = "select hash_d, codi_sector, id_cip from u11"
        for hash, sector, id in getAll(sql, 'import'):
            self.ids[id] = {'hash': hash, 'sector': sector}                
            
    def hash_to_cip(self):
        """."""
        self.hashos = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        for c,h in getAll(sql, 'pdp'):
            self.hashos[h] = c
 
    def get_cips(self):
        """."""
        self.cips = {}
        for (cip) in readCSV(file, sep='@'):                                                                                                                                                                                                                                                                                                           
            self.cips[cip[0][:13]] =  cip[0]

 
    def get_tractaments(self):
        """."""
        upload = []
        controlCip = {}
        sql = "select id_cip, ppfmc_atccodi, ppfmc_pf_codi,  ppfmc_pmc_data_ini, ppfmc_data_fi, ppfmc_durada, ppfmc_freq, ppfmc_posologia from tractaments where left(ppfmc_atccodi,5) in {}".format(farmacs)
        for id, atc, pf, ini, fi, durada, freq, posologia in getAll(sql, "import"):
            if id in self.pob:
                hash = self.ids[id]['hash']
                cip = self.hashos[hash]
                if cip in self.cips:
                    cipVH = self.cips[cip]
                    descatcs = self.catATC[atc]
                    descpf = self.catPF[pf]
                    upload.append([cipVH, atc, descatcs, pf, descpf, ini, fi, durada, freq, posologia])
                    controlCip[cip] = True

        writeCSV(tempFolder + 'atc_mpoc.txt', upload)        
        upload = []
        for cip in self.cips:
            if cip not in controlCip:
                upload.append([self.cips[cip]])
        writeCSV(tempFolder + 'controlCip.txt', upload)         
        
if __name__ == "__main__":
    printTime('Inici')
    
    atc_mpoc()
    
    printTime('Fi')