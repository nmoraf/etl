from sisapUtils import getAll, ageConverter, writeCSV, tempFolder


tabac = {id: tab for (id, tab) in getAll('select id_cip_sec, tab from eqa_tabac where last = 1', 'nodrizas')}
data = {}
for id, sexe, edat in getAll('select id_cip_sec, sexe, edat from assignada_tot where ates = 1 and maca = 0 and institucionalitzat = 0', 'nodrizas'):
    key = (sexe, ageConverter(edat))
    if key not in data:
        data[key] = [0, 0, 0]
    data[key][0] += 1
    if id in tabac:
        data[key][1] += 1
        if tabac[id] == 1:
            data[key][2] += 1
upload = [key + tuple(n) for key, n in data.items()]
writeCSV(tempFolder + 'tabac.csv', upload, sep=';')
