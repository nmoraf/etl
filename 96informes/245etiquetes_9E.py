# coding: latin1

"""
.
"""


import collections as c
from datetime import timedelta

import sisapUtils as u


"""
etiquetes = {}
for sector in u.sectors:
    for  cen, cla, cod, desc in u.getAll("select eti_centre,eti_classe,eti_codi,eti_descripcio from vistb079", sector):
        etiquetes[(cod)] = desc
 """       
upload = []

sql = """SELECT etiqueta, count(1) recompte
            FROM preduffa.sisap_covid_pac_visites
            WHERE tipus LIKE '9E%' AND data > DATE '2020-08-31' and servei='MG'
            GROUP BY etiqueta"""
for etiqueta, rec in u.getAll(sql, 'redics'):
    upload.append([etiqueta, None, rec])
    
file = u.tempFolder + "etiquetes_9E.csv"
u.writeCSV("etiquetes_9E.csv", upload, sep=';')
    
        
 