# -*- coding: utf8 -*-

"""
Bronquiolitis i VRS en nens
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


def get_nivell(edat):
    """."""
    nivell = 'INF1C' if edat < 3 else  'INF2C' if edat <6 else 'PRI' if edat < 12 else 'ESO' if edat <16 else 'BAX' if edat <18 else 'ADULTS'
    return (nivell)

tb = 'sisap_vrs_2020'
db = 'permanent'

codis = '("J21.8","J21.9","J21","J20.5","J12.1","B97.4","J21.0","C01-B97.4","C01-J20.5","C01-J12.1","C01-J21.0","C01-J21.1","C01-J21.8","C01-J21.9","C01-J21")'

class Pneumonies(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_assignada()
        self.get_refredats()

    
    def get_assignada(self):
        """."""
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement from assignada'
        for id, sexe, naix in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        
        sql = "select id_cip, pr_up, pr_dde, pr_cod_ps, pr_th \
           from problemes \
           where pr_cod_o_ps = 'C' and \
                 pr_cod_ps in {} and \
                 extract(year_month from pr_dde) >'200908' and \
                 pr_data_baixa = 0 \
                 group by id_cip, pr_dde, pr_cod_ps, pr_th, pr_up".format(codis)
        pneumo = c.Counter()
        for id, up, dde, ps, th in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dde)
            except:
                ed = None  
            grup = None
            if ed != None:
                if int(ed) < 15:
                    grup = 0
                elif 15 <= int(ed) < 65:
                    grup = 1
                elif 64 < int(ed) < 75:
                    grup = 2
                elif int(ed)>74:
                    grup = 3
                else:
                    grup = 9
                nivell = get_nivell(ed) 
            pneumo[(dde, up, grup, sexe, nivell)] += 1
  
        cols = ("up varchar(5)", "data date", "grup_edat int", "sexe varchar(10)", "nivell varchar(10)","recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (dde, up, grup, sexe, nivell), n in pneumo.items():
            n = int (n)
            upload.append([up, dde, grup, sexe, nivell, n])
        u.listToTable(upload, tb, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Pneumonies()
    
    u.printTime("Final")                 
