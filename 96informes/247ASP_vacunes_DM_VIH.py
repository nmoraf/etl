# encoding: utf-8

import sisapUtils as u
import datetime as dt
from dateutil import relativedelta as rd

start = dt.datetime.utcnow()
print(start)

# Petición
"""
trello: https://trello.com/c/T1GChjJ0
pide: Montse Martinez (departament de salut)
define: Carol Guiriguet
el Ministeri de Sanitat ens sol·licita la cobertura vacunal de en les persones amb DM i VIH.
Operativización:
    * cobertura de PNC (pneumococica conjugada), PNP (pneumococica polisacarida) y GRIP
    * en población VIH y DM
    * de atesos, en estas cohorts_edats (contra any tancat [2019: year(dext)-1])
        GT65    between 1906 and 1954 (>65)
        1564    between 1955 and 2004 (between 15 and 64)
        0214    between 2005 and 2017 (between 2 and 14)
        LT15    between 2005 and 2019 (<15

- 202010: Actualització [Edu]

"""


# Funciones auxiliares

def ageAtDate(birthDate, refDate=dt.datetime.utcnow().date()):
    # Get age in years between the refDate and the birthDate
    age = rd.relativedelta(refDate, birthDate)
    age = age.years
    return age

def get_dextraccio():
    # Get current data_ext
    """fecha de extracción (datetime.date)"""
    sql = "select data_ext from dextraccio"
    return u.getOne(sql, "nodrizas")[0]


# Params & buckets

## fechas relevantes
dext_year = get_dextraccio().year
previous_year = dext_year - 1

CLOSED_YEAR_END = dt.date(dext_year, 12, 31)
FLU_YEAR_START = dt.date(previous_year, 9, 1)
FLU_YEAR_END = dt.date(dext_year, 1, 31)

## Vacunes
grip, pnc, pnp = set(), set(), set()
VACUNES_TB = "import.vacunes"

GRIP = {"A-GRIP", "A00033"} # Antigens de vacunes de grip
PNC = {"A00025", "A00026", "A00027"} # Antigens de vacunes de Pneumococ conjugat
PNP = {"A00028"} # Antigens de Pneumococ Polisacarids

## Problemes
dm, vih = set(), set()
PROBLEMES_TB = ["eqa_problemes", "ped_problemes"]

DM = {18, 24} # PARAM: agrupadors de DM
VIH = {101} # PARAM: agrupador de VIH i SIDA

""" Atesos """
at_gt65, at_1564, at_0214, at_lt15 = set(), set(), set(), set()
ATESOS_TB = "assignada_tot"


# Querys

atesos_query = "select id_cip_sec, data_naix from {} where ates=1".format(ATESOS_TB)

problemes_query = "select id_cip_sec, ps from {{}} where ps in {}".format(tuple(VIH | DM))

vacunes_query = """
    select
        v.id_cip_sec, va_u_data_vac, antigen
        , va_u_motiu_baixa
    from
        {} v
    inner join
        import.cat_prstb040_new c
    on
        v.va_u_cod = c.vacuna
        and c.antigen in {}
        -- and va_u_motiu_baixa = ''
""".format(VACUNES_TB, tuple(PNC | PNP | GRIP))


# GET 

## GET Vacunes
for id_cip_sec, data, antigen, baixa in u.getAll(vacunes_query, "nodrizas"):
    if data <= FLU_YEAR_END and data >= FLU_YEAR_START and antigen in GRIP and baixa == '':
        grip.add(id_cip_sec)
    if data <= CLOSED_YEAR_END and antigen not in GRIP and baixa == '':
        if antigen in PNC:
            pnc.add(id_cip_sec)
        elif antigen in PNP:
            pnp.add(id_cip_sec)

## GET Problemes
for tb in PROBLEMES_TB:
    for id_cip_sec, ps in u.getAll(problemes_query.format(tb), "nodrizas"):
        if ps in VIH:
            vih.add(id_cip_sec)
        if ps in DM:
            dm.add(id_cip_sec)

## GET Atesos
for id_cip_sec, data_naix in u.getAll(atesos_query, "nodrizas"):
    edat_years = ageAtDate(data_naix, CLOSED_YEAR_END)
    if edat_years >= 65:
        at_gt65.add(id_cip_sec)
    if 15 <= edat_years <= 64:
        at_1564.add(id_cip_sec)
    if 2 <= edat_years <= 14:
        at_0214.add(id_cip_sec)
    if edat_years < 15:
        at_lt15.add(id_cip_sec)

# OUTPUT
output = []
# output.append(('indicador','numerador','denominador'))

## OUTPUT vih
output.append(('grip & vih_at_lt15', len(grip & vih & at_lt15), len(vih & at_lt15)))
output.append(('grip & vih_at_1564', len(grip & vih & at_1564), len(vih & at_1564)))
output.append(('grip & vih_at_gt65', len(grip & vih & at_gt65), len(vih & at_gt65)))
output.append(('pnp & vih_at_0214', len(pnp & vih & at_0214), len(vih & at_0214)))
output.append(('pnp & vih_at_1564', len(pnp & vih & at_1564), len(vih & at_1564)))
output.append(('pnp & vih_at_gt65', len(pnp & vih & at_gt65), len(vih & at_gt65)))
output.append(('pnc & vih_at_lt15', len(pnc & vih & at_lt15), len(vih & at_lt15)))
output.append(('pnc & vih_at_1564', len(pnc & vih & at_1564), len(vih & at_1564)))
output.append(('pnc & vih_at_gt65', len(pnc & vih & at_gt65), len(vih & at_gt65)))
output.append(('pnc & pnp & vih_at_1564', len(pnc & pnp & vih & at_1564), len(vih & at_1564)))
output.append(('pnc & pnp & vih_at_gt65', len(pnc & pnp & vih & at_gt65), len(vih & at_gt65)))

## OUTPUT dm
output.append(('grip & dm_at_lt15',       len(grip & dm & at_lt15), len(dm & at_lt15)))
output.append(('grip & dm_at_1564',       len(grip & dm & at_1564), len(dm & at_1564)))
output.append(('grip & dm_at_gt65',       len(grip & dm & at_gt65), len(dm & at_gt65)))
output.append(('pnp  & dm_at_lt15',       len(pnp  & dm & at_lt15), len(dm & at_lt15)))
output.append(('pnp  & dm_at_1564',       len(pnp  & dm & at_1564), len(dm & at_1564)))
output.append(('pnp  & dm_at_gt65',       len(pnp  & dm & at_gt65), len(dm & at_gt65)))

i = -1
for indicador in output:
    i+= 1
    print(i, indicador)

end = dt.datetime.utcnow()

print(start)
print(end)
print(end - start)
