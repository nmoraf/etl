import sisapUtils as u
import pandas as pd
from datetime import timedelta, date


def daterange_str(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        day = start_date + timedelta(n)
        yield day.strftime("%Y-%m-%d")

start_date = date(2020, 3, 1)
tomorrow = date.today() + timedelta(1)

sql = """
    SELECT '{d}' as snapshot
        , residencia AS resi_cod
        , count(*) AS residents
        -- , sum(decode(ESTAT, 'Confirmat',1,'Possible',1, 0)) AS casos
        , sum(CASE WHEN DATE '{d}' - cas_data >= 0 AND ESTAT = 'Confirmat' THEN 1 ELSE 0 END) AS confirmats
        -- , sum(decode(Estat,'Possible',1,0)) AS  possibles
        -- , sum(CASE ESTAT WHEN 'Confirmat' THEN 0 WHEN 'Possible' THEN 0 ELSE 1 END) AS suceptibles
        ,  count(*) - sum(CASE WHEN DATE '{d}' - cas_data >= 0 AND ESTAT = 'Confirmat' THEN 1 ELSE 0 END) as suceptibles
    FROM
        PREDUFFA.SISAP_COVID_RES_MASTER
                INNER JOIN preduffa.SISAP_COVID_CAT_RESIDENCIA cat_res
         ON residencia = cat_res.cod AND tipus = 1
    WHERE
        DATE '{d}' BETWEEN
            least(entrada,
                    CASE WHEN cas_data IS NULL THEN 
                        CASE motiu WHEN 'cataleg'  THEN DATE '2020-03-01'
                                WHEN 'sense up' THEN DATE '2020-03-01' ELSE
                        entrada END
                    ELSE cas_data
                    END) 
            AND nvl2(sortida,sortida,sysdate)
    GROUP BY
        residencia
"""

data = []
for day in daterange_str(start_date, tomorrow):
    for row in u.getAll(sql.format(d= day), 'redics'):
        data.append(row)

cols = ['snapshot', 'resi_cod', 'residents', 'confirmats', 'suceptibles']

data_df = pd.DataFrame(data= data, columns= cols)

data_df['residencia_afectada'] = [1 if x > 0 else 0 for x in data_df['confirmats']]



data_df['exposats'] =  0
data_df['exposats'] = data_df['suceptibles'][data_df['residencia_afectada']==1]

# [x for x in data_df['exposats'][data_df['residencia_afectada']==1]]

# data_df.groupby(['residencia_afectada']).agg({'exposats':'sum', 'resi_cod':'count'})

# data_df.info()

first_date = data_df[data_df['residencia_afectada']==1].groupby(['resi_cod']).agg({'snapshot':'min'})

# data_df.merge(first_date, on='resi_cod').to_excel(u.tempFolder + 'estimaPCR3.xlsx')

data_df_first = data_df.merge(first_date, on='resi_cod', how='left')
data_df_first = data_df_first[data_df_first['snapshot_x']==data_df_first['snapshot_y']]

data_df_first['snapshot_y'] = pd.to_datetime(data_df_first['snapshot_y'], format = "%Y-%m-%d")
data_df_first['snapshot_3d'] = data_df_first['snapshot_y']+timedelta(3)
data_df_first['snapshot_7d'] = data_df_first['snapshot_y']+timedelta(7)

pd.melt(data_df_first
        ,id_vars=["snapshot_x","resi_cod","confirmats","exposats","residencia_afectada","residents","suceptibles"]
        ,var_name="ronda"
        ,value_name="data").groupby(
            ["ronda","data"]).agg(
            {'residents':'sum', 'residencia_afectada':'count'}).to_excel(
            u.tempFolder + 'estimaPCR5.xlsx', merge_cells=True
            )

#data_df[data_df['confirmats'] > 0].groupby(['resi_cod']).agg({min: 'snapshot'})

data_df[pd.isna(data_df['first_date'])]


#data_df.to_excel(u.tempFolder + 'estimaPCR2.xlsx')

