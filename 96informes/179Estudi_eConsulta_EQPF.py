# -*- coding: utf8 -*-

"""
.
"""

from collections import defaultdict,Counter
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = "SISAP_ECONSULTA_EQPF"
db = "redics"
   
class EConsultaeqpf(object):
    """."""

    def __init__(self):
        """."""    
        self.get_professionals()
        self.get_EQPF()
        self.upload_data()
        
    def get_professionals(self):
        """."""
        self.professionals = defaultdict(list)
        sql = "select left(ide_numcol, 8),ide_dni from import.cat_pritb992"
        for numcol, dni in u.getAll(sql, "import"):
            self.professionals[numcol].append(dni)
            
    
    def get_EQPF(self):
        """."""
        results = {}
        self.resultats = []
        a = 0
        sql = "select up, collegiat, nvl(punts,0) from pdptb213 where any_carrega='2017' and mes_carrega='12'"
        for sector in u.sectors:
            sectoraf = sector + 'af'
            for up, numcol, punts in u.getAll(sql, sectoraf):
                if numcol in self.professionals:
                    for dni in self.professionals[numcol]:
                        results[(up, numcol, u.getNif(dni), punts)] = True        
                else:
                    a+= 1
        print a
        for (up, numcol, dni, punts), t in results.items():
            self.resultats.append([up, numcol, u.getNif(dni), punts])
                
    def upload_data(self):
        """."""
        columns = [ "up varchar2(5)", "collegiat varchar2(14)", "dni varchar2(14)","EQPF number"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.resultats, tb, db)
        u.grantSelect(tb, ("PREDUPRP", "PREDUEHE"), db)

                    
if __name__ == '__main__':
    EConsultaeqpf()
