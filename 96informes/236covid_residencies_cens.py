import sisapUtils as u
import os
import sys

# cwd = os.getcwd()
# scriptFolder = '96informes'
WATCHERS = ("PREDULMB", "PREDUPRP", "PREDUMMP")
pathname = os.path.dirname(sys.argv[0])
filename = 'map_residencia_autoritzacio.txt'
fullPath = u.readCSV(os.path.join(pathname, filename))

db = 'redics'
tb = 'sisap_map_residencia_aut'
cols = ['resi_cod varchar(13)','aut_cod varchar(6)']

# fullPath = u.readCSV(os.path.join(cwd, scriptFolder, filename))
upload = []
for row in fullPath:
    upload.append(tuple(row))

u.createTable(tb, "({})".format(",".join(cols)), db)
u.uploadOra(upload, tb, db)

u.grantSelect(tb, WATCHERS, db)
