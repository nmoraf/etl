# coding: utf8

debug = False

from sisapUtils import *
from collections import defaultdict,Counter
import datetime

imp = 'import'

recomptes = Counter()
sql = "select id_cip_sec, pf_periode, pf_num_visites_r, pf_num_sessions_r from rhb16"
for id, periode, visites, sessions in getAll(sql, imp):
    if 201601 <= int(periode) <= 201712:
        total = visites + sessions
        recomptes[(periode)] += total
       
    
upload = []
for (periode),r in recomptes.items():
    upload.append([periode, r])

file = tempFolder + 'Visites_rhb16.txt'
writeCSV(file, upload, sep=';')  