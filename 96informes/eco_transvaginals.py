# coding: iso-8859-1

from sisapUtils import *
import random
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

#Validation
write=True
validation=True


alt = "altres"
nod = "nodrizas"
imp="import"
year=2017

def get_date_dextraccio(self):
    """ Consigue la fecha de hace 12 meses y la fecha actual.
        Devuelve a tuple de dos fechas en in datetime.date format
    """
    sql="select date_add(date_add(data_ext, interval - 12 month), interval + 1 day), data_ext from nodrizas.dextraccio;"
    for past, current in getAll(sql,nod):
        return past,current	

def get_tables_year(year):
    """ Consigue las tablas del any year (que va a ser 2017 en este caso asi que se saltan directamente las que ponen 2010/2011)
        Para ello selecciona una fecha de visita de cada una de las particiones y comprueba que el any de esta fecha sea el any deseado.
        Si es asi, se anyade a una lista que es lo que devuelve la funcion
    """
    tables = []
    for table in getSubTables('visites'):
        dat = [dat for dat, in getAll("select visi_data_visita from {} limit 1".format(table), imp)][0]
        #dat, = getOne("select visi_data_visita from {} limit 1".format(table), 'import')
        if dat.year == year:
            tables.append(table)
    return tables

def get_ass_desc():
    """ Se asocia cada up con su br, su descripcion y la descripcion del assir en un diccionario.
        Se devuelve un diccionario de {up:(br,up_desc,assir_desc)}
    """
    sql="select up, br,up_desc,assir from {}.ass_centres".format(nod)
    return {up:(br,up_desc,assir_desc) for up,br,up_desc,assir_desc in getAll(sql,nod)}


def get_vis_modul(params):
    """ Se obtiene el numero de visitas realizadas en las que el tipo fue ECOU o ECOG y que el servicio es de GIN en el any year (2017)
        Se devuelve un diccionario de Counters {up:{modul: n visitas}}
    """
    table,year=params
    vis_mod=defaultdict(Counter)
    sql="select visi_up,  visi_tipus_visita, visi_data_visita from {} where visi_situacio_visita='R' and visi_tipus_visita in ('ECOU','ECOG') and visi_servei_codi_servei like '%GIN%' and visi_data_baixa=0".format(table)
    for up,modul, data in getAll(sql,imp):
        if data.year==year:
            vis_mod[up][modul]+=1
    return vis_mod

def multiprocess_visites(year):
    """ Se realiza un multiproceso: se aplica la funcion get_vis_modul para cada una de las tablas de get_tables_year(year) que dan las tablas con datos
        de vistas del any year (2017). Se obtienen diccionario de counters para cada tabla y se combina en una variable global con todos los datos de las tablas.
        Se devuelve un diccionario de counters {up {modul: n visites}}
    """
    tables=get_tables_year(year)
    print(tables)
    printTime("Tables done")
    vis_mod_final=defaultdict(Counter)
    for vis_mod in multiprocess(get_vis_modul, [(table,year) for table in tables]):
        for up in vis_mod:
            for modul,n in vis_mod[up].iteritems():
                vis_mod_final[up][modul]+=n
    return vis_mod_final

def get_rows(vis_mod_final,up_assir):
    return [ (up_assir[up][0], up_assir[up][1], up_assir[up][2], modul, n) for up in vis_mod_final for modul,n in  vis_mod_final[up].iteritems()]



###
if __name__ == '__main__':
# comencem!
    printTime('inici')
    
    vis_mod_final=multiprocess_visites(year)
    printTime("multiprocess")
    up_assir=get_ass_desc()
    
    up_rows=get_rows(vis_mod_final,up_assir)
    printTime("rows",len(up_rows))
    #tabac_values={id for lista in tabac_id.values() for id in lista}
    #print(all_id- tabac_values)

    if write:
        with open("eco_transvag_informe.txt","wb") as out_file:
            row_format ="{:>35}" * len(up_rows[0])+"\n"
            header=["UP","UP DESCRIPCIÒ","ASSIR DESCRIPCIÒ","MODUL", "N VISITES"]
            out_file.write(row_format.format(*header))
            for row in up_rows:
                out_file.write(row_format.format(*row))