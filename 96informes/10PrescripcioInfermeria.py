from sisapUtils import *
from collections import defaultdict

ora = 'redics'
my = 'import'
table = 'sisap_prescripcio_infermeria'
distinct = table + '_distinct'

dataIni = '20130101'
dataFi = '20141231'

def downloadVersions():
    
    file = tempFolder + 'versions.txt'
    execute('create table {} (id_cip_sec int,hppf_usuari_cip varchar(40),codi_sector varchar(4),hppf_pmc_codi int,hppf_num_prod int,hppf_versio int,tipus varchar(1),hppf_pf_codi int,hppf_durada int,hppf_num_env int,hppf_data_alta date,hppf_data_fi date,hppf_data_mod date,hppf_amb_cod_up varchar(5),hppf_usuari_ora varchar(20),hppf_visi_codi_servei varchar(5),hppf_visi_data_visita date)'.format(table),my)
    sql = "select /*+NO_INDEX(a)*/ id_cip_sec,hppf_usuari_cip,a.codi_sector,hppf_pmc_codi,hppf_num_prod,hppf_versio,decode(hppf_versio,1,'A','M'),hppf_pf_codi,hppf_durada,hppf_num_env,to_char(hppf_data_alta,'YYYYMMDD'),to_char(hppf_data_fi,'YYYYMMDD'),to_char(hppf_data_mod,'YYYYMMDD'),hppf_amb_cod_up,hppf_usuari_ora,hppf_visi_codi_servei,to_char(hppf_visi_data_visita,'YYYYMMDD') from redics.ppftb116r a,(select distinct ide_usuari from pritb992 where ide_categ_prof_c='30999') b,sisap_u11 c where a.hppf_usuari_cip=c.hash_a and a.codi_sector=c.codi_sector and hppf_usuari_ora=ide_usuari and hppf_data_mod between to_date('{}','YYYYMMDD') and to_date('{}','YYYYMMDD') and hppf_pf_codi is not null".format(dataIni,dataFi)
    with openCSV(file) as c:
        for row in getAll(sql,ora):
            c.writerow(row)
    loadData(file,table,my)
    execute("drop table if exists {}".format(distinct),my)
    execute("create table {} as select distinct id_cip_sec from {}".format(distinct,table),my)
    execute("alter table {} add primary key id (id_cip_sec)".format(distinct),my)

try:
    fet, = getOne("select 1 from information_schema.tables where table_schema='{}' and table_name='{}'".format(my,table),my)
except TypeError:
    downloadVersions()

pob = {}
sql = "select a.id_cip_sec,usua_data_naixement,usua_sexe from assignada a inner join {} b on a.id_cip_sec=b.id_cip_sec".format(distinct)
for id,naix,sex in getAll(sql,my):
    pob[id] = naix,sex

crg = {}
sql = "select a.id_cip_sec,crg from crg a inner join {} b on a.id_cip_sec=b.id_cip_sec".format(distinct)
for id,val in getAll(sql,my):
    crg[id] = val

prob = defaultdict(list)
sql = "select a.id_cip_sec,pr_v_dat,pr_dum,pr_cod_ps from problemes a inner join {} b on a.id_cip_sec=b.id_cip_sec where pr_v_dat between '{}' and '{}'".format(distinct,dataIni,dataFi)
for id,vis,dum,ps in getAll(sql,my):
    if ps not in prob[id,vis]:
        prob[id,vis].append(ps)
    if ps not in prob[id,dum]:
        prob[id,dum].append(ps)

ag = defaultdict(list)
sql = "select a.id_cip_sec,cesp_data_alta,cesp_cod_mce from aguda a inner join {} b on a.id_cip_sec=b.id_cip_sec where cesp_data_alta between '{}' and '{}'".format(distinct,dataIni,dataFi)
for id,dat,cod in getAll(sql,my):
    ag[id,dat].append(cod)

atc = {}
sql = "select pf_codi,pf_cod_atc from cpftb006"
for pf,codi in getAll(sql,ora):
    atc[pf] = codi

dni = {}
sql = "select ide_usuari,ide_dni from pritb992"
for usu,num in getAll(sql,ora):
    dni[usu] = num

upload = []
i = 1
sql = "select * from {}".format(table)
for id,hash,sector,pmc,prod,versio,tipus,pf,durada,env,dalta,dfi,dmod,up,usu,serv,dvisita in getAll(sql,my):
    i += 1
    naix,sex = pob[id]
    crgVal = crg[id] if id in crg else ''
    ps = ';'.join(prob[id,dmod]) if (id,dmod) in prob else ''
    aguda = ';'.join(ag[id,dmod]) if (id,dmod) in ag else ''
    atcCodi = atc[pf]
    dniCodi = dni[usu]
    upload.append([i,sector,hash,naix,sex,crgVal,pmc,prod,versio,tipus,pf,atcCodi,durada,env,dalta,dfi,dmod,up,usu,dniCodi,serv,dvisita,ps,aguda])
      
try:
    execute('drop table {}'.format(table),ora)
except:
    pass
execute('create table {} (id int,codi_sector varchar2(4),hppf_usuari_cip varchar(40),usua_data_naixement date,usua_sexe varchar2(1),usua_crg varchar2(5),hppf_pmc_codi int,hppf_num_prod int,hppf_versio int,tipus varchar(1),hppf_pf_codi int,atc_codi varchar2(7),hppf_durada int,hppf_num_env int,hppf_data_alta date,hppf_data_fi date,hppf_data_mod date,hppf_amb_cod_up varchar(5),hppf_usuari_ora varchar(20),ide_dni varchar2(10),hppf_visi_codi_servei varchar(5),hppf_visi_data_visita date,ps varchar2(250),ag varchar2(250))'.format(table),ora)
uploadOra(upload,table,ora)
calcStatistics(table,ora)

users= ['PREDUECR','PREDUMMP','PREDUPRP']
for user in users:
    execute("grant select on {} to {}".format(table,user),ora)
