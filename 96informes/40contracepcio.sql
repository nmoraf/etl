# proces jacobo, cal canviar dates a 243
# en acabar, abans es treia la taula aco3 a excel però ara es carrega a redics amb el proc 40contracepcio.py

drop database if exists anticoncepcio;
CREATE database anticoncepcio;
USE anticoncepcio;

CREATE TABLE poblacion
select 
	id_cip_sec,
	up,
	uba,
	sexe, 
	data_naix, 
	nacionalitat
from nodrizas.assignada_tot
group by id_cip_sec,
	up,
	uba,
	sexe, 
	data_naix, 
	nacionalitat
;	
--AÑADIR NACIONALIDAD
	
create table pob_2
select 
	id_cip_sec,
	up,
	uba,
	sexe, 
	data_naix, 
	nacionalitat,
	desc_nac
from anticoncepcio.poblacion
Left join nodrizas.cat_nacionalitat
on anticoncepcio.poblacion.nacionalitat=nodrizas.cat_nacionalitat.codi_nac;
;
--CREAR TABLA CONJUNTA CENTROS

create table ups
select * from nodrizas.cat_centres
;
ALTER TABLE UPS CHANGE scs_codi UP varchar(50);
ALTER TABLE UPS CHANGE ICS_DESC UP_DESC varchar(50);

create table assir
select * from nodrizas.ass_centres
;
CREATE TABLE CENTROS
SELECT UP, amb_desc, sap_desc, UP_desc FROM ups
UNION ALL
SELECT UP, amb_desc, sap_desc, UP_desc FROM assir
;

CREATE TABLE CENTROS2
SELECT UP, amb_desc, sap_desc, UP_desc FROM CENTROS
#UNION ALL
#SELECT UP, amb_desc, sap_desc, UP_desc FROM centrosmis
;
CREATE TABLE CENTROSDEF
SELECT CENTROS2.UP, CENTROS2.amb_desc, CENTROS2.sap_desc, CENTROS2.UP_desc, ASSIR 
FROM CENTROS2
Left join nodrizas.ass_centres
on anticoncepcio.CENTROS2.up=nodrizas.ass_centres.up;
;
create table centrosdef2
select distinct *
from centrosdef
;
--DEPURAR DUPLICADOS

delete from centrosdef2
where up_desc='ATENCIÓ CONTINUADA REUS' and UP='00071'
;
delete from centrosdef2
where up_desc='NIVELL HOSP. CAP II TORRENT DE LES FLORS' and UP='00527'
;
delete from centrosdef2
where up_desc='ACUT BARCELONA CIUTAT' and UP='04842'
;
delete from centrosdef2
where up_desc='PAC SANT ANDREU' and UP='07207'
;
--AÑADIR NOMBRE CENTROS

create table pob_3
select 
	id_cip_sec,
	pob_2.up,
	sexe, 
	data_naix, 
	desc_nac,
	amb_desc,
	sap_desc,
	UP_desc,
	assir
from anticoncepcio.pob_2
Left join centrosdef2
on anticoncepcio.pob_2.up=anticoncepcio.centrosdef2.up;	
;
--SELECCION CODIGOS ANTICONCEPCION

CREATE TABLE anticon1
select 
	id_cip_sec,
	cesp_cod_mce,
	cesp_up,
	cesp_data_alta
from import.aguda
WHERE cesp_cod_mce LIKE 'ANTIC' 
;

select count(*) from anticon1
;
CREATE TABLE anticon2
select 
	id_cip_sec,
	val_var,
	val_up,
	val_data,
	val_val
from import.assir216
WHERE val_var LIKE 'PAC4001' OR val_var LIKE 'VPW2001'
;
CREATE TABLE anticon3
select 
	id_cip_sec,
	au_cod_ac,
	au_up,
	au_dat_act
from import.activitats
WHERE au_cod_ac LIKE 'IPC' OR au_cod_ac LIKE 'PAD23' OR au_cod_ac LIKE 'URG27' OR au_cod_ac LIKE 'CURG27'
;
insert into anticon3
select 
	id_cip_sec,
	pr_cod_ps as au_cod_ac,
	pr_up as au_up,
	pr_dde as au_dat_act
from import.problemes
WHERE pr_cod_ps='Z30.06' and pr_hist=1 and pr_cod_o_ps = 'C'
;
insert into anticon3
select 
	id_cip_sec,
	vu_cod_vs as au_cod_ac,
	vu_up as au_up,
	vu_dat_act as au_dat_act
from import.variables
WHERE vu_cod_vs = 'PW2001'
;

--HOMOGENIZAR CAMPOS

ALTER TABLE anticon1 CHANGE id_cip_sec ID int (10);
ALTER TABLE anticon1 CHANGE cesp_cod_mce COD varchar(50);
ALTER TABLE anticon1 CHANGE cesp_up UP varchar(50);
ALTER TABLE anticon1 CHANGE cesp_data_alta FECHA varchar(50);
alter table anticon1
ADD MOTIVO varchar(50) DEFAULT 'ATENCIO PER ANTICONCEPCIO'
;
ALTER TABLE anticon2 CHANGE id_cip_sec ID int(10);
ALTER TABLE anticon2 CHANGE val_var COD varchar(50);
ALTER TABLE anticon2 CHANGE val_up UP varchar(50);
ALTER TABLE anticon2 CHANGE val_data FECHA varchar(50);
ALTER TABLE anticon2 CHANGE val_val MOTIVO varchar(50);

ALTER TABLE anticon3 CHANGE id_cip_sec ID int(10);
ALTER TABLE anticon3 CHANGE au_cod_ac COD varchar(50);
ALTER TABLE anticon3 CHANGE au_up UP varchar(50);
ALTER TABLE anticon3 CHANGE au_dat_act FECHA varchar(50);
alter table anticon3
ADD MOTIVO varchar(50) DEFAULT 'ADMINISTRACIO FARMAC POSTCOITAL'
;
--AÑADIR DESCRIPCION UP

create table anticon1a
select 
	ID,
	COD,
	ANTICON1.UP,
	FECHA,
	MOTIVO,
	amb_desc as AMBIT,
	sap_desc as SAP,
	UP_desc as ICS,
	ASSIR
from anticoncepcio.anticon1
Left join CENTROSDEF2
on anticoncepcio.anticon1.up=CENTROSDEF2.UP;
;
create table anticon2a
select 
	ID,
	COD,
	ANTICON2.UP,
	FECHA,
	MOTIVO,
	amb_desc as AMBIT,
	sap_desc as SAP,
	UP_desc as ICS,
	ASSIR
from anticoncepcio.anticon2
Left join CENTROSDEF2
on anticoncepcio.anticon2.up=CENTROSDEF2.UP;
;
create table anticon3a
select 
	ID,
	COD,
	ANTICON3.UP,
	FECHA,
	MOTIVO,
	amb_desc as AMBIT,
	sap_desc as SAP,
	UP_desc as ICS,
	ASSIR
from anticoncepcio.anticon3
Left join CENTROSDEF2
on anticoncepcio.anticon3.up=CENTROSDEF2.UP;
;
--UNIR TABLAS ANTICONCEPCION

CREATE TABLE anticon4
SELECT ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO FROM anticon1A
UNION ALL
SELECT ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO FROM anticon3A
;
CREATE TABLE anticon4a
SELECT ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO FROM anticon4
group by ID, fecha
;
CREATE TABLE anticon5
SELECT ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO FROM anticon4a
UNION ALL
SELECT ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO FROM anticon2a
;
--SELECCIONAR AÑO 2014

CREATE TABLE anticon6
SELECT ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO FROM anticon5
WHERE FECHA BETWEEN '2018-01-01' AND '2018-12-31'
;
--RECUENTO ACO

CREATE TABLE anticon7
select ID, count(COD) as TOTAL_ACO from anticon6
GROUP BY ID
;
--SELECCIONAR PRIMERA FECHA

CREATE TABLE anticon8
SELECT ID, min(FECHA) as PRIMERAF FROM anticon6
GROUP BY ID
;
--SELECCIONAR ULTIMA

CREATE TABLE anticon9
SELECT ID, MAX(FECHA) as ULTIMAF FROM anticon6
GROUP BY ID
;
--UNIR FECHAS

create table anticon10A
select 
	anticon8.ID,
	anticon8.PRIMERAF,
	anticon9.ULTIMAF
From anticon8
Left join anticon9
on anticon8.ID=anticon9.ID
;
--UNIR PRIMERA Y ULTIMA FECHA DE ACO

create table anticon10b
select
anticon6.ID, COD, UP, AMBIT, SAP, ICS, ASSIR, FECHA, MOTIVO, primeraf, ultimaf
from anticon6
left join anticon10a
on anticon6.id=anticon10a.id
;
create table anticon10c
select * from anticon10b
where fecha = primeraf or fecha = ultimaf
;
--SELECCIONAR ULTIMAS VISITAS

create table anticon10d
select * from anticon10c
where ultimaf <> primeraf
;
create table anticon10e
select * from anticon10d
where fecha = ultimaf
;
--CREAR TABLA PRIMERA Y ULTIMA VISITA

CREATE TABLE visitas2
select
anticon10b.id,
anticon10b.PRIMERAF, 
anticon10b.COD as COD1, 
anticon10b.UP AS UP1, 
anticon10b.AMBIT AS AMBIT1, 
anticon10b.SAP AS SAP1, 
anticon10b.ICS AS ICS1, 
anticon10b.ASSIR AS ASSIR1, 
anticon10b.FECHA AS FECHA1, 
anticon10b.MOTIVO AS MOTIVO1,
anticon10b.ULTIMAF, 
anticon10e.COD AS COD2, 
anticon10e.UP AS UP2, 
anticon10e.AMBIT AS AMBIT2, 
anticon10e.SAP AS SAP2, 
anticon10e.ICS AS ICS2, 
anticon10e.ASSIR AS ASSIR2, 
anticon10e.FECHA AS FECHA2, 
anticon10e.MOTIVO AS MOTIVO2
FROM anticon10b
left join anticon10e
on anticon10b.id=anticon10e.id
GROUP BY ID
;
--UNIR TOTAL ANTICONCEPTIVOS

create table anticon11
select 
	visitas2.ID,
	visitas2.COD1,
	visitas2.UP1,
	visitas2.AMBIT1, 
	visitas2.SAP1, 
	visitas2.ICS1,
	visitas2.ASSIR1, 
	visitas2.PRIMERAF, 
	visitas2.UP2,
	visitas2.AMBIT2, 
	visitas2.SAP2, 
	visitas2.ICS2,
	visitas2.ASSIR2, 
	visitas2.ULTIMAF,
	visitas2.MOTIVO1,
	visitas2.MOTIVO2,
	anticon7.TOTAL_ACO
From visitas2
Left join anticon7
on visitas2.ID=anticon7.ID
;
UPDATE anticon11
SET ULTIMAF = '0000-00-00'
WHERE TOTAL_ACO = '1'
;
UPDATE anticon11
SET MOTIVO2 = ''
WHERE TOTAL_ACO = '1'
;
--COMPROBACION

SELECT COUNT(*) FROM ANTICON6;
SELECT SUM(TOTAL_ACO)FROM ANTICON11;
#SELECT ID, COUNT(*) FROM ANTICON10
#GROUP BY ID
#ORDER BY COUNT(*)DESC;
SELECT COUNT(*) FROM ANTICON11;

--CREACION INDICES

alter table POB_3
ADD INDEX (id_cip_sec)
;
alter table anticon11
ADD INDEX (ID)
;
--AÑADIR DATOS POBLACION

create table ACO
select 
	anticon11.ID,
	POB_3.DATA_NAIX,
	POB_3.SEXE,
	POB_3.UP AS UP_ASSIGNAT,
	POB_3.AMB_DESC AS AMB_ASSIGNAT,
	POB_3.SAP_DESC AS SAP_ASSIGNAT,
	POB_3.UP_DESC AS ICS_ASSIGNAT,
	POB_3.ASSIR AS ASSIR_ASSIGNAT,
	anticon11.UP1 AS UP_ADM_P,
	anticon11.AMBIT1 AS AMBITP, 
	anticon11.SAP1 AS SAPP, 
	anticon11.ICS1 AS ICSP,
	anticon11.ASSIR1 AS ASSIRP, 
	anticon11.UP2 AS UP_ADMF,
	anticon11.AMBIT2 AS AMBITF, 
	anticon11.SAP2 AS SAPF, 
	anticon11.ICS2 AS ICSF,
	anticon11.ASSIR2 AS ASSIRF, 
	anticon11.TOTAL_ACO,
	anticon11.PRIMERAF,
	anticon11.ULTIMAF,
	POB_3.DESC_NAC,
	anticon11.COD1 as cod,
	anticon11.MOTIVO1,
	anticon11.MOTIVO2
From anticon11
Left join pob_3
on anticon11.ID=pob_3.id_cip_sec
;
--CALCULO EDAD

CREATE TABLE aco2
select 
	ID,
	SEXE,
	extract(YEAR from primeraf)-extract(Year FROM(data_naix))as EDAT, 
	UP_ASSIGNAT,
	AMB_ASSIGNAT,
	SAP_ASSIGNAT,
	ICS_ASSIGNAT,
	UP_ADM_P,
	AMBITP, 
	SAPP, 
	ICSP,
	UP_ADMF,
	AMBITF, 
	SAPF, 
	ICSF,
	TOTAL_ACO,
	PRIMERAF,
	ULTIMAF,
	DESC_NAC,
	COD,
	motivo1 as MOTIVOP,
	motivo2 as MOTIVOF
from ACO
;
--CORREGIR ALGUN CENTRO QUE FALTA

UPDATE aco2
SET icsp = 'PAC BANYOLES'
WHERE UP_ADM_P = '04883'
;
UPDATE aco2
SET icsF = 'PAC BANYOLES'
WHERE UP_ADMF = '04883'
;
--AÑADIR NACIONALIDAD A POB ASIGNADA (ESPAÑA)

UPDATE ACO2
SET DESC_NAC='ESPANYA'
WHERE DESC_NAC IS NULL AND UP_ASSIGNAT IS NOT NULL;
;
--AÑADIR HASH

alter table aco2
ADD INDEX (ID)
;

CREATE TABLE hash
select 
	id_cip_sec,
	HASH_D,
	CODI_SECTOR
	From import.u11
;	
alter table hash
ADD INDEX (id_cip_sec)
;

CREATE TABLE aco3
select 
	HASH_D,
	CODI_SECTOR,
	SEXE,
	EDAT, 
	UP_ASSIGNAT,
	AMB_ASSIGNAT,
	SAP_ASSIGNAT,
	ICS_ASSIGNAT,
	UP_ADM_P,
	AMBITP, 
	SAPP, 
	ICSP,
	UP_ADMF,
	AMBITF, 
	SAPF, 
	ICSF,
	TOTAL_ACO,
	PRIMERAF,
	ULTIMAF,
	DESC_NAC,
	COD,
	MOTIVOP,
	MOTIVOF
From aco2
Left join hash
on aco2.ID=hash.id_cip_sec
;
--COMPROBACION NULOS

create table ACOCOMP
select 
	anticoncepcio.anticon11.ID,
	nodrizas.assignada_tot.SEXE,
	nodrizas.assignada_tot.UP AS UP_OR,
	nodrizas.assignada_tot.EDAT,
	anticoncepcio.anticon11.UP1, 
	anticoncepcio.anticon11.PRIMERAF, 
	anticoncepcio.anticon11.UP2, 
	anticoncepcio.anticon11.ULTIMAF,
	anticoncepcio.anticon11.TOTAL_ACO,
	anticoncepcio.anticon11.MOTIVO1,
    anticoncepcio.anticon11.MOTIVO2
	#anticoncepcio.anticon11.COD
From anticoncepcio.anticon11
Left join nodrizas.assignada_tot
on anticoncepcio.anticon11.ID=nodrizas.assignada_tot.id_cip_sec
;
select count(*)
from aco3
where hash_d is null
;
select count(*)
from acocomp
where sexe is null
;
select sum(total_aco)
from aco
;
SELECT COUNT(*), UP_ADMf
FROM ACO2
WHERE ICSP IS NULL
GROUP BY UP_ADMf
;

#SELECT COUNT(*), ICS_DESC
#FROM pob_4
#WHERE ICS_DESC IS NULL
#GROUP BY UP
#;
DROP table POB_3;
DROP TABLE anticon1a;
DROP TABLE anticon2a;
DROP TABLE anticon3a;
DROP TABLE anticon4a;
DROP TABLE anticon5;
DROP TABLE anticon6;
DROP TABLE anticon7;
DROP TABLE anticon8;
DROP TABLE anticon9;
#DROP TABLE anticon10;
DROP TABLE anticon11;
DROP TABLE ANTICON10A;
DROP TABLE ANTICON10B;

#DROP TABLE BBB;
#DROP TABLE CCC;
#DROP TABLE DDD;
#DROP TABLE PRIMERAV;
#DROP TABLE ULTIMAV;
#DROP TABLE visitas;
DROP TABLE ACO;
DROP TABLE aco2;

DROP TABLE CENTROS;
DROP TABLE CENTROS2;
DROP TABLE CENTROSDEF;
#DROP TABLE CENTROSMIS;

rename table aco3 to permanent.anticoncepcio;
drop database anticoncepcio;
