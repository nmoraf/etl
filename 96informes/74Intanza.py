# coding: iso-8859-1
#Carmen Cabezas Intanza grip, Ermengol, Novembre 2016
from sisapUtils import *
from collections import defaultdict, Counter

imp = 'import'
db = 'test'
nod = 'nodrizas'

temp = 'cat_vacunes'

dini ='20150101'
dfi = '20161123'


centres = {}
sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc from cat_centres where ep='0208' and ics_codi like 'BR%'"
for up, br, desc, sap, ambit in getAll(sql, nod):
    centres[up] = {'eap': desc, 'sap': sap, 'amb': ambit}

pob = {}
sql = 'select id_cip_sec, up, edat from assignada_tot where edat>14'
for id, up, edat in getAll(sql, nod):
    gedat = 0
    if edat <60:
        gedat = 'Menors 60'
    elif 60 <= edat <= 64:
        gedat = 'Entre 60 i 64'
    elif edat >64:
        gedat = 'Majors 65'
    else:
        gedat = 'error'
    try:
        eap = centres[up]['eap']
        sap = centres[up]['sap']
        ambit = centres[up]['amb']
    except KeyError:
        continue
    pob[id] = {'up': up, 'edat': gedat, 'eap': eap, 'sap': sap, 'ambit': ambit}


#vacunes administrades

sql = 'drop table if exists {}'.format(temp)
execute(sql, db)
sql = "create table {} as select vacu_cod vacuna,if(vacu_an_cod = '',hom_an_cod,vacu_an_cod) antigen,if(vacu_tipus_dosis = 0,300,vacu_tipus_dosis) dosis,if(vacu_model_nou='S','NEW','OLD') model from import.cat_prstb040 a left join import.cat_prstb062 b on a.vacu_cod_hom=b.hom_ch_cod".format(temp)
execute(sql, db)

Antigen = {}
sql = "select vacuna from {0} where antigen='A-GRIP'".format(temp)
for vac, in getAll(sql, db):
    Antigen[vac] = True
    
Intanza = {}
sql = "select pf_codi from import.cat_cpftb006 where pf_desc_gen like ('INTANZA%')"
for pf, in getAll(sql, imp):
    Intanza[pf] = True

nVacunes = Counter()
sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}_%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    printTime(particio)
    vacunes = {}
    sql = "select id_cip_sec,va_u_cod,date_format(va_u_data_vac,'%Y%m'),date_format(va_u_data_vac,'%Y%m%d'), va_u_pf_cod from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, anys, dat, pf in getAll(sql, imp):
        if dini <= dat <= dfi:
            try:
                Antigen[vac]
            except KeyError:
                continue
            try:
                up = pob[id]['up']
                edat = pob[id]['edat']
                eap = pob[id]['eap']
                sap = pob[id]['sap']
                ambit = pob[id]['ambit']
            except KeyError:
                continue
            try:
                Intanza[pf]
                marca = 'Intanza'
            except KeyError:
                marca = 'Altres'
            nVacunes[(up, eap, sap, ambit, edat, marca, anys)] += 1

upload = []
for (up, eap, sap, ambit,edat, marca, anys), d in nVacunes.items():
    upload.append([up, eap, sap, ambit, edat, anys, marca, d])

file = tempFolder + 'antigripal_per mes.txt'
writeCSV(file, upload, sep=';')

