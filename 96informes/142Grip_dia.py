
# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
import datetime
from collections import defaultdict,Counter


imp = 'import'


inici = datetime.date(2008, 01, 01)
final = datetime.date(2018, 01, 01)

pob = {}

sql = 'select id_cip_sec, usua_data_naixement, usua_abs_codi_abs, usua_sexe from assignada'
for id, naix, abs, sexe in getAll(sql, imp):
    pob[id] = {'naix': naix, 'sexe': sexe, 'abs': abs}

grip = Counter()
rec = 0
sql = "select id_cip_sec, pr_up, pr_dde, pr_v_dat from problemes where pr_cod_ps in ('J10','J10.0','J10.1','J10.8','J11','J11.0','J11.1','J11.8','J09') and pr_data_baixa is null and pr_cod_o_ps ='C'"
for id, up, dde, datv in getAll(sql, imp):
    if id in pob:
        naix = pob[id]['naix']
        sexe = pob[id]['sexe']
        abs = pob[id]['abs']
    else:
        rec += 1
        print id
        continue
    edat = yearsBetween(naix, dde)
    if edat < 15:
        grup = 'Menors 15 anys'
    elif edat >14:
        grup = 'Majors de 15 anys'
    grip[(dde, grup)] +=1

recomptes = {}   
for single_date in dateRange(inici, final):
    try:
        menors = grip[(single_date, 'Menors 15 anys')]
    except KeyError:
        menors = 0
    try:
        majors = grip[(single_date, 'Majors de 15 anys')]
    except KeyError:
        majors = 0
    recomptes[(single_date)] = {'majors': majors, 'menors': menors}
    

upload = []
for (single_date), n in recomptes.items():
    upload.append([single_date, n['menors'],n['majors']])
    
file = tempFolder + 'Grip_dia.txt'
writeCSV(file, upload, sep=';')
