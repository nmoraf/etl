# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta

codi="POCS2018"
imp="import"
nod="nodrizas"
alt="altres"

like_statement=['B20%', 'B21%', 'B23%', 'C81%', 'C82%', 'C83%', 'C84%', 'C85%', 'C90%', 'C91%', 'C92%', 'C93%', 'C94%', 
'C95%', 'C96%', 'D46%', 'D47%', 'D57%', 'D58%', 'D60%', 'D61%', 'D80%', 'E84%', 'F70%', 'F71%', 'F72%', 'F73%', 'F78%', 
'F79%', 'I05%', 'I06%', 'I09%', 'I15%', 'I25%', 'I31%', 'I35%', 'I50%', 'J43%', 'J44%', 'J84%', 'K70%', 'K74%', 'N05%', 
'N07%', 'Q90%', 'Z49%', 'Z74%', 'Z94%']

where_like=" ".join("or pr_cod_ps like '{}'".format(el) for el in like_statement)
in_statement=('B24','C97','D70','D84.9','D86.2','G20','G21','I27.0','I34.0','I36.1','I38','I42.6','I42.8','I48','I51.8',
'J63.1','J92.0','J96.1','J96.9','K50.9','N04','N18.0','N18.9','Y84.1','Z73.91')

codis_psicotropos='N05%'

ini=datetime.date(day=15,month=6,year=2018)
fi=datetime.date(day=15,month=9,year=2018)


def get_problemes(taula):
    """ Selecciona de import.problemas los ids que presenten un pr_cod_ps dentro de las opciones en in_statement o que empiecen con los caracteres que hay 
        en la variable like_statement, que se ha 'traducido' a query de sql en la variable where_like. import.problemes se filtra como se hace para crear eqa.problemes
        (pr_cod_o_ps = 'C' and pr_hist = 1, y que la fecha de baixa logica del servicio sea nula)
        De lo id que resultan, se coge la fecha de diagnostico (dde) y la de fin del problema de salud (dba). Si dba es nula, se le asigna la del fin del periodo de calculo.
        Por ultimo, se anyaden a un set aquellos ids para los que la dba es mayor o igual a la 
        del fin del periodo (es mas reciente o la misma)
    """
    sql="select id_cip_sec,pr_dde,pr_dba from {}\
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0 and (pr_cod_ps in {} {})".format(taula,in_statement,where_like)
    
    pacients=set()
    for id, dde,dba in getAll(sql,imp):
        #dba= fi if not dba else dba
        if not dba or dba >= fi:
            pacients.add(id)
    return pacients

def get_tractaments():
    """ Selecciona de import.tractament aquellos pacientes que estan en tratamiento con piscotropos (codigo padre de psicotropos en codis_psicotropos).
        Filtra por aquellos pacientes cuya fecha de fin de tratamiento es mayor o igual a la del fin de periodo de calculo.
        Devuelve un set de ids.
    """
    sql="select id_cip_sec,ppfmc_data_fi from import.tractaments where ppfmc_atccodi like '{}'".format(codis_psicotropos)
   
    return {id for id,data_fi in getAll(sql,imp) if data_fi >= fi}

def get_visi_periodo(taula):
    """Selecciona pacientes que tienen visitas realizadas dentro del periodo de calculo; en los que la fecha de visita es mas reciente que la de inicio de periodo
       pero mas antigua que la de fin de periodo.
       Devuelve un set de ids
    """
    sql="select id_cip_sec,visi_data_visita,visi_lloc_visita from {} where visi_situacio_visita='R'".format(taula)
    visites_lloc=defaultdict(set)
    for id,data,lloc in getAll(sql,imp): 
        if ini <= data <= fi:
            visites_lloc[lloc].add(id)
    return visites_lloc


def get_pocs_registre(taula):
    """Selecciona pacientes que tienen la actividad POCS realizada dentro del periodo de calculo; en los que la fecha de actividad es mas reciente que la de inicio de periodo
       pero mas antigua que la de fin de periodo.
       Devuelve un set de ids
    """
    sql="select id_cip_sec,au_dat_act from {} where au_cod_ac='POCS'".format(taula)
    act_lloc=defaultdict(set)
    for id,data in getAll(sql,imp): 
        if ini <= data <= fi:
            act_lloc['act'].add(id)
    return act_lloc

def get_tables_visites(table_parent,visi_field):
    """Selecciona las subtablas de la table_parent en las que el campo visi_field (una fecha) su año y mes son o los de inicio del periodo o los del fin del periodo.
       Devuelve una lista de las tablas que cumplen los criterios de fecha. 
    """
    tables = []
    for table in getSubTables(table_parent):
        #dat = [dat for dat, in getAll("select {}u_dat_act from {} limit 1".format(self.table_codi[0],table), imp)][0]
        dat, = getOne("select {} from {} limit 1".format(visi_field,table), 'import')
        if dat.year in (ini.year,fi.year) and dat.month in (ini.month,fi.month):
            tables.append(table)
    printTime("Tables")
    return tables

def get_up_desc():
    sql='select scs_codi,ics_codi,ics_desc,amb_desc from nodrizas.cat_centres;'
    return {up: {"br":ics_codi,"desc":ics_desc, "amb":amb_desc} for up,ics_codi,ics_desc,amb_desc in getAll(sql,nod)}

def get_indicador(taula,lloc,numerador,denominador,up_desc):
    """ Construye indicador iterando por assignada_tot y agrupando a nivel de up. Mira si el id cumple los criterios de edad, de pcc o de maca o esta
        en el set denominador, que incluye los problemas de salut y los tratamientos con psicotropos. Si cumple alguno de estos criterios, se checkea que este
        en el numerador.
        Devuelve una lista de tuples (fila)
    """
    lloc='' if lloc=='act' else lloc
    indicador=defaultdict(Counter)
    sql = "select up,id_cip_sec,  edat, pcc, maca from {}.assignada_tot where ates=1".format(nod)

    for up, id, edat,pcc,maca in getAll(sql,nod):
        if up in up_desc:
            if (edat <=2 or edat > 75) or (pcc==1 or maca ==1) or id in denominador:
                indicador[up]["DEN"]+=1
                if id in numerador:
                    indicador[up]["NUM"]+=1
    return [(up_desc[up]['br'], up_desc[up]['desc'],up_desc[up]['amb'], "{} {} {}".format(codi,taula,lloc),indicador[up]["NUM"], indicador[up]["DEN"]) for up in indicador]


def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))




if __name__ == '__main__':
    printTime('inici')
    table_name="pocs2018"
    uba_columns="(up varchar(5),indicador varchar(15),numerador int, denominador int)"
    createTable(table_name,uba_columns,alt,rm=True)


    tables_multiprocess={"visites":("visi_data_visita",get_visi_periodo),"activitats": ("au_dat_act",get_pocs_registre)}

    numeradors=defaultdict(lambda: defaultdict(set))
    up_desc=get_up_desc()

    #Sacar numerador: 
    #visitas registradas en el periodo especificado y pacientes con pocs registrado
    
    for table_parent,(visi_field,func) in tables_multiprocess.iteritems():
        tables=get_tables_visites(table_parent,visi_field)
        for dicti in multiprocess(func,tables):
            for lloc in dicti:
                numeradors[table_parent][lloc] |= dicti[lloc]
    
    printTime("visitas y pocs {}".format([len(numeradors[parent]) for parent in numeradors]))
    
    #Sacar denominador:
    #pacientes con problemas de salud registrados
    denominador=set()
    for ids in multiprocess(get_problemes,getSubTables("problemes")):
        denominador |= ids

    printTime("problemas {}".format(len(denominador)))
    
    #pacientes con tratamiento de psicotropos
    denominador |= get_tractaments()
    printTime("tractaments {}".format(len(denominador)))
   
    #se calcula el indicador con los dos numeradores que se quieren
    rows_final=[]
    for taula in numeradors:
        for lloc in numeradors[taula]:
            rows=get_indicador(taula,lloc,numeradors[taula][lloc],denominador,up_desc)
            rows_final.extend(rows)
            #listToTable(rows,table_name,alt)
            printTime("numerador {} done".format(taula))
    
    
    header=['BR CODI','DESC','AMBIT','INDICADOR',"NUMERADOR","DENOMINADOR"]
    export_txt_file("pocs_2018.txt",header,rows_final)