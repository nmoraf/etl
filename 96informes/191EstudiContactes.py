# coding: iso-8859-1


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

aga_codi="('AGA00071','AGA00046','AGA00047','AGA00070')"
nod = 'nodrizas'
imp = 'import'

class Contactes(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_centres()
        self.get_problemes()
        self.get_poblacio()
        self.export_data()
        
    def get_centres(self):
        """EAPs de Barcelona ciutat
        """

        sql="select scs_codi,sap_desc from cat_centres where aga in {} and ep='0208'".format(aga_codi)
        self.centres = { up:sap for up,sap in getAll(sql,nod)}
        
    def get_problemes(self):
        """."""
        self.casos=defaultdict(set)
        sql = "select id_cip_sec, pr_cod_ps, year(pr_dde), if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) from problemes, nodrizas.dextraccio where  pr_cod_ps in ('Z20.2', 'Z20.5','Z20.6') \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext)"
        for id, ps, anys, baixa in getAll(sql, "import"):
            if int(anys) in (2016, 2017, 2018):
                self.casos[id].add(anys)

    def get_poblacio(self):
        """."""
        self.pacients =  Counter()
        sql = "select id_cip_sec, up from assignada_tot "
        for id, up in getAll(sql, "nodrizas"):
            if up in self.centres:
                sap = self.centres[up]
                if id in self.casos:
                    for anys in self.casos[id]:
                        self.pacients[anys,sap] += 1
                    
    def export_data(self):
        """."""
        rec = []
        for (anys, sap), n in self.pacients.items():
            rec.append([anys, sap, n])
        writeCSV(tempFolder + 'estudi_contactes.txt', rec)
        
        
      
if __name__ == '__main__':
    Contactes()