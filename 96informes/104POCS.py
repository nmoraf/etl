
# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False


data_calcul = '2017-06-30'

imp = 'import'
nod = 'nodrizas'

territoris = {}
sql = "select up.up_codi_up_scs ,br.up_desc_up_ics, amb.amb_desc_amb, sap.dap_desc_dap   from    cat_gcctb007 br \
        inner join cat_gcctb008 up on br.up_codi_up_ics=up.up_codi_up_ics \
        inner join cat_gcctb006 sap on br.dap_codi_dap=sap.dap_codi_dap \
        inner join cat_gcctb005 amb on sap.amb_codi_amb=amb.amb_codi_amb \
        where e_p_cod_ep='0208'"
for up, desc, territori, sap in getAll(sql,imp):
    territoris[up] = {'ambit':territori, 'sap': sap, 'desc': desc }

#Indicador 2 Activitat POCS   

totaCatMes, totaCat = Counter(), Counter()
pocs = Counter()
sql = "select id_cip_sec,au_val, au_up, year(au_dat_act), month(au_dat_act) from activitats where au_cod_ac='POCS' and au_dat_act <'{0}' {1}".format(data_calcul, ' limit 10' if debug else '')
for id,valor,up, anyCalcul, mesCalcul in getAll(sql,imp):
    try:
        sap = territoris[up]['sap']
        ambit = territoris[up]['ambit']
        desc = territoris[up]['desc']
    except KeyError:
        continue
    if anyCalcul in [2015, 2016, 2017]:
        pocs[(up, desc, sap, ambit, anyCalcul)] += 1
        totaCat[anyCalcul] += 1
        if mesCalcul == 6:
            totaCatMes[(ambit,anyCalcul)] += 1

print totaCat

upload = [] 
for (a, b),d in totaCatMes.items():
    upload.append([b, a, d])
file = tempFolder + 'Pocs_juny' + '.txt'
writeCSV(file, upload, sep=';')


 
'''
upload = [] 
for ((up, desc,sap, ambit, anyCalcul)), d in pocs.items():
    upload.append([up, desc,sap, ambit, anyCalcul, d])
file = tempFolder + 'POCS_' + '.txt'
writeCSV(file, upload, sep=';')

#Indicador 1. Pob diana

diana = {}  
sql = 'select id_cip_sec, ps from eqa_problemes where ps in (45, 107, 764)'
for id, ps in getAll(sql, nod):
    if ps == 107:
        diana[(id, 'sol')] = True
    else:
        diana[(id, 'd')] = True

recompte = Counter() 
recompteTot = 0 
sql = 'select id_cip_sec, up,  edat, pcc, maca from assignada_tot where ates = 1'
for id, up, edat, pcc, maca in getAll(sql, nod):
    try:
        sap = territoris[up]['sap']
        ambit = territoris[up]['ambit']
        desc = territoris[up]['desc']
    except KeyError:
        continue
    esDiana = 0
    if pcc == 1:
        esDiana = 1
    if maca == 1:
        esDiana = 1
    if edat == 0:
        esDiana = 1
    if edat >75:
        if (id, 'sol') in diana:
            esDiana = 1
    if (id, 'd') in diana:
        esDiana = 1
    recompte[(up, desc, sap, ambit)] += esDiana
    recompteTot += esDiana

print recompteTot    

upload = [] 
for ((up, desc,sap, ambit)), d in recompte.items():
    upload.append([up, desc,sap, ambit, d])
file = tempFolder + 'POCS_PobDiana' + '.txt'
writeCSV(file, upload, sep=';')


#Indicador 3 Morbiditat Onada calor
 
onada = Counter()

sql = "select id_cip_sec, extract(year from pr_dde), pr_up from problemes  where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= '{0}' \
            and (pr_data_baixa is null or pr_data_baixa > '{0}') and pr_cod_ps in ('T67.0', 'T67.1')".format(data_calcul) 
for id, anyCalcul, up in getAll(sql,imp):
    try:
        sap = territoris[up]['sap']
        ambit = territoris[up]['ambit']
        desc = territoris[up]['desc']
    except KeyError:
        continue
    if anyCalcul in [2017]:
        onada[(up, desc, sap, ambit)] += 1
 
upload = [] 
for ((up, desc,sap, ambit)), d in onada.items():
    upload.append([up, desc,sap, ambit, d])
file = tempFolder + 'Onada_Calor' + '.txt'
writeCSV(file, upload, sep=';')
'''