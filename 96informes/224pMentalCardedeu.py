# coding: utf8

"""Petició de mirar malaltia mental per nens"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter

pat_mental= "('P71','P72','P73','P74','P76','P76','P79','P82','P85','P98')"

db = 'test'

class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_patmental()
        self.get_pob()
        self.export_data()        
        
    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres where scs_codi='00374'"
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
    
    def get_patmental(self):
        """Pacients amb patologia mental a final del període."""
        pmentalciap = {}
        self.cataleg = {}
        self.dades = defaultdict(lambda: defaultdict(list))
        self.cim_to_ciap = {}
        sql = "select codi_cim10, desc_ciap, codi_ciap from import.cat_md_ct_cim10_ciap where codi_ciap in {}".format(pat_mental)
        for cim10, desc, ciap in u.getAll(sql, "import"):
            pmentalciap[cim10] = True
            self.cataleg[ciap] = desc
            self.cim_to_ciap[cim10] = ciap
   
        in_crit = tuple(pmentalciap)
        
        sql = ("select id_cip_sec, pr_cod_ps from problemes,  nodrizas.dextraccio \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(in_crit),
               "import")
        for id, ps in u.getAll(*sql):
            ciap = self.cim_to_ciap[ps]
            self.dades[id][ciap].append(ciap)
    
    def get_pob(self):
        """Agafem assignada actual"""
        self.mentals = Counter()
        self.prev = Counter()
        sql = "select id_cip_sec, up, edat from assignada_tot where edat<15"
        for id, up, edat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                self.mentals[('DEN')] +=1
                self.prev[(edat, 'den')] += 1
                if id in self.dades:
                    self.prev[(edat, 'num')] += 1
                    for ciap in self.dades[id]:
                        desc = self.cataleg[ciap]
                        self.mentals[(desc)] += 1
                    
      
    def export_data(self):
        """."""
        upload = []
        for (tipus), n in self.mentals.items():
            print tipus, n
    
        for (edat, tip), n in self.prev.items():
            print edat, tip, n

if __name__ == '__main__':
    Frequentacio()