import sisapUtils as u
from pcrFuncionaris_aux import ids # {dni:(centre, nom)} in 96informes/
import pandas as pd
import hashlib as h

"""
# README:
    Proves de coronavirus de funcionaris de presons, per DNI.
    <Maig-2020>
    per: Elisabeth Turu Santigosa
    outfile: pcrFuncionaris.xlsx (per mail, zip amb clau)
"""

# Constants
outfile = u.tempFolder + "pcrFuncionaris.xlsx"
dnis_sql = "SELECT usua_dni, usua_cip FROM usutb040"
proves_sql = """
    SELECT
    --    i.cip,
        p.hash, {}
    FROM
        preduffa.sisap_covid_pac_prv_ext p
    --        INNER JOIN
    --    preduffa.sisap_covid_pac_id i
    --        ON i.hash = p.hash
    """
columns = ["DNI", "CENTRE", "NOM", "prova", "origen", "data", "resultat_cod", "resultat_des", "estat_cod", "estat_des"]
proves_sql = proves_sql.format(", ".join("p." + col for col in columns[3:]))
db = "redics"

# GET: Auxiliary Objects
dni_to_cip = {}
for sector in ["6951"] + u.sectors:
    print(sector)
    for dni, cip in u.getAll(dnis_sql, sector):
        if dni and dni[:8] in [i[:8] for i in ids]:
            dni_to_cip[dni] = cip
            dni_to_cip[dni[:8]] = cip
cip_to_dni = {cip: (dni,) for dni, cip in dni_to_cip.items() if len(dni) == 9}

hash_to_cip = {}
for cip in cip_to_dni:
    hash_to_cip[h.sha1(cip).hexdigest().upper()] = cip


# GET: proves
""" proves = [cip_to_dni[row[0]] + row[1:] for row in u.getAll(proves_sql, db) if row[0] in cip_to_dni] """
proves = [cip_to_dni[hash_to_cip[row[0]]]
          # + ids[cip_to_dni[hash_to_cip[row[0]]][0]]
          + row[1:]
          for row in u.getAll(proves_sql, db) if row[0] in hash_to_cip]

proves_df = pd.DataFrame(proves, columns= (columns[0],) + tuple(columns[3:]))
# proves_df["resultat_des"] = proves_df["resultat_des"].str.decode('iso_8859-1').str.encode('utf-8')

proves_df.set_index("DNI", inplace=True)

index_df = pd.DataFrame(ids, ["CENTRE","NOM"]).transpose()

merge_df = index_df.merge(proves_df, left_index=True, right_index=True, how='outer').reset_index()
merge_df.rename(columns={"index":"DNI"}, inplace= True)

# Outfile
merge_df.set_index(["CENTRE","DNI"]).sort_values(by="data", ascending=False).sort_index().to_excel(outfile)
