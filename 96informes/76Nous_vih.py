# coding: iso-8859-1
#Carol i Mireia VIH, Ermengol, Desembre 2016

from sisapUtils import *
from collections import defaultdict, Counter

db = 'test'
nod = 'nodrizas'

nous_VIH = {}
sql = "select id_cip_sec, dde from eqa_problemes_incid where ps in (667, 682) and year(dde)='2016'"
for id, dde in getAll(sql, nod):
    nous_VIH[id] = {'num12': 0, 'num': 0, 'dat': dde}

sql = 'select id_cip_sec, ps, dde, year(dde) from eqa_problemes_incid where ps in (666, 681)'
for id, ps, dde, anys in getAll(sql, nod):
    if id in nous_VIH:
        dat = nous_VIH[id]['dat']
        if dde < dat:
            nous_VIH[id]['num'] = 1
            b = monthsBetween(dde, dat)
            if 0 <= b <= 11:
                nous_VIH[id]['num12'] = 1

upload = []
for (id), d in nous_VIH.items():
    upload.append([id, d['num12'], d['num']])
  
table = 'nous_vih'      
createTable(table, '(id_cip_sec int, num12 double, num double)', db, rm=True)
listToTable(upload, table, db)
