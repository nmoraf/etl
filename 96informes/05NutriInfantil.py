#Carmen Cabezas (ion 3451), gener15, Ermengol
#Mirar tambe ion 2628
#treure de nod_derivacions de nodrizas TS codi(05999) i nodrizas.eqa_variables agrupador=115 (activitat 944, val social)
#sisap_nutricio de redICs es una vista amb les dades que necessiten de nutricio infantil
from sisapUtils import *
import csv,os
from time import strftime

db="test"
nod="nodrizas"
imp="import"

conn= connect('redics')
c= conn.cursor()

tu11="u11"
tder="nod_derivacions"
serveid="('05999')"
OutFile= tempFolder + 'derTS.txt'
tDes= "derTS"
tval="nodrizas.ped_variables"
valsoc="('115')"
OutFileF=tempFolder + 'desnut.txt'
OutFileT=tempFolder + 'desnutTOT.txt'
tDesnut="desnut"
tDesnutTOT="desnutTOT"

printTime('Sisap_nutricio')
sql= "select * from sisap_nutricio"
a=0
desnut = {}
with open(OutFileT,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for id_pacient,b,c,d,uppac,updx,g,h,dde,j,k in c.execute(sql):
        desnut[id_pacient]= {'b':b,'c':c,'d':d,'up':uppac,'updx':updx,'g':g,'h':h,'dde':dde,'j':j,'k':k,'vsoc':0,'val_social':0}
        w.writerow([id_pacient,b,c,d,uppac,updx,g,h,dde,j,k,0,0])
        a+=1
print a

sql="drop table if exists test.desnutTOT"
execute(sql,db)
sql="create table test.desnutTOT (hash varchar(40) not null default'', sexe varchar(1) not null default'',situacio varchar(1) not null default'',nacionalitat varchar(5) not null default'',eap_pacient varchar(5) not null default''\
        ,eap_dx varchar(5) not null default'',gerencia_dx varchar(100) not null default'',cim10 varchar(10) not null default'',data_dx date,data_passiu date not null default 0,edat_diagnostic double,vsoc double, val_social date not null default 0)"
execute(sql,db)
loadData(OutFileT,tDesnutTOT,db)

printTime('U11')
u11={}
b=0
sql= "select id_cip_sec,hash_d from {0}".format(tu11)
for idsec, hash in getAll(sql,imp):
    if hash in desnut:
        u11[int(idsec)]= {'hash': hash}
        b+=1
    else:
        ok=1
print b

printTime('Derivacions')
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    sql="select id_cip_sec, oc_data, oc_servei_ori,motiu from {0} where inf_servei_d_codi in {1}".format(tder,serveid)
    for id,data,ori,motiu in getAll(sql,nod):
        id=int(id)
        if id in u11:
            w.writerow([id,u11[id]['hash'],data,ori,motiu])
   
sql="drop table if exists test.derTS"
execute(sql,db)
sql="create table test.derTS (id_cip_sec double,hash varchar(40) not null default'', data date,ori varchar(10) not null default'',motiu varchar(5) not null default'')"
execute(sql,db)
loadData(OutFile,tDes,db)
 
printTime('Val social')
sql="select id_cip_sec, max(dat) from {0} where agrupador in {1} group by id_cip_sec".format(tval,valsoc)
for id,data in getAll(sql,nod):
    id=int(id)
    if id in u11:
        hash=u11[id]['hash']
        if hash in desnut:
            desnut[hash]['val_social']=data
            desnut[hash]['vsoc']+=1

printTime('Final')
with open(OutFileF,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for hash,d in desnut.iteritems():
        w.writerow([hash,d['b'],d['c'],d['d'],d['up'],d['updx'],d['g'],d['h'],d['dde'],d['j'],d['k'],d['vsoc'],d['val_social']])

sql="drop table if exists test.desnut"
execute(sql,db)
sql="create table test.desnut (hash varchar(40) not null default'', sexe varchar(1) not null default'',situacio varchar(1) not null default'',nacionalitat varchar(5) not null default'',eap_pacient varchar(5) not null default''\
        ,eap_dx varchar(5) not null default'',gerencia_dx varchar(100) not null default'',cim10 varchar(10) not null default'',data_dx date,data_passiu date not null default 0,edat_diagnostic double,vsoc double, val_social date not null default 0)"
execute(sql,db)
loadData(OutFileF,tDesnut,db)
u11.clear()    
desnut.clear()

sql="update {0} a inner join {1} b on a.hash=b.hash set a.vsoc=b.vsoc,a.val_social=b.val_social".format(tDesnutTOT,tDesnut)
execute(sql,db)
sql="update {0} set val_social=0 where val_social<data_dx".format(tDesnutTOT)
execute(sql,db)

