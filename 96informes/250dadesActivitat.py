# coding: utf-8

import sisapUtils as u
import datetime as d
from os import remove

TODAY = d.datetime.now().date() - d.timedelta(days=1)
TODAY7D = d.datetime.now().date() - d.timedelta(days=7)
DATES = (TODAY7D.strftime("%Y-%m-%d"), TODAY.strftime("%Y-%m-%d"))


class dadesActivitat(object):
    def __init__(self):
        """ . """

        self.get_upload()

    def get_upload(self):
        """ . """
        print("Get Select")
        self.upload = []
        sql = """select data, residencia, des, sisap_class_des,
                        up_des, sap_des, amb_des, servei,
                        visita_tip, visita_des, sum(recompte) AS nvis
                 from
                    preduffa.SISAP_COVID_RES_VISITES
                    , preduffa.SISAP_COVID_CAT_RESIDENCIA cat
                    , preduffa.SISAP_COVID_CAT_RESIDENCIA_TIP tip
                    , preduffa.sisap_covid_cat_up up
                 where
                    residencia=cod
                    and sisap_class=tipus
                    and up_cod=eap
                    and data between to_date('{}','yyyy-mm-dd')
                                 and to_date('{}','yyyy-mm-dd')
                 group by data, residencia, des, sisap_class_des,
                          up_des, sap_des, amb_des,
                          servei, visita_tip, visita_des""".format(*DATES)
        for row in u.getAll(sql, 'redics'):
            self.upload.append((row))
        print("Create file")
        file = u.tempFolder + 'dadesActivitat_{}.csv'
        file = file.format(TODAY.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('data',
                     'residencia', 'des',
                     'sisap_class_des', 'up_des', 'sap_des', 'amb_des',
                     'servei', 'visita_tip', 'visita_des',
                     'nvis')] + self.upload, sep=";")
        print("Send Mail")
        subject = 'Dades Activitat'
        text = 'Arxiu amb les dades activitat'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'eulalia.dalmau@gencat.cat',
                      ['mbustos.bnm.ics@gencat.cat','ehermosilla@idiapjgol.info'],
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == "__main__":

    dadesActivitat()
