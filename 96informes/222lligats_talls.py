# coding: latin1

"""
Prescripcions amb diagnòstic lligat.
"""

import sisapUtils as u

sectors=['6837', '6734']

class Main(object):
    """."""

    def __init__(self):
        """."""
        num, den = 0, 0
        resultat = u.multiprocess(Sector, u.sectors)
        for sector in resultat:
            num += sector.num
            den += sector.den
        print num, den, num / float(den)

class Sector(object):
    """."""

    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_lligats()
        self.get_prescripcions()
        self.lligats = None
        print self.sector

    def get_lligats(self):
        """."""
        sql = "select dgppf_pmc_codi, dgppf_num_prod from ppftb023"
        self.lligats = set([row for row in u.getAll(sql, self.sector)])
        #print(self.lligats)

    def get_prescripcions(self):
        """."""
        #fet a l'octubre de 2019, volem agafar fins a l'abril de 2018 inicialment (-17 mesos)
        sql = "select ppfmc_pmc_codi, ppfmc_num_prod from ppftb016 \
               where ppfmc_pmc_data_ini < date '2019-09-18' and \
               ppfmc_data_fi > date '2019-09-18'"
        self.num, self.den = 0, 0
        for row in u.getAll(sql, self.sector):
            self.den += 1
            if row in self.lligats:
                self.num += 1


if __name__ == "__main__":
    Main()
