# coding: latin1

"""
Diagn�stics incidents per Xarxa vigil�ncia hospitalaria
Afegim fins a data extraccio i sense filtrar per edat
"""

import sys,datetime, re

import sisapUtils as u

VIH_Sida=set(['101'])
DM=set(['18','24'])

#Llista de codis de vacunes
GRIP=set(['GRIP-N', 'GRIP', 'GRIP-A', 'G(A)-1', 'G(A)-4', 'G(A)-2', 'G(A)-3', 'P-G-AR', 'P-G-NE', 'P-G-AD', 'P-G-60'])
VNC13=set(['PNC10', 'PN10-1', 'PN10-3', 'PN10-2', 'PN13-3', 'PN13-1', 'PN13-2', 'PNC13', 'PNC7-4', 'PNC7-2', 'PNC7-3', 'PNCP-2', 'PNC7', 'P00100', 'P00107', 'P00108', 'P00227', 'P00099', 'P00104', 'P00109', 'P00098', 'P00103', 'P00223', 'P00228', 'P00105', 'P00106', 'P00221', 'P00222', 'P00224', 'P00225', 'P00097', 'P00101', 'P00102', 'P00110'])
VNP23=set(['PNCP', 'PNCP-3', 'PNEU-3', 'PNEU-2', 'PNEU-1', 'PNEUMO', 'PNCP', 'PNCP-3', 'P00111', 'P00112', 'P00113', 'P00114', 'P00115'])

#Llista de tots els codis de diabetes
codis_diab=set(['C01-E10', 'C01-E10.1', 'C01-E10.10', 'C01-E10.11', 'C01-E10.2', 'C01-E10.21', 'C01-E10.22', 'C01-E10.29', 'C01-E10.3', 'C01-E10.31', 'C01-E10.311', 'C01-E10.319', 'C01-E10.32', 'C01-E10.321', 'C01-E10.329', 'C01-E10.33', 'C01-E10.331', 'C01-E10.339', 'C01-E10.34', 'C01-E10.341', 'C01-E10.349', 'C01-E10.35', 'C01-E10.351', 'C01-E10.359', 'C01-E10.36', 'C01-E10.39', 'C01-E10.4', 'C01-E10.40', 'C01-E10.41', 'C01-E10.42', 'C01-E10.43', 'C01-E10.44', 'C01-E10.49', 'C01-E10.5', 'C01-E10.51', 'C01-E10.52', 'C01-E10.59', 'C01-E10.6', 'C01-E10.61', 'C01-E10.610', 'C01-E10.618', 'C01-E10.62', 'C01-E10.620', 'C01-E10.621', 'C01-E10.622', 'C01-E10.628', 'C01-E10.63', 'C01-E10.630', 'C01-E10.638', 'C01-E10.64', 'C01-E10.641', 'C01-E10.649', 'C01-E10.65', 'C01-E10.69', 'C01-E10.8', 'C01-E10.9', 'C01-E11', 'C01-E11.0', 'C01-E11.00', 'C01-E11.01', 'C01-E11.2', 'C01-E11.21', 'C01-E11.22', 'C01-E11.29', 'C01-E11.3', 'C01-E11.31', 'C01-E11.311', 'C01-E11.319', 'C01-E11.32', 'C01-E11.321', 'C01-E11.329', 'C01-E11.33', 'C01-E11.331', 'C01-E11.339', 'C01-E11.34', 'C01-E11.341', 'C01-E11.349', 'C01-E11.35', 'C01-E11.351', 'C01-E11.359', 'C01-E11.36', 'C01-E11.39', 'C01-E11.4', 'C01-E11.40', 'C01-E11.41', 'C01-E11.42', 'C01-E11.43', 'C01-E11.44', 'C01-E11.49', 'C01-E11.5', 'C01-E11.51', 'C01-E11.52', 'C01-E11.59', 'C01-E11.6', 'C01-E11.61', 'C01-E11.610', 'C01-E11.618', 'C01-E11.62', 'C01-E11.620', 'C01-E11.621', 'C01-E11.622', 'C01-E11.628', 'C01-E11.63', 'C01-E11.630', 'C01-E11.638', 'C01-E11.64', 'C01-E11.641', 'C01-E11.649', 'C01-E11.65', 'C01-E11.69', 'C01-E11.8', 'C01-E11.9', 'C01-E13', 'C01-E13.0', 'C01-E13.00', 'C01-E13.01', 'C01-E13.1', 'C01-E13.10', 'C01-E13.11', 'C01-E13.2', 'C01-E13.21', 'C01-E13.22', 'C01-E13.29', 'C01-E13.3', 'C01-E13.31', 'C01-E13.311', 'C01-E13.319', 'C01-E13.32', 'C01-E13.321', 'C01-E13.329', 'C01-E13.33', 'C01-E13.331', 'C01-E13.339', 'C01-E13.34', 'C01-E13.341', 'C01-E13.349', 'C01-E13.35', 'C01-E13.351', 'C01-E13.359', 'C01-E13.36', 'C01-E13.39', 'C01-E13.4', 'C01-E13.40', 'C01-E13.41', 'C01-E13.42', 'C01-E13.43', 'C01-E13.44', 'C01-E13.49', 'C01-E13.5', 'C01-E13.51', 'C01-E13.52', 'C01-E13.59', 'C01-E13.6', 'C01-E13.61', 'C01-E13.610', 'C01-E13.618', 'C01-E13.62', 'C01-E13.620', 'C01-E13.621', 'C01-E13.622', 'C01-E13.628', 'C01-E13.63', 'C01-E13.630', 'C01-E13.638', 'C01-E13.64', 'C01-E13.641', 'C01-E13.649', 'C01-E13.65', 'C01-E13.69', 'C01-E13.8', 'C01-E13.9'])


def get_data_extraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format('nodrizas')
    return u.getOne(sql,'nodrizas')[0]

class cob_vacunal(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execuci� seq�encial."""
        self.get_poblacio_adulta()
        self.get_poblacio_pedia()
        self.get_VIH_Sida()
        self.get_DM()
        self.get_vacunats()
        self.get_indicadors()
        #self.get_problemes()
        #self.set_master()
        #self.write_CSV()
    
    def get_poblacio_adulta(self):
        """
        Discriminem si el centre �s ICS o concertat 
        Si �s major de 14 ho guardem com a id_adult
        Si �s menor guardem el centre per a creuar-ho amb dades de pediatria
        """
        self.id_adults={}
        self.centre_ped={}
        sql = "select id_cip_sec, edat, ep from assignada_tot where ates=1"
        for id, edat, ep in u.getAll(sql, 'nodrizas'):
            #definir si el centre �s concertat o no
            if ep=="0208":
                centre="Centre ICS"
            else:
                centre="Centre concertat"
            #Guardem id, edat i tipus centre per adult i tipus centre per menors
            #de 15 anys
            if edat >14:
                self.id_adults[id]=(edat, centre)
            else:
                self.centre_ped[id]=centre
        print "id's saved"
    
        print(len(self.id_adults))
        u.writeCSV(u.tempFolder + 'id.csv', self.id_adults.items())

    def get_poblacio_pedia(self):
            """
            Creuem dades de poblaci� menor de 15 amb els seus centres
            """
            self.id_pedia={}
            sql = "select id_cip_sec, edat_a, edat_m from ped_assignada where ates=1"
            for id, edat_a, edat_m in u.getAll(sql, 'nodrizas'):
                self.id_pedia[id]=(edat_a, edat_m, self.centre_ped[id])
            print "id's saved"
        
            print(len(self.id_pedia))
            u.writeCSV(u.tempFolder + 'id_pedia.csv', self.id_pedia.items())         
            
    def get_VIH_Sida(self):
        """
        Obtenim els que tenen VIH i Sida de la poblaci� atesa assignada
        """
        self.VIH_adults={}
        self.VIH_pedia={}
        data_max=datetime.datetime.strptime('2018-12-31','%Y-%m-%d').date()
        sql = "select id_cip_sec, dde from eqa_problemes where ps=101"
        for id, data in u.getAll(sql, 'nodrizas'):
            if id in self.id_adults and data <= data_max:
                self.VIH_adults[id]=self.id_adults[id]

        sql = "select id_cip_sec, dde from ped_problemes where ps=101"
        for id, data in u.getAll(sql, 'nodrizas'):
            if id in self.id_pedia and data <= data_max:
                self.VIH_pedia[id]=self.id_pedia[id]
        
        print "id's VIH saved"
        print(len(self.VIH_adults))
        u.writeCSV(u.tempFolder + 'id_VIH.csv', self.VIH_adults.items()) 
        print(len(self.VIH_pedia))
        u.writeCSV(u.tempFolder + 'id_VIH_pedia.csv', self.VIH_pedia.items()) 
        
    def get_DM(self):
        """
        Obtenim els que tenen DM de la poblaci� atesa assignada
        """
        self.DM_adults={}
        self.DM_pedia={}
        data_max=datetime.datetime.strptime('2018-12-31','%Y-%m-%d').date()
        #sql = "select id_cip_sec, dde from eqa_problemes where ps in (18, 700)"
        sql="select id_cip_sec, pr_dde from problemes where pr_cod_ps in {}".format(tuple(codis_diab))
        #for id, data in u.getAll(sql, 'nodrizas'):
        for id, data in u.getAll(sql, 'import'):
            if id in self.id_adults and data <= data_max:
                self.DM_adults[id]=self.id_adults[id]
            if id in self.id_pedia and data <= data_max:
                self.DM_pedia[id]=self.id_pedia[id]   
        print "id's DM saved"
        print(len(self.DM_adults))
        print(len(self.DM_pedia))
        u.writeCSV(u.tempFolder + 'id_DM.csv', self.DM_adults.items()) 
        
    def get_vacunats(self):
        """
        Obtenim els que tenen vacuna de grip, VNP23, VNC23 i les dues alhora
        de la poblaci� atesa assignada
        """
        self.vacunats={}
        self.pedia_vacunats={}
        data_max_grip=datetime.datetime.strptime('2019-01-31','%Y-%m-%d').date()
        data_max=datetime.datetime.strptime('2018-12-31','%Y-%m-%d').date()
        data_min=datetime.datetime.strptime('2018-01-01','%Y-%m-%d').date()
        
        
        #Busquem a import.vacunes els que tenen VNP23 i VNC23 
        sql = "select id_cip_sec, va_u_cod, va_u_data_modi from vacunes where \
        va_u_cod in {} or  va_u_cod in {}".format(tuple(VNC13), tuple(VNP23))  
        for id, cod, data in u.getAll(sql, 'import'):
            if data <= data_max:
                if cod in VNC13:
                    agr=698
                else:
                    agr=48
                if id in self.id_pedia:
                    self.pedia_vacunats[id, agr]=data
                else:
                    self.vacunats[id, agr]=data
        
        #Busquem les vacunes de la grip a import.vacunes
        sql = "select id_cip_sec, va_u_data_modi from vacunes where \
        va_u_cod in {}".format(tuple(GRIP)) 
        for id, data in u.getAll(sql, 'import'):
            if data >=data_min and data <= data_max_grip:
                if id in self.id_adults:
                    self.vacunats[id, 99]=data
                if id in self.id_pedia:
                    self.pedia_vacunats[id, 99]=data
        
        
        print "id's vacunats guardats"
        print(len(self.vacunats))
        u.writeCSV(u.tempFolder + 'id_vacunats.csv', self.vacunats.items()) 
        print(len(self.pedia_vacunats))
        u.writeCSV(u.tempFolder + 'id_pedia_vacunats.csv', self.pedia_vacunats.items()) 
    
    def get_indicadors(self):
        """
        Obtenir els indicadors
        """
        self.master={}
        #Nens
        for id in self.id_pedia:
            #Nens amb VIH 
            if id in self.VIH_pedia:
                num=0
                den=1
                #Nens amb VIH amb VNC13
                if (id,698) in self.pedia_vacunats: 
                    num=1                        
                self.master[id, "COBVAC09"]=("COBVAC09", num, den, self.id_pedia[id][2])
                #Nens amb VIH amb 6 mesos o m�s
                if self.id_pedia[id][1] >= 6:
                    num=0
                    den=1
                    #Nens amb VIH majors de 6 mesos i amb vacuna grip
                    if (id,99) in self.pedia_vacunats: 
                        num=1                        
                    self.master[id, "COBVAC01"]=("COBVAC01", num, den, self.id_pedia[id][2])

                    #Nens amb VIH amb 2 anys o m�s
                    if self.id_pedia[id][0] >= 2:
                        num=0
                        den=1
                        #Nens amb VIH majors de 2 anys i amb VNP23
                        if (id,48) in self.pedia_vacunats: 
                            num=1              
                        self.master[id, "COBVAC04"]=("COBVAC04", num, den, self.id_pedia[id][2])
                     
            #Nens amb DM        
            if id in self.DM_pedia:
                #Nens amb DM amb 6 mesos o m�s
                if self.id_pedia[id][1] >= 6:
                    num=0
                    den=1
                    #Nens amb DM majors de 6 mesos i amb vacuna grip
                    if (id,99) in self.pedia_vacunats: 
                        num=1                        
                    self.master[id, "COBVAC12"]=("COBVAC12", num, den, self.id_pedia[id][2])

                    #Nens amb DM amb 2 anys o m�s
                    if self.id_pedia[id][0] >= 2:
                        num=0
                        den=1
                        #Nens amb DM majors de 2 anys i amb VNP23
                        if (id,48) in self.pedia_vacunats: 
                            num=1              
                        self.master[id, "COBVAC15"]=("COBVAC15", num, den, self.id_pedia[id][2])
         
        #Adults
        for id in self.id_adults:
            #Adults amb VIH 
            if id in self.VIH_adults:
                #adults amb VIH amb menys de 65 anys
                if self.id_adults[id][0] < 65:
                    num=0
                    den=1
                    #adults amb VIH amb menys de 65 anys i amb vacuna grip
                    if (id,99) in self.vacunats: 
                        num=1                        
                    self.master[id, "COBVAC02"]=("COBVAC02", num, den, self.id_adults[id][1])

                    #adults amb VIH amb menys de 65 anys i amb VNP23
                    num=0
                    den=1
                    if (id,48) in self.vacunats: 
                        num=1              
                    self.master[id, "COBVAC05"]=("COBVAC05", num, den, self.id_adults[id][1])
                    
                    #adults amb VIH amb menys de 65 anys i amb VNC13
                    num=0
                    num2=0
                    den=1
                    if (id,698) in self.vacunats: 
                        num=1
                        #adults amb VIH amb menys de 65 anys, amb VNP23 i amb VNC13
                        if (id,48) in self.vacunats: 
                            num2=1
                    self.master[id, "COBVAC07"]=("COBVAC07", num, den, self.id_adults[id][1])
                    self.master[id, "COBVAC10"]=("COBVAC10", num2, den, self.id_adults[id][1])
                    
                #adults amb VIH amb 65 anys o m�s
                else:
                    num=0
                    den=1
                    #adults amb VIH amb menys de 65 anys i amb vacuna grip
                    if (id,99) in self.vacunats: 
                        num=1                        
                    self.master[id, "COBVAC03"]=("COBVAC03", num, den, self.id_adults[id][1])

                    #adults amb VIH amb menys de 65 anys i amb VNP23
                    num=0
                    den=1
                    if (id,48) in self.vacunats: 
                        num=1              
                    self.master[id, "COBVAC06"]=("COBVAC06", num, den, self.id_adults[id][1])
                    
                    #adults amb VIH amb menys de 65 anys i amb VNC13
                    num=0
                    num2=0
                    den=1
                    if (id,698) in self.vacunats: 
                        num=1
                        #adults amb VIH amb menys de 65 anys, amb VNP23 i amb VNC13
                        if (id,48) in self.vacunats: 
                            num2=1
                    self.master[id, "COBVAC08"]=("COBVAC08", num, den, self.id_adults[id][1])
                    self.master[id, "COBVAC11"]=("COBVAC11", num2, den, self.id_adults[id][1])
                     
            #adults amb DM        
            if id in self.DM_adults:
                #adults amb DM amb menys de 65 anys
                if self.id_adults[id][0] < 65:
                    num=0
                    den=1
                    #adults amb DM amb menys de 65 anys i amb vacuna grip
                    if (id,99) in self.vacunats: 
                        num=1                        
                    self.master[id, "COBVAC13"]=("COBVAC13", num, den, self.id_adults[id][1])

                    #adults amb DM amb menys de 65 anys i amb VNP23
                    num=0
                    den=1
                    if (id,48) in self.vacunats: 
                        num=1              
                    self.master[id, "COBVAC16"]=("COBVAC16", num, den, self.id_adults[id][1])
                #adults amb DM amb 65 anys o m�s
                else:
                    num=0
                    den=1
                    #adults amb DM amb m�s de 65 anys i amb vacuna grip
                    if (id,99) in self.vacunats: 
                        num=1                        
                    self.master[id, "COBVAC14"]=("COBVAC14", num, den, self.id_adults[id][1])

                    #adults amb DM amb m�s de 65 anys i amb VNP23
                    num=0
                    den=1
                    if (id,48) in self.vacunats: 
                        num=1              
                    self.master[id, "COBVAC17"]=("COBVAC17", num, den, self.id_adults[id][1])
                
        
        u.writeCSV(u.tempFolder + 'master_vacunacions.csv', self.master.values()) 
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    cob_vacunal()
    
    u.printTime("Fi")