# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import csv,os,sys
import pandas as pd
from datetime import datetime

import sisapUtils as u

tb = "SISAP_ECONSULTA_PROF_vars"
db = "redics"

EXCEL_SHEETS = 1

path = u.tempFolder + "ECONSULTA/" 

class EConsultaP(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_info()
        self.get_ubas()
        self.get_eqa()
        self.get_poblacio()
        self.get_converses()
        self.get_missatges()
        self.get_dies()
        self.get_acc()
        self.get_professionals()
        self.upload_data()

       
    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi, ics_desc, ep from cat_centres"
        for up, br, desc, ep in u.getAll(sql, 'nodrizas'):
            self.centres[up] = {'br': br, 'desc': desc, 'ep': ep}
    
    def get_info(self):
        """Mirem a excel de Manolo les dades dels professionals"""
        self.prof_info = {}
        sheets = []
        for file in os.listdir(path):
            print file
            xls = pd.ExcelFile(path + file)
            for sheet in range(EXCEL_SHEETS):
                sheets.append(xls.parse(sheet))
        fitxes = pd.concat(sheets)
        for row in fitxes.itertuples():
            doc16,doc17,doc18, edat, per_sexe = row[11], row[12], row[13],  row[7], row[8]
            d16 = 0 if doc16 == 'N' else 1
            d17 = 0 if doc17 == 'N' else 1
            d18 = 0 if doc18 == 'N' else 1
            self.prof_info[row[1]] = {'edat': edat, 'sex': per_sexe, 'd16': d16, 'd17': d17, 'd18': d18}

    def get_ubas(self):
        """."""
        self.ubas = c.defaultdict(set)
        sql = "select up, tipus, uba from mst_ubas"
        for up, tipus, uba in u.getAll(sql, 'eqa_ind'):
            if up in self.centres:
                self.ubas[(up, tipus)].add(uba)
    
    def get_eqa(self):
        """."""
        self.eqa17 = {}
        sql = "select up,uab,tipus,  punts from eqasintetic where dataany='2017' and datames='12'"
        for up, uba, tipus, punts in u.getAll(sql, 'pdp'):
            self.eqa17[(up, uba, tipus)] = punts
    
    def get_poblacio(self):
        """."""
        self.pob = c.Counter()
        self.pobatesa = c.Counter()
        sql = "select id_cip_sec, up, uba, ubainf, edat, ates from assignada_tot"
        for id, up, uba, ubainf, edat, ates in u.getAll(sql, 'nodrizas'):
            if uba in self.ubas[(up, 'M')] or ubainf in self.ubas[(up, 'I')]:
                edat = 'ped' if edat <15 else 'ad'
                self.pob[(up, uba, 'M', edat)] += 1
                self.pob[(up, ubainf, 'I', edat)] += 1
                if ates == 1:
                    self.pobatesa[(up, uba, 'M', edat)] += 1
                    self.pobatesa[(up, ubainf, 'I', edat)] += 1
      
    def get_converses(self):
        """."""
        self.converses = {}
        self.professionals_time = {}
        self.pacients_time = {}
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv, nodrizas.dextraccio"
        for pac, sector, id, autor, desti, estat, dat in u.getAll(sql, 'import'):
            prof = (autor if autor else desti)
            self.converses[(sector, id)] = {'pac': pac,
                                            'prof': prof,
                                            'inicia': not desti,
                                            'estat': estat,
                                            'sector': sector,
                                            'msg': {}}
            if autor:
                if prof in self.professionals_time:
                    first, last = self.professionals_time[prof]['first'],self.professionals_time[prof]['last']
                    if dat < first:
                        self.professionals_time[prof]['first'] = dat
                    if dat > last:
                        self.professionals_time[prof]['last'] = dat
                else:
                    self.professionals_time[prof] = {'first': dat, 'last': dat}
            if desti:
                if prof in self.pacients_time:
                    first, last = self.pacients_time[prof]['first'],self.pacients_time[prof]['last']
                    if dat < first:
                        self.pacients_time[prof]['first'] = dat
                    if dat > last:
                        self.pacients_time[prof]['last'] = dat
                else:
                    self.pacients_time[prof] = {'first': dat, 'last': dat}
 
    def get_missatges(self):
        """."""
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data, date_format(msg_data,'%Y%m') \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data, period in u.getAll(sql, 'import'):
            if (sector, id_c) in self.converses:
                this = {'prof': autor if autor else desti,
                        'inicia': not desti,
                        'data': data,
                        'periodemsg': period}
                self.converses[(sector, id_c)]['msg'][id] = this   
    
    def get_dies(self):
        """."""
        self.dies = c.Counter()
        for conversa in self.converses.values():
            if not conversa['inicia']:
                self.dies[(conversa['sector'], conversa['prof'], 'DEN')] += 1
                pregunta = conversa['msg'][1]['data']
                resposta = None
                for id in range(2, 1000):
                    if id in conversa['msg']:
                        if conversa['msg'][id]['inicia']:
                            resposta = conversa['msg'][id]['data']
                            break
                    else:
                        break
                if resposta:
                    dies = EConsultaP.conta_dies(pregunta, resposta)
                    if dies <5:
                        self.dies[(conversa['sector'], conversa['prof'], 'NUM')] += 1 
    
    def get_acc(self):
        """."""
        self.accessibilitat = c.Counter()
        sql = "select * from sisap_accessibilitat where  peticions is not null and to_char(dia, 'YYYYMMDD') between '20170501' and '20180531'"
        for row in u.getAll(sql, 'redics'):  
            up, uba, servei, peticions =  row[1], row[2], row[3], row[5]
            forats48 = row[6] + row[7] + row[8]
            forats5d = row[6] + row[7] + row[8] + row[9] + row[10] + row[11]
            forats10d = row[6] + row[7] + row[8] + row[9] + row[10] + row[11] + row[12] + row[13] + row[14] + row[15] + row[16]
            dia = row[4]
            tipus = 'M' if servei in ('MG', 'PED') else 'I'
            if forats48 > peticions:
                forats48 = peticions
            if forats5d > peticions:
                forats5d = peticions
            if forats10d > peticions:
                forats10d = peticions
            self.accessibilitat[(up, uba, tipus, 'peti')] += peticions
            self.accessibilitat[(up, uba, tipus, 'forats48')] += forats48
            self.accessibilitat[(up, uba, tipus, 'forats5d')] += forats5d
            self.accessibilitat[(up, uba, tipus, 'forats10d')] += forats10d   
    
       
    def get_professionals(self):
        """."""
        self.upload = []
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, \
                      lloc_proveidor_dni_proveidor \
               from cat_pritb025"
        llocs = {(up, lloc): dni for (up, lloc, dni)
                 in u.getAll(sql, 'import') if up in self.centres}
        params = (('codi_sector','uab_codi_up', 'M', 'uab_codi_uab',
                   'uab_lloc_de_tr_codi_lloc_de_tr', 'cat_vistb039'),
                  ('codi_sector','uni_codi_up', 'I', 'uni_codi_unitat',
                   'uni_ambit_treball', 'cat_vistb059'))
        sql = "select {}, {}, '{}', {}, {} from {}"
        for param in params:
            for sector, up, tipus, uba, lloc in u.getAll(sql.format(*param), 'import'):
                if up in self.centres:
                    dni = llocs.get((up, lloc))
                    if dni and (uba in self.ubas[(up, tipus)]):
                        dni2 = u.getNif(dni)
                        nens = self.pob[(up, uba, tipus, 'ped')]
                        adults = self.pob[(up, uba, tipus, 'ad')]
                        nensatesos = self.pobatesa[(up, uba, tipus, 'ped')]
                        adultsatesos = self.pobatesa[(up, uba, tipus, 'ad')]
                        edat = self.prof_info[dni2]['edat'] if dni2 in self.prof_info else None
                        sexe = self.prof_info[dni2]['sex'] if dni2 in self.prof_info else None
                        d16 = self.prof_info[dni2]['d16'] if dni2 in self.prof_info else 0
                        d17 = self.prof_info[dni2]['d17'] if dni2 in self.prof_info else 0
                        d18 = self.prof_info[dni2]['d18'] if dni2 in self.prof_info else 0
                        eqa2017 = self.eqa17[(up, uba, tipus)] if (up, uba, tipus) in self.eqa17 else 0
                        firstp = self.professionals_time[dni]['first'] if dni in  self.professionals_time else None
                        lastp = self.professionals_time[dni]['last'] if dni in  self.professionals_time else None
                        firstpac = self.pacients_time[dni]['first'] if dni in  self.pacients_time else None
                        lastpac = self.pacients_time[dni]['last'] if dni in  self.pacients_time else None
                        dies_num = self.dies[(sector, dni, 'NUM')] if (sector, dni, 'NUM') in self.dies else 0
                        dies_den = self.dies[(sector, dni, 'DEN')] if (sector, dni, 'DEN') in self.dies else 0
                        peti = self.accessibilitat[(up, uba, tipus, 'peti')] if (up, uba, tipus, 'peti') in self.accessibilitat else 0
                        forats48 = self.accessibilitat[(up, uba, tipus, 'forats48')] if (up, uba, tipus, 'forats48') in self.accessibilitat else 0
                        forats5d = self.accessibilitat[(up, uba, tipus, 'forats5d')] if (up, uba, tipus, 'forats5d') in self.accessibilitat else 0
                        forats10d = self.accessibilitat[(up, uba, tipus, 'forats10d')] if (up, uba, tipus, 'forats10d') in self.accessibilitat else 0
                        self.upload.append([sector, dni, up, self.centres[up]['br'], self.centres[up]['desc'], self.centres[up]['ep'], uba, tipus, nens, adults, nensatesos, adultsatesos,
                            str(firstp), str(lastp), str(firstpac), str(lastpac), dies_num, dies_den, peti, forats48, forats5d, forats10d, str(edat), str(sexe), str(d16), str(d17), str(d18), eqa2017])
                        
    def upload_data(self):
        """."""
        columns = ["codi_sector varchar2(4)","dni varchar2(14)", "up varchar2(5)",   "br varchar2(5)",  "desc_up varchar2(300)", "ep varchar2(10)", 
            "uba varchar2(5)", "tipus varchar2(1)", "nens number", "adults number", "nens_atesos number", "adults_atesos number", "primera_conv_prof varchar2(40)", "ultima_conv_prof varchar2(40)",
            "primera_conv_pac varchar2(40)", "ultima_conv_pac varchar2(40)", "menys5dies_num number", "menys5dies_den number", "peticions number",
            "forats48 number", "forats5d number", "forats10d number", "edat varchar2(10)", "sexe varchar2(5)", "docencia2016 varchar2(100)", "docencia2017 varchar2(100)", "docencia2018 varchar2(100)", "eqa17 varchar2(50)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUEHE"), db)
        
    @staticmethod
    def conta_dies(inici, final):
        """."""
        total = (final - inici).days
        laborables = 0
        for i in range(total):
            dia = inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                laborables += 1
        return laborables
        
if __name__ == '__main__':
    EConsultaP()
    