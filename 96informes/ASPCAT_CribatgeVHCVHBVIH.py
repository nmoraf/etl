# coding: iso-8859-1

import sisapUtils as u
from datetime import datetime
from dateutil.relativedelta import relativedelta
import pandas as pd
import collections as c

'''
*** Operativitzaci� Mireia ***
*** el doble guionet es especifiaci� Leo ***
DONE atesos eap 2018 : finalmente lo rehago segun modelo eqa_last_visit (ermen dice: from assignada_historica)
TODO (TEST) atesos urgencies 2018: finalmente lo rehago segun modelo acut.pac_visites  (ermen dice: nodrizas.urg_poblacio (las del ultimo a�o) recalcularlo con fecha 2018
    francesc dice: acut.pac_visites pero igual es solo �ltimo a�o, mira a ver)
Cribratge d'hepatitis C en pakistanesos
----------------------------------------
Per�ode: 2018
Poblaci�: Pacients pakistanesos assignats a la Regi� Sanit�ria de Barcelona (3 �mbits: Barcelona, Metro Nord i Metro Sud). 
           Donem dades dels EAP ICS i de la Xarxa.
    -- inclusi�:
        -- Pacients (id_permanent)
        -- Nacionalitat: pakistan (cod: ?)
        -- Assignats a UAB_UP (up) que el seu �mbit sigui BCN, Metro-nord o Metro-Sud (via cat_centres)
            -- o b� que no sent ICS siguin de EAP amb ABS a territoris de BCN, Metro-nord, Metro-sud
Com passar les dades
De moment farem una taula amb hash, veurem si despr�s val la pena agrupar o no. D?aix� dependr� com els passem les dades.

    - id_permanent: Hash
    - up: Equip on est� assignat el pacient
    - ates_2018 (0/1): T� una visita a l?equip el 2018?
    - Data del resultat de la �ltima serologia pel virus de la Hepatitis C (pr�via a 01/01/2019)
        -- nodrizas.nod_serologies(cod) <--> nodrizas.cat_dbs(codi)
        -- select codi, agrupador from import.cat_dbscat where taula = 'serologies' and agrupador in ('V_VHC_CARREGA', 'V_VHC_SEROLOGIA');
    - �ltima c�rrega (RNA): 
        - data de la �ltima
        - valor
    - Data del diagn�stic d'Hepatitis C ja sigui obert o tancat
'''

PERIODE = (2015, 2016, 2017, 2018, 2019)


class HepatitisC(object):
    def __init__(self):
        '''.'''

        ts = datetime.now()
        self.get_centres()
        print('{}: Catàleg CENTRES {}'.format(datetime.now() - ts, len(self.centres)))

        ts = datetime.now()
        self.get_catSerologies()
        print('{}: Catàleg SEROLOGIES {}'.format(datetime.now() - ts, len(self.cat_serologies)))

        ts = datetime.now()
        self.get_catNacionalitat()
        print('{}: Catàleg NACIONALITAT {}'.format(datetime.now() - ts, len(self.cat_nacionalitat)))

        ts = datetime.now()
        self.get_poblacio()
        print('{}: POBLACIÓ {}'.format(datetime.now() - ts, len(self.poblacio)))

        ts = datetime.now()
        self.get_u11()
        print('{}: U11: {}'.format(datetime.now() - ts, len(self.u11)))

        ts = datetime.now()
        self.get_hash()
        print('{}: HASH'.format(datetime.now() - ts))

        ts = datetime.now()
        self.get_visit_subtables()
        print('{}: get_visit_subtables'.format(datetime.now() - ts))

        ts = datetime.now()
        self.get_ates()
        print('{}: ates'.format(datetime.now() - ts))

        ts = datetime.now()
        self.get_serologies()
        print('{}: SEROLOGIES'.format(datetime.now() - ts))

        ts = datetime.now()
        self.update_serologies()
        print('{}: UPD_SEROLOGIES'.format(datetime.now() - ts))

        ts = datetime.now()
        self.get_dxPrevis()
        print('{}: DIAGNÒSTICS'.format(datetime.now() - ts))

        ts = datetime.now()
        self.get_derivacions()
        print('{}: DERIVACIONS'.format(datetime.now() - ts))

        ts = datetime.now()
        self.get_upload()
        print('{}: UPLOAD: {}'.format(datetime.now() - ts, len(self.upload)))

        ts = datetime.now()
        self.export_upload()
        print('{}: EXPORT UPLOAD: {}'.format(datetime.now() - ts, len(self.upload)))

        # ts = datetime.now()
        # self.put_outfile()
        # print('{}: outputs'.format(datetime.now() - ts))

    def get_visit_subtables(self):
        """ SE QUEDA CON LAS PARTICIONES del PERIDOE
            copiado de informe 215, tambien 209 y 219
            faltaria hacer el _workers y el _multiprocess
        """
        self.visit_subtables = c.defaultdict(set)     
        sql = "select year(visi_data_visita) from {} limit 1"
        for taula in u.getSubTables("visites","import"):
            if taula[-6:] != '_s6951':  # esto arregla un NoneTypeError (excluyendo visitas de prisiones)
                this, = u.getOne(sql.format(taula), "import")
                for year in PERIODE:
                    if year == this:
                        self.visit_subtables[year].add(taula)

    def get_centres(self):
        '''.'''
        self.centres = {}
        
        db = "nodrizas"
        tb = "cat_centres"
        sql = '''SELECT scs_codi, ics_desc FROM {}.{}'''.format(db, tb)

        for up, desc in u.getAll(sql, db):
            self.centres[up] = {'desc': desc}

    def get_catSerologies(self):
        '''.'''
        
        self.cat_serologies = {}

        db = 'import'
        tb = 'cat_dbscat'
        flt = """WHERE taula = 'serologies' \
                 and agrupador in ('V_VHC_SEROLOGIA', 'V_VHC_CARREGA',
                                   'V_VHB_SEROLOGIA_AG_S', 'V_VHB_SEROLOGIA_AG_E',
                                   'V_VHB_SEROLOGIA_AC_C_IGG', 'V_VHB_SEROLOGIA_AC_C_IGM',
                                   'V_VHB_SEROLOGIA_AC_E', 'V_VHB_SEROLOGIA_AC_S',
                                   'V_VHB_CARREGA',
                                   'V_VIH_SEROLOGIA')"""
        sql = '''SELECT codi, agrupador FROM {}.{} {}'''.format(db, tb, flt)
        
        for codi, agrupador in u.getAll(sql, db):
            self.cat_serologies[codi] = agrupador

    def get_catNacionalitat(self):
        """ . """
        self.cat_nacionalitat = {}
        db = 'nodrizas'
        sql = "select codi_nac, desc_nac from cat_nacionalitat"
        for cod, desc in u.getAll(sql, db):
            cod = int(cod)
            self.cat_nacionalitat[cod] = {'desc': desc}
        #print(self.cat_nacionalitat)

    def get_poblacio(self):
        '''.'''
        
        self.poblacio = {}
        
        db = 'nodrizas'
        tb = 'assignada_tot'
        sql = """select nacionalitat, id_cip, up, edat, sexe \
                 from {}.{}
                 where sexe!='M'""".format(db, tb)

        for nacionalitat, id_cip, up, edat, sexe in u.getAll(sql, db):
            if up in self.centres.keys():
                # if nacionalitat == '':
                #    nacionalitat = 724
                # nac_desc = self.cat_nacionalitat[int(nacionalitat)]['desc'] if int(nacionalitat) in self.cat_nacionalitat else nacionalitat
                self.poblacio[id_cip] = {'hash': None,
                                            'sector': None,
                                            'up': up,
                                            'edat': edat,
                                            'sexe': sexe,
                                            'nac': nacionalitat,
                                            # 'ates_EAP_2015': 0,
                                            # 'ates_CUAP_2015': 0,
                                            # 'ates_EAP_2016': 0,
                                            # 'ates_CUAP_2016': 0,
                                            # 'ates_EAP_2017': 0,
                                            # 'ates_CUAP_2017': 0,
                                            # 'ates_EAP_2018': 0,
                                            # 'ates_CUAP_2018': 0,
                                            # 'ates_EAP_2019': 0,
                                            # 'ates_CUAP_2019': 0,
                                            # 'vhc_serologia_dat': None, # 'YYYYMMDD'
                                            # 'vhc_serologia_val': None,
                                            # 'vhc_carrega_dat': None, # 'YYYYMMDD'
                                            # 'vhc_carrega_val': None,
                                            'vhc_dx_previ_dat': None, # datetime -> 'YYYY-MM-DD'
                                            'vhc_dx_previ_tancat_dat': None,
                                            'vhc_dx_previ_obert': None,
                                            'vhc_dx_tipus': None,
                                            # 'vhc_der_dat': None,
                                            # 'vhb_serologia_ag_s_dat': None, # 'YYYYMMDD'
                                            # 'vhb_serologia_ag_s_val': None,
                                            # 'vhb_serologia_ag_e_dat': None, # 'YYYYMMDD'
                                            # 'vhb_serologia_ag_e_val': None,
                                            # 'vhb_serologia_ac_s_dat': None, # 'YYYYMMDD'
                                            # 'vhb_serologia_ac_s_val': None,                                            
                                            # 'vhb_serologia_ac_c_igg_dat': None, # 'YYYYMMDD'
                                            # 'vhb_serologia_ac_c_igg_val': None,
                                            # 'vhb_serologia_ac_c_igm_dat': None, # 'YYYYMMDD'
                                            # 'vhb_serologia_ac_c_igm_val': None,
                                            # 'vhb_serologia_ac_e_dat': None, # 'YYYYMMDD'
                                            # 'vhb_serologia_ac_e_val': None,
                                            # 'vhb_carrega_dat': None, # 'YYYYMMDD'
                                            # 'vhb_carrega_val': None,
                                            'vhb_dx_previ_dat': None, # datetime -> 'YYYY-MM-DD'
                                            'vhb_dx_tipus': None,
                                            # 'vhb_der_dat': None,
                                            # 'vih_serologia_dat': None, # 'YYYYMMDD'
                                            # 'vih_serologia_val': None,
                                            'vih_dx_previ_dat': None # datetime -> 'YYYY-MM-DD'                                            
                                            # 'vih_der_dat': None
                                            }
        #print(self.poblacio)

    def get_u11(self):
        '''.'''
        print("---------------------------------------------------------- U11")
        self.u11 = {}
        db = 'import'
        tb = 'u11'
        sql = 'select id_cip, hash_d, codi_sector from {}.{}'.format(db, tb)
        for id_cip, hash_d, sector in u.getAll(sql, db):
            if id_cip in self.poblacio:
                self.u11[id_cip] = (hash_d, sector)

    def get_hash(self):
        '''la sortida per hash'''
        print("--------------------------------------------------------- HASH")
        for id_cip, val in self.u11.items():
            if id_cip in self.poblacio:
                self.poblacio[id_cip]['hash'] = val[0]
                self.poblacio[id_cip]['sector'] = val[1]

    def get_atesURG2018(self):
        """ TODO: francesc dice: acut.pac_visites pero igual es solo �ltimo a�o, mira a ver """
        """model urg_poblacio"""
        # centres = {}
        # sql = "select up_codi_up_scs, a.up_codi_up_ics, up_desc_up_ics, c.dap_codi_dap, c.dap_desc_dap, d.amb_codi_amb, d.amb_desc_amb \
        #     from cat_gcctb007 a \
        #     inner join cat_gcctb008 b on a.up_codi_up_ics = b.up_codi_up_ics \
        #     inner join cat_gcctb006 c on a.dap_codi_dap = c.dap_codi_dap \
        #     inner join cat_gcctb005 d on c.amb_codi_amb = d.amb_codi_amb \
        #     where (a.up_desc_up_ics like 'ACUT%' or \
        #             a.up_desc_up_ics like 'CUAP%' or \
        #             a.up_desc_up_ics like 'PAC %' or \
        #             a.up_desc_up_ics like 'CAC %' or \
        #             a.up_desc_up_ics like 'DISPOSITIU%' or \
        #             a.up_desc_up_ics like '%CONTINUADA%') and \
        #             a.up_data_baixa = 0 and \
        #             b.up_data_baixa = 0"
        # for up, br, des, sapc, sapd, ambc, ambd in getAll(sql, imp):
        #     centres[up] = ['CUAP' + br if br[0] == "0" else br, des, br, sapc, sapd, ambc, ambd]
        pass

    def get_ates(self):
        """ esta adaptado a import.visitas para que funcione cuando visitas anteriores a ultimos 2 a�os"""
        # databases
        db = 'import'
        nod = 'nodrizas'
        
        # conjunt d'UPs d'urgencies segons centres d'urgencies del SISAP
        UP_URG = set()
        sql = "select scs_codi from urg_centres"
        for up, in u.getAll(sql, nod):
            UP_URG.add(up)

        # tuple(Servei, modul) per defecte de les urgencies
        DEFAULT_URG_MODUL = ("URGEN", "URCEN") 

        # conjunt d'altres moduls d'urgencies segons CMBDAP. definits pel full-path: tuple(sec, cen, cla, ser, mod)
        sql = """
                select
                    cuag_sector,
                    cuag_codi_centre,
                    cuag_classe_centre,
                    cuag_codi_servei,
                    cuag_codi_modul
                from
                    cat_cmbdtb002 """
        OTHER_URG_MODULS = set([row for row in u.getAll(sql, db) if row[3:5] != DEFAULT_URG_MODUL])

        # criteri per excloure d'urgencies les visitesXpacient no presencials
        VISITES_TIPUS_VIRTUAL = ("9E", "9T")

        # criteris de visites per pacient (pasos intermitjos fins 'visites_flt_and')
        visites_flt_equal = ["visi_situacio_visita = 'R'", "visi_data_baixa = 0"]
        visites_dict_not = {
            'visi_etiqueta': ('ESCO', 'VAES', 'RODN', 'VESC', 'VACU', 'RODON', 'V.ES'),
            'visi_modul_codi_modul': ('VESC', 'I-COL', 'I-VAC', 'RODN', 'RODN1', 'RODN2', 'RODN3', 'RODN4', 'RODN5', 'RODON', 'RODON1', 'RODON2', 'RODON3', 'RODON4', 'RODON5', 'REV', 'UAAU', 'ALTES', 'ADM'),
            'visi_servei_codi_servei': ('UAAU', 'ADM')
        }
        visites_flt_not = []
        for k, v in visites_dict_not.items():
            visites_flt_not.append(" not in ".join((k, str(v))),)

        # Plantilla SQL de visitesXpacient
            # amb filtres 'AND' aplicat: (visites_flt_equal + visites_flt_not)
        sql = """
                SELECT
                    codi_sector,
                    visi_centre_codi_centre,
                    visi_centre_classe_centre,
                    visi_servei_codi_servei,
                    visi_modul_codi_modul,
                        id_cip,
                        visi_up,
                        visi_tipus_visita
                FROM
                    {{db}}.{{tb}}
                WHERE
                    {where}
                """.format(where = " and ".join(i for i in visites_flt_equal + visites_flt_not))

        # nreg_pob = 0
        # nreg_max = 1

        self.ates, self.urg = c.defaultdict(set), c.defaultdict(set)
        for year in PERIODE:
            for tb in self.visit_subtables[year]:
                for sector, centre, classe, servei, modul, id_cip, up, tipus in u.getAll(sql.format(db= db, tb= tb), db):
                    if up in self.centres.keys():
                        self.ates[year].add(id_cip)
                    if up in UP_URG and tipus not in VISITES_TIPUS_VIRTUAL:
                        if (servei, modul) == DEFAULT_URG_MODUL or (sector, centre, classe, servei, modul) in OTHER_URG_MODULS:
                            self.urg[year].add(id_cip)
        # for id_cip, in u.getAll(sql, db):
        #    self.ates.add(id_cip)

        # for k in self.poblacio:
        #     for year in PERIODE:
        #         if k in self.ates[year]:
        #             if year==2015:
        #                 self.poblacio[k]['ates_EAP_2015'] = 1
        #             elif year==2016:
        #                 self.poblacio[k]['ates_EAP_2016'] = 1
        #             elif year==2017:
        #                 self.poblacio[k]['ates_EAP_2017'] = 1
        #             elif year==2018:
        #                 self.poblacio[k]['ates_EAP_2018'] = 1
        #             elif year==2019:
        #                 self.poblacio[k]['ates_EAP_2019'] = 1
        #         if k in self.urg[year]:
        #             if year == 2015:
        #                 self.poblacio[k]['ates_CUAP_2015'] = 1
        #             elif year == 2016:
        #                 self.poblacio[k]['ates_CUAP_2016'] = 1
        #             elif year == 2017:
        #                 self.poblacio[k]['ates_CUAP_2017'] = 1
        #             elif year == 2018:
        #                 self.poblacio[k]['ates_CUAP_2018'] = 1
        #             elif year == 2019:
        #                 self.poblacio[k]['ates_CUAP_2019'] = 1

    def get_serologies(self):
        """ . """

        self.vhc_serologies, self.vhc_carregues = c.defaultdict(set), c.defaultdict(set)

        self.vhb_serologies_ag_s, self.vhb_serologies_ag_e = c.defaultdict(set), c.defaultdict(set)
        self.vhb_serologies_ac_c_igg, self.vhb_serologies_ac_c_igm = c.defaultdict(set), c.defaultdict(set)
        self.vhb_serologies_ac_s, self.vhb_serologies_ac_e = c.defaultdict(set), c.defaultdict(set)
        self.vhb_carregues = c.defaultdict(set)

        self.vih_serologies = c.defaultdict(set)

        db = 'nodrizas'
        # tb = 'nod_serologies'
        tb = 'sidiap_serologies'
        # flt = self.cat_serologies.iteritems() # esto hay que perfilarlo mejor
        sql = '''SELECT id_cip, dat, cod, val FROM {}.{}'''.format(db, tb)

        for id_cip, dat, cod, val in u.getAll(sql, db):
            dat = datetime.strptime(dat, "%Y%m%d").date()            
            if cod in self.cat_serologies:
                if dat < datetime(2020,1,1).date():
                    if self.cat_serologies[cod] == 'V_VHC_SEROLOGIA':
                        self.vhc_serologies[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHC_CARREGA':
                        self.vhc_carregues[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_SEROLOGIA_AG_S':
                        self.vhb_serologies_ag_s[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_SEROLOGIA_AG_E':
                        self.vhb_serologies_ag_e[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_SEROLOGIA_AC_C_IGG':
                        self.vhb_serologies_ac_c_igg[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_SEROLOGIA_AC_C_IGM':
                        self.vhb_serologies_ac_c_igm[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_SEROLOGIA_AC_E':
                        self.vhb_serologies_ac_e[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_SEROLOGIA_AC_S':
                        self.vhb_serologies_ac_s[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VHB_CARREGA':
                        self.vhb_carregues[id_cip].add((dat, val))
                    if self.cat_serologies[cod] == 'V_VIH_SEROLOGIA':
                        self.vih_serologies[id_cip].add((dat, val))

    def update_serologies(self):
        '''.'''

        self.vhc_serologies_pax = {}
        for id_cip in self.vhc_serologies:
            maxtupla = max(self.vhc_serologies[id_cip])
            self.vhc_serologies_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhc_carregues_pax = {}
        for id_cip in self.vhc_carregues:
            maxtupla = max(self.vhc_carregues[id_cip])
            self.vhc_carregues_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_serologies_ag_s_pax = {}
        for id_cip in self.vhb_serologies_ag_s:
            maxtupla = max(self.vhb_serologies_ag_s[id_cip])
            self.vhb_serologies_ag_s_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_serologies_ag_e_pax = {}
        for id_cip in self.vhb_serologies_ag_e:
            maxtupla = max(self.vhb_serologies_ag_e[id_cip])
            self.vhb_serologies_ag_e_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_serologies_ac_s_pax = {}
        for id_cip in self.vhb_serologies_ac_s:
            maxtupla = max(self.vhb_serologies_ac_s[id_cip])
            self.vhb_serologies_ac_s_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_serologies_ac_c_igg_pax = {}
        for id_cip in self.vhb_serologies_ac_c_igg:
            maxtupla = max(self.vhb_serologies_ac_c_igg[id_cip])
            self.vhb_serologies_ac_c_igg_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_serologies_ac_c_igm_pax = {}
        for id_cip in self.vhb_serologies_ac_c_igm:
            maxtupla = max(self.vhb_serologies_ac_c_igm[id_cip])
            self.vhb_serologies_ac_c_igm_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_serologies_ac_e_pax = {}
        for id_cip in self.vhb_serologies_ac_e:
            maxtupla = max(self.vhb_serologies_ac_e[id_cip])
            self.vhb_serologies_ac_e_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vhb_carregues_pax = {}
        for id_cip in self.vhb_carregues:
            maxtupla = max(self.vhb_carregues[id_cip])
            self.vhb_carregues_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

        self.vih_serologies_pax = {}
        for id_cip in self.vih_serologies:
            maxtupla = max(self.vih_serologies[id_cip])
            self.vih_serologies_pax[id_cip]= {'dat': maxtupla[0], 'val': maxtupla[1]}

    def get_dxPrevis(self):
        '''los dx de hepatitis tambien estan ya agrupados en
        nodrizas.eqa_problemes que iria mas rapido, pero no tienen
        fecha de baja'''
        
        # VHC
        # codis = ['C01-B17.10','C01-B18.2','C01-Z22.52',
        #          'C01-B17.1','C01-B17.11',
        #          'C01-B19.2','C01-B19.20','C01-B19.21',
        #          'B17.1','B18.2']
        # solo hay registro en los 2 primeros

        # VHB
        # codis = ['','']
        codis = ['C01-B17.10', 'C01-B18.2',  # VHC
                 'C01-B16', 'C01-B16.0', 'C01-B16.1', 'C01-B16.2', 'C01-B16.9',
                 'C01-B18.0','C01-B18.1' # VHB
                 'C01-Z21', 'C01-B20', 'C01-R75' # VIH
                 ]
        db = 'import'
        tb = 'problemes'
        flt = "pr_cod_ps in {} and year(pr_dde)>=1900".format(tuple(codis)) # arreglar esto
        sql = '''SELECT id_cip, pr_cod_ps, pr_dde, pr_dba FROM {}.{} WHERE {}'''.format(db, tb, flt)
        
        # nreg_ps = 0
        # nreg_max = 1

        for id_cip, cod, dat, dba in u.getAll(sql, db):
            if id_cip in self.poblacio and dat < datetime(2020,1,1).date():
                if cod in ('C01-B17.10', 'C01-B18.2'): # VHC
                    if not self.poblacio[id_cip]['vhc_dx_previ_dat'] or self.poblacio[id_cip]['vhc_dx_previ_dat'] > dat:
                        # nreg_ps += 1
                        self.poblacio[id_cip]['vhc_dx_previ_dat'] = dat
                        if cod in ('C01-B18.2'):
                            self.poblacio[id_cip]['vhc_dx_tipus'] = 'C'
                        if cod in ('C01-B17.10'):
                            self.poblacio[id_cip]['vhc_dx_tipus'] = 'A'
                        # if nreg_ps <= nreg_max:
                        #     print('ps:: ', self.poblacio[id_cip])

                    if not dba or dba >= datetime(2020,1,1).date():
                        dba = datetime.now().date()
                        self.poblacio[id_cip]['vhc_dx_previ_obert'] = True

                    if self.poblacio[id_cip]['vhc_dx_previ_obert']:
                        self.poblacio[id_cip]['vhc_dx_previ_tancat_dat'] = None
                    else:
                        if not self.poblacio[id_cip]['vhc_dx_previ_tancat_dat'] or self.poblacio[id_cip]['vhc_dx_previ_tancat_dat'] < dba:
                            self.poblacio[id_cip]['vhc_dx_previ_tancat_dat'] = dba

                if cod in ('C01-B16', 'C01-B16.0', 'C01-B16.1', 'C01-B16.2',
                           'C01-B16.9', 
                           'C01-B18.0', 'C01-B18.1'): # VHB
                    if not self.poblacio[id_cip]['vhb_dx_previ_dat'] or self.poblacio[id_cip]['vhb_dx_previ_dat'] > dat:
                        self.poblacio[id_cip]['vhb_dx_previ_dat'] = dat
                        if cod in ('C01-B18.0', 'C01-B18.1'):
                            self.poblacio[id_cip]['vhb_dx_tipus'] = 'C'
                        if cod in ('C01-B16', 'C01-B16.0', 'C01-B16.1', 'C01-B16.2','C01-B16.9'):  # noqa
                            self.poblacio[id_cip]['vhb_dx_tipus'] = 'A'

                if cod in ('C01-Z21', 'C01-B20', 'C01-R75'): # VIH
                    if not self.poblacio[id_cip]['vih_dx_previ_dat'] or self.poblacio[id_cip]['vih_dx_previ_dat'] > dat:
                        self.poblacio[id_cip]['vih_dx_previ_dat'] = dat

    def get_derivacions(self):
        """ . """
        self.vhc_der = {}
        self.vhb_der = {}
        self.vih_der = {}
        sql = """select id_cip, oc_data, inf_servei_d_codi, motiu
                 from nod_derivacions
                 where inf_servei_d_codi in ('10105', 'CL107', '10101',
                                             '10125')"""
        db = 'nodrizas'
        for id_cip, dat, cod, _motiu in u.getAll(sql, db):
            if id_cip in self.poblacio and dat < datetime(2020,1,1).date():
                if self.poblacio[id_cip]['vhc_dx_previ_dat']:
                    if dat >= self.poblacio[id_cip]['vhc_dx_previ_dat'] and dat <= (self.poblacio[id_cip]['vhc_dx_previ_dat'] + relativedelta(days=90)):
                        # for c in ('B18.2','B19.2','B17.1'):
                        #     if c in motiu:
                        self.vhc_der[id_cip] = {'vhc_der_dat': dat}
                if self.poblacio[id_cip]['vhb_dx_previ_dat']:
                    if dat >= self.poblacio[id_cip]['vhb_dx_previ_dat'] and dat <= (self.poblacio[id_cip]['vhb_dx_previ_dat'] + relativedelta(days=90)):
                        # for c in ('B18.0','B18.1','B19.1','B16'):
                        #     if c in motiu:                
                        #self.poblacio[id_cip]['vhb_der_dat'] = dat
                        self.vhb_der[id_cip] ={'vhb_der_dat': dat}                      
                if self.poblacio[id_cip]['vih_dx_previ_dat']:
                    if dat >= self.poblacio[id_cip]['vih_dx_previ_dat'] and dat <= (self.poblacio[id_cip]['vih_dx_previ_dat'] + relativedelta(days=90)):                                
                        # for c in ('Z21','B20','R75'):
                        if cod in ('10101','10125'): # and c in motiu:                
                            #self.poblacio[id_cip]['vih_der_dat'] = dat                                                
                            self.vih_der[id_cip] ={'vih_der_dat': dat}

    def get_upload(self):
        """ . """
        self.upload = []
        for id_cip_sec in self.poblacio:
            hash_d = self.poblacio[id_cip_sec]['hash']
            sector = self.poblacio[id_cip_sec]['sector']
            up = self.poblacio[id_cip_sec]['up']
            edat = self.poblacio[id_cip_sec]['edat']
            sexe = self.poblacio[id_cip_sec]['sexe']
            nacionalitat = self.poblacio[id_cip_sec]['nac']
            ates_EAP_2015 = 0
            ates_CUAP_2015 = 0
            ates_EAP_2016 = 0
            ates_CUAP_2016 = 0
            ates_EAP_2017 = 0
            ates_CUAP_2017 = 0
            ates_EAP_2018 = 0
            ates_CUAP_2018 = 0
            ates_EAP_2019 = 0
            ates_CUAP_2019= 0
            for year in PERIODE:
                if id_cip_sec in self.ates[year]:
                    if year==2015:
                        ates_EAP_2015 = 1
                    elif year==2016:
                        ates_EAP_2016 = 1
                    elif year==2017:
                        ates_EAP_2017 = 1
                    elif year==2018:
                        ates_EAP_2018 = 1
                    elif year==2019:
                        ates_EAP_2019 = 1
                if id_cip_sec in self.urg[year]:
                    if year == 2015:
                        ates_CUAP_2015 = 1
                    elif year == 2016:
                        ates_CUAP_2016 = 1
                    elif year == 2017:
                        ates_CUAP_2017 = 1
                    elif year == 2018:
                        ates_CUAP_2018 = 1
                    elif year == 2019:
                        ates_CUAP_2019 = 1

            # ates_EAP_2015 = self.poblacio[id_cip_sec]['ates_EAP_2015']
            # ates_CUAP_2015 = self.poblacio[id_cip_sec]['ates_CUAP_2015']
            # ates_EAP_2016 = self.poblacio[id_cip_sec]['ates_EAP_2016']
            # ates_CUAP_2016 = self.poblacio[id_cip_sec]['ates_CUAP_2016']
            # ates_EAP_2017 = self.poblacio[id_cip_sec]['ates_EAP_2017']
            # ates_CUAP_2017 = self.poblacio[id_cip_sec]['ates_CUAP_2017']
            # ates_EAP_2018 = self.poblacio[id_cip_sec]['ates_EAP_2018']
            # ates_CUAP_2018 = self.poblacio[id_cip_sec]['ates_CUAP_2018']
            # ates_EAP_2019 = self.poblacio[id_cip_sec]['ates_EAP_2019']
            # ates_CUAP_2019= self.poblacio[id_cip_sec]['ates_CUAP_2019']

            vhc_serologia_dat = self.vhc_serologies_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhc_serologies_pax else None
            vhc_serologia_val = self.vhc_serologies_pax[id_cip_sec]['val'] if id_cip_sec in self.vhc_serologies_pax else None
            vhc_carrega_dat = self.vhc_carregues_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhc_carregues_pax else None
            vhc_carrega_val = self.vhc_carregues_pax[id_cip_sec]['val'] if id_cip_sec in self.vhc_carregues_pax else None
            vhc_dx_previ_dat = self.poblacio[id_cip_sec]['vhc_dx_previ_dat']
            vhc_dx_previ_tancat_dat = self.poblacio[id_cip_sec]['vhc_dx_previ_tancat_dat']  #noqa
            vhc_dx_previ_obert = self.poblacio[id_cip_sec]['vhc_dx_previ_obert']  # noqa
            vhc_dx_tipus = self.poblacio[id_cip_sec]['vhc_dx_tipus']
            vhc_der_dat = self.vhc_der[id_cip_sec]['vhc_der_dat'] if id_cip_sec in self.vhc_der else None

            vhb_serologia_ag_s_dat = self.vhb_serologies_ag_s_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_serologies_ag_s_pax else None
            vhb_serologia_ag_s_val = self.vhb_serologies_ag_s_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_serologies_ag_s_pax else None
            vhb_serologia_ag_e_dat = self.vhb_serologies_ag_e_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_serologies_ag_e_pax else None
            vhb_serologia_ag_e_val = self.vhb_serologies_ag_e_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_serologies_ag_e_pax else None

            vhb_serologia_ac_s_dat = self.vhb_serologies_ac_s_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_serologies_ac_s_pax else None
            vhb_serologia_ac_s_val = self.vhb_serologies_ac_s_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_serologies_ac_s_pax else None
            vhb_serologia_ac_c_igg_dat = self.vhb_serologies_ac_c_igg_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_serologies_ac_c_igg_pax else None
            vhb_serologia_ac_c_igg_val = self.vhb_serologies_ac_c_igg_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_serologies_ac_c_igg_pax else None            
            vhb_serologia_ac_c_igm_dat = self.vhb_serologies_ac_c_igm_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_serologies_ac_c_igm_pax else None
            vhb_serologia_ac_c_igm_val = self.vhb_serologies_ac_c_igm_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_serologies_ac_c_igm_pax else None            
            vhb_serologia_ac_e_dat = self.vhb_serologies_ac_e_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_serologies_ac_e_pax else None
            vhb_serologia_ac_e_val = self.vhb_serologies_ac_e_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_serologies_ac_e_pax else None            
            vhb_carrega_dat = self.vhb_carregues_pax[id_cip_sec]['dat'] if id_cip_sec in self.vhb_carregues_pax else None
            vhb_carrega_val = self.vhb_carregues_pax[id_cip_sec]['val'] if id_cip_sec in self.vhb_carregues_pax else None            
            vhb_dx_previ_dat = self.poblacio[id_cip_sec]['vhb_dx_previ_dat']
            vhb_dx_tipus = self.poblacio[id_cip_sec]['vhb_dx_tipus']
            vhb_der_dat = self.vhb_der[id_cip_sec]['vhb_der_dat'] if id_cip_sec in self.vhb_der else None

            vih_serologia_dat = self.vih_serologies_pax[id_cip_sec]['dat'] if id_cip_sec in self.vih_serologies_pax else None
            vih_serologia_val = self.vih_serologies_pax[id_cip_sec]['val'] if id_cip_sec in self.vih_serologies_pax else None            
            vih_dx_previ_dat = self.poblacio[id_cip_sec]['vih_dx_previ_dat']
            vih_der_dat = self.vih_der[id_cip_sec]['vih_der_dat'] if id_cip_sec in self.vih_der else None

            self.upload.append((hash_d, sector, up, edat, sexe,
nacionalitat, 
ates_EAP_2015, ates_CUAP_2015,
ates_EAP_2016, ates_CUAP_2016,
ates_EAP_2017, ates_CUAP_2017,
ates_EAP_2018, ates_CUAP_2018,
ates_EAP_2019, ates_CUAP_2019,
vhc_serologia_dat,
vhc_serologia_val,
vhc_carrega_dat,
vhc_carrega_val,
vhc_dx_previ_dat,
vhc_dx_previ_tancat_dat,
vhc_dx_previ_obert,
vhc_dx_tipus,
vhc_der_dat,
vhb_serologia_ag_s_dat,
vhb_serologia_ag_s_val,
vhb_serologia_ag_e_dat,
vhb_serologia_ag_e_val,
vhb_serologia_ac_s_dat,
vhb_serologia_ac_s_val,
vhb_serologia_ac_c_igg_dat,
vhb_serologia_ac_c_igg_val,
vhb_serologia_ac_c_igm_dat,
vhb_serologia_ac_c_igm_val,
vhb_serologia_ac_e_dat,
vhb_serologia_ac_e_val,
vhb_carrega_dat,
vhb_carrega_val,
vhb_dx_previ_dat,
vhb_dx_tipus,
vhb_der_dat,
vih_serologia_dat,
vih_serologia_val,
vih_dx_previ_dat,
vih_der_dat))

    def export_upload(self):
        """ . """
        u.writeCSV(u.tempFolder + 'CribatgeVHCVHBVIH.csv', self.upload)

    def put_outfile(self):
        """ Es para agencia de salut publica """
        tb = "SISAP_ASPCAT_HEPC_ALL"
        db = "redics"
        cols = ['hash varchar(40)',
                'sector varchar(5)',
                'up varchar(5)',
                'edat int',
                'sexe varchar(1)',
                'country_586 int',
                'ates_EAP_2018 int',
                'ates_CUAP_2018 int',
                'serologia_dat date',
                'serologia_val int',
                'carrega_dat date',
                'carrega_val int',
                'dx_previ_dat date',
                'dx_previ_tancat_dat date']
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                           "PREDUMBO"), db)
        pass

if __name__ == "__main__":
    clock_in = datetime.now()
    print(clock_in)
    HepatitisC()
    clock_out = datetime.now()
    print('''START: {}'''.format(clock_in))
    print('''END: {}'''.format(clock_out))
    print('''DELTA: {}'''.format (clock_out - clock_in))
