from sisapUtils import printTime, sshExecuteCommand, sshGetFile, readCSV, tempFolder, writeCSV
from collections import defaultdict
from numpy import std, mean, percentile
from os import remove

indicadors = {'EQA0406': 'ta', 'EQA0407': 'ta', 'EQA1201': 'tn', 'EQA0601': 'oa', 'EQA0801': 'on', 'EQA0802': 'on', 'EQA0803': 'on'}
iguals = {'EQA9001': 'EQA0406', 'EQA9002': 'EQA0407', 'EQA9003': 'EQA1201', 'EQA9101': 'EQA0601', 'EQA9106': 'EQA0801', 'EQA9103': 'EQA0802', 'EQA9104': 'EQA0803'}
iguals_inv = {v: k for k, v in iguals.items()}

host = 'khalix'
name = 'bridge'
folder = 'out_calc_metes\\'
file_bat = 'eqa_dades.bat'
file_base = 'EQA{}_DADES.txt'
noms = {'a': 'DULTS', 'n': 'PEDIA', 'ta': 'TS', 'on': 'ODO', 's': 'SSIR'}
files = {key: file_base.format(val) for key, val in noms.items()}
file_eap = 'catalegeap.txt'
file_mf = 'plantilla_mf.txt'
file_esp = 'ambesper.txt'
file_uba = 'puntsmax_mf.txt'


def get_files():
    command = 'cd {} && {}'.format(folder, file_bat)
    status, stdout, stderr = sshExecuteCommand(command, name, host)  # ##########
    for file in files.values():
        sshGetFile(file, name, host, subfolder=folder)
    sshGetFile(file_eap, name, host, subfolder=folder)
    sshGetFile(file_mf, name, host, subfolder=folder)
    sshGetFile(file_esp, name, host, subfolder=folder)
    sshGetFile(file_uba, name, host, subfolder=folder)


def get_eap():
    eap_pob = {key: set() for key in ('i', 'a', 'n', 't', 'o')}
    for concepte, _r, eap, _r, _r, _r, _r, _r, valor in readCSV(tempFolder + file_eap, sep='{'):
        if concepte == 'AMBDESC' and 'Xarxa Concertada' not in valor:
            eap_pob['i'].add(eap)
        if concepte == 'POBTIP' and valor != 'NENS':
            eap_pob['a'].add(eap)
        if concepte == 'POBTIP' and valor != 'ADULTS':
            eap_pob['n'].add(eap)
        if concepte == 'TSODOTIP' and valor in ('AMBTS', 'SENSEODO'):
            eap_pob['t'].add(eap)
        if concepte == 'TSODOTIP' and valor in ('AMBTS', 'SENSETS'):
            eap_pob['o'].add(eap)
    eaps = defaultdict(set)
    eaps['a'] = eap_pob['i'].intersection(eap_pob['a'])
    eaps['n'] = eap_pob['i'].intersection(eap_pob['n'])
    eaps['ta'] = eaps['a'].intersection(eap_pob['t'])
    eaps['tn'] = eaps['n'].intersection(eap_pob['t'])
    eaps['oa'] = eaps['a'].intersection(eap_pob['o'])
    eaps['on'] = eaps['n'].intersection(eap_pob['o'])
    return eaps


def get_mf():
    mf = {(eap, 'a' if tipus[-1:] == 'A' else 'n'): float(n) for (_a, _b, eap, _d, tipus, _f, _g, _h, n) in readCSV(tempFolder + file_mf, sep='{')}
    return mf


def get_esp():
    esp = set([row[0] for row in readCSV(tempFolder + file_esp, sep='{')])
    return esp


def get_uba():
    uba = set([row[0] for row in readCSV(tempFolder + file_uba, sep='{') if int(float(row[8])) > 0])
    return uba


def get_pmax(coef):
    return 50 if coef < 0.1 else 60 if coef < 0.2 else 70 if coef < 0.3 else 80


def get_pdet(coef):
    return 20 if coef < 0.1 else 30 if coef < 0.2 else 40 if coef < 0.3 else 50 if coef < 0.4 else 100


def conv(n):
    return str(round(n, 8)).replace('.', ',')


def get_dades():
    eaps = get_eap()
    mf = get_mf()
    esp = get_esp()
    uba = get_uba()
    dades = defaultdict(int)
    adults = set()
    key_ind = {}
    for key, file in files.items():
        for indicador, periode, eap, concepte, _edat, poblacio, _r, _r, valor in readCSV(tempFolder + file, sep='{'):
            if indicador not in iguals:
                tipus = indicadors[indicador] if indicador in indicadors else key
                key_ind[indicador] = key
                if key in ('a', 'n'):
                    adults.add(indicador)
                if eap in eaps[tipus] or eap[:2] == 'SD':
                    dades[(eap, poblacio, indicador, concepte)] = float(valor)

    deteccio, resolucio, esperats, metges = defaultdict(list), defaultdict(list), defaultdict(int), defaultdict(int)
    for (eap, poblacio, indicador, concepte), valor in dades.items():
        if concepte == 'DENESPER' or (eap[:2] == 'SD' and concepte == 'DEN'):
            resolucio[(poblacio, indicador)].append((dades[(eap, poblacio, indicador, 'NUM')] / dades[(eap, poblacio, indicador, 'DEN')]) if dades[(eap, poblacio, indicador, 'DEN')] > 0 else 0)
            if indicador in esp:
                deteccio[(poblacio, indicador)].append(dades[(eap, poblacio, indicador, 'DEN')] / valor)
            if indicador in adults:
                esperats[(poblacio, indicador)] += valor
                metges[(poblacio, indicador)] += mf[(eap, key_ind[indicador])] if (eap, key_ind[indicador]) in mf else 0

    # resolucio
    interval = 2
    variacio = {k: (std(valors) / mean(valors)) if mean(valors) > 0 else 0 for k, valors in resolucio.items()}
    pmax = {k: get_pmax(coef) for k, coef in variacio.items()}
    percentils = {k: (percentile(resolucio[k], 20), percentile(resolucio[k], val)) for k, val in pmax.items()}
    mitjana_mf = {k: n / (metges[k] if k[1] in uba else len(resolucio[k])) for k, n in esperats.items()}
    pac_diff = {k: percentils[k][1] * v - percentils[k][0] * v for k, v in mitjana_mf.items()}
    metes = {k: (a if k not in pac_diff or pac_diff[k] >= interval else max([0, b - ((interval / mitjana_mf[k]) if mitjana_mf[k] > 0 else 0)]), b) for k, (a, b) in percentils.items()}
    resolucio_to_export = [(k[0], k[1], None, 1 if k[1] in uba else 0, len(resolucio[k]), conv(variacio[k]), pmax[k], conv(a * 100), conv(b * 100), conv(mitjana_mf[k]) if k in mitjana_mf else None, conv(pac_diff[k]) if k in pac_diff else None, conv(metes[k][0] * 100), conv(mean([metes[k][0], metes[k][1]]) * 100), conv(metes[k][1] * 100), conv((metes[k][0] - a) * 100)) for k, (a, b) in percentils.items()]
    resolucio_to_export.extend([(row[0], iguals_inv[row[1]], row[1]) + (row[3:]) for row in resolucio_to_export if row[1] in iguals_inv])
    export = sorted(resolucio_to_export, key=lambda x: (x[0], x[1]))
    titles = [['poblacio', 'indicador', 'indicador_orig', 'uba', 'n eap', 'cv', 'percentil meta', 'p20', 'pmax', 'pacients mitjana', 'pacients diff', 'mmin', 'mint', 'mmax', 'canvi mmin']]
    writeCSV(tempFolder + 'resolucio_metes.csv', titles + [row for row in export if row[0] == 'NOINSAT'], sep=';')
    export_klx = [(indicador, 'DEF2016', 'NOENT', 'AGMMIN', 'EDATS5', poblacio, 'SEXE', 'N', mmin.replace(',', '.')) for poblacio, indicador, ind_orig, uab, n, cv, ppmax, p20, pmax, mitj, dif, mmin, mint, mmax, canvi in export]
    export_klx.extend([(indicador, 'DEF2016', 'NOENT', 'AGMINT', 'EDATS5', poblacio, 'SEXE', 'N', mint.replace(',', '.')) for poblacio, indicador, ind_orig, uab, n, cv, ppmax, p20, pmax, mitj, dif, mmin, mint, mmax, canvi in export])
    export_klx.extend([(indicador, 'DEF2016', 'NOENT', 'AGMMAX', 'EDATS5', poblacio, 'SEXE', 'N', mmax.replace(',', '.')) for poblacio, indicador, ind_orig, uab, n, cv, ppmax, p20, pmax, mitj, dif, mmin, mint, mmax, canvi in export])
    writeCSV(tempFolder + 'EQA_METES_RESOLUCIO_2016.TXT', export_klx, sep='{')

    # deteccio
    variacio = {k: (std(valors) / mean(valors)) if mean(valors) > 0 else 0 for k, valors in deteccio.items()}
    pdet = {k: get_pdet(coef) for k, coef in variacio.items()}
    metes = {k: percentile(valors, pdet[k]) for k, valors in deteccio.items()}
    deteccio_to_export = [(k[0], k[1], None, conv(variacio[k]), pdet[k], conv(meta * 100), conv(min(meta, 1) * 100)) for k, meta in metes.items()]
    deteccio_to_export.extend([(row[0], iguals_inv[row[1]], row[1]) + (row[3:]) for row in deteccio_to_export if row[1] in iguals_inv and iguals_inv[row[1]] in esp])
    export = sorted(deteccio_to_export, key=lambda x: (x[0], x[1]))
    titles = [['poblacio', 'indicador', 'indicador_orig', 'cv', 'percentil', 'meta', 'corregida']]
    writeCSV(tempFolder + 'deteccio_metes.csv', titles + [row for row in export if row[0] == 'NOINSAT'], sep=';')
    export_klx = [(indicador, 'DEF2016', 'NOENT', 'PORESPER', 'EDATS5', poblacio, 'SEXE', 'N', corregida.replace(',', '.')) for poblacio, indicador, ind_orig, cv, percentil, meta, corregida in export]
    export_klx.extend([row[:3] + ('PORESPERU',) + row[4:8] + (float(row[8]) * 0.8,) for row in export_klx if row[0] in uba])
    writeCSV(tempFolder + 'EQA_PORESPER_2016.TXT', export_klx, sep='{')


def del_files():
    for file in files.values():
        remove(tempFolder + file)
    remove(tempFolder + file_eap)
    remove(tempFolder + file_mf)
    remove(tempFolder + file_esp)
    remove(tempFolder + file_uba)


for func in (get_files, get_dades, del_files):
    printTime()
    func()
