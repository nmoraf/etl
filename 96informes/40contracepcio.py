import sisapUtils as u


TABLE_O = "anticoncepcio"
DATABASE_O = "permanent"
TABLE_D = "sisap_peticio_anticoncepcio"
DATABASE_D = "redics"
USERS = ("PREDUPRP", "PREDUMMP", "PREDUMBO")


cols = ["TOTAL_ACO number" if col == "TOTAL_ACO" else
        u.getColumnInformation(col, TABLE_O, DATABASE_O, desti="ora")["create"]
        for col in u.getTableColumns(TABLE_O, DATABASE_O)]
cols_s = "({})".format(", ".join(cols))
u.createTable(TABLE_D, cols_s, DATABASE_D, rm=True)
data = u.getAll("select * from {}".format(TABLE_O), DATABASE_O)
u.listToTable(list(data), TABLE_D, DATABASE_D)
u.grantSelect(TABLE_D, USERS, DATABASE_D)
