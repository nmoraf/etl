# coding: latin1

"""
Basat en l'indicador de patologia cr�nica de l'ESIAP.
"""

import collections as c

import sisapUtils as u


DEBUG = False
FILE = u.tempFolder + "frequentacio.csv"

SPEC = {
    "HTA": {"ag": [55]},
    "DM": {"ag": [18, 24]},
    "IC": {"ag": [21]},
    "MPOC": {"ag": [62]},
    "RESP": {"ag": [136],
             "ps": ["J60", "J61", "J62", "J62.0", "J62.8", "J63", "J63.0",
                    "J63.1", "J63.2", "J63.3", "J63.4", "J63.5", "J63.8",
                    "J84.1", "J96.1"]},
    "SAOS": {"ag": [356]},
    "ASMA": {"ag": [58]},
    "DLP": {"ag": [47]},
    "OBE": {"ag": [239]},
    "IN.URI": {"ag": [454]},
    "IN.FEC": {"ag": [455]},
    "DEMEN": {"ag": [86]},
    "FERID": {"ps": ["L89", "I83.0", "I73.9", "L89.4", "I00044", "I00046"]},
    "FA": {"ag": [180]},
    "OSTEO": {"ps": ["M17", "M17.0", "M17.1", "M17.2", "M17.3", "M17.4",
                     "M17.5", "M17.9"],
              "th": [5371, 1425, 1426, 1451]},
    "VIH": {"ag": [666]},
    "HEP": {"ps": ["B18", "B18.0", "B18.1", "B18.2", "B18.8", "B18.9"]},
    "CI": {"ag": [1]},
    "DIGES1": {"ag": [328],
               "ps": ["K90.0", "K58", "K58.0", "K58.9"]},
    "DIGES2": {"ps": ["K50", "K50.0", "K50.1", "K50.8", "K50.9", "K51",
                      "K51.0", "K51.1", "K51.2", "K51.3", "K51.4", "K51.5",
                      "K51.8", "K51.9"]},
    "OP": {"ag": [77]},
    "COSES": {"ps": ["M79.7", "G93.3", "T78.4"]},
    "ANSI": {"ps": ["F41", "F41.0", "F41.1", "F41.2", "F41.3",
                    "F41.8", "F41.9"]},
    "DEPRE": {"ag": [425]},
    "IR": {"ag": [52, 53, 54]},
    "ONCO": {"ag": [721],
             "tancat": True},
    "IV": {"ps": ["I87.2"]},
    "MAP": {"ag": [11]},
    "NEURO": {"ag": [728, 344],
              "th": [5262]},
    "FRAG": {"ag": [764]},
    "DISC": {"ag": [301, 45]},
    "OH": {"ag": [84]},
    "DROGUES": {"ag": [797]},
    "ADD": {"ps": ["F63.0"]},
    "DISF": {"ps": ["R13"]}
}
VIP = ("HTA", "DM", "IC")


class Frequentacio(object):
    """."""

    def __init__(self):
        """."""
        self.invert_spec()
        self.get_problemes()
        self.get_visites()
        self.get_centres()
        self.get_resultat()
        self.export()

    def invert_spec(self):
        """."""
        self.codis = c.defaultdict(set)
        self.tancats = set()
        for grup, codis in SPEC.items():
            for key, values in codis.items():
                if key == "tancat":
                    self.tancats.add(grup)
                else:
                    for value in values:
                        if key == "ag":
                            sql = "select criteri_codi from eqa_criteris \
                                   where agrupador = {}".format(value)
                            for fill, in u.getAll(sql, "nodrizas"):
                                self.codis[("ps", fill)].add(grup)
                        else:
                            self.codis[(key, value)].add(grup)

    def get_problemes(self):
        """."""
        self.problemes = c.defaultdict(set)
        jobs = [k for (k, v) in sorted(u.getSubTables("problemes").items(),
                                       key=lambda x: x[1], reverse=True)
                if v > 0]
        if DEBUG:
            resultat = [self._get_problemes_worker(jobs[-1])]
        else:
            resultat = u.multiprocess(self._get_problemes_worker, jobs, 12)
        for worker in resultat:
            for key, patients in worker.items():
                for id in patients:
                    self.problemes[id].add(key)

    def _get_problemes_worker(self, taula):
        """."""
        problemes = c.defaultdict(set)
        rest = set()
        sql = "select id_cip_sec, pr_cod_ps, pr_th, \
                      if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) \
               from {}, \
                    nodrizas.dextraccio \
               where pr_dde <= data_ext and \
                     (pr_data_baixa = 0 or \
                      pr_data_baixa > data_ext)".format(taula)
        for id, ps, th, tancat in u.getAll(sql, "import"):
            for key in [("ps", ps), ("th", th)]:
                for grup in self.codis[key]:
                    if not tancat or grup in self.tancats:
                        if grup in VIP:
                            problemes[(grup, "all")].add(id)
                        else:
                            rest.add(id)
        for (grup, _x), pacients in problemes.items():
            other = set.union(*[problemes[(g, "all")] for (g, _y)
                                in problemes if g != grup])
            problemes[(grup, "alone")] = pacients - rest - other
        return problemes

    def get_visites(self):
        """."""
        self.visites = c.Counter()
        jobs = ["p{}".format(i) for i in range(13)]
        if DEBUG:
            resultat = [self._get_visites_worker(jobs[3])]
        else:
            resultat = u.multiprocess(self._get_visites_worker, jobs)
        for worker in resultat:
            for key, n in worker:
                self.visites[key] += n

    def _get_visites_worker(self, particio):
        """."""
        visites = c.Counter()
        sql = "select pac_id, if(prof_categ like '1%', 'med', 'inf') \
               from ag_longitudinalitat_new partition({}), dextraccio \
               where pac_up = modul_up and \
                     prof_categ in ('30999', '10117', '10999') and \
                     modul_espe <> 'EXTRA' and \
                     vis_data between adddate(data_ext, interval - 1 year) and\
                                      data_ext".format(particio)
        for key in u.getAll(sql, "nodrizas"):
            visites[key] += 1
        return visites.items()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc \
               from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_resultat(self):
        """."""
        self.resultat = c.defaultdict(c.Counter)
        sql = "select id_cip_sec, up \
               from assignada_tot \
               where edat > 14 and ep = '0208'"
        for id, up in u.getAll(sql, "nodrizas"):
            if id in self.problemes:
                for key in self.problemes[id]:
                    self.resultat[(key, up)]["pac"] += 1
                    for cat in ("med", "inf"):
                        self.resultat[(key, up)][cat] += self.visites[(id, cat)]  # noqa

    @staticmethod
    def _pretty(num):
        return str(round(num, 2)).replace(".", ",")

    def export(self):
        """."""
        export = []
        header = [("br", "amb", "sap", "eap", "ps", "grup",
                   "n pac", "n med", "n inf", "freq med", "freq inf")]
        for (key, up), data in sorted(self.resultat.items()):
            centre = self.centres[up]
            n_pac = data["pac"]
            n_med = data["med"]
            n_inf = data["inf"]
            f_med = Frequentacio._pretty(n_med / float(n_pac) if n_pac else 0)
            f_inf = Frequentacio._pretty(n_inf / float(n_pac) if n_pac else 0)
            this = centre + key + (n_pac, n_med, n_inf, f_med, f_inf)
            export.append(this)
        u.writeCSV(FILE, header + sorted(export), sep=";")


if __name__ == "__main__":
    Frequentacio()
