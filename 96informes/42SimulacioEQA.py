# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
from numpy import *

eqa = "eqa_ind"

metodes = {
            'antic': {'taula': 'exp_ecap_uba_antic', 'columna': 'resultatPerPunts'},
            'nou': {'taula': 'exp_ecap_uba', 'columna': 'resolucioPerPunts'}
            }


def get_percentil(valors, perc):
    percentils = round(percentile(valors, perc), 2)
    return percentils


def get_centres():
    centres = {}
    sql = "select scs_codi, ics_codi from nodrizas.cat_centres where ep='0208'"
    for up, br in getAll(sql, eqa):
        centres[up] = br
    return centres


def get_resultat_sintetic(taula, centres):
    sintet = Counter()
    sintetic = defaultdict(list)
    sql = "select up, uba, indicador, punts from {} where tipus='M'".format(taula)
    for up, uba, indicador, punts in getAll(sql, eqa):
        if up in centres:
            sintet[(up, uba)] += punts
    for (up, uba), p in sintet.items():
        sintetic[(up, uba)].append(p)
    return sintetic


def get_resultat_indicadors(taula, columna, centres):
    indicadors = defaultdict(list)
    amb100 = {}
    sql = "select up, indicador, deteccioPerresultat, {0} from {1} where tipus='M'".format(columna, taula)
    for up, ind, detec, result in getAll(sql, eqa):
        if up in centres:
            valor = detec * result
            indicadors[(ind)].append(valor)
            if ind in amb100:
                amb100[ind]['den'] += 1
                if valor == 1:
                    amb100[ind]['num'] += 1
            else:
                amb100[ind] = {'num': 0, 'den': 1}
                if valor == 1:
                    amb100[ind]['num'] += 1
    return indicadors, amb100


def do_simulation(sintetic):
    valors = []
    for (up, uba), val in sintetic.items():
        valors.append(val)
    p80 = get_percentil(valors, 80)
    p50 = get_percentil(valors, 50)
    p20 = get_percentil(valors, 20)
    mitjana = mean(valors, 0)
    desv_std = std(valors, 0)
    return p80, p50, p20, mitjana, desv_std


def do_simul_ind(indicadors, amb100):
    valors = []
    for indicador in indicadors:
        valors = indicadors[(indicador)]
        mitjana = mean(valors, 0)
        den = amb100[indicador]['den']
        try:
            num = amb100[indicador]['num']
        except KeyError:
            num = 0
        print indicador, mitjana, num, den


for metode in metodes:
    centres = get_centres()
    sintetic = get_resultat_sintetic(metodes[metode]['taula'], centres)
    p80, p50, p20, mitjana, desv_std = do_simulation(sintetic)
    print metode, p80, p50, p20, mitjana, desv_std
    if metode == 'nou':
        indicadors, amb100 = get_resultat_indicadors(metodes[metode]['taula'], metodes[metode]['columna'], centres)
        print amb100
        print metode
        do_simul_ind(indicadors, amb100)
