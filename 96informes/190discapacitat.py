# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

"""
Procés passar ATCs MPOC d'uns pacientc concrets
"""


nod = "nodrizas"


file = tempFolder + 'discapacitat.txt'


class discapacitat(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_centres()
        self.get_problemes()
        self.get_poblacio()
        self.export_fitxer()
        
        
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_desc from cat_centres where ep='0208'"
        self.centres = {up: desc for (up, desc) in getAll(sql, 'nodrizas')}
        
    def get_problemes(self):
        """."""
        self.disc={}
        sql = "select id_cip_sec, pr_cod_ps, year(pr_dde), if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) from problemes, nodrizas.dextraccio where  pr_cod_ps in ('Z73.6') \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext)"
        for id, ps, anys, baixa in getAll(sql, "import"):
            self.disc[id] = True
    
    def get_poblacio(self):
        """."""
        self.recomptes = Counter()
        sql = "select id_cip, up, edat, sexe from assignada_tot"
        for id, up, edat, sexe in getAll(sql, "nodrizas"):
            if up in self.centres:
                if id in self.disc:
                    self.recomptes[edat, sexe] += 1
    
    def export_fitxer(self):
        """."""
        upload = []
        for (edat, sexe), n in self.recomptes.items():
            upload.append([edat,sexe,n])
        writeCSV(file, upload)        
           
        
if __name__ == "__main__":
    printTime('Inici')
    
    discapacitat()
    
    printTime('Fi')