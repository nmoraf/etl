# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = "SISAP_ECONSULTA_ACTIVITAT"
db = "redics"

class EConsulta(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_logins()
        self.get_col()
        self.get_id()
        self.get_LMS()
        self.get_visites()
        self.get_vvirtuals()
        self.get_converses()
        self.get_missatges()
        self.get_dades_converses()
        self.load_file()
        self.upload_data()
    
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}
        
    def get_logins(self):
        """."""
        sql = "select ide_usuari, ide_dni from cat_pritb992"
        self.logins = {dni: login for (login, dni) in u.getAll(sql, 'import')}
        
    def get_col(self):
        """."""
        sql = "select lloc_numcol, lloc_proveidor_dni_proveidor \
               from cat_pritb025"
        self.collegiat = {dni: col for (col, dni) in u.getAll(sql, 'import')}       

    def get_id(self):
        """."""
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        self.hash_to_id = {(hash, sec): id for (id, sec, hash) in u.getAll(sql, 'import')} 
    
    def get_LMS(self):
        """Mirem que el pacient estigui a la taula de redics de LMS que vam crear a partir de les dades que ens van passar els de HC3"""
        a = 0
        self.LMS_pacients = {}
        sql = "select codi_sector, hash, data_acreditacio, data_primer_autoregistre, data_ultim_autoregistre from SISAP_LMS_AUTOREGISTRATS"
        for sector, hash, data_acredit, data_first_auto, data_last_auto in u.getAll(sql, "redics"):
            try:
                data_a = datetime.strptime(data_acredit[:10], '%Y-%m-%d')
            except ValueError:
                data_a = datetime.strptime(data_acredit[:10], '%d/%m/%Y')
            try:
                id = self.hash_to_id[(hash, sector)] 
            except KeyError:
                a += 1
                continue
            self.LMS_pacients[(id)] = data_a.strftime('%Y%m')
        print a
       
    def get_visites(self):
        """."""
        self.visites = c.Counter()
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat >= 2015:
                print table
                sql = "select  codi_sector, visi_dni_prov_resp,  visi_tipus_visita, date_format(visi_data_visita, '%Y%m') \
               from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_data_baixa = 0 and visi_situacio_visita ='R'".format(table)
                for sector, prof, tipus, periode in u.getAll(sql, 'import'):
                    if tipus == '9T':
                        self.visites[(sector, prof, periode, '9T')] += 1
                    elif tipus == '9E':
                        self.visites[(sector, prof, periode, '9E')] += 1
                    else:
                        self.visites[(sector, prof, periode, 'presencials')] += 1
    
    def get_vvirtuals(self):
        """."""
        self.virtuals = c.Counter()
        for sector in u.sectors:
            print sector
            sql = "select to_char(VVI_DATA_ACTIV, 'YYYYMM'), vvi_tipus, vvi_num_col from vistb048 where vvi_situacio = 'R'"
            for periode, tipus, col in u.getAll(sql, sector):
                self.virtuals[(sector, col, periode, 'vv')] += 1
                if tipus == 'ECONSULTA':
                    self.virtuals[(sector, col, periode, 'EC')] += 1
                
    
    def get_converses(self):
        """."""
        self.converses = {}
        self.professionals_ec = {}
        self.pacients_first_time = {}
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat,  date_format(conv_dini,'%Y%m') \
               from econsulta_conv, nodrizas.dextraccio"
        for pac, sector, id, autor, desti, estat, periode in u.getAll(sql, 'import'):
            prof = (autor if autor else desti)
            self.converses[(sector, id)] = {'pac': pac,
                                            'prof': prof,
                                            'inicia': not desti,
                                            'estat': estat,
                                            'periode': periode,
                                            'sector': sector,
                                            'msg': {}}
            self.professionals_ec[(sector, prof, periode)] = True
            if pac in self.pacients_first_time:
                periode2 = self.pacients_first_time[pac]
                if periode < periode2:
                    self.pacients_first_time[pac] = periode
            else:
                self.pacients_first_time[pac] = periode
                
    def get_missatges(self):
        """."""
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data, date_format(msg_data,'%Y%m') \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data, period in u.getAll(sql, 'import'):
            if (sector, id_c) in self.converses:
                this = {'prof': autor if autor else desti,
                        'inicia': not desti,
                        'data': data,
                        'periodemsg': period}
                self.converses[(sector, id_c)]['msg'][id] = this
    
    def get_dades_converses(self):
        """."""
        a = 0
        self.inici_prof = c.Counter()
        self.inici_pac = c.Counter()
        self.pacients_ec = c.Counter()
        self.missatges = c.Counter()
        control_pac = {}
        for conversa in self.converses.values():
            if conversa['inicia']:
                if conversa['pac'] in self.LMS_pacients:
                    dat = self.LMS_pacients[conversa['pac']]
                    if dat <= conversa['periode']:
                        self.inici_prof[(conversa['sector'], conversa['prof'], conversa['periode'])] += 1
                else:
                    a+= 1
            if not conversa['inicia']:
                pregunta = conversa['msg'][1]['data']
                resposta = None
                for id in range(2, 1000):
                    if id in conversa['msg']:
                        if conversa['msg'][id]['inicia']:
                            resposta = conversa['msg'][id]['data']
                            break
                    else:
                        break
                if resposta:
                    self.inici_pac[(conversa['sector'], conversa['prof'], conversa['periode'], 'RESPOSTA')] += 1
                else:
                     self.inici_pac[(conversa['sector'], conversa['prof'], conversa['periode'], 'NO RESPOSTA')] += 1
            pac = conversa['pac']
            if pac in self.LMS_pacients:
                dat = self.LMS_pacients[conversa['pac']]
                if dat <= conversa['periode']:
                    if pac in self.pacients_first_time and (pac, conversa['periode']) not in control_pac:
                        if conversa['periode'] == self.pacients_first_time[pac]:
                            self.pacients_ec[(conversa['sector'], conversa['prof'], conversa['periode'], 'NOUS')] += 1
                            control_pac[(pac, conversa['periode'])] = True
                        else:
                            self.pacients_ec[(conversa['sector'], conversa['prof'], conversa['periode'], 'REPETITS')] += 1
                            control_pac[(pac, conversa['periode'])] = True
                    elif pac not in self.pacients_first_time and  (pac, conversa['periode']) not in control_pac:
                        self.pacients_ec[(conversa['sector'], conversa['prof'], conversa['periode'], 'REPETITS')] += 1
                        control_pac[(pac, conversa['periode'])] = True
                    for id in range(1, 1000):
                        if id in conversa['msg']:
                            self.missatges[(conversa['sector'], conversa['prof'], conversa['msg'][id]['periodemsg'])] += 1  
        print a
    
    def load_file(self):
        """."""
        self.upload = []
        for (sector, prof, periode), d in self.professionals_ec.items():
            login = self.logins[prof] if prof in self.logins else None
            col = self.collegiat[prof] if prof in self.collegiat else None
            iniprof = self.inici_prof[(sector, prof, periode)]
            inipacr = self.inici_pac[(sector, prof, periode, 'RESPOSTA')] 
            inipacsr = self.inici_pac[(sector, prof, periode, 'NO RESPOSTA')] 
            pacn = self.pacients_ec[(sector, prof, periode, 'NOUS')]
            pacR = self.pacients_ec[(sector, prof, periode, 'REPETITS')]
            miss = self.missatges[(sector, prof, periode)]
            telf = self.visites[(sector, prof, periode, '9T')] if (sector, prof, periode, '9T') in self.visites else 0
            e9 = self.visites[(sector, prof, periode, '9E')] if (sector, prof, periode, '9E') in self.visites else 0
            presencials = self.visites[(sector, prof, periode, 'presencials')] if (sector, prof, periode, 'presencials') in self.visites else 0
            vvir = self.virtuals[(sector, col, periode, 'vv')] if (sector, col, periode, 'vv') in self.virtuals else 0
            vvirec = self.virtuals[(sector, col, periode, 'EC')] if (sector, col, periode, 'EC') in self.virtuals else 0
            self.upload.append([sector, prof, login, col, periode, iniprof, inipacr, inipacsr, pacn, pacR, miss, presencials, telf, e9, vvir, vvirec])
   
    def upload_data(self):
        """."""
        columns = ["codi_sector varchar2(4)", "dni varchar2(14)",   "login varchar2(20)",  "collegiat varchar2(14)",  "periode varchar2(6)", 
            "inici_prof number", "inici_pac_resposta number", "inici_pac_no_resposta number", "nous_pac number", "pac_repetits number", "missatges number",
            "visites_presencials number", "visites_9T number", "visites_9E number", "virtuals number", "virtuals_econsulta number"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUECR"), db)
                    
if __name__ == '__main__':
    EConsulta()