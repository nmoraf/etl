# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "cmbd_concordanca"
DATABASE = "permanent"

PERIOD = (2015, 2017)
CODES = {
    ("AVC", "A", "C9"): ["430", "431", "432", "433", "434", "435", "436",
                         "437", "4329", "4330", "4331", "4332", "4333", "4338",
                         "4339", "4340", "4341", "4349", "4350", "4351",
                         "4352", "4353", "4358", "4359", "4371", "4378",
                         "4379", "34660", "34661", "34662", "34663", "43301",
                         "43311", "43321", "43331", "43381", "43391", "43401",
                         "43411", "43491", "99702"],
    ("AVC", "C", "C9"): ["438", "V1254"],
    ("AVC", "S", "C9"): ["4380", "4381", "4382", "4383", "4384", "4385",
                         "4386", "4387", "4388", "4389", "43810", "43811",
                         "43812", "43813", "43814", "43819", "43820", "43821",
                         "43822", "43830", "43831", "43832", "43840", "43841",
                         "43842", "43850", "43851", "43852", "43853", "43881",
                         "43882", "43883", "43884", "43885", "43889"],
    ("IAM", "A", "C9"): ["410", "411", "413", "4100", "4101", "4102", "4103",
                         "4104", "4105", "4106", "4107", "4108", "4109",
                         "4110", "4111", "4118", "4130", "4131", "4139",
                         "41000", "41001", "41002", "41010", "41011", "41012",
                         "41020", "41021", "41022", "41030", "41031", "41032",
                         "41040", "41041", "41042", "41050", "41051", "41052",
                         "41060", "41061", "41062", "41070", "41071", "41072",
                         "41080", "41081", "41082", "41090", "41091", "41092",
                         "41189"],
    ("IAM", "C", "C9"): ["412", "414", "4142", "4148", "4149"],
    ("IAM", "S", "C9"): ["4297"],
    ("FRA", "A", "C9"): ["820", "821", "8200", "8201", "8202", "8203", "8208",
                         "8209", "8210", "8211", "8212", "8213", "73314",
                         "73315", "73396", "73397", "82000", "82001", "82002",
                         "82003", "82009", "82010", "82011", "82012", "82013",
                         "82019", "82020", "82021", "82022", "82030", "82031",
                         "82032", "82100", "82101", "82110", "82111", "82120",
                         "82121", "82122", "82123", "82129", "82130", "82131",
                         "82132", "82133", "82139"],
    ("FRA", "C", "C9"): ["9053"],
    ("FRA", "S", "C9"): [],
    ("AVC", "A", "C10"): ["G45", "G45.0", "G45.1", "G45.2", "G45.8", "G45.9",
                          "G46", "G46.0", "G46.1", "G46.2", "G46.3", "G46.4",
                          "G46.5", "G46.6", "G46.7", "G46.8", "I60", "I60.0",
                          "I60.1", "I60.2", "I60.3", "I60.4", "I60.5", "I60.6",
                          "I60.7", "I60.8", "I60.9", "I61", "I61.0", "I61.1",
                          "I61.2", "I61.3", "I61.4", "I61.5", "I61.6", "I61.8",
                          "I61.9", "I62", "I62.9", "I63", "I63.0", "I63.1",
                          "I63.2", "I63.3", "I63.4", "I63.5", "I63.6", "I63.8",
                          "I63.9", "I64", "I67", "I67.8", "I67.9", "I68",
                          "I68.8"],
    ("AVC", "C", "C10"): [],
    ("AVC", "S", "C10"): ["I69", "I69.0", "I69.1", "I69.2", "I69.3", "I69.4",
                          "I69.8"],
    ("IAM", "A", "C10"): ["I20", "I20.0", "I20.1", "I20.8", "I20.9", "I21",
                          "I21.0", "I21.1", "I21.2", "I21.3", "I21.4", "I21.9",
                          "I22", "I22.0", "I22.1", "I22.8", "I22.9", "I23",
                          "I23.8", "I24", "I24.8", "I24.9", "I25.5", "I25.6"],
    ("IAM", "C", "C10"): ["I25", "I25.1", "I25.2", "I25.8", "I25.9"],
    ("IAM", "S", "C10"): [],
    ("FRA", "A", "C10"): ["S72", "S72.0", "S72.1", "S72.2", "S72.3", "S72.4",
                          "S72.7", "S72.8", "S72.9"],
    ("FRA", "C", "C10"): [],
    ("FRA", "S", "C10"): ["T93.1"]
}


class CMBDH(object):
    """."""

    def __init__(self):
        """."""
        self.get_cim10mc()
        self.invert_codes()
        self.get_ecap()
        self.get_poblacio()
        self.get_cmbd()
        self.upload_data()

    def get_cim10mc(self):
        """."""
        sql = "select ps_cod_o_ap, cim10mc from cat_prstb001cmig"
        self.cim10mc = {pre: post for (pre, post) in u.getAll(sql, "import")}

    def invert_codes(self):
        """."""
        self.codes = c.defaultdict(dict)
        for (ps, tip, cla), cods in CODES.items():
            for cod in cods:
                self.codes[cla][cod] = (ps, tip)
                if cod in self.cim10mc:
                    self.codes[cla][self.cim10mc[cod]] = (ps, tip)

    def get_ecap(self):
        """."""
        self.ecap = c.defaultdict(set)
        codes = self.codes["C10"]
        sql = "select id_cip, pr_cod_ps, pr_dde, pr_dba  \
               from {{}} \
               where pr_cod_ps in {}".format(tuple(codes.keys()))
        d = u.getSubTables("problemes")
        tables = sorted(d, key=d.get, reverse=True)
        jobs = [(sql.format(table), "import") for table in tables]
        dades = u.multiprocess(self._worker, jobs, 12)
        for worker in dades:
            for id, cod, dde, dba in worker:
                ps, tip = codes[cod]
                self.ecap[(id, ps, tip)].add((dde, dba))

    def _worker(self, params):
        """."""
        return [row for row in u.getAll(*params)]

    def get_poblacio(self):
        """."""
        sql = "select id_cip, up, edat, sexe from assignada_tot"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_cmbd(self):
        """."""
        self.resultat = []
        codes = self.codes["C9"]
        sql = "select id_cip, dp, dalta \
               from nod_cmbdh2 \
               where t_act_ah = '11' and \
                     dalta between {}0101 and {}1231 and \
                     dp in {}".format(PERIOD[0], PERIOD[1], tuple(codes.keys()))  # noqa
        for id, cod, dat in u.getAll(sql, "nodrizas"):
            ps, tip = codes[cod]
            if tip == "A" and id in self.poblacio:
                up, edat, sexe = self.poblacio[id]
                ind1 = 1 * any([-3 <= u.monthsBetween(dat, dde) <= 11
                                for (dde, dba) in self.ecap[(id, ps, "A")]])
                ind2 = 0
                for key in [(id, ps, type) for type in ("A", "C", "S")]:
                    if any([not dba or dba > dat
                            for (dde, dba) in self.ecap[key]]):
                        ind2 = 1
                this = (id, up, edat, sexe, ps, dat, cod, ind1, ind2)
                self.resultat.append(this)

    def upload_data(self):
        """."""
        cols = "(id int, up varchar(5), edat int, sexe varchar(1), \
                 grup varchar(3), alta date, codi varchar(10), \
                 ind_1 int, ind_2 int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.resultat, TABLE, DATABASE)


if __name__ == "__main__":
    CMBDH()
