# -*- coding: utf8 -*-

"""
Peticio econsulta pel sidiap
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

LMS_file = u.tempFolder + "Accessos_LMS.txt"

class EConsultaPac(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_gma()
        self.get_medea()
        self.get_assignats()
        self.get_econsulta()
        self.get_LMS()
        #self.get_visites()
        self.get_its()
        
    def get_centres(self):
        """."""
        upload = []
        sql = "select scs_codi, ics_codi, ics_desc, ep, medea from cat_centres"
        for up, br, desc, ep, medea in u.getAll(sql, 'nodrizas'):
            upload.append([up, br, desc, ep, medea])
        file = u.tempFolder + 'Econsulta_centres.txt'
        u.writeCSV(file, upload, sep=';')
    
    def get_gma(self):
        """."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        self.gma = {(id): (cod, cmplx, num) for (id, cod, cmplx, num) 
                    in  u.getAll(*sql)}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        sql = ("select sector, valor \
                from sisap_medea", "redics")
        valors = {sector: str(valor) for (sector, valor)
                  in u.getAll(*sql)}
        self.Imedea = {}                  
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if str(sector) in valors:
                self.Imedea[(id)] = valors[sector] 
                
    def get_assignats(self):
        """Assignada"""
        u.printTime("Assignada")
        upload = []
        self.assignats = {}
        sql = "select id_cip_sec, up, edat, sexe, nacionalitat, pcc, maca, nivell_cobertura, lms_peticio, lms_connexio from assignada_tot"
        for id, edat, sexe, nac, pcc, maca, ncob, lms_pet, lms_con in u.getAll(sql, 'nodrizas'):
            gmac, gmax, medea = None, None, None
            if (id) in self.Imedea:
                medea = self.Imedea[(id)]
            if (id) in self.gma:
                gmac = self.gma[(id)][0]
                gmax = self.gma[(id)][1]
            upload.append([id, up, edat, sexe, nac, medea, gmac, gmax, pcc, maca, ncob, lms_pet, lms_con])
            self.assignats[id] = True
        file = u.tempFolder + 'Econsulta_assignada.txt'
        u.writeCSV(file, upload, sep=';')
                
    def get_econsulta(self):  
        u.printTime("econsulta")
        converses = []
        pac_econs = {}
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv"
        for id, sector, conv_id, conv_autor, conv_desti, conv_estat, conv_dini in u.getAll(sql, 'import'):
            if id in self.assignats:
                converses.append([id, sector, conv_id, conv_autor, conv_desti, conv_estat, conv_dini])
                pac_econs[conv_id] = True
            
        missatges = []
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data, date_format(msg_data,'%Y%m') \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data, period in u.getAll(sql, 'import'):
            if id_c in pac_econs:
                missatges.append([sector, id_c, id, autor, desti, data])
            
        file = u.tempFolder + 'Econsulta_converses.txt'
        u.writeCSV(file, converses, sep=';')
        
        file = u.tempFolder + 'Econsulta_missatges.txt'
        u.writeCSV(file, missatges, sep=';')
            
        
    def get_LMS(self):
        """Ve de un fitxer local que ens van passar al novembre 2019"""
        u.printTime("LMS")
        upload = []
        nies = {}
        sql = ("select id_cip_sec, usua_nia from crg", "import")
        for id, nia in u.getAll(*sql):
            nia = str(nia)
            nies[nia] = id
        for (nia, pes, cid) in u.readCSV(LMS_file, sep=';'):
            if str(nia) in nies:
                id = nies[nia]
                if id in self.assignats:
                    upload.append([id, pes, cid])   
        file = u.tempFolder + 'Econsulta_LMS.txt'
        u.writeCSV(file, upload, sep=';')
        
    def get_visites(self):
        """."""
        u.printTime("Visites")
        visites = []
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select  extract(year_month from visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat >= 201806:
                print table
                sql = "select  codi_sector, id_cip_sec,  visi_tipus_visita, visi_data_visita, visi_servei_codi_servei,  s_espe_codi_especialitat, visi_lloc_visita, visi_etiqueta \
               from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_data_baixa = 0 and visi_situacio_visita ='R'".format(table)
                for sector, id, tipus, periode, servei, especialitat, lloc, etiqueta in u.getAll(sql, 'import'):
                    if id in self.assignats:
                        visites.append([sector, id, tipus, periode, servei, especialitat, lloc, etiqueta]) 
        file = u.tempFolder + 'Econsulta_Visites.txt'
        u.writeCSV(file, visites, sep=';')
        
    def get_its(self):
        """Its de la taula import baixes"""
        u.printTime("Baixes")
        its = []
        sql = "select id_cip_sec, ilt_data_baixa, ilt_data_alta, year(ilt_data_baixa) from baixes"
        for id, baixa, alta, anys in u.getAll(sql, 'import'):
            if anys >= 2018:
                if id in self.assignats:
                    its.append([id, baixa, alta])
        file = u.tempFolder + 'Econsulta_IT.txt'
        u.writeCSV(file, its, sep=';')
           
if __name__ == '__main__':
    u.printTime("Inici")
    
    EConsultaPac()
    
    u.printTime("Final")
    