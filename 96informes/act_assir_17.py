from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random


YEAR = 2018  # S'utilitza nodrizas.ass_visites1, que cal tirar abans pq s'elimina (a ass_visites no hi ha homes). Compte que a la taula hi ha 18 mesos.

nod="nodrizas"
imp="import"
write=True
    
def get_embaras_actiu():
	""" Selecciona los id_cip_seq de las mujeres en assir con embarazo activo en YEAR
        (que la fecha de inicio o la de fin sea en YEAR y que al menos hayan pasado mas de 3 meses de gestacion)
		Devuelve un set de id_cip_seq
	"""
	sql = "select id_cip_sec,inici,fi from {0}.ass_embaras;".format(nod)
	embaras_ids = {id:(inici,fi) for id,inici,fi in getAll(sql,nod) if monthsBetween(inici,fi) > 3 and (inici.year==YEAR or fi.year==YEAR)}
	return embaras_ids

def get_br_assir():
    sql="select up,up_assir,br_assir,assir from {}.ass_centres".format(nod)
    up_assir_dict={}
    for up,up_assir,br,assir in getAll(sql,nod):
        up_assir_dict[up]=(up_assir,br,assir)
    return up_assir_dict

def get_pacient_sexe():
    sql = "select id_cip_sec, usua_sexe from {}.assignada".format(imp)
    return {id:sexe for id,sexe in getAll(sql,nod)}

def get_vis_up(embaras_id,id_sexe,up_br):
    """
    """
    sql = "select id_cip_sec,visi_up,visi_data_visita from {}.ass_visites1 where year(visi_data_visita)={}".format(nod, YEAR)

    
    vis=defaultdict(lambda: defaultdict(set))
    for id,up,data in getAll(sql,nod):
        if up in up_br and id in id_sexe:
            up_assir=up_br[up]
            vis[up_assir]["T"].add(id)
            sexe=id_sexe[id]

            if sexe == "D":
                vis[up_assir]["DT"].add(id)

            if id in embaras_id:
                vis[up_assir]["E"].add(id)
                if not embaras_id[id][0] < data < embaras_id[id][1]:
                    vis[up_assir]["D"].add(id)
            else:
                vis[up_assir][sexe].add(id)
       
    rows = [ (up[0],up[1], up[2], 
             len(vis[up]["E"]), 
             len(vis[up]["D"]),
             len(vis[up]["DT"]),
             len(vis[up]["H"]),
             len(vis[up]["T"]))
             for up in vis]
    return rows


if __name__ == '__main__':
    printTime('inici')
    embaras_id=get_embaras_actiu()
    print(random.sample(embaras_id,5))
    printTime("Emb activas")

    id_sexe=get_pacient_sexe()
    printTime("Id sexe")

    up_br=get_br_assir()
    printTime("Br assir")


    rows=get_vis_up(embaras_id,id_sexe,up_br)  
    printTime('Get Rows')
	
    for row in rows[:3]:
        print(row)
    
    if write:
        header=[("UP ASSIR","BR ASSIR","ASSIR DESC","EMBARASSADES","DONES NO EMBARASSADES","DONES TOTAL","HOMES", "POBLACIO TOTAL")]
        writeCSV(tempFolder + "ACTIVITAT_ASSIR.CSV", header + rows, sep=";")
        """
        with open() as out_file:
            row_format ="{:>40}" * len(rows[0])+"\n"
            
            out_file.write(row_format.format(*header))
            for row in rows:
                out_file.write(row_format.format(*row))
        """