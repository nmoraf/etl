import collections as c
import datetime as d

import sisapUtils as u


any_estudi = 2018
file = u.tempFolder + "avk_miglesias.csv"


def pool(concepte, sql):
    tables = [k for (k, v) in sorted(u.getSubTables(concepte).items(),
                                     key=lambda x: x[1], reverse=True)]
    jobs = [sql.format(table) for table in tables]
    for table in u.multiprocess(worker, jobs, 12):
        for row in table:
            yield row


def worker(sql):
    return [row for row in u.getAll(sql, "import")]


if __name__ == "__main__":
    inr = c.defaultdict(set)
    sql = "select id_cip, vu_dat_act \
           from {{}} \
           where vu_cod_vs in ('INR', 'INR2', 'INR3') and \
                 vu_dat_act <= {}1231".format(any_estudi)
    for id, dat in pool("variables", sql):
        inr[id].add(dat)

    aco = {}
    sql = "select id_cip, ppfmc_pmc_data_ini \
           from {{}} \
           where ppfmc_atccodi like 'B01AA%' and \
                 ppfmc_pmc_data_ini <= {}1231".format(any_estudi)
    for id, dat in pool("tractaments", sql):
        if id not in aco or dat < aco[id]:
            aco[id] = dat

    resultat = [("id", "mes_primer_inr", "n_inr_13d", "dies_aco")]
    for id, dates in inr.items():
        ordenades = sorted(dates)
        first = ordenades[0]
        if first.year == any_estudi:
            mes = first.month
            n = 0
            for i in range(1, 14):
                dat = first + d.timedelta(days=i)
                if dat in dates:
                    n += 1
            if id in aco:
                dies = (aco[id] - first).days
            else:
                dies = None
            resultat.append((id, mes, n, dies))
    u.writeCSV(file, resultat, sep=";")
