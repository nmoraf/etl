# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
from numpy import *

nod = 'nodrizas'

tip_EQA = {
            'Adults': {'pob': ['ADULTS', 'MIXTE'],
                       'TS_ODO': ['AMBTS', 'SENSETS', 'SENSEODO', 'SENSETSODO'],
                       },
            'Pedia': {'pob': ['NENS', 'MIXTE'],
                      'TS_ODO': ['AMBTS', 'SENSETS', 'SENSEODO', 'SENSETSODO'],
                      },
            'ODO': {'pob': ['MIXTE'],
                    'TS_ODO': ['AMBTS', 'SENSETS'],
                    },
            'TS': {'pob': ['MIXTE'],
                   'TS_ODO': ['AMBTS', 'SENSEODO'],
                   },
        }


def get_percentil(valors, perc):
    percentils = round(percentile(valors, perc), 2)
    return percentils


def get_centres_ics():
    centres = {}
    sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
    for up, br in getAll(sql, nod):
        centres[br] = True
    return centres


def get_Valors(centres, tipEQA, tpob, ttsodo):
    valors = []
    a = 0
    for br, ts_odo, pob, eqaA, eqaP, EQAO, EQATS in readCSV('basals_EQA.txt', sep='@'):
        try:
            centres[br]
        except KeyError:
            continue
        if ts_odo in ttsodo:
            if pob in tpob:
                if tipEQA == 'Adults':
                    val = eqaA
                elif tipEQA == 'Pedia':
                    val = eqaP
                elif tipEQA == 'ODO':
                    val = EQAO
                elif tipEQA == 'TS':
                    val = EQATS
                else:
                    val = 0
                if float(val) != 0:
                    valors.append(float(val))
                    a += 1
    p15 = get_percentil(valors, 15)
    p70 = get_percentil(valors, 70)
    p20 = get_percentil(valors, 20)
    p80 = get_percentil(valors, 80)
    print tipEQA, p15, p70, a, p20, p80

centres = get_centres_ics()

for tipEQA in tip_EQA:
    ttsodo = tip_EQA[tipEQA]['TS_ODO']
    tpob = tip_EQA[tipEQA]['pob']
    get_Valors(centres, tipEQA, tpob, ttsodo)
