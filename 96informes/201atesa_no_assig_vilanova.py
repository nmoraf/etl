import collections as c

import sisapUtils as u


pacients = c.defaultdict(set)
visites = c.Counter()
total = set()
sql = "select visi_usuari_cip, visi_servei_codi_servei \
       from vistb043 \
       where visi_data_visita between 2458120 and 2458484 and \
             visi_up = '06009' and \
             visi_situacio_visita = 'R' and \
             visi_rel_proveidor in ('3', '4')"
for id, servei in u.getAll(sql, "6519"):
    pacients[servei].add(id)
    visites[servei] += 1
    total.add(id)

for servei, n in visites.items():
    print servei, len(pacients[servei]), n
print len(total), sum(visites.values())
