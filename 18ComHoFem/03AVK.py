# -*- coding: utf8 -*-

"""
ComHoFem AVK.
"""

import collections
import datetime

import sisapUtils as u


redo = False  # torna a capturar dades
data_acfa_avk = '20141001'
data_inr = '20150101'
data_tall = '20170630'
imp = 'import'
nod = 'nodrizas'
tst = 'test'
master = 'comhofem_avk_{}'
tables = {key: master.format(key)
          for key in ('main', 'flow', 'trt', 'tmp_pob', 'tmp_inr')}


class AVK(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        if redo:
            self.create_flow()
            self.get_chads2()
            self.get_poblacio()
            self.get_acfa()
            self.get_avk()
            self.get_inr()
        self.reget_inr()
        self.get_inr_rang()
        self.get_degradat()
        self.get_trt()
        self.upload_data()

    def create_flow(self):
        """Crea la taula de recomptes."""
        tb = tables['flow']
        u.createTable(tb, '(id varchar(10), des varchar(250), n int)',
                      tst, rm=True)
        u.listToTable((('pob', 'Poblacio ICS, NOINSAT, >=40a', 0),
                       ('fa', 'ACFA abans de {}'.format(data_acfa_avk), 0),
                       ('avk', 'AVK abans de {}, actius a {}'.format(
                                                    data_acfa_avk,
                                                    data_tall), 0),
                       ('inr', 'Seguiment INR 2 anys, no gaps', 0)), tb, tst)

    def __update_flow(self, id, n):
        """Actualitza un recompte."""
        tb = tables['flow']
        sql = "update {} set n = {} where id = '{}'".format(tb, n, id)
        u.execute(sql, tst)

    def get_chads2(self):
        """Valor CHADS2."""
        sql = 'select id_cip_sec, valor from eqa_chads2'
        self.chads2 = {id: val for (id, val) in u.getAll(sql, nod)}

    def get_poblacio(self):
        """Edat i sexe."""
        ics = {up: medea for (up, medea) in u.getAll('select scs_codi, medea \
                                                      from cat_centres', nod)}
        sql = 'select id_cip_sec, up, edat, sexe from assignada_tot \
               where ates = 1 and institucionalitzat = 0 and edat > 39'
        self.poblacio = {id: (id, edat, sexe, ics[up],
                              self.chads2[id] if id in self.chads2 else None)
                         for (id, up, edat, sexe)
                         in u.getAll(sql, nod)
                         if up in ics}
        self.__update_flow('pob', len(self.poblacio))

    def get_acfa(self):
        """Pacients amb ACFA."""
        sql = 'select id_cip_sec from eqa_problemes \
               where ps = 180 and dde < {}'.format(data_acfa_avk)
        self.acfa = set((id for id, in u.getAll(sql, nod)
                         if id in self.poblacio))
        self.__update_flow('fa', len(self.acfa))

    def get_avk(self):
        """Tractaments AVK."""
        sql = 'select id_cip_sec from eqa_tractaments \
               where farmac = 3 and pres_orig < {}'.format(data_acfa_avk)
        self.avk = set((id for id, in u.getAll(sql, nod) if id in self.acfa))
        self.__update_flow('avk', len(self.avk))
        tb = tables['tmp_pob']
        u.createTable(tb, '(id int, edat int, sexe varchar(1), \
                            medea varchar(2),chads2 int)',
                      tst, rm=True)
        u.listToTable((self.poblacio[id] for id in self.avk), tb, tst)

    def get_inr(self):
        """Determinacions INR."""
        sql = "select id_cip_sec, if(vu_cod_vs like 'INR%', 'INR', vu_cod_vs),\
               date_format(vu_dat_act, '%Y%m%d'), vu_val from variables \
               where vu_cod_vs in ('INR', 'INR2', 'INR3', 'RTER1', 'RTER2') \
               and vu_dat_act between {} and {}".format(data_inr, data_tall)
        inr = (row for row in u.getAll(sql, imp)
               if row[0] in self.avk)
        tb = tables['tmp_inr']
        u.createTable(tb, '(id int, cod varchar(5), dat date, val double)',
                      tst, rm=True)
        u.listToTable(inr, tb, tst)

    def reget_inr(self):
        """Captura dades guardades INR rangs."""
        sql = 'select * from {}'.format(tables['tmp_inr'])
        self.inr, self.rinf, self.rsup = {}, {}, {}
        for id, cod, dat, val in u.getAll(sql, tst):
            if cod == 'INR':
                self.inr[(id, dat)] = val
            elif cod == 'RTER1':
                self.rinf[(id, dat)] = val
            else:
                self.rsup[(id, dat)] = val

    def get_inr_rang(self):
        """Uneix INR amb rangs."""
        self.inr_rang = collections.defaultdict(list)
        for key, val in self.inr.items():
            if key in self.rinf and key in self.rsup:
                rinf = self.rinf[key]
                rsup = self.rsup[key]
                self.inr_rang[key[0]].append((key[1], val, rinf, rsup))

    def get_degradat(self):
        """Valors diaris."""
        self.degradat = collections.defaultdict(list)
        for id, info in self.inr_rang.items():
            ordenat = sorted(info, reverse=True)
            posicio = 0
            for dat, val, rinf, rsup in ordenat:
                posicio += 1
                if posicio == len(info):
                    this = (dat, True, rinf <= val <= rsup)
                    self.degradat[id].append(this)
                else:
                    prev_dat, prev_val = ordenat[posicio][0:2]
                    dies = (dat - prev_dat).days
                    canvi_dia = (prev_val - val) / dies
                    for i in range(dies):
                        dia = dat - datetime.timedelta(days=i)
                        estimat = val + (canvi_dia * i)
                        this = (dia, i == 0, rinf <= estimat <= rsup)
                        self.degradat[id].append(this)

    def get_trt(self):
        """Temps en rang terapèutic."""
        self.trt = collections.Counter()
        self.trt_dt = collections.defaultdict(list)
        dies = 720
        gap = 60
        dies_6m = 180
        dies_12m = 360
        data_tall_d = datetime.datetime.strptime(data_tall, '%Y%m%d').date()
        dia_max = data_tall_d - datetime.timedelta(days=gap)
        for id, dades in self.degradat.items():
            if len(dades) >= dies and dades[0][0] >= dia_max:
                numerador = 0
                numerador_6 = 0
                numerador_12 = 0
                sense = 0
                eliminar = False
                alterat_6m = False
                minim_6m = 1
                k = 1
                for i in range(dies):
                    numerador += dades[i][2]
                    if i < dies_12m:
                        numerador_12 += dades[i][2]
                    if i < dies_6m:
                        numerador_6 += dades[i][2]
                    if dades[i][1]:
                        sense = 0
                        numerador_6m = 0
                        if len(dades) >= i + dies_6m:  # ######################
                            for j in range(dies_6m):
                                numerador_6m += dades[i + j][2]
                            trt_6m = numerador_6m / float(dies_6m)
                            self.trt_dt[id].append((k, round(100 * trt_6m, 2)))
                            k += 1
                            if trt_6m < 0.65:
                                alterat_6m = True
                            minim_6m = min(trt_6m, minim_6m)
                    else:
                        sense += 1
                    if sense > gap:
                        eliminar = True
                if not eliminar:
                    self.trt[id] = (
                        round(100 * numerador / float(dies), 2),
                        round(100 * numerador_12 / float(dies_12m), 2),
                        round(100 * numerador_6 / float(dies_6m), 2),
                        1 * alterat_6m,
                        round(100 * minim_6m, 2))

    def upload_data(self):
        """Uneix dades i les puja a destí."""
        self.dades = ((id, edat, sexe, medea, chads2) + self.trt[id]
                      for (id, edat, sexe, medea, chads2)
                      in u.getAll('select * from {}'.format(tables['tmp_pob']),
                                  tst)
                      if id in self.trt)
        tb = tables['main']
        u.createTable(tb, '(id int, edat int, sex varchar(1), \
                            medea varchar(2), chads2 int, \
                            trt_2a double, trt_1a double, trt_6m double, \
                            trt_6m_alterat double, trt_6m_minim double)',
                      tst, rm=True)
        u.listToTable(self.dades, tb, tst)
        self.__update_flow('inr', u.getTableCount(tb, tst))
        self.detall = []
        for id, valors in self.trt_dt.items():
            if id in self.trt:
                self.detall.extend([(id,) + valor for valor in valors])
        tb = tables['trt']
        u.createTable(tb, '(id int, seq int, trt double)', tst, rm=True)
        u.listToTable(self.detall, tb, tst)


if __name__ == '__main__':
    AVK()
