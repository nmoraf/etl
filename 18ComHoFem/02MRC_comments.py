# coding: utf8

"""
Intento completar el comhofem de MRC amb dades extretes a posteriori!
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

tb_mare = "comhofem_mrc_actius"
db = 'test'
imp = 'import'

tiazidics = ['C03AA03', 'C09XA52', 'C09XA54', 'C03AB04', 'C03AH01', 'C03AB03', 'C03EA01', 'C03AX01', 'C09DX03', 'C09DX01', 'C03BA04', 'C03BB04', 'C03EA06', 'C03BA11', 'C09BX01']
ferro = "('B03AA', 'B03AB')"

class MRCcomments(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hashos()
        self.get_comment1()
        #self.get_comment2()
        #self.get_comment3()
        self.get_comment4()
        self.get_comment5()
        
    def get_hashos(self):
        """per poder treure nova info amb id_cip_sec"""
        self.id_to_hash = {}
        sql = 'select id_cip_sec, codi_sector, hash_d from u11'
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[id] = {'sec': sec, 'hash': hash}
        
    def get_comment1(self):
        """sobre trasnferrina i ferrtina en anèmia"""
        self.transferr = {}
        self.solicitud = {}
        for partition in u.getTablePartitions('laboratori', imp):
            sql = "select id_cip_sec, cr_codi_prova_ics, cr_res_lab from {} where cr_codi_prova_ics in ('Q28585','PR047','Q34685')".format(partition)
            for id, prova, resul in u.getAll(sql, imp):
                if id in self.id_to_hash:
                    sec, hash = self.id_to_hash[id]['sec'], self.id_to_hash[id]['hash']
                    self.solicitud[(sec, hash)] = True
                    try:
                        val = float(resul)
                    except ValueError:
                        continue
                    if prova == 'Q34685':
                        if val <20:
                            self.transferr[(sec, hash)] = True
                    else:
                        if val <200:
                            self.transferr[(sec, hash)] = True
        self.anemia_ferro = {}
        sql = "select codi_sector, hash_d from comhofem_mrc_actius where anemia=1 or (hemoglobina >0 and hemoglobina<11)"
        num, num2, den = 0, 0, 0
        for sec, hash in u.getAll(sql, db):
            den += 1
            if (sec, hash) in self.transferr:
                num += 1
                self.anemia_ferro[(sec, hash)] = True
            if (sec, hash) in self.solicitud:
                num2 += 1
        print "transferrina i ferritina en anèmia: ", num, den
        print "anemia amb sol·licitud transfe: ", num2, den
        
    def get_comment2(self):
        """pacients sense IECA, quants tenien FG<30 o K<6"""
        dx_mrc = {}
        sql = "select id_cip_sec, dde from eqa_problemes where ps in (52, 53, 54)"
        for id, dde in u.getAll(sql, 'nodrizas'):
            if id in dx_mrc:
                d2 = dx_mrc[id]
                if dde < d2:
                    dx_mrc[id] = dde
            else:
                dx_mrc[id] = dde
        labK, labfg = {}, {}
        sql = "select id_cip_sec, agrupador, data_var, valor from eqa_variables where agrupador in (30, 739)"
        for id, agr, data, valor in u.getAll(sql, 'nodrizas'):
            if id in dx_mrc:
                date_mrc = dx_mrc[id]
                if id in self.id_to_hash:
                    sec, hash = self.id_to_hash[id]['sec'], self.id_to_hash[id]['hash']
                    if agr == 30:
                        if valor < 30:
                            if data >= date_mrc:
                                labfg[(sec, hash)] = True
                    elif agr == 739:
                        if valor > 6:
                            if data >= date_mrc:
                                labK[(sec, hash)] = True
        numk, numfg, den = 0, 0, 0
        sql = "select codi_sector, hash_d from comhofem_mrc_actius where (hta=1 or dm2=1) and qac=1 and (ieca=0 and araII=0)"
        for sec, hash in u.getAll(sql, db):
            den += 1
            if (sec, hash) in labK:
                numk += 1
            if (sec, hash) in labfg:
                numfg += 1
        print "pacients sense IECA, quants tenien FG<30 o K<6: ",  numk, numfg, den
            
    def get_comment3(self):
        """Tractaments(a 201609 que és data comhofem de mrc): Diurètics tiazídics, diurètics i ADO"""
        self.diu_tiaz = {}
        self.diu = {}
        self.ado = {}
        for partition in u.getTablePartitions('tractaments', imp):
            sql = "select date_format(ppfmc_pmc_data_ini,'%Y%m') from {} limit 1".format(partition)
            for anys, in u.getAll(sql, imp):
                if int(anys) <= int(201609):
                    sql = "select id_cip_sec, ppfmc_atccodi, left(ppfmc_atccodi,3), ppfmc_pmc_data_ini, date_format(ppfmc_data_fi, '%Y%m') from {0} where ppfmc_atccodi like ('C03%') or ppfmc_atccodi like ('C09%') or ppfmc_atccodi like ('A10%')".format(partition)
                    for id, atc, atc3, ini, fi in u.getAll(sql, imp):
                        if int(fi) >= int(201609):
                            if id in self.id_to_hash:
                                sec, hash = self.id_to_hash[id]['sec'], self.id_to_hash[id]['hash']
                                if atc in tiazidics:
                                    self.diu_tiaz[(sec, hash)] = True
                                if atc3 == 'C03':
                                    self.diu[(sec, hash)] = True
                                if atc3 == 'A10':
                                    self.ado[(sec, hash)] = True
                                    
        diur1 = 0
        sql = "select codi_sector, hash_d from comhofem_mrc_actius where aines=1 and (araII=1 or ieca=1)"
        for sec, hash in u.getAll(sql, db):
            if (sec, hash) in self.diu:
                diur1 += 1
        print "Aine+ieca+diurètic: ", diur1
        
        diur2 = 0
        sql = "select codi_sector, hash_d from comhofem_mrc_actius where fg <30 and fg>0"
        for sec, hash in u.getAll(sql, db):
            if (sec, hash) in self.diu_tiaz:
                diur2 += 1
        print "Tiazídics + fg<30: ", diur2
        
        ados1 = 0
        sql = "select codi_sector, hash_d from comhofem_mrc_actius where fg <50 and fg>0"
        for sec, hash in u.getAll(sql, db):
            if (sec, hash) in self.ado:
                ados1 += 1
        print "ADO+ fg<50: ", ados1
        
    def get_comment4(self):
        """Pacients amb anèmia i ferro baix amb prescripció de ferro"""
        self.ferros = {}
        for partition in u.getTablePartitions('tractaments', imp):
            sql = "select date_format(ppfmc_pmc_data_ini,'%Y%m') from {} limit 1".format(partition)
            for anys, in u.getAll(sql, imp):
                if int(anys) <= int(201609):
                    sql = "select id_cip_sec, ppfmc_atccodi, left(ppfmc_atccodi,3), ppfmc_pmc_data_ini, date_format(ppfmc_data_fi, '%Y%m') from {0} where left(ppfmc_atccodi,5) in {1}".format(partition, ferro)
                    for id, atc, atc3, ini, fi in u.getAll(sql, imp):
                        if int(fi) >= int(201609):
                            if id in self.id_to_hash:
                                sec, hash = self.id_to_hash[id]['sec'], self.id_to_hash[id]['hash']
                                self.ferros[(sec, hash)] = True

        den, answer = 0, 0
        for (sec, hash),n in self.anemia_ferro.items():
            den += 1
            if (sec, hash) in self.ferros:
                answer += 1
        print "anèmia amb tractament amb ferro: ", answer, den
   
    def get_comment5(self):
        """Pacients amb AVC, CI antiagregats/anticoagulats"""
        agrs = []
        sql = "select criteri_codi,agrupador from eqa_criteris where agrupador in (3,5,6,379)"
        for cim10, agr in u.getAll(sql, 'nodrizas'):
            agrs.append(cim10)
      
        in_crit = tuple(agrs)
        
        self.antiag = {}
        for partition in u.getTablePartitions('tractaments', imp):
            sql = "select date_format(ppfmc_pmc_data_ini,'%Y%m') from {} limit 1".format(partition)
            for anys, in u.getAll(sql, imp):
                if int(anys) <= int(201609):
                    sql = "select id_cip_sec, ppfmc_atccodi, left(ppfmc_atccodi,3), ppfmc_pmc_data_ini, date_format(ppfmc_data_fi, '%Y%m') from {0} where ppfmc_atccodi in {1}".format(partition, in_crit)
                    for id, atc, atc3, ini, fi in u.getAll(sql, imp):
                        if int(fi) >= int(201609):
                            if id in self.id_to_hash:
                                sec, hash = self.id_to_hash[id]['sec'], self.id_to_hash[id]['hash']
                                self.antiag[(sec, hash)] = True

        den, answer = 0, 0
        sql = "select codi_sector, hash_d from comhofem_mrc_actius where avc=1 or ci=1"
        for sec, hash in u.getAll(sql, db):
            den += 1
            if (sec, hash) in self.antiag:
                answer += 1
        print "AVC, CI amb antiagregació/anticoagulació: ", answer, den
   
if __name__ == '__main__':
    MRCcomments()
    