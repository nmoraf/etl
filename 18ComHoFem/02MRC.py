
from sisapUtils import *
from collections import defaultdict, Counter

db = 'test'
patologia = '18'

nod = 'nodrizas'
imp = 'import'

poliquistosi = "('Q61', 'Q61.1', 'Q61.2', 'Q61.3', 'Q61.5', 'Q61.8', 'Q61.9')"

sql = 'select data_ext from dextraccio'
dext,=getOne(sql, nod)

def get_hashos():
    hashos = {}
    sql = 'select id_cip_sec, codi_sector, hash_d from u11'
    for id, sector, hash in getAll(sql, imp):
        hashos[id] = {'sector': sector, 'hash': hash}
    
    return hashos

def get_centres():
    centres = {}
    sql = "select scs_codi,ics_codi, ics_desc, medea from cat_centres where ep='0208'"
    for up,br, ics_desc, medea in getAll(sql,nod):
        centres[(up)] = medea
    return centres
    
def get_poblacio(centres):
    poblacio = {}
    sql = 'select id_cip_sec, up, edat from assignada_tot where edat > 17 and ates=1'
    for id, up, edat in getAll(sql, nod):
        if up in centres:
            medea = centres[(up)]
            poblacio[id] = {'edat': edat, 'medea':medea}
    return poblacio

hashos = get_hashos()
centres = get_centres()
poblacio = get_poblacio(centres)    

#1 Antecedents
'''
TableMy = 'ComHoFem_MRC_antecedents'
create = '(codi_sector varchar(4), hash_d varchar(40), id_cip_sec int, edat int, medea varchar(10), fg int, qac int )'
createTable(TableMy,create,db, rm=True)

file_cribratge = 'ComHoFem_MRC_antecedents'

def get_cribratge():
    cribFG , cribQAC= {}, {}
    sql = 'select id_cip_sec, data_var, agrupador from eqa_variables where agrupador in (30, 513)'
    for id, data, agrupador in getAll(sql, nod):
        mesos = monthsBetween(data, dext)
        if 0 <= mesos <=11:
            if agrupador == 30:
                cribFG[id] = True
            else:
                cribQAC[id] = True
    return cribFG, cribQAC
    
cribFG , cribQAC = get_cribratge()

def get_antecedents(poblacio, cribFG , cribQAC):
    antecedents = {}
    sql = 'select id_cip_sec from eqa_problemes where ps in (55, 18, 1, 211, 7, 212, 50, 51, 11, 213, 21)'
    for id, in getAll(sql, nod):
        if id in poblacio:
            edat = poblacio[id]['edat']
            medea = poblacio[id]['edat']
            antecedents[id] = {'edat': edat, 'medea': medea}
    sql = 'select id_cip_sec from familiars where af_cod_ps in {}'.format(poliquistosi)
    for id, in getAll(sql, imp):
        if id in poblacio:
            edat = poblacio[id]['edat']
            medea = poblacio[id]['edat']
            antecedents[id] = {'edat': edat, 'medea': medea}
    
    upload = []
    for (id), valors in antecedents.items():
        hash = hashos[id]['hash']
        sector = hashos[id]['sector']
        FG, QAC = 0, 0
        if id in cribFG:
            FG = 1
        if id in cribQAC:
            QAC = 1
        upload.append([sector, hash, id, valors['edat'], valors['medea'],FG, QAC ])
      
    listToTable(upload, TableMy, db)
    writeCSV(file_cribratge, upload, sep=';') 
    
get_antecedents(poblacio, cribFG , cribQAC)
'''
#2 Pacients amb MRC incident

TableMy = 'ComHoFem_MRC_incidents'
create = '(codi_sector varchar(4), hash_d varchar(40), id_cip_sec int, edat int, medea varchar(10), fg int, qac int, microalb int, fg1 int)'
createTable(TableMy,create,db, rm=True)

file = 'ComHoFem_MRC_incidents'

FG_x2 = {}
FG_x1={}
sql = "select id_cip_sec, data_var, valor, usar from eqa_variables where agrupador=30"
for id, data_var, valor, usar in getAll(sql, nod):
    mesos = monthsBetween(data_var, dext)
    if 0 <= mesos <= 23:
        if 0 < valor < 59.99:
            if (id) in FG_x2:
                data = FG_x2[(id)]['data']
                dies = daysBetween(data_var, data)
                if abs(dies) > 90:
                    usar1 = FG_x2[(id)]['usar']
                    difusar = usar - usar1
                    if abs(difusar) == 1:
                        FG_x2[(id)]['num'] += 1
                        FG_x2[(id)]['data'] = data_var
                        FG_x2[(id)]['usar'] = usar
                    else:
                        num = FG_x2[(id)]['num']
                        if num > 1:
                            continue
                        else:
                            FG_x2[(id)]['data'] = data_var
                            FG_x2[(id)]['usar'] = usar
            else:
                FG_x2[(id)] = {'data': data_var, 'num': 1, 'usar': usar}
            if (id) in FG_x1:
                FG_x1[(id)]['num'] += 1
            else:
                FG_x1[(id)] = {'num': 1}
                
CAC_x2 = {}
sql = "select id_cip_sec, data_var, valor, usar from eqa_variables where agrupador=513"
for id, data_var, valor, usar in getAll(sql, nod):
    mesos = monthsBetween(data_var, dext)
    if 0 <= mesos <= 23:
        if  valor >30:
            if (id) in CAC_x2:
                CAC_x2[(id)]['num'] += 1
            else:
                CAC_x2[(id)] = {'num': 1}
microalb_= {}
sql = 'select id_cip_sec from eqa_microalb'
for id, in getAll(sql, nod):
    microalb_[(id)] = True
 
incidents = {}
sql = 'select id_cip_sec, dde from eqa_problemes_incid where ps in (52,53,54)'
for id, dde in getAll(sql, nod):
    mesos = monthsBetween(dde, dext)
    if 0 <= mesos <=11:
        if id in poblacio:
            incidents[id] = {'edat':poblacio[id]['edat'],'medea':poblacio[id]['medea'],'data':dde}


upload = []
for (id), valors in incidents.items():
    hash = hashos[id]['hash']
    sector = hashos[id]['sector']
    numfg = 0
    numfg1 = 0
    numcac = 0
    numalb = 0
    if id in FG_x2:
        nfg = FG_x2[id]['num']
        if nfg >1:
            numfg = 1
    if id in CAC_x2:
        ncac = CAC_x2[id]['num']
        if ncac >1:
            numcac = 1
    if id in microalb_:
        numalb = 1
    if id in FG_x1:
        nfg1 = FG_x1[id]['num']
        if nfg1 == 1:
            numfg1 = 1
    upload.append([sector, hash, id, valors['edat'], valors['medea'],numfg,numcac,numalb,numfg1 ])
      
listToTable(upload, TableMy, db)
writeCSV(file, upload, sep=';')   


#3 MRC
'''
TableMy = 'ComHoFem_MRC_actius'
create = '(codi_sector varchar(4), hash_d varchar(40), id_cip_sec int, edat int, medea varchar(10),  estadiatge double, hta double, dm2 double, dislip double, psobesitat double, hiperuricemia double, \
            ci double, avc double, icc double,claud double, anemia double, hiperpara double, fg double, qac double, ieca double, araII double, aines double, metformina double, aas double, ado double,\
                tabac double, glicada double, tas double, tad double, obesitat double, hemoglobina double, ldl double, tetanus double, grip double, vhb double, pneumococ double, dialisi double)'
createTable(TableMy,create,db, rm=True)

file = 'ComHoFem_MRC_actius'

estadiatge = {}
sql = "select id_cip_sec, vu_val, vu_dat_act from variables where vu_cod_vs = 'VU2000'"
for id, val, data in getAll(sql, imp):
    if id in estadiatge:
        dat1 = estadiatge[id]['data']
        if dat1 < data:
            estadiatge[id]['data'] = data
            estadiatge[id]['nivell'] = val
    else:
        estadiatge[id] = {'nivell': val, 'data': data}
        
filtrat = {}
sql = 'select id_cip_sec, valor from eqa_variables where agrupador=30 and usar=1'
for id, valor in getAll(sql, nod):
    filtrat[id] = valor


    
qac = {}
sql = 'select id_cip_sec, data_var from eqa_variables where agrupador=513 and valor>30 and usar <3'
for id, data in getAll(sql, nod):
    if id in qac:
        dat1 = qac[id]['data']
        mes = monthsBetween(dat1,data)
        if -3 <= mes <= 3:
            qac[id]['num'] += 1
    else:
        qac[id] = {'data':data, 'num': 1}
        
complicprob = defaultdict(list)
sql = 'select id_cip_sec, ps from eqa_problemes where ps in (55,18,47,239,73,1,211,7,212,50,51,11,213, 376, 208, 645, 21, 646)'
for id, ps in getAll(sql, nod):
    complicprob[id].append(ps)
    
tractaments = defaultdict(list)
sql = 'select id_cip_sec, farmac from eqa_tractaments where farmac in (56,72,118,173,5,22)'
for id, farmac in getAll(sql, nod):
    tractaments[id].append(farmac)
    
tabac = {}
sql = 'select id_cip_sec from eqa_tabac where tab=1 and last=1'
for id, in getAll(sql, nod):
    tabac[id] = True
    
glicada, tas, tad, ldl, obesitat, hemoglobina = {}, {}, {}, {}, {}, {}
sql = 'select id_cip_sec, agrupador, data_var, valor from nodrizas.eqa_variables where usar=1 and agrupador in (19, 20, 16, 17, 9, 260)'
for id, agr, data, valor in getAll(sql, nod):
    if agr == 19:
        obesitat[id] = valor
    else:
        mesos = monthsBetween(data, dext)
        if 0<=mesos<= 11:
            if agr == 20:
                glicada[id] = valor
            elif agr == 16:
                tas[id] = valor
            elif agr == 17:
                tad[id] = valor
            elif agr == 9:
                ldl[id] = valor
            elif agr == 260:
                hemoglobina[id] = valor
                
tetanus, grip, vhb, pneumococ = {},{},{},{}
sql = 'select id_cip_sec, agrupador, datamax, dosis from eqa_vacunes where agrupador in (49,99,15,48)'
for id, agr, data, dosis in getAll(sql, nod):
    if agr==49:
        m = monthsBetween(data, dext)
        if 0<= m <= 300:
            tetanus[id] = True
    if agr == 99:
        m = monthsBetween(data, dext)
        if 0<= m <= 18:
            grip[id] = True
    if agr == 15:
        vhb[id] = True
    if agr == 48:
        pneumococ[id]=True
pre_upload = {}   
sql = 'select id_cip_sec from eqa_problemes where ps in (52,53,54)'
for id, in getAll(sql, nod):
    pre_upload[id] = True
    
sql = 'select id_cip_sec from eqa_microALB'
for id, in getAll(sql, nod):
    pre_upload[id] = True 

upload = []    

for id, cosetes in pre_upload.items():
    if id in poblacio:
        edat = poblacio[id]['edat']
        medea = poblacio[id]['medea']
        hash = hashos[id]['hash']
        sector = hashos[id]['sector']
        mrcestadiatge = 99999
        if id in estadiatge:
            mrcestadiatge = estadiatge[id]['nivell']
        hta, dm2, dislip, psobesitat, hiperuri, ci, avc, claud, anemia,hiperpara, icc, dialisi = 0,0,0,0,0,0,0,0,0,0,0,0
        if id in complicprob:
            complis = complicprob[id]
            for problem in complis:
                if problem == 55:
                    hta = 1
                elif problem == 18:
                    dm2 = 1
                elif problem == 47:
                    dislip = 1
                elif problem == 239:
                    psobesitat = 1
                elif problem == 73:
                    hiperuri = 1
                elif problem == 1 or problem == 211:
                    ci = 1
                elif problem == 7 or problem == 212:
                    avc = 1
                elif problem == 50 or problem == 51:
                    avc = 1
                elif problem == 11 or problem == 213:
                    claud = 1
                elif problem == 376:
                    hiperpara = 1
                elif problem == 208 or problem == 645:
                    anemia = 1
                elif problem == 21:
                    icc = 1
                elif problem == 646:
                    dialisi = 1
        mrcfg, mrcqac = None, 0
        if id in filtrat:
            mrcfg = filtrat[id]
        if id in qac:
            mrcqac = qac[id]['num']
        ieca, araII, aines,metformina,aas, ado = 0,0,0,0,0, 0
        if id in tractaments:
            farmacs = tractaments[id]
            for far in farmacs:
                if far == 56:
                    ieca = 1
                elif far == 72:
                    araII = 1
                elif far == 118:
                    aines = 1
                elif far == 173:
                    metformina = 1
                elif far == 5:
                    aas = 1
                elif far == 22:
                    ado = 1
        mrctabac, mrcglicada, mrctas, mrctad, mrcobesitat, mrchemoglo, mrcldl = 0, 0, 0, 0, 0, 0,0
        if id in tabac:
            mrctabac = 1
        if id in glicada:
            mrcglicada = glicada[id]
        if id in tas:
            mrctas = tas[id]
        if id in tad:
            mrctad = tad[id]
        if id in obesitat:
            mrcobesitat = obesitat[id]
        if id in hemoglobina:
            mrchemoglo = hemoglobina[id]
        if id in ldl:
            mrcldl = ldl[id]
        mrctetanus, mrcgrip,mrcvhb,mrcpneumococ = 0,0,0,0
        if id in tetanus:
            mrctetanus =1
        if id in grip:
            mrcgrip = 1
        if id in vhb:
            mrcvhb = 1
        if id in pneumococ:
            mrcpneumococ=1
        upload.append([sector, hash, id, edat, medea, mrcestadiatge, hta, dm2, dislip, psobesitat, hiperuri, ci, avc, icc,claud, anemia,hiperpara, mrcfg, mrcqac, ieca, araII, aines,metformina,aas, ado,mrctabac, mrcglicada, mrctas, mrctad, mrcobesitat, mrchemoglo,mrcldl,mrctetanus,mrcgrip,mrcvhb,mrcpneumococ, dialisi ])      
listToTable(upload, TableMy, db)
writeCSV(file, upload, sep=';')   
'''