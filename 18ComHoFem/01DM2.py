#necessitem que a test hi hagi mst_corbes
from sisapUtils import *
from collections import defaultdict, Counter

db = 'test'
patologia = '18'

nod = 'nodrizas'
imp = 'import'

file_pob = 'ComHoFem_DM2_pob_article'
file_dada ='ComHoFem_DM2_article'

sql = 'select data_ext from dextraccio'
dext,=getOne(sql, nod)

def get_hashos():
    hashos = {}
    sql = 'select id_cip_sec, codi_sector, hash_d from u11'
    for id, sector, hash in getAll(sql, imp):
        hashos[id] = {'sector': sector, 'hash': hash}
    
    return hashos

def get_centres():
    centres = {}
    sql = "select scs_codi,ics_codi, ics_desc, medea from cat_centres where ep='0208'"
    for up,br, ics_desc, medea in getAll(sql,nod):
        centres[(up)] = medea
    return centres
    
def get_patologia():
    pato = {}
    sql = "select id_cip_sec, dde from eqa_problemes where ps in ('{0}')".format(patologia)
    for id, dde in getAll(sql, nod):
        pato[id] = dde
    return pato
    
def get_risc_crib():
    riscos = {}
    sql = "select id_cip_sec from eqa_problemes where ps in (239,1,211,7,212,50,51,11,213,487,563,564,565)"
    for id, in getAll(sql, nod):
        riscos[id] = True
    sql = "select id_cip_sec from eqa_variables where agrupador=19 and usar=1 and valor>30"
    for id, in getAll(sql, nod):
        riscos[id] = True
    sql = "select id_cip_sec from familiars where af_cod_ps like ('E11%%') or af_cod_ps like ('E12%%') or af_cod_ps like ('E13%%')"
    for id, in getAll(sql, imp):
        riscos[id] = True
    
    return riscos
    
def get_glucemia():
    glucemia1a = {}
    glucemia3a = Counter()
    sql = 'select id_cip_sec, data_var from eqa_variables where agrupador in (460, 475)'
    for id, data in getAll(sql, nod):
        mesos = monthsBetween(data, dext)
        if 0 <= mesos <= 11:
            glucemia1a[id] = True
        if 0 <= mesos <= 35:
            glucemia3a[id] += 1
    return glucemia1a, glucemia3a

def get_pob(centres):
    pob = {}
    sql = "select id_cip_sec, up, edat, ates from assignada_tot where id_cip_sec >0 and edat>14"
    for id, up, edat, ates in getAll(sql, nod):
        try:
            medea = centres[(up)]
        except KeyError:
            continue
        pob[(id)] = {'edat': edat, 'medea': medea, 'ates': ates}
    
    return pob
    
def do_pob(pob, pato, riscos, glucemia1a, glucemia3a, hashos):
    pob2 = {}
    for (id), vals in pob.items():
        edat = vals['edat']
        medea = vals['medea']
        ates = vals['ates']
        pob2[(id)] = {'edat': edat, 'medea': medea, 'ates': ates, 'dm2': 0, 'risc': 0, 'gluc1a': 0, 'gluc3a':0}
        if id in pato:
            pob2[(id)]['dm2'] = 1
        if id in riscos:
            pob2[(id)]['risc'] = 1
        if id in glucemia1a:
            pob2[(id)]['gluc1a'] = 1
        if id in glucemia3a:
            ngluc = glucemia3a[id]
            pob2[(id)]['gluc3a'] = ngluc
    upload = []
    for (id), valors in pob2.items():
        hash = hashos[id]['hash']
        sector = hashos[id]['sector']
        upload.append([sector, hash,valors['edat'],valors['medea'],valors['ates'],valors['dm2'],valors['risc'],valors['gluc1a'],valors['gluc3a']])
      
    listToTable(upload, TableMy, db)
    writeCSV(file_pob, upload, sep=';') 

def get_hba1c():
    hba1c = {}
    sql = 'select id_cip_sec, data_var, valor from eqa_variables where agrupador=20 and usar=1'
    for id, data, valor in getAll(sql, nod):
        mesos = monthsBetween(data, dext)
        if 0 <= mesos <= 11:
            hba1c[id] = valor
    return hba1c

def get_complicacions():
    complicacions = {}
    sql = "select id_cip_sec, ps from eqa_problemes where ps in (1, 7, 11, 44, 54, 586, 52, 53)"
    for id, ps in getAll(sql, nod):
        mcv = 0
        ci = 0
        avc = 0
        cint = 0
        retino = 0
        nefro = 0
        neuro = 0
        if int(ps) == 1 or int(ps) == 7 or int(ps) == 11:
            mcv = 1
            if int(ps) == 1:
                ci = 1
            if int(ps) == 7:
                avc = 1
            if int(ps) == 11:
                cint = 1
        elif int(ps) == 44:
            retino = 1
        elif int(ps) == 54 or int(ps) == 52 or int(ps) == 53:
            nefro = 1
        elif int(ps) == 586:
            neuro = 1
        if id in complicacions:
            complicacions[id]['mcv'] += mcv
            complicacions[id]['ci'] += ci
            complicacions[id]['avc'] += avc
            complicacions[id]['cint'] += cint
            complicacions[id]['retino'] += retino
            complicacions[id]['nefro'] += nefro
            complicacions[id]['neuro'] += neuro
        else:
            complicacions[id] ={'mcv': mcv, 'ci':ci, 'avc':avc, 'cint':cint, 'retino': retino, 'nefro': nefro, 'neuro': neuro}
    sql = "select id_cip_sec from eqa_microalb"
    for id, in getAll(sql, nod):
        if id in complicacions:
            complicacions[id]['nefro'] += 1
        else:
            complicacions[id] ={'mcv': 0, 'ci':0, 'avc':0, 'cint':0,'retino': 0, 'nefro': 1, 'neuro': 0}
    
    return complicacions

def get_tires():
    tires = {}
    sql = 'select id_cip_sec, tires from eqa_tires'
    for id, tir in getAll(sql, nod):
        tires[id] = {'tires': tir}
    
    return tires
    
def get_farmacs():
    farmac = {}
    sql = 'select id_cip_sec, farmac from eqa_tractaments where farmac in (173,174,412,588,587,589,590,591,175,592,22)'
    for id, far in getAll(sql, nod):
        farmac[id, far] = True
        
    return farmac
    
def do_controls(pob, pato, hba1c, complicacions, tires, farmacs, hashos):
    diabetics = {}
    for (id), vals in pob.items():
        edat = vals['edat']
        ates = vals['ates']
        if id in pato:
            dde = pato[id]
            years = yearsBetween(dde, dext)
            diabetics[id] = {'edat': edat, 'ates':ates, 'dde': dde, 'years':years, 'glicada': 0, 'val_glic': None, 'complicacions': 0, 'mcv': 0, 'ci': 0,'avc':0,'cint':0,'retino': 0, 'nefro': 0, 'neuro': 0, 'tires': 0, 'metformina':0, 'sulfonil': 0, 'ins_rapida': 0, 'ins_int': 0, 'ins_lent': 0, 'glinida': 0,'tioglitazones':0,'alfa_glic':0,'gliptines':0,'gcp1':0,'altres':0}
            if id in complicacions:
                diabetics[id]['complicacions'] = 1
                diabetics[id]['mcv'] = complicacions[id]['mcv']
                diabetics[id]['ci'] = complicacions[id]['ci']
                diabetics[id]['avc'] = complicacions[id]['avc']
                diabetics[id]['cint'] = complicacions[id]['cint']
                diabetics[id]['retino'] = complicacions[id]['retino']
                diabetics[id]['nefro'] = complicacions[id]['nefro']
                diabetics[id]['neuro'] = complicacions[id]['neuro']
            if id in hba1c:
                try:
                    glicada = hba1c[id]
                    diabetics[id]['glicada'] = 1
                    diabetics[id]['val_glic'] = glicada
                except KeyError:
                    continue
            if id in tires:
                tir = tires[id]['tires']
                diabetics[id]['tires'] = tir
    for (id, far),vs in farmacs.items():
        if id in diabetics:
            met = 0
            sulf = 0
            insr = 0
            insi = 0
            insl = 0
            glin = 0
            tio = 0
            alfa = 0
            glipt = 0
            gcp1 = 0
            ado = 0
            if far == 173:
                met = 1
            elif far == 174:
                sulf = 1
            elif far == 412:
                insr = 1
            elif far == 588:
                insi = 1
            elif far == 587:
                insl = 1
            elif far == 589:
                glin = 1
            elif far == 590:
                tio = 1
            elif far == 591:
                alfa = 1
            elif far == 175:
                glipt = 1
            elif far == 592:
                gcp1 = 1
            elif far == 22:
                ado = 1
            diabetics[id]['metformina'] += met
            diabetics[id]['sulfonil'] += sulf
            diabetics[id]['ins_rapida'] += insr
            diabetics[id]['ins_int'] += insi
            diabetics[id]['ins_lent'] += insl
            diabetics[id]['tioglitazones'] += tio
            diabetics[id]['glinida'] += glin
            diabetics[id]['alfa_glic'] += alfa
            diabetics[id]['gliptines'] += glipt
            diabetics[id]['gcp1'] += gcp1
            diabetics[id]['altres'] += ado
    upload = []
    for (id), valors in diabetics.items():
        hash = hashos[id]['hash']
        sector = hashos[id]['sector']
        upload.append([sector, hash,valors['edat'],valors['ates'],valors['dde'],valors['years'],valors['glicada'],valors['val_glic'],valors['complicacions'],valors['mcv'],valors['ci'],valors['avc'],valors['cint'],valors['retino'],valors['nefro'],valors['neuro'], valors['tires'],valors['metformina'],valors['sulfonil'],valors['ins_rapida'],valors['ins_int'],valors['ins_lent'],valors['tioglitazones'],valors['glinida'],valors['alfa_glic'],valors['gliptines'],valors['gcp1'],valors['altres']])
    listToTable(upload, TableMy, db)
    writeCSV(file_dada, upload, sep=';') 

# 1) Taula per fer Cribratge
TableMy = 'ComHoFem_DM2_pob_article'
create = '(codi_sector varchar(4), hash_d varchar(40), edat int, medea varchar(10), ates int, dm2 int, risc int, gluc1a int, gluc3a int)'
createTable(TableMy,create,db, rm=True)

centres = get_centres()
hashos = get_hashos()
pato = get_patologia()
riscos = get_risc_crib()
glucemia1a, glucemia3a = get_glucemia()
pob = get_pob(centres)
do_pob(pob, pato, riscos, glucemia1a, glucemia3a, hashos)

# 2) Taula per fer controls

TableMy = 'ComHoFem_DM2_article'
create = "(codi_sector varchar(4), hash_d varchar(40), edat int, ates int, dde date, anysdm2 int, glicada int, val_glicada double, complicacions int, mcv int, ci int,avc int,cint int,retino int, nefro int, neuro int, tires double,\
            metformina int, sulfonil int, ins_rapida int, ins_int int, ins_lenta int, tio int,glinides int,alfa_gluc int,gliptines int, gcp1 int, altres int)"
createTable(TableMy,create,db, rm=True)

hba1c = get_hba1c()
complicacions = get_complicacions()
tires = get_tires()
farmacs = get_farmacs()
do_controls(pob, pato, hba1c, complicacions, tires, farmacs, hashos)



