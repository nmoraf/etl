# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


DATABASE = "TEST"
TABLES = ("COMHOFEM_ITS_POBLACIO", "COMHOFEM_ITS_SEROLOGIES")
PERIODE = ("20170101", "20181231")


def get_tables(key, col):
    """."""
    tables = []
    db = "import"
    sql = "select date_format({}, '%Y%m%d') from {} limit 1"
    for table in u.getTablePartitions(key, db):
        if u.getTableCount(table, db) > 0:
            dat, = u.getOne(sql.format(col, table), db)
            if PERIODE[0] <= dat <= PERIODE[1]:
                tables.append(table)
    return(tables)


class All(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_problemes()
        self.get_problemes_incid()
        self.get_usuaris()
        self.get_cribratge()
        self.get_conversor()
        self.upload_data()

    def get_poblacio(self):
        """."""
        medea = {up: valor for (up, valor)
                 in u.getAll("select scs_codi, medea from cat_centres \
                              where ep = '0208'", "nodrizas")}
        sql = "select id_cip_sec, edat, sexe, up \
               from assignada_tot \
               where ates = 1 and \
                     institucionalitzat = 0 and \
                     edat between 15 and 64"
        self.poblacio = {id: (edat, sexe, up, medea[up])
                         for (id, edat, sexe, up)
                         in u.getAll(sql, "nodrizas") if up in medea}

    def get_problemes(self):
        """."""
        ps = {"vih": (666, 681, 667, 682), "vhb": (13, 14),
              "vhc": (12, 29, 550, 553), "its": (406, 680), "cin": (491, 492)}
        conversor = {}
        for des, codis in ps.items():
            for codi in codis:
                conversor[codi] = des
        sql = "select id_cip_sec, ps, date_format(dde, '%Y%m%d') \
               from eqa_problemes \
               where ps in {} and dde <= {}".format(tuple(conversor),
                                                    PERIODE[1])
        self.comorbilitat = set()
        self.diagnostics = c.defaultdict(str)
        for id, cod, dat in u.getAll(sql, "nodrizas"):
            if id in self.poblacio:
                self.comorbilitat.add(id)
                if conversor[cod] in ("vih", "vhb", "vhc"):
                    self.diagnostics[(id, conversor[cod])] = dat

    def get_problemes_incid(self):
        """."""
        ps = {855: "risc", 856: "its", 857: "vih"}
        self.problemes_incid = c.defaultdict(set)
        sql = "select id_cip_sec, ps from eqa_problemes_incid \
               where ps in {} and \
                     dde between {} and {}".format(tuple(ps), *PERIODE)
        for id, agr in u.getAll(sql, "nodrizas"):
            if id in self.poblacio:
                self.problemes_incid[ps[agr]].add(id)

    def get_usuaris(self):
        """."""
        sql = "select ide_usuari, ide_categ_prof_c \
               from cat_pritb992 where ide_categ_prof_c <> ''"
        self.usuaris = {usu: categ for (usu, categ) in u.getAll(sql, "import")}

    def get_cribratge(self):
        """."""
        self.cribrats = set()
        self.en_risc = set()
        self.categories = c.defaultdict(set)
        sqls = {"activitats": ("select id_cip_sec, au_val = '2', au_usu \
                                from {} \
                                where au_cod_ac = 'CA601' and \
                                      au_dat_act between {} and {} and \
                                      au_val in ('1', '2')", "au_dat_act"),
                "variables": ("select id_cip_sec, vu_val = 1, vu_usu \
                               from {} \
                               where vu_cod_vs = 'EA6001' and \
                                     vu_dat_act between {} and {} and \
                                     vu_val in (0, 1)", "vu_dat_act")}
        for key, (sql, dat) in sqls.items():
            for table in get_tables(key, dat):
                this = sql.format(table, *PERIODE)
                for id, risc, usu in u.getAll(this, "import"):
                    if id in self.poblacio:
                        self.cribrats.add(id)
                        if risc:
                            self.en_risc.add(id)
                        if usu in self.usuaris:
                            self.categories[id].add(self.usuaris[usu])

    def get_conversor(self):
        """."""
        self.conversor = {id: (sector, hash) for (id, sector, hash)
                          in u.getAll("select id_cip_sec, codi_sector, hash_d \
                                       from u11", "import")
                          if id in self.poblacio}

    def upload_data(self):
        """."""
        data = ((id,) + self.conversor[id] + values +
                (1 * (id in self.comorbilitat),
                 self.diagnostics[(id, "vih")],
                 self.diagnostics[(id, "vhb")],
                 self.diagnostics[(id, "vhc")],
                 1 * (id in self.problemes_incid["risc"]),
                 1 * (id in self.problemes_incid["its"]),
                 1 * (id in self.problemes_incid["vih"]),
                 1 * (id in self.cribrats),
                 9 if id not in self.cribrats else (1 * (id in self.en_risc)),
                 ",".join(sorted(self.categories[id])))
                for (id, values)
                in self.poblacio.items())
        table = TABLES[0]
        u.createTable(table, "(id int, sector varchar(4), hash varchar(40), \
                               edat int, sexe varchar(1), up varchar(5), \
                               medea varchar(2), comorbilitat int, \
                               dx_vih varchar(8), dx_vhb varchar(8), \
                               dx_vhc varchar(8), conducta_risc int, \
                               contacte_its int, contacte_vih int, \
                               cribratge_fet int, cribratge_risc int, \
                               cribratge_qui varchar(50))",
                      DATABASE, rm=True)
        u.listToTable(data, table, DATABASE)


class Serologies(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_homologades()
        self.get_no_homologades()
        self.upload_data()

    def get_poblacio(self):
        """."""
        sql = "select id from {} \
               where conducta_risc = 1 or \
                     cribratge_risc = 1 or \
                     contacte_its = 1 or \
                     contacte_vih = 1".format(TABLES[0])
        self.poblacio = set([id for id, in u.getAll(sql, DATABASE)])

    def get_homologades(self):
        """."""
        conversor = {codi: (agr.split("_")[1], "_".join(agr.split("_")[2:]))
                     for (codi, agr)
                     in u.getAll("select codi, agrupador from cat_dbscat \
                                  where taula = 'serologies'", "import")
                     if agr.split("_")[1] != "VHA"}
        sql = "select id_cip_sec, dat, cod, val from nod_serologies \
               where dat between {} and {}".format(*PERIODE)
        self.serologies = [(id, dat) + conversor[cod] + (val,)
                           for (id, dat, cod, val) in u.getAll(sql, "nodrizas")
                           if id in self.poblacio and cod in conversor]

    def get_no_homologades(self):
        """."""
        agrupadors = {650: "sifilis", 652: "gonococ", 654: "clamidia"}
        codis = {codi: agrupadors[agr] for (codi, agr)
                 in u.getAll("select criteri_codi, agrupador \
                              from eqa_criteris \
                              where agrupador in {}".format(tuple(agrupadors)),
                             "nodrizas")}
        sql = "select id_cip_sec, date_format(cr_data_reg, '%Y%m%d'), \
                      cr_codi_prova_ics, cr_res_lab \
               from {} \
               where cr_codi_prova_ics in {} and \
                     cr_data_reg between {} and {}"
        for table in get_tables("laboratori", "cr_data_reg"):
            this = sql.format(table, tuple(codis), *PERIODE)
            for id, dat, cod, val in u.getAll(this, "import"):
                ps = codis[cod]
                if ps == "sifilis":
                    prova = None
                    homol = 9
                else:
                    prova = "PCR"
                    if "POSITI" in val.upper():
                        homol = 1
                    elif "NEGATI" in val.upper():
                        homol = 0
                    else:
                        homol = 9
                self.serologies.append((id, dat, ps, prova, homol))

    def upload_data(self):
        """."""
        table = TABLES[1]
        u.createTable(table, "(id int, dat varchar(8), ps varchar(10), \
                               prova varchar(50), val int)",
                      DATABASE, rm=True)
        u.listToTable(self.serologies, table, DATABASE)


if __name__ == "__main__":
    All()
    Serologies()
