# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els històrics de acc que els hem tret de khalix
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'mst_control_eqa'
tb_cat = 'cat_control_indicadors'

class hist_econsulta(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.periodes, self.indicadors, self.resultats = {},[],Counter()
        self.get_territoris()
        self.get_cataleg()
        self.get_indicador()
        self.export_data()
                
    def get_territoris(self):
        """Obtenim àmbits i sector"""
        
        self.centres = {}
        sql = 'select ics_codi, amb_desc, sector from cat_centres'
        for br, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
            
    def get_cataleg(self):
        """Obtenim el catàleg de khalix. Si hi ha nous indicadors, afegir-los"""
        self.cataleg = {}
        file = u.baseFolder + ["67LadyBug\dades_noesb", "ACC_cataleg.txt"]
        file = "\\".join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[(codi)] = {'d': desc, 'p': pare}
            
    
    def get_indicador(self):
        """Les dades de num i den dels indicadors econsulta dels històrics que figurin al ppi del fitxer. """
        nom = "ACC.csv"
        file = u.baseFolder + ["67LadyBug\dades_noesb", nom]
        file = "\\".join(file)
        for indicador, periode, br, tip, n in u.readCSV(file, sep=";"):
            n = int(n)
            self.indicadors.append(indicador)
            periode = periode.replace('A', '20')
            self.periodes[(periode)] = True
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
                
       
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indicadors = tuple(self.indicadors)
        for (periode),i in self.periodes.items():
            sql = "delete a.* from {0} a where periode = '{1}' and indicador in {2}".format(tb, periode, indicadors)
            u.execute(sql, db)
        u.listToTable(upload, tb, db)
        
        upload_cat = []
        for (codi), dadesc in self.cataleg.items():
            desc = dadesc['d']
            pare = dadesc['p']
            upload_cat.append([codi, desc, pare])

        sql = "delete a.* from {0} a where indicador in {1}".format(tb_cat, indicadors)
        u.execute(sql, db)
        u.listToTable(upload_cat, tb_cat, db)
      
if __name__ == '__main__':
    hist_econsulta()
    