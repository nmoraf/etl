CONSOB01@Percentatge de nens i nenes atesos dins del programa Salut i Escola en Consulta Oberta@Salut i escola
CONSOB02@Ratio d'infermeres de Salut i Escola per població escolar de referència@Salut i escola
CONSOB03@Percentatge d'escoles on es realitza Salut i Escola@Salut i escola
CONSOB04@Visites de consulta oberta@Salut i escola
CONSOB04a@Primeres visites de nens i nenes atesos a consulta oberta@Salut i escola
CONSOB04b@Visites successives de nens i nenes atesos en consulta oberta@Salut i escola
CONSOB05@Percentatge de visites en consulta oberta segons el motiu@Salut i escola
CONSOB06@Percentatge de derivació de la infermera escolar a altres professionals@Salut i escola
CONSOB07@Percentatge de plans terapèutics de les visites de Consulta Oberta@Salut i escola