TIR001@Pacients diabètics tipus 2 amb tractament amb dieta sense tires@Tires DM
TIR002@Pacients diabètics tipus 2 amb tractament amb ADO no secretagogs sense tires@Tires DM
TIR003@Pacients diabètics tipus 2 amb tractament amb ADO secretagogs amb menys de 4 tires / setmana@Tires DM
TIR004@Pacients diabètics tipus 2 amb tractament amb insulina lenta amb menys de 8 tires / setmana@Tires DM
TIR005@Pacients diabètics tipus 2 amb tractament amb insulina ràpida amb menys de 15 tires/setmana@Tires DM
TIR006@Pacients diabètics tipus 1 amb menys de 49 tires/setmana@Tires DM
TIR007@Pacients amb diabetis gestacional amb menys de 49 tires/setmana@Tires DM
TIR006P@Pacients DM1 amb menys de 49 tires/setmana (%) (menors de 14 anys)@Tires DM