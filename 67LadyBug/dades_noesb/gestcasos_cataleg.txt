GC0001@Pla de Cures@Gestió de casos
GC0002@Valoració Integral@Gestió de casos
GC0003@Adherència al tractament@Gestió de casos
GC0004@Prevenció de caigudes@Gestió de casos
GC0005@Recomanacions en cas de crisis PCC/MACA@Gestió de casos
GC0006@Pla de decisions anticipades en els malalts MACA@Gestió de casos
GC0007@Valoració estat nutricional (MNA)@Gestió de casos
GC0008@Freqüentació dels pacients en gestió de casos@Gestió de casos
GC0009@Pacients en gestió de casos inclosos en el programa ATDOM@Gestió de casos
