from sisapUtils import multiprocess, getSubTables, getAll, tempFolder, writeCSV, getDataExt, sshExecuteCommand, printTime, latin2utf as utf
import collections
import datetime
import json
from subprocess import Popen


centres = {scs_c: (ics_c, utf(amb_d), utf(sap_d), utf(ics_d)) for (scs_c, ics_c, amb_d, sap_d, ics_d) in getAll('select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc from cat_centres', 'nodrizas')}
serveis = {(sec, cod): utf(des) for (sec, cod, des) in getAll('select codi_sector, s_codi_servei, s_descripcio from cat_pritb103', 'import')}
moduls = {(sec, cen, cla, ser, cod): (utf(des), bool(uab)) for (sec, cen, cla, ser, cod, des, uab) in getAll("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, \
                                                                                                              modu_codi_modul, modu_descripcio, if(modu_codi_uab <> '', 1, 0) from cat_vistb027", 'import')}


folder = '{}\\druid\\visites\\'.format(tempFolder)
folder_unix = '/home/sisap/druid/visites'
spec = {"type": "index_hadoop",
        "spec": {
            "ioConfig": {
                "type": "hadoop",
                "inputSpec": {"type": "static", "paths": "{}/*.txt".format(folder_unix)}},
            "dataSchema": {
                "dataSource": "visites",
                "granularitySpec": {
                    "type": "uniform",
                    "segmentGranularity": "year",
                    "queryGranularity": "day",
                    "intervals": ["2010-01-01/{}-{}-{}".format(getDataExt()[:4], getDataExt()[4:6], getDataExt()[6:8])]},
                "parser": {
                    "type": "string",
                    "parseSpec": {
                        "format": "csv",
                        "columns": ["DIA", "SERVEI", "PROGRAMADES", "REALITZADES", "REALITZADES_QC", "REALITZADES_FILTRE", "EAP_BR", "AMBIT", "SAP", "EAP", "AGENDA", "UAB"],
                        "dimensionsSpec": {"dimensions": ["AMBIT", "SAP", "EAP_BR", "EAP", "SERVEI", "AGENDA", "UAB", "REALITZADES_FILTRE"]},
                        "timestampSpec": {
                            "format": "auto",
                            "column": "DIA"}}},
                "metricsSpec": [
                    {"name": "PROGRAMADES", "type": "longSum", "fieldName": "PROGRAMADES"},
                    {"name": "REALITZADES", "type": "longSum", "fieldName": "REALITZADES"},
                    {"name": "REALITZADES_QC", "type": "longSum", "fieldName": "REALITZADES_QC"},
                    {"name": "DIES_AGENDA", "type": "count"}]},
            "tuningConfig": {
                "type": "hadoop",
                "partitionsSpec": {"type": "hashed", "targetPartitionSize": 5000000000, "assumeGrouped": True},
                "ignoreInvalidRows": True,
                "jobProperties": {}}}}


def get_data(taula):
    programades = collections.Counter()
    realitzades = collections.Counter()
    realitzades_qc = collections.Counter()
    sql = "select codi_sector, date_format(visi_data_visita, '%Y-%m-%d'), visi_up, visi_centre_codi_centre, visi_centre_classe_centre, visi_servei_codi_servei, visi_modul_codi_modul, visi_situacio_visita \
           from {}, nodrizas.dextraccio where visi_data_visita <= data_ext".format(taula)
    for row in getAll(sql, 'import'):
        programades['|'.join(row[:-1])] += 1
        if row[-1] == 'R':
            realitzades['|'.join(row[:-1])] += 1
            realitzades_qc['|'.join(row[:-1])] += 1
        elif row[-1] == 'P':
            realitzades_qc['|'.join(row[:-1])] += 1
    data_file = []
    for id, count in programades.items():
        sector, dia, up, centre, classe, servei, modul = id.split('|')
        fetes = realitzades[id]
        fetes_qc = realitzades_qc[id]
        if up in centres:
            this = ('{}T12:00:00.000Z'.format(dia), serveis[(sector, servei)], count, fetes, fetes_qc,
                    '0-2' if fetes < 3 else '3-4' if fetes < 5 else '5-9' if fetes < 10 else '10-14' if fetes < 15 else '15M') + centres[up] + moduls[(sector, centre, classe, servei, modul)] if (sector, centre, classe, servei, modul) in moduls else 'NO CONEGUT'
            data_file.append(this)
    if data_file:
        writeCSV(folder + taula + '.txt', data_file, sep=',')


if __name__ == '__main__':
    printTime('my to txt')
    t = Popen('rm {}*'.format(folder), shell=False)
    multiprocess(get_data, sorted(getSubTables('visites'), key=getSubTables('visites').get, reverse=True), 12)
    with open(folder + 'spec.json'.format(tempFolder), 'wb') as f:
        json.dump(spec, f)
    printTime('txt to druid')
    sshExecuteCommand('docker-compose exec druid bin/post-index-task --file {}/spec.json'.format(folder_unix), 'druid', 'x0002')
    printTime('the end')
