from sisapUtils import getAll, tempFolder, createTable, listToTable, calcStatistics, sendGeneral, ecapFolder
from gzip import open as open_gz
from datetime import datetime
from os import walk


db = 'redics'
tb = 'sisap_espiros'


files = {'P': None, 'H': None}
for r, d, f in walk(ecapFolder):
    for file_ in f:
        file = r + "/" + file_
        if 'espiros_AP_xml' in file:
            files['P'] = max(files['P'], file)
        if 'espiros_H_pdf' in file:
            files['H'] = max(files['H'], file)

pdp = {}
sql = 'select usua_cip, usua_cip_cod from pdptb101'
for cip, hash in getAll(sql, 'pdp'):
    pdp[cip] = hash

data = []
for domini, file in files.items():
    with open_gz(file, 'rb') as f:
        for row in f.readlines():
            l = row.split(';')
            cip = l[0][:13]
            dat = datetime.strptime(l[1][6:10]+l[1][3:5]+l[1][0:2], '%Y%m%d')
            up = l[8]
            try:
                hash = pdp[cip]
            except KeyError:
                continue
            else:
                data.append([hash, dat, up, domini])

createTable(tb, '(esp_cip varchar2(40), esp_dat date, esp_up varchar2(5), esp_ambit varchar2(1))', db, rm=True)
listToTable(data, tb, db)
calcStatistics(tb, db)
sendGeneral('SISAP <sisap@gencat.cat>', 'ffinaaviles@gencat.cat', 'ecomaredon@gencat.cat', 'Taula copiada a REDICS', 'S\'ha generat la taula {} a REDICS a partir dels txt'.format(tb))
