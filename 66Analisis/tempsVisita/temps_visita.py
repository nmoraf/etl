# coding: utf8

"""
Temps de visita
"""

import collections as c
from datetime import datetime,timedelta

import sisapUtils as u

db = "permanent"

debug = False

cohort = 2019

tb_pac = "Temps_visita_" + str(cohort)

class TempsVisit(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_usuaris()
        self.get_serveis()
        self.get_atdom()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.get_hashos()
        self.get_time()
        self.export_dades()
        
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi \
                from cat_centres", "nodrizas")
        self.centres = {up: br for (up, br) in u.getAll(*sql)}
    
    def get_usuaris(self):
        """."""
        self.usuaris = {}
        sql = "select left(ide_dni, 8), ide_categ_prof_c \
               from cat_pritb992 where ide_categ_prof_c <> ''"
        for usu, categ in u.getAll(sql, "import"):
            if categ in ['10999', '10117', '10116']:
                self.usuaris[usu] = categ
    
    def get_serveis(self):
        """."""
        sql = "select codi_sector, s_codi_servei, s_descripcio, \
                      s_espe_codi_especialitat, s_codi_servei_hom \
               from cat_pritb103"
        self.serveis = {row[:2]: row[3:] for row in u.getAll(sql, "import")}
    
    
    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdoms = {}
        sql = "select id_cip_sec from eqa_problemes where ps = 45"
        for id, in u.getAll(sql, 'nodrizas'):
           self.atdoms[id]= True
    
    def get_pob(self):
        """Agafem poblacio """
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id_cip_sec, codi_sector, up, sexe, edat, nacionalitat, institucionalitzat from assignada_tot"
        for id, sec, up, sexe, edat, nac, insti in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                nac1 = 1 if nac in renta else 0
                self.dades[id] = {
                              'sector': sec, 'up': up, 'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': None, 'insti': insti 
                              }
    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]
    
    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}    
            
    def get_time(self):
        """."""
        proves = {}
        self.upload = []
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select  extract(year from visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat == cohort:
                print table
                sql = "select codi_sector, id_cip_sec, visi_data_upda, visi_data_visita, date_format(visi_data_upda,'%Y%m%d'), date_format(visi_data_visita,'%Y%m%d'), extract(hour from visi_data_upda), \
                visi_up, visi_servei_codi_servei, visi_modul_codi_modul, visi_tipus_visita, visi_lloc_visita, \
                left(visi_dni_prov_resp, 8), left(visi_col_prov_resp, 1) \
                from {}, nodrizas.dextraccio where VISI_SITUACIO_VISITA='R' and visi_data_visita <= data_ext and visi_data_baixa = 0 \
                order by visi_up, visi_modul_codi_modul, visi_data_upda {}".format(table, 'limit 100' if debug else '')
                for sector, id, visihour, dat, char_h, char_d, hour, up, servei, modul, tipus, lloc, dni, col in u.getAll(sql, 'import'):
                    if char_h == char_d:
                        key = char_d + '@' + up + '@' + servei + '@' + modul
                        if key in proves:
                            hour_ant = proves[key]
                            proves[key] = visihour
                            try:
                                temps =  visihour -  hour_ant
                                temps2 = temps.seconds
                            except TypeError:
                                temps2 = 0
                        else:
                            temps2 = 0
                            proves[key] = visihour
                    else:
                        temps2 = 0
                    if dni in self.usuaris and col == '1':
                        espe, s_homol = self.serveis.get((sector, servei), [None, None])
                        nac1 = self.dades[id]['renta'] if id in self.dades else 0
                        atd = 1 if id in self.atdoms else 0
                        cod = self.dades[id]['gma_cod'] if id in self.dades else None
                        cmplx = self.dades[id]['gma_cmplx'] if id in self.dades else None
                        gma_num = self.dades[id]['gma_num'] if id in self.dades else None
                        medea = self.dades[id]['medea'] if id in self.dades else None
                        insti = self.dades[id]['insti'] if id in self.dades else None
                        sexe = self.dades[id]['sex'] if id in self.dades else None
                        edat = self.dades[id]['edat'] if id in self.dades else None
                        
                        if edat > 14:
                            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                            idh = hash + ':' + sec

                            self.upload.append([idh, dat, hour, up, s_homol, modul, tipus, lloc,  sexe, edat, nac1, insti, atd, cod, cmplx, gma_num, medea, temps2])

    def export_dades(self):
        """."""
        columns = ["id varchar(100)", "data_visita date", "hora_visita int", "up varchar(5)", "servei varchar(10)", "modul varchar(10)", 
                    "tipus_visita varchar(10)", "lloc_visita varchar(10)", "sexe varchar(1)",
                    "edat int", "nac1 int",  'institucionalitzat int', 'atdom int', 'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                    "temps_segon int"]
        u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb_pac, db)
    
            
if __name__ == "__main__":
    TempsVisit()
