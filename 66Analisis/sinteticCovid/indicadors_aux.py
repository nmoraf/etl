# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u

db = "redics"
tb = "sisap_covid_ind_sintetic_AUX"

TODAY = d.datetime.now().date()
print TODAY

d1 = TODAY - timedelta(days=3)
print d1


class diagrama_risc(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_cataleg()
        self.get_covid()
        self.get_calculs()
        self.get_indicadors()
        self.export_data()
        self.dona_grants()
        self.export_mail()
        
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg, self.cataleg_abs, self.cataleg_territori = {}, {}, {}
        sql = """select  localitat_c, comarca_c, provincia_c, municipi_c from sisap_covid_cat_localitat"""
        for  loc, com, prov, municipi in u.getAll(sql, db):
            self.cataleg[loc] = {'comarca': com, 'provincia': prov, 'municipi': municipi}
            
            
        sql = """select abs_cod, aga_cod, regio_cod  from sisap_covid_cat_aga"""
        for up, aga, rs in u.getAll(sql, db):
            self.cataleg_abs[up] = {'AGA': aga, 'REGIO': rs}
            
        sql = """select abs_c, sector_c from sisap_covid_cat_territori"""
        for abs, sector in u.getAll(sql, db):
            self.cataleg_territori[abs] = {'SECTOR': sector}
    
    
    def get_covid(self):
        """Agafem dx de covid"""
        self.observats1 = c.Counter()
        u.printTime("agafem covid")
        sql = """SELECT localitat, abs, DATA, grup, residencia, sum(poblacio), sum(CASOS_CONFIRMAT),  
                sum(pcr_no_cas_pos), sum(pcr_no_cas), sum(pcr), sum(ingressats_critic), sum(pneumonia)
                FROM sisap_covid_web_master_setm
                GROUP BY localitat, abs, data, grup, residencia"""
        for loc, abs, data, grup, resi, pob, casos, pcrP, pcrPD, pcr, uci, pneumo in u.getAll(sql, db):
            if abs in self.cataleg_abs:
                self.observats1[('ABS', abs, data, 'UCI')] += uci
                self.observats1[('ABS', abs, data, 'pobT')] += pob
                if grup >0:
                    self.observats1[('ABS', abs, data, 'pobT15')] += pob
                    self.observats1[('ABS', abs, data, 'pneumo')] += pneumo 
                if resi == 0:
                    self.observats1[('ABS', abs, data, 'pob')] += pob
                    self.observats1[('ABS', abs, data, 'casos')] += casos
                    self.observats1[('ABS', abs, data, 'pcrP')] += pcrP
                    self.observats1[('ABS', abs, data, 'pcrPD')] += pcrPD
                    self.observats1[('ABS', abs, data, 'PCR')] += pcr
                    if grup > 1:
                        self.observats1[('ABS', abs, data, 'pob65')] += pob
                        self.observats1[('ABS', abs, data, 'casos65')] += casos
                    if grup > 0:
                        self.observats1[('ABS', abs, data, 'pcrP15')] += pcrP
                        self.observats1[('ABS', abs, data, 'pcrPD15')] += pcrPD
                        self.observats1[('ABS', abs, data, 'PCR15')] += pcr
                        self.observats1[('ABS', abs, data, 'pob15')] += pob
                if resi == 1:
                    self.observats1[('ABS', abs, data, 'pobR')] += pob
                    self.observats1[('ABS', abs, data, 'casosR')] += casos
            
                aga = self.cataleg_abs[abs]['AGA']
                self.observats1[('AGA', aga, data, 'UCI')] += uci
                self.observats1[('AGA', aga, data, 'pobT')] += pob
                if grup >0:
                    self.observats1[('AGA', aga, data, 'pobT15')] += pob
                    self.observats1[('AGA', aga, data, 'pneumo')] += pneumo 
                if resi == 0:
                    self.observats1[('AGA', aga, data, 'pob')] += pob
                    self.observats1[('AGA', aga, data, 'casos')] += casos
                    self.observats1[('AGA', aga, data, 'pcrP')] += pcrP
                    self.observats1[('AGA', aga, data, 'pcrPD')] += pcrPD
                    self.observats1[('AGA', aga, data, 'PCR')] += pcr
                    if grup > 1:
                        self.observats1[('AGA', aga, data, 'pob65')] += pob
                        self.observats1[('AGA', aga, data, 'casos65')] += casos
                    if grup > 0:
                        self.observats1[('AGA', aga, data, 'pcrP15')] += pcrP
                        self.observats1[('AGA', aga, data, 'pcrPD15')] += pcrPD
                        self.observats1[('AGA', aga, data, 'PCR15')] += pcr
                        self.observats1[('AGA', aga, data, 'pob15')] += pob
                if resi == 1:
                    self.observats1[('AGA', aga, data, 'pobR')] += pob
                    self.observats1[('AGA', aga, data, 'casosR')] += casos
            
            if abs in self.cataleg_territori:
                sector = self.cataleg_territori[abs]['SECTOR']
                self.observats1[('SECTOR', sector, data, 'UCI')] += uci
                self.observats1[('SECTOR', sector, data, 'pobT')] += pob
                if grup >0:
                    self.observats1[('SECTOR', sector, data, 'pobT15')] += pob
                    self.observats1[('SECTOR', sector, data, 'pneumo')] += pneumo 
                if resi == 0:
                    self.observats1[('SECTOR', sector, data, 'pob')] += pob
                    self.observats1[('SECTOR', sector, data, 'casos')] += casos
                    self.observats1[('SECTOR', sector, data, 'pcrP')] += pcrP
                    self.observats1[('SECTOR', sector, data, 'pcrPD')] += pcrPD
                    self.observats1[('SECTOR', sector, data, 'PCR')] += pcr
                    if grup > 1:
                        self.observats1[('SECTOR', sector, data, 'pob65')] += pob
                        self.observats1[('SECTOR', sector, data, 'casos65')] += casos
                    if grup > 0:
                        self.observats1[('SECTOR', sector, data, 'pcrP15')] += pcrP
                        self.observats1[('SECTOR', sector, data, 'pcrPD15')] += pcrPD
                        self.observats1[('SECTOR', sector, data, 'PCR15')] += pcr
                        self.observats1[('SECTOR', sector, data, 'pob15')] += pob
                if resi == 1:
                    self.observats1[('SECTOR', sector, data, 'pobR')] += pob
                    self.observats1[('SECTOR', sector, data, 'casosR')] += casos
            
            
            if loc in self.cataleg:
                municipi = self.cataleg[loc]['municipi']
                self.observats1[('MUNICIPI', municipi, data, 'UCI')] += uci
                self.observats1[('MUNICIPI', municipi, data, 'pobT')] += pob
                if grup >0:
                    self.observats1[('MUNICIPI', municipi, data, 'pobT15')] += pob
                    self.observats1[('MUNICIPI', municipi, data, 'pneumo')] += pneumo 
                if resi == 0:
                    self.observats1[('MUNICIPI', municipi, data, 'pob')] += pob
                    self.observats1[('MUNICIPI', municipi, data, 'casos')] += casos
                    self.observats1[('MUNICIPI', municipi, data, 'pcrP')] += pcrP
                    self.observats1[('MUNICIPI', municipi, data, 'pcrPD')] += pcrPD
                    self.observats1[('MUNICIPI', municipi, data, 'PCR')] += pcr
                    if grup > 1:
                        self.observats1[('MUNICIPI', municipi, data, 'pob65')] += pob
                        self.observats1[('MUNICIPI', municipi, data, 'casos65')] += casos
                    if grup > 0:
                        self.observats1[('MUNICIPI', municipi, data, 'pcrP15')] += pcrP
                        self.observats1[('MUNICIPI', municipi, data, 'pcrPD15')] += pcrPD
                        self.observats1[('MUNICIPI', municipi, data, 'PCR15')] += pcr
                        self.observats1[('MUNICIPI', municipi, data, 'pob15')] += pob
                if resi == 1:
                    self.observats1[('MUNICIPI', municipi, data, 'pobR')] += pob
                    self.observats1[('MUNICIPI', municipi, data, 'casosR')] += casos
            
    def get_calculs(self):
        """."""
        u.printTime("Calculaaaaando")
        self.resultats = {}
        for (nivell, aga, data, tip), n in self.observats1.items():
            self.resultats[(nivell, aga, data,'control')] = 1
            if tip == 'pob':
                casos = self.observats1[(nivell, aga, data,'casos')]
                T_casos_pob = float(casos)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_pob')] = T_casos_pob
                pcr = self.observats1[(nivell, aga, data, 'PCR')]
                T_pcr_pob = float(pcr)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_pcr')] = T_pcr_pob
                self.resultats[(nivell, aga, data,'N_casos')] = casos
                self.resultats[(nivell, aga, data,'N_pob')] = n
            if tip == 'pob15':
                pcr = self.observats1[(nivell, aga, data,'PCR15')]
                T_pcr_pob = float(pcr)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_pcr15')] = T_pcr_pob
            if tip == 'pob65':
                casos = self.observats1[(nivell, aga, data, 'casos65')]
                T_casos_pob65 = float(casos)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_pob65')] = T_casos_pob65
            if tip == 'pobR':
                casos = self.observats1[(nivell, aga, data,'casosR')]
                T_casos_pobR = float(casos)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_pobR')] = T_casos_pobR
            if tip == 'pcrPD':
                positius = self.observats1[(nivell, aga, data, 'pcrP')]
                try:
                    positivitat = float(positius)/float(n)*100
                except ZeroDivisionError:
                    positivitat = 0
                self.resultats[(nivell, aga, data,'positivitat')] = positivitat
            if tip == 'pcrPD15':
                positius =self.observats1[(nivell, aga, data, 'pcrP15')]
                try:
                    positivitat15 = float(positius)/float(n)*100
                except ZeroDivisionError:
                    positivitat15 = 0
                self.resultats[(nivell, aga, data,'positivitat15')] = positivitat15
            if tip == 'pobT':
                uci = self.observats1[(nivell, aga, data, 'UCI')]
                T_uci_pobT = float(uci)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_uci')] = T_uci_pobT
            if tip == 'pobT15':
                pneumo = self.observats1[(nivell, aga, data, 'pneumo')]
                T_pneumo_pobT = float(pneumo)/float(n)*100000
                self.resultats[(nivell, aga, data,'Taxa_pneumo')] = T_pneumo_pobT
        
    def get_indicadors(self):
        """."""
        u.printTime("Indicadors")
        self.upload = []
        for (nivell, aga, data, ind), n in self.resultats.items():
            if ind == 'control':
                ind1 = self.resultats[(nivell, aga, data, 'Taxa_pob')] if (nivell, aga, data, 'Taxa_pob') in self.resultats else 0
                ind2 = self.resultats[(nivell, aga, data, 'Taxa_pob65')] if (nivell, aga, data, 'Taxa_pob65') in self.resultats else 0
                ind3 = self.resultats[(nivell, aga, data, 'Taxa_pobR')] if (nivell, aga, data, 'Taxa_pobR') in self.resultats else 0
                ind7 = self.resultats[(nivell, aga, data, 'positivitat')] if (nivell, aga, data, 'positivitat') in self.resultats else 0
                ind8 = self.resultats[(nivell, aga, data, 'positivitat15')] if (nivell, aga, data, 'positivitat15') in self.resultats else 0
                ind9 = self.resultats[(nivell, aga, data, 'Taxa_pcr')] if (nivell, aga, data, 'Taxa_pcr') in self.resultats else 0
                ind10 = self.resultats[(nivell, aga, data, 'Taxa_pcr15')] if (nivell, aga, data, 'Taxa_pcr15') in self.resultats else 0
                ind12 = self.resultats[(nivell, aga, data, 'Taxa_uci')] if (nivell, aga, data, 'Taxa_uci') in self.resultats else 0
                ind13 = self.resultats[(nivell, aga, data, 'Taxa_pneumo')] if (nivell, aga, data, 'Taxa_pneumo') in self.resultats else 0
                casos = self.resultats[(nivell, aga, data, 'N_casos')] if (nivell, aga, data, 'N_casos') in self.resultats else 0
                pob = self.resultats[(nivell, aga, data, 'N_pob')] if (nivell, aga, data, 'N_pob') in self.resultats else 0
                
                d7 = data - timedelta(days=7)
                ant1 = self.resultats[(nivell, aga, d7, 'Taxa_pob')] if (nivell, aga, d7, 'Taxa_pob') in self.resultats else 0
                ant2 = self.resultats[(nivell, aga, d7, 'Taxa_pob65')] if (nivell, aga, d7, 'Taxa_pob65') in self.resultats else 0
                ant3 = self.resultats[(nivell, aga, d7, 'Taxa_pobR')] if (nivell, aga, d7, 'Taxa_pobR') in self.resultats else 0
                ant7 = self.resultats[(nivell, aga, d7, 'positivitat')] if (nivell, aga, d7, 'positivitat') in self.resultats else 0
                ant8 = self.resultats[(nivell, aga, d7, 'positivitat15')] if (nivell, aga, d7, 'positivitat15') in self.resultats else 0
                ant9 = self.resultats[(nivell, aga, d7, 'Taxa_pcr')] if (nivell, aga, d7, 'Taxa_pcr') in self.resultats else 0
                ant10 = self.resultats[(nivell, aga, d7, 'Taxa_pcr15')] if (nivell, aga, d7, 'Taxa_pcr15') in self.resultats else 0
                ant12 = self.resultats[(nivell, aga, d7, 'Taxa_uci')] if (nivell, aga, d7, 'Taxa_uci') in self.resultats else 0
                ant13 = self.resultats[(nivell, aga, d7, 'Taxa_pneumo')] if (nivell, aga, d7, 'Taxa_pneumo') in self.resultats else 0
                
                try:
                    var1 = ((float(ind1) - float(ant1))/float(ant1))*100
                except ZeroDivisionError:
                    var1 = 0
                try:
                    var2 = ((float(ind2) - float(ant2))/float(ant2))*100
                except ZeroDivisionError:
                    var2 = 0
                try:
                    var3 = ((float(ind3) - float(ant3))/float(ant3))*100
                except ZeroDivisionError:
                    var3 = 0
                try:
                    var7 = ((float(ind7) - float(ant7))/float(ant7))*100
                except ZeroDivisionError:
                    var7 = 0
                try:
                    var8 = ((float(ind8) - float(ant8))/float(ant8))*100
                except ZeroDivisionError:
                    var8 = 0
                try:    
                    var9 = ((float(ind9) - float(ant9))/float(ant9))*100
                except ZeroDivisionError:
                    var9 = 0
                try:    
                    var10 = ((float(ind10) - float(ant10))/float(ant10))*100
                except ZeroDivisionError:
                    var10 = 0
                try:
                    var12 = ((float(ind12) - float(ant12))/float(ant12))*100
                except ZeroDivisionError:
                    var12 = 0
                try:
                    var13 = ((float(ind13) - float(ant13))/float(ant13))*100
                except ZeroDivisionError:
                    var13 = 0
                self.upload.append([nivell, str(aga), data, casos, pob,ind1, ind2, ind3, ind7, ind8, ind9, ind10, ind12, ind13, var1, var2, var3, var7, var8, var9, var10, var12, var13])

        
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("nivell varchar2(100)", "codi varchar2(30)","data date",  "N_casos int", "N_pob int",
                "T_casos number(20, 13)", "T_casos_M65 number(20, 13)", "T_casos_resis number(20, 13)",
                "P_positivitat number(20, 13)", "P_positivitat_M15 number(20, 13)", "T_pcr number(20, 13)", "T_pcr_M15 number(20, 13)",
                "T_uci number(20, 13)", "T_pneumo_M15 number(20, 13)",
                "V_casos number(20, 13)", "V_casos_M65 number(20, 13)", "V_casos_resis number(20, 13)",
                "V_positivitat number(20, 13)", "V_positivitat_M15 number(20, 13)", "V_pcr number(20, 13)", "V_pcr_M15 number(20, 13)",
                "V_uci number(20, 13)", "V_pneumo_M15 number(20, 13)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        u.printTime("grants")
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
            
    def export_mail(self):
        """Enviem fitxer"""
        u.printTime("mail")
        upload = []
        upload.append(['codi','up_des','sector_des','n_casos','taxa_casos','positivitat_m15','var_casos_perc','taxa_casos_resi'])
        sql = """ select codi,up_des,sector_des,
                      n_casos,
                      round(t_casos,1)as taxa_casos,
                      round(p_positivitat_m15,1) as positivitat_m15,
                       round(v_casos,1) as Var_casos_perc,
                       round(t_casos_resis,1) as taxa_resis
                 from sisap_covid_ind_sintetic_aux a
                      join
                      SISAP_COVID_CAT_abs b
                      on a.codi=b.abs_cod
                      join
                      SISAP_COVID_CAT_up c
                      on b.eap=c.up_cod
                where to_char(data, 'DD-MM-YYYY')= '{}' and nivell='ABS'  order by t_casos desc""".format(d1.strftime("%d-%m-%Y"))
        for codi, up, des, n, tc,pos, var, resi in u.getAll(sql, 'redics'):
            upload.append([codi, up, des, n, str(tc).replace('.', ','),str(pos).replace('.', ','), str(var).replace('.', ','), str(resi).replace('.', ',')])
        file = u.tempFolder + "abs_covid.csv"
        u.writeCSV(file, upload, sep=';')
        text= "Us fem arribar les dades per abs."
        u.sendPolite('ecomaredon@gencat.cat','ABS covid',text,file)
        
        upload = []
        upload.append(['codi','municipi','comarca','n_pob', 'n_casos','taxa_casos','positivitat_m15','var_casos_perc','taxa_casos_resi'])
        sql = """select codi,municipi, comarca,
                    avg(n_pob),
                    avg(n_casos),
                    avg(round(t_casos,1)) as taxa_casos,
                    avg(round(p_positivitat_m15,1)) as positivitat_m15,
                    avg(round(v_casos,1)) as Var_casos_perc,
                    avg(round(t_casos_resis,1)) as taxa_resis
              from sisap_covid_ind_sintetic_aux a
              join
              SISAP_COVID_CAT_localitat b  
              on  codi=municipi_c 
            where to_char(data, 'DD-MM-YYYY')= '{}' and nivell='MUNICIPI' group by codi,municipi, comarca""".format(d1.strftime("%d-%m-%Y"))
        for codi, municipi, comarca, pob, n, tc,pos, var, resi in u.getAll(sql, 'redics'):
            upload.append([codi, municipi, comarca, pob, n, str(tc).replace('.', ','),str(pos).replace('.', ','), str(var).replace('.', ','), str(resi).replace('.', ',')])
        file = u.tempFolder + "municipi_covid.csv"
        u.writeCSV(file, upload, sep=';')
        text= "Us fem arribar les dades per municipi."
        u.sendPolite('ecomaredon@gencat.cat','Municipi covid',text,file)

if __name__ == '__main__':
    u.printTime("Inici")

    diagrama_risc()
    