# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import csv,os,sys
from datetime import datetime, timedelta

import sisapUtils as u

db = 'redics'

edats = [0, 1, 2, 3]
tipus_g = ['casos', 'veloc']

tb = 'sisap_covid_agr_ili_alertes'

class ILI_estimacio(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_estimacio()
        self.get_cataleg()
        self.get_alertes()
        self.export_data()
        self.dona_grants()
        
        
    def get_estimacio(self):
        """Agafo dades estimacio"""
        u.printTime("agafem estimacions")
        self.estimacions = {}
        sql = """select nivell, codi, data, edat, forecast,  hi80,   hi95 from sisap_covid_agr_ili_estimacio where tipus='veloc'"""
        for  territori, codi, data, edat, forecast,  hi80,  hi95 in u.getAll(sql, db):
            self.estimacions[(territori, codi, data, edat)]= {'tc': forecast, 'hi80': hi80, 'hi95': hi95}
    
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg = {}
        sql = """select up, comarca_c, provincia_c, regio_c, sector_c, localitat_c from sisap_covid_cat_territori"""
        for up, com, prov, rs, sec, loc in u.getAll(sql, db):
            self.cataleg[up] = {'comarca': com, 'provincia': prov, 'regio': rs, 'sap': None, 'sec': sec, 'amb': None, 'aga': None, 'localitat': loc}
            
        sql = """select up_cod, sap_cod, amb_cod from sisap_covid_cat_up"""
        for up, sap, amb in u.getAll(sql, db):
            if up in self.cataleg:
                self.cataleg[up]['sap'] = sap
                self.cataleg[up]['amb'] = amb
                
        sql = """select up, aga_cod  from sisap_covid_cat_aga, sisap_covid_cat_territori where abs_cod=abs_c"""
        for up, aga in u.getAll(sql, db):
            if up in self.cataleg:
                self.cataleg[up]['aga'] = str(aga)
    
    
    def get_alertes(self):
        """Agafem observats i calculem alertes"""
        observats1 = c.Counter()
        observats7 = c.Counter()
        u.printTime("agafem alertes")
        sql = """select up, data, grup, sum(dx_ili) + sum(dx_covid) from sisap_covid_web_master where residencia=0 group by up, data, grup"""
        for up, data, edat, n in u.getAll(sql, db):
            observats1[('GLOBAL','GLOBAL', data, edat)] += n
            edat = 3
            observats1[('GLOBAL','GLOBAL', data, edat)] += n
            if up in self.cataleg:
                regio = self.cataleg[up]['regio']
                comarca = self.cataleg[up]['comarca']
                provincia = self.cataleg[up]['provincia']
                sap = self.cataleg[up]['sap']
                sec = self.cataleg[up]['sec']
                amb = self.cataleg[up]['amb']
                aga = self.cataleg[up]['aga']
                localitat = self.cataleg[up]['localitat']
                observats1[('REGIO', regio, data, edat)] += n
                observats1[('COMARCA', comarca, data, edat)] += n
                observats1[('PROVINCIA', provincia, data, edat)] += n
                observats1[('SAP', sap, data, edat)] += n
                observats1[('AMBIT', amb, data, edat)] += n
                observats1[('SECTOR', sec, data, edat)] += n
                observats1[('AGA', aga, data, edat)] += n
                observats1[('LOCALITAT', localitat, data, edat)] += n
                
        for (territori, codi, data, edat), casos in observats1.items():
            d1 = data - timedelta(days=6)
            observats7[(territori, codi, data, edat)] += casos
            for single_date in u.dateRange(d1, data):
                casos_ant = observats1[(territori, codi, single_date, edat)]
                observats7[(territori, codi, data, edat)] += casos_ant
                
            
        pre_alertes = {}      
        for (territori, codi, data, edat), casos in observats7.items():
            if (territori, codi, data, edat) in self.estimacions:
                 dat_ant = data - timedelta(days=1)
                 point_ant = observats7[(territori, codi, dat_ant, edat)]
                 try:
                    vel = 100*((float(casos) - float(point_ant))/float(point_ant))
                 except ZeroDivisionError:
                    vel = None
                 tcentral = self.estimacions[(territori, codi, data, edat)]['tc']
                 hi80 = self.estimacions[(territori, codi, data, edat)]['hi80']
                 hi95 = self.estimacions[(territori, codi, data, edat)]['hi95']
                 sobre_tc, sobre_hi80,sobre_hi95, sobre_5 = 0,0,0,0
                 if vel != None:
                     sobre_tc = 1 if vel >= tcentral else 0
                     sobre_hi80 = 1 if vel >= hi80 or vel==hi80 else 0
                     sobre_hi95 = 1 if vel >= hi95 else 0
                     sobre_5 = 1 if vel >= 5 else 0
                 pre_alertes[(territori, codi, data, edat)] = {'vel': vel, 'tc': sobre_tc, 'hi80': sobre_hi80, 'hi95': sobre_hi95, '5':sobre_5, 'observat': casos}
        
        self.upload = []
        for (territori, codi, data, edat), dades in pre_alertes.items():
            hi95, hi80, a5, tc = dades['hi95'], dades['hi80'], dades['5'], dades['tc']
            alerta95, alerta80, alerta5, alertatc = 0, 0, 0, 0
            if hi95 == 1:
                d1 = data - timedelta(days=1)
                if (territori, codi, d1, edat) in pre_alertes:
                    pre_hi95 = pre_alertes[(territori, codi, d1, edat)]['hi95']
                    alerta95 = 1 if pre_hi95 == 1 else 0
            if hi80 == 1:
                na = 1
                d1 = data - timedelta(days=2)
                for single_date in u.dateRange(d1, data):
                    if (territori, codi, single_date, edat) in pre_alertes:
                        pre_hi80 = pre_alertes[(territori, codi, single_date, edat)]['hi80']
                        na += pre_hi80
                alerta80 = 1 if na == 3 else 0
            if a5 == 1:
                na = 1
                d1 = data - timedelta(days=2)
                for single_date in u.dateRange(d1, data):
                    if (territori, codi, single_date, edat) in pre_alertes:
                        pre_5 = pre_alertes[(territori, codi, single_date, edat)]['5']
                        na += pre_5
                alerta5 = 1 if na == 3 else 0
            if tc == 1:
                na = 1
                d1 = data - timedelta(days=6)
                for single_date in u.dateRange(d1, data):
                    if (territori, codi, single_date, edat) in pre_alertes:
                        pre_tc = pre_alertes[(territori, codi, single_date, edat)]['tc']
                        na += pre_tc
                alertatc = 1 if na == 7 else 0
            if data > d.datetime.strptime('2020-03-07', '%Y-%m-%d'): 
                self.upload.append([territori, codi, data, edat, dades['vel'], dades['observat'], alerta95, alerta80, alerta5, alertatc])
             
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("nivell varchar2(100)", "codi varchar2(20)", "data date", "edat int", "velocitat number(20, 13)", "casos number(20, 13)",
                "alerta95 int", "alerta80  int", "alerta5  int", "alertatc int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)

if __name__ == '__main__':
    u.printTime("Inici")
    
    ILI_estimacio()
    
    u.printTime("Final")