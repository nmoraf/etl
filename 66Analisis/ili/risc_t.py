# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u

db = "redics"
tb = "sisap_covid_agr_risc_ili_t"

tb_a = 'sisap_covid_agr_ili_alertes'



class diagrama_risc(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_cataleg()
        self.get_covid()
        self.get_ilis()
        self.get_calculs()
        self.export_data()
        self.dona_grants()
        
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg, self.cataleg_abs = {}, {}
        sql = """select  localitat_c, comarca_c, provincia_c, municipi_c from sisap_covid_cat_localitat"""
        for  loc, com, prov, municipi in u.getAll(sql, db):
            self.cataleg[loc] = {'comarca': com, 'provincia': prov, 'municipi': municipi}
            
            
        sql = """select abs_cod, aga_cod, regio_cod  from sisap_covid_cat_aga"""
        for up, aga, rs in u.getAll(sql, db):
            self.cataleg_abs[up] = {'AGA': aga, 'REGIO': rs}
    
    
    def get_covid(self):
        """Agafem dx de covid"""
        self.observats1 = c.Counter()
        u.printTime("agafem covid")
        sql = """SELECT localitat, abs, DATA, sum(poblacio), sum(CASOS_CONFIRMAT),  sum(CASOS_CONFIRMAT) + sum(CASOS_DIAGNOSTIC), sum(DX_covid), sum(CASOS_CONFIRMAT_ONLY_TAR_SIMPT)
                FROM preduffa.sisap_covid_web_master
                WHERE  residencia = 0
                GROUP BY localitat, abs, data"""
        for up, abs, data, poblacio, confirmats, totals, dx, ili1 in u.getAll(sql, db):
            self.observats1[('GLOBAL','GLOBAL', data, 'pob')] += poblacio
            self.observats1[('GLOBAL','GLOBAL', data, 'confirmats')] += confirmats
            self.observats1[('GLOBAL','GLOBAL', data, 'dx')] += dx
            self.observats1[('GLOBAL','GLOBAL', data, 'totals')] += totals
            ili = ili1
            self.observats1[('GLOBAL','GLOBAL', data, 'ili')] += ili
            self.observats1[('LOCALITAT', up, data, 'pob')] += poblacio
            self.observats1[('LOCALITAT', up, data, 'confirmats')] += confirmats
            self.observats1[('LOCALITAT', up, data, 'dx')] += dx
            self.observats1[('LOCALITAT', up, data, 'ili')] += ili
            self.observats1[('LOCALITAT', up, data, 'totals')] += totals
            self.observats1[('ABS', abs, data, 'pob')] += poblacio
            self.observats1[('ABS', abs, data, 'confirmats')] += confirmats
            self.observats1[('ABS', abs, data, 'dx')] += dx
            self.observats1[('ABS', abs, data, 'ili')] += ili
            self.observats1[('ABS', abs, data, 'totals')] += totals
            if up in self.cataleg:
                comarca = self.cataleg[up]['comarca']
                provincia = self.cataleg[up]['provincia']
                municipi = self.cataleg[up]['municipi']
                self.observats1[('COMARCA', comarca, data, 'pob')] += poblacio
                self.observats1[('PROVINCIA', provincia, data, 'pob')] += poblacio
                self.observats1[('MUNICIPI', municipi, data, 'pob')] += poblacio
                
                self.observats1[('COMARCA', comarca, data, 'confirmats')] += confirmats
                self.observats1[('PROVINCIA', provincia, data, 'confirmats')] += confirmats
                self.observats1[('MUNICIPI', municipi, data, 'confirmats')] += confirmats
                
                self.observats1[('COMARCA', comarca, data, 'dx')] += dx
                self.observats1[('PROVINCIA', provincia, data, 'dx')] += dx
                self.observats1[('MUNICIPI', municipi, data, 'dx')] += dx
                
                self.observats1[('COMARCA', comarca, data, 'ili')] += ili
                self.observats1[('PROVINCIA', provincia, data, 'ili')] += ili
                self.observats1[('MUNICIPI', municipi, data, 'ili')] += ili

                self.observats1[('COMARCA', comarca, data, 'totals')] += totals
                self.observats1[('PROVINCIA', provincia, data, 'totals')] += totals
                self.observats1[('MUNICIPI', municipi, data, 'totals')] += totals
                
            if abs in self.cataleg_abs:
                aga = self.cataleg_abs[abs]['AGA']
                regio = self.cataleg_abs[abs]['REGIO']
                self.observats1[('AGA', aga, data, 'pob')] += poblacio
                self.observats1[('REGIO', regio, data, 'pob')] += poblacio
                
                self.observats1[('AGA', aga, data, 'confirmats')] += confirmats
                self.observats1[('REGIO', regio, data, 'confirmats')] += confirmats
                
                self.observats1[('AGA', aga, data, 'dx')] += dx
                self.observats1[('REGIO', regio, data, 'dx')] += dx
                
                self.observats1[('AGA', aga, data, 'ili')] += ili
                self.observats1[('REGIO', regio, data, 'ili')] += ili

                self.observats1[('AGA', aga, data, 'totals')] += totals
                self.observats1[('REGIO', regio, data, 'totals')] += totals

    
    def get_ilis(self):
        """ILI des de taula brots"""
        self.ilis = {}
        for  (nivell, codi, data, tip), n in self.observats1.items():
            if tip == 'pob':
                confirmats = self.observats1[(nivell, codi, data, 'confirmats')]
                dx =  self.observats1[(nivell, codi, data, 'dx')]
                ili =  self.observats1[(nivell, codi, data, 'ili')]
                totals =  self.observats1[(nivell, codi, data, 'totals')]
                self.ilis[(data, nivell, codi)] = {'pob': n, 'confirmats': confirmats, 'r0': None, 'IA14': None, 'dx': dx, 'r0_dx': None, 'IA14_dx': None,
                                                    'ili': ili, 'r0_ili': None, 'IA14_ili': None,
                                                    'totals': totals, 'r0_totals': None, 'IA14_totals': None}

    def get_calculs(self):
        """."""
        for (data, nivell, codi), n in self.ilis.items():
            d1 = data - timedelta(days=1)
            d2 = data - timedelta(days=2)
            d5 = data - timedelta(days=5)
            d6 = data - timedelta(days=6)
            d7 = data - timedelta(days=7)
            pob = self.ilis[(data, nivell, codi)]['pob']
            ili1 = self.ilis[(d1, nivell, codi)]['confirmats'] if (d1, nivell, codi) in self.ilis else 0
            ili2 = self.ilis[(d2, nivell, codi)]['confirmats'] if (d2, nivell, codi) in self.ilis else 0
            ili5 = self.ilis[(d5, nivell, codi)]['confirmats'] if (d5, nivell, codi) in self.ilis else 0
            ili6 = self.ilis[(d6, nivell, codi)]['confirmats'] if (d6, nivell, codi) in self.ilis else 0
            ili7 = self.ilis[(d7, nivell, codi)]['confirmats'] if (d7, nivell, codi) in self.ilis else 0
            ili0 = n['confirmats']
            try: 
                r0 = float((ili0 + ili1 + ili2))/float((ili5 + ili6 + ili7))
            except ZeroDivisionError:
                r0 = float((ili0 + ili1 + ili2))/1
            if r0 > 5:
                r0 = 5
            self.ilis[(data, nivell, codi)]['r0'] = r0
            ia14 = ili0
            d14 = data - timedelta(days=13)
            for single_date in u.dateRange(d14, data):
                try:
                    iliN = self.ilis[(single_date, nivell, codi)]['confirmats']
                except KeyError:
                    iliN = 0
                ia14 += iliN
            IA = float(ia14)/float(pob)*100000
            self.ilis[(data, nivell, codi)]['IA14'] = IA
            
            ili1 = self.ilis[(d1, nivell, codi)]['dx'] if (d1, nivell, codi) in self.ilis else 0
            ili2 = self.ilis[(d2, nivell, codi)]['dx'] if (d2, nivell, codi) in self.ilis else 0
            ili5 = self.ilis[(d5, nivell, codi)]['dx'] if (d5, nivell, codi) in self.ilis else 0
            ili6 = self.ilis[(d6, nivell, codi)]['dx'] if (d6, nivell, codi) in self.ilis else 0
            ili7 = self.ilis[(d7, nivell, codi)]['dx'] if (d7, nivell, codi) in self.ilis else 0
            ili0 = n['dx']
            try: 
                r0 = float((ili0 + ili1 + ili2))/float((ili5 + ili6 + ili7))
            except ZeroDivisionError:
                r0 = float((ili0 + ili1 + ili2))/1
            if r0 > 5:
                r0 = 5
            self.ilis[(data, nivell, codi)]['r0_dx'] = r0
            ia14 = ili0
            d14 = data - timedelta(days=13)
            for single_date in u.dateRange(d14, data):
                try:
                    iliN = self.ilis[(single_date, nivell, codi)]['dx']
                except KeyError:
                    iliN = 0
                ia14 += iliN
            IA = float(ia14)/float(pob)*100000
            self.ilis[(data, nivell, codi)]['IA14_dx'] = IA
            
            ili1 = self.ilis[(d1, nivell, codi)]['ili'] if (d1, nivell, codi) in self.ilis else 0
            ili2 = self.ilis[(d2, nivell, codi)]['ili'] if (d2, nivell, codi) in self.ilis else 0
            ili5 = self.ilis[(d5, nivell, codi)]['ili'] if (d5, nivell, codi) in self.ilis else 0
            ili6 = self.ilis[(d6, nivell, codi)]['ili'] if (d6, nivell, codi) in self.ilis else 0
            ili7 = self.ilis[(d7, nivell, codi)]['ili'] if (d7, nivell, codi) in self.ilis else 0
            ili0 = n['ili']
            try: 
                r0 = float((ili0 + ili1 + ili2))/float((ili5 + ili6 + ili7))
            except ZeroDivisionError:
                r0 = float((ili0 + ili1 + ili2))/1
            if r0 > 5:
                r0 = 5
            self.ilis[(data, nivell, codi)]['r0_ili'] = r0
            ia14 = ili0
            d14 = data - timedelta(days=13)
            for single_date in u.dateRange(d14, data):
                try:
                    iliN = self.ilis[(single_date, nivell, codi)]['ili']
                except KeyError:
                    iliN = 0
                ia14 += iliN
            IA = float(ia14)/float(pob)*100000
            self.ilis[(data, nivell, codi)]['IA14_ili'] = IA
            
            ili1 = self.ilis[(d1, nivell, codi)]['totals'] if (d1, nivell, codi) in self.ilis else 0
            ili2 = self.ilis[(d2, nivell, codi)]['totals'] if (d2, nivell, codi) in self.ilis else 0
            ili5 = self.ilis[(d5, nivell, codi)]['totals'] if (d5, nivell, codi) in self.ilis else 0
            ili6 = self.ilis[(d6, nivell, codi)]['totals'] if (d6, nivell, codi) in self.ilis else 0
            ili7 = self.ilis[(d7, nivell, codi)]['totals'] if (d7, nivell, codi) in self.ilis else 0
            ili0 = n['totals']
            try: 
                r0 = float((ili0 + ili1 + ili2))/float((ili5 + ili6 + ili7))
            except ZeroDivisionError:
                r0 = float((ili0 + ili1 + ili2))/1
            if r0 > 5:
                r0 = 5
            self.ilis[(data, nivell, codi)]['r0_totals'] = r0
            ia14 = ili0
            d14 = data - timedelta(days=13)
            for single_date in u.dateRange(d14, data):
                try:
                    iliN = self.ilis[(single_date, nivell, codi)]['totals']
                except KeyError:
                    iliN = 0
                ia14 += iliN
            IA = float(ia14)/float(pob)*100000
            self.ilis[(data, nivell, codi)]['IA14_totals'] = IA
          
        
        self.upload = []  
        for (data, nivell, codi), n in self.ilis.items():
            d7 = data - timedelta(days=6)
            r7 =  self.ilis[(data, nivell, codi)]['r0']
            for single_date in u.dateRange(d7, data):
                try:
                    r0 = self.ilis[(single_date, nivell, codi)]['r0']
                except KeyError:
                    r0 = 0
                r7 += r0
            r0m = float(r7)/float(7)
            ia = self.ilis[(data, nivell, codi)]['IA14']
            EPG = float(r0m) * float(ia)
            
            r7 =  self.ilis[(data, nivell, codi)]['r0_dx']
            for single_date in u.dateRange(d7, data):
                try:
                    r0 = self.ilis[(single_date, nivell, codi)]['r0_dx']
                except KeyError:
                    r0 = 0
                r7 += r0
            r0m_dx = float(r7)/float(7)
            ia_dx = self.ilis[(data, nivell, codi)]['IA14_dx']
            EPG_dx = float(r0m_dx) * float(ia_dx)
            
            r7 =  self.ilis[(data, nivell, codi)]['r0_ili']
            for single_date in u.dateRange(d7, data):
                try:
                    r0 = self.ilis[(single_date, nivell, codi)]['r0_ili']
                except KeyError:
                    r0 = 0
                r7 += r0
            r0m_ili = float(r7)/float(7)
            ia_ili = self.ilis[(data, nivell, codi)]['IA14_ili']
            EPG_ili = float(r0m_ili) * float(ia_ili)
            
            r7 =  self.ilis[(data, nivell, codi)]['r0_totals']
            for single_date in u.dateRange(d7, data):
                try:
                    r0 = self.ilis[(single_date, nivell, codi)]['r0_totals']
                except KeyError:
                    r0 = 0
                r7 += r0
            r0m_totals = float(r7)/float(7)
            ia_totals = self.ilis[(data, nivell, codi)]['IA14_totals']
            EPG_totals = float(r0m_totals) * float(ia_totals)
            if data > d.datetime.strptime('2020-03-13', '%Y-%m-%d'): 
                self.upload.append([data, nivell, str(codi), n['pob'],  n['confirmats'], n['r0'], r0m, n['IA14'], EPG, n['ili'],n['r0_ili'], r0m_ili, n['IA14_ili'], EPG_ili])
        
    
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("data date", "nivell varchar2 (100)", "codi varchar2(30)",
                "poblacio int", "confirmat int",  "r0_confirmat number(20, 13)", "r0_confirmat_M number(20, 13)", "IA14_confirmat number(20, 13)", "iEPG_confirmat  number(20, 13)",
                 "TAR_SIMPT int",  "r0_TAR_SIMPT number(20, 13)", "r0_TAR_SIMPT_M number(20, 13)", "IA14_TAR_SIMPT number(20, 13)", "iEPG_TAR_SIMPT  number(20, 13)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)

if __name__ == '__main__':
    u.printTime("Inici")
    try:    
        start = 1
        while start == 1:
            status,=u.getOne("select extraccio_status from sisap_covid_web_timings", db)
            if status == 0: 
                u.execute("update sisap_covid_web_timings set calcul_inici=current_timestamp", db)
                diagrama_risc()
                u.execute("update sisap_covid_web_timings set calcul_status=0",db)
                u.execute("update sisap_covid_web_timings set calcul_fi=current_timestamp",db)
                start = 0
            else:
                sleep(10)
    except:
        u.execute("update sisap_covid_web_timings set calcul_status=1",db)
        
    
    u.printTime("Final")