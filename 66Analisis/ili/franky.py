# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import date, timedelta

import sisapUtils as u

db = "redics"
tb = "sisap_covid_agr_brots_informe"

codis_aga = ['70','46','71','47']



class frankesntein(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_cataleg()
        self.get_covid()
        self.get_risc()
        self.get_franky()
        self.export_data()
        self.dona_grants()
        
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg, self.cataleg_abs = {}, {}
        sql = """select localitat_c, localitat, comarca, comarca_c from sisap_covid_cat_localitat"""
        for  loc, desc, com, comc in u.getAll(sql, db):
            self.cataleg[loc] = {'comarca': com, 'comarca_c': comc, 'desc': desc}
            
        sql = """select abs_cod, aga_cod, aga_des  from sisap_covid_cat_aga where regio_cod='78'"""
        for up, aga, aga_des in u.getAll(sql, db):
            self.cataleg_abs[up] = {'AGA': str(aga), 'desc': aga_des}
    
    
    def get_covid(self):
        """Agafem dx de covid"""
        self.recomptes = c.Counter()
        u.printTime("agafem covid")
        sql ="""select max(data) from sisap_covid_web_master"""
        for d, in u.getAll(sql, db):
            max_data = d
        self.avui = max_data
        self.fa1 = max_data - timedelta(days=1)
        self.fa7 = max_data - timedelta(days=7)
        self.fa3 = max_data - timedelta(days=3)
        self.fa4 = max_data - timedelta(days=4)
        self.fa10 = max_data - timedelta(days=10)
        sql = """SELECT localitat, abs, DATA, sum(poblacio), sum(CASOS_CONFIRMAT),  sum(pcr), sum(edat), sum(dx_ili) + sum(dx_covid)
                FROM preduffa.sisap_covid_web_master
                WHERE  residencia = 0
                GROUP BY localitat, abs, data"""
        for local, abs, data, pob, casos, pcr, edat, ili in u.getAll(sql, db):
            if local in self.cataleg:
                localitat = self.cataleg[local]['desc'] 
                self.recomptes[('LOCALITAT', local, localitat, data, 'pob')] += pob
                self.recomptes[('LOCALITAT', local, localitat, data, 'casos')] += casos
                self.recomptes[('LOCALITAT', local, localitat, data, 'pcr')] += pcr
                self.recomptes[('LOCALITAT', local, localitat, data, 'edat')] += edat
                self.recomptes[('LOCALITAT', local, localitat, data, 'ili')] += ili
                comarca = self.cataleg[local]['comarca']
                comarca_c = self.cataleg[local]['comarca_c']
                self.recomptes[('COMARCA', comarca_c, comarca, data, 'pob')] += pob
                self.recomptes[('COMARCA', comarca_c, comarca, data, 'casos')] += casos
                self.recomptes[('COMARCA', comarca_c, comarca, data, 'pcr')] += pcr
                self.recomptes[('COMARCA', comarca_c, comarca, data, 'edat')] += edat
                self.recomptes[('COMARCA', comarca_c, comarca, data, 'ili')] += ili
            if abs in self.cataleg_abs:
                aga = self.cataleg_abs[abs]['AGA']
                aga_des = self.cataleg_abs[abs]['desc']
                self.recomptes[('AGA', aga, aga_des, data, 'pob')] += pob
                self.recomptes[('AGA', aga, aga_des, data, 'casos')] += casos
                self.recomptes[('AGA', aga, aga_des, data, 'pcr')] += pcr
                self.recomptes[('AGA', aga, aga_des, data, 'edat')] += edat
                self.recomptes[('AGA', aga, aga_des, data, 'ili')] += ili
               
    def get_risc(self):
        """risc rebrot"""
        self.riscrebrot = {}
        sql =  """select data, nivell, codi, IEPG_confirmat, IA14_confirmat, R0_CONFIRMAT_M from sisap_covid_agr_risc_ili"""
        for data, nivell, codi, iepg, ia, RT in u.getAll(sql, db):
            self.riscrebrot[(nivell, codi, data)] = {'iepg': iepg, 'ia': ia, 'rt': RT}
    
    
    def get_franky(self):
        """calculs"""
        self.upload = []
        pre_up = {}
        self.les_ili = {}
        els_riscos = {}
        for (nivell, codi, desc,  data, tip), n in self.recomptes.items():
            if tip == 'pob':
                casos, pcr, ili = self.recomptes[(nivell, codi, desc, data, 'casos')], self.recomptes[(nivell, codi,desc,  data, 'pcr')],self.recomptes[(nivell, codi, desc, data, 'ili')]
                edat = self.recomptes[(nivell, codi, desc, data, 'edat')]
                d6 = data - timedelta(days=6)
                for single_date in u.dateRange(d6, data):
                    try:
                        casos_a, pcr_a, ili_a = self.recomptes[(nivell, codi, desc, single_date, 'casos')],self.recomptes[(nivell, codi, desc, single_date, 'pcr')],self.recomptes[(nivell, codi,desc,  single_date, 'ili')]     
                        edat_a =  self.recomptes[(nivell, codi, desc, single_date, 'edat')]
                    except KeyError:
                        iliN = 0
                    casos += casos_a
                    pcr += pcr_a
                    ili += ili_a
                    edat += edat_a
                try:
                    edat_m = float(edat)/float(casos)
                except ZeroDivisionError:
                    edat_m = 0
                tcasos = (float(casos)/float(n))*100000
                tpcr = (float(pcr)/float(n))*100000
                tili = (float(ili)/float(n))*100000
                self.les_ili[(nivell, codi, desc,  data)] = tili
                iepg = self.riscrebrot[(nivell, codi, data)]['iepg'] if (nivell, codi, data) in self.riscrebrot else 0
                ia14 = self.riscrebrot[(nivell, codi, data)]['ia'] if (nivell, codi, data) in self.riscrebrot else 0
                rt = self.riscrebrot[(nivell, codi, data)]['rt'] if (nivell, codi, data) in self.riscrebrot else 0
                if iepg >= 100:
                    riscR = 4
                elif 70 <= iepg < 100:
                    riscR = 3
                elif 30 <= iepg < 70:
                    riscR = 2
                else:
                    riscR = 1
                els_riscos[(nivell, codi, desc,  data)] = riscR  
                if data == self.fa3 or data == self.fa4 or data == self.fa10:
                    if data == self.fa3:
                        dat = '1. actual'
                    elif data == self.fa4:
                        dat = '2. dia anterior'
                    elif data == self.fa10:
                        dat = '3. setmana anterior'
                      
                    if (nivell =='LOCALITAT' and n > 20000) or nivell == 'COMARCA' or (nivell=='AGA' and codi in codis_aga):
                        pre_up[(nivell, codi, desc, dat, n, casos, tcasos, iepg, ia14, rt, riscR, tpcr, pcr, edat_m)] = True
            
        for (nivell, codi, desc, dat, n, casos, tcasos, iepg, ia14, rt, riscR, tpcr, pcr, edat_m), res in pre_up.items():
            var_dia_RR, var_set_RR = 0, 0
            if dat == '1. actual':
                tili = self.les_ili[(nivell, codi, desc, self.avui)]
                risc1 = els_riscos[(nivell, codi, desc,  self.fa4)] if (nivell, codi, desc,  self.fa1) in els_riscos else  0
                risc7 = els_riscos[(nivell, codi, desc,  self.fa10)] if (nivell, codi, desc,  self.fa7) in els_riscos else  0
                var_dia_RR = int(riscR) - int(risc1)
                var_set_RR = int(riscR) - int(risc7)
            elif dat == '2. dia anterior':
                tili = self.les_ili[(nivell, codi, desc, self.fa1)]
            if dat == '3. setmana anterior':
                tili = self.les_ili[(nivell, codi, desc, self.fa7)]
            self.upload.append([nivell, str(codi), desc, dat, n, casos, tcasos, ia14,  rt, iepg, tpcr, pcr, tili, edat_m, riscR, var_dia_RR, var_set_RR])
    
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("nivell varchar2 (100)", "codi varchar2(30)",  "descripcio varchar2(500)","data varchar2(30)", 
                "poblacio int",   "confirmat_N int", "confirmat_t_7 number(20, 13)", "confirmat_t_14 number(20, 13)", "Rt_M  number(20, 13)", "iEPG_confirmat  number(20, 13)","pcr number(20, 13)", "pcr_N number(20, 13)","ili number(20, 13)", "edat_m number(20, 13)",
                "iEPG_categ int", "var_dia_RR int", "var_set_RR int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)

if __name__ == '__main__':
    u.printTime("Inici")
    
    frankesntein()
    
    u.printTime("Final")