# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import csv,os,sys
from datetime import datetime, timedelta

import sisapUtils as u

db = 'redics'
tb = 'sisap_covid_agr_ili_estimacio'

path = u.tempFolder + "ILI/" 

path2 = u.tempFolder + "ILI2021/" 

cat_territoris = {'COMARCA': 'COMARCA', 'REGIO': 'REGIO', 'SAP_DES': 'SAP', 'GLOBAL': 'GLOBAL', 'PROVINCIA': 'PROVINCIA', 'SECTOR':'SECTOR', 'AMB_DES':'AMBIT', 'AGA_DES':'AGA', 'LOCALITAT': 'LOCALITAT'}


class ILI_estimacio(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_files()
        self.get_files21()
        self.export_data()
        self.dona_grants()  
    
    def get_files(self):
        """Agafem estimacions"""
        u.printTime("fitxers")
        self.upload = []
        n = 0
        for file1 in os.listdir(path):
            n += 1
            print file1
            tipus = file1[7:12]
            g_ed = file1[-12:-11]
            file = path + file1
            if g_ed in ('0', '1','2'):
                edat = int(g_ed)
            else:
                edat = 3
            for territori, codi, data, forecast, low80, hi80, low90, hi90, low95, hi95 in u.readCSV(file, sep=";"):
                if territori == 'unitat_territori':
                    continue
                territori_cod = cat_territoris[territori]    
                dat = d.datetime.strptime(data, '%Y-%m-%d')    
                forecast = float(forecast.replace(",", "."))
                low80 = float(low80.replace(",", "."))
                hi80 = float(hi80.replace(",", "."))
                low90 = float(low90.replace(",", "."))
                hi90 = float(hi90.replace(",", "."))
                low95 = float(low95.replace(",", "."))
                hi95 = float(hi95.replace(",", "."))
                if dat >= d.datetime.strptime('2020-03-01', '%Y-%m-%d') :
                    self.upload.append([str(tipus), territori_cod, codi, dat, edat, forecast, low80,  hi80, low90, hi90, low95, hi95])
        print n
        
    def get_files21(self):
        """Agafem estimacions 2020 2021"""
        u.printTime("fitxers")
        self.upload2 = []
        n = 0
        for file1 in os.listdir(path2):
            n += 1
            print file1
            tipus = file1[7:12]
            g_ed = file1[-12:-11]
            file = path2 + file1
            if g_ed in ('0', '1','2'):
                edat = int(g_ed)
            else:
                edat = 3
            for territori, codi, data, forecast, low80, hi80, low90, hi90, low95, hi95 in u.readCSV(file, sep=";"):
                if territori == 'unitat_territori':
                    continue
                territori_cod = cat_territoris[territori]    
                dat = d.datetime.strptime(data, '%Y-%m-%d')    
                forecast = float(forecast.replace(",", "."))
                low80 = float(low80.replace(",", "."))
                hi80 = float(hi80.replace(",", "."))
                low90 = float(low90.replace(",", "."))
                hi90 = float(hi90.replace(",", "."))
                low95 = float(low95.replace(",", "."))
                hi95 = float(hi95.replace(",", "."))
                self.upload2.append([str(tipus), territori_cod, codi, dat, edat, forecast, low80,  hi80, low90, hi90, low95, hi95])
        print n
        
    
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("tipus varchar2(20)", "nivell varchar2(100)", "codi varchar2(20)", "data date", "edat int", "forecast number(20, 13)",
                "low80 number(20, 13)", "hi80 number(20, 13)", "low90 number(20, 13)", "hi90 number(20, 13)", "low95 number(20, 13)", "hi95 number(20, 13)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.listToTable(self.upload2, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
    
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    ILI_estimacio()
    
    u.printTime("Final")