# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

db = "redics"
tb = "sisap_covid_agr_risc_aquas"



class diagrama_risc(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_cataleg()
        self.get_covid()
        self.get_aquas()
        self.get_ilis()
        self.get_calculs()
        self.export_data()
        self.dona_grants()
        
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg = {}
        sql = """select abs_c, comarca_c, provincia_c, regio_c, sector_c from sisap_covid_cat_territori"""
        for abs, com, prov, rs, sec in u.getAll(sql, db):
            self.cataleg[abs] = {'comarca': com, 'provincia': prov, 'regio': rs, 'sec': sec, 'aga': 'n0'}
            
        sql = """select abs_cod, aga_cod from sisap_covid_cat_aga"""
        for abs, aga in u.getAll(sql, db):
            if abs in self.cataleg:
                self.cataleg[abs]['aga'] = str(aga)
    
    def get_covid(self):
        """Agafem dx de covid"""
        self.observats1 = c.Counter()
        u.printTime("agafem pob rca")
        sql = """SELECT abs, DATA, sum(poblacio)
                FROM preduffa.sisap_covid_tst_brots
                GROUP BY abs, data"""
        for abs, data, poblacio in u.getAll(sql, db):
                self.observats1[('GLOBAL','GLOBAL', data, 'pob')] += poblacio
                regio = self.cataleg[abs]['regio']
                comarca = self.cataleg[abs]['comarca']
                provincia = self.cataleg[abs]['provincia']
                sec = self.cataleg[abs]['sec']
                aga = self.cataleg[abs]['aga']
                self.observats1[('REGIO', regio, data, 'pob')] += poblacio
                self.observats1[('COMARCA', comarca, data, 'pob')] += poblacio
                self.observats1[('PROVINCIA', provincia, data, 'pob')] += poblacio
                self.observats1[('SECTOR', sec, data, 'pob')] += poblacio  
                self.observats1[('AGA', aga, data, 'pob')] += poblacio                  
  
    def get_aquas(self):
        """Agafem aquas"""
        u.printTime("agafem aquas")    
        sql = "select data_afectat, abs_codi, count(*) from SISAP_COVID_PAC_AQUAS where afectat in ('Positiu') group by data_afectat, abs_codi"
        for data, abs, aquas in u.getAll(sql, db):
            self.observats1[('GLOBAL','GLOBAL', data, 'aquas')] += aquas
            if abs in self.cataleg:
                regio = self.cataleg[abs]['regio']
                comarca = self.cataleg[abs]['comarca']
                provincia = self.cataleg[abs]['provincia']
                sec = self.cataleg[abs]['sec']
                aga = self.cataleg[abs]['aga']
                self.observats1[('REGIO', regio, data, 'aquas')] += aquas
                self.observats1[('COMARCA', comarca, data, 'aquas')] += aquas
                self.observats1[('PROVINCIA', provincia, data, 'aquas')] += aquas
                self.observats1[('SECTOR', sec, data, 'aquas')] += aquas
                self.observats1[('AGA', aga, data, 'aquas')] += aquas
 
    def get_ilis(self):
        """ILI des de taula brots"""
        self.ilis = {}
        for  (nivell, codi, data, tip), n in self.observats1.items():
            if tip == 'pob':
                aquas = self.observats1[(nivell, codi, data, 'aquas')]
                self.ilis[(data, nivell, codi)] = {'pob': n, 'aquas': aquas, 'r0': None, 'IA14': None}

    def get_calculs(self):
        """."""
        for (data, nivell, codi), n in self.ilis.items():
            d1 = data - timedelta(days=1)
            d2 = data - timedelta(days=2)
            d5 = data - timedelta(days=5)
            d6 = data - timedelta(days=6)
            d7 = data - timedelta(days=7)
            pob = self.ilis[(data, nivell, codi)]['pob']
            ili1 = self.ilis[(d1, nivell, codi)]['aquas'] if (d1, nivell, codi) in self.ilis else 0
            ili2 = self.ilis[(d2, nivell, codi)]['aquas'] if (d2, nivell, codi) in self.ilis else 0
            ili5 = self.ilis[(d5, nivell, codi)]['aquas'] if (d5, nivell, codi) in self.ilis else 0
            ili6 = self.ilis[(d6, nivell, codi)]['aquas'] if (d6, nivell, codi) in self.ilis else 0
            ili7 = self.ilis[(d7, nivell, codi)]['aquas'] if (d7, nivell, codi) in self.ilis else 0
            ili0 = n['aquas']
            try: 
                r0 = float((ili0 + ili1 + ili2))/float((ili5 + ili6 + ili7))
            except ZeroDivisionError:
                r0 = 0
            self.ilis[(data, nivell, codi)]['r0'] = r0
            ia14 = ili0
            d14 = data - timedelta(days=13)
            for single_date in u.dateRange(d14, data):
                try:
                    iliN = self.ilis[(single_date, nivell, codi)]['aquas']
                except KeyError:
                    iliN = 0
                ia14 += iliN
            IA = float(ia14)/float(pob)*100000
            self.ilis[(data, nivell, codi)]['IA14'] = IA
            
        
        self.upload = []  
        for (data, nivell, codi), n in self.ilis.items():
            d7 = data - timedelta(days=6)
            r7 =  self.ilis[(data, nivell, codi)]['r0']
            for single_date in u.dateRange(d7, data):
                try:
                    r0 = self.ilis[(single_date, nivell, codi)]['r0']
                except KeyError:
                    r0 = 0
                r7 += r0
            r0m = float(r7)/float(7)
            ia = self.ilis[(data, nivell, codi)]['IA14']
            EPG = float(r0m) * float(ia)
            
            if data > d.datetime.strptime('2020-03-13', '%Y-%m-%d'): 
                self.upload.append([data, nivell, codi, n['pob'], n['aquas'], n['r0'], r0m, n['IA14'], EPG])
        
    
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("data date", "nivell varchar2 (100)", "codi varchar2(30)",
                "poblacio_rca int", "confirmat_aquas int",  "r0_aquas number(20, 13)", "r0_aquas_M number(20, 13)", "IA14_aquas number(20, 13)", "iEPG_aquas  number(20, 13)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)

if __name__ == '__main__':
    u.printTime("Inici")
    
    diagrama_risc()
    
    u.printTime("Final")