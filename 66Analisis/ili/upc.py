# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep
import hashlib as h

import sisapUtils as u

db = "redics"

avui =  d.datetime.now().date() 


file = u.tempFolder + "dades_risc_art_" + str(avui) + ".txt"
file_g = u.tempFolder + "dades_risc_gravetat_" + str(avui) + ".txt"
file_nc = u.tempFolder + "dades_positivitat_" + str(avui) + ".txt"
file_pn = u.tempFolder + "dades_pneumonia_" + str(avui) + ".txt"
file_tar = u.tempFolder + "dades_TAR_" + str(avui) + ".txt"
file_tar_pos = u.tempFolder + "positivitat_tar_" + str(avui) + ".txt"


class diagrama_risc(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_cataleg()
        self.get_ilis()
        #self.get_aquas()
        self.get_master()
        self.get_simptomes()
        self.export_fitxer()   
    
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg = {}   
        sql = """select abs_cod, aga_cod, aga_des from sisap_covid_cat_aga"""
        for  abs, aga, aga_des  in u.getAll(sql, db):
            self.cataleg[abs] = {'AGA': aga, 'desc': aga_des}

    def get_ilis(self):
        """Agafem dx de covid"""
        self.observats = c.Counter()
        self.pcrNocas = c.Counter()
        u.printTime("agafem covid")
        sql = """SELECT abs, grup, to_char(DATA, 'YYYY-MM-DD'), sum(poblacio), sum(CASOS_CONFIRMAT), sum(simptomes_si), sum(DX_covid), sum(DX_ILI), sum(pcr), sum (exitus), 
        sum(ingressats_total), sum(ingressats_critic), sum(pcr_no_cas_pos),sum(pcr_no_cas), sum(pneumonia),
        sum(casos_confirmat_only_tar),sum(tar),sum(tar_no_cas_pos),sum(tar_no_cas)
                FROM preduffa.sisap_covid_web_master
                WHERE  residencia = 0
                GROUP BY abs, data, grup"""
        for abs, grup, data, pob, casos, simpt, dx, ili1, pcr, exitus, ing, uci, pcrPos, pcrNC, pneumo, casos_tar, tar, tar_pos, tar_nc in u.getAll(sql, db):
            if abs in self.cataleg:
                aga = self.cataleg[abs]['AGA']
                aga_desc = self.cataleg[abs]['desc']
                self.observats[(data, aga, aga_desc, 'pob')] += pob
                self.observats[(data, aga, aga_desc, 'casos')] += casos
                self.observats[(data, aga, aga_desc, 'casos_tar')] += casos_tar
                self.observats[(data, aga, aga_desc, 'dx')] += dx
                ili = dx + ili1
                self.observats[(data, aga, aga_desc, 'ili')] += ili
                self.observats[(data, aga, aga_desc, 'pcr')] += pcr
                self.observats[(data, aga, aga_desc, 'tar')] += tar
                self.observats[(data, aga, aga_desc, 'pcrSIMPT')] += simpt
                self.observats[(data, aga, aga_desc, 'exitus')] += exitus
                self.observats[(data, aga, aga_desc, 'ing')] += ing 
                self.observats[(data, aga, aga_desc, 'uci')] += uci
                self.pcrNocas[(data, aga, aga_desc, grup, 'pcrPos')] += pcrPos
                self.pcrNocas[(data, aga, aga_desc, grup, 'tarPos')] += tar_pos
                self.pcrNocas[(data, aga, aga_desc, grup, 'pcrNC')] += pcrNC
                self.pcrNocas[(data, aga, aga_desc, grup, 'tarNC')] += tar_nc
                self.pcrNocas[(data, aga, aga_desc, grup, 'pob')] += pob
                self.pcrNocas[(data, aga, aga_desc, grup, 'pneumo')] += pneumo
            else:
                self.observats[(data, 'SES', 'SES', 'pob')] += pob
                self.observats[(data, 'SES', 'SES', 'casos')] += casos
                self.observats[(data, 'SES', 'SES','casos_tar')] += casos_tar
                self.observats[(data, 'SES', 'SES', 'dx')] += dx
                ili = dx + ili1
                self.observats[(data, 'SES', 'SES', 'ili')] += ili
                self.observats[(data, 'SES', 'SES','pcr')] += pcr
                self.observats[(data, 'SES', 'SES', 'tar')] += tar
                self.observats[(data, 'SES', 'SES', 'pcrSIMPT')] += simpt
                self.observats[(data, 'SES', 'SES', 'exitus')] += exitus
                self.observats[(data, 'SES', 'SES', 'ing')] += ing 
                self.observats[(data, 'SES', 'SES', 'uci')] += uci
                self.pcrNocas[(data, 'SES', 'SES', grup, 'pcrPos')] += pcrPos
                self.pcrNocas[(data, 'SES', 'SES',grup, 'tarPos')] += tar_pos
                self.pcrNocas[(data, 'SES', 'SES', grup, 'pcrNC')] += pcrNC
                self.pcrNocas[(data, 'SES', 'SES', grup, 'tarNC')] += tar_nc
                self.pcrNocas[(data, 'SES', 'SES',  grup, 'pob')] += pob
                self.pcrNocas[(data, 'SES', 'SES',  grup, 'pneumo')] += pneumo
            
    def get_aquas(self):
        """Agafem positius aquas segons data carrega o data peticio"""
        positius_ = {}
        sql = """	select a.cip, abs, data_validacio_mostra,to_char(data_alta_resultat , 'YYYY-MM-DD')
                        from dwaquas.COVID19_AUT_RESUL_PCR a, rca_cip_nia b
                        where a.cip=b.cip and resultat='Positiu'"""
        for cip, abs, data_m, data_r in u.getAll(sql, 'exadata'):
            if cip in positius_:
                dant = positius_[cip]['d1']
                try:
                    if data_m < dant:
                        positius_[cip]['d1'] = data_m
                        positius_[cip]['d2'] = data_r
                        positius_[cip]['abs'] = abs 
                except TypeError:
                    continue
            else:
                positius_[cip] = {'d1': data_m, 'd2': data_r, 'abs': abs}
        
        for cip, n in positius_.items():
            dat = n['d2']
            abs =n['abs']
            if str(abs) in self.cataleg:
                aga = self.cataleg[abs]['AGA']
                aga_desc = self.cataleg[abs]['desc']
                self.observats[(dat, aga, aga_desc, 'resultats')] += 1
            else:
                self.observats[(dat, 'SES', 'SES', 'resultats')] += 1
        
        sql = """select abs_codi, to_char(data_afectat, 'YYYY-MM-DD'),to_char(primera_data_notif_positiu, 'YYYY-MM-DD')
                from dwaquas.f_covid19_v2
                where afectat='Positiu' and rela_resid is null or rela_resid <>'Resident'"""
        for abs, data_a, data_r in u.getAll(sql, 'exadata'):
            if str(abs) in self.cataleg:
                aga = self.cataleg[abs]['AGA']
                aga_desc = self.cataleg[abs]['desc']
                self.observats[(data_a, aga, aga_desc, 'afectats')] += 1
                self.observats[(data_r, aga, aga_desc, 'first_r')] += 1
            else:
                self.observats[(data_a, 'SES','SES', 'afectats')] += 1
                self.observats[(data_r, 'SES','SES', 'first_r')] += 1
                

    def get_master(self):
        """."""
        sql = """select abs,  to_char(dx_dde, 'YYYY-MM-DD') 
                from preduffa.sisap_covid_pac_master 
                where resi is null and (estat='Possible' or (estat='Confirmat' and (dx_dde<pcr_data or pcr_data is null))) and dx_dde is not null"""
        for abs, data in u.getAll(sql, 'redics'):
            if str(abs) in self.cataleg:
                aga = self.cataleg[abs]['AGA']
                aga_desc = self.cataleg[abs]['desc']
                self.observats[(data, aga, aga_desc, 'master')] += 1
            else:
                self.observats[(data, 'SES','SES', 'master')] += 1

    def get_simptomes(self):
        """."""
        sql = """SELECT abs, to_char(cas_data, 'YYYY-MM-DD')  
            FROM PREDUFFA.sisap_covid_pac_master 
            where resi is null and estat in ('Possible','Confirmat') and (simpt_estatgeneral is not null or simpt_estatmental is not null or simpt_tos is not null or simpt_dispnea is not null or simpt_dolorpleuritic is not null or simpt_hemoptisi is not null
            or simpt_vomits is not null or simpt_diarrea is not null or simpt_altgust is not null) and dx_dde is not null"""
        for abs, data in u.getAll(sql, 'redics'):
            if str(abs) in self.cataleg:
                aga = self.cataleg[abs]['AGA']
                aga_desc = self.cataleg[abs]['desc']
                self.observats[(data, aga, aga_desc, 'simptomes')] += 1
            else:
                self.observats[(data, 'SES','SES', 'simptomes')] += 1
        
    def export_fitxer(self):
        """obtenim fitxer"""
        upload, upload_g,upload_nc, upload_pn, upload_tar, upload_tar_pos = [], [], [], [], [],[]
        for (data, aga, aga_desc, tipus), n in self.observats.items():
            if tipus == 'pob':
                casos = self.observats[(data, aga, aga_desc, 'casos')] if (data, aga, aga_desc, 'casos') in self.observats else 0
                casos_tar = self.observats[(data, aga, aga_desc, 'casos_tar')] if (data, aga, aga_desc, 'casos_tar') in self.observats else 0
                dx = self.observats[(data, aga, aga_desc, 'dx')] if (data, aga, aga_desc, 'dx') in self.observats else 0
                ili = self.observats[(data, aga, aga_desc, 'ili')] if (data, aga, aga_desc, 'ili') in self.observats else 0
                #afectats = self.observats[(data, aga, aga_desc, 'afectats')] if (data, aga, aga_desc, 'afectats') in self.observats else 0
                pcr = self.observats[(data, aga, aga_desc, 'pcr')] if (data, aga, aga_desc, 'pcr') in self.observats else 0
                tar = self.observats[(data, aga, aga_desc, 'tar')] if (data, aga, aga_desc, 'tar') in self.observats else 0
                #resultats = self.observats[(data, aga, aga_desc, 'resultats')] if (data, aga, aga_desc, 'resultats') in self.observats else 0
                #first_r = self.observats[(data, aga, aga_desc, 'first_r')] if (data, aga, aga_desc, 'first_r') in self.observats else 0
                master = self.observats[(data, aga, aga_desc, 'master')] if (data, aga, aga_desc, 'master') in self.observats else 0
                simptomes = self.observats[(data, aga, aga_desc, 'simptomes')] if (data, aga, aga_desc, 'simptomes') in self.observats else 0
                pcrSIMPT = self.observats[(data, aga, aga_desc, 'pcrSIMPT')] if (data, aga, aga_desc, 'pcrSIMPT') in self.observats else 0
                upload.append([data, aga, dx, ili, casos, pcr, n])
                upload_tar.append([data, aga, casos_tar, tar, n])
                
                exitus = self.observats[(data, aga, aga_desc, 'exitus')] if (data, aga, aga_desc, 'exitus') in self.observats else 0
                ing = self.observats[(data, aga, aga_desc, 'ing')] if (data, aga, aga_desc, 'ing') in self.observats else 0
                uci = self.observats[(data, aga, aga_desc, 'uci')] if (data, aga, aga_desc, 'uci') in self.observats else 0
                upload_g.append([data, aga,ing, uci,exitus, n])
        
            
        u.writeCSV(file, upload, sep='@')
        u.writeCSV(file_g, upload_g, sep='@')
        u.writeCSV(file_tar, upload_tar, sep='@')
        for (data, aga, aga_desc, grup, tipus), n in self.pcrNocas.items():
            if tipus == 'pob':
                pcrP = self.pcrNocas[(data, aga, aga_desc, grup, 'pcrPos')] if (data, aga, aga_desc, grup, 'pcrPos') in self.pcrNocas else 0
                pcrNC = self.pcrNocas[(data, aga, aga_desc, grup, 'pcrNC')] if (data, aga, aga_desc, grup, 'pcrNC') in self.pcrNocas else 0
                tarP = self.pcrNocas[(data, aga, aga_desc, grup, 'tarPos')] if (data, aga, aga_desc, grup, 'tarPos') in self.pcrNocas else 0
                tarNC = self.pcrNocas[(data, aga, aga_desc, grup, 'tarNC')] if (data, aga, aga_desc, grup, 'tarNC') in self.pcrNocas else 0
                upload_nc.append([data, aga, aga_desc, grup, pcrP, pcrNC, n])
                upload_tar_pos.append([data, aga, aga_desc, grup, tarP, tarNC, n])
                
                pneumo = self.pcrNocas[(data, aga, aga_desc, grup, 'pneumo')] if (data, aga, aga_desc, grup, 'pneumo') in self.pcrNocas else 0
                upload_pn.append([data, aga, aga_desc, grup,pneumo, n])
        u.writeCSV(file_nc, upload_nc, sep='@')
        u.writeCSV(file_tar_pos, upload_tar_pos, sep='@')
        u.writeCSV(file_pn, upload_pn, sep='@')

if __name__ == '__main__':
    u.printTime("Inici")

    diagrama_risc()

    u.printTime("Final")
                
            