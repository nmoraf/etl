
import collections as c
import datetime as d
import sys
import datetime as d
from time import time,sleep

import sisapUtils as u



codis = ("C01-B34.2", "C01-B97.21", "C01-B97.29", "C01-J12.81",
                 "C01-J12.89", "C01-Z20.828", "C01-U07.1",
                 "B25.0", "B30.2","J00","J09","J10","J10.0","J10.1","J10.8","J11","J11.0","J11.1","J11.8", 
"J12","J12.0","J12.1","J12.2","J12.3","J12.8","J12.9","J17.1","J20.3","J20.4","J20.5","J20.6","J20.7","J21.0", 
"J21.1","P23.0","U04","U04.9","C01-B25.0","C01-B30.2","C01-B33.4","C01-B97.21","C01-J00","C01-J09","C01-J09.X", 
"C01-J09.X1","C01-J09.X2","C01-J09.X3","C01-J09.X9","C01-J10","C01-J10.0","C01-J10.00","C01-J10.01","C01-J10.08", 
"C01-J10.1","C01-J10.2","C01-J10.8","C01-J10.81","C01-J10.82","C01-J10.83","C01-J10.89","C01-J11","C01-J11.0","C01-J11.00","C01-J11.08", 
"C01-J11.1","C01-J11.2","C01-J11.8","C01-J11.81","C01-J11.82","C01-J11.83","C01-J11.89","C01-J12","C01-J12.0","C01-J12.1","C01-J12.2","C01-J12.3","C01-J12.8", 
"C01-J12.81","C01-J12.89","C01-J12.9","C01-J20.3","C01-J20.4","C01-J20.5","C01-J20.6","C01-J20.7","C01-J21.0","C01-J21.1","C01-P23.0","C01-J20.7","C01-J21.0", 
"C01-J21.1","C01-P23.0")
sql = "select  pr_cod_u,pr_cod_ps, pr_th, pr_dde, pr_hist \
               from prstb015 \
               where pr_cod_o_ps = 'C' and \
                     pr_cod_ps in {} and \
                     to_char(pr_dde,'YYYYMMDD')>'20200228' and \
                     pr_data_baixa is null".format(codis)
diagnostics = c.defaultdict(set)
dadesd = c.Counter()

for cip, ps, th, dde, hist in u.getAll(sql, 'redics'):
    if ps != "C01-Z20.828" or th == 16185:
        diagnostics[hash].add(dde)
for hash, dates in diagnostics.items():
    for dat in dates:
        te_previ = any([(dat - d.timedelta(days=14)) < dia < dat
                                for dia in dates])
        if not te_previ:
            dadesd[dat] += 1

upload = []
for (dat), n in dadesd.items(): 
    upload.append([dat, n])
u.writeCSV(u.tempFolder + "prova_dx_tot", upload, sep="@")       