# coding: utf8

"""
Procés per treure dades de longitudinalitat de sisap
"""

anys = [2017, 2018]

import collections as c
import datetime as d

import sisapUtils as u

tb_pac = "long_pacient_2019"
db = "permanent"


class LongCont(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_long()
        self.get_hashos()
        self.get_atdom()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.export_dades()
 
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: br for (up, br) in u.getAll(*sql)}
    
    def get_long(self):
        """De la taula longitudinalitat de sisap"""
        self.long = {}
        sql = "select id_cip_sec, nprof, vprof, totals, r1, r2, r3, r4 from mst_long_cont_pacient where servei = 'MG'"
        for id, nprof, vprof, totals, r1, r2, r3, r4 in u.getAll(sql, 'altres'):
           self.long[(id, 'nprof')] = nprof
           self.long[(id, 'vprof')] = vprof
           self.long[(id, 'nvisites')] = totals
           self.long[(id, 'CONT0001')] = r1
           self.long[(id, 'CONT0002')] = r2
           self.long[(id, 'CONT0003')] = r3
           self.long[(id, 'CONT0004')] = r4
           

    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}                

    
  
    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdoms = {}
        sql = "select id_cip_sec from eqa_problemes where ps = 45"
        for id, in u.getAll(sql, 'nodrizas'):
           self.atdoms[id]= True
    
    def get_pob(self):
        """Agafem poblacio """
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id_cip_sec, codi_sector, up, sexe, edat, nacionalitat, institucionalitzat from assignada_tot where edat>14"
        for id, sec, up, sexe, edat, nac, insti in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                idh = hash + ':' + sec
                atd = 1 if id in self.atdoms else 0
                nac1 = 1 if nac in renta else 0
                cont001 = self.long[(id, 'CONT0001')] if (id, 'CONT0001') in self.long else None
                cont002 = self.long[(id, 'CONT0002')] if (id, 'CONT0002') in self.long else None
                cont003 = self.long[(id, 'CONT0003')] if (id, 'CONT0003') in self.long else None
                cont004 = self.long[(id, 'CONT0004')] if (id, 'CONT0004') in self.long else None
                nvisites = self.long[(id, 'nvisites')] if (id, 'nvisites') in self.long else None
                nprof = self.long[(id, 'nprof')] if (id, 'nprof') in self.long else None
                self.dades[idh] = {
                              'sector': sec, 'up': up, 'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': None, 'insti': insti, 'atdom': atd, 
                              'C1': cont001, 'C2': cont002, 'C3': cont003, 'C4': cont004, 'nvisites': nvisites, 'nprof': nprof}
    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades:
                self.dades[idh]['gma_cod'] = cod
                self.dades[idh]['gma_cmplx'] = cmplx
                self.dades[idh]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades and sector in valors:
                self.dades[idh]['medea'] = valors[sector]
    
   
    def export_dades(self):
        """."""
        upload = [(id, d['up'], d['sex'], d['edat'],d['renta'], d['gma_cod'], d['gma_cmplx'],d['gma_num'], d['medea'], d['insti'], d['atdom'],
                    d['C1'], d['C2'], d['C3'], d['C4'], d['nvisites'], d['nprof'] )
                 for id, d in self.dades.items()]

        columns = ["id varchar(100)",   "up varchar(5)", "sexe varchar(1)",
                    "edat int", "nac1 int",  'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'atdom int', "cont001 double", "cont002 double", "cont003 double", "cont004 double",
                     "nvisites int", "nprof int"]
        u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb_pac, db)
    
if __name__ == "__main__":
    LongCont()

    