# coding: utf8

"""
Mirem de treure el percentatge de visites d'un metge de fora d'una uba
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'long_up'

class visites_foraUba(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_adults()
        self.get_moduls()
        self.get_hores_agenda()
        self.get_visites()
        self.get_prof()
        self.get_despesa()
        self.get_its()
        self.upload_data()

    def get_centres(self):
        """EAP ICS i no ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}
        self.brs = {br: (up) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_adults(self):
        """Població adulta"""
        self.pob = {}
        sql = 'select id_cip_sec, up from assignada_tot where edat>14'
        for id, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                self.pob[id] = True
    
    def get_moduls(self):
        """Moduls de MG lligats a UBA"""
        sql = ("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul, modu_codi_uab \
                from cat_vistb027 \
                where modu_codi_uab <> '' and modu_servei_codi_servei='MG'", "import")
        self.moduls = {(sec, cen, cla, ser, cod): (uba) for (sec, cen, cla, ser, cod, uba)
                        in u.getAll(*sql)}
                        
    def get_visites(self):
        """."""
        self.dades = Counter()
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), "import")
            if dat == 2018:
                sql ="select  codi_sector, id_cip_sec, visi_up, visi_centre_codi_centre, visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul\
                      from {} \
                        where visi_servei_codi_servei = 'MG' and visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_col_prov_resp <> 0".format(table)
                for sector, id, up, centre, classe, servei, modul in u.getAll(sql, "import"):
                    if up in self.centres:
                        br = self.centres[up][0]
                        if id in self.pob:
                            self.dades[(up,'den')] += 1
                            if (sector, centre, classe, servei, modul) in self.moduls:
                                uba = self.moduls[(sector, centre, classe, servei, modul)]
                                self.dades[(up, 'num')] += 1
        self.visites_mf_uba = {}
        for (up,  tip), rec in self.dades.items():
            if tip == 'den':
                num = self.dades[(up, 'num')]
                vfm = float(num)/float(rec)
                self.visites_mf_uba[up] = vfm
    
    def get_hores_agenda(self):
        """."""
        self.agendes = {}
        sql = """           
            SELECT
                    br
                    ,count(*)
                    ,round(avg(cuenta_dias_de_agendas),2) AS avg_weekly_agenda_days
                    ,round(avg(suma_hores),2) AS avg_weekly_hours
                    ,round(avg(gt25w)*100,2) AS media_gt25w
            FROM
                    (SELECT
                            br, CENTRE, ambit, codi
                            ,(sum(minuts_9c)+ sum(minuts_9d)+ sum(minuts_9r)+sum(minuts_9t)+sum(minuts_9e))/60 AS suma_hores
                            , CASE WHEN
                                    (sum(minuts_9c)+ sum(minuts_9d)+ sum(minuts_9r)+sum(minuts_9t)+sum(minuts_9e))/60 >25
                                    THEN 1 ELSE 0 END AS gt25w
                            ,count(*) AS cuenta_dias_de_agendas
                      FROM
                                    preduffa.sisap_estructura_agendes
                      group BY
                                    br, CENTRE, ambit, codi)
            group BY
                    br
            """
        for br, n_agendes, days, hours, mes25 in u.getAll(sql, 'redics'):
            self.agendes[br] = {'days': days, 'hours': hours, 'mes25': mes25}
    
    def get_prof(self):
        """Mirem mitjana de professionals per plantilla de 2018 i 2017"""
        self.mf_2018 = Counter()
        file = u.baseFolder + ["66Analisis\Longitudinalitat", "MF_2018.txt"]
        file = "\\".join(file)
        for br, mf in u.readCSV(file, sep="@"):
            self.mf_2018[br] += float(mf)
            if br in self.brs:
                ok = 1
            else:
                print br
            
        self.mf_2017 = Counter()
        file = u.baseFolder + ["66Analisis\Longitudinalitat", "MF_2017.txt"]
        file = "\\".join(file)
        for br, mf in u.readCSV(file, sep="@"):
            self.mf_2017[br] += float(mf)
        
    
    def get_despesa(self):
        """despesa de khalix AGDVA (capítol 1 variable) de categoria AGDMET. Ho trec en khalix per excel i ho passo per dividir pel nombre de metges assistencials
        """
        self.despesa_var = {}
                
        file = u.baseFolder + ["66Analisis\Longitudinalitat", "AGDVA_AGDMET.txt"]
        file = "\\".join(file)
        for br, despesa in u.readCSV(file, sep="@"):
            self.despesa_var[br] = despesa
        
    def get_its(self):
        """Agafem ITs, baixes maternitat i reduccions jornades del que ens passa nòmines dels anys 2017 i 2018"""
        self.conceptes = Counter()
        
        file = u.baseFolder + ["66Analisis\Longitudinalitat", "it_2017.txt"]
        file = "\\".join(file)
        for br, conc, metges, dies in u.readCSV(file, sep="@"):
            dies = int(dies)
            if conc in ('IN', 'IT', 'PI'):
                self.conceptes[(br, 'IT')] += dies
            elif conc in ('MJ', 'PT', 'RE'):
                self.conceptes[(br, 'BM')] += dies
            elif conc in ('RJ', 'RV'):
                self.conceptes[(br, 'RJ')] += dies
            elif conc == 'AB':
                self.conceptes[(br, 'AB')] += dies
            if br in self.brs:
                ok = 1
            else:
                print br
                
        file = u.baseFolder + ["66Analisis\Longitudinalitat", "it_2018.txt"]
        file = "\\".join(file)
        for br, conc, metges, dies in u.readCSV(file, sep="@"):
            dies = int(dies)
            if conc in ('IN', 'IT', 'PI'):
                self.conceptes[(br, 'IT')] += dies
            elif conc in ('MJ', 'PT', 'RE'):
                self.conceptes[(br, 'BM')] += dies
            elif conc in ('RJ', 'RV'):
                self.conceptes[(br, 'RJ')] += dies
            elif conc == 'AB':
                self.conceptes[(br, 'AB')] += dies
                
    
    def upload_data(self):
        """."""
        upload = []
        for up, da in self.centres.items():
            br = da[0]
            vfm = self.visites_mf_uba[up] if up in self.visites_mf_uba else 0
            days = self.agendes[br]['days'] if br in self.agendes else 0
            hours = self.agendes[br]['hours'] if br in self.agendes else 0
            mes25 = self.agendes[br]['mes25'] if br in self.agendes else 0
            despesaM = self.despesa_var[br] if br in self.despesa_var else 0
            mf18 = self.mf_2018[br] if br in self.mf_2018 else 0
            mf17 = self.mf_2017[br] if br in self.mf_2017 else 0
            mftot = float(mf18 + mf17) / 2
            absencies = self.conceptes[(br, 'AB')] if (br,'AB') in self.conceptes else 0
            ites = self.conceptes[(br, 'IT')] if (br,'IT') in self.conceptes else 0
            maternals = self.conceptes[(br, 'BM')] if (br,'BM') in self.conceptes else 0
            reduc_jorn = self.conceptes[(br, 'RJ')] if (br,'RJ') in self.conceptes else 0
            upload.append([up, br, vfm, days, hours, mes25, despesaM, absencies, ites, maternals, reduc_jorn, mf17, mf18, mftot])
                
        
        columns = ["up varchar(5)", "br varchar(5)",  "visites_mf_uba double", "avg_weekly_agenda_days double", "avg_weekly_hours double", "media_gt25w double", "AGDVA_MET double" ,
                    "absencies double", "IT double", "maternal double", "reduccio double", "mf17 double", "mf18 double", "mftotal double"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        
        u.listToTable(upload, tb, db)
        
if __name__ == '__main__':
    visites_foraUba()
    