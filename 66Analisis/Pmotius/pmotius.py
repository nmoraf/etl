# coding: latin1

"""
Demanen analitzar programació per motius
"""

import collections as c

import sisapUtils as u


SERVEIS = ("MG", "PED", "INF", "ENF", "INFPD", "INFP", "INFG", "URGEN", "ESP")
USUARIS = ("04104", "04416", "04302")


class GIS001(object):
    """."""

    def __init__(self):
        """."""
        self.get_hashos()
        self.get_tasques()
        self.get_numerador()
        self.get_pim()
        self.get_centres()
        self.get_usuaris()
        self.get_gma()
        self.get_medea()
        self.get_atdom()
        self.get_poblacio()
        self.get_indicador()
        self.export_data()

    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}   
    
    def get_tasques(self):
        """."""
        self.tasques = set()
        sql = "select id_cip_sec, alv_data_al \
               from alertes_s{}, nodrizas.dextraccio \
               where alv_tipus = 'TASCAD' and \
                     alv_data_al between adddate(data_ext, interval -1 year) and data_ext"
        jobs = [(sql.format(sector), "import") for sector in u.sectors]
        for sector in u.multiprocess(_worker, jobs, 8):
            self.tasques |= set(sector)

    def get_numerador(self):
        """."""
        self.numerador = {}
        sql = "select visi_id , visi_motiu_prior  \
               from motius, nodrizas.dextraccio \
               where (visi_data_visita between adddate(data_ext, interval -1 year) and data_ext) and \
                     visi_motiu_prior not in ('', 'CAP')"
        for id, motiu  in u.getAll(sql, 'import'):
            self.numerador[id] = motiu
            
        self.numerador2 = {}
        sql = "select visi_id  \
               from motius, nodrizas.dextraccio \
               where (visi_data_visita between adddate(data_ext, interval -1 year) and data_ext) and \
                     (visi_motiu_prior not in ('', 'CAP', 'No expressa motiu') or \
                      visi_usuari_text <> '' or \
                      visi_motiu_lliure <> '')"
        #for id,  in u.getAll(sql, 'import'):
            #self.numerador2[id] = True

    def get_pim(self):
        """."""
        self.pim = set()
        sql = "select visi_id \
               from motius_s{}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_ticket_teseo <> ''"
        jobs = [(sql.format(sector), "import") for sector in u.sectors]
        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.pim |= pacients

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_desc, sap_desc, ics_codi, ics_desc \
               from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_usuaris(self):
        """."""
        sql = "select codi_sector, ide_usuari from cat_pritb992 \
               where ide_categ_prof_c in {}".format(USUARIS)
        self.usuaris = set(u.getAll(sql, "import"))
        
        self.usuarist = {}
        sql = "select left(ide_dni, 8), ide_categ_prof_c \
                   from cat_pritb992 where ide_categ_prof_c <> ''"
        for usu, categ in u.getAll(sql, "import"):
            tip = 'N'
            if categ in ('10888', '10999', '10117','10116'):
                tip = 'M'
            elif categ in ('30999'):
                tip = 'I'
            if tip in ('M', 'I'):
                self.usuarist[usu] = tip

    def get_gma(self):
        """Obté la complexitat del gma"""
        self.gma = {}
        sql = ("select id_cip_sec, gma_ind_cmplx \
                from gma", "import")
        for id, cmplx in u.getAll(*sql):
            self.gma[id] = cmplx
            
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        self.medea = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if sector in valors:
                self.medea[id] = valors[sector]

    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdom = {}
        sql = ("select id_cip_sec from eqa_problemes where ps = 45",
               "nodrizas")
        for id, in u.getAll(*sql):
            self.atdom[id] = True
    
    def get_poblacio(self):
        """."""
        self.pob = []

        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, up, edat, sexe, ates, \
                       institucionalitzat, nacionalitat, nivell_cobertura, pcc, maca \
                from assignada_tot", "nodrizas")
        for id, up, edat, sexe, ates, insti, nac, cob, pcc, maca in u.getAll(*sql):
            if up in self.centres:
                nac1 = 1 if nac in renta else 0
                gma_cmplx = self.gma[id] if id in self.gma else ''
                atd = 1 if id in self.atdom else 0
                medea1 = self.medea[id] if id in self.medea else ''
                hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                idh = hash + ':' + sec
                self.pob.append([idh,  up, sexe, edat, nac1, gma_cmplx, medea1,  insti,  atd, cob])
    
    
    def get_indicador(self):
        """."""
        self.indicador, self.indicador2 = [], []
        sql = "select visi_id, id_cip_sec, visi_data_visita, left(visi_dni_prov_resp, 8), \
                      codi_sector, visi_assign_visita, visi_tipus_visita, visi_up, visi_dia_peticio \
               from visites1, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_servei_codi_servei in {} and \
                     visi_tipus_visita not in ('9T','9E') and \
                     visi_internet <> 'S' and \
                      visi_forcada_s_n = 'N'".format(SERVEIS)
        for id, pac, dat, dni, sec, usu, tipvis, up, datapeti in u.getAll(sql, "import"):
            if up in self.centres:
                if (sec, usu) in self.usuaris:
                    if (pac, dat) not in self.tasques and id not in self.pim:
                        if id in self.numerador:
                            motiu = self.numerador[id]
                            tipus = self.usuarist[dni] if dni in self.usuarist else ''
                            hash, sec = self.idcip_to_hash[pac]['hash'],self.idcip_to_hash[pac]['sec']
                            idh = hash + ':' + sec
                            self.indicador.append([idh, datapeti, dat, up, tipus, motiu]) 
                        #if id in self.numerador2:
                         #   tipus = self.usuarist[dni] if dni in self.usuarist else ''
                          #  hash, sec = self.idcip_to_hash[pac]['hash'],self.idcip_to_hash[pac]['sec']
                           # idh = hash + ':' + sec
                            #self.indicador2.append([idh, dat, up, tipus]) 
    
    def export_data(self):
        """."""
        db = "permanent"
        tb_pac = "gis_poblacio"
        columns = ["id varchar(100)",   "up varchar(5)", "sexe varchar(1)",
                    "edat int", "nac1 int",   'gma_complexitat double',
                    'medea_seccio double',
                   'institucionalitzat int', 'atdom int', "Cobertura varchar(10)"]
        u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.pob, tb_pac, db)
        """
        tb_vis = "gis_visites"
        columns = ["id varchar(100)",   "data_visita date", "up varchar(10)", "professional varchar(10)", "motiu varchar(2000)"]
        u.createTable(tb_vis, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.indicador2, tb_vis, db)
        """
        tb_pgr = "gis_visites_pgr"
        columns = ["id varchar(100)",   "data_peticio date", "data_visita date", "up varchar(10)", "professional varchar(10)", "motiu varchar(2000)"]
        u.createTable(tb_pgr, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.indicador, tb_pgr, db)


def _worker(params):
    return list(u.getAll(*params))


if __name__ == "__main__":
    GIS001()
