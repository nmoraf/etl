# coding: utf8

import collections as c
import datetime as d

import sisapUtils as u


tb = "sisap_socioecon"
db = "permanent"



class Nivell_socioeco(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hashos()
        self.get_aportacio()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.export_dades()
        
    def get_hashos(self):
        """."""
        u.printTime("hashos")
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec} 

    def get_aportacio(self):
        """."""
        u.printTime("aportacio aquas")
        self.nivell_r = {}
        sql = "select hash, sector, nrenda1 from sisap_nivellrenda"
        for hash, sector, renda1 in u.getAll(sql, 'redics'):
            self.nivell_r[(hash, sector)] = renda1
    
    def get_pob(self):
        """Agafem poblacio """
        u.printTime("pob")
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id_cip_sec, codi_sector, up, sexe, edat, nacionalitat, institucionalitzat from assignada_tot where edat>14"
        for id, sec, up, sexe, edat, nac, insti in u.getAll(sql, 'nodrizas'):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            nac1 = 1 if nac in renta else 0
            nivell = self.nivell_r[(hash, sec)] if (hash,sec) in self.nivell_r else None
            self.dades[idh] = {
                              'sector': sec, 'up': up, 'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': None, 'nivell': nivell, 'insti': insti }    
    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        u.printTime("gma")
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades:
                self.dades[idh]['gma_cod'] = cod
                self.dades[idh]['gma_cmplx'] = cmplx
                self.dades[idh]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades and sector in valors:
                self.dades[idh]['medea'] = valors[sector]
        
    def export_dades(self):
        """."""
        u.printTime("export")
        upload = [(id, d['up'], d['sex'], d['edat'],d['renta'], d['gma_cod'], d['gma_cmplx'],d['gma_num'], d['medea'], d['nivell'], d['insti'])
                 for id, d in self.dades.items()]

        columns = ["id varchar(100)",   "up varchar(5)", "sexe varchar(1)",
                    "edat int", "nac1 int",  'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double', "nrenda1 varchar(100)",
                   'institucionalitzat int']
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
        
if __name__ == "__main__":
    Nivell_socioeco()
