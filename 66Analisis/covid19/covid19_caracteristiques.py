# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

resta = d.datetime.now().hour < 21
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)

db = 'redics'
tb = 'sisap_covid_rec_master__actual'

dx_file =  "dx_covid.txt"

cat_tract = {'P01BA02': 'Hidroxicloroquina', 
             'S01AA26': 'Azitromicina',
             'J01FA10': 'Azitromicina',
             'R03CC52': 'Salbutamol',
             'R03AK13': 'Salbutamol',
             'R03AL02': 'Salbutamol',
             'R03AC02': 'Salbutamol',
             'R03AK04': 'Salbutamol',
             'R03CC02': 'Salbutamol',
             }

class article_dani(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hash()
        self.get_centres()
        self.get_covids()
        self.get_id_to_hash()
        self.get_dbs()
        self.get_dbs_2019()
        self.get_resis()
        self.get_medea()
        self.get_tractaments()
        self.get_ingres()
        self.get_master()
        self.export_data()
        self.dona_grants()
        
  
    def get_hash(self):
        """conversor de hash"""
        u.printTime("hash")
        self.hashI = {}
        sql = """ select hash, hash_ics
                    from sisap_covid_pac_id"""
        for hash,hashi in u.getAll(sql, 'redics'):
            self.hashI[hashi] = hash
     
    def get_centres(self):
        """."""
        self.centres = {}
        sql = """select scs_codi, medea 
                from cat_centres
             """
        for up, medea in u.getAll(sql, 'nodrizas'):
            self.centres[up] = medea
    
    def get_covids(self):
        """."""
        u.printTime("covids")
        self.covids = {}
        sql = """select hash, cas_data, to_char(cas_data, 'YYYYMMDD') from 
                sisap_covid_pac_master where estat in ('Confirmat','Possible')
                """
        for hash, dat, dat2 in u.getAll(sql, db):
            self.covids[hash] = dat2
     
    def get_id_to_hash(self):
        """id a hash"""
        u.printTime("id to hash")
        self.id_to_hash = {}
        sql = """select id_cip, hash_d
                from u11"""
        for id, hash in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash   

    def get_dbs(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS")
        self.factors_header = ("nacionalitat", "CHARTLSTON_V", "CHARTLSTON_D")
        sql = """select hash,
                    c_nacionalitat,
                    V_CHARTLSTON_VALOR,
                    V_CHARTLSTON_DATA,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs a, sisap_covid_pac_id b 
                where a.c_cip = b.hash_ics"""
        self.factors = {}
        self.in_dbs_true = {}
        for (cip,
             nac,
             chart_v,
             chart_d,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, db):
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            if nac:
                self.factors[(cip, "nacionalitat")] = nac
            if chart_d:
                self.factors[(cip, "CHARTLSTON_D")] = chart_d
                self.factors[(cip, "CHARTLSTON_V")] = chart_v
            if hta:
                self.factors[(cip, "hta")] = hta
            if dm1:
                self.factors[(cip, "dm")] = dm1
            if dm2:
                self.factors[(cip, "dm")] = dm2
            if mpoc:
                self.factors[(cip, "mpoc")] = mpoc
            if asma:
                self.factors[(cip, "asma")] = asma
            if bc:
                self.factors[(cip, "bc")] = bc
            if ci:
                self.factors[(cip, "ci")] = ci
            if mcv:
                self.factors[(cip, "mcv")] = mcv
            if ic:
                self.factors[(cip, "ic")] = ic
            if acfa:
                self.factors[(cip, "acfa")] = acfa
            if valv:
                self.factors[(cip, "valv")] = valv
            if hepat:
                self.factors[(cip, "hepat")] = hepat
            if vhb:
                self.factors[(cip, "vhb")] = vhb
            if vhc:
                self.factors[(cip, "vhc")] = vhc
            if neo:
                self.factors[(cip, "neo")] = neo
            if mrc:
                self.factors[(cip, "mrc")] = mrc
            if obes:
                self.factors[(cip, "obes")] = obes
            if vih:
                self.factors[(cip, "vih")] = vih
            if sida:
                self.factors[(cip, "sida")] = sida
            if demencia:
                self.factors[(cip, "demencia")] = demencia
            if artrosi:
                self.factors[(cip, "artrosi")] = artrosi 
            if rcv_dat:
                self.factors[(cip, "rcv_dat")] = rcv_dat
                self.factors[(cip, "rcv_val")] = rcv_val
            if ieca:
                self.factors[(cip, "ieca")] = ieca    
            if calcioa:
                self.factors[(cip, "calcioa")] = calcioa
            if diur:
                self.factors[(cip, "diur")] = diur
            if betb:
                self.factors[(cip, "betb")] = betb
            if alfab:
                self.factors[(cip, "alfab")] = alfab
            if hta_alt:
                self.factors[(cip, "hta_alt")] = hta_alt
            if hta_comb:
                self.factors[(cip, "hta_comb")] = hta_comb    
            if ado:
                self.factors[(cip, "ado")] = ado
            if insulina:
                self.factors[(cip, "insulina")] = insulina
            if f_mpoc:
                self.factors[(cip, "f_mpoc")] = f_mpoc
            if antiag:
                self.factors[(cip, "antiag")] = antiag
            if aine:
                self.factors[(cip, "aine")] = aine
            if analg:
                self.factors[(cip, "analg")] = analg 
            if antidep:
                self.factors[(cip, "antidep")] = antidep
            if ansiol:
                self.factors[(cip, "ansiol")] = ansiol
            if antipsic:
                self.factors[(cip, "antipsic")] = antipsic
            if ulcer:
                self.factors[(cip, "ulcer")] = ulcer
            if anties:
                self.factors[(cip, "anties")] = anties    
            if cortis:
                self.factors[(cip, "cortis")] = cortis
            if epil:
                self.factors[(cip, "epil")] = epil
            if hipol:
                self.factors[(cip, "hipol")] = hipol
            if mhda:
                self.factors[(cip, "mhda")] = mhda
    
    def get_dbs_2019(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        
        sql = """select c_cip,
                    c_nacionalitat,
                    V_CHARTLSTON_VALOR,
                    V_CHARTLSTON_DATA,
                    ps_embaras_data,
                    ps_viu_sol_data,
                    c_gma_codi, c_gma_complexitat,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                    F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs_2019"""
        self.factors19 = {}
        for (cip,
             nac,
             chart_v,
             chart_d,
             embaras,
             viu_sol,
             gma_c, gma_cmplx,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, SIDICS_DB):
            if cip in self.hashI:
                cipCov = self.hashI[cip]
                if cipCov not in self.in_dbs_true:
                    self.factors19[(cipCov, "in_dbs")] = 1 
                    if embaras and u.daysBetween(embaras, TODAY) <280:
                        self.factors19[(cipCov, "embaras")] = 1
                    if viu_sol:
                        self.factors19[(cipCov, "sol")] = 1
                    if gma_c:
                        self.factors19[(cipCov, "gma_c")] = gma_c
                        self.factors19[(cipCov, "gma_cmplx")] = gma_cmplx
                    if nac:
                        self.factors19[(cipCov, "nacionalitat")] = nac
                    if chart_d:
                        self.factors19[(cipCov, "CHARTLSTON_D")] = chart_d
                        self.factors19[(cipCov, "CHARTLSTON_V")] = chart_v
                    if hta:
                        self.factors19[(cipCov, "hta")] = hta
                    if dm1:
                        self.factors19[(cipCov, "dm")] = dm1
                    if dm2:
                        self.factors19[(cipCov, "dm")] = dm2
                    if mpoc:
                        self.factors19[(cipCov, "mpoc")] = mpoc
                    if asma:
                        self.factors19[(cipCov, "asma")] = asma
                    if bc:
                        self.factors19[(cipCov, "bc")] = bc
                    if ci:
                        self.factors19[(cipCov, "ci")] = ci
                    if mcv:
                        self.factors19[(cipCov, "mcv")] = mcv
                    if ic:
                        self.factors19[(cipCov, "ic")] = ic
                    if acfa:
                        self.factors19[(cipCov, "acfa")] = acfa
                    if valv:
                        self.factors19[(cipCov, "valv")] = valv
                    if hepat:
                        self.factors19[(cipCov, "hepat")] = hepat
                    if vhb:
                        self.factors19[(cipCov, "vhb")] = vhb
                    if vhc:
                        self.factors19[(cipCov, "vhc")] = vhc
                    if neo:
                        self.factors19[(cipCov, "neo")] = neo
                    if mrc:
                        self.factors19[(cipCov, "mrc")] = mrc
                    if obes:
                        self.factors19[(cipCov, "obes")] = obes
                    if vih:
                        self.factors19[(cipCov, "vih")] = vih
                    if sida:
                        self.factors19[(cipCov, "sida")] = sida
                    if demencia:
                        self.factors19[(cipCov, "demencia")] = demencia
                    if artrosi:
                        self.factors19[(cipCov, "artrosi")] = artrosi
                    if rcv_dat:
                        self.factors19[(cipCov, "rcv_dat")] = rcv_dat
                        self.factors19[(cipCov, "rcv_val")] = rcv_val
                    if ieca:
                        self.factors19[(cipCov, "ieca")] = ieca    
                    if calcioa:
                        self.factors19[(cipCov, "calcioa")] = calcioa
                    if diur:
                        self.factors19[(cipCov, "diur")] = diur
                    if betb:
                        self.factors19[(cipCov, "betb")] = betb
                    if alfab:
                        self.factors19[(cipCov, "alfab")] = alfab
                    if hta_alt:
                        self.factors19[(cipCov, "hta_alt")] = hta_alt
                    if hta_comb:
                        self.factors19[(cipCov, "hta_comb")] = hta_comb    
                    if ado:
                        self.factors19[(cipCov, "ado")] = ado
                    if insulina:
                        self.factors19[(cipCov, "insulina")] = insulina
                    if f_mpoc:
                        self.factors19[(cipCov, "f_mpoc")] = f_mpoc
                    if antiag:
                        self.factors19[(cipCov, "antiag")] = antiag
                    if aine:
                        self.factors19[(cipCov, "aine")] = aine
                    if analg:
                        self.factors19[(cipCov, "analg")] = analg 
                    if antidep:
                        self.factors19[(cipCov, "antidep")] = antidep
                    if ansiol:
                        self.factors19[(cipCov, "ansiol")] = ansiol
                    if antipsic:
                        self.factors19[(cipCov, "antipsic")] = antipsic
                    if ulcer:
                        self.factors19[(cipCov, "ulcer")] = ulcer
                    if anties:
                        self.factors19[(cipCov, "anties")] = anties    
                    if cortis:
                        self.factors19[(cipCov, "cortis")] = cortis
                    if epil:
                        self.factors19[(cipCov, "epil")] = epil
                    if hipol:
                        self.factors19[(cipCov, "hipol")] = hipol
                    if mhda:
                        self.factors19[(cipCov, "mhda")] = mhda
                        
    def get_resis(self):
        """Agafem pacients que hagin estat algun cop en residències"""
        u.printTime("resis")
        self.pac_res = {}
        sql = "select hash, residencia, entrada, sortida from sisap_covid_res_master"
        for hash, res, entrada, sortida in u.getAll(sql, db):
            self.pac_res[hash] = {'res': res, 'entrada': entrada, 'sortida': sortida}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        self.censal = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              db)}
        sql = ("select usua_cip, sector_censal from md_poblacio", db)
        for id, sector in u.getAll(*sql):
            if id in self.hashI:
                hash = self.hashI[id] 
                if sector in valors:
                    self.censal[hash] = valors[sector]
    
    def get_tractaments(self):
        """tractaments"""
        u.printTime("tractaments post")
        self.tracta_post = {}
        sql = """select id_cip, ppfmc_atccodi, date_format(ppfmc_pmc_data_ini,'%Y%m%d'), date_format(ppfmc_data_fi,'%Y%m%d') 
                    from tractaments
                    where date_format(ppfmc_pmc_data_ini,'%Y%m%d') >'20200301'  and
                    ppfmc_atccodi in ('P01BA02', 'S01AA26', 'J01FA10',' R03CC52', 'R03AK13','R03AL02','R03AC02','R03AK04','R03CC02')"""
        for id, atc, ini, fi in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id]
            if hash in self.hashI:
                idcovid = self.hashI[hash]
                if idcovid in self.covids:
                    data_cas = self.covids[idcovid]
                    if data_cas <= ini:
                        desc_atc = cat_tract[atc]
                        if (idcovid, desc_atc) in self.tracta_post:
                            dat_ant =  self.tracta_post[(idcovid, desc_atc)]['ini']
                            if ini < dat_ant:
                                self.tracta_post[(idcovid, desc_atc)] = {'ini': ini, 'fi': fi}
                        else:
                            self.tracta_post[(idcovid, desc_atc)] = {'ini': ini, 'fi': fi}
   
    def get_ingres(self):
        """ingressos"""
        u.printTime("ingres")
        dx_ingres = []
        desc_dx = {}
        self.ingres_urg = {}
        for (dx, desc, tip) in u.readCSV(dx_file, sep='@'):
            dx_ingres.append(dx)
            desc_dx[dx] = desc
        in_crit = tuple(dx_ingres)
        
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (u_motiu in {0} or u_dd1 in {0} or u_dd2 in {0} or u_dd3 in {0} or u_dd4 in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315'""".format(in_crit)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
        dx2 = "('J11','J12','J13','J14','J15','J16','J17','J18','J19','J20','J22','J40','J41','J42','J43','J44','J45','J47','J80','J84','J96')"
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (substr(u_motiu, 0,3) in {0} or substr(u_dd1, 0,3) in {0} or substr(u_dd2, 0,3) in {0} or substr(u_dd3, 0,3) in {0} or substr(u_dd4, 0,3) in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315'""".format(dx2)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
    
    
    def get_master(self):
        """obtenim dades de master"""
        u.printTime("master")
        self.upload = []
        sql = """
                select hash, edat, sexe, localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n,exitus, rx, pneumonia,
                embaras, viu_sol, gma_codi, gma_complexitat, ing_alta, ing_cod, ing_desti, fr_hta_no_control
                from sisap_covid_pac_master
                where estat in ('Possible', 'Confirmat')
              """
        for (hash, edat,sexe,localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus, rx, pneumonia,
                embaras, viu_sol, gma_codi, gma_complexitat, ing_alta, ing_cod, ing_desti, hta_noc) in u.getAll(sql, db): 
            medeaU = self.centres[up] if up in self.centres else None
            entrada = None
            sortida = None
            nac, chart_d, chartls_v = None, None, None
            hta, dm, mpoc = None, None, None
            asma, bc, ci = None, None, None
            mcv, ic, acfa, valv = None, None, None, None
            hepat, vhb, vhc, neo, mrc = None, None, None, None, None
            obes, vih, sida = None, None, None
            demencia, artrosi = None, None
            medea = None
            in_dbs = 0
            rcv_dat, rcv_val = None, None
            ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb = None, None,None,None,None,None,None
            ado, insulina, f_mpoc, antiag, aine, analg = None,None,None,None,None,None
            antidep, ansiol, antipsic, ulcer, anties, cortis = None,None,None,None,None,None
            epil, hipol, mhda = None, None, None
            hcq, azitro, salbutamol = None, None, None
            hcqI = self.tracta_post[(hash, 'Hidroxicloroquina')]['ini'] if (hash, 'Hidroxicloroquina') in self.tracta_post else None
            hcqF = self.tracta_post[(hash, 'Hidroxicloroquina')]['fi'] if (hash, 'Hidroxicloroquina') in self.tracta_post else None
            azitroI = self.tracta_post[(hash, 'Azitromicina')]['ini'] if (hash, 'Azitromicina') in self.tracta_post else None
            azitroF = self.tracta_post[(hash, 'Azitromicina')]['fi'] if (hash, 'Azitromicina') in self.tracta_post else None
            salbutamolI = self.tracta_post[(hash, 'Salbutamol')]['ini'] if (hash, 'Salbutamol') in self.tracta_post else None
            salbutamolF = self.tracta_post[(hash, 'Salbutamol')]['fi'] if (hash, 'Salbutamol') in self.tracta_post else None
            ingres_data = self.ingres_urg[hash]['dat'] if hash in self.ingres_urg else None
            ingres_cod = self.ingres_urg[hash]['dx'] if hash in self.ingres_urg else None
            if hash in self.censal:
                medea = self.censal[hash]
            if hash in self.pac_res:
                resi = self.pac_res[hash]['res']
                entrada = self.pac_res[hash]['entrada']
                sortida = self.pac_res[hash]['sortida']
            if hash in self.in_dbs_true:
                in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                nac = self.factors[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors else None
                chartls_v = self.factors[(hash, "CHARTLSTON_V")] if (hash, "CHARTLSTON_V") in self.factors else None
                chartls_d = self.factors[(hash, "CHARTLSTON_D")] if (hash, "CHARTLSTON_D") in self.factors else None
                hta = self.factors[(hash, "hta")] if (hash, "hta") in self.factors else None
                dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                mpoc = self.factors[(hash, "mpoc")] if (hash, "mpoc") in self.factors else None
                asma = self.factors[(hash, "asma")] if (hash, "asma") in self.factors else None
                bc = self.factors[(hash, "bc")] if (hash, "bc") in self.factors else None
                ci = self.factors[(hash, "ci")] if (hash, "ci") in self.factors else None
                mcv = self.factors[(hash, "mcv")] if (hash, "mcv") in self.factors else None
                ic = self.factors[(hash, "ic")] if (hash, "ic") in self.factors else None
                acfa = self.factors[(hash, "acfa")] if (hash, "acfa") in self.factors else None
                valv = self.factors[(hash, "valv")] if (hash, "valv") in self.factors else None
                hepat = self.factors[(hash, "hepat")] if (hash, "hepat") in self.factors else None
                vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                vhc = self.factors[(hash, "vhc")] if (hash, "vhc") in self.factors else None
                neo = self.factors[(hash, "neo")] if (hash, "neo") in self.factors else None
                mrc = self.factors[(hash, "mrc")] if (hash, "mrc") in self.factors else None
                obes = self.factors[(hash, "obes")] if (hash, "obes") in self.factors else None
                vih = self.factors[(hash, "vih")] if (hash, "vih") in self.factors else None
                sida = self.factors[(hash, "sida")] if (hash, "sida") in self.factors else None
                demencia = self.factors[(hash, "demencia")] if (hash, "demencia") in self.factors else None
                artrosi = self.factors[(hash, "artrosi")] if (hash, "artrosi") in self.factors else None
                rcv_dat = self.factors[(hash, "rcv_dat")] if (hash, "rcv_dat") in self.factors else None
                rcv_val = self.factors[(hash, "rcv_val")] if (hash, "rcv_val") in self.factors else None
                ieca = self.factors[(hash, "ieca")] if (hash, "ieca") in self.factors else None
                calcioa = self.factors[(hash, "calcioa")] if (hash, "calcioa") in self.factors else None
                betb = self.factors[(hash, "betb")] if (hash, "betb") in self.factors else None
                diur = self.factors[(hash, "diur")] if (hash, "diur") in self.factors else None
                alfab = self.factors[(hash, "alfab")] if (hash, "alfab") in self.factors else None
                hta_alt = self.factors[(hash, "hta_alt")] if (hash, "hta_alt") in self.factors else None
                hta_comb = self.factors[(hash, "hta_comb")] if (hash, "hta_comb") in self.factors else None
                ado = self.factors[(hash, "ado")] if (hash, "ado") in self.factors else None
                insulina = self.factors[(hash, "insulina")] if (hash, "insulina") in self.factors else None
                f_mpoc = self.factors[(hash, "f_mpoc")] if (hash, "f_mpoc") in self.factors else None
                antiag = self.factors[(hash, "antiag")] if (hash, "antiag") in self.factors else None
                aine = self.factors[(hash, "aine")] if (hash, "aine") in self.factors else None
                analg = self.factors[(hash, "analg")] if (hash, "analg") in self.factors else None
                antidep = self.factors[(hash, "antidep")] if (hash, "antidep") in self.factors else None
                ansiol = self.factors[(hash, "ansiol")] if (hash, "ansiol") in self.factors else None
                antipsic = self.factors[(hash, "antipsic")] if (hash, "antipsic") in self.factors else None
                ulcer = self.factors[(hash, "ulcer")] if (hash, "ulcer") in self.factors else None
                anties = self.factors[(hash, "anties")] if (hash, "anties") in self.factors else None
                cortis = self.factors[(hash, "cortis")] if (hash, "cortis") in self.factors else None
                epil = self.factors[(hash, "epil")] if (hash, "epil") in self.factors else None
                hipol = self.factors[(hash, "hipol")] if (hash, "hipol") in self.factors else None
                mhda = self.factors[(hash, "mhda")] if (hash, "mhda") in self.factors else None
            else:
                in_dbs = self.factors19[(hash, "in_dbs")]  if (hash, "in_dbs") in self.factors19 else 0
                nac = self.factors19[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors19 else None
                chartls_v = self.factors19[(hash, "CHARTLSTON_V")] if (hash, "CHARTLSTON_V") in self.factors19 else None
                chartls_d = self.factors19[(hash, "CHARTLSTON_D")] if (hash, "CHARTLSTON_D") in self.factors19 else None
                embaras = self.factors19[(hash, "embaras")] if (hash, "embaras") in self.factors19 else embaras
                viu_sol = self.factors19[(hash, "sol")] if (hash, "sol") in self.factors19 else viu_sol
                gma_codi = self.factors19[(hash, "gma_c")] if (hash, "gma_c") in self.factors19 else gma_codi
                gma_complexitat = self.factors19[(hash, "gma_cmplx")] if (hash, "gma_cmplx") in self.factors19 else gma_complexitat
                hta = self.factors19[(hash, "hta")] if (hash, "hta") in self.factors19 else None
                dm = self.factors19[(hash, "dm")] if (hash, "dm") in self.factors19 else None
                mpoc = self.factors19[(hash, "mpoc")] if (hash, "mpoc") in self.factors19 else None
                asma = self.factors19[(hash, "asma")] if (hash, "asma") in self.factors19 else None
                bc = self.factors19[(hash, "bc")] if (hash, "bc") in self.factors19 else None
                ci = self.factors19[(hash, "ci")] if (hash, "ci") in self.factors19 else None
                mcv = self.factors19[(hash, "mcv")] if (hash, "mcv") in self.factors19 else None
                ic = self.factors19[(hash, "ic")] if (hash, "ic") in self.factors19 else None
                acfa = self.factors19[(hash, "acfa")] if (hash, "acfa") in self.factors19 else None
                valv = self.factors19[(hash, "valv")] if (hash, "valv") in self.factors19 else None
                hepat = self.factors19[(hash, "hepat")] if (hash, "hepat") in self.factors19 else None
                vhb = self.factors19[(hash, "vhb")] if (hash, "vhb") in self.factors19 else None
                vhc = self.factors19[(hash, "vhc")] if (hash, "vhc") in self.factors19 else None
                neo = self.factors19[(hash, "neo")] if (hash, "neo") in self.factors19 else None
                mrc = self.factors19[(hash, "mrc")] if (hash, "mrc") in self.factors19 else None
                obes = self.factors19[(hash, "obes")] if (hash, "obes") in self.factors19 else None
                vih = self.factors19[(hash, "vih")] if (hash, "vih") in self.factors19 else None
                sida = self.factors19[(hash, "sida")] if (hash, "sida") in self.factors19 else None
                demencia = self.factors19[(hash, "demencia")] if (hash, "demencia") in self.factors19 else None
                artrosi = self.factors19[(hash, "artrosi")] if (hash, "artrosi") in self.factors19 else None
                rcv_dat = self.factors19[(hash, "rcv_dat")] if (hash, "rcv_dat") in self.factors19 else None
                rcv_val = self.factors19[(hash, "rcv_val")] if (hash, "rcv_val") in self.factors19 else None
                ieca = self.factors19[(hash, "ieca")] if (hash, "ieca") in self.factors19 else None
                calcioa = self.factors19[(hash, "calcioa")] if (hash, "calcioa") in self.factors19 else None
                betb = self.factors19[(hash, "betb")] if (hash, "betb") in self.factors19 else None
                diur = self.factors19[(hash, "diur")] if (hash, "diur") in self.factors19 else None
                alfab = self.factors19[(hash, "alfab")] if (hash, "alfab") in self.factors19 else None
                hta_alt = self.factors19[(hash, "hta_alt")] if (hash, "hta_alt") in self.factors19 else None
                hta_comb = self.factors19[(hash, "hta_comb")] if (hash, "hta_comb") in self.factors19 else None
                ado = self.factors19[(hash, "ado")] if (hash, "ado") in self.factors19 else None
                insulina = self.factors19[(hash, "insulina")] if (hash, "insulina") in self.factors19 else None
                f_mpoc = self.factors19[(hash, "f_mpoc")] if (hash, "f_mpoc") in self.factors19 else None
                antiag = self.factors19[(hash, "antiag")] if (hash, "antiag") in self.factors19 else None
                aine = self.factors19[(hash, "aine")] if (hash, "aine") in self.factors19 else None
                analg = self.factors19[(hash, "analg")] if (hash, "analg") in self.factors19 else None
                antidep = self.factors19[(hash, "antidep")] if (hash, "antidep") in self.factors19 else None
                ansiol = self.factors19[(hash, "ansiol")] if (hash, "ansiol") in self.factors19 else None
                antipsic = self.factors19[(hash, "antipsic")] if (hash, "antipsic") in self.factors19 else None
                ulcer = self.factors19[(hash, "ulcer")] if (hash, "ulcer") in self.factors19 else None
                anties = self.factors19[(hash, "anties")] if (hash, "anties") in self.factors19 else None
                cortis = self.factors19[(hash, "cortis")] if (hash, "cortis") in self.factors19 else None
                epil = self.factors19[(hash, "epil")]  if (hash, "epil") in self.factors19 else None
                hipol = self.factors19[(hash, "hipol")] if (hash, "hipol") in self.factors19 else None
                mhda = self.factors19[(hash, "mhda")] if (hash, "mhda") in self.factors19 else None
            self.upload.append([hash, edat,sexe, nac, localitat, abs, up, medea, medeaU, resi, entrada, sortida, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus, rx, pneumonia,
                embaras, viu_sol, gma_codi, gma_complexitat,  chartls_d, chartls_v, rcv_dat, rcv_val, ingres_data, ingres_cod, ing_alta, ing_cod, ing_desti,
                 in_dbs, hta, hta_noc, dm, mpoc,asma, bc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                 demencia, artrosi,
                 ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                 ado, insulina, f_mpoc, antiag, aine, analg,
                 antidep, ansiol, antipsic, ulcer, anties, cortis,
                 epil, hipol, mhda,
                 hcqI, hcqF, azitroI, azitroF, salbutamolI, salbutamolF])
                
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("hash varchar2(40)", "edat int",
            "sexe varchar2(1)", "nacionalitat varchar2(100)", "localitat varchar2(7)", "abs int",
            "up varchar2(5)", "medea number(6, 3)", "medea_up varchar2(10)","resi varchar2(10)", "entrada_resi date", "sortida_resi date", "hotel varchar2(255)",
             "estat varchar2(16)", "origen_d varchar2(40)","cas_data date",
            "dx_cod varchar2(16)", "dx_dde date", "dx_dba date",
            "dx_sit varchar2(16)",  "pcr_data date", "pcr_res varchar2(30)", "pcr_n int","pcr_ia_n int", "test_n int", "test_ia_n int",
            "exitus date", "rx date",
            "pneumonia date", "embaras int", "viu_sol int",
            "gma_codi varchar2(3)", "gma_complexitat number(6, 3)",
            "chartlston_d date", "chartlston_v int",  "rcv_dat date", "rcv_val number(6, 2)", 
            "ingres_data date", "ingres_cod varchar2(500)", "ing_alta date","ing_cod varchar2(16)", "ing_desti varchar2(64)",
            "in_dbs int",
            "FR_hta date", "FR_hta_no_control int", "FR_dm date", "FR_mpoc date", "FR_asma date", "FR_bc date", "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date",
            "F_HTA_IECA_ARA2 varchar2(500)", "F_HTA_CALCIOA varchar2(500)","F_HTA_BETABLOQ varchar2(500)","F_HTA_DIURETICS varchar2(500)",
            "F_HTA_ALFABLOQ varchar2(500)", "F_HTA_ALTRES varchar2(500)", "F_HTA_COMBINACIONS varchar2(500)",
             "F_DIAB_ADO varchar2(500)", "F_DIAB_INSULINA varchar2(500)", "F_MPOC_ASMA varchar2(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar2(500)", "F_AINE varchar2(500)", "F_ANALGESICS varchar2(500)",
             "F_ANTIDEPRESSIUS varchar2(500)", "F_ANSIO_HIPN varchar2(500)", "F_ANTIPISCOTICS varchar2(500)",
             "F_ANTIULCEROSOS varchar2(500)", "F_ANTIESP_URI varchar2(500)", "F_CORTIC_SISTEM varchar2(500)",
             "F_ANTIEPILEPTICS varchar2(500)", "F_HIPOLIPEMIANTS varchar2(500)", "F_MHDA_VIH varchar2(500)",
             "F_POST_HCQ_I int", "F_POST_HCQ_F int", "F_POST_AZITRO_I int", "F_POST_AZITRO_F int", "F_POST_SALBU_I int", "F_POST_SALBU_F int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    article_dani()
    
    u.printTime("Final")
