# -*- coding: utf8 -*-

"""
Refredats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'sisap_neo_pell_hist'
db = 'permanent'

dx_file =  "neo_pell.txt"

class melanoma(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_assignada()
        self.get_neos()
        self.export()

    def get_assignada(self):
        """."""
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement from assignada'
        for id, sexe, naix in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    
    def get_neos(self):
        """."""
        u.printTime("problemes")
        dx_codis = []
        tip_dx = {}
        self.skin_cancer = c.Counter()
        for (dx,  tip) in u.readCSV(dx_file, sep='@'):
            dx_codis.append(dx)
            tip_dx[dx] = tip
        in_crit = tuple(dx_codis)
        
        sql = "select id_cip, pr_dde, date_format(pr_dde,'%Y%m'),  pr_cod_ps, pr_up, count(*) \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  \
                            and extract(year_month from pr_dde) >'201312' and \
                            extract(year_month from pr_dde) <'202010' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde, date_format(pr_dde,'%Y%m'), pr_cod_ps, pr_up".format(in_crit)
        for id, dat, periode,codi, up, recompte in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            tipus = tip_dx[codi]
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None
            if ed != None:
                self.skin_cancer[(periode, up, u.ageConverter(ed,5), u.sexConverter(sexe), tipus)] += 1
      


    def export(self):
        """."""
        cols = ("periode varchar(6)", "up varchar(10)", "edat varchar(10)", "sexe varchar(10)", "tipus varchar(1)", "recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (periode, up, edat, sexe, tipus), n in self.skin_cancer.items():
            n = int (n)
            upload.append([periode, up, edat, sexe, tipus, n])
        u.listToTable(upload, tb, db)
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    melanoma()
    
    u.printTime("Final")                 
