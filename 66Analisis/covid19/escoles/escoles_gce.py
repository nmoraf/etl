# -*- coding: utf8 -*-

"""
escoles
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'sisap_escoles_gce_des02'
tb_res = 'sisap_escoles_resul_des02'
db = 'permanent'


class escoles(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_perfil()
        self.get_sense_cip()
        self.get_up()
        self.get_grups()
        self.get_cursos()
        self.get_positius()
        self.get_nens()
        self.get_resultats()
        self.export()
        
    def get_perfil(self):
        """."""
        u.printTime("perfils")
        self.perfils = {}
        sql = "select id, nom from dwsisap_escola.PERSONES_PERFIL"
        for id, nom in u.getAll(sql, 'exadata'):
            self.perfils[id] = nom
    
    def get_sense_cip(self):
        """."""
        self.no_cip = {}
        sql = "select  persona_id from DWSISAP_ESCOLA.CLINICA_ESTATPERSONA  where tipus_id=4"
        for id, in u.getAll(sql, 'exadata'):
            self.no_cip[id] = True
    
    def get_up(self):
        """."""
        self.abs_up = {}
        sql = "select a.id, a.abs_id, b.codi_up from dwsisap_escola.CENTRES_CENTRE a inner join dwsisap_escola.CENTRES_abs b on a.abs_id=b.id"
        for id, abs, up in u.getAll(sql, 'exadata'):
            self.abs_up[id] = {'abs': abs, 'up': up}
    
    def get_grups(self):
        """."""
        u.printTime("grups")
        self.alumnes = c.defaultdict(lambda: c.defaultdict(set))
        sql = "select curs_id, gce_id,persona_id from dwsisap_escola.grups_alumne"
        for curs, gce_id, person_id in u.getAll(sql, 'exadata'):
            self.alumnes[person_id][curs].add(gce_id)
            
        self.tipus_gce = {}
        sql = "select id, nom, centre_id from dwsisap_escola.grups_gce"
        for gce, nom, centre in u.getAll(sql, 'exadata'):
            self.tipus_gce[gce] = {'nom': nom, 'centre': centre}
        
    def get_cursos(self):
        """Nivell"""
        u.printTime("Nivell")
        self.nivells = {}
        sql = """
                select a.id, d.codi from dwsisap_escola.GRUPS_curs a
                inner join dwsisap_escola.grups_nivell b on a.niVell_id=b.id 
                inner join dwsisap_escola.grups_ensenyament c on b.ENSENYAMENT_ID=c.id 
                inner join dwsisap_escola.grups_tipusensenyament d
                on c.TIPUS_ID=d.id
            """
        for curs, nivell in u.getAll(sql, 'exadata'):
            self.nivells[curs] = nivell
            
    def get_positius(self):
        """."""
        u.printTime("positius")
        self.positius = {}
        sql = "select persona_id, to_char(data_prova,'YYYY-MM-DD'), to_char(data_resultat,'YYYY-MM-DD') from dwsisap_escola.clinica_positiu"
        for id, datap, dataR in u.getAll(sql, 'exadata'):
            self.positius[id] = {'datap': datap, 'dataR': dataR}
            
    def get_nens(self):
        """."""
        u.printTime("nens")
        self.dades = []
        sql = "select id, to_char(data_naixement,'YYYY-MM-DD'), sexe_id, perfil_id from DWSISAP_ESCOLA.PERSONES_PERSONA"
        for id, naix, sex, perfil in u.getAll(sql, 'exadata'):
            nivell, gce_id, nom_gce, centre, abs, up = None, None, None, None, None, None
            positiu = 1 if id in self.positius else 0
            datap = self.positius[id]['datap'] if id in self.positius else None
            dataR = self.positius[id]['dataR'] if id in self.positius else None
            perfil_desc = self.perfils[perfil] if perfil in self.perfils else 'Desconegut'
            sense_cip = 1 if id in self.no_cip else 0
            if id in self.alumnes:
                for curs in self.alumnes[id]:
                    nivell = self.nivells[curs] if curs in self.nivells else None
                    for gce in self.alumnes[id][curs]:
                        gce_id = gce
                        nom_gce = self.tipus_gce[gce_id]['nom'] if gce_id in self.tipus_gce else None
                        centre = self.tipus_gce[gce_id]['centre'] if gce_id in self.tipus_gce else None 
                        abs = self.abs_up[centre]['abs'] if centre in self.abs_up else None
                        up = self.abs_up[centre]['up'] if centre in self.abs_up else None
                        self.dades.append([id, sense_cip, naix, sex,perfil_desc, nivell, gce_id, centre, abs, up, positiu, datap, dataR])
            else:
                self.dades.append([id, sense_cip, naix, sex,perfil_desc, nivell, gce_id, centre, abs, up, positiu, datap, dataR])
            
    def get_resultats(self):
        """."""
        u.printTime("proves")
        self.upload_result = []
        sql = """select to_char(y.dat, 'YYYY-MM-DD'), x.id, y.resultat 
                  from DWSISAP_ESCOLA.persones_persona x, 
                       (select cip, data_validacio_mostra dat, resultat_codi resultat 
                          from dwaquas.covid19_aut_resul_pcr 
                          where data_validacio_mostra is not null
                          and resultat_codi in (0, 1, 3, 4) 
                          union all 
                          select cip, data_prova, resultat_codi 
                          from dwaquas.covid19_aut_resul_antigen 
                          where resultat_codi in (0, 1)) y
                     where x.cip = y.cip"""
        for dat, id, result in u.getAll(sql, 'exadata'):
            self.upload_result.append([id, dat, result])

    def export(self):
        """."""
        u.printTime("export")
        cols = ("id varchar(100)", "sense_cip int", "naix varchar(40)", "sexe varchar(10)", "perfil varchar(100)", "nivell varchar(100)", "gce_id varchar(40)",  "centre_id varchar(100)", 
        "abs varchar(10)", "up varchar(10)", "positiu int", "data_prova varchar(40)", "data_resultat varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.dades, tb, db)
        
        cols = ("id varchar(100)", "data_prova varchar(40)", "resultat int")
        u.createTable(tb_res, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload_result, tb_res, db)

        
    
    
    
if __name__ == '__main__':
    u.printTime("Inici")
     
    escoles()
    
    u.printTime("Final")      