# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

resta = d.datetime.now().hour < 21
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)

db = 'redics'
tb = 'sisap_covid_rec_socioec6'

dx_file =  "dx_covid.txt"


class vpp(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_resis()
        self.get_ingres()
        self.get_master()
        self.get_ene()
        self.get_taxes()
        self.export_data()
        self.dona_grants()

    def get_resis(self):
        """Agafem pacients que hagin estat algun cop en residències"""
        u.printTime("resis")
        self.pac_res = {}
        sql = "select hash, residencia, entrada, sortida from sisap_covid_res_master"
        for hash, res, entrada, sortida in u.getAll(sql, db):
            self.pac_res[hash] = {'res': res, 'entrada': entrada, 'sortida': sortida}

    def get_ingres(self):
        """ingressos"""
        u.printTime("ingres")
        dx_ingres = []
        desc_dx = {}
        self.ingres_urg = {}
        for (dx, desc, tip) in u.readCSV(dx_file, sep='@'):
            dx_ingres.append(dx)
            desc_dx[dx] = desc
        in_crit = tuple(dx_ingres)
        
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (u_motiu in {0} or u_dd1 in {0} or u_dd2 in {0} or u_dd3 in {0} or u_dd4 in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315' and to_char(u_data, 'YYYYMMDD')<'20200511'""".format(in_crit)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
        dx2 = "('J11','J12','J13','J14','J15','J16','J17','J18','J19','J20','J22','J40','J41','J42','J43','J44','J45','J47','J80','J84','J96')"
        sql = """select hash, u_data,  u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (substr(u_motiu, 0,3) in {0} or substr(u_dd1, 0,3) in {0} or substr(u_dd2, 0,3) in {0} or substr(u_dd3, 0,3) in {0} or substr(u_dd4, 0,3) in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315' and to_char(u_data, 'YYYYMMDD')<'20200511'""".format(dx2)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
   
    
    def get_master(self):
        """obtenim dades de master"""
        u.printTime("master")
        self.taxes = c.Counter()
        sql = """
                select hash, edat, sexe, estat,up, cas_data, pcr_data,
                 exitus,to_char(exitus, 'YYYYMMDD'),pcr_n+pcr_ia_n+test_n+test_ia_n, ing_alta, cas_data, to_char(cas_data, 'YYYYMMDD')
                from sisap_covid_pac_master
                where estat in ('Possible', 'Confirmat') and resi is null
              """
        for (hash, edat,sexe,estat, up, cas_data, pcr, exitus, exit, n_pcr, alta, data, cas_data) in u.getAll(sql, db): 
            if hash in self.pac_res:
                continue
            else:
                if cas_data < '20200511':
                    edat = int(edat) 
                    ed_grup = u.ageConverter(edat,5)
                    self.taxes[(up, edat, ed_grup, sexe, estat)] +=1
                    self.taxes[(up, edat, ed_grup,  sexe, 'n_pcr')] += n_pcr
                    if alta != None:
                        self.taxes[(up, edat, ed_grup,  sexe, 'alta')] +=1
                    if exitus != None:
                        if exit < '20200511':
                            self.taxes[(up, edat, ed_grup,  sexe, 'exitus')] +=1
                    if pcr != None:
                        self.taxes[(up, edat, ed_grup,  sexe, 'pcr')] +=1
                    if hash in self.ingres_urg:
                        ingres_data = self.ingres_urg[hash]['dat']
                        self.taxes[(up, edat, ed_grup,  sexe, 'ingres')] +=1

    def get_ene(self):
        """."""
        sql = "select hash, up, edat, sexe, resultat,igg from sisap_covid_ene_master where ecap=1 and resultat is not null and edat is not null"
        for hash, up, edat,sexe, resultat, igg in u.getAll(sql, db):
            edat = int(edat)
            ed_grup = u.ageConverter(edat,5)
            self.taxes[(up, edat, ed_grup, sexe, 'resultat')] += resultat
            self.taxes[(up, edat, ed_grup, sexe, 'igg')] += igg
            self.taxes[(up, edat, ed_grup, sexe, 'den')] += 1
    
    def get_taxes(self):
        """."""
        pob = c.Counter()
        sql = "select up, edat, sexe, recompte from sisap_covid_agr_poblacio"
        for up, edat, sexe, rec in u.getAll(sql, db):
            edat = int(edat)
            ed_grup = u.ageConverter(edat,5)
            pob[(up, edat, ed_grup, sexe)] += rec
        
        self.upload = []
        for (up, edat,ed_grup,  sexe), n in pob.items():
            confirmats = self.taxes[(up, edat,ed_grup,  sexe, 'Confirmat')] if (up, edat,ed_grup,  sexe, 'Confirmat') in self.taxes else 0
            possibles = self.taxes[(up, edat,ed_grup,  sexe, 'Possible')] if (up, edat,ed_grup,  sexe, 'Possible') in self.taxes else 0
            totals = confirmats + possibles
            ingres = self.taxes[(up, edat,ed_grup,  sexe, 'ingres')] if (up, edat, ed_grup, sexe, 'ingres') in self.taxes else 0
            alta = self.taxes[(up, edat, ed_grup, sexe, 'alta')] if (up, edat,ed_grup,  sexe, 'alta') in self.taxes else 0
            exitus = self.taxes[(up, edat,ed_grup,  sexe, 'exitus')] if (up, edat, ed_grup, sexe, 'exitus') in self.taxes else 0
            pcr = self.taxes[(up, edat,ed_grup,  sexe, 'pcr')] if (up, edat, ed_grup, sexe, 'pcr') in self.taxes else 0
            pcr_n = self.taxes[(up, edat,ed_grup,  sexe, 'n_pcr')] if (up, edat, ed_grup, sexe, 'n_pcr') in self.taxes else 0
            ene_r = self.taxes[(up, edat, ed_grup, sexe, 'resultat')] if (up, edat, ed_grup, sexe, 'resultat') in self.taxes else 0
            ene_igg = self.taxes[(up, edat,ed_grup,  sexe, 'igg')] if (up, edat, ed_grup, sexe, 'igg') in self.taxes else 0
            den = self.taxes[(up, edat, ed_grup, sexe, 'den')] if (up, edat, ed_grup, sexe, 'den') in self.taxes else 0
            self.upload.append([up, edat, ed_grup, sexe, confirmats, possibles, totals, ene_igg, ene_r,den,  pcr, pcr_n, ingres, alta, exitus, n])
                
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("up varchar2(5)", "edat int", "edat_grup varchar2(20)",
            "sexe varchar2(1)",   "confirmats int",
              "possibles int",  "totals int", "ENE_IGG int", "ENE_resultat int", "ENE_den int", "pcr int", "pcr_N int","ingressos int", "alta int", "exitus int",  "poblacio int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)        


if __name__ == '__main__':
    u.printTime("Inici")
    
    vpp()
    
    u.printTime("Final")