# -*- coding: utf8 -*-

"""
Refredats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


def get_grup(edat):
    """."""
    nivell = '0-2' if edat < 3 else  '3-7' if edat <8 else '8-14' if edat < 15 else '15-44' if edat <45 else '45-64' if edat <65 else '65-74' if edat <75 else '>75' if edat >74 else 'error'
    return (nivell)

tb = 'sisap_pac_visites_2anys'
db = 'permanent'
file = 'frequentacio_visites.csv'



class Visites_edat(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_serveis()
        self.get_centres()
        self.get_assignada()
        self.get_visites()
        self.export()

    
    def get_serveis(self):
        """."""
        u.printTime("serveis")
        self.serveis = {}
        sql = """select sector, cod, homol 
                from sisap_covid_cat_serveis"""
        for sec, cod, homol in u.getAll(sql, 'redics'):
            self.serveis[(sec, cod)] = homol
    
    def get_centres(self):
        """EAP ICS."""
        u.printTime("centres")
        sql = ("select scs_codi, amb_desc \
                from cat_centres ", "nodrizas")
        self.centres = {up: ambit for (up, ambit) in u.getAll(*sql)}
    
    def get_assignada(self):
        """."""
        u.printTime("pob")
        self.pob = {}
        self.assignades = c.Counter()
        sql = 'select id_cip_sec, usua_sexe, usua_data_naixement from assignada'
        for id, sexe, naix in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'edat': naix}

            
    
    def get_visites(self):
        """visites dos anys"""
        u.printTime("Visites")
        self.recomptes = c.Counter()
        self.taula_rec = c.Counter()
        sql ="""select id_cip_sec, codi_sector, extract(year_month from visi_data_visita),  visi_data_visita, visi_up,visi_servei_codi_servei,  
                        if(visi_etiqueta='ECTA', '9Ec', if(visi_etiqueta='VITA','9Ev', if(visi_tipus_visita like '9%', visi_tipus_visita, 'ALTRES'))),  if(visi_modul_codi_modul = '4CW', 1, 0)
                from visites2 
                where visi_situacio_visita = 'R' and visi_data_baixa=0 """
        for id, sec, periode, data, up, servei, tipus, es_4cW in u.getAll(sql, 'import'):
            if up in self.centres:
                ambit = self.centres[up]
                sexe = self.pob[id]['sexe'] if id in self.pob else '-1'
                naix = self.pob[id]['edat'] if id in self.pob else '-1'
                try:
                    edat = u.yearsBetween(naix, data)
                    grup = get_grup(edat)
                except:
                    grup = '-1'
                servei_homol = self.serveis[(sec, servei)]
                self.recomptes[(periode, ambit, grup, sexe, servei_homol, tipus, es_4cW)] += 1
                self.taula_rec[(periode, up, grup, sexe, servei_homol, tipus, es_4cW)] += 1
       
    def export(self):
        """."""
        u.printTime("export")
        upload = []
        for (periode, ambit, edat, sexe, servei_homol, tipus, es_4cW), n in self.recomptes.items():
            upload.append([periode, ambit, edat, sexe, servei_homol, tipus, es_4cW, n])
        u.writeCSV(file, upload, sep='@')
        
        upload = []
        for (periode, up, grup, sexe, servei_homol, tipus, es_4cW), n in self.taula_rec.items():
            upload.append([periode, up, grup, sexe, servei_homol, tipus, es_4cW, n])
        columns = ["periode varchar(26)", "up varchar2(5)", "grup varchar2 (10)", "sexe varchar2(5)", "servei varchar2(10)", "tipus_visita varchar2(10)", "es_4cW int", "recompte int"]
        u.createTable(tb, "({})".format(", ".join(columns)), 'redics', rm=True)
        u.listToTable(upload, tb, 'redics')
        
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)     
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Visites_edat()
    
    u.printTime("Final")          