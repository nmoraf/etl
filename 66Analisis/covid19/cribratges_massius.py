# coding: utf8


import sisapUtils as u

from collections import Counter
import urllib as w
import os


file = u.tempFolder + "cribratges_massius.txt"
tb = "sisap_covid_tmp_massius"
db = 'redics'


costos = {'juny': 93, 'juliol': 93, 'agost': 93, 'setembre': 93, 'octubre': 75, 'novembre': 75}


class massius(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_municipi()
        self.get_dades_7d()
        self.get_epi()
        self.get_dades()
        self.dona_grants()
        
   
    def get_centres(self):
        """."""
        u.printTime("Centres")
        self.centres = {}
        
        sql = "select a.abs_cod, a.eap, b.up_des from preduffa.sisap_covid_cat_abs a inner join preduffa.sisap_covid_cat_up b on a.eap=b.up_cod"
        for abs, up, desc in u.getAll(sql, db):
            self.centres[str(up)] = {'abs': abs, 'desc': desc}
    
    def get_municipi(self):
        """.Agafem codis de municipis"""
        u.printTime("municipi")
        self.municipis = {}
        self.descripcions = {}
        sql = """select  localitat_c, comarca_c, provincia_c, municipi_c, municipi from sisap_covid_cat_localitat"""
        for  loc, com, prov, municipi, desc in u.getAll(sql, db):
            self.municipis[loc] = municipi
            self.descripcions[municipi] = desc
    

    def get_dades_7d(self):
        """*"""
        u.printTime("dades 7 dies")
        self.dades7_M, self.dades7_A = Counter(), Counter()
        sql = """SELECT localitat, abs, to_char(data,'DD-MM-YYYY'), sum(CASOS_CONFIRMAT),  sum(PCR_NO_CAS_POS), SUM(PCR_NO_CAS),sum(DX_COVID)
                FROM preduffa.sisap_covid_web_master_setm
                GROUP BY localitat, abs, data"""
        for loc, abs, data, casos, num, den, dx in u.getAll(sql, db):
            if loc in self.municipis:
                municipi = self.municipis[loc]
                self.dades7_M[(data, municipi, 'casos')] += casos
                self.dades7_M[(data, municipi, 'num')] += num
                self.dades7_M[(data, municipi, 'den')] += den
                self.dades7_M[(data, municipi, 'dx')] += dx
            self.dades7_A[(data, abs, 'casos')] += casos
            self.dades7_A[(data, abs, 'num')] += num
            self.dades7_A[(data, abs, 'den')] += den
            self.dades7_A[(data, abs, 'dx')] += dx
        
    
    def get_epi(self):
        """."""
        u.printTime("Epi: rt, ia14, epg")
        self.epi = {}
        
        sql = """select to_char(data,'DD-MM-YYYY'), nivell, codi, poblacio, r0_confirmat_m, ia14_confirmat, iepg_confirmat, r0_DX_COVID_m, ia14_DX_COVID, iepg_DX_COVID 
                from SISAP_COVID_agr_risc_ili"""
        for data, nivell, codi, pob, r0, ia14, iepg, r0_dx, ia14_dx, iepg_dx in u.getAll(sql, db):
            self.epi[(data, nivell, codi)] = {'r0': r0, 'ia14': ia14, 'iepg': iepg, 'pob': pob, 'r0_dx': r0_dx, 'ia14_dx': ia14_dx, 'iepg_dx': iepg_dx}
  
    def get_dades(self):
        """."""
        u.printTime("dades")
        upload = []
        
        for (id, inici, mes, municipi, eap, br,  tipus, den, num, coment) in u.readCSV(file, sep="@"):
            cost_pcr = costos[mes]
            try:
                suma_costos = int(cost_pcr)*int(den)
            except:
                suma_costos = 0
            abs = self.centres[eap]['abs'] if eap in self.centres else None
            up_desc = self.centres[eap]['desc'] if eap in self.centres else None
            r0_m = self.epi[(inici, 'MUNICIPI', municipi)]['r0'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            ia14_m = self.epi[(inici, 'MUNICIPI', municipi)]['ia14'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            epg_m = self.epi[(inici, 'MUNICIPI', municipi)]['iepg'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            r0_dx_m = self.epi[(inici, 'MUNICIPI', municipi)]['r0_dx'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            ia14_dx_m = self.epi[(inici, 'MUNICIPI', municipi)]['ia14_dx'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            epg_dx_m = self.epi[(inici, 'MUNICIPI', municipi)]['iepg_dx'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            pob_m = self.epi[(inici, 'MUNICIPI', municipi)]['pob'] if (inici, 'MUNICIPI', municipi) in self.epi else None
            casos_m = self.dades7_M[(inici, municipi, 'casos')] if (inici, municipi, 'casos') in self.dades7_M else None
            num_m = self.dades7_M[(inici, municipi, 'num')] if (inici, municipi, 'num') in self.dades7_M else None
            den_m = self.dades7_M[(inici, municipi, 'den')] if (inici, municipi, 'den') in self.dades7_M else None
            dx_m = self.dades7_M[(inici, municipi, 'dx')] if (inici, municipi, 'dx') in self.dades7_M else None
            desc_municipi = self.descripcions[municipi] if municipi in self.descripcions else None
            
            r0_a = self.epi[(inici, 'ABS', abs)]['r0'] if (inici, 'ABS', abs) in self.epi else None
            ia14_a = self.epi[(inici, 'ABS', abs)]['ia14'] if (inici, 'ABS', abs) in self.epi else None
            epg_a = self.epi[(inici, 'ABS', abs)]['iepg'] if (inici, 'ABS', abs) in self.epi else None
            r0_dx_a = self.epi[(inici, 'ABS', abs)]['r0_dx'] if (inici, 'ABS', abs) in self.epi else None
            ia14_dx_a = self.epi[(inici, 'ABS', abs)]['ia14_dx'] if (inici, 'ABS', abs) in self.epi else None
            epg_dx_a = self.epi[(inici, 'ABS', abs)]['iepg_dx'] if (inici, 'ABS', abs) in self.epi else None
            pob_a = self.epi[(inici, 'ABS', abs)]['pob'] if (inici, 'ABS', abs) in self.epi else None
            casos_a = self.dades7_A[(inici, abs, 'casos')] if (inici, abs, 'casos') in self.dades7_A else None
            num_a = self.dades7_A[(inici, abs, 'num')]if (inici, abs, 'num') in self.dades7_A else None
            den_a = self.dades7_A[(inici, abs, 'den')] if (inici, abs, 'den') in self.dades7_A else None
            dx_a = self.dades7_A[(inici, abs, 'dx')] if (inici, abs, 'dx') in self.dades7_A else None
            upload.append([id, inici, mes, municipi, desc_municipi, pob_m, eap, up_desc, br, abs, pob_a, tipus, den, num, casos_m, num_m, den_m, r0_m, ia14_m, epg_m,
                            dx_m, r0_dx_m, ia14_dx_m, epg_dx_m,
                         casos_a, num_a, den_a, r0_a, ia14_a, epg_a,  
                        dx_a, r0_dx_a, ia14_dx_a, epg_dx_a, 
                        suma_costos,coment])
            
        cols = ("id int", "inici varchar2(40)", "mes varchar2(10)", "municipi varchar2(10)", "municipi_desc varchar2(200)", "pob_municipi int",
        "eap varchar2(10)", "eap_desc varchar2(300)", "codi_br varchar2(10)", "abs varchar2(10)", "pob_abs int", "tipus varchar2(100)", 
        "cribrats int", "positius int", "casos7_municipi int",  "num_pos_municipi int", "den_pos_municipi int",
        "rt_municipi number(20, 13)", "IA14_municipi number(20, 13)", "iEPG_municipi  number(20, 13)",
        "dx7_municipi int", "rt_dx_municipi number(20, 13)", "IA14_dx_municipi number(20, 13)", "iEPG_dx_municipi  number(20, 13)",
         "casos7_abs int", "num_pos_abs int", "den_pos_abs int", 
         "rt_abs number(20, 13)", "IA14_abs number(20, 13)", "iEPG_abs  number(20, 13)",
         "dx7_abs int","rt_dx_abs number(20, 13)", "IA14_dx_abs number(20, 13)", "iEPG_dx_abs  number(20, 13)", "cost_cribratge int",
        "comentari varchar2(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(upload, tb, db)
  
    def dona_grants(self):
        """."""
        u.printTime("grants")
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
  
if __name__ == '__main__':
    u.printTime("Inici")
    
    massius()
    
    u.printTime("Final")