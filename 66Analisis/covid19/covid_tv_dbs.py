# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

resta = d.datetime.now().hour < 21
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)

db = 'redics'
tb = 'sisap_covid_TV_dbs'

dx_file = 'tv_dx.txt'


class triple_v(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hash()
        self.get_hashos()
        self.get_indicadors_pedia()
        self.get_indicadors_adults()
        self.get_ps()
        self.get_exclusions()
        self.get_medea()
        self.get_master()
        self.get_dbs()
        self.export_data()
        
  
    def get_hash(self):
        """conversor de hash"""
        u.printTime("hash")
        self.hashI = {}
        sql = """ select hash, hash_ics
                    from sisap_covid_pac_id"""
        for hash,hashi in u.getAll(sql, 'redics'):
            self.hashI[hash] = hashi
    
    def get_hashos(self):
        """."""
        u.printTime("hash proces")
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11withjail"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}   

    def get_indicadors_pedia(self):
        """Obtenim indicadors de pedia"""
        u.printTime("EQA pedia")
        self.ind_pedia = {}
        sql = """select id_cip_sec, indicador, num 
                from mst_indicadors_pacient 
                where den=1 and excl=0 and indicador in ('EQA0702', 'EQA0715', 'EQA0716', 'EQA0717', 'EQA0712','EQA0707')"""
        for id, ind, num in u.getAll(sql, 'pedia'):
            hash = self.idcip_to_hash[id]
            self.ind_pedia[(id, ind)] = num
    
    def get_indicadors_adults(self):
        """Obtenim indicadors d adults"""
        u.printTime("EQA adults")
        self.ind_adults = {}
        sql = """select id_cip_sec, grup_codi, num 
                from mst_indicadors_pacient 
                where den=1  and ci=0 and clin=0 and excl=0 and grup_codi in ('EQA0308', 'EQA0309', 'EQA0312', 'EQA0501', 'EQA0502')"""
        for id, ind, num in u.getAll(sql, 'eqa_ind'):
            hash = self.idcip_to_hash[id]
            self.ind_adults[(id, ind)] = num
    
    def get_ps(self):
        """Obtenim dx de patologies"""
        self.dx_patologia = {}
        for (dx, grup) in u.readCSV(dx_file, sep='@'):
            dx_pato.append(dx)
            desc_dx[dx] = grup
        in_crit = tuple(dx_pato)
        sql = """select id_cip_sec, pr_dde, pr_cod_ps 
                from problemes
                where  pr_cod_o_ps='C' and pr_cod_ps in {0} and pr_data_baixa=0""".format(in_crit)
        for id, dde, ps in u.getAll(sql, 'import'):
            hash = self.idcip_to_hash[id]
            pato = desc_dx[ps]
            self.dx_patologia[(hash, pato)] = dde
            
    def get_exclusions(self):
        """exclusions vacunes"""
        self.exclusions = {}
        cataleg = {'DT' : {'E_DIFTERIA','E_TETANUS'},
                    'DTP': {'E_DIFTERIA','E_TETANUS','E_PERTUSSIS'},
                    'Td' : {'E_DIFTERIA','E_TETANUS'},
                    'TDP': {'E_DIFTERIA','E_TETANUS','E_PERTUSSIS'},
                    'T': {'E_TETANUS'},
                    'ROTAV': {'E_ROTAVIRUS'},
                    'MCC': {'E_MCC'},
                    'VARICE': {'E_VARICELA'},
                    'PO': {'E_POLIO'},
                    'HA+B': {'E_VHB', 'E_VHA'},
                    'HB': {'E_VHB'},
                    'HA': {'E_VHA'},
                    'XARAMP': {'E_XARAMPIO'},
                    'XRP': {'E_XARAMPIO', 'E_RUBEOLA', 'E_PAROTIDITIS'},
                    'RUBEOL': {'E_RUBEOLA'},
                    'PARODI': {'E_PAROTIDITIS'},
                    'PNC': {'E_PNC'},
                    'PNC10': {'E_PNC'},
                    'PNC13': {'E_PNC'},
                    'PNC7': {'E_PNC'},
                    'VPH2': {'E_VPH'},
                    'VPH4': {'E_VPH'},
                    'GRIP': {'E_GRIP'}
                    }
        sql = """select id_cip_sec, ief_cod_v, ief_exc
                from exclusions
                where ief_inc_exc='E' and ief_data_baixa=0"""
        for id, vac, motiu in u.getAll(sql, 'import'):
            hash = self.idcip_to_hash[id]
            if vac in cataleg:
                tips = cataleg[vac]
                for tipus in tips:
                    self.exclusions[(hash, tipus)] = motiu
            
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        self.censal = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              db)}
        sql = ("select usua_cip, sector_censal from md_poblacio", db)
        for id, sector in u.getAll(*sql):
            if sector in valors:
                self.censal[id] = valors[sector]
    
    def get_master(self):
        """."""
        u.printTime("master")
        self.covids = {}
        sql = "select hash, estat, cas_data, ing_alta, pneumonia, exitus from sisap_covid_pac_master where estat in ('Possible', 'Confirmat')"
        for hash, estat, casdata, alta, pneumonia, exitus in u.getAll(sql, db):
            if hash in self.hashI:
                hash2 = self.hashI[hash]
                self.covids[hash2] = {'estat': estat, 'casdata': casdata, 'alta': alta, 'pneumonia': pneumonia, 'exitus':exitus}
     
    def get_dbs(self):
        """."""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        self.upload = []
        sql = """select 
                C_CIP, C_EDAT_ANYS,C_DATA_NAIX, C_GRUP_EDAT, C_SEXE ,C_NACIONALITAT ,C_UP_ABS,C_UP,
                C_GMA_CODI ,C_GMA_COMPLEXITAT ,C_INSTITUCIONALITZAT,PR_MACA_DATA,PR_PCC_DATA,
                C_ULTIMA_VISITA_EAP, C_VISITES_ANY,
                I_DIFTERIA_VALOR,I_DIFTERIA_DATA,
                I_TETANUS_VALOR,I_TETANUS_DATA,I_PERTUSSIS_VALOR,I_PERTUSSIS_DATA,I_ROTAVIRUS_VALOR,I_ROTAVIRUS_DATA,
                I_MCC_VALOR,I_MCC_DATA,I_VARICELA_VALOR,I_VARICELA_DATA,I_POLIO_VALOR,I_POLIO_DATA,
                I_VHB_VALOR,I_VHB_DATA,I_VHA_VALOR,I_VHA_DATA,I_XARAMPIO_VALOR,I_XARAMPIO_DATA,
                I_RUBEOLA_VALOR,I_RUBEOLA_DATA,I_PAROTIDITIS_VALOR,I_PAROTIDITIS_DATA,
                I_PNC_VALOR,I_PNC_DATA,I_HIB_VALOR,I_HIB_DATA,I_VPH_VALOR,I_VPH_DATA,
                I_GRIP_VALOR,I_GRIP_DATA, V_VHA_SEROLOGIA_DATA, V_VHA_SEROLOGIA_VALOR
                FROM dbs"""
        for dades in u.getAll(sql, SIDICS_DB):
            medea = self.censal[dades[0]] if dades[0] in self.censal else None
            estat = self.covids[dades[0]]['estat'] if dades[0] in self.covids else None
            cas_data = self.covids[dades[0]]['casdata'] if dades[0] in self.covids else None
            alta = self.covids[dades[0]]['alta'] if dades[0] in self.covids else None
            pneumonia = self.covids[dades[0]]['pneumonia'] if dades[0] in self.covids else None
            exitus = self.covids[dades[0]]['exitus'] if dades[0] in self.covids else None
            EQA0702 = self.ind_pedia[(dades[0], 'EQA0702')] if (dades[0], 'EQA0702') in self.ind_pedia else 2
            EQA0715 = self.ind_pedia[(dades[0], 'EQA0715')] if (dades[0], 'EQA0715') in self.ind_pedia else 2
            EQA0716 = self.ind_pedia[(dades[0], 'EQA0716')] if (dades[0], 'EQA0716') in self.ind_pedia else 2
            EQA0717 = self.ind_pedia[(dades[0], 'EQA0717')] if (dades[0], 'EQA0717') in self.ind_pedia else 2
            EQA0712 = self.ind_pedia[(dades[0], 'EQA0712')] if (dades[0], 'EQA0712') in self.ind_pedia else 2
            EQA0707 = self.ind_pedia[(dades[0], 'EQA0707')] if (dades[0], 'EQA0707') in self.ind_pedia else 2
            EQA0308 = self.ind_adults[(dades[0], 'EQA0308')] if (dades[0], 'EQA0308') in self.ind_adults else 2
            EQA0309 = self.ind_adults[(dades[0], 'EQA0309')] if (dades[0], 'EQA0309') in self.ind_adults else 2
            EQA0312 = self.ind_adults[(dades[0], 'EQA0312')] if (dades[0], 'EQA0312') in self.ind_adults else 2
            EQA0501 = self.ind_adults[(dades[0], 'EQA0501')] if (dades[0], 'EQA0501') in self.ind_adults else 2
            EQA0502 = self.ind_adults[(dades[0], 'EQA0502')] if (dades[0], 'EQA0502') in self.ind_adults else 2
            VHA_DATA = self.dx_patologia[(dades[0], 'VHA_DATA')] if (dades[0], 'VHA_DATA') in self.dx_patologia else None
            XARAMPIO_DATA = self.dx_patologia[(dades[0], 'XARAMPIO_DATA')] if (dades[0], 'XARAMPIO_DATA') in self.dx_patologia else None
            PAROTIDITIS_DATA = self.dx_patologia[(dades[0], 'PAROTIDITIS_DATA')] if (dades[0], 'PAROTIDITIS_DATA') in self.dx_patologia else None
            RUBEOLA_DATA = self.dx_patologia[(dades[0], 'RUBEOLA_DATA')] if (dades[0], 'RUBEOLA_DATA') in self.dx_patologia else None
            E_DIFTERIA = 'E' if (dades[0], 'E_DIFTERIA') in self.exclusions else None
            E_DIFTERIA_MOTIU = self.exclusions[(dades[0], 'E_DIFTERIA')] if (dades[0], 'E_DIFTERIA') in self.exclusions else None
            E_TETANUS = 'E' if (dades[0], 'E_TETANUS') in self.exclusions else None
            E_TETANUS_MOTIU = self.exclusions[(dades[0], 'E_TETANUS')] if (dades[0], 'E_TETANUS') in self.exclusions else None
            E_PERTUSSIS = 'E' if (dades[0], 'E_PERTUSSIS') in self.exclusions else None
            E_PERTUSSIS_MOTIU = self.exclusions[(dades[0], 'E_PERTUSSIS')] if (dades[0], 'E_PERTUSSIS') in self.exclusions else None
            E_ROTAVIRUS = 'E' if (dades[0], 'E_ROTAVIRUS') in self.exclusions else None
            E_ROTAVIRUS_MOTIU = self.exclusions[(dades[0], 'E_ROTAVIRUS')] if (dades[0], 'E_ROTAVIRUS') in self.exclusions else None
            E_MCC = 'E' if (dades[0], 'E_MCC') in self.exclusions else None
            E_MCC_MOTIU = self.exclusions[(dades[0], 'E_MCC')] if (dades[0], 'E_MCC') in self.exclusions else None
            E_VARICELA = 'E' if (dades[0], 'E_VARICELA') in self.exclusions else None
            E_VARICELA_MOTIU = self.exclusions[(dades[0], 'E_VARICELA')] if (dades[0], 'E_VARICELA') in self.exclusions else None
            E_POLIO = 'E' if (dades[0], 'E_POLIO') in self.exclusions else None
            E_POLIO_MOTIU = self.exclusions[(dades[0], 'E_POLIO')] if (dades[0], 'E_POLIO') in self.exclusions else None
            E_VHB = 'E' if (dades[0], 'E_VHB') in self.exclusions else None
            E_VHB_MOTIU = self.exclusions[(dades[0], 'E_VHB')] if (dades[0], 'E_VHB') in self.exclusions else None
            E_VHA = 'E' if (dades[0], 'E_VHA') in self.exclusions else None
            E_VHA_MOTIU = self.exclusions[(dades[0], 'E_VHA')] if (dades[0], 'E_VHA') in self.exclusions else None
            E_XARAMPIO = 'E' if (dades[0], 'E_XARAMPIO') in self.exclusions else None
            E_XARAMPIO_MOTIU = self.exclusions[(dades[0], 'E_XARAMPIO')] if (dades[0], 'E_XARAMPIO') in self.exclusions else None
            E_RUBEOLA = 'E' if (dades[0], 'E_RUBEOLA') in self.exclusions else None
            E_RUBEOLA_MOTIU = self.exclusions[(dades[0], 'E_RUBEOLA')] if (dades[0], 'E_RUBEOLA') in self.exclusions else None
            E_PAROTIDITIS = 'E' if (dades[0], 'E_PAROTIDITIS') in self.exclusions else None
            E_PAROTIDITIS_MOTIU = self.exclusions[(dades[0], 'E_PAROTIDITIS')] if (dades[0], 'E_PAROTIDITIS') in self.exclusions else None
            E_PNC = 'E' if (dades[0], 'E_PNC') in self.exclusions else None
            E_PNC_MOTIU = self.exclusions[(dades[0], 'E_PNC')] if (dades[0], 'E_PNC') in self.exclusions else None
            E_HIB = 'E' if (dades[0], 'E_HIB') in self.exclusions else None
            E_HIB_MOTIU = self.exclusions[(dades[0], 'E_HIB')] if (dades[0], 'E_HIB') in self.exclusions else None
            E_VPH = 'E' if (dades[0], 'E_VPH') in self.exclusions else None
            E_VPH_MOTIU = self.exclusions[(dades[0], 'E_VPH')] if (dades[0], 'E_VPH') in self.exclusions else None
            E_GRIP = 'E' if (dades[0], 'E_GRIP') in self.exclusions else None
            E_GRIP_MOTIU = self.exclusions[(dades[0], 'E_GRIP')] if (dades[0], 'E_GRIP') in self.exclusions else None
            up = dades + (estat,cas_data, alta, pneumonia, exitus, medea, EQA0702, EQA0715,
                        EQA0716, EQA0717, EQA0712,EQA0707, EQA0308, EQA0309, EQA0312, EQA0501, EQA0502, VHA_DATA, XARAMPIO_DATA, PAROTIDITIS_DATA, RUBEOLA_DATA,
                        E_DIFTERIA,E_DIFTERIA_MOTIU,E_TETANUS,E_TETANUS_MOTIU,E_PERTUSSIS,E_PERTUSSIS_MOTIU,E_ROTAVIRUS,E_ROTAVIRUS_MOTIU,E_MCC,E_MCC_MOTIU,
                        E_VARICELA,E_VARICELA_MOTIU,E_POLIO, E_POLIO_MOTIU,E_VHB,E_VHB_MOTIU, E_VHA, E_VHA_MOTIU,E_XARAMPIO,E_XARAMPIO_MOTIU,E_RUBEOLA, E_RUBEOLA_MOTIU,
                        E_PAROTIDITIS, E_PAROTIDITIS_MOTIU,E_PNC, E_PNC_MOTIU,E_HIB,E_HIB_MOTIU,E_VPH,E_VPH_MOTIU,E_GRIP,E_GRIP_MOTIU)
            self.upload.append(up)
            

    def export_data(self):
        u.printTime("export")
        cols = ("C_CIP varchar(40)", "C_EDAT_ANYS int" ,"C_DATA_NAIX date", "C_GRUP_EDAT int","C_SEXE varchar(10)","C_NACIONALITAT varchar(40)" ,"C_UP_ABS varchar(10)","C_UP varchar(10)",
                "C_GMA_CODI  varchar(10)","C_GMA_COMPLEXITAT double" ,"C_INSTITUCIONALITZAT VARCHAR(10)", "PR_MACA_DATA date","PR_PCC_DATA date",
                "C_ULTIMA_VISITA_EAP date", "C_VISITES_ANY int", "I_DIFTERIA_VALOR int","I_DIFTERIA_DATA date",
                "I_TETANUS_VALOR int","I_TETANUS_DATA date","I_PERTUSSIS_VALOR int","I_PERTUSSIS_DATA  date","I_ROTAVIRUS_VALOR int","I_ROTAVIRUS_DATA date",
                "I_MCC_VALOR int","I_MCC_DATA date","I_VARICELA_VALOR int","I_VARICELA_DATA date","I_POLIO_VALOR int","I_POLIO_DATA date","I_VHB_VALOR int",
                "I_VHB_DATA date","I_VHA_VALOR int","I_VHA_DATA date","I_XARAMPIO_VALOR int","I_XARAMPIO_DATA date","I_RUBEOLA_VALOR int","I_RUBEOLA_DATA date","I_PAROTIDITIS_VALOR int","I_PAROTIDITIS_DATA date",
                "I_PNC_VALOR int","I_PNC_DATA date","I_HIB_VALOR int","I_HIB_DATA date","I_VPH_VALOR int","I_VPH_DATA date","I_GRIP_VALOR int","I_GRIP_DATA date", 
                "V_VHA_SEROLOGIA_DATA DATE", "V_VHA_SEROLOGIA_VALOR int", "estat varchar(20)",
                "cas_data date", "ing_alta date", "pneumonia date", "exitus date", "medea_seccio double",
                "EQA0702 int", "EQA0715 int", "EQA0716 int", "EQA0717 int", "EQA0712 int", "EQA0707 int",
                "EQA0308 int", "EQA0309 int", "EQA0312 int", "EQA0501 int", "EQA0502 int",
                "VHA_DATA date", "XARAMPIO_DATA date", "PAROTIDITIS_DATA date", "RUBEOLA_DATA date",
                "E_DIFTERIA VARCHAR2(1)","E_DIFTERIA_MOTIU VARCHAR2(100)","E_TETANUS VARCHAR2(1)","E_TETANUS_MOTIU VARCHAR2(100)","E_PERTUSSIS VARCHAR2(1)","E_PERTUSSIS_MOTIU VARCHAR2(100)",
                "E_ROTAVIRUS VARCHAR2(1)","E_ROTAVIRUS_MOTIU VARCHAR2(100)","E_MCC VARCHAR2(1)","E_MCC_MOTIU VARCHAR2(100)","E_VARICELA VARCHAR2(1)",
                "E_VARICELA_MOTIU VARCHAR2(100)","E_POLIO VARCHAR2(1)","E_POLIO_MOTIU VARCHAR2(100)","E_VHB VARCHAR2(1)","E_VHB_MOTIU VARCHAR2(100)",
                "E_VHA VARCHAR2(1)","E_VHA_MOTIU VARCHAR2(100)","E_XARAMPIO VARCHAR2(1)","E_XARAMPIO_MOTIU VARCHAR2(100)","E_RUBEOLA VARCHAR2(1)","E_RUBEOLA_MOTIU VARCHAR2(100)",
                "E_PAROTIDITIS VARCHAR2(1)","E_PAROTIDITIS_MOTIU VARCHAR2(100)","E_PNC VARCHAR2(1)","E_PNC_MOTIU VARCHAR2(100)","E_HIB VARCHAR2(1)","E_HIB_MOTIU VARCHAR2(100)",
                "E_VPH VARCHAR2(1)","E_VPH_MOTIU VARCHAR2(100)","E_GRIP VARCHAR2(1)","E_GRIP_MOTIU VARCHAR2(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), "permanent", rm=True)
        u.listToTable(self.upload, tb, "permanent")

    
if __name__ == '__main__':
    u.printTime("Inici")
    
    triple_v()
    
    u.printTime("Final")
