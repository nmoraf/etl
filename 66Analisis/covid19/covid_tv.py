# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

resta = d.datetime.now().hour < 21
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)

db = 'redics'
tb = 'sisap_covid_rec_TV'

dx_file =  "dx_covid.txt"

class article_dani(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hash()
        self.get_dbs()
        self.get_dbs_2019()
        self.get_resis()
        self.get_ingres()
        self.get_master()
        self.export_data()
        self.dona_grants()
        
  
    def get_hash(self):
        """conversor de hash"""
        u.printTime("hash")
        self.hashI = {}
        self.hash_C = {}
        sql = """ select hash, hash_ics
                    from sisap_covid_pac_id"""
        for hash,hashi in u.getAll(sql, 'redics'):
            self.hashI[hashi] = hash
            self.hash_C[hash] = hashi
            
    def get_dbs(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS")
        sql = """select hash,
                    c_nacionalitat,
                    PR_MACA_DATA, PR_PCC_DATA,
                    I_DIFTERIA_VALOR, I_TETANUS_VALOR, I_PERTUSSIS_VALOR, I_ROTAVIRUS_VALOR,
                    I_MCC_VALOR, I_VARICELA_VALOR, I_POLIO_VALOR, I_VHB_VALOR, I_VHA_VALOR,
                    I_XARAMPIO_VALOR, I_RUBEOLA_VALOR, I_PAROTIDITIS_VALOR, I_PNC_VALOR, I_HIB_VALOR,
                    I_VPH_VALOR, I_GRIP_VALOR
                from dbs a, sisap_covid_pac_id b 
                where a.c_cip = b.hash_ics"""
        self.factors = {}
        self.in_dbs_true = {}
        for (cip,
             nac,
             MACA, PCC,
             difteria, tetanus, pertussis, rotavirus,
             mcc, varicela, polio, vhb, vha,
             xarampio, rubeola, parotiditis, pnc, hib,
             vph, grip) in u.getAll(sql, db):
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            if nac:
                self.factors[(cip, "nacionalitat")] = nac
            if MACA:
                self.factors[(cip, "MACA")] = MACA
            if PCC:
                self.factors[(cip, "PCC")] = PCC
            if difteria:
                self.factors[(cip, "difteria")] = difteria
            if tetanus:
                self.factors[(cip, "tetanus")] = tetanus
            if pertussis:
                self.factors[(cip, "pertussis")] = pertussis
            if rotavirus:
                self.factors[(cip, "rotavirus")] = rotavirus
            if mcc:
                self.factors[(cip, "mcc")] = mcc
            if varicela:
                self.factors[(cip, "varicela")] = varicela
            if polio:
                self.factors[(cip, "polio")] = polio
            if vhb:
                self.factors[(cip, "vhb")] = vhb
            if vha:
                self.factors[(cip, "vha")] = vha
            if xarampio:
                self.factors[(cip, "xarampio")] = xarampio
            if rubeola:
                self.factors[(cip, "rubeola")] = rubeola
            if parotiditis:
                self.factors[(cip, "parotiditis")] = parotiditis
            if pnc:
                self.factors[(cip, "pnc")] = pnc
            if hib:
                self.factors[(cip, "hib")] = hib
            if vph:
                self.factors[(cip, "vph")] = vph
            if grip:
                self.factors[(cip, "grip")] = grip
           
    def get_dbs_2019(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        
        sql = """select c_cip,
                    c_nacionalitat,
                    c_gma_codi, c_gma_complexitat,
                    PR_MACA_DATA, PR_PCC_DATA,
                    I_DIFTERIA_VALOR, I_TETANUS_VALOR, I_PERTUSSIS_VALOR, I_ROTAVIRUS_VALOR,
                    I_MCC_VALOR, I_VARICELA_VALOR, I_POLIO_VALOR, I_VHB_VALOR, I_VHA_VALOR,
                    I_XARAMPIO_VALOR, I_RUBEOLA_VALOR, I_PAROTIDITIS_VALOR, I_PNC_VALOR, I_HIB_VALOR,
                    I_VPH_VALOR, I_GRIP_VALOR
                from dbs_2019"""
        self.factors19 = {}
        for (cip,
             nac,
             gma_c, gma_cmplx,
             MACA, PCC,
             difteria, tetanus, pertussis, rotavirus,
             mcc, varicela, polio, vhb, vha,
             xarampio, rubeola, parotiditis, pnc, hib,
             vph, grip) in u.getAll(sql, SIDICS_DB):
            if cip in self.hashI:
                cipCov = self.hashI[cip]
                if cipCov not in self.in_dbs_true:
                    self.factors19[(cipCov, "in_dbs")] = 1 
                    if gma_c:
                        self.factors19[(cipCov, "gma_c")] = gma_c
                        self.factors19[(cipCov, "gma_cmplx")] = gma_cmplx
                    if nac:
                        self.factors19[(cipCov, "nacionalitat")] = nac
                    if MACA:
                        self.factors19[(cipCov, "MACA")] = MACA
                    if PCC:
                        self.factors19[(cipCov, "PCC")] = PCC
                    if difteria:
                        self.factors19[(cipCov, "difteria")] = difteria
                    if tetanus:
                        self.factors19[(cipCov, "tetanus")] = tetanus
                    if pertussis:
                        self.factors19[(cipCov, "pertussis")] = pertussis
                    if rotavirus:
                        self.factors19[(cipCov, "rotavirus")] = rotavirus
                    if mcc:
                        self.factors19[(cipCov, "mcc")] = mcc
                    if varicela:
                        self.factors19[(cipCov, "varicela")] = varicela
                    if polio:
                        self.factors19[(cipCov, "polio")] = polio
                    if vhb:
                        self.factors19[(cipCov, "vhb")] = vhb
                    if vha:
                        self.factors19[(cipCov, "vha")] = vha
                    if xarampio:
                        self.factors19[(cipCov, "xarampio")] = xarampio
                    if rubeola:
                        self.factors19[(cipCov, "rubeola")] = rubeola
                    if parotiditis:
                        self.factors19[(cipCov, "parotiditis")] = parotiditis
                    if pnc:
                        self.factors19[(cipCov, "pnc")] = pnc
                    if hib:
                        self.factors19[(cipCov, "hib")] = hib
                    if vph:
                        self.factors19[(cipCov, "vph")] = vph
                    if grip:
                        self.factors19[(cipCov, "grip")] = grip
                        
    def get_resis(self):
        """Agafem pacients que hagin estat algun cop en residències"""
        u.printTime("resis")
        self.pac_res = {}
        sql = "select hash, residencia, entrada, sortida from sisap_covid_res_master"
        for hash, res, entrada, sortida in u.getAll(sql, db):
            self.pac_res[hash] = {'res': res, 'entrada': entrada, 'sortida': sortida}
   

    def get_ingres(self):
        """ingressos"""
        u.printTime("ingres")
        dx_ingres = []
        desc_dx = {}
        self.ingres_urg = {}
        for (dx, desc, tip) in u.readCSV(dx_file, sep='@'):
            dx_ingres.append(dx)
            desc_dx[dx] = desc
        in_crit = tuple(dx_ingres)
        
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (u_motiu in {0} or u_dd1 in {0} or u_dd2 in {0} or u_dd3 in {0} or u_dd4 in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315'""".format(in_crit)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
        dx2 = "('J11','J12','J13','J14','J15','J16','J17','J18','J19','J20','J22','J40','J41','J42','J43','J44','J45','J47','J80','J84','J96')"
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (substr(u_motiu, 0,3) in {0} or substr(u_dd1, 0,3) in {0} or substr(u_dd2, 0,3) in {0} or substr(u_dd3, 0,3) in {0} or substr(u_dd4, 0,3) in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315'""".format(dx2)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
    
    
    
    def get_master(self):
        """obtenim dades de master"""
        u.printTime("master")
        self.upload = []
        sql = """
                select hash, edat, sexe, localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n,exitus, rx, pneumonia,
                 gma_codi, gma_complexitat, ing_alta, ing_cod, ing_desti,
                 fr_numero, fr_literal, fr_edat, fr_hta_no_control, fr_diabetis, fr_patol_resp, fr_mcv,
                 fr_hepatopatia, fr_neoplasia, fr_mrc, fr_immunodepressio
                from sisap_covid_pac_master
                where estat in ('Possible', 'Confirmat')
              """
        for (hash, edat,sexe,localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus, rx, pneumonia,
               gma_codi, gma_complexitat, ing_alta, ing_cod, ing_desti, 
                fr_numero, fr_literal, fr_edat, fr_hta_no_control, fr_diabetis, fr_patol_resp, fr_mcv,
                 fr_hepatopatia, fr_neoplasia, fr_mrc, fr_immunosupressio) in u.getAll(sql, db):
            hashi = self.hash_C[hash]
            entrada = None
            sortida = None
            nac = None
            in_dbs = 0
            MACA, PCC = None, None
            difteria, tetanus, pertussis, rotavirus = None, None, None, None
            mcc, varicela, polio, vhb, vha = None, None, None, None, None
            xarampio, rubeola, parotiditis, pnc, hib =  None, None, None, None, None
            vph, grip = None, None
            ingres_data = self.ingres_urg[hash]['dat'] if hash in self.ingres_urg else None
            ingres_cod = self.ingres_urg[hash]['dx'] if hash in self.ingres_urg else None
            if hash in self.pac_res:
                resi = self.pac_res[hash]['res']
                entrada = self.pac_res[hash]['entrada']
                sortida = self.pac_res[hash]['sortida']
            if hash in self.in_dbs_true:
                in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                nac = self.factors[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors else None
                MACA = self.factors[(hash, "MACA")] if (hash, "MACA") in self.factors else None
                PCC = self.factors[(hash, "PCC")] if (hash, "PCC") in self.factors else None
                difteria = self.factors[(hash, "difteria")] if (hash, "difteria") in self.factors else None
                tetanus = self.factors[(hash, "tetanus")] if (hash, "tetanus") in self.factors else None
                pertussis = self.factors[(hash, "pertussis")] if (hash, "pertussis") in self.factors else None
                rotavirus = self.factors[(hash, "rotavirus")] if (hash, "rotavirus") in self.factors else None
                mcc = self.factors[(hash, "mcc")] if (hash, "mcc") in self.factors else None
                varicela = self.factors[(hash, "varicela")] if (hash, "varicela") in self.factors else None
                polio = self.factors[(hash, "polio")] if (hash, "polio") in self.factors else None
                vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                vha = self.factors[(hash, "vha")] if (hash, "vha") in self.factors else None
                xarampio = self.factors[(hash, "xarampio")] if (hash, "xarampio") in self.factors else None
                rubeola = self.factors[(hash, "rubeola")] if (hash, "rubeola") in self.factors else None
                parotiditis = self.factors[(hash, "parotiditis")] if (hash, "parotiditis") in self.factors else None
                pnc = self.factors[(hash, "pnc")] if (hash, "pnc") in self.factors else None
                hib = self.factors[(hash, "hib")] if (hash, "hib") in self.factors else None
                vph = self.factors[(hash, "vph")] if (hash, "vph") in self.factors else None
                grip = self.factors[(hash, "grip")] if (hash, "grip") in self.factors else None
            else:
                in_dbs = self.factors19[(hash, "in_dbs")]  if (hash, "in_dbs") in self.factors19 else 0
                nac = self.factors19[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors19 else None
                gma_codi = self.factors19[(hash, "gma_c")] if (hash, "gma_c") in self.factors19 else gma_codi
                gma_complexitat = self.factors19[(hash, "gma_cmplx")] if (hash, "gma_cmplx") in self.factors19 else gma_complexitat
                MACA = self.factors19[(hash, "MACA")] if (hash, "MACA") in self.factors19 else None
                PCC = self.factors19[(hash, "PCC")] if (hash, "PCC") in self.factors19 else None
                difteria = self.factors19[(hash, "difteria")] if (hash, "difteria") in self.factors19 else None
                tetanus = self.factors19[(hash, "tetanus")] if (hash, "tetanus") in self.factors19 else None
                pertussis = self.factors19[(hash, "pertussis")] if (hash, "pertussis") in self.factors19 else None
                rotavirus = self.factors19[(hash, "rotavirus")] if (hash, "rotavirus") in self.factors19 else None
                mcc = self.factors19[(hash, "mcc")] if (hash, "mcc") in self.factors19 else None
                varicela = self.factors19[(hash, "varicela")] if (hash, "varicela") in self.factors19 else None
                polio = self.factors19[(hash, "polio")] if (hash, "polio") in self.factors19 else None
                vhb = self.factors19[(hash, "vhb")] if (hash, "vhb") in self.factors19 else None
                vha = self.factors19[(hash, "vha")] if (hash, "vha") in self.factors19 else None
                xarampio = self.factors19[(hash, "xarampio")] if (hash, "xarampio") in self.factors19 else None
                rubeola = self.factors19[(hash, "rubeola")] if (hash, "rubeola") in self.factors19 else None
                parotiditis = self.factors19[(hash, "parotiditis")] if (hash, "parotiditis") in self.factors19 else None
                pnc = self.factors19[(hash, "pnc")] if (hash, "pnc") in self.factors19 else None
                hib = self.factors19[(hash, "hib")] if (hash, "hib") in self.factors19 else None
                vph = self.factors19[(hash, "vph")] if (hash, "vph") in self.factors19 else None
                grip = self.factors19[(hash, "grip")] if (hash, "grip") in self.factors19 else None
            self.upload.append([hash, hashi, edat,sexe, nac, localitat, abs, up, resi, entrada, sortida, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus, rx, pneumonia,
                 gma_codi, gma_complexitat,  ing_alta, ing_cod, ing_desti, ingres_data, ingres_cod,
                 in_dbs, fr_numero, fr_literal, fr_edat, fr_hta_no_control, fr_diabetis, fr_patol_resp, fr_mcv,
                 fr_hepatopatia, fr_neoplasia, fr_mrc, fr_immunosupressio,
                 MACA, PCC,
                 difteria, tetanus, pertussis, rotavirus,
                 mcc, varicela, polio, vhb, vha,
                 xarampio, rubeola, parotiditis, pnc, hib,
                 vph, grip])

                 
                
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("hash varchar2(40)", "hash_ics varchar2(40)", "edat int",
            "sexe varchar2(1)", "nacionalitat varchar2(100)", "localitat varchar2(7)", "abs int",
            "up varchar2(5)",  "resi varchar2(10)", "entrada_resi date", "sortida_resi date", "hotel varchar2(255)",
             "estat varchar2(16)", "origen_d varchar2(40)","cas_data date",
            "dx_cod varchar2(16)", "dx_dde date", "dx_dba date",
            "dx_sit varchar2(16)",  "pcr_data date", "pcr_res varchar2(30)", "pcr_n int","pcr_ia_n int", "test_n int", "test_ia_n int",
            "exitus date", "rx date",
            "pneumonia date", 
            "gma_codi varchar2(3)", "gma_complexitat number(6, 3)",
             "ing_alta date","ing_cod varchar2(16)", "ing_desti varchar2(64)", "ingres_data date", "ingres_cod varchar2(500)",
            "in_dbs int",
            "fr_numero int", "fr_literal varchar2(500)", "fr_edat int", "fr_hta_no_control int", "fr_diabetis int", "fr_patol_resp int", "fr_mcv int",
                 "fr_hepatopatia int", "fr_neoplasia int", "fr_mrc int", "fr_immunodepressio int",
                 "MACA date", "PCC date",
                 "difteria int", "tetanus int", "pertussis int", "rotavirus int",
                 "mcc int", "varicela int", "polio int", "vhb int", "vha int",
                 "xarampio int", "rubeola int", "parotiditis int", "pnc int", "hib int",
                 "vph int", "grip int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    article_dani()
    
    u.printTime("Final")
