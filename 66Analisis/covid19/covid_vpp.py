# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

resta = d.datetime.now().hour < 21
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)

db = 'redics'
tb = 'sisap_covid_rec_vpp'

dx_file =  "dx_covid.txt"



class vpp(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_id()
        self.get_visit()
        self.get_resis()
        self.get_ingres()
        self.get_pcr()
        self.get_master()
        self.export_data()
        self.dona_grants()
        
  
    def get_id(self):
        """."""
        u.printTime("Ids")
        self.ids = {}
        sql = "select hash, cip from sisap_covid_pac_id"
        for hash, cip in u.getAll(sql, db):
            self.ids[cip] = hash
    
    
    def get_visit(self):
        """Agafem la data introduccio a problemes"""
        u.printTime("data_alta")
        self.altes_dx = {}
        for sector in u.sectors:
            print sector
            sql = "select pr_cod_u, pr_data_alta from prstb015 where pr_cod_ps in ('C01-B34.2', 'C01-B97.21', 'C01-B97.29', 'C01-J12.81', 'C01-J12.89')"
            for cip2, alta in u.getAll(sql, sector):
                if cip2 in self.ids:
                    cip = self.ids[cip2]
                    if alta != None:
                        if cip in self.altes_dx:
                            dat = self.altes_dx[cip]
                            if alta < dat:
                                self.altes_dx[cip] = alta
                        else:
                            self.altes_dx[cip] = alta       
                        
    
    def get_resis(self):
        """Agafem pacients que hagin estat algun cop en residències"""
        u.printTime("resis")
        self.pac_res = {}
        sql = "select hash, residencia, entrada, sortida from sisap_covid_res_master"
        for hash, res, entrada, sortida in u.getAll(sql, db):
            self.pac_res[hash] = {'res': res, 'entrada': entrada, 'sortida': sortida}

    def get_ingres(self):
        """ingressos"""
        u.printTime("ingres")
        dx_ingres = []
        desc_dx = {}
        self.ingres_urg = {}
        for (dx, desc, tip) in u.readCSV(dx_file, sep='@'):
            dx_ingres.append(dx)
            desc_dx[dx] = desc
        in_crit = tuple(dx_ingres)
        
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (u_motiu in {0} or u_dd1 in {0} or u_dd2 in {0} or u_dd3 in {0} or u_dd4 in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315'""".format(in_crit)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
        dx2 = "('J11','J12','J13','J14','J15','J16','J17','J18','J19','J20','J22','J40','J41','J42','J43','J44','J45','J47','J80','J84','J96')"
        sql = """select hash, u_data, u_motiu, u_dd1, u_dd2, u_dd3, u_dd4 
                from sisap_covid_pac_urgencies where (u_condicio like ('2%') or u_condicio like ('3%') or u_condicio like ('7%'))
                and (substr(u_motiu, 0,3) in {0} or substr(u_dd1, 0,3) in {0} or substr(u_dd2, 0,3) in {0} or substr(u_dd3, 0,3) in {0} or substr(u_dd4, 0,3) in {0}) and m_estat in ('Possible','Confirmat')
                and to_char(u_data, 'YYYYMMDD')>'20200315'""".format(dx2)
        for hash, data, motiu, dd1, dd2, dd3, dd4 in u.getAll(sql, db):
            diagnostic = motiu + ' - ' + dd1 + ' - ' + dd2 + ' - ' + dd3 + ' - ' + dd4
            if hash in self.ingres_urg:
                dat2 = self.ingres_urg[hash]['dat']
                if data<dat2:
                    self.ingres_urg[hash] = {'dat': data,'dx':diagnostic}    
            else:
                self.ingres_urg[hash] = {'dat': data,'dx':diagnostic} 
    
    def get_pcr(self):
        """."""
        self.pcr_dat = {}
        sql = """
                select hash, min(data) 
                from preduffa.sisap_covid_pac_prv_aux 
                group by hash"""
        for hash, dat in u.getAll(sql, db):
            self.pcr_dat[(hash)]= dat
    
    def get_master(self):
        """obtenim dades de master"""
        u.printTime("master")
        self.upload = []
        sql = """
                select hash, edat, sexe, up, estat, origen_d, cas_data, 
                dx_cod, dx_dde, pcr_data, pcr_res,ing_alta, exitus
                from sisap_covid_pac_master
                where estat in ('Possible', 'Confirmat') and dx_cod in 
                 ('C01-B34.2', 'C01-B97.21', 'C01-B97.29', 'C01-J12.81', 'C01-J12.89')

              """
        for (hash, edat,sexe, up, estat, origen_d, cas_data, 
                dx_cod, dx_dde,pcr_data, pcr_res, ing_alta, exitus) in u.getAll(sql, db): 
            resi = 0
            pcr_first = None
            if hash in self.pac_res:
                resi = 1
            ingres_data = self.ingres_urg[hash]['dat'] if hash in self.ingres_urg else None
            ingres_cod = self.ingres_urg[hash]['dx'] if hash in self.ingres_urg else None
            pcr_first = self.pcr_dat[hash] if hash in self.pcr_dat else None
            dx_alta = self.altes_dx[hash] if hash in self.altes_dx else cas_data
            self.upload.append([hash, edat,sexe, up, resi,estat, origen_d, cas_data, dx_alta,
               dx_cod, dx_dde,pcr_first, pcr_data, pcr_res, 
               ingres_data, ingres_cod, ing_alta,exitus])
                
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("hash varchar2(40)", "edat int",
            "sexe varchar2(1)",  "up varchar(5)", "resi int",
             "estat varchar2(16)", "origen_d varchar2(40)","cas_data date","cas_alta date",
            "dx_cod varchar2(16)", "dx_dde date", "pcr_first date", "pcr_data date", "pcr_res varchar2(30)",
            "ingres_data date", "ingres_cod varchar2(500)", "ing_alta date", "exitus date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    vpp()
    
    u.printTime("Final")
