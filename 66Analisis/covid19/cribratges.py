# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u
import hashlib as h

db = "redics"
tb = "sisap_covid_pac_cribratges"

TODAY = d.datetime.now().date()
print TODAY

class cribratges_pob(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_resis()
        self.get_pcr()
        self.get_rca()
        self.get_cribratge()
        self.export_data()
        self.dona_grants()
        
    def get_resis(self):
        """Obtenim els pacients en residencia de pac_resis"""
        u.printTime("resis")
        sql = """select hash 
                from sisap_covid_res_master"""
        self.resis = set([hash for hash, in u.getAll(sql, db)])
    
    def get_pcr(self):
        """."""
        u.printTime("PCR")
        self.pcrs = c.defaultdict(lambda: c.defaultdict(set))
        sql = """select hash,data,estat_cod 
                from preduffa.SISAP_COVID_PAC_PRV_AUX 
                where prova='PCR'"""
        for hash, dat, estat in u.getAll(sql, db):
            self.pcrs[hash][dat].add(estat)
    
    def get_rca(self):
        """."""
        u.printTime("rca")
        self.pob_rca = {}
        sql = """select hash, sexe, data_naixement, localitat, abs
                from preduffa.sisap_covid_pac_rca 
                where situacio='A'"""
        for hash, sexe, naix, loc, abs in u.getAll(sql, db):
            resi = 1 if hash in self.resis else 0
            edat = u.yearsBetween(naix, TODAY)
            self.pob_rca[hash] = {'edat': edat, 'sexe':sexe, 'loc': loc,'abs': abs, 'resi': resi}
            
    def get_cribratge(self):
        """Del codi de problema de salut C01-Z11.59"""
        self.upload = []
        u.printTime("cribratge")
        sql = """select cip_usuari_cip, pr_cod_ps, pr_th, pr_dde, pr_dba 
                from prstb015, usutb011 
                where pr_cod_u = cip_cip_anterior and 
                      pr_cod_o_ps = 'C' and 
                      pr_cod_ps in ('C01-Z11.59', 'Z11.59')"""
        for sector in u.sectors:
            print(sector)
            for cip, ps, th, dde, dba in u.getAll(sql,sector):
                hash = h.sha1(cip).hexdigest().upper()
                edat = self.pob_rca[hash]['edat'] if hash in self.pob_rca else -1
                sexe = self.pob_rca[hash]['sexe'] if hash in self.pob_rca else '-1'
                localitat = self.pob_rca[hash]['loc'] if hash in self.pob_rca else 'SE'
                abs = self.pob_rca[hash]['abs'] if hash in self.pob_rca else 'SE'
                resi = self.pob_rca[hash]['resi'] if hash in self.pob_rca else -1
                dat_pcr, res_pcr = None, 9
                if hash in self.pcrs:
                    for dats in self.pcrs[hash]:
                        cribrat_diff = u.daysBetween(dde, dats)
                        ce_cri7 = 1 if -8 <cribrat_diff < 8 else 0
                        if ce_cri7 == 1:
                            resultats = self.pcrs[hash][dats]
                            for res in resultats:
                                if res_pcr != 0:
                                    dat_pcr = dats
                                    res_pcr = res
                self.upload.append([hash, edat, sexe, abs, localitat, resi, dde, dat_pcr, res_pcr])
                
                    
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("hash varchar2(40)", "edat int",
            "sexe varchar2(10)",   "abs varchar(10)", "localitat varchar(10)","resi int", "data_cribratge date", "data_pcr date", "res_pcr int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
            
            
if __name__ == '__main__':
    u.printTime("Inici")
    
    cribratges_pob()
    
    u.printTime("Final")