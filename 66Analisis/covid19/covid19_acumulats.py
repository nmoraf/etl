# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u
      
db = 'permanent'
tb_ind = "covids_acumulats_update"     
        
class Coronavirus_ind(object):
    """."""

    def __init__(self):
        """."""
        self.indicadors = []
        self.get_acumulats()
        self.get_pob()
        self.export_ind()
       
    
    
    def get_acumulats(self):
        """Acumulats setmanals"""
        
        sql = "select min(cas_data), max(cas_data) from sisap_covid_pac_master"
        for mind, maxd in u.getAll(sql, 'redics'):
            self.ini= mind
            self.fi = maxd 
            
            
        self.acumulats = c.Counter()
        sql = "select up, edat, sexe, cas_data, estat, pneumonia, exitus from sisap_covid_pac_master where estat in ('Possible', 'Confirmat')"
        for up, edat, sexe, dat, estat, pneumo, exitus in u.getAll(sql, 'redics'):
            self.acumulats[(up,  edat, sexe, 'casos')] += 1
            if pneumo != None:
                self.acumulats[(up,  edat, sexe,'pneumo')] += 1
            if exitus != None:
                self.acumulats[(up,  edat, sexe,'exitus')] += 1
            if estat == "Possible":
                self.acumulats[(up,  edat, sexe,'casosP')] += 1
            if estat == "Confirmat":
                self.acumulats[(up,  edat, sexe, 'casosC')] += 1
    
    def get_pob(self):
        """ Agafem les poblacions de la taula sisap_coronavirus_poblacio"""
        self.pob, self.cataleg = c.Counter(), {}
        sql = "select up, edat, sexe, recompte from sisap_covid_agr_poblacio"
        for up, edat, sexe, n in u.getAll(sql, 'redics'):
            num, numP, numC = 0, 0, 0
            num = self.acumulats[(up, edat, sexe, 'casos')]
            numP = self.acumulats[(up, edat, sexe, 'casosP')]
            numC = self.acumulats[(up, edat, sexe, 'casosC')]
            numpneumo = self.acumulats[(up, edat, sexe, 'pneumo')]
            exit = self.acumulats[(up, edat, sexe, 'exitus')]
            self.indicadors.append([self.fi, up, edat, sexe,  'casos' , num, n])
            self.indicadors.append([self.fi, up, edat, sexe,  'possibles' , numP, n])
            self.indicadors.append([self.fi, up, edat, sexe,  'confirmats' , numC, n])
            self.indicadors.append([self.fi, up, edat, sexe,  'pneumonia' , numpneumo, n])
            self.indicadors.append([self.fi, up, edat, sexe,  'exitus' , exit, n])

    
    def export_ind(self):
        """fem export dels indicadors"""
   
        columns = ["data date", "up varchar(10)","edat int", "sexe varchar(1)", "indicador varchar(20)",  "num int", "den int"]
        u.createTable(tb_ind, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.indicadors, tb_ind, db)
                                    
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    Coronavirus_ind()
    
    u.printTime("Final")