from argparse import ArgumentParser
import git
from os.path import isdir
import os

branch = 'master'
repos = 'sisap', 'sisapUtils', 'secret',

conditions = {'sisap': ' M 02nod/dades_noesb/nod_dextraccio.txt',
              'sisapUtils': '', 'secret': '', }

os.chdir((os.path.dirname(os.path.realpath(__file__))))

parser = ArgumentParser()
parser.add_argument('tag', nargs='?')
args = parser.parse_args()
tag = args.tag


def get_repo_path(repo):
    return '../../../{}'.format(repo)


def repo_exists(repo):
    return isdir(get_repo_path(repo))


def get_repo(repo):
    return git.Repo(get_repo_path(repo))


def check_repo(repo):
    client = get_repo(repo)
    client.remotes.origin.fetch()  # actualizo el historial de versione
    st = client.git.status(porcelain=True)  # ?diferencias en working tree
    out = client.git.log("origin/{branch}..{branch}".format(branch=branch), pretty="oneline")  # ?commits to push  # noqa
    inc = client.git.log("{branch}..origin/{branch}".format(branch=branch), pretty="oneline")  # ?commits to pull  # noqa
    if inc and not (st + out):
        pull = client.git.pull()  # git pull = hg pull && hg update
    else:
        pull = False
    if repo in conditions and st == conditions[repo] and not out:
        go = True
    else:
        go = False
    return st, out, inc, pull, go,


proceed = True
for repo in repos:
    if repo_exists(repo):
        st, out, inc, pull, go, = check_repo(repo)
        if not tag:
            print repo
            print 'st', st
            print 'ou', out
            print 'in', inc
            if pull:
                print 'pulled'
        else:
            if repo in conditions:
                proceed = proceed and go

if tag:
    if proceed:
        for repo in repos:
            if repo in conditions:
                client = get_repo(repo)
                if conditions[repo]:
                    client.git.add(".")
                    client.index.commit("canvi dext per calcul {}".format(tag))
                client.git.push()
                new_tag = client.create_tag(tag, message='Automatic tag "{0}"'.format(tag))  # noqa
                client.remotes.origin.push(new_tag)
        print('done')
    else:
        print('ups, algo ha fallat')
