import sisapUtils as u

users = ["PREDUFFA", "PREDUECR", "PREDUMMP", "PREDULMB", "PREDUJVG",
         "PREDUPRP", "PREDUMQS", "PREDUMPP", "PREDUXMG", "PREDUTFP",
         "PREDUMGS", "PREDUMAA", "PREDUEVC"]

sql = "select table_name from all_tables where owner='PDP'"
grant_sql = "grant select on {} to {}"
synonym_sql = "create or replace synonym {0}.{1} for {1}"

for table, in u.getAll(sql, 'pdp'):
    for user in users:
        u.execute(grant_sql.format(table, user), 'pdp')
        u.execute(synonym_sql.format(user, table), 'pdp')
