from sisapUtils import changePwd
from random import SystemRandom
from string import ascii_letters, digits
from base64 import b64encode


n = 24
pwd = ''.join(SystemRandom().choice(ascii_letters + digits) for _ in range(n))
secret = b64encode(pwd[::-1])


changePwd('redics', pwd)
print 'secret: {}'.format(secret)
