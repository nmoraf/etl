from sisapUtils import *
import sys,time

db= 'redics'

def query():

    if checkConnection(db):
        sql="select decode(d_dhf_rec_inc,null,0,1) status from redics.directora where d_di_pinc = (select max(d_di_pinc) from redics.directora)"
        status,= getOne(sql,db)
    else:
        status= 0
    return status

try:
    sys.argv[1]== "web"
except IndexError:
    web= False
else:
    web= True

if web:
    status= query()
    print status
else:
    f= 0
    while f== 0:
        status= query()
        if status== 0:
            wait= 5*60
        else:
            wait= 0
            f= 1
        time.sleep(wait)