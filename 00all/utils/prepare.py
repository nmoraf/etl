import csv
import sys
import os

setmanal = ('01down','02nod','03ind','04ped','05soc','06odn','07expEQA_my2pdp','20recerca_av','07expEQA_my2txt','08alt','09catsalut','10ass','11dbs','16jail','17rev','72recomptes','20recerca','24assir','98delay')
mensual_exc = ('92khalix','07expEQA_txt2klx_weekly','07expEQA_txt2klx','07expEQA_txt2klx_conv')


def readCSV(file,sep='@',header=False):

    with open(file,'rb') as f:
        primera = True
        c= csv.reader(f,delimiter=sep)
        for row in c:
            if header and primera:
                primera = False
            else:
                yield row

def writeCSV(file,data,mode='w',sep='@'):

    with open(file,'{0}b'.format(mode)) as f:
        c= csv.writer(f,delimiter=sep)
        c.writerows(data)


try:
    arg = sys.argv[1]
    if arg == 'res':
        glob = 0
    elif arg == 'setmanal':
        glob = 9
    else:
        glob = 1
except IndexError:
    glob = 1


SYSTEM = "unix" if os.name == "posix" else "win"
os.chdir((os.path.dirname(os.path.realpath(__file__))))
executable = "python" if SYSTEM == "unix" else "c:\Python27\python"
extension = "sh" if SYSTEM == "unix" else "bat"
sep= ';'

components1= os.path.abspath("../components/components.txt")
components2= components1.replace('components.txt','__components.txt')
components= []

for nom,pth,src,db,call in readCSV(components1,sep=sep):
    if glob == 9:
        do = 1 if pth in setmanal else 0
        delete = do
    elif glob == 1:
        do = 0 if pth in mensual_exc else 1
        delete = 1
    else:
        do = glob
        delete = do
    components.append([nom,pth,src,db,call,delete])
    file1= '../../{0}/proc/{1}'.format(pth,src).replace('__','')
    file2= '../../{0}/proc/{1}'.format(pth,src)
    bat0= '../../{0}/00blank.{1}'.format(pth, extension)
    bat1= '../../{0}/01del.{1}'.format(pth, extension)
    bat2= '../../{0}/02gen.{1}'.format(pth, extension)
    bat3= '../../{0}/03run.{1}'.format(pth, extension)
    rows= []
    if pth== '01down':
        for id,name,tab,cr,j in readCSV(file1,sep=sep):
            rows.append([id,name,tab,do if cr== '1' else 'NA',do,j,1 if cr in ('2', '3') else 0])
        with open(bat0,'wb') as b0,open(bat1,'wb') as b1,open(bat2,'wb') as b2,open(bat3,'wb') as b3:
            b0.write('')
            b1.write('{} ../00all/proc/01del.py {}'.format(executable, db))
            b1.write('\n{} ../00all/proc/01del.py {}_jail'.format(executable, db))
            b2.write('{} ./source/generate.py'.format(executable))
            b3.write('{} ./source/download.py'.format(executable))
    else:
        for name,ext,param,rescue in readCSV(file1,sep=sep):
            rows.append([name,ext,do,param,rescue])
        with open(bat0,'wb') as b0,open(bat1,'wb') as b1,open(bat2,'wb') as b2,open(bat3,'wb') as b3:
            b0.write('')
            b1.write('{} ../00all/proc/01del.py {}'.format(executable, db))
            b2.write('{} ../00all/proc/02gen.py {} {}'.format(executable, db,nom))
            b3.write('{} ../00all/proc/03run.py {}'.format(executable, db))
    writeCSV(file2,rows,sep=sep)
    if SYSTEM == "unix":
        os.chmod(file2, 0666)
        os.chown(file2, 1000, 1000)
        os.chmod(bat0, 0666)
        os.chown(bat0, 1000, 1000)
        os.chmod(bat1, 0666)
        os.chown(bat1, 1000, 1000)
        os.chmod(bat2, 0666)
        os.chown(bat2, 1000, 1000)
        os.chmod(bat3, 0666)
        os.chown(bat3, 1000, 1000)
writeCSV(components2,components,sep=sep)
if SYSTEM == "unix":
    os.chmod(components2, 0666)
    os.chown(components2, 1000, 1000)
    for file in ("../../execute.sh", "../log/log.txt", "../log/error.txt"):
        if not os.path.exists(file):
            os.mknod(file)
        os.chmod(file, 0766)
        os.chown(file, 1000, 1000)
    os.chmod('../../02nod/dades_noesb/nod_dextraccio.txt', 0666)
