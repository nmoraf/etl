from sisapUtils import sisap_, readCSV, execute, getOne, checkConnection, redicsShutdown, SYSTEM
from sys import argv
from os import system
from subprocess import Popen, PIPE
from datetime import datetime
from MySQLdb import escape_string


_db = argv[1]
_table = 'ctl_' + _db
_host = sisap_['hst']
_port = str(sisap_['prt'])
_user = sisap_['usr']
_call = {'sql': ['mysql', _db, '-u', _user, '-h', _host, '--port', _port], 'py': ['python' if SYSTEM == "unix" else 'c:\\Python27\\python']}


for file, extension, exe, param, delayable in readCSV('./proc/__source.txt', sep=';'):
    if exe == '1':
        if SYSTEM == "win":
            system('title {}.{}'.format(file, extension))
        execute("update {} set ts_start=current_timestamp, ts_finish=0, status=9, error='' where file = '{}' and extension = '{}' and param = '{}'".format(_table, file, extension, param), _db)
        proceed = True
        if delayable == '1':
            previs, = getOne('select count(1) from {} where status = 4'.format(_table), _db)
            funciona = checkConnection('redics')
            dia, hora = datetime.now().isoweekday(), datetime.now().hour
            if previs > 0 or not funciona or (dia == 6 and redicsShutdown() and hora in (21, 22, 23)):
                exit = 4
                error = 'delayed'
                proceed = False
        if proceed:
            ruta = './source/{}.{}'.format(file, extension)
            if extension == 'py':
                crida, info = _call[extension] + [ruta, param], None
            elif extension == 'sql':
                crida, info = _call[extension], 'source {}'.format(ruta)
            process = Popen(crida, stdout=PIPE, stdin=PIPE, stderr=PIPE, shell=SYSTEM == "win")
            output, errors = process.communicate(info)
            error = str(escape_string(errors))
            exit = process.returncode
            if errors or exit <> 0:
                exit = 1            
        execute("update {} set ts_finish=current_timestamp, status={}, error='{}' where file = '{}' and extension = '{}' and param = '{}'".format(_table, exit, error, file, extension, param), _db)
