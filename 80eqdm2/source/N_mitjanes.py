from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="eqdm2"
conn=connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/eqdm2_relacio.txt"
pathi="./dades_noesb/eqdm2_indicadors.txt"

criteris="nodrizas.eqa_criteris"
dext="nodrizas.dextraccio"

variables="dm2_variables"

dm2="mst_diabetics"

camps="a.id_cip_sec,a.up,a.edat,a.sexe,a.ates,a.institucionalitzat,a.maca"

with open(pathi, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      i,desc,tip = cat[0],cat[1],cat[2]
      if tip=="M":
         taula="%s" % i
         c.execute("drop table if exists %s" % taula)
         c.execute("create table %s (id_cip_sec int,up varchar(5), edat double, sexe varchar(1), ates double, institucionalitzat double, maca double,ctl int )" % taula)


with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11]
      if tipus=="M":
         taula="%s" % ind
         if agr=="anys":
            c.execute("insert into %s select %s, IF((YEAR(%s) - YEAR(%s)) <= 0,0,(YEAR(%s) - YEAR(%s)) - (MID(%s, 6, 5) < MID(%s, 6, 5))) ctl from %s a,%s" % (taula,camps,tmin,tmax,tmin,tmax,tmin,tmax,dm2,dext))
         else:
            c.execute("select distinct taula from %s where agrupador='%s'" % (criteris,agr))
            pres=c.fetchall()
            for i in pres:
               nod= i[0]
               if nod=="variables":
                  c.execute("insert into %s select %s, valor ctl from %s a where agrupador=%s and (valor between %s and %s)" % (taula,camps,variables,agr,vmin,vmax))
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")