from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="eqdm2"
conn=connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/eqdm2_relacio.txt"
pathi="./dades_noesb/eqdm2_indicadors.txt"

edats_k = "nodrizas.khx_edats5a"
dext="nodrizas.dextraccio"

pac = "mst_indicadors_pacient"

up = "eqa_khalix_up_pre"
khalix_pre = "eqa_khalix_up_ind"
khalix = "exp_khalix_up_ind"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"

catalegKhx = "exp_khalix_cataleg"

conceptes=[['NUM','num']]


c.execute("select concat(right(year(data_ext),2),if(month(data_ext) between 1 and 3,'T1',if(month(data_ext) between 4 and 6,'T2',if(month(data_ext) between 7 and 9,'T3',if(month(data_ext) between 10 and 12,'T4','err')))))  from %s" % dext)
periodo,=c.fetchone()

c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double)" % pac)
with open(pathi, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      i,desc,tip = cat[0],cat[1],cat[2]
      if tip=="I" or i=="codi":
         ok=1
      else:   
         c.execute("insert into %s select id_cip_sec, '%s', up,edat,sexe,ates,institucionalitzat,maca,ctl num from %s" % (pac,i,i))

c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,indicador,num from %s a inner join %s b on a.edat=b.edat" % (up,sexe,comb,pac,edats_k))

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (periode varchar(4),up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
   conc,var=r[0],r[1]
   c.execute("insert into %s select '%s',up,edat,sexe,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,periodo,conc,var,khalix_pre))
   c.execute("insert into %s select '%s',up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,periodo,conc,var,khalix_pre))

c.execute("drop table if exists %s" % catalegKhx)
c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'')" % catalegKhx)


with open(pathi, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      i,desc,tip = cat[0],cat[1],cat[2]
      if i=="codi":
         ok=1
      else:
         c.execute("insert into %s Values('%s','%s')" % (catalegKhx,i,desc))
    
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")