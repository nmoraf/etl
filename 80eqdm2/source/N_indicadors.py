from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="eqdm2"
conn=connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/eqdm2_relacio.txt"
pathi="./dades_noesb/eqdm2_indicadors.txt"

relacio1="dm2_relacio1"
relacio="dm2_relacio"
criteris="nodrizas.eqa_criteris"
dext="nodrizas.dextraccio"

problemes="dm2_problemes"
variables="dm2_variables"
tabac="dm2_tabac"

dm2="mst_diabetics"

camps="a.id_cip_sec,a.up,a.edat,a.sexe,a.ates,a.institucionalitzat,a.maca"

#agafem 12 mesos de variables i ultim valor
#tabac agafem situacio actual, independentment ultim control

c.execute("select date_add(date_add(data_ext, interval -1 year), interval +1 day),data_ext from %s" % (dext))
fa1any,avui,=c.fetchone()


with open(pathi, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      i,desc,tip = cat[0],cat[1],cat[2]
      if tip=="N":
         taula="%s" % i
         c.execute("drop table if exists %s" % taula)
         c.execute("create table %s (id_cip_sec int,up varchar(5), edat double, sexe varchar(1), ates double, institucionalitzat double, maca double,ctl int )" % taula)

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11]
      if tipus=="N":
         taula="%s" % ind
         if agr=="18":
            c.execute("insert into %s select %s,%s from %s a" % (taula,camps,f,dm2))
            c.execute("alter table %s add index(id_cip_sec,up,edat,sexe,ates,institucionalitzat,maca)" % taula)
         else:
            if agr=="sexe":
               c.execute("insert into %s select %s,%s from %s a where sexe='%s'" % (taula,camps,f,dm2,sexe))
            elif agr=="tabac":
               c.execute("insert into %s select %s,%s from %s a where tab between %s and %s" % (taula,camps,f,tabac,vmin,vmax))
            else:
               c.execute("select distinct taula from %s where agrupador='%s'" % (criteris,agr))
               pres=c.fetchall()
               for i in pres:
                  nod= i[0]
                  if nod=="problemes":
                     c.execute("insert into %s select %s,%s from %s a where ps=%s group by id_cip_sec" % (taula,camps,f,problemes,agr))
                  elif nod=="variables":
                     if vmax:
                        c.execute("insert into %s select %s,%s from %s a where agrupador=%s and (valor between %s and %s) group by id_cip_sec" % (taula,camps,f,variables,agr,vmin,vmax))
                     else:
                        c.execute("insert into %s select %s,%s from %s a where agrupador=%s group by id_cip_sec" % (taula,camps,f,variables,agr))

c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s(indicador varchar(10), ctl double)" % relacio1)
c.execute("drop table if exists %s" % relacio)
c.execute("create table %s(indicador varchar(10), ctl double)" % relacio)
                        
with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11]
      if tipus=="N" and f>"1":
         c.execute("insert into %s values('%s',%s)" % (relacio1,ind,f))

c.execute("insert into %s select indicador,max(ctl) ctl from %s group by indicador" % (relacio,relacio1))

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11]
      if tipus=="N" and f>"1":
         t1="%s_1" % ind
         t2="%s" % ind
         c.execute("drop table if exists %s" % t1)
         c.execute("create table %s like %s" % (t1,t2))
         c.execute("insert into %s select %s,count(distinct ctl) ctl from %s a group by id_cip_sec" % (t1,camps,t2))
         c.execute("drop table if exists %s" % t2)
         c.execute("create table %s (id_cip_sec int,up varchar(5), edat double, sexe varchar(1), ates double, institucionalitzat double, maca double,ctl int )" % t2)
         c.execute("insert into %s select %s,1 ctl from %s a, %s b where b.indicador='%s' and a.ctl>=b.ctl group by id_cip_sec" % (t2,camps,t1,relacio,t2))
         c.execute("drop table if exists %s" % t1)
     
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")