from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="eqdm2"
conn=connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/eqdm2_criteris_tractaments.txt"

nproblemes="nodrizas.eqa_problemes"
nvariables="nodrizas.eqa_variables"
ntabac="nodrizas.eqa_tabac"
assignada="nodrizas.assignada_tot"
dext="nodrizas.dextraccio"
ntractaments="import.tractaments"

criteris_tto="eqdm2_criteris_tractaments"
problemes="dm2_problemes"
variables="dm2_variables"
tabac="dm2_tabac"
pretto="tractaments"
tractaments="dm2_tractaments"

pre_dm2="diabetics"
criteri_dm2="ps=18"
dm2="mst_diabetics"
criteri_edat="(edat between 30 and 90)"
criteri_edatdx="(IF((YEAR(dde) - YEAR(data_naix)) <= 0,0,(YEAR(dde) - YEAR(data_naix)) - (MID(dde, 6, 5) < MID(data_naix, 6, 5))) between 30 and 90)"
criteri_ntractaments="ppfmc_pmc_data_ini <= data_ext"
criteri_dm2tractaments="tancat=0"

#agafem 12 mesos de variables i ultim valor
#tabac agafem situacio actual, independentment ultim control

c.execute("select date_add(date_add(data_ext, interval -1 year), interval +1 day),data_ext from %s" % (dext))
fa1any,avui,=c.fetchone()

c.execute("drop table if exists %s" % pre_dm2)
c.execute("create table %s (id_cip_sec int,dde date)" % pre_dm2)
c.execute("insert into %s select id_cip_sec,dde from %s where %s" % (pre_dm2,nproblemes,criteri_dm2))
c.execute("alter table %s add index(id_cip_sec,dde)" % pre_dm2)

c.execute("drop table if exists %s" % dm2)
c.execute("create table %s (id_cip_sec int,up varchar(5), dde date, data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double )" % dm2)
c.execute("insert into %s select a.id_cip_sec,up,dde,data_naix,edat,sexe,ates,institucionalitzat,maca from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where %s and %s" % (dm2,pre_dm2,assignada,criteri_edat,criteri_edatdx))
c.execute("alter table %s add index(id_cip_sec,up,dde,data_naix,edat,sexe,ates,institucionalitzat,maca)" % dm2)
c.execute("drop table if exists %s" % pre_dm2)

c.execute("drop table if exists %s" % problemes)
c.execute("create table %s (id_cip_sec int,up varchar(5), dde date, ps double,data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double )" % (problemes))
c.execute("insert into %s select a.id_cip_sec,b.up,a.dde,ps,b.data_naix,b.edat,b.sexe,ates,institucionalitzat,maca from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" % (problemes,nproblemes,dm2))
c.execute("alter table %s add index(id_cip_sec,up,dde,ps,data_naix,edat,sexe,ates,institucionalitzat,maca)" % problemes)

c.execute("drop table if exists %s" % variables)
c.execute("create table %s (id_cip_sec int,up varchar(5), agrupador double, valor double null, dat date not null default 0,data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double )" % (variables))
c.execute("insert into %s select a.id_cip_sec,b.up,a.agrupador,valor,data_var dat,b.data_naix,b.edat,b.sexe,ates,institucionalitzat,maca from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where usar=1 and (data_var between '%s' and '%s')" % (variables,nvariables,dm2,fa1any,avui))
c.execute("alter table %s add index(id_cip_sec,up,agrupador,valor,dat,data_naix,edat,sexe,ates,institucionalitzat,maca)" % variables)

c.execute("drop table if exists %s" % tabac)
c.execute("create table %s (id_cip_sec int,up varchar(5), tab double, dalta date not null default 0,dbaixa date not null default 0,dlast date not null default 0,data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double )" % (tabac))
c.execute("insert into %s select a.id_cip_sec,b.up,tab,dalta,dbaixa,dlast,b.data_naix,b.edat,b.sexe,ates,institucionalitzat,maca from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where data_ext between dalta and dbaixa" % (tabac,ntabac,dm2,dext))
c.execute("alter table %s add index(id_cip_sec,up,tab,dalta,dbaixa,dlast,data_naix,edat,sexe,ates,institucionalitzat,maca)" % tabac)

print 'inici tractaments: ' + strftime("%Y-%m-%d %H:%M:%S")

c.execute("drop table if exists %s" % criteris_tto)
c.execute("create table %s (atc7 varchar(7), descripcio varchar(300),agrupador double, tancat double)" % criteris_tto)
with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      atc7,desc,agr,tancat = cat[0],cat[1],cat[2],cat[3]
      if atc7=="ATC7":
         ok=1
      else:
         c.execute("insert into %s values('%s','%s',%s,%s)" % (criteris_tto,atc7,desc,agr,tancat))
c.execute("alter table %s add index(atc7,agrupador,tancat)" % criteris_tto)

c.execute("drop table if exists %s" % pretto)      
c.execute("create table %s (id_cip_sec int, agrupador double,tancat double)" % pretto)
c.execute("insert into %s select id_cip_sec, agrupador ,if(ppfmc_data_fi > data_ext,0,1) tancat from %s a inner join %s b on a.ppfmc_atccodi=atc7,%s where %s group by 1,2,3" % (pretto,ntractaments,criteris_tto,dext,criteri_ntractaments))

print 'intermig tractaments: ' + strftime("%Y-%m-%d %H:%M:%S")

c.execute("drop table if exists %s" % tractaments)
c.execute("create table %s (id_cip_sec int,up varchar(5), agrupador double,data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double )" % (tractaments))
c.execute("insert into %s select a.id_cip_sec,b.up,agrupador,b.data_naix,b.edat,b.sexe,ates,institucionalitzat,maca from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where %s" % (tractaments,pretto,dm2,criteri_dm2tractaments))
c.execute("alter table %s add index(id_cip_sec,up,agrupador,data_naix,edat,sexe,ates,institucionalitzat,maca)" % tractaments) 

print 'fi tractaments: ' + strftime("%Y-%m-%d %H:%M:%S")

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")