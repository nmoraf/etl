# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="agrupacions"
conn = connect((db,'aux'))
c=conn.cursor()

lab="nodrizas.lab_anual"

laburv="import.cat_sisap_catlab_urv"
urv="catLABURV"
centres="nodrizas.cat_centres"
dext="nodrizas.dextraccio"
#where="cr_codi_lab<>'RIMAP'"


desti1="lab_urv1"
desti2="lab_urv2"
desti21="lab_urv21"
desti3="lab_urv3"
desti4="lab_urv4"
desti5="mst_lab_urv"

urv_laboratori="exp_khalix_lab_urv"

ct="LABURV"

c.execute("drop table if exists %s" % urv)
c.execute("create table %s like %s" % (urv,laburv))
c.execute("insert into %s select * from %s" % (urv,laburv))
c.execute("alter table %s add index (cr_codi_prova_ics,agrupador,urv)" % urv)

c.execute("drop table if exists %s" % desti1)
c.execute("create table %s (id_cip_sec int, up varchar(5),codi_lab varchar(10),cr_codi_prova_ics varchar(10) not null default'',cr_data_reg date,mes int)" % desti1)
#c.execute("insert into %s select id_cip_sec,codi_up up,cr_codi_lab codi_lab,cr_codi_prova_ics,cr_data_reg,month(cr_data_reg) mes from %s,%s where %s and year(data_ext)=year(cr_data_reg)" % (desti1,lab,dext,where))
c.execute("insert into %s select id_cip_sec,codi_up up,cr_codi_lab codi_lab,cr_codi_prova_ics,cr_data_reg,month(cr_data_reg) mes from %s,%s where year(data_ext)=year(cr_data_reg)" % (desti1,lab,dext))


c.execute("drop table if exists %s" % desti2)
c.execute("create table %s (id_cip_sec int, up varchar(5),codi_lab varchar(10),agrupador varchar(10) not null default'',cr_data_reg date,mes int,valor double)" % desti2)
c.execute("insert into %s select id_cip_sec,up,codi_lab,agrupador,cr_data_reg,mes, urv valor from  %s a inner join %s b on a.cr_codi_prova_ics=b.cr_codi_prova_ics" % (desti2,desti1,urv))
c.execute("drop table if exists %s" % desti1)

c.execute("drop table if exists %s" % desti21)
c.execute("create table %s (id_cip_sec int, up varchar(5),codi_lab varchar(10),agrupador varchar(10) not null default'',cr_data_reg date,mes int,valor double)" % desti21)
c.execute("insert into %s select id_cip_sec,up,codi_lab,agrupador,cr_data_reg,mes, valor from  %s group by id_cip_sec,up,codi_lab,cr_data_reg,agrupador" % (desti21,desti2))
c.execute("drop table if exists %s" % desti2)

c.execute("drop table if exists %s" % desti3)
c.execute("create table %s (up varchar(5),codi_lab varchar(10),agrupador varchar(10) not null default'',mes int, n int,valor double, resultat double)" % desti3)
c.execute("insert into %s select up,codi_lab,agrupador,mes,count(*) n,valor,count(*)*valor resultat from %s  group by up,codi_lab,agrupador,mes" % (desti3,desti21))
c.execute("drop table if exists %s" % desti21)

c.execute("drop table if exists %s" % desti5)
c.execute("create table %s (up varchar(5),codi_lab varchar(10),mes int, urv double)" % desti5)
c.execute("insert into %s select up,codi_lab,mes,sum(resultat) from %s group by up,codi_lab,mes" % (desti5,desti3))
c.execute("drop table if exists %s" % desti3)

c.execute("drop table if exists %s" % desti4)
c.execute("create table %s (up varchar(5),codi_lab varchar(10),tipus varchar(10) not null default'',gener double,febrer double,marc double,abril double,maig double,juny double,juliol double,agost double,setembre double,octubre double,novembre double,desembre double)" % desti4)
c.execute("insert into %s select up,codi_lab,'%s', case when mes=1 then urv else 0 end as gener,case when mes=2 then urv else 0 end as febrer,case when mes=3 then urv else 0 end as marc,case when mes=4 then urv else 0 end as abril,case when mes=5 then urv else 0 end as maig,case when mes=6 then urv else 0 end as juny,case when mes=7 then urv else 0 end as juliol,case when mes=8 then urv else 0 end as agost,case when mes=9 then urv else 0 end as setembre,case when mes=10 then urv else 0 end as octubre,case when mes=11 then urv else 0 end as novembre,case when mes=12 then urv else 0 end as desembre from %s" %(desti4,ct,desti5))


c.execute("drop table if exists %s" % urv_laboratori)
c.execute("create table %s (up varchar(5),codi_lab varchar(10),tipus varchar(10) not null default'',gener double,febrer double,marc double,abril double,maig double,juny double,juliol double,agost double,setembre double,octubre double,novembre double,desembre double)" % urv_laboratori)
c.execute("insert into %s select up,codi_lab,tipus,sum(gener) gener, sum(febrer) febrer, sum(marc) marc, sum(abril) abril, sum(maig) maig, sum(juny) juny, sum(juliol) juliol, sum(agost) agost, sum(setembre) setembre, sum(octubre) octubre, sum(novembre) novembre, sum(desembre) desembre from %s group by up,codi_lab" % (urv_laboratori,desti4))
c.execute("drop table if exists %s" % desti4)

conn.close()

error= []
centres="export.khx_centres"

taula="%s.exp_khalix_lab_urv" % db
y = getKhalixDates()[1]
query = "select tipus, 'A{0}01', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', gener from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}02', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', febrer from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}03', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', marc from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}04', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', abril from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}05', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', maig from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}06', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', juny from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}07', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', juliol from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}08', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', agost from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}09', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', setembre from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}10', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', octubre from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}11', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', novembre from {1} a inner join {2} b on a.up=b.scs_codi \
         union select tipus, 'A{0}12', ics_codi, concat('L', codi_lab), 'NOCAT', 'NOIMP', 'DIM6SET', 'N', desembre from {1} a inner join {2} b on a.up=b.scs_codi".format(y, taula, centres)
file="LAB_URV_MENSUAL" 
error.append(exportKhalix(query,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

print strftime("%Y-%m-%d %H:%M:%S")
