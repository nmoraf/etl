# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime

db="agrupacions"
nod="nodrizas"

OutFile= tempFolder + 'proves.txt'
TaulaMy='exp_khalix_pro'


catedat='khx_edats5a'
proves='nod_proves'
oc_alt='nod_oc_altres'
dext='dextraccio'
khx_uba='export.khx_ubas'
sexe = "if(sexe='H','HOME','DONA')"

printTime('Inici Proves')
 
def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,uba,edat,{} from assignada_tot'.format(sexe)
    for id_cip_sec,up,uba,edat,sex in getAll(sql,nod):
         assig[int(id_cip_sec)]= {'up':up,'uba':uba,'edat':int(edat),'sex':sex}
    return assig 
    
gedat,br={},{}
sql='select edat,khalix from {}'.format(catedat)
for anys,khalix in getAll(sql,nod):
    gedat[int(anys)]=khalix
sql='select distinct scs_codi,ics_codi from cat_centres'
for up1,khx in getAll(sql,nod):
    br[up1]=khx   
sql="select year(data_ext) from {}".format(dext)
for a in getOne(sql,nod):
    anys=a

ass = getPacients()

def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text

dic_replace={".":"","-":""," ":"","_":""}  
    
printTime('Proves anual')
npro= {}
sql="select id_cip_sec,oc_data,homol_origen,inf_codi_prova, date_format(oc_data,'%m'),year(oc_data)from {0},{1} where oc_data<=data_ext and year(oc_data)='{2}'\
    union  select id_cip_sec,oc_data,homol_origen,inf_codi_prova, date_format(oc_data,'%m'),year(oc_data)from {3},{1} where oc_data<=data_ext and year(oc_data)='{2}'".format(proves,dext,anys,oc_alt)
for id,dat,serveio,serveid,mes,anysc in getAll(sql,nod):
    id=int(id)
    serveid=replace_all(serveid,dic_replace)
    try:
        up=ass[id]['up']
        e=gedat[ass[id]['edat']]
        uba=ass[id]['uba']
        s=ass[id]['sex']
    except KeyError:
        continue
    if (up,uba,e,s,serveid,serveio,mes,anysc) in npro:
        npro[(up,uba,e,s,serveid,serveio,mes,anysc)]['n']+=1
    else:
        npro[(up,uba,e,s,serveid,serveio,mes,anysc)]={'n':1}
gedat.clear()    
           
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,uba,e,s,serveid,serveio,mes,anysc),dict in npro.iteritems():
        num=dict['n']
        try:
            upkhx=br[up]
        except KeyError:
            continue
        w.writerow((up,upkhx,uba,mes,anysc,e,s,serveid,serveio,num))        
   

printTime('inici carrega')
sql= 'drop table if exists {}'.format(TaulaMy)
execute(sql,db)
sql= "create table {} ( up varchar(5) not null default '',br varchar(5) not null default '',uba varchar(5) not null default '',mes varchar(2) not null default'', anys varchar(4) not null default'',edat varchar(10) not null default'', sexe varchar(5),prova varchar(50) not null default'', serveio varchar(50) not null default'',num int)".format(TaulaMy)
execute(sql,db)
loadData(OutFile,TaulaMy,db)

npro.clear()  
br.clear()

printTime('export a Khalix')
error= []
taula="%s.%s" % (db,TaulaMy)

sql="select 'ORCLINIC',concat('A',right(anys,2),mes),br,concat('C',prova),edat,serveio,sexe,'N',sum(num) from {0} where prova <>'' group by br,mes,anys,prova,edat,serveio,sexe".format(taula)
filekhx="ORDRECLINICA_NUM" 
error.append(exportKhalix(sql,filekhx))


if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Fi proces') 
