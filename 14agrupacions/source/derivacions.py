# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime

db="agrupacions"
nod="nodrizas"

OutFile= tempFolder + 'der.txt'
TaulaMy='exp_khalix_der'


catedat='khx_edats5a'
der='nod_derivacions'
dext='dextraccio'
khx_uba='export.khx_ubas'
sexe = "if(sexe='H','HOME','DONA')"

printTime('Inici Derivacions')
 
def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,uba,edat,{} from assignada_tot_with_jail'.format(sexe)
    for id_cip_sec,up,uba,edat,sex in getAll(sql,nod):
         assig[int(id_cip_sec)]= {'up':up,'uba':uba,'edat':int(edat),'sex':sex}
    return assig 
    
gedat,br={},{}
sql='select edat,khalix from {}'.format(catedat)
for anys,khalix in getAll(sql,nod):
    gedat[int(anys)]=khalix
sql='select distinct scs_codi,ics_codi from cat_centres'
for up1,khx in getAll(sql,nod):
    br[up1]=khx  
sql='select distinct scs_codi,ics_codi from jail_centres'
for up1,khx in getAll(sql,nod):
    br[up1]=khx       
sql="select year(data_ext) from {}".format(dext)
for a in getOne(sql,nod):
    anys=a

ass = getPacients()

printTime('Derivacions anual')
nder= {}
sql="select id_cip_sec,oc_data,homol_origen,homol_desti,ciap2, date_format(oc_data,'%m'),year(oc_data)from {0},{1} where oc_data<=data_ext and year(oc_data)='{2}'".format(der,dext,anys)
for id,dat,serveio,serveid,mot,mes,anysc in getAll(sql,nod):
    if mot in ('NOCIAP', 'MULTCIAP') or not 30 <= int(mot[2:4] if mot[0:2]== 'YX' else mot[1:3]) <= 69:
        id=int(id)
        try:
            up=ass[id]['up']
            e=gedat[ass[id]['edat']]
            uba=ass[id]['uba']
            s=ass[id]['sex']
        except KeyError:
            continue
        if (up,uba,e,s,serveid,serveio,mot,mes,anysc) in nder:
            nder[(up,uba,e,s,serveid,serveio,mot,mes,anysc)]['n']+=1
        else:
            nder[(up,uba,e,s,serveid,serveio,mot,mes,anysc)]={'n':1}
gedat.clear()    
           
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,uba,e,s,serveid,serveio,mot,mes,anysc),dict in nder.iteritems():
        num=dict['n']
        try:
            upkhx=br[up]
        except KeyError:
            continue
        w.writerow((up,upkhx,uba,mes,anysc,e,s,serveid,serveio,mot,num))        
   

printTime('inici carrega')
sql= 'drop table if exists {}'.format(TaulaMy)
execute(sql,db)
sql= "create table {} ( up varchar(5) not null default '',br varchar(5) not null default '',uba varchar(5) not null default '',mes varchar(2) not null default'', anys varchar(4) not null default'',edat varchar(10) not null default'', sexe varchar(5),serveid varchar(50) not null default'', serveio varchar(50) not null default'', motiu varchar(50) not null default'', num int)".format(TaulaMy)
execute(sql,db)
loadData(OutFile,TaulaMy,db)

nder.clear()  
br.clear()

printTime('export a Khalix')
error= []
taula="%s.%s" % (db,TaulaMy)

sql="select 'DERIVACIO',concat('A',right(anys,2),mes),br,serveid,edat,serveio,sexe,'N',sum(num) from {0} group by br,mes,anys,serveid,edat,serveio,sexe".format(taula)
filekhx="DERIVACIO_NUM" 
error.append(exportKhalix(sql,filekhx))

sql="select replace(motiu,'.',''),concat('A','periodo'),br,serveid,edat,'NOIMP',sexe,'N',sum(num) from {0} where serveio in ('OMEDFAM','OPEDIAT','OODONTO','OINFERM','OALTEAP') group by motiu,br,serveid,edat,sexe".format(taula)
filekhx="DERIVACIO_MOT" 
error.append(exportKhalix(sql,filekhx,quarter=True))

sql="select 'DERIVACIO',concat('A','periodo','YTD'),concat(br,'M',a.uba),serveid,edat,if(serveio in ('OCIRUGI','ODERMAT','OGINECO','OOFTALM','OOTORRI','OREHABI','OSERVAC','OTRAUMA'),'OALTNOE',serveio),sexe,'N',sum(num) from {0} a \
 inner join {1} b on a.up=b.up and a.uba=b.uba where b.tipus='M' group by br,a.uba,serveid,edat,if(serveio in ('OCIRUGI','ODERMAT','OGINECO','OOFTALM','OOTORRI','OREHABI','OSERVAC','OTRAUMA'),'OALTNOE',serveio) ,sexe".format(taula,khx_uba)
filekhx="DERIVACIONS_UBA"
error.append(exportKhalix(sql,filekhx))

sql="select 'DERIVACIO',concat('A',right(anys,2),mes),br,'NUM','NOCAT','NOIMP','DIM6SET','N',sum(num) from {0} a inner join nodrizas.cat_centres b on a.up=b.scs_codi where serveio in ('OMEDFAM','OPEDIAT','OODONTO','OINFERM','OALTEAP') and motiu not in ('A98') group by br,mes,anys\
        union select 'DERIVACIO',concat('A',right(anys,2),mes),br,'DEN','NOCAT','NOIMP','DIM6SET','N', sum(num) from {0} a inner join nodrizas.cat_centres b on a.up=b.scs_codi where serveio in ('OMEDFAM','OPEDIAT','OODONTO','OINFERM','OALTEAP') group by br,mes,anys".format(taula)
filekhx="DERIVACIONS_CIAP2_A98"
error.append(exportKhalix(sql,filekhx))

sql="select 'DERIVACIO',concat('A','periodo','YTD'),concat(br,'M',uba),'NUM','NOCAT','NOIMP','DIM6SET','N',sum(num) from {0} a inner join nodrizas.cat_centres b on a.up=b.scs_codi where serveio in ('OMEDFAM','OPEDIAT','OODONTO','OINFERM','OALTEAP') and motiu not in ('A98') group by br,uba\
     union select 'DERIVACIO',concat('A','periodo','YTD'),concat(br,'M',uba),'DEN','NOCAT','NOIMP','DIM6SET','N',sum(num) from {0} a inner join nodrizas.cat_centres b on a.up=b.scs_codi  where serveio in ('OMEDFAM','OPEDIAT','OODONTO','OINFERM','OALTEAP') group by br,uba".format(taula)
filekhx="DERIVACIO_CIAP2_A98_UBA"
error.append(exportKhalix(sql,filekhx))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Fi proces') 
