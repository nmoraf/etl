from sisapUtils import *
from collections import defaultdict,Counter
import sys

nod = 'nodrizas'
db = 'agrupacions'
table = 'exp_prescripcions_'
origens = {
    'general': {'db':'import','assig':'assignada_tot','centres':'cat_centres'},
    'jail': {'db':'import_jail','assig':'jail_assignada','centres':'jail_centres'}
}

def get_atcs():
    klx = 'khalix'
    greater = getOne("select sym_index from icsap.klx_master_symbol where dim_index = 0 and sym_name = 'FARMATC'", klx)
    child_codes = []
    tmp = 'sisap_sym_tmp'

    def get_child(parent, i=1):
        createTable(tmp, '(sym number)', klx, rm=True)
        listToTable([[p] for p in parent], tmp, klx)
        child = defaultdict(list)
        child_continue = []
        for p, c in getAll('select parent_index, sym_index from icsap.klx_parent_child a, {} b where dim_index = 0 and a.parent_index = b.sym'.format(tmp), klx):
            child[p].append(c)
        if not child:
            child_codes.extend(parent)
            createTable(tmp, '(sym number)', klx, rm=True)
            listToTable([[c] for c in child_codes], tmp, klx)
            return True
        else:
            for p in parent:
                if p in child:
                    child_continue.extend(child[p])
                else:
                    child_codes.append(p)
            get_child(list(set(child_continue)), i++1)

    t = get_child(greater)
    atc = [a for a, in getAll('select sym_name from icsap.klx_master_symbol a, {} b where dim_index = 0 and a.sym_index = b.sym'.format(tmp), klx)]
    return atc

def typeConverter(tipus):

    return 'TRACINI' if tipus == 'I' else 'TRACACT'

go = get_atcs()        
dext, = getOne("select date_format(data_ext,'%Y%m') from dextraccio",nod)
error = []

for origen in origens.keys():
    
    centres = {}
    sql = 'select scs_codi,ics_codi from {}'.format(origens[origen]['centres'])
    for up,br in getAll(sql,nod):
        centres[up] = br
    
    assig = {}
    sql = 'select id_cip_sec,up,edat,sexe from {}'.format(origens[origen]['assig'])
    for id,up,edat,sexe in getAll(sql,nod):
        assig[id] = {'br':centres[up],'edat':ageConverter(edat,'g'),'sexe':sexConverter(sexe)}

    far = Counter()
    sql = "select id_cip_sec,ppfmc_atccodi,date_format(ppfmc_pmc_data_ini,'%Y%m'),if(ppfmc_data_fi>data_ext,1,0) from tractaments,nodrizas.dextraccio where ppfmc_pmc_data_ini<=data_ext"
    for id,atc,ini,obert in getAll(sql,origens[origen]['db']):
        tipus = []
        try:
            pob = assig[id]
        except KeyError:
            continue
        if atc not in go:
            continue
        if ini == dext:
            tipus.append('I')
        if obert == 1:
            tipus.append('P')
        if len(tipus) == 0:
            continue
        for tip in tipus:
            far[atc,pob['br'],tip,pob['edat'],pob['sexe']] += 1

    file = tempFolder + table + origen + '.txt'
    with openCSV(file) as c:
        for (atc,br,tipus,edat,sexe),count in far.items():
            c.writerow([atc,br,typeConverter(tipus),edat,sexe,count])
    
    execute('drop table if exists {}'.format(table + origen),db)
    execute('create table {} (atc varchar(7),br varchar(5),tipus varchar(9),edat varchar(6),sexe varchar(4),total int)'.format(table + origen),db)
    loadData(file,table + origen,db)

    sql = "select atc,concat('A','periodo'),br,tipus,edat,'NOIMP',sexe,'N',total from {}.{}".format(db,table + origen)
    file = 'TRACTAMENTS' + ('_JAIL' if origen == 'jail' else '')
    error.append(exportKhalix(sql,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")