from sisapUtils import getAll, multiprocess, sectors, yearsBetween, getOne, createTable, listToTable, ageConverter, exportKhalix
from collections import defaultdict


tb = 'exp_visites'
db = 'agrupacions'
file = 'VISITES_CMBDAP'
especialitats = {'10999': 'AGDMET',
                 '30999': 'AGDINF',
                 '10888': 'AGDPED',
                 '10777': 'AGDODO',
                 '10117': 'AGDMET',
                 '10116': 'AGDMIR',
                 '05999': 'AGDASS',
                 '10106': 'AGDODO',
                 '30888': 'AGDAIN',
                 '30085': 'AGDLLE',
                 '': 'AGNOCAT',
                 'other': 'AGDALT'}


def get_data(sector):
    centres = {up: br for (up, br) in getAll('select scs_codi, ics_codi from cat_centres', 'nodrizas')}
    dext, = getOne('select year(data_ext) from dextraccio', 'nodrizas')
    dades = defaultdict(int)
    sql = "select id_cip_sec, str_to_date(cp_d_naix, '%Y%m%d'), cp_sexe, cp_ecap_dvis, cp_up_ecap, trim(cp_t_act), cp_ecap_categoria, cp_tprof \
           from cmbdap_s{}, nodrizas.dextraccio where cp_ecap_dvis between '{}0101' and last_day(adddate(data_ext, interval -1 month))".format(sector, dext)
    for id, dnaix, sex, dvis, up, tip, espe, tprof in getAll(sql, 'import'):
        if dvis > dnaix and up in centres:
            periode = 'A{}{}'.format(str(dvis.year)[2:], str(dvis.month).zfill(2))
            especialitat = especialitats[espe] if espe in especialitats else especialitats['other']
            tipprof = 'TIPPROF{}'.format(tprof)
            key = ('VISAP{}'.format(tip), periode, centres[up], especialitat, ageConverter(yearsBetween(dnaix, dvis), 5), tipprof, 'DONA' if sex == '1' else 'HOME', 'N')
            dades[key] += 1
    listToTable([key + (n,) for (key, n) in dades.items()], tb, db)


if __name__ == '__main__':
    createTable(tb, '(a varchar(10), b varchar(10), c varchar(10), d varchar(10), e varchar(10), f varchar(10), g varchar(10), h varchar(10), n int)', db, rm=True)
    multiprocess(get_data, sectors, 8)
    exportKhalix('select * from {}.{}'.format(db, tb), file)
