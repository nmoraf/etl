from sisapUtils import *
from collections import defaultdict,Counter
import sys

nod = 'nodrizas'
db = 'agrupacions'
table = 'exp_problemes_'
origens = {
    'general': {'db':'import','assig':'assignada_tot','centres':'cat_centres'},
    'jail': {'db':'import_jail','assig':'jail_assignada','centres':'jail_centres'}
}

def discardCode(codi,sexe,edat):

    if codi == 'NOCIAP':
        return False
    elif 30 <= int(codi[2:4] if codi[0:2]== 'YX' else codi[1:3]) <= 69:
        return True
    elif sexe == 'H' and (codi[0:1] == 'W' or codi[0:1] == 'X'):
        return True
    elif sexe == 'D' and codi[0:1] == 'Y' and codi[1:2] != 'X':
        return True
    elif edat in ('EC01','EC24','EC59'):
        for x in ('W','X01','X02','X03','X04','X05','X06','X07','X08','X09','X10','X11','X12','X13','X70','X71','X73','X89','X91','X92','Y70','Y71','Y72','Y76','Z12','Z16'):
            if x in codi:
                return True
    else:
        return False
        
def typeConverter(tipus):

    return 'DIAGNOU' if tipus == 'N' else 'DIAGACTIU'
    
codeConverter = defaultdict(list)
sql = "select codi_cim10,if(sexe='','B',sexe),codi_ciap_m from cat_md_ct_cim10_ciap"
for cim10,sexe,ciap in getAll(sql,'import'):
    ciap = ciap.replace('.','')
    if sexe == 'B':
        codeConverter[cim10,'H'].append(ciap)
        codeConverter[cim10,'D'].append(ciap)
    else:
        codeConverter[cim10,sexe].append(ciap)
        
sql = "select date_format(data_ext,'%Y%m') from dextraccio"
dext, = getOne(sql,nod)

new = []
error = []

for origen in origens.keys():
    
    centres = {}
    sql = 'select scs_codi,ics_codi from {}'.format(origens[origen]['centres'])
    for up,br in getAll(sql,nod):
        centres[up] = br
    
    assig = {}
    sql = 'select id_cip_sec,up,edat,sexe from {}'.format(origens[origen]['assig'])
    for id,up,edat,sexe in getAll(sql,nod):
        assig[id] = {'br':centres[up],'edat':ageConverter(edat,5),'sexe':sexe}

    ps = Counter()
    done = {}
    sql = "select id_cip_sec,pr_cod_ps,date_format(pr_dde,'%Y%m'),if(pr_dba=0 or pr_dba>data_ext,0,1) from problemes,nodrizas.dextraccio where pr_cod_o_ps='C' and pr_hist=1 and pr_dde<=data_ext"
    for id,cim10,dde,dba in getAll(sql,origens[origen]['db']):
        tipus = []
        try:
            pob = assig[id]
        except KeyError:
            continue
        if dde == dext:
            tipus.append('N')
        if dba == 0:
            tipus.append('A')
        if len(tipus) == 0:
            continue
        try:
            ciap = codeConverter[cim10,pob['sexe']]
        except KeyError:
            if (cim10,pob['sexe']) not in new:
                new.append((cim10,pob['sexe']))
            continue
        for codi in ciap:
            if not discardCode(codi,pob['sexe'],edat):    
                for tip in tipus:
                    ok = False
                    if codi[1:3] == '99':
                        ok = True
                    else:
                        if (id,tip,codi) not in done:
                            ok = True
                            done[id,tip,codi] = True
                    if ok:
                        ps[tip,pob['br'],codi,pob['edat'],pob['sexe']] += 1

    file = tempFolder + table + origen + '.txt'
    with openCSV(file) as c:
        for (tipus,br,codi,edat,sexe),count in ps.items():
            c.writerow([codi,br,typeConverter(tipus),edat,sexConverter(sexe),count])
    
    execute('drop table if exists {}'.format(table + origen),db)
    execute('create table {} (ciap varchar(6),br varchar(5),tipus varchar(9),edat varchar(6),sexe varchar(4),total int)'.format(table + origen),db)
    loadData(file,table + origen,db)

    sql = "select ciap,concat('A','periodo'),br,tipus,edat,'NOIMP',sexe,'N',total from {}.{} where tipus='DIAGNOU'".format(db,table + origen)
    file = 'DIAGNOU' + ('_JAIL' if origen == 'jail' else '')
    error.append(exportKhalix(sql,file))

    sql = "select ciap,concat('A','periodo'),br,tipus,edat,'NOIMP',sexe,'N',total from {}.{} where tipus='DIAGACTIU'".format(db,table + origen)
    file = 'DIAG_ACTIU' + ('_JAIL' if origen == 'jail' else '')
    error.append(exportKhalix(sql,file,quarter=True))
    
sql = 'select codi_cim10, ps_def, codi_ciap_m, desc_ciap_m from cat_md_ct_cim10_ciap inner join cat_prstb001 on codi_cim10 = ps_cod'
file = 'CIE10_CIAP2_cataleg'
error.append(exportKhalix(sql, file))

if len(new)> 0:
    codis+= "\r\n\r\nCodis cim10:\r\n\r\n" + '\r\n'.join(map(str,new))
    sendSISAP(['framospe@gencat.cat','mfabregase@gencat.cat','lmendezboo@gencat.cat'],'Nous CIM10 epidades','Paqui',codis)                

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
