import itertools as it
import requests as r

import sisapUtils as u


tb = "sisap_xml"
tb2 = "sisap_xml_cb"
db = "redics"

banned = ("CIPPacient", "CIPPacient14", "NomPacient")

sectors = u.sectors + ['6951']


def main():
    cols = "(xml_cip varchar2(40), codi_sector varchar2(4), xml_id number(12),\
             xml_origen varchar2(10), xml_tipus_orig varchar2(10), \
             xml_data_alta date, xml_up varchar2(5), camp_codi varchar2(255), \
             camp_valor varchar2(4000))"
    parts = ', '.join(["partition s{0} values ('{0}')".format(sector)
                       for sector in sectors])
    partitions = " partition by list (codi_sector) ({})".format(parts)
    for table in (tb, tb2):
        u.createTable(table, cols + partitions, db, rm=True)
        u.grantSelect(table, ("PREDUMMP", "PREDUPRP", "PREDUEHE", "PREDULMB"), db)  # noqa
    u.multiprocess(do_sector, list(it.product(sectors, range(2))))
    for table in (tb, tb2):
        u.calcStatistics(table, db)
    get_metadata()
    u.sendGeneral("SISAP <sisap@gencat.cat>", "ffinaaviles@gencat.cat",
                  "ecomaredon@gencat.cat", "Taula copiada a REDICS",
                  "S\"ha generat la taula {} a partir dels sectors".format(tb))

def do_sector(params):
    sector, grup = params
    if grup == 0:
        sql = "select xml_id, xml_cip, xml_origen, xml_tipus_orig, \
                      xml_data_alta, xml_up, p.xml_xml_1.getClobVal() \
               from prstb690 p \
               where xml_origen = 'GABINETS'"
        upload = []
        upload2 = []
        i = 0
        for id, cip, orig, tipus, dat, up, xml in u.getAll(sql, sector + "af"):
            regs = xml.read().split("<Camp Id")
            for reg in regs[1:]:
                camps = reg.split("\"")
                cod, val = camps[1], camps[3][:4000]
                if val and cod not in banned:
                    this = (cip, sector, id, orig, tipus, dat, up, cod, val)
                    upload.append(this)
                    if "|" in val:
                        for element in val.split("|"):
                            that = (cip, sector, id, orig, tipus, dat, up, cod, element)  # noqa
                            upload2.append(that)
                    i += 1
                    if i % 10**5 == 0:
                        upload_it(upload, tb, db)
                        upload_it(upload2, tb2, db)
                        upload = []
                        upload2 = []
        upload_it(upload, tb, db)
        upload_it(upload2, tb2, db)
    elif grup == 1:
        sql_101 = "select usua_cip, usua_cip_cod from pdptb101"
        anonymizer = {cip: hash for (cip, hash)
                      in u.getAll(sql_101, sector + "a")}
        sql = "select vu_cod_u, vu_cod_vs, vu_dat_act, \
                      vu_up, vu_xml \
               from prstb017 \
               where (vu_cod_vs in ( \
                        select vs_cod from prstb004 \
                        where vs_url is not null and vs_udm like 'CATEG%') or \
                      vu_cod_vs = 'IP2702') and \
                     vu_xml is not null"
        upload = []
        j = 0
        for i, (cip, var, dat, up, xml) in enumerate(u.getAll(sql, sector), 1):
            hash = anonymizer.get(cip)
            if hash:
                id = "{}{}".format(sector, str(i).zfill(8))
                regs = xml.split("<item>")
                for reg in regs[1:]:
                    values = []
                    for word in ("codigo", "select"):
                        inici = reg.find("<{}>".format(word)) + len(word) + 2
                        final = reg.find("</{}>".format(word))
                        values.append(reg[inici:final])
                    this = [hash, sector, id, 'VARIABLES', var, dat, up] + values  # noqa
                    upload.append(this)
                    j += 1
                    if j % 10**5 == 0:
                        upload_it(upload, tb, db)
                        upload = []
        upload_it(upload, tb, db)


def upload_it(data, tab, db):
    try:
        u.listToTable(data, tab, db)
    except Exception:
        n = 100
        for minidata in [data[i:i + n] for i in xrange(0, len(data), n)]:
            try:
                u.listToTable(minidata, tab, db)
            except Exception:
                z = 10
                for microdata in [minidata[j:j + z] for j in xrange(0, len(minidata), z)]:  # noqa
                    u.listToTable(microdata, tab, db)


def clean_and_split(text):
    new = []
    for field in text.replace("\n", "").replace("\r", "").split("@"):
        new_field = field.strip().decode("utf8").encode("latin1", errors="ignore")
        if new_field and new_field[0] == "'":
            new_field = new_field[1:-1]
        new.append(new_field)
    return new


def get_metadata():
    credentials = {"EntradaUsuario": "lmendez", "EntradaClave": "lmendez"}
    url_credentials = "http://p400.ecap.intranet.gencat.cat/ecap/ajuda/gestio/validacio.php"  # noqa
    url = "http://p400.ecap.intranet.gencat.cat/ecap/ajuda/gestio/XMLexportCSV.php"  # noqa
    with r.Session() as s:
        s.post(url_credentials, data=credentials)
        page = s.get(url)
    for i, taula in enumerate(page.content.split("<h4>")):
        if i > 0:
            data = []
            for j, row in enumerate(taula.split("<br>")):
                if j == 0:
                    titol = row[:row.find("</h4>")]
                    headers = clean_and_split(row[row.find("</h4>") + 5:])
                elif j + 1 < len(taula.split("<br>")):
                    data.append(clean_and_split(row))
            name = titol.decode("utf8").encode("ascii", "ignore").replace(" ", "_").lower()  # noqa
            table_name = "sisap_xml_{}".format(name)[:30]
            columns = ", ".join(["{} varchar2(1024)".format(header.replace("'", "")) for header in headers])  # noqa
            table_columns = "({})".format(columns)
            u.createTable(table_name, table_columns, db, rm=True)
            upload_it(data, table_name, db)
            u.grantSelect(table_name, ("PREDULMB", "PREDUPRP"), db)
            u.calcStatistics(table_name, db)


if __name__ == "__main__":
    main()
