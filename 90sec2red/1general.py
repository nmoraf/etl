from sisapUtils import getTableColumns, getColumnInformation, createTable, grantSelect, sectors, getAll, getOne, listToTable, calcStatistics, sendGeneral
from time import sleep


tables = [
    {'table': 'pactb100i', 'cip': 'cip', 'sector': 'usua_sector_login', 'select': ('usua_com_email', 'usua_sms', ), 'where': ' where usua_sector_login is not null', 'do': True, },
    {'table': 'prstb208', 'cip': 'cui_usu', 'do': True, },
    {'table': 'prstb218', 'cip': 'mi_cip', 'do': True, },
    {'table': 'prstb224', 'cip': 'eco_cip', 'grants': ('PREDUEHE', 'PREDUMMP', ), 'do': True, },
    {'table': 'prstb380', 'cip': 'af_cip', 'do': True},
    {'table': 'ppftb301', 'cip': None, 'do': True, },
    {'table': 'pritb940', 'cip': 'conv_cip', 'grants': ('PREDUMMP', 'PREDUPRP'), 'try': True, 'do': True, },
    {'table': 'pritb941', 'cip': None, 'grants': ('PREDUMMP', 'PREDUPRP'), 'try': True, 'do': True, },
    {'table': 'pritb942', 'cip': None, 'grants': ('PREDUMMP', 'PREDUPRP'), 'try': True, 'do': True, },
    {'table': 'pritb943', 'cip': 'acc_cip', 'grants': ('PREDUMMP', 'PREDUPRP'), 'try': True, 'do': True, },
    {'table': 'usutb040', 'cip': 'usua_cip', 'select': ('usua_nis', 'usua_cip_rca', ), 'usua_cip_rca': {'query': 'decode(usua_cip_rca, \'\', 0, 1)', 'create': 'usua_cip_rca int'}, 'eapp': True, 'do': True, },
    {'table': 'vistb016', 'cip': 'tract_cip', 'select': ('tract_seq', 'tract_cod_up', 'tract_num_col', 'tract_data_creacio', 'tract_data_fi', ), 'eapp': True, 'do': True, },
    {'table': 'vistb220', 'cip': 'sgtr_cip', 'select': ('sgtr_tract_seq', 'sgtr_num_col', 'sgtr_data', 'sgtr_num_dosi', ), 'eapp': True, 'do': True, },
    {'table': 'usutb040', 'cip': 'usua_cip', 'select': ('usua_uab_up', 'usua_cip_rca', 'usua_nass1', 'usua_data_alta', ),
     'usua_cip_rca': {'query': 'decode(usua_cip_rca, \'\', 0, 1)', 'create': 'usua_cip_rca int'}, 'usua_nass1': {'query': 'decode(usua_nass1, null, 0, 1)', 'create': 'usua_nass1 int'},
     'usua_data_alta': {'query': "to_date(usua_data_alta, 'J')", 'create': 'usua_data_alta date'},
     'where': " where usua_residencia = 'C01' and usua_servei_origen = 1 and usua_dup is null and usua_uab_up is not null and usua_situacio = 'A' and usua_data_alta > 2456658", 'do': True, },
    {'table': 'usutb042', 'cip': 'sr_cip', 'select': ('sr_tipus_baixa', ), 'where': ' where sr_data_baixa is null', 'do': True, },
    {'table': 'prstb057', 'cip': 'vaf_usua_cip', 'select': ('vaf_cod', 'vaf_data_vac', ), 'where': ' where vaf_data_vac < add_months(sysdate, 1) and vaf_data_baixa is null', 'do': True, },
    {'table': 'vistb408', 'cip': None, 'select': ('ag_data', 'ag_id_modul', 'ag_id_tram', 'ag_for'), 'ag_data': {'query': "to_date(ag_data, 'J')", 'create': 'ag_data date'}, 'where': " where to_date(ag_data, 'J') < current_date + 32", 'do': True, },
    {'table': 'prstb285', 'cip': None, 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb001', 'cip': None, 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb007', 'cip': 'fit_cip', 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb008', 'cip': None, 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb009', 'cip': None, 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb013', 'cip': None, 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb016', 'cip': 'pf_cip', 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'rehtb017', 'cip': None, 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'prstb690', 'cip': 'xml_cip', 'select': ('xml_origen', 'xml_tipus_orig', 'xml_data_alta', 'xml_up', ), 'do': True},
    {'table': 'ppftb300', 'cip': 'fit_cip', 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'ppftb080', 'cip': 'so_cip', 'grants': ('PREDUPRP', ), 'do': True},
    {'table': 'pritb142', 'cip': 'condet_cip', 'eapp': True, 'do': True, },
    {'table': 'usutb196a', 'cip': 'com22_cip', 'eapp': True, 'do': True, },
    {'table': 'ppftb302', 'cip': None, 'do': True, },
]

db_origen = 'sectors'
db_desti = 'redics'


def get_anonymizer():
    anonymizer = {}
    sql = 'select usua_cip,usua_cip_cod from pdptb101'
    for cip, hash in getAll(sql, 'pdp'):
        anonymizer[cip] = hash
    return anonymizer


def upload_it(data, tab, db):
    try:
        listToTable(data, tab, db)
    except:
        n = 100
        for minidata in [data[i:i + n] for i in xrange(0, len(data), n)]:
            try:
                listToTable(minidata, tab, db)
            except:
                z = 10
                for microdata in [minidata[j:j + z] for j in xrange(0, len(minidata), z)]:
                    listToTable(microdata, tab, db)


anonymizer = get_anonymizer()

status = {}
for table in [table for table in tables if table['do']]:
    try:
        table_origen = table['table']
        table_desti = 'sisap_eapp_{}'.format(table_origen) if 'eapp' in table else 'sisap_{}'.format(table_origen)
        cip = table['cip']
        cip_str = cip if cip else ''
        if 'sector' in table:
            sec = table['sector']
            origens = ('6734', )
        elif 'eapp' in table:
            sec = ''
            origens = ('6951', )
        else:
            sec = ''
            origens = sectors
        select = getTableColumns(table_origen, db_origen, ) if 'select' not in table else table['select']
        columns = [table[column] if column in table else getColumnInformation(column, table_origen, db_origen, desti='ora', ) for column in select if column.lower() not in (cip_str.lower(), sec.lower(), )]
        if cip:
            create = '({cip} varchar2(40),codi_sector varchar2(4),{columns})'
            sql = 'select {cip},{{sector}},{columns} from {table}{where}'
        else:
            create = '(codi_sector varchar2(4),{columns})'
            sql = 'select {{sector}},{columns} from {table}{where}'
        createTable(table_desti, create.format(cip=cip, columns=','.join([column['create'] for column in columns])), db_desti, rm=True, )
        if 'grants' in table:
            grantSelect(table_desti, table['grants'], db_desti, )

        sql = sql.format(cip=cip, columns=','.join([column['query'] for column in columns]), table=table_origen, where='' if 'where' not in table else table['where'])
        for sector in origens:
            if 'try' in table:
                try:
                    n, = getOne("select 1 from all_tables where table_name='{}'".format(table_origen.upper()), sector)
                except TypeError:
                    continue
            this = sql.format(sector='{}'.format(sector) if sec == '' else sec)
            data = []
            i = 0
            for row in getAll(this, sector):
                if cip:
                    try:
                        cod = anonymizer[row[0][:13]]
                    except KeyError:
                        continue
                    else:
                        data.append((cod, ) + row[1:])
                        i += 1
                else:
                    data.append(row)
                    i += 1
                if i % (5 * 10**5) == 0:
                    upload_it(data, table_desti, db_desti)
                    data = []
            upload_it(data, table_desti, db_desti)

        calcStatistics(table_desti, db_desti, )
    except:
        status[table_desti] = False
    else:
        status[table_desti] = True

status_global = all(status.values())
sendGeneral('SISAP <sisap@gencat.cat>', 'ffinaaviles@gencat.cat', 'ecomaredon@gencat.cat',
            'Taules copiades EXP -> REDICS (status {})'.format(status_global), '\r\n'.join(['{} - {}'.format(t, s) for t, s in status.items()]))
