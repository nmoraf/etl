from sisapUtils import *

atcs = []
sql = 'select criteri_codi from eqa_criteris where agrupador=80'
for atc, in getAll(sql,'nodrizas'):
    atcs += [atc]

pfs = []
getATC = {}    
sql = 'select pf_codi,pf_cod_atc from cpftb006 where pf_cod_atc in {}'.format(str(tuple(atcs)))
for pf,atc in getAll(sql,'redics'):
    pfs += [pf]
    getATC[pf] = atc

table = 'sisap_prescripcio_historica'
db = 'redics'
try:
    execute('drop table {}'.format(table),db)
except:
    pass
execute('create table {} (pmc_usuari_cip varchar2(40),codi_sector varchar2(4),ppfmc_pf_codi number(6),pf_cod_atc varchar2(7),ppfmc_pmc_data_ini date,ppfmc_data_fi date,ppfmc_durada number(4),ppfmc_freq number(5),ppfmc_posologia number(8,3),ppfmc_pmc_amb_cod_up varchar2(5),ppfmc_pmc_amb_num_col varchar2(9))'.format(table),db)
    
for sector in sectors:
    n = 0
    tables = ('ppftb165', 'ppftb166')
    try:
        sql = 'select 1 from {}'
        for t in tables:
            n += getOne(sql.format(t), sector)[0]
    except:
        pass
    if n == len(tables):
        coded = {}
        sql = 'select usua_cip,usua_cip_cod from pdptb101'
        for cip,cod in getAll(sql,'{}a'.format(sector)):
            coded[cip] = cod
        
        getCIP = {}
        sql = 'select pmc_codi,pmc_usuari_cip from ppftb165'
        for pmc,cip in getAll(sql,sector):
            try:
                getCIP[pmc] = coded[cip]
            except KeyError:
                continue

        data = []
        sql = 'select ppfmc_pmc_codi,ppfmc_pf_codi,ppfmc_pmc_data_ini,ppfmc_data_fi,ppfmc_durada,ppfmc_freq,ppfmc_posologia,ppfmc_pmc_amb_cod_up,ppfmc_pmc_amb_num_col from ppftb166 where ppfmc_pf_codi in {}'.format(str(tuple(pfs)))
        for pmc,pf,ini,fi,dur,freq,pos,up,col in getAll(sql,sector):
            try:
                hash = getCIP[pmc]
            except KeyError:
                continue
            else:
                data.append([hash,sector,pf,getATC[pf],ini,fi,dur,freq,pos,up,col])
       
        uploadOra(data,table,db)

calcStatistics(table,db)
sendGeneral('SISAP <sisap@gencat.cat>','ffinaaviles@gencat.cat','ecomaredon@gencat.cat','Taula copiada a REDICS','S\'ha generat la taula {} a REDICS a partir dels sectors'.format(table))
