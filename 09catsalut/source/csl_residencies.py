import collections as c
import pandas as pd
import xlrd

import sisapUtils as u


tb_pac = 'mst_indicadors_pacient'
tb_klx = 'exp_khalix_up_ind'
db = 'catsalut'
ind = 'IAP25'
nod = 'nodrizas'


file = 'dades_noesb/DUP_CATALEG_EP-UP_VIGENTS_RESIDENCIES_70-71.xlsx'
assistides = set([str(up).zfill(5)
                  for up in pd.read_excel(file)['cod_Unitat Proveidora']])

atdom = set([id for id, in u.getAll('select id_cip_sec from eqa_problemes \
                                     where ps = 45', nod)])
sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, ates, \
              institucionalitzat, up_residencia, maca \
       from assignada_tot_with_jail where institucionalitzat = 1 and edat > 14"
pacients = []
khalix = c.Counter()
for (id, up, uba, ubainf, edat, sexe, ates, instit,
     residencia, maca) in u.getAll(sql, nod):
    indicadors = ['{}A'.format(ind)]
    if residencia in assistides:
        indicadors.append('{}B'.format(ind))
    for indi in indicadors:
        pacients.append((id, indi, up, uba, ubainf, edat, sexe, ates, instit,
                         maca, int(id in atdom), 1, 0))
        edat = u.ageConverter(edat)
        sexe = u.sexConverter(sexe)
        grups = ['INSASS', 'INSAT'] if ates else ['INSASS']
        for grup in grups:
            khalix[(up, edat, sexe, grup, indi, 'DEN')] += 1
            if id in atdom:
                khalix[(up, edat, sexe, grup, indi, 'NUM')] += 1

u.execute("delete from {} where indicador like '{}%'".format(tb_pac, ind), db)
u.listToTable(pacients, tb_pac, db)
u.execute("delete from {} where indicador like '{}%'".format(tb_klx, ind), db)
u.listToTable([key + (n,) for key, n in khalix.items()], tb_klx, db)
