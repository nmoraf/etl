from sisapUtils import getAll, getOne, yearsBetween, ageConverter, sexConverter, createTable, listToTable, execute, writeCSV, tempFolder
from collections import defaultdict


indicadors = {'T02BISA': {'dx': [(625,), (626, 627)], 'px': [(632,)], 'edat': (20, 200)},
              'T02BISB': {'dx': [(633,), (635, 634), (626, 634)], 'px': [], 'edat': (20, 200)}, }


nod = 'nodrizas'
db = 'catsalut'
tb = 'csl_cmbdh'
klx = 'exp_khalix_up_ind'


def get_denominador():
    denominador = {indicador: defaultdict(int) for indicador in indicadors}
    sql = 'select up, edat, sexe from assignada_tot_with_jail'
    for up, edat, sexe in getAll(sql, nod):
        for indicador, criteris in indicadors.items():
            if is_between(edat, criteris['edat']):
                denominador[indicador][(up, ageConverter(edat, 5), sexConverter(sexe))] += 1
    return denominador


def get_ingressos():
    dext, = getOne('select data_ext from dextraccio', nod)
    ups = set([up for up, in getAll('select scs_codi from cat_centres_with_jail', nod)])
    demografiques = {}
    ingressos = defaultdict(set)
    sql = 'select ingres, agr, up, naix, sexe from eqa_ingressos'
    for ingres, agrupador, up, dnaix, sexe in getAll(sql, nod):
        if up in ups:
            edat = yearsBetween(dnaix, dext)
            if ingres not in demografiques:
                demografiques[ingres] = (up, edat, sexe)
            ingressos[ingres].add(agrupador)
    return demografiques, ingressos


def get_procediments():
    procediments = defaultdict(set)
    sql = 'select ingres, agr from eqa_procediments'
    for ingres, agrupador in getAll(sql, nod):
        procediments[ingres].add(agrupador)
    return procediments


def is_between(v, r):
    return r[0] <= v <= r[1]


def is_included(valors, criteris):
    included = False
    for criteri in criteris:
        if all([codi in valors for codi in criteri]):
            included = True
    return included


def get_numerador():
    numerador = {indicador: defaultdict(int) for indicador in indicadors}
    for ingres, dx in ingressos.items():
        up, edat, sexe = demografiques[ingres]
        px = procediments[ingres]
        for indicador, criteris in indicadors.items():
            if is_between(edat, criteris['edat']) and is_included(dx, criteris['dx']) and not is_included(px, criteris['px']):
                numerador[indicador][(up, ageConverter(edat, 5), sexConverter(sexe))] += 1
    return numerador


def upload_data():
    createTable(tb, '(indicador varchar(10), up varchar(5), edat varchar(10), sexe varchar(10), tipus varchar(10), recompte int)', db, rm=True)
    for indicador in indicadors:
        listToTable([(indicador,) + k + ('NUM', n) for k, n in numerador[indicador].items()], tb, db)
        listToTable([(indicador,) + k + ('DEN', n,) for k, n in denominador[indicador].items()], tb, db)


def export_klx():
    for indicador in indicadors:
        execute("delete from {} where indicador = '{}'".format(klx, indicador), db)
        execute("insert into {} select up, edat, sexe, 'NOINSASS', indicador, tipus, recompte from {} where indicador = '{}'".format(klx, tb, indicador), db)


def explore_data():
    file = tempFolder + 'T02bis.csv'
    brs = {up: br for (up, br) in getAll("select scs_codi, ics_codi from cat_centres_with_jail where ics_codi like 'BR%'", nod)}
    data = {}
    total = {indicador: [0, 0] for indicador in indicadors}
    for indicador, up, tipus, n in getAll('select indicador, up, tipus, recompte from {}'.format(tb), db):
        if up in brs:
            id = (indicador, up)
            if id not in data:
                data[id] = [0, 0]
            tipus = 0 if tipus == 'NUM' else 1
            data[id][tipus] += n
            total[indicador][tipus] += n
    resul = [(indicador, brs[up], num, den, str(round(1000.0 * num / den, 2)).replace('.', ',')) for (indicador, up), (num, den) in data.items()]
    writeCSV(file, sorted(resul), sep=';')
    for indicador, valors in total.items():
        print indicador, valors[0], valors[1], 1000.0 * valors[0] / valors[1]


if __name__ == '__main__':
    denominador = get_denominador()
    demografiques, ingressos = get_ingressos()
    procediments = get_procediments()
    numerador = get_numerador()
    upload_data()
    export_klx()
    # explore_data()
