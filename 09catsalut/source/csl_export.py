# coding: utf8

"""
.
"""

import collections as c
import datetime as d

import sisapUtils as u


FILE = "SIAC_ECAP_{}{}.txt"


class CatSalut(object):
    """."""

    def __init__(self):
        """."""
        self.dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        self.dades = c.defaultdict(c.Counter)
        self.get_centres()
        self.get_catsalut()
        self.get_eqa()
        self.get_pedia()
        self.get_icam()
        self.get_econsulta()
        self.get_accessibilitat()
        self.get_longitudinalitat()
        self.get_upload()
        self.export_data()

    def get_centres(self):
        """."""
        self.conversio = {}
        self.linies = set()
        sql = "select ics_codi, scs_codi from cat_centres"
        for br, up in u.getAll(sql, "nodrizas"):
            self.conversio[br] = up
            if "BN" in br:
                self.linies.add(up)

    def get_catsalut(self):
        """."""
        codis = {"IAP01": "AP01", "IAP11BIS": "AP011bis", "IAP18": "AP18"}
        pobs = {"IAP01": ("NOINSATRC", "INSATRC"),
                "IAP11BIS": ("NOINSAT", "INSAT"),
                "IAP18": ("NOINSASS", "INSASS")}
        sql = "select indicador, comb, up, conc, n \
               from exp_khalix_up_ind_def \
               where indicador in {}".format(tuple(codis))
        for ind, pob, up, analisi, n in u.getAll(sql, "catsalut"):
            if pob in pobs[ind]:
                self.dades[(codis[ind], up)][analisi] += n

    def get_eqa(self):
        """."""
        codis = {"EQA0306": "AP16", "EQA0208": "AP19", "EQA0210": "RS_AP01",
                 "EQA0301": "RS_AP02", "EQA0204": "AP20", "EQA0209": "AP21",
                 "EQA0312": "AP22"}
        sql = "select left(indicador, 7), up, conc, n from exp_khalix_up_ind \
               where left(indicador, 7) in {} and \
                     comb in ('NOINSAT', 'INSAT')".format(tuple(codis))
        for ind, up, analisi, n in u.getAll(sql, "eqa_ind"):
            self.dades[(codis[ind], up)][analisi] += n

    def get_pedia(self):
        """."""
        sql = "select id_cip_sec, up_rca from assignada_tot \
               where up in {} and \
                     up_rca not in ('', up)".format(tuple(self.linies))
        relacio = {id: up for (id, up) in u.getAll(sql, "nodrizas")}
        sql = "select id_cip_sec, up, num, den \
               from mst_indicadors_pacient \
               where indicador = 'EQA0708' and \
                     ates = 1 and maca = 0 and excl = 0"
        for id, up, num, den in u.getAll(sql, "pedia"):
            if up in self.linies:
                if id in relacio:
                    rca = relacio[id]
                else:
                    continue
            else:
                rca = up
            self.dades[("AP04", rca)]["NUM"] += num
            self.dades[("AP04", rca)]["DEN"] += den

    def get_icam(self):
        """."""
        codis = ("IT003OST", "IT003MEN", "IT003TRA", "IT003SIG")
        sql = "select entity, analisi, valor from exp_khalix_it \
               where indicador in {} and \
                     length(entity) = 5".format(codis)
        for br, analisi, n in u.getAll(sql, "altres"):
            self.dades[("SGAM02-AP", self.conversio[br])][analisi] += n

    def get_econsulta(self):
        """."""
        sql = "select br, analisis, n from exp_khalix_up_econsulta \
               where indicador like 'ECONS0001%'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP26bis", up)][analisi] += n

    def get_accessibilitat(self):
        """."""
        codis = {"QACC5D": "AP23", "QACC10D": "AP24"}
        sql = "select k0, k2, k3, v from exp_qc_forats \
               where k4 = 'ANUAL' and \
                     k0 in {}".format(tuple(codis))
        for ind, br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[(codis[ind], up)][analisi] += n

    def get_longitudinalitat(self):
        """."""
        sql = "select up, analisi, resultat from exp_long_cont_up \
               where indicador ='CONT0002'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP25", up)][analisi] += n

    def get_upload(self):
        """."""
        self.upload = []
        especials = {"AP18": 1}
        for (ind, up), dades in self.dades.items():
            if especials.get(ind, 0) >= self.dext.month:
                periode = self.dext.year - 1
            else:
                periode = self.dext.year
            estat = "D" if especials.get(ind, 12) == self.dext.month else "P"
            num = dades["NUM"]
            den = dades["DEN"]
            if num and den:
                res = str(round(num / float(den), 4)).replace(".", ",")
            else:
                res = 0
            this = (periode, ind, up, res, int(num), int(den),
                    self.dext.strftime("%d/%m/%Y"), "", estat,
                    d.date.today().strftime("%d/%m/%Y"))
            self.upload.append(this)

    def export_data(self):
        """."""
        mes = self.dext.strftime("%m")
        file = FILE.format(self.dext.year, mes)
        u.writeCSV(u.tempFolder + file, self.upload, sep=";")
        text = 'Adjuntem arxiu SIAC_ECAP SISAP corresponent a {}/{}.'.format(mes, self.dext.year)
        u.sendGeneral('SISAP <sisap@gencat.cat>', 'ecros@catsalut.cat','sisap@gencat.cat', 'Dades SIAC_ECAP SISAP','Benvolguts,\r\n\r\n{}\r\n\r\nSalutacions cordials,\r\n\r\nSISAP'.format(text), u.tempFolder + file)
        u.sshPutFile(file, "export", "catsalut", subfolder="ICS/")
        u.sshChmod(file, "export", "catsalut", 0666, subfolder="ICS/")


if __name__ == "__main__":
    if u.IS_MENSUAL:
        CatSalut()
