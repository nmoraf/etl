from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

debug = False

# DBs

nod = 'nodrizas'
db = 'catsalut'
imp = 'import'

indicadors = ['IAP14','IAP09','IAP10','IT23']
resultats = Counter()
contraI = {}
OutFile = tempFolder + 'dens_cmbd.txt'


# Tables

criteris = 'eqa_criteris'
relacio = 'eqa_relacio'
cmbd = 'cmbdap'
dext = 'nodrizas.dextraccio'
centres = 'export.khx_centres'

TableMy = 'exp_khalix_csl_cmbdap'

# Dictionary

DenCSLDict = {
        'default': {
            'eqa': 1,
            'tipus': 'den',
            'ates': 1,
            'edat_min': 15,
            'edat_max': 80,
            'sexe': 0
        },
        'IAP14': {
            'indic': 'EQA0313',
            'tipus': 'ci',
            'edat_min': 50,
            'edat_max': 200,
            'sexe': 'H'
        },
        'IAP09': {
            'indic': 'EQA0213',
         },
        'IAP10': {
            'indic': 'EQA0209',
        },
        'IT23': {
            'eqa': 0,
            'indic': '62',
            'ates': 0,
            'edat_min': 40,
        },
}

def getEQA(indicador):

    try:
        a = DenCSLDict[indicador]['eqa']
    except:
        a = DenCSLDict['default']['eqa']
    return a
    
def getTipus(indicador):

    try:
        a = DenCSLDict[indicador]['tipus']
    except:
        a = DenCSLDict['default']['tipus']
    return a
    
def getAtes(indicador):

    try:
        a = DenCSLDict[indicador]['ates']
    except:
        a = DenCSLDict['default']['ates']
    return a
    
def getEdatMin(indicador):

    try:
        a = DenCSLDict[indicador]['edat_min']
    except:
        a = DenCSLDict['default']['edat_min']
    return a
    
def getEdatMax(indicador):

    try:
        a = DenCSLDict[indicador]['edat_max']
    except:
        a = DenCSLDict['default']['edat_max']
    return a
    
def getIndicador(indicador):

    try:
        a = DenCSLDict[indicador]['indic']
    except:
        a = DenCSLDict['default']['indic']
    return a

def getSexe(indicador):

    try:
        a = DenCSLDict[indicador]['sexe']
    except:
        a = DenCSLDict['default']['sexe']
    return a