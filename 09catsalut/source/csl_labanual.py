from sisapUtils import *
from collections import defaultdict
import csv, os

OutFile= tempFolder + 'csl_lab_anual.txt'
tableMain="mst_csl_main"
table= 'csl_lab_anual'
nod= 'nodrizas'
db='catsalut'

indicador="VISAN"

catcen="cat_centres_with_jail"
lab="lab_anual"
dext="dextraccio"

printTime('inici calcul') 
centres={}
c=0
sql="select scs_codi from {}".format(catcen)
for up, in getAll(sql,nod):
    centres[up]=up

lany1,lany={},{}
sql="select id_cip,cr_data_reg,codi_up from {}".format(lab)
for id_cip_sec,data,up in getAll(sql,nod):
    try:
        lany1[(id_cip_sec,data,up)]=1
    except KeyError:
        continue
        
for (id_cip_sec,data,up),n in lany1.items(): 
    if up in lany:
        lany[up]['n']+=1
    else:
        try:
            lany[up]={'n':1}
        except KeyError:
            ok=1
lany1.clear()
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for codi_up,d in lany.items():
        try:
            up=centres[codi_up]
        except KeyError:
            continue
        try:
            w.writerow([up,indicador,d['n']])
        except KeyError:
            continue
execute('drop table if exists {}'.format(table),db)
execute('create table {} (up varchar(5),indicador varchar(10),recompte int)'.format(table),db)
loadData(OutFile,table,db)
sql= "create table if not exists {} (up varchar(5) not null default'',ates double,indicador varchar(20) not null default'',valor double)".format(tableMain)
execute(sql,db)
sql="delete a.* from {0} a where indicador='{1}'".format(tableMain,indicador)
execute(sql,db)
sql= "insert into {0} select up,1,indicador,recompte from {1}".format(tableMain,table)
execute(sql,db)
lany.clear()
printTime('fi calcul') 