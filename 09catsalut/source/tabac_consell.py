from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
#Validation
file_name="COMUNICATS_JAIL"
validation=True
up_codi=("00380","00368","00378")

nod="nodrizas"
alt = "altres"
db = "catsalut"
agr=827
codi_indicador="IAP11BIS"

def get_date_dextraccio():
    """Consigue la fecha de hace 12 meses y la fecha actual.
        Devuelve a tuple de dos fechas en in datetime.date format
    """
    sql="select date_add(date_add(data_ext, interval - 12 month), interval + 1 day), data_ext from nodrizas.dextraccio;"
    for past, current in getAll(sql,nod):
        return past,current



def get_fumadors(past_date):
    """Consigue a los pacientes con estado de fumador diagnosticado en los ultimos 12 meses
       Devuelve un set de pacient ids.
    """
    sql="select id_cip_sec, dalta,dbaixa from {}.eqa_tabac where tab=1;".format(nod)
    return {id for (id,dalta,dbaixa) in getAll(sql,nod) if dalta < past_date < dbaixa  }

def get_consell(agr,current_date):
    """Consigue a los pacientes que han recibido consejo para dejar de fumar en el ultimo any.
       Devuelve un set de pacient ids.
    """
    sql="select id_cip_sec,data_var from {}.eqa_variables where agrupador={}".format(nod,agr)
    return {id for (id,dalta) in getAll(sql,nod) if monthsBetween(dalta,current_date) < 12  }

def get_rows(denominador,numerador):
    """ Construye las rows de la tabla final a nivel de paciente. Solo entran los ids en el denominador y la columna num
        se cambia a 1 si esta en numerador.
        Devuelve una lista de tuples (rows).
    """
    sql="select id_cip_sec, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca from {}.assignada_tot where edat >= 15 and ates=1".format(nod)
    rows=[]
    for id, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca  in getAll(sql,nod):
        if id in denominador:
            num=0
            if id in numerador:
                num=1
            rows.append((id, codi_indicador,up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca, num,1, 0))
    return rows


def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)
	
	
if __name__ == '__main__':

    printTime("Inicio")
    past_date,current_date=get_date_dextraccio()
    print(current_date)
	
    denominador=get_fumadors(past_date)
    printTime("Fumadores")
    numerador=get_consell(agr,current_date)
    printTime("Consejo")

    rows=get_rows(denominador,numerador)
    taula_name="indicador_{}".format(codi_indicador)
    columns="(id_cip_sec int, indicador varchar(10),up varchar(8), uba varchar(8),ubainf varchar(8),edat int, sexe varchar(5),ates int, institucionalitzat int, maca int,num int, den int, excl int)"
    export_table(taula_name,
				columns,
				db,
				rows)
	
	#query_template_string="select indicador,concat('A','periodo'),ics_codi,{strg},'NOCAT','NOIMP','DIM6SET','N',{var} from altres.{taula} a inner join nodrizas.jail_centres b on a.up=scs_codi where a.{var} != 0"
	#query_string=query_template_string.format(strg="'NUM'",var="numerador",taula=taula_name)+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=taula_name)
	#print(query_string)
	#exportKhalix(query_string,file_name)