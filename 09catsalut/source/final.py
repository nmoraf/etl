from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="catsalut"
conn = connect((db,'aux'))
c=conn.cursor()

lligats="csl_lligats"
IAP09 = "csl_IAP09"
IAP08 = "csl_visites"
IAP016 = "csl_iap016"

path="./dades_noesb/csl_ind.txt"

pac = "mst_indicadors_pacient"
pacass = "mst_indicadors_pacient_assir"
pacass1 = "indicadors_pacient_assir"
pobassir="nodrizas.ass_imputacio_up"
assignadass="nodrizas.ass_poblacio"
edats_k = "nodrizas.khx_edats5a"
centresass="nodrizas.ass_centres"

up = "csl_khalix_up_pre"
khalix_pre = "csl_khalix_up_ind"
khalix = "exp_khalix_up_ind"
upass = "csl_khalix_upass_pre"
khalix_preass = "csl_khalix_upass_ind"
khalixass = "exp_khalix_upass_ind"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
combass ="NOINSAT"
#condicio_khalix = "excl=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
condicio_khalix = "excl=0"
#condicio_khalix_pob = "institucionalitzat=1 or (institucionalitzat=0 and maca=0)"

conceptes=[['NUM','num'],['DEN','den']]

c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(5) not null default '',ubainf varchar(5) not null default '',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double)" % pac)
with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      indicador="indicador_%s" % ind
      if ind=="ICMBDAP01":
         c.execute("insert into %s select id_cip_sec,'%s' indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,0 excl from %s" % (pac,ind,lligats))
      elif ind=="IAP09":
         c.execute("insert into %s select id_cip_sec,indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,0 excl from %s" % (pac,IAP09))
      elif ind=="IAP08":
         c.execute("insert into %s select id_cip_sec,indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,0 excl from %s" % (pac,IAP08))
      elif ind=="IAP016":
         c.execute("insert into %s select id_cip_sec,indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,excl from %s" % (pac,IAP016))            
      elif grup=="CSLA01": 
         c.execute("insert into %s select a.id_cip_sec,indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,excl from %s a " % (pac,indicador))

c.execute("drop table if exists %s" % pacass1)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',num double,den double,excl double,llistat double)" % pacass1)

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      indicador="indicador_%s" % ind
      if grup=="CSLA02":
         c.execute("insert into %s select a.id_cip_sec,'%s',visi_up up, num,1 den, excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" %(pacass1,ind,indicador,pobassir))

       
c.execute("drop table if exists %s" % pacass)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',edat double,sexe varchar(1) not null default'',num double,den double,excl double,llistat double)" % pacass)         
with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      indicador="indicador_%s" % ind
      if grup=="CSLA02":
         c.execute("insert into %s select a.id_cip_sec,indicador,up,edat,'D' sexe, num,den,excl,%s llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s'" % (pacass,llistat,pacass1,assignadass,ind))

         
c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(10) not null default '',indicador varchar(10) not null default'',num double,den double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,comb,pac,edats_k,condicio_khalix))

up_rca = {id: rca for (id, rca) in getAll("select id_cip_sec, up_rca from assignada_tot where edat < 15 and up in (select scs_codi from cat_centres where ics_codi like 'BN%') and up_rca <> '' and up_rca not in (select scs_codi from cat_centres where ics_codi like 'BN%')", "nodrizas")}
dades = [(row[0], up_rca[row[0]], row[2], row[3], row[4] + "RC", row[5], row[6], row[7]) for row in getAll("select * from {} where edat in ('EC01', 'EC24', 'EC59', 'EC1014')".format(up), db) if row[0] in up_rca]
listToTable(dades, up, db)

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(10) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(10) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))         
         

c.execute("drop table if exists %s" % upass)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',indicador varchar(10) not null default'',num double,den double)" % upass)        
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (upass,sexe,pacass,edats_k,condicio_khalix))       

c.execute("drop table if exists %s" % khalix_preass)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_preass)
c.execute("insert into %s select up,edat,sexe,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4" % (khalix_preass,upass))
c.execute("drop table if exists %s" % khalixass)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(10) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalixass)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up_assir,edat,sexe,'%s' comb,indicador,'%s',sum(%s) n from %s a inner join %s b on a.up=b.up group by 1,2,3,4,5" % (khalixass,combass,conc,var,khalix_preass,centresass)) 

print strftime("%Y-%m-%d %H:%M:%S")
