from sisapUtils import *
import sys
import collections as c

db= "catsalut"

error= []
centres="nodrizas.cat_centres_with_jail"
centresass="nodrizas.ass_centres"

taula="%s.exp_khalix_upass_ind" % db
query="select indicador,concat('B','periodo'),br_assir,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=b.up_assir group by indicador,concat('A','periodo'),br_assir,conc,edat,comb,sexe,'N',n" % {'taula': taula,'centres':centresass}
file="CATSALUT_ASSIR_NOU" 
error.append(exportKhalix(query,file))

origen = "exp_khalix_up_ind"
desti = origen + "_def"
linies = set([cod for cod, in getAll("select scs_codi from cat_centres where tip_eap = 'N'", "nodrizas")])
sql = "select * from {}".format(origen)
dades = c.Counter()
for up, edat, sexe, pob, ind, var, val in getAll(sql, db):
    if val:
        dades[(up, edat, sexe, pob, ind, var)] += val
        if up not in linies and "RC" not in pob:
            dades[(up, edat, sexe, pob + "RC", ind, var)] += val
createTable(desti, "like {}".format(origen), db, rm=True)
listToTable([k + (v,) for (k, v) in dades.items()], desti, db)

query="select indicador,concat('B','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from {}.{} a inner join {} b on a.up=scs_codi where comb not like '%RC'".format(db, desti, centres)
file="CATSALUT_NOU" 
error.append(exportKhalix(query,file))

query="select indicador,concat('B','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from {}.{} a inner join {} b on a.up=scs_codi where comb like '%RC'".format(db, desti, centres)
file="CATSALUT_NOU_RC" 
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_up_ind" % db
query="select indicador,concat('B','periodo'),aga,conc,edat,comb,sexe,'N', sum(n) from %(taula)s a inner join nodrizas.cat_centres b on a.up=scs_codi group by indicador, aga, conc, edat,comb,sexe" % {'taula': taula}
file="CATSALUT_NOU_AGA" 
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_uba_ind" % db
query="select indicador,concat('A','periodo'),concat(ics_codi,tipus,uba),analisis,'NOCAT',detalle,'DIM6SET','N',valor from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres':centres}
file="CATSALUT_UBA_NOU"
error.append(exportKhalix(query,file))

taula="%s.mst_csl_main" % db
query="select indicador,concat('A','periodo'),ics_codi,'NOCLI','NOCAT','NOIMP','DIM6SET','N',valor from %(taula)s a inner join %(centres)s b on a.up=scs_codi where ates=1" % {'taula': taula,'centres':centres}
file="CATSALUT" 
error.append(exportKhalix(query,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
