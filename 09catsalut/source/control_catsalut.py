# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els indicadors de la carpeta CATSALUT
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'mst_control_eqa'
tb_cat = 'cat_control_indicadors'
db_origen = "catsalut"

"""Taules origen"""
tb_ind = "exp_khalix_up_ind_def"


periode,= u.getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')

class LadyBug_altres(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """."""
        self.indicadors, self.resultats, self.cataleg = [], Counter(), {}
        self.get_territoris()
        self.get_catsalut_ind()
        self.export_data()
        
    def get_territoris(self):
        """Obtenim àmbits i sector"""
        self.centres, self.centresSCS = {}, {}
        sql = 'select ics_codi, scs_codi, amb_desc, sector from cat_centres'
        for br, up, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
            self.centresSCS[up] = {'sector': sector, 'ambit': amb}
            
    def get_catsalut_ind(self):
        """."""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "Catsalut_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, conc, n from {} where comb='NOINSAT'".format(tb_ind)
        for indicador, up, conc, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            if indicador in self.cataleg:
                if conc == 'DEN':
                    self.resultats[periode, sector, ambit, indicador, 'den'] += n
                if conc == 'NUM':
                    self.resultats[periode, sector, ambit, indicador, 'num'] += n
    
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indicadors = tuple(self.indicadors)
        sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(tb, periode, indicadors)
        u.execute(sql, db)
        u.listToTable(upload, tb, db)
        
        upload_cat = []
        for (codi), dadesc in self.cataleg.items():
            desc = dadesc['d']
            pare = dadesc['p']
            upload_cat.append([codi, desc, pare])

        sql = "delete a.* from {0} a where indicador in {1}".format(tb_cat, indicadors)
        u.execute(sql, db)
        u.listToTable(upload_cat, tb_cat, db)
   
if __name__ == '__main__':
    LadyBug_altres()
    