from sisapUtils import *
import csv,os
from time import strftime
from collections import defaultdict
print strftime("%Y-%m-%d %H:%M:%S")

db="catsalut"
conn = connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/csl_ind.txt"
pathr="./dades_noesb/csl_relacio.txt"

nod ='nodrizas'
ass_emb="nodrizas.ass_embaras"
visites="nodrizas.ass_visites"
dext="nodrizas.dextraccio"
tabac="nodrizas.eqa_tabac"
ass_tabac="ass_tabac"
variables="nodrizas.ass_variables"
agrupador_puerperi=389
poblacio="nodrizas.ass_poblacio"

puerperi="ass_puerperi"
embaras="csl_embaras"
visembaras1="csl_visites_embarassades1"
visembaras="csl_visites_embarassades"
den="indicador"

with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus=="ASS":
         t="%s_%s" % (den,ind)
         t2="%s_%s_1" % (den,ind)
         c.execute("drop table if exists %s" % t)
         c.execute("create table %s (id_cip_sec int,num double,den double,excl double)" % t)
         c.execute("drop table if exists %s" % t2)
         c.execute("create table %s (id_cip_sec int)" % t2)
         
c.execute("select date_add(data_ext,interval - 12 month),date_add(data_ext,interval - 15 day),date_add(data_ext, interval + 6 month),date_add(data_ext,interval - 3 year),data_ext,date_add(data_ext,interval - 75 day) from %s" % (dext))
fa1any,fa15dies,en6mesos,fa3anys,avui,fa75dies,=c.fetchone()

c.execute("drop table if exists %s" % embaras)
c.execute("create table %s like %s" % (embaras,ass_emb))
c.execute("insert into %s select * from %s where inici>'%s' or fi >'%s'"  % (embaras,ass_emb,fa1any,fa1any))
c.execute("alter table %s add index(id_cip_sec,inici,fi,risc)" % embaras)

c.execute("drop table if exists %s" % puerperi)
c.execute("create table %s (id_cip_sec int, dat date)" % puerperi)
c.execute("insert into %s select id_cip_sec,dat from %s where agrupador=%s " % (puerperi,variables,agrupador_puerperi))
c.execute("alter table %s add index (id_cip_sec,dat)" % puerperi)

#creo taula visites de dones assir

c.execute("drop table if exists %s" % visembaras1)
c.execute("create table %s (id_cip_sec int, risc varchar(15),inici date, fi date,visi_data_visita date,sg double)" % visembaras1)
c.execute("insert into %s select a.id_cip_sec,risc,inici,fi,visi_data_visita,(datediff(fi,visi_data_visita))/7 as sg from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where visi_data_visita between inici and fi" % (visembaras1,embaras,visites))
c.execute("drop table if exists %s" % visembaras)
c.execute("create table %s (id_cip_sec int, risc varchar(15),recompte double)" % visembaras)
c.execute("insert into %s select id_cip_sec,risc,count(*) recompte from %s group  by id_cip_sec,risc" % (visembaras,visembaras1))

c.execute("drop table if exists %s" % ass_tabac)
c.execute("create table %s (id_cip_sec int, agrupador int,val_txt varchar(10) not null default'', embaras int, puerperi int)" % ass_tabac)
c.execute("insert into %s select a.id_cip_sec,agrupador,val_txt,if(dat between inici and fi, 1, 0) embaras, if(dat between date_add(fi, interval 35 day) and date_add(fi, interval 75 day), 1, 0) puerperi from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" % (ass_tabac,variables,embaras))

#algunes definicions

sql = 'select data_ext from dextraccio'
for dat, in getAll(sql, nod):
    ddext = dat

def get_consumOH():
    ConsumOH = {}
    sql = 'select id_cip_sec, data_var from eqa_variables where agrupador=84'
    for id, data in getAll(sql, nod):
        mesos = monthsBetween(data, ddext)
        if 0 <= mesos <= 23:
            ConsumOH[id] = True
    sql = 'select id_cip_sec, dat from {} where agrupador=391'.format(variables)
    for id, data in getAll(sql, nod):
        mesos = monthsBetween(data, ddext)
        if 0 <= mesos <= 23:
            ConsumOH[id] = True
    sql = 'select id_cip_sec from eqa_problemes where ps = 85'
    for id, in getAll(sql, nod):
        ConsumOH[id] = True
    return ConsumOH


def get_TosFerina():
    tos_ferina = {}
    sql = 'select id_cip_sec, datamax from eqa_vacunes where agrupador=631'
    for id, data in getAll(sql, nod):
        tos_ferina[id] = {'data': data}
    return tos_ferina
    
    
with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      t="%s_%s" % (den,ind)
      t2="%s_%s_1" % (den,ind)
      if tipus=="ASSRISC":
         c.execute("insert into %s select id_cip_sec from %s where recompte>=%s" % (t2,visembaras,vmax))
         c.execute("alter table %s add index(id_cip_sec)" % t2)
         c.execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where risc like ('%%alt%%') and fi<'%s'" % (t,embaras,avui))
         c.execute("alter table %s add index(id_cip_sec)" % t)
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set den=den+1 where sg<=12" % (t,visembaras1))
         c.execute("update %s a set den=0 where den<>2" % (t))
         c.execute("update %s a set den=1 where den=2" % (t))        
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2))
      elif tipus=="ASSTABAC":
         c.execute("insert into %s select a.id_cip_sec from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where tab=%s and dalta between inici and fi " % (t2,embaras,tabac,vmax))
         c.execute("alter table %s add index(id_cip_sec)" % t2)
         c.execute("insert into %s select a.id_cip_sec, 0 num,1 den, 0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where tab=1 and dalta<inici and dbaixa>inici group by id_cip_sec" % (t,embaras,tabac))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2))
         c.execute("alter table %s add index(id_cip_sec)" % t)
      elif tipus=="ASSTABACA":
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=%s where %s=1 and agrupador=%s and val_txt='%s'" % (t,ass_tabac,vmax,tmin,agr,vmin))
      elif tipus=="ASS1VIS": 
         c.execute("insert into %s select id_cip_sec from %s a where visi_data_visita between inici and date_add(inici,interval + %s week)" % (t2,visembaras1,tmax))
         c.execute("alter table %s add index(id_cip_sec)" % t2)
         c.execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where inici > date_add('%s',interval - 10 week)" % (t,embaras,fa1any))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2))
      elif tipus=="ASSPUERP": 
         c.execute("insert into %s select distinct a.id_cip_sec from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where dat between fi and date_add(fi,interval + %s day)" % (t2,embaras,puerperi,tmax))
         c.execute("alter table %s add index(id_cip_sec)" % t2)
         c.execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where fi <= '%s'" % (t,embaras,fa15dies))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2))
      elif tipus=="ASSCITO": 
         c.execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where edat between %s and %s" % (t,poblacio,emin,emax))
         c.execute("alter table %s add index(id_cip_sec)" % t)
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador=203 and  STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%%d-%%M-%%Y') between '%s' and '%s'" % (t,variables,fa3anys,avui))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador=204 and val_num=1 and dat between '%s' and '%s'" % (t,variables,fa3anys,avui))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador in ('203','400')  and dat between '%s' and '%s'" % (t,variables,fa3anys,avui))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador=205  and STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%%d-%%M-%%Y') between '%s' and '%s'" % (t,variables,avui,en6mesos))
      elif tipus=="OH":
         upload = []
         ConsumOH = get_consumOH()
         sql = 'select id_cip_sec from {}'.format(embaras)
         for id, in getAll(sql, db):
            denom, num, excl = 1, 0, 0
            if id in ConsumOH:
                num = 1
            upload.append([id, num, denom, excl])
         listToTable(upload, t, db)
      elif tipus == "DTPA":
        upload = []
        tos_ferina = get_TosFerina()
        sql = 'select id_cip_sec, inici, fi from {} where temps >265'.format(embaras)
        for id, inici, final in getAll(sql, db):
            denom, num, excl = 1, 0, 0
            if id in tos_ferina:
                dtos = tos_ferina[id]['data']
                if inici <= dtos <= final:
                    num =1
            upload.append([id, num, denom, excl])
        listToTable(upload, t, db)
      elif tipus == 'IVE':
        denom = set()
        num = set()
        visites = defaultdict(set)
        sql = 'select id_cip_sec, visi_data_visita from ass_visites'
        for id, dat in getAll(sql, nod):
            visites[id].add(dat)
        sql = "select id_cip_sec, dat from ass_variables where agrupador = 636 and val_num = 0"
        for id, dat in getAll(sql, nod):
            denom.add(id)
            if any([0 < (vis - dat).days < 30 for vis in visites[id]]):
                num.add(id)
        resul = [(id, 1 * (id in num), 1, 0) for id in denom]
        listToTable(resul, t, db)
      elif tipus == "copiar":
        if vmax == 'den':
            c.execute('insert into {} select id_cip_sec, 0 num, 1 den, 0 excl from {}_{} {}'.format(t, den, agr, 'where num = 1' if vmin == 'num' else ''))
            c.execute('alter table {} add index (id_cip_sec)'.format(t))
            if ind == 'IASSIR12':
                c.execute("update {} set num = 1".format(t))
                c.execute("update {} a inner join {} b on a.id_cip_sec = b.id_cip_sec set excl = 1 where b.fi > '{}'".format(t, embaras, fa75dies))
        else:
            raise AttributeError('vmax {} not implemented'.format(vmax))


conn.close()

print strftime("%Y-%m-%d %H:%M:%S")
