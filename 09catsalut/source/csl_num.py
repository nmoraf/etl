from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="catsalut"
conn = connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/csl_ind.txt"
pathr="./dades_noesb/csl_relacio.txt"

criteris="nodrizas.eqa_criteris"
criterisped="nodrizas.ped_criteris_var"
pedvacunes="nodrizas.ped_vacunes"
vacunes="nodrizas.eqa_vacunes"
dext="nodrizas.dextraccio"
problemes="nodrizas.eqa_problemes"
nvariables="nodrizas.eqa_variables"
variables="csl_variables"
pedvariables="nodrizas.ped_variables"
pedproblemes="nodrizas.ped_problemes"
tabac="nodrizas.eqa_tabac"
espiros = "import.espiros"
psicosi="nodrizas.csl_psicosi"
agrvar="csl_criteris_variables"

atesatot="atesa_tot"
assignada="nodrizas.assignada_tot_with_jail"


num="numerador"
den="denominador"
csl_i="indicador"
relacio1="csl_relacio1"
relacio="csl_relacio"



print 'inici variables: ' + strftime("%Y-%m-%d %H:%M:%S")

c.execute("drop table if exists %s" % variables)
c.execute("create table %s like %s" % (variables,nvariables))

c.execute("drop table if exists %s" % agrvar)
c.execute("create table %s (agrupador double)" % agrvar)
c.execute("alter table %s add unique(agrupador)" % agrvar)
with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus=="NUM":
         c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            nod= i[0]
            if nod in ("variables","activitats","actuacions","laboratori"):
               c.execute("insert ignore into %s values(%s)" % (agrvar,agr))

c.execute("insert into %s select a.* from %s a inner join %s b on a.agrupador=b.agrupador" % (variables,nvariables,agrvar))            

print 'fi variables: ' + strftime("%Y-%m-%d %H:%M:%S")

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      if grup=="CSLA01":
         taulad1="%s_%s_1" % (num,ind)
         taulad="%s_%s_2" % (num,ind)
         td="%s_%s" % (num,ind)
         c.execute("drop table if exists %s" % taulad1)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,ctl double)" % taulad1)
         c.execute("drop table if exists %s" % taulad)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,ctl double)" % taulad)
         c.execute("drop table if exists %s" % td)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double)" % td)
      
      

with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus=="NUM":
         tnum="%s_%s_1" % (num,ind) 
         tden="%s_%s" % (den,ind) 
         if agr=="lligats":
            ko=1
         elif agr=="tabac":
            if tmin:
               c.execute("select date_add(data_ext,interval - %s month),date_add(data_ext,interval - %s month) from %s" % (tmin,tmax,dext))
               fatemps,ara,=c.fetchone()
               c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf, a.edat,sexe,ates,institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where (a.edat between %s and %s) and tab=%s and (dalta between '%s' and '%s') and dbaixa>=data_ext" % (tnum,ind,f,tden,tabac,dext,emin,emax,vmin,fatemps,ara))
         elif agr=="ESPIROS":
            c.execute("select date_add(data_ext,interval - %s month),date_add(data_ext,interval - %s month) from %s" % (tmin,tmax,dext))
            fatemps,ara,=c.fetchone()
            c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf, a.edat,sexe,ates,institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where esp_dat between '%s' and '%s'" % (tnum,ind,f,tden,espiros,dext,fatemps,ara)) 
         else:
            c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
            pres=c.fetchall()
            for i in pres:
                  nod= i[0]
                  if nod=="psicosi_variables":
                     c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s" % (tnum,ind,f,tden,psicosi,agr))
                  elif nod in ("variables","activitats","actuacions","laboratori"):
                     if vmax:
                        if tmin:
                           c.execute("select date_add(data_ext,interval - %s month),date_add(data_ext,interval - %s month) from %s" % (tmin,tmax,dext))
                           fatemps,ara,=c.fetchone()
                           if uvalor:
                              c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s and (a.edat between %s and %s) and (valor between %s and %s) and (data_var between '%s' and '%s') and usar=%s" % (tnum,ind,f,tden,variables,agr,emin,emax,vmin,vmax,fatemps,ara,uvalor))
                           else: 
                              c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s and (a.edat between %s and %s) and (valor between %s and %s) and (data_var between '%s' and '%s')" % (tnum,ind,f,tden,variables,agr,emin,emax,vmin,vmax,fatemps,ara))
                        else:
                           if uvalor:
                              c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s and (a.edat between %s and %s) and (valor between %s and %s) and usar=%s" % (tnum,ind,f,tden,variables,agr,emin,emax,vmin,vmax,uvalor))
                           else:
                              c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s and (a.edat between %s and %s) and (valor between %s and %s)" % (tnum,ind,f,tden,variables,agr,emin,emax,vmin,vmax))
                     else:
                        c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s and (a.edat between %s and %s)" % (tnum,ind,f,tden,variables,agr,emin,emax))
                  elif nod=="vacunesped":
                     c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates,institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where agrupador=%s and (a.edat between %s and %s) and (dosis between %s and %s)" % (tnum,ind,f,tden,pedvacunes,agr,emin,emax,vmin,vmax))
                  elif nod=="problemes":
                     c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates, institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where ps=%s and (a.edat between %s and %s)" % (tnum,ind,f,tden,problemes,agr,emin,emax))
c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s (indicador varchar(10), ctl int)" % relacio1)        
with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus=="NUM":
         c.execute("insert into %s values('%s','%s') " % (relacio1,ind,f))      
c.execute("drop table if exists %s" % relacio)
c.execute("create table %s (indicador varchar(10), maxim double)" % relacio)   
c.execute("insert into %s select indicador,max(ctl) maxim from %s group by indicador" % (relacio,relacio1))
c.execute("alter table %s add primary key (indicador,maxim)" % relacio)

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      if grup=="CSLA01":
         taula2="%s_%s_2" % (num,ind) 
         taula="%s_%s_1" % (num,ind)
         t="%s_%s" % (num,ind)
         c.execute("insert into %s select id_cip_sec,indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,count(distinct ctl) ctl from %s group by id_cip_sec,indicador,up,uba,upinf,ubainf" % (taula2,taula))
         c.execute("insert into %s select id_cip_sec,a.indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca from %s a inner join %s b on a.indicador=b.indicador where ctl>=maxim" % (t,taula2,relacio))
         c.execute("alter table %s add index(id_cip_sec)" % t)

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      if grup=="CSLA01":
         taulaind="%s_%s" % (csl_i,ind)
         denominador="%s_%s" % (den,ind)
         numerador="%s_%s" % (num,ind)
         c.execute("drop table if exists %s" % taulaind)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,den double, num double, excl double)" % taulaind)      
         c.execute("insert into %s select id_cip_sec,indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,1 den, 0 num, 0 excl from %s" % (taulaind,denominador))
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (taulaind,numerador))
      
with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12] 
      if tipus=="EXCL":
         taulaind="%s_%s" % (csl_i,ind) 
         c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
               nod= i[0]
               if nod=="problemes":
                  c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where ps=%s" % (taulaind,problemes,agr))
       
              
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")