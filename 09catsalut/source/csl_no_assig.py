# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter

db = 'catsalut'
imp = 'import'
nod = 'nodrizas'

def getIndicCSL():
    edats = {}
    sql = "select id_cip_sec,(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) from assignadawithjail, nodrizas.dextraccio"
    for id, edat in getAll(sql, imp):
        if edat < 15:
            edats[id] = 'ATNAS014'
        else:
            edats[id] = 'ATNAS14'
    return edats
        
def getCentres():
    centres = {}
    sql = 'select scs_codi,ics_codi from cat_centres_with_jail'
    for up,br in getAll(sql,nod):
        centres[up] = br
    return centres

def getDades(ed, cent):
    dades = Counter()
    sql = 'select id_cip_sec, up from csl_no_assig'
    for id, up in getAll(sql, nod):
        try:
            indicador = ed[id]
        except KeyError:
            continue
        try:
            br = cent[up]
        except KeyError:
            continue
        dades[up, indicador] += 1
    upl = []
    for (up, indicador), count in dades.items():
        upl.append([up, indicador, count])
    return upl
    
printTime()
tableMy = 'mst_no_assig'
createTable(tableMy, '(scs_codi varchar(5),indicador varchar(10),valor double)', db, rm=True)
ed = getIndicCSL()
cent = getCentres()
printTime()
upload = getDades(ed, cent)    
listToTable(upload, tableMy, db)
finalTable = "mst_csl_main"
sql = "delete a.* from {0} a where indicador in ('ATNAS014', 'ATNAS14')".format(finalTable)
execute(sql, db)
sql = "insert into {0}  select scs_codi, 1, indicador, valor from {1}".format(finalTable, tableMy)
execute(sql, db)
printTime()      

