from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="catsalut"
conn = connect((db,'aux'))
c=conn.cursor()

atesatot="atesa_tot"
#criteri_atesa_tot="ates=1" #francesc setembre14: canvio nomes aixo per tenir tots els indicadors per atesa i per assignada
criteri_atesa_tot="1=1"

assignada="nodrizas.assignada_tot_with_jail"
assig="assignada_adults"
instit="if(edat between 0 and 14,institucionalitzat_ps,b.institucionalitzat)"

nodlligats="nodrizas.csl_lligats"
lligats="csl_lligats"



c.execute("drop table if exists %s" % atesatot)
c.execute("create table %s as select * from %s where 1 = 0" % (atesatot,assignada))
c.execute("insert into %s select * from %s where %s" % (atesatot,assignada,criteri_atesa_tot))
c.execute("alter table %s add index(id_cip_sec,up,edat)" % atesatot)

c.execute("drop table if exists %s" % assig)
c.execute("create table %s as select * from %s where 1 = 0" % (assig,assignada))
c.execute("insert into %s select * from %s " % (assig,assignada))
c.execute("alter table %s add index(id_cip_sec,up,edat)" % assig)

c.execute("drop table if exists %s" % lligats)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double, den double)" % lligats)
c.execute("insert into %s select a.id_cip_sec,up,uba,upinf,ubainf,edat,sexe,ates,%s institucionalitzat,maca,num,den from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" % (lligats,instit, nodlligats,atesatot))

            
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")