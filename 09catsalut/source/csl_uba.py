from sisapUtils import readCSV, createTable, listToTable, getAll


db = 'catsalut'
tb_cataleg = 'exp_ecap_cataleg'
tb_resultat = 'exp_ecap_uba'
tb_llistat = 'exp_ecap_pacient'
tb_khalix = 'exp_khalix_uba_ind'

cataleg = [row for row in readCSV('./dades_noesb/csl_ind.txt')]
tenen_resultat_uba = set([row[1] for row in cataleg if row[10] == '1'])
tenen_llistat_uba = set([row[1] for row in cataleg if row[7] == '1' and row[10] == '1'])
tenen_llistat_rup = set([row[1] for row in cataleg if row[7] == '1' and row[10] == '0'])


id_to_hash = {id: (hash, sector) for (id, sector, hash) in getAll('select id_cip_sec, codi_sector, hash_d from u11', 'import')}

data_uba = {}
data_llistat = set()
for id, indicador, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca, num, den, excl in getAll('select * from mst_indicadors_pacient where id_cip_sec > 0', db):
    if indicador in tenen_resultat_uba and ates and (not institucionalitzat or indicador in ('IAP25A', 'IAP25B')) and not maca and not excl:
        for key in [(up, uba, 'M', indicador), (up, ubainf, 'I', indicador)]:
            if key[1]:
                if key not in data_uba:
                    data_uba[key] = [0, 0]
                data_uba[key][0] += num
                data_uba[key][1] += den
                if indicador in tenen_llistat_uba:
                    if indicador in ('IGFM04AP', 'IGFM05AP'):
                        if num == den:
                            data_llistat.add(key + id_to_hash[id] + (0,))
                    else:
                        if num != den:
                            data_llistat.add(key + id_to_hash[id] + (0,))

createTable(tb_resultat, '(up varchar(5), uba varchar(5), tipus varchar(1), indicador varchar(10), numerador int, denominador int, resultat double)', db, rm=True)
listToTable(((up, uba, tipus, indicador, num, den, float(num) / den) for (up, uba, tipus, indicador), (num, den) in data_uba.items()), tb_resultat, db)

createTable(tb_llistat, '(up varchar(5), uba varchar(5), tipus varchar(1), indicador varchar(10), hash varchar(40), sector varchar(4), exclos int)', db, rm=True)
listToTable(data_llistat, tb_llistat, db)
# Boletus boletum
for indicador in tenen_llistat_rup:
    listToTable(getAll('select * from {}_llistat_rup'.format(indicador), db), tb_llistat, db)

data_cataleg = [(row[1], row[2], row[8], row[7], 'http://sisap-umi.eines.portalics/indicador/codi/' + row[1].format(row[1]), row[11]) for row in cataleg if row[1] in tenen_resultat_uba | tenen_llistat_rup]
createTable(tb_cataleg, '(indicador varchar(10), literal varchar(250), ordre int, llistat int, wiki varchar(250), oficial varchar(15))', db, rm=True)
listToTable(data_cataleg, tb_cataleg, db)

createTable(tb_khalix, '(up varchar(5), uba varchar(5), tipus varchar(1), indicador varchar(10), analisis varchar(10), detalle varchar(10), valor int)', db, rm=True)
ubas_reals = set([row for row in getAll('select * from eqa_ind.mst_ubas union select * from pedia.mst_ubas', 'import')])
listToTable(((up, uba, tipus, indicador, 'NUM', 'NOINSAT', num) for (up, uba, tipus, indicador), (num, den) in data_uba.items() if (up, uba, tipus) in ubas_reals), tb_khalix, db)
listToTable(((up, uba, tipus, indicador, 'DEN', 'NOINSAT', den) for (up, uba, tipus, indicador), (num, den) in data_uba.items() if (up, uba, tipus) in ubas_reals), tb_khalix, db)
