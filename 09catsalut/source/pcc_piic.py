
from sisapUtils import *
import time
from collections import defaultdict,Counter
from datetime import *

khalix=True

nod="nodrizas"
imp="import"
db="catsalut"
cod_var_dict= {
    "14":("esp",0,"num"),
    "15":("esp",1,"txt"),
    "17":("gen",1,"txt"),
    "01":("alt",1,"txt"),
    "02":("ant",1,"txt")
}

codi_estat= {
    "pcc":("IAP23bisa","ER0001"),
    "maca":("IAP23bisB","ER0002")
}

#FUNCIONES

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_piic_pacient(current_date):
    """Para cada paciente guarda la informacion que se tiene del ultimo piic registrado, si hace menos de 12 meses de este registro.
       Se guarda la informacion en un diccionario con estructura de arbol:
       - Tipo de recomendacion: Puede ser especifica, generica, afegir,altra, anticipada
            - Para cada tipo de recomendacion se guarda el ultimo numero de la variable que contiene la inforamcion, de esa forma si son diferentes desplegables/recomendaciones (en el caso de especidicas y afegir recomendacion y gen),
            se guardan todos dentro de un tipo de recomendacion segun el numero (01,02,03,04,..99) y sino se guardan 01 en caso de alt o en 02 en caso de ant.
                - Para cada numero de variable (o en el caso alt, ant solo hay un numero, porque solo es una variable) se guarda un tuple de los siguientes elementos:
                    0) En posicion 0 esta el valor de mi_val_num/mi_val_txt de la variable que corresponda (en el caso de esp y afegir). En el resto que no importa queda como 0.
                    1) En posicion 1 estan los comentarios para el campo que corresponda
        Se devuelve este arbol a nivel de id de paciente.
    """
    sql="select id_cip_sec,mi_cod_var,mi_val_num,mi_val_txt,mi_data_reg from import.piic where mi_ddb = 0;"
    tree = lambda: defaultdict(tree)
    id_piic=tree()

    for id, cod_var, num, txt,data_reg in getAll(sql,imp):
        if monthsBetween(data_reg,current_date) < 12:
            key_field=cod_var[6:8]
            num_field=cod_var[-2:]

            if key_field in cod_var_dict:
                valor_dict={"num":num,"txt":txt}
                key_rec,tuple_index,valor=cod_var_dict[key_field]

                id_piic[id][key_rec].setdefault(num_field,[0,""])
                id_piic[id][key_rec][num_field][tuple_index]=valor_dict[valor]
                
                #if key_field in ("14","15"):
                #    id_piic[id]["exp"].setdefault(num_field,[0,0])
                #    id_piic[id]["exp"][num_field][tuple_index]=1


    return id_piic

def get_len_comentari(key_rec_dict):
    """Suma la longitud de todos los comentarios de un apartado de recomendacion.
       Recibe un diccionario en el formato {num_field: [num,txt]} y suma la longitud del segundo elemento del tuple, que contiene el comentario.
       Devuelve un numero que es la suma de la longitud de todos los comentarios que hay en un apartado de recomendacion.
    """
    return sum([len(tupl[1]) for num_field,tupl in key_rec_dict.iteritems()])

def get_cumplidors(id,estat,cumplidor_table):
    """Obtiene si un paciente que tiene un piic debidamente cumplimentado segun los criterios para pcc y maca. Para un id checkea tres condiciones:
        - cumplidor_1: Que tenga de alguna de las recomendaciones especificas el desplegable mas algun comentario o, 
                       sino tiene ninguna, que en afegir (recomendaciones anyadidas) tenga un problema de salut/valoracion y su correspondiente comentario.
        - cumplidor_2: Que la longitud de los comentarios en especificas, genericas, afegir y altres sea mayor de 25.

        - cumplidor_3: Que la longitud de genericas y anticipada sea mayor de 100.

        Si cumple las dos primeras, se considera que es cumplidor si es pcc y si cumple las 3 que es cumplidor si es maca.
        Se devuelve un diccionario de sets de la siguiente estructura: {"pcc": set ids que cumplen, "maca": sets de ids que cumplen}
    """
   
    cumplidor_1=False
    cumplidor_2=False
    cumplidor_3=False

    ## FIRST CONDITION
    #check especific
    for num_field,tupl in id_piic[id]["esp"].iteritems():
        desplegaple,comentari_esp=tupl
        if desplegaple !=0 and len(comentari_esp) > 0:
            cumplidor_1= True
            break

    ## SECOND CONDITION
    len_all= sum(get_len_comentari(id_piic[id][key_rec]) for key_rec in ["esp","gen","alt"])
    cumplidor_2= len_all >= 25

    ## MACA CONDITION
    len_gen_ant=sum(get_len_comentari(id_piic[id][key_rec]) for key_rec in ["gen","ant"])
    cumplidor_3= len_gen_ant >= 100


    ## FINAL
    if estat=="pcc":
        cumplidor_table.append((int(id),int(cumplidor_1),int(cumplidor_2),int(bool(id_piic[id]["alt"])), int(cumplidor_1 and cumplidor_2),id_piic[id]["exp"][0],id_piic[id]["exp"][1]))
        return cumplidor_1 and cumplidor_2
    elif estat=="maca":
        return cumplidor_3 and cumplidor_1 and cumplidor_2

def get_estat(es_cod):
    """Obtiene los ids de los pacientes con cierto 'estado', es decir, los ids de maca o pcc.
       Devuelve un set de ids.
    """
    sql="select id_cip_sec from {}.estats where es_cod= '{}'".format(imp,es_cod)
    return {id for (id,) in getAll(sql,imp)}

def get_rows_up(estat_id,estat,current_date,codi,cumplidor_table):
    """ Construye las rows de la tabla a nivel de up. Se queda con todos los vivos y con los muertos en el ultimo any.
        Devuelve una lista de tuples(lista de filas)
    """
    sql="select id_cip_sec,usua_uab_up,usua_situacio,usua_data_situacio from {}.assignada".format(imp)
    indicador=defaultdict(Counter)
    
    for id,up,usua_situacio,situacio_data in getAll(sql,imp):
        if up == "" or (usua_situacio== "D" and monthsBetween(situacio_data,current_date) > 11):
            continue
        if id in estat_id:
            indicador[up]["DEN"]+=1
            if get_cumplidors(id,estat,cumplidor_table):
                indicador[up]["NUM"]+=1
    
    rows= [(up, codi, indicador[up]["NUM"], indicador[up]["DEN"]) for up in indicador]
    return rows,cumplidor_table
	
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)        

if __name__=="__main__":
    
    table_name="exp_khalix_pcc_maca_uba"
    uba_columns="(up varchar(8),indicador varchar(15),numerador int, denominador int)"
    createTable(table_name,uba_columns,db,rm=True)
    printTime("Tabla creada")

    current_date=get_date_dextraccio()
    printTime("Current date")

    id_piic=get_piic_pacient(current_date)
    printTime("Pacients piic info")

    cumplidor_table=[]
    for estat,(codi,es_cod) in codi_estat.iteritems():
        estat_id=get_estat(es_cod)
        printTime("Pacient {}".format(estat))
        rows,cumplidor_table=get_rows_up(estat_id,estat,current_date,codi,cumplidor_table)
        printTime("Rows")
        listToTable(rows,table_name,db)
        printTime("Insert into the table {}".format(codi))

    taula_name="id_criteris_pcc"
    columns="(id_cip_sec int, criteri1 int,criteri2 int, alt int, num int)"
    export_table(taula_name,
				columns,
				"altres",
				cumplidor_table)
        






