from sisapUtils import exportPDP


exportPDP('select * from catsalut.exp_ecap_uba', 'cslIndicadors', dat=True)
exportPDP('select * from catsalut.exp_ecap_pacient', 'cslLlistats', truncate=True, pkOut=True, pkIn=True)
exportPDP('select data_ext from nodrizas.dextraccio', 'cslLlistatsData', truncate=True)
exportPDP('select * from catsalut.exp_ecap_cataleg', 'cslCataleg', datAny=True)
