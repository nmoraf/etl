from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


nod="nodrizas"
db = "catsalut"
imp="import"
codi_indicador="CMBDAP03"
file_name="CMBDAP03_catsalut"


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from nodrizas.dextraccio;"
    return getOne(sql,nod)[0]


def get_rows(taula):
    sql="select cp_up_ecap, cp_ecap_dvis, cp_d1, cp_din1 from {0};".format(taula)

    indicador=defaultdict(Counter)
    for up, cp_ecap_dvis, cp_d1, cp_din1 in getAll(sql,imp):
        if monthsBetween(cp_ecap_dvis,current_date) < 6:
            indicador[up]["DEN"]+=1
            if cp_d1 or cp_din1:
                indicador[up]["NUM"]+=1
    
    return indicador

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)


#Current date como variable global	
current_date=get_date_dextraccio()	

if __name__ == '__main__':

    printTime("Inicio")
    
    print(current_date)
	
    tables = getSubTables('cmbdap')
    resul_by_taula = multiprocess(get_rows, tables)
    printTime("Indicadores por taula")

    indicador=defaultdict(Counter)
    for indicador_taula in resul_by_taula:
        for up in indicador_taula:
            for num_den in indicador_taula[up]:
                indicador[up][num_den]+=indicador_taula[up][num_den]
    
    printTime("Indicador final")
    
    rows=[(up, codi_indicador,indicador[up]["NUM"], indicador[up]["DEN"]) for up in indicador]
    printTime("Rows OK")

    taula_name="exp_khalix_{}_uba".format(codi_indicador)
    columns="(up varchar(8), indicador varchar(12),numerador int, denominador int)"
    export_table(taula_name,
				columns,
				db,
				rows)

    