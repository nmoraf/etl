# coding: iso-8859-1
from sisapUtils import *
import sys
from time import strftime
from collections import defaultdict,Counter


nod = 'nodrizas'
db = 'catsalut'

OutFile = tempFolder + 'iap09.txt'
TaulaMy = "csl_iap09"
indicador = 'IAP09'

def TASLimit(edat,dm2):

    if dm2 == 1:
        return 150
    elif 15 <= edat <=59:
        return 150
    elif edat > 59:
        return 160
    else:
        return 999

printTime('Pob')
assig = {}
sql = 'select id_cip_sec,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca from assignada_tot_with_jail where edat between 15 and 80'
for id,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca in getAll(sql,nod):
    assig[int(id)] = {'up':up,'uba':uba,'ubainf':ubainf,'edat':edat,'sexe':sexe,'ates':ates,'insti':institucionalitzat,'maca':maca}

printTime('Inici procés')
teDM2,iap09 = {},{}
sql="select id_cip_sec,ps from eqa_problemes where ps in ('55','18')"
for id,ps in getAll(sql,nod):
    id = int(id)
    try:
        edat = assig[id]['edat']
    except KeyError:
        continue
    if ps == 55:
        iap09[id] = {'den':1,'TAS':0,'TAD':0,'edat':edat}
    elif ps == 18:
        teDM2[id] = True
        
sql = "select id_cip_sec,agrupador, valor from eqa_variables,dextraccio where usar=1 and agrupador in ('16','17') and data_var between date_add(date_add(data_ext,interval - 1 year),interval +1 day) and data_ext"
for id,agr,val in getAll(sql,nod):
    id = int(id)
    try:
        edat = iap09[id]['edat']
    except KeyError:
        continue        
    try:
        if teDM2[id]:
            dm2 = 1
    except KeyError:
        dm2 = 0
    if agr == 16:
        limitA = TASLimit(edat,dm2)
        if val <= limitA:
            iap09[id]['TAS'] = 1
    elif agr == 17:
        if val <= 95:
            iap09[id]['TAD'] = 1
            
with openCSV(OutFile) as c:
    for (id),d in iap09.items():
        tas = d['TAS']
        tad = d['TAD']
        den = d['den']
        if tas == 1 and tad == 1:
            num = 1
        else:
            num = 0
        c.writerow([id,indicador,assig[id]['up'],assig[id]['uba'],assig[id]['ubainf'],d['edat'],assig[id]['sexe'],assig[id]['ates'],assig[id]['insti'],assig[id]['maca'],num,den])
 
execute('drop table if exists {}'.format(TaulaMy),db)
execute("create table {} (id_cip_sec double null,indicador varchar(10) not null default'', up varchar(5) not null default'',uba varchar(5) not null default '',ubainf varchar(5) not null default '',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double)".format(TaulaMy),db)
loadData(OutFile,TaulaMy,db) 
            
printTime('Fi procés')   