from sisapUtils import *
import time
from collections import defaultdict,Counter
from datetime import *

file_name="VISMACAPCC_CATSALUT"

codis=["pcc_maca","CMBDAP03"]    
taulas= ["exp_khalix_{}_uba".format(codi) for codi in codis]       

query_template_string="select indicador,concat('A','periodo'),ics_codi,{strg},'NOCAT','NOIMP','DIM6SET','N',{var} from {db}.{taula} a inner join nodrizas.cat_centres b on a.up=scs_codi where a.{var} != 0"
query_string=" union ".join(query_template_string.format(strg="'NUM'",var="numerador",taula=taula_name_kh,db="catsalut")+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=taula_name_kh,db="catsalut") for taula_name_kh in taulas)
print(query_string)

exportKhalix(query_string,file_name)
