from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="catsalut"
conn = connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/csl_ind.txt"
pathr="./dades_noesb/csl_relacio.txt"

criteris="nodrizas.eqa_criteris"
criterisped="nodrizas.ped_criteris_var"
pedvacunes="nodrizas.ped_vacunes"
vacunes="nodrizas.eqa_vacunes"
dext="nodrizas.dextraccio"
problemes="nodrizas.eqa_problemes"
variables="nodrizas.eqa_variables"
pedvariables="nodrizas.ped_variables"
pedproblemes="nodrizas.ped_problemes"
tabac="nodrizas.eqa_tabac"
lligats="csl_lligats"

atesatot="atesa_tot"
assignada="nodrizas.assignada_tot_with_jail"
assig="assignada_adults"


den="denominador"
relacio1="csl_relacio1"
relacio="csl_relacio"

instit="if(a.edat between 0 and 14,institucionalitzat_ps,a.institucionalitzat)"


with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      if grup=="CSLA01":
         taulad1="%s_%s_1" % (den,ind)
         taulad="%s_%s_2" % (den,ind)
         td="%s_%s" % (den,ind)
         c.execute("drop table if exists %s" % taulad1)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,ctl double)" % taulad1)
         c.execute("drop table if exists %s" % taulad)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,ctl double)" % taulad)
         c.execute("drop table if exists %s" % td)
         c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double, excl double)" % td)
      


with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus=="DEN":
         taula="%s_%s_1" % (den,ind)
         if tpob=="AT":
            if agr=="pob":
               c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,edat,sexe,ates,%s institucionalitzat,maca,%s from %s a where (a.edat between %s and %s)" % (taula,ind,instit,f,atesatot,emin,emax))
            elif agr=="tabac":
               if tmin:
                  c.execute("select date_add(data_ext,interval - %s month),date_add(data_ext,interval - %s month) from %s" % (tmin,tmax,dext))
                  fatemps,ara,=c.fetchone()
                  c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,edat,sexe,ates,%s institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where tab=%s and (a.edat between %s and %s) and (dalta between '%s' and '%s') and dbaixa>=data_ext" % (taula,ind,instit,f,atesatot,tabac,dext,vmin,emin,emax,fatemps,ara))   
               else:
                  c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,edat,sexe,ates,%s institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where tab=%s and (a.edat between %s and %s) and dbaixa>=data_ext" % (taula,ind,instit,f,atesatot,tabac,dext,vmin,emin,emax))   
            elif agr=="lligats":
               ko=1
            else:
               c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
               pres=c.fetchall()
               for i in pres:
                  nod= i[0]
                  if nod=="problemes":
                     c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates,%s institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where ps=%s and (a.edat between %s and %s)" % (taula,ind,instit,f,atesatot,problemes,agr,emin,emax))
         elif tpob=="ASS":
            if agr=="pob":
               c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,edat,sexe,ates,%s institucionalitzat,maca,%s from %s a where (a.edat between %s and %s)" % (taula,ind,instit,f,assignada,emin,emax))
            elif agr=="tabac":
               if tmin:
                  c.execute("select date_add(data_ext,interval - %s month),date_add(data_ext,interval - %s month) from %s" % (tmin,tmax,dext))
                  fatemps,ara,=c.fetchone()
                  c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,edat,sexe,ates,%s institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where tab=%s and (a.edat between %s and %s) and (dalta between '%s' and '%s') and dbaixa>=data_ext" % (taula,ind,instit,f,assignada,tabac,dext,vmin,emin,emax,fatemps,ara))   
               else:
                  c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,edat,sexe,ates,%s institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s where tab=%s and (a.edat between %s and %s) and dbaixa>=data_ext" % (taula,ind,instit,f,assignada,tabac,dext,vmin,emin,emax))   
            elif agr=="lligats":
               ko=1
            else:
               c.execute("select distinct taula from %s where agrupador='%s' union select distinct taula from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
               pres=c.fetchall()
               for i in pres:
                  nod= i[0]
                  if nod=="problemes":
                     c.execute("insert into %s select a.id_cip_sec,'%s',a.up,a.uba,a.upinf,a.ubainf,a.edat,sexe,ates,%s institucionalitzat,maca,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where ps=%s and (a.edat between %s and %s)" % (taula,ind,instit,f,assignada,problemes,agr,emin,emax))
c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s (indicador varchar(10), ctl int)" % relacio1)        
with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus=="DEN":
         c.execute("insert into %s values('%s','%s') " % (relacio1,ind,f))      
c.execute("drop table if exists %s" % relacio)
c.execute("create table %s (indicador varchar(10), maxim double)" % relacio)   
c.execute("insert into %s select indicador,max(ctl) maxim from %s group by indicador" % (relacio,relacio1))
c.execute("alter table %s add primary key (indicador,maxim)" % relacio)

with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,uba,oficial = cat
      if grup=="CSLA01":
         taula2="%s_%s_2" % (den,ind) 
         taula="%s_%s_1" % (den,ind)
         t="%s_%s" % (den,ind)
         c.execute("insert into %s select id_cip_sec,indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,count(distinct ctl) ctl from %s group by id_cip_sec,indicador,up,uba,upinf,ubainf" % (taula2,taula))
         c.execute("insert into %s select id_cip_sec,a.indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,0 num,0 excl from %s a inner join %s b on a.indicador=b.indicador where ctl>=maxim" % (t,taula2,relacio))
         c.execute("alter table %s add index(id_cip_sec,indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,num,excl)" % t)
  
              
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")