# coding: iso-8859-1
from sisapUtils import *
import sys, csv, os
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
db = 'catsalut'
imp = 'import'

origen = 'cmbdap'
dext = 'nodrizas.dextraccio'
OutFile = tempFolder + 'csl_visites1.txt'
indicador = 'IAP08'


desti = 'csl_visites'

espe = ['10999','30999','10888','10777','10117','10116','05999','10106','30888']

printTime('Pob')
assig = {}
sql = 'select id_cip_sec,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca from assignada_tot_with_jail'
for id,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca in getAll(sql,nod):
    assig[int(id)] = {'up':up,'uba':uba,'ubainf':ubainf,'edat':edat,'sexe':sexe,'ates':ates,'insti':institucionalitzat,'maca':maca}
    
printTime('Inici procés')

save = {}
    
sql = "select id_cip_sec,if(cp_t_act in ('43','44'),1,0) from {0},{1} where cp_ecap_dvis between date_add(date_add(data_ext,interval - 1 year),interval +1 day) and data_ext and cp_ecap_categoria in {2}{3}".format(origen,dext,str(tuple(espe)),' and id_cip_sec=4' if debug else '')
for id,value in getAll(sql,imp):
    id = int(id)
    try:
        edat = assig[id]['edat']
    except KeyError:
        continue
    if id in save:
        save[id]['num'] += value
        save[id]['den'] += 1
    else:
        try:
            save[id] = {'edat':edat,'num':value,'den':1}
        except KeyError:
            continue

with openCSV(OutFile) as c:
    for (id),count in save.items():
        num = count['num']
        den = count['den']
        edat = count['edat']
        c.writerow([id,indicador,assig[id]['up'],assig[id]['uba'],assig[id]['ubainf'],edat,assig[id]['sexe'],assig[id]['ates'],assig[id]['insti'],assig[id]['maca'],num,den])
    
execute('drop table if exists {}'.format(desti),db)
execute("create table {} (id_cip_sec double null,indicador varchar(10) not null default'', up varchar(5) not null default'',uba varchar(5) not null default '',ubainf varchar(5) not null default '',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double)".format(desti),db)
loadData(OutFile,desti,db)

printTime('Fi procés')
