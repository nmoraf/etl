#coding: utf8
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


nod="nodrizas"
db = "catsalut"
imp="import"
agr_vac=99
codi_indicador="IAP18"



tb_pac = 'mst_indicadors_pacient'
tb_klx = 'exp_khalix_up_ind'

vacunes_grip=('GRIP-N',
'GRIP',
'GRIP-A',
'G(A)-4',
'G(A)-2',
'G(A)-3',
'G(A)-1',
'P-G-AR',
'P-G-NE',
'P-G-AD',
'P-G-60')

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from nodrizas.dextraccio;"
    return getOne(sql,nod)[0]


def get_vacunats_ult_any(params):
    """ Selecciona los vacunados de la gripe (que han sido vacunados con alguno de los codigos en vacunes_grip ) en un periodo de tiempo determinado por
        ini y fi.
        Devuelve un set de ids.
    """
    print params,len(params)
    table,ini,fi=params
    sql="select id_cip_sec, va_u_data_vac from  {}, nodrizas.dextraccio where va_u_cod in {} \
        and va_u_data_alta <= data_ext and (va_u_data_baixa=0 or va_u_data_baixa > data_ext) and va_u_dosi > 0 ".format(table,vacunes_grip)
    return {id for id,data in getAll(sql,imp) if ini <= data <= fi }

def get_rows(vacunats):
    """ Construye las rows de la tabla final a nivel de paciente y el indicador agrupando por up, edad y sexo.
        Devuelve una dos listas de tuples:
            -> Una con datos a nivel de paciente
            -> Una con datos a nivel de up,edat,sexe
    """
    sql="select id_cip_sec,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca from {}.assignada_tot where edat >= 65 ".format(nod)
    indicador=defaultdict(lambda: defaultdict(lambda: defaultdict(Counter)))
    pacient_rows=[]
    for id,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca in getAll(sql,nod):
        den,num=1,0
        indicador[up][edat][sexe]["DEN"]+=1
        if id in vacunats:
            indicador[up][edat][sexe]["NUM"]+=1
            num=1
        pacient_rows.append((id,codi_indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,0))
    
    
    rows= [ (up, edat, sexe, 'NOINSAT', codi_indicador, num_den, n) for up in indicador 
                                                                        for edat in indicador[up]
                                                                            for sexe in indicador[up][edat]
                                                                                for num_den, n in indicador[up][edat][sexe].iteritems()]
   
    #Para obtener el excel para elisabet
    """
    rows= [ (up, edat, sexe, 'NOINSAT', codi_indicador, indicador[up][edat][sexe]["NUM"], indicador[up][edat][sexe]["DEN"]) 
                                                                    for up in indicador 
                                                                        for edat in indicador[up]
                                                                            for sexe in indicador[up][edat] ]
    """
    return rows,pacient_rows

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    grip, dext = getOne("select grip, data_ext from dextraccio", "nodrizas")
    if grip or dext.month == 1:
        listToTable(rows, table, db)


#Current date como variable global	


if __name__ == '__main__':

    printTime("Inicio")

    current_date=get_date_dextraccio()	
    printTime(current_date)

    #El any de la campanya vacunal se determina por el mes de la fecha de extraccion. Si el mes es anterior a agosto, se consideta que en any de campaña es el anterior
    # y a partir de septiembre se coge el any actual
    any_camp= current_date.year if current_date.month > 8 else current_date.year-1

    #La fecha de inicio de campanya es en septiembre de any de campany y la de fin, la de febrero del any siguiente
    ini,fi= datetime.date(year=any_camp,month=9, day=1), datetime.date(year= any_camp+1,month=2, day=28)
	
    #Con un multiproceso se consiguen todos los vacunados
    vacunats_total= set.union(*[vacunats for vacunats in multiprocess(get_vacunats_ult_any, [ (table,ini,fi) for table in getSubTables('vacunes')])])
    printTime('vacunats', len(vacunats_total))

    #Se consiguen las filas
    rows,pacient_rows=get_rows(vacunats_total)
    printTime("Rows OK")


    #Para obtener el excel para elisabet
    """
    columns_uba="(up varchar(15), edat varchar(15), sexe varchar(15), comb varchar(15), codi_indicador varchar(15), num int, den int)"
    columns_pac='(id_cip_sec int ,up varchar(15),uba varchar(15),ubainf varchar(15),edat varchar(15),sexe varchar(15), ates int ,institucionalitzat int ,maca int, num int ,den int ,exclos int)'
  
    
    for rows_list,taula_name,columns in zip ([rows,pacient_rows],
                                        ["exp_khalix_{}_uba","exp_khalix_{}_pac"],
                                        [columns_uba, columns_pac]):
        taula=taula_name.format(codi_indicador)
        export_table(taula,columns,db,rows_list)
        printTime(taula)
    """


    taula_name="indicador_{}".format(codi_indicador)
    columns="(id_cip_sec int, indicador varchar(10),up varchar(8), uba varchar(8),ubainf varchar(8),edat int, sexe varchar(5),ates int, institucionalitzat int, maca int,num int, den int, excl int)"
    export_table(taula_name,
				columns,
				db,
				pacient_rows)

    printTime(taula_name)

    