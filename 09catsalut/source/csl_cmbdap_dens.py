# coding: iso-8859-1
import sys, os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from csl_utils import *

printTime('Inici Pob')

ass = {}
sql = 'select id_cip_sec, up, edat, ates, sexe from assignada_tot_with_jail'
for id_cip_sec, up, edat, ates, sexe in getAll(sql, nod):
    ass[int(id_cip_sec)] = {'up': up, 'edat': edat, 'ates': ates, 'sexe': sexe}

printTime('Fi Pob')
printTime('Inici Càlcul')

for indicador in indicadors:
    eqa = getEQA(indicador)
    tipus = getTipus(indicador)
    indic = getIndicador(indicador)
    edatMin = getEdatMin(indicador)
    edatMax = getEdatMax(indicador)
    ates = getAtes(indicador)
    sexe = getSexe(indicador)
    if eqa == 1:
        agrs = []
        sql = "select distinct agrupador from {0} where left(ind_codi,7) = '{1}' and taula='problemes' and tipus='{2}'".format(relacio, indic, tipus)
        for agrupador, in getAll(sql, nod):
            agrs.append(int(agrupador))
        agrs = ", ".join(map(str, agrs))
    else:
        agrs = indic
    crits = []
    sql = "select distinct criteri_codi from {0} where agrupador in ({1})".format(criteris, agrs)
    for crit, in getAll(sql, nod):
        crits.append(crit)
    in_crit = tuple(crits)
    if tipus == 'den' and ates == 1:
        sql = "select id_cip_sec from {0}, {1} where cp_ecap_dvis between date_add(date_add(data_ext,interval - 12 month),interval + 1 day) and data_ext and (cp_d1 in {2} or cp_d2 in {2} or cp_d3 in {2} or cp_d4 in {2} or cp_d5 in {2} or cp_d6 in {2}) {3}".format(cmbd, dext, in_crit, ' limit 2' if debug else '')
    else:
        sql = "select id_cip_sec from {0}, {1} where cp_ecap_dvis<=data_ext and (cp_d1 in {2} or cp_d2 in {2} or cp_d3 in {2} or cp_d4 in {2} or cp_d5 in {2} or cp_d6 in {2}) {3}".format(cmbd, dext, in_crit, ' limit 2' if debug else '')
    for id, in getAll(sql, imp):
        id = int(id)
        if tipus == 'den':
            try:
                edat = ass[id]['edat']
            except KeyError:
                continue
            if edatMin <= edat <= edatMax:
                up = ass[id]['up']
                resultats[(indicador, id, up)] += 1
        elif tipus == 'ci':
            contraI[(id)] = True
    if tipus == 'ci':
        for id, dades in ass.items():
            edat = dades['edat']
            up = dades['up']
            at = dades['ates']
            sex = dades['sexe']
            try:
                if contraI[(id)]:
                    continue
            except KeyError:
                if edatMin <= edat <= edatMax:
                    if ates == 1:
                        if at == 1:
                            if sexe == 0:
                                resultats[(indicador, id, up)] += 1
                            else:
                                if sexe == sex:
                                    resultats[(indicador, id, up)] += 1
                    else:
                        if sexe == 0:
                            resultats[(indicador, id, up)] += 1
                        else:
                            if sexe == sex:
                                resultats[(indicador, id, up)] += 1

execute("drop table if exists {}".format(TableMy), db)
execute("create table {} (indicador varchar(10) not null default'', id_cip_sec int, up varchar(5) not null default'', recompte double)".format(TableMy), db)

with openCSV(OutFile) as c:
    for (indicador, id, up), dict in resultats.items():
        c.writerow([indicador, id, up, dict])

loadData(OutFile, TableMy, db)

printTime('Fi Càlcul')
printTime('Inici khalix')

error = []

sql = "select indicador,concat('A','periodo'),ics_codi,'den2','NOCAT','NOIMP','DIM6SET','N',count(*) from {0}.{1} inner join {2} on up=scs_codi group by indicador,ics_codi".format(db, TableMy, centres)
file = "CSL_CMBDAP_DEN"
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Fi khalix')
