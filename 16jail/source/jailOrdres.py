# coding: iso-8859-1
import sys
import os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *

indicador = 'IEP0702'
 
assig= {}
tab = 'jail_assignada'
sql= 'select id_cip_sec,up,uba,upinf,ubainf,edat,sexe,data_naix, last_visit from {}'.format(tab)
for id,up,uba,upinf,ubainf,edat,sexe,naix, dingres in getAll(sql,nod):
    assig[id]= {'up':up,'uba':uba,'upinf':upinf,'ubainf':ubainf,'sexe':sexe,'edat':edat, 'dingres': dingres}

denIEP07, iep0702 = {}, {}
for id, seq, inici, fi in getAll(sqlOrdres.format(Ordres), imp):
    if fi == None:
        denIEP07[seq] = {'id':id, 'den': 1, 'num': 0}
    else:
        try:
            dataingres = assig[id]['dingres']
            if fi>dataingres:
                denIEP07[seq] = {'id':id, 'den': 1, 'num': 0}
        except:
            continue

for seq, in getAll(sqlSeguiment.format(Seguiment),imp):
    try:
        denIEP07[seq]['num'] = 1
    except KeyError:
        continue

for seq, d in denIEP07.items():
    id = d['id']
    if id in iep0702:
        iep0702[id]['den'] += d['den']
        iep0702[id]['num'] += d['num']
    else:
        try:
            iep0702[id] = {'up': assig[id]['up'], 'uba': assig[id]['uba'], 'upinf': assig[id]['upinf'], 'ubainf': assig[id]['ubainf'], 'sexe': assig[id]['sexe'], 'edat': assig[id]['edat'], 'den': d['den'], 'num': d['num'], 'excl': 0}
        except KeyError:
            continue
            
with openCSV(ordresFile) as w:
    for id,dades in iep0702.items():
        w.writerow([id, indicador, dades['up'], dades['uba'], dades['upinf'], dades['ubainf'], dades['edat'] ,dades['sexe'] ,dades['num'], dades['den'], dades['excl']]) 

sql = "drop table if exists {}".format(ordresTable)
execute(sql,db)
sql = "create table {0} ({1})".format(ordresTable,iepCreate)
execute(sql,db)

loadData(ordresFile,ordresTable,db)         
