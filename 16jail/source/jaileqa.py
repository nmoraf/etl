# coding: iso-8859-1
import sys, os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *

printTime('Inici')

indicadors = getIndicadors()

assig= getPoblacio()

sql = "drop table if exists {}".format(iepTable)
execute(sql,db)
sql = "create table {0} ({1})".format(iepTable,iepCreate)
execute(sql,db)

for ind in indicadors:
    indicador = ind[0]
    grup = ind[1]
    iepIndicador = {}
    for id, coses in assig.items():
        up,uba,upinf,ubainf,edat,sexe = coses['up'],coses['uba'],coses['upinf'],coses['ubainf'],coses['edat'],coses['sexe']
        iepIndicador[id] = {'up':up,'uba':uba,'upinf':upinf,'ubainf':ubainf,'sexe':sexe,'edat':edat,'den':0,'num':0,'excl':0}
    for comp in components:
        component = comp[0]
        nmax = 0
        calcul = {}
        sql = "select numero_criteri,agrupador,data_min,data_max,valor_min,valor_max,var_ult,var_num,sense from {0} where baixa=0 and indicador='{1}' and tipus='{2}' order by numero_criteri".format(tbs['R']['taula'],indicador,component)
        for n,agr,dmin,dmax,vmin,vmax,vult,vnum,sense in getAll(sql,db):
            if n > nmax:
                nmax = n
            sql = getQuery(grup,agr,dmin,dmax,vmin,vmax,vult,vnum)
            for id, in getAll(sql,db):
                if id in calcul:
                    if calcul[id]['nult'] == n:
                        continue
                    else:
                        calcul[id]['nult'] = n
                        calcul[id]['n'] += 1
                else:
                    calcul[id] = {'n': 1, 'nult':n}
        for id,dades in calcul.items():
            npac = dades['n']
            if npac >= nmax:              
                try:
                    iepIndicador[id][component] = 1
                except KeyError:
                    continue 
        calcul.clear()
    with openCSV(iepFile) as w:
        for id,dadesfinal in iepIndicador.items():
            denominador = dadesfinal['den']
            numerador = dadesfinal['num']
            exclusions = dadesfinal['excl']
            up,uba,upinf,ubainf,edat,sexe = dadesfinal['up'],dadesfinal['uba'],dadesfinal['upinf'],dadesfinal['ubainf'],dadesfinal['edat'],dadesfinal['sexe']
            if denominador == 1:
                w.writerow([id,indicador,up,uba,upinf,ubainf,edat,sexe,numerador,denominador,exclusions]) 
    loadData(iepFile,iepTable,db)  

sql = 'insert into {0} select * from {1}'.format(iepTable, ordresTable)
execute(sql, db)
sql = 'insert into {0} select * from {1}'.format(iepTable, tableDM2)
execute(sql, db)
sql = 'insert into {0} select * from {1}'.format(iepTable, tablepresc)
execute(sql, db)

printTime('export ecap')

execute("drop table if exists {0},{1},{2},{3}".format(IEPecap_ind,IEPecap_pac,IEPcataleg,IEPcatalegPare),db)
execute("create table {} (up varchar(5) not null default '',uba varchar(20) not null default '',indicador varchar(8) not null default '',numerador int,denominador int,resultat double)".format(IEPecap_ind),db)

execute("insert into {0} select up,uba,indicador,sum(num),sum(den),round(sum(num)/sum(den),4) from {1} where excl=0 group by up,uba,indicador".format(IEPecap_ind,iepTable),db)
# execute("insert into {0} select up,'PRS',indicador,sum(num),sum(den),round(sum(num)/sum(den),4) from {1} where excl=0 group by up,indicador".format(IEPecap_ind,iepTable),db)
            
execute("create table {0} (id_cip_sec double,up varchar(5) not null default '',uba varchar(20) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')".format(IEPecap_pac),db)
execute("insert into {0} select a.id_cip_sec,up,uba,upinf,ubainf,indicador as grup_codi,0 exclos,hash_d,codi_sector sector from {1} a inner join {2} b on a.id_cip_sec=b.id_cip_sec where num=0 and uba<>''".format(IEPecap_pac,iepTable,u11),db)

execute("create table {0} (indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),llistat int,invers int,mmin double,mint double,mmax double,toShow int,wiki varchar(250),tipus varchar(2), ncol int)".format(IEPcataleg),db)
execute("insert into {0} select indicador,descripcio,ordre,grup,llistat,0,'','','',1,concat('http://sisap-umi.eines.portalics/indicador/codi/IEP',right(indicador,4)),tipus, ncol from {1}".format(IEPcataleg,tbs['I']['taula']),db)

execute("create table {0} (pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int,subtotal int)".format(IEPcatalegPare),db)
execute("insert into {0} select distinct grup,grup_desc,grup_ordre,subtotal from {1}".format(IEPcatalegPare,tbs['I']['taula'],),db)

printTime('Fi')

