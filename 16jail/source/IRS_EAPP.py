# coding: latin1

"""
Indicadors RS EAPP.
"""

import sisapUtils as u
import collections as c
import datetime as d
from dateutil.relativedelta import relativedelta

TABLE = "mst_indicadors_pacient_irs_eapp"
DATABASE = "jail"
FILE = "JAILRISCSUI"

LITERALS = (("RS001", "Criteris d'inclusió de risc de suicidi"),
            ("RS002", "Derivació a Salut Mental en risc elevat"),
            ("RS003", "Seguiment de risc elevat"),
            ("RS004", "Derivació a Salut Mental en risc moderat"),
            ("RS005", "Seguiment de risc moderat"))

#Codi per extreure la taula jail_atesa actualitzada fins al dia d'avui
#Modificació del codi per crear jail_atesa amb dies=0
def jail_atesa_avui():
    from sisapUtils import getAll, createTable, listToTable, getOne

    table = 'jail_atesa_avui'
    db = 'nodrizas'
    dies = 0
    dext, dext_s = getOne("select data_ext, date_format(data_ext, '%Y%m%d') from dextraccio", db)

    moviments = []
    actual = [None, None, None, None, None]
    sql = "select id_cip_sec, huab_data_ass, if(huab_data_final = 0, null, huab_data_final), huab_up_codi, huab_uab_codi from moviments where huab_data_ass <= {} order by 1 asc, 2 asc, 3 desc".format(dext_s)
    for id, ini, fi, up, uba in getAll(sql, 'import_jail'):
        fi = dext if not fi or fi > dext else fi
        if id == actual[0] and up == actual[3]:
            actual[2] = fi
        else:
            if actual[2] == dext and (dext - actual[1]).days > dies:
                moviments.append([actual[0], actual[3], actual[1], actual[4]])
            actual = [id, ini, fi, up, uba]

    createTable(table, '(id_cip_sec int, up varchar(5), ingres date, uba varchar(5), primary key (id_cip_sec))', db, rm=True)
    listToTable(moviments, table, db)
    
def getHashToId():
    hashId={}
    for id,hash_d in u.getAll("select id_cip_sec, hash_d from import_jail.u11", "import_jail"):
        hashId[id]=hash_d
    return hashId


class IRS_EAPP(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_denom_rs001()
        self.get_xml()
        self.get_rs001()
        self.get_data_derivacio()
        self.get_rs002()
        self.get_visites_psi()
        self.get_rs003()
        self.get_rs004()
        self.set_master()
        self.export_khalix()
        #self.delete_ecap()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()

    def get_denom_rs001(self):
        """ Selecció de pacients que ingressen al centre els darrers 6 mesos 
        """
        self.poblacio = {}
        
        #nodrizas.jail_atesa data entrada presó a 30 dies de data extracció
        #nodrizas.jail_atesa_avui data entrada presó fins a data extracció
        
        sql="select data_ext from {}.dextraccio;".format("nodrizas")
        data_ext= u.getOne(sql,"nodrizas")[0]
        
        #crea la taula jail_atesa actualitzada a dia d'avui
        jail_atesa_avui()
        
        sql = "select id_cip_sec, ingres, up, uba, data_ext \
               from jail_atesa_avui, dextraccio \
               where ingres between adddate(data_ext, interval -6 month) \
                                        and data_ext \
               order by 1,2 desc"
        
        #A cada individu li assignem només la seva darrera data d'ingrés        
        actual = 0
        for id, ini, up, uba, data_ext in u.getAll(sql, "nodrizas"):
            if id !=actual:
                self.poblacio[id] = (ini, up, uba)
                actual = id
                
    def get_xml(self):
        """ Dels individus de self.poblacio seleccionem només els que tenen 
            RiscSeleccionat a la variable XML0000083 de xml_detall
        """
        self.xml = c.defaultdict(dict)
        sql = "select id_cip_sec, xml_data_alta, camp_codi, camp_valor \
               from xml_detall where xml_tipus_orig = 'XML0000083' and \
               camp_codi ='RiscSeleccionat' order by 1,2 asc"
        
        #A cada individu li assignem només el primer valor després 
        #de la data d'ingrés
        #S'obvien els que tenen data de RiscSeleccionat anterior a data ingrés
        actual = 0
        for id, dat, cod, val in u.getAll(sql, "import_jail"):
            if id in self.poblacio:
                if dat >= self.poblacio[id][0] and id != actual:
                    actual = id
                    self.xml[id] = (dat, cod, val)
                
    def get_rs001(self):
        """ Calcula indicador RS001
            Den: Individus amb RiscSeleccionat
            Num: Individus amb registre de prioritat els 7 primers dies d'ingrés
        """
        self.master = []    
        RiscSeleccionat = {'1','3','2'}
        for key in self.poblacio:
            den = 1
            exc_den = 0
            data_ingres = self.poblacio[key][0]
            data_oc = 0
            if key in self.xml.keys() and self.xml[key][2] in RiscSeleccionat:
                data_registre = self.xml[key][0]
                if data_registre >= data_ingres and data_registre <= data_ingres + d.timedelta(days=7):
                    num = 1
                else:
                    num = 0
            else:
                data_registre = 0
                num = 0
            self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS001", num, den, exc_den))  

    def get_data_derivacio(self):
        """ Obtenim taula amb data derivació i servei de derivació per a cada
            pacient amb RiscSeleccionat
        """

        self.id_data = {}
        self.servei = {}
        self.data = {}
        self.data_derivacio = {}
        self.data_psi = {}

        #A 11 de juliol de 2019 només hi ha 148 individus amb DataDerivacio i 90 d'ells 
        #compleixen criteris d'inclusió
        sql = "select id_cip_sec, camp_codi, camp_valor \
               from xml_detall where xml_tipus_orig = 'XML0000083' \
               and camp_codi = 'DataDerivacio'"
        for id, cod, val in u.getAll(sql, "import_jail"):
            if id in self.poblacio:
                self.id_data[id] = val

        sql = "select inf_numid, inf_servei_d_codi from oc2"
        for id, servei in u.getAll(sql, "import_jail"):
            #'50115' és el camp de text del servei de psiquiatria
            if servei == '50115':
                servei = 'PSI'
            self.servei[id] = (servei)
         
        sql = "select oc_numid, oc_data from oc1"
        for id, data in u.getAll(sql, "import_jail"):
            if id in self.servei:
                self.data[id] = (data, self.servei[id])

        for key in self.id_data:
            self.data_derivacio[key] = self.data[int(self.id_data[key])][0]
            if self.data[int(self.id_data[key])][1] == 'PSI':
                self.data_psi[key] = self.data_derivacio[key]

    def get_rs002(self):
        """ Calcula indicador RS002
            Den: Individus amb RiscSeleccionat '3', prioritat URGENT
            Num: Individus amb derivació a psiquiatria el mateix dia de la prioritat
        """   
        RiscSeleccionat = {'3'}
        
        sql="select data_ext from {}.dextraccio;".format("nodrizas")
        data_ext= u.getOne(sql,"nodrizas")[0]
        
        for key in self.poblacio:
            den = 1
            exc_den = 0
            data_ingres = self.poblacio[key][0]
            if key in self.xml:
                prior = self.xml[key][2]
                if prior in RiscSeleccionat:
                    data_registre = self.xml[key][0]
                    #Hi ha individus de self.xml que no tenen data de derivació
                    if key in self.data_psi.keys():
                        data_psi = self.data_psi[key]
                    else:
                        data_psi = 0
                    num = 0
                    if data_psi == data_registre:
                        num = 1
                    self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS002", num, den, exc_den))  

    def get_visites_psi(self):
        """ Obtenim taula amb dates de visita a psiquiatria posteriors a OC
        """
        
        self.data_visita7_psi = {}
        self.data_visita_psi = {}
        
        sql = "select id_cip_sec, visi_data_visita from visites \
               where visi_modul_codi_modul = 'PSI' and visi_situacio_visita = 'R' \
               order by 1,2 asc"
        for id, data in u.getAll(sql, "import_jail"):
            if id in self.xml and id in self.data_derivacio:
                if u.daysBetween(self.data_derivacio[id], data)>=0 and \
                u.daysBetween(self.data_derivacio[id], data)<=7:
                    self.data_visita7_psi[id] = data
                    if u.daysBetween(self.data_derivacio[id], data)<=1:
                        self.data_visita_psi[id] = data 

    def get_rs003(self):
        """ Calcula indicadors RS003 i RS005
            RS003
                Den: Individus amb RiscSeleccionat '3', prioritat URGENT i data OC darrers 6 mesos
                Num: Individus amb visita mòdul psiquiatria el mateix dia de la prioritat
            RS005
                Den: Individus amb RiscSeleccionat '2', prioritat PREFERENT i data OC darrers 6 mesos
                Num: Individus amb visita mòdul psiquiatria màxim 7 dies després d'OC
        """   
        RiscSeleccionat = {'3','2'}
        #rs005 RiscSeleccionat 2 i 7 dies
        
        sql="select data_ext from {}.dextraccio;".format("nodrizas")
        data_ext= u.getOne(sql,"nodrizas")[0]
        
        for key in self.poblacio:
            den = 1
            exc_den = 0
            data_ingres = self.poblacio[key][0]
            if key in self.xml:
                prior = self.xml[key][2]
                if prior in RiscSeleccionat:
                    #data_registre és la data de la prioritat
                    data_registre = self.xml[key][0]
     
                    #Hi ha individus de self.xml que no tenen data de derivació
                    if key in self.data_derivacio.keys():
                        data_der = self.data_derivacio[key]
                        if key in self.data_psi.keys():
                            data_psi = self.data_psi[key]
                        else:
                            data_psi = 0
                    else:
                        data_der = 0
                        data_psi = 0
                    
                    #data OC en els darrers 6 mesos
                    if data_der !=0 and u.monthsBetween(data_ext, data_der)<6:
                        num1 = 0
                        num2 = 0
                        #data_visita_psi dataset amb individus amb visita mòdul 
                        #psiquiatria el mateix dia o posterior a ordre clínica
                        
                        #data_visita7_psi dataset amb individus amb visita mòdul 
                        #psiquiatria fins als 7 dies posteriors a ordre clínica
                        
                        if prior=='3':
                            if key in self.data_visita_psi:
                                num1 = 1    
                            self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS003", num1, den, exc_den))  
                            
                        if prior=='2':
                            if key in self.data_visita7_psi:
                                num2 = 1
                            self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS005", num2, den, exc_den)) 

    def get_rs004(self):
        """ Calcula indicador RS004
            Den: Individus amb RiscSeleccionat '2', prioritat PREFERENT darrers 6 mesos
            Num: Individus amb derivació a psiquiatria el mateix dia de la prioritat
        """   
        RiscSeleccionat = {'2'}
        for key in self.poblacio:
            exc_den = 0
            den = 1
            data_ingres = self.poblacio[key][0]
            if key in self.xml.keys() and self.xml[key][2] in RiscSeleccionat:
                data_registre = self.xml[key][0]
                if key in self.data_psi.keys():
                    data_psi = self.data_psi[key]
                else:
                    data_psi = 0
                num = 0
                if data_psi == data_registre:
                    num = 1
                self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS004", num, den, exc_den))  


    def set_master(self):
        """."""
        cols = "(id int, dat date, up varchar(5), uba varchar(5), \
                 ind varchar(5), num int, den int, exc_den int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, TABLE, DATABASE)
    
    def export_khalix(self):
        """."""
        base = "select ind, 'Aperiodo', ics_codi, '{0}', \
                       'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum({0}) \
                from {1}.{2} a \
                inner join nodrizas.jail_centres b on a.up = b.scs_codi \
                where exc_den = 0 \
                group by ind, ics_codi"
        sql = " union ".join([base.format(analysis, DATABASE, TABLE)
                              for analysis in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """."""
        params = [("uba", "indicador", "RS0"),
                  ("pacient", "grup_codi", "RS0"),
                  ("cataleg", "indicador", "RS0"),
                  ("catalegPare", "pare", "RISCSUI")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)
        
    def export_ecap_uba(self):
        """."""
        sql = "select up, uba, ind, sum(num), sum(den), sum(num) / sum(den) \
               from {} \
               where exc_den = 0 \
               group by up, uba, ind".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """."""
        sql = "select distinct id, up, uba, '', '', ind, 0, \
                               hash_d, codi_sector \
               from {} a \
               inner join import_jail.u11 b on id = id_cip_sec \
               where num = 0 and \
                     exc_den = 0".format(TABLE)
        upload2 = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload2, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """."""
        umi = "http://sisap-umi.eines.portalics/indicador/codi/{}"
        upload = [(cod, des, i + 1, "RISCSUI", 1, 0, 0, 0, 0, 1, umi.format(cod), "MI", 0)
                  for i, (cod, des) in enumerate(LITERALS)]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("RISCSUI", "Risc Suicidi", 10, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)

if __name__ == "__main__":
    IRS_EAPP()