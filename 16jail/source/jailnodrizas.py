# coding: iso-8859-1
import sys, os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *

for tab in tbs:
    sql = "drop table if exists {}".format(tbs[tab]['taula'])
    execute(sql,db)
    sql = "create table {0} ({1})".format(tbs[tab]['taula'],tbs[tab]['create'])
    execute(sql,db)
 
#jail_criteris
tip = 'C' 
with openCSV(fls[tip]) as w:
    with open(pathcriteris, 'rb') as file:
        p=csv.reader(file, delimiter='@')
        for ind in p:
            taula,criteri,agr = ind[0],ind[2], ind[4]
            getTaula(taula).append(criteri)
            getAgr(taula)[criteri].append(int(agr))
            w.writerow([taula,ind[1],criteri,ind[3],agr,ind[5]])         
loadData(fls[tip],tbs[tip]['taula'],db)  

#jail_indicadors
tip = 'I' 
with openCSV(fls[tip]) as w:
    with open(pathindicadors, 'rb') as file:
        p=csv.reader(file, delimiter='@')
        for ind in p:
            w.writerow([ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8],ind[9],ind[10],ind[11]])         
loadData(fls[tip],tbs[tip]['taula'],db)        

#jail_indicadors EQA
tip = 'E' 
with openCSV(fls[tip]) as w:
    with open(patheqaindicadors, 'rb') as file:
        p=csv.reader(file, delimiter='@')
        for ind in p:
            w.writerow([ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6]])         
loadData(fls[tip],tbs[tip]['taula'],db)   

#jail_relacio
tip = 'R' 
with openCSV(fls[tip]) as w:
    with open(pathrelacio, 'rb') as file:
        p=csv.reader(file, delimiter='@')
        for ind in p:
            w.writerow([ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8],ind[9],ind[10],ind[11],ind[12]])         
loadData(fls[tip],tbs[tip]['taula'],db)    

#jail_metes
tip = 'M' 
with openCSV(fls[tip]) as w:
    with open(pathmetes, 'rb') as file:
        p=csv.reader(file, delimiter='@')
        for ind in p:
            w.writerow([ind[0],ind[1],ind[2],ind[3],ind[4]])         
loadData(fls[tip],tbs[tip]['taula'],db) 

#jail_nodrizas  

assig= getPoblacio()  

for table in tables:
    criteris = getTaula(table)
    agrupadors = getAgr(table)
    tip = tables[table]['tip']
    if len(criteris)> 0: 
        pac = []
        data = defaultdict(lambda: defaultdict(list))
        sql = master.format(tables[table]['var'],tables[table]['dat'],tables[table]['val'],table,str(tuple(criteris)))
        for id, variable, dat, val in getAll(sql, imp):
            try:        
                agrupador = agrupadors[variable]
                for agr in agrupador:
                    agr=int(agr)
            except KeyError:
                continue
            val = int(val)
            data[id, agr][dat].append(val)
        upload = []
        for (id, agr), dates in data.items():
            pac = [(id, agr, dat, sum(valors) / len(valors)) for dat, valors in dates.items()]
            upload.extend([(id, agr, dat, val, i) for i, (id, agr, dat, val) in enumerate(sorted(pac, key=lambda x: x[2], reverse=True), start=1)])
        listToTable(upload,tbs[tip]['taula'],db)
        
'''
            values=int(values)
            if id in assig:
                try:        
                    agrupador = agrupadors[variable]
                    for agr in agrupador:
                        agr=int(agr)
                except KeyError:
                    continue
                pac.append([id, agr, data, values])
        upload.extend([(id, agr, data, values, i) for i, (id, agr, data, values) in enumerate(sorted(pac, key=lambda x: x[2], reverse=True), start=1)])
        listToTable(upload,tbs[tip]['taula'],db)
        
'''