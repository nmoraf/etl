# coding: latin1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import string
import random
import operator
import re

validacion=False
agr_vac_dict= {15:'vhb',34:'vha'}

agr_table= {"variables": ("data_var",840,841)}


inm_A=set(['B15','B15.9','B15.0','C01-B15.9','C01-B15.0'])
inm_B=set(['B16','B16.0','B16.1','B16.2','B16.9','B18.0','B18.1','C01-B16.9',\
'C01-B16.0','C01-B16.1','C01-B16.2','C01-B18.0','C01-B18.1'])



serologies= {
    "vha":set(['S09785','013354','011444','S09885','003847']),
    "vhb": set(['D01785', 'D01885', '011194', '003738', '003740', 'S10085', '011557', '011556', '011315', '003739','003740']),
    "vhc": set(['S10385','S10485','S10685','R23185','S23185','003849','D02085','011448','011449','007144','S10585','003848'])
}

rna_pos_codis=set(["V01585","V01485","006807","006382","001995"])

codis_atc= ('J05AP54','J05AP51','J05AP55','J05P57')
nod="nodrizas"
alt="jail"
imp='import'



def get_codi_serologias():
    """Crea un diccionario en el que asigna a cada codigo de serologia de hepatitis el agrupador correspondiente que se encuentra en import.cat_dbscat.
       Devuelve {codi:agrupador}
    """
    sql="SELECT codi,AGRUPADOR FROM import.cat_dbscat WHERE agrupador LIKE '%V_VH%_SEROLOGIA%' or agrupador LIKE '%V_VHC_CARREGA%' ;"
    return {codi:agrupador for codi,agrupador in getAll(sql,imp)}

def get_labels_codis(labels,codis_list):
    """ Crea un diccionario que relaciona cada codigo de codis_list con un label en labels_list.
        Devuelve {codi:label}
    """
    return {codi: name for name,set_codis in zip(labels,codis_list) for codi in set_codis}

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]	

def get_pac_tract(current_date):
    """Obtiene los pacientes que en el ultimo any (fecha de inicio o de fin de hace menos de 12 meses) han estado medicados con los farmacos cuyo codi atc 
       esta en la variable global codis_atc.
       Devuelve un set de ids
    """
    sql='select id_cip_sec,ppfmc_pmc_data_ini,ppfmc_data_fi from import_jail.tractaments where ppfmc_atccodi in {}'.format(codis_atc)
    print sql
    return {id for id,ini,fi in getAll(sql,'import_jail') if monthsBetween(ini,current_date) < 12 or monthsBetween(fi,current_date) < 12}

def get_inmmunizats():
    """Obtiene los pacientes que presentan inmunizacion segun los codigos en las variables inm_A e inmB y se agrupan en un diccionario a nivel del
       virus para el que estan inmunizados. Esto se consigue utilizando un diccionario, inmmunizats_codis, que a cada codigo de inmunizacion le asigna
       el nombre de virus que corresponde.
       Devuelve un diccionario de sets {virus: set(ids inmunizados)}
    """
    inmmunizats_codis= get_labels_codis(['vha','vhb'],[inm_A,inm_B])
    inmmunizats_by_virus=defaultdict(set)
    sql="select id_cip_sec,imu_cod_ps from import_jail.immunitzats where imu_cod_ps in {};".format(tuple(inm_A | inm_B))
    for id,cod in getAll(sql,'import_jail'):
        inmmunizats_by_virus[inmmunizats_codis[cod]].add(id)
    return inmmunizats_by_virus

def getCipToHash():
    #cip_ant=getCipAnt()
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    return {cip: hash for cip, hash in getAll(sql, 'pdp') }

def getHashToId():
    hashId={}
    for id,hash_d in getAll("select id_cip_sec, hash_d from import_jail.u11", "import_jail"):
        hashId[hash_d]=id
    return hashId


def get_pacient_serolog(current_date):
    """ Obtiene tres estructuras que almacenan los siguentes datos:
            ->  Los pacientes que se les haya realizado alguna serologia en toda la vida (pacients) agrupando por descriptivo del codigo de serologia y 
                valor de la serologia
                pacients= { AGRUPADOR: {valor_serologia: id } }
            -> Los pacientes con el valor del ultmo registro de rna de VHC utilizando la funcion get_last_registre
                rna_pacients= {id: (ultimo_valor,fecha)}
            -> Los pacientes con rna de VHC positivo en el ultimo any:
                rna_last_year= set(ids)

    Devuelve estas tres estructuras(pacients, rna_pos_pacients,rna_last_year)
    """
    #Cada codigo de serologia lo relaciona con un descriptivo que indica el tipo de virus y el tipo de antigeno/carga
    codis_serologies=get_codi_serologias()
    
    pacients,rna_pacients,rna_last_year=defaultdict(lambda: defaultdict(set)),{},set()
    sql="select id_cip_sec, dat,cod,val from {}.jail_serologies where cod in {}".format(nod,tuple(codis_serologies.iterkeys()))
    for id, dat, codi,val in getAll(sql,nod):
        val=int(val)
        #se procesa la fecha, que viene en una string
        fecha=datetime.date(year=int(dat[:4]),month=int(dat[4:6]),day=int(dat[6:]))
        if val not in (1,0,9):
            val = 10
        if codis_serologies[codi] == 'V_VHC_CARREGA':
            rna_pacients[id]=get_last_registre(rna_pacients,id,(val,fecha))
            if monthsBetween(fecha,current_date) < 12 and val != 0:
                rna_last_year.add(id)
            
        else:
            pacients[codis_serologies[codi]][val].add(id)
        
    return pacients, rna_pacients,rna_last_year

def get_last_registre(dicti,key,label_fecha):
    """ Guarda como valor en un diccionario dicti el registro (tuple label_fecha con los elementos (label, fecha)) que tenga una fecha mas reciente.
        Cuando la llave key no existe en dicti, la crea y la asocia al valor label_fecha.
        Si key ya existe, se comprueba que la fecha en el tuple label_fecha sea mas reciente a la que esta guardada en el diccionari. Si esto es asi, se 
        devuelve el valor nuevo label_fecha. Sino, se devuelve el valor anteriormente guardado.
        Devuelve un tuple (label,ini) donde laber es una str y ini una fecha.
    """
    try: dicti[key]
    except KeyError: 
        return label_fecha
    else: 
        if dicti[key][1] < label_fecha[1]:
            return label_fecha
        else:
            return dicti[key]

            
def get_vacunats():
    """ Se consigue los pacientes presos (id_cip_sec < 0) que han recibido una dosis o mas de las vacunas con agrupador en agr_vac_dict.
        Agrupa usando el agr de la vacuna los pacientes que han sido vacunados.
        Devuelve {agr_vacuna: set(ids)}
    """
    vacunats=defaultdict(set)
    sql="select id_cip_sec,agrupador,datamax from {}.eqa_vacunes where agrupador in {} and dosis >= 1 and id_cip_sec < 0".format(nod,tuple(agr_vac_dict.keys()))
    for (id,agrupador,data) in getAll(sql,nod):
        vacunats[agr_vac_dict[agrupador]].add(id)
    return vacunats

def get_eco_fibro(current_date):
    """ Obtiene los pacientes con las variables eco y fibroscan en eqa_variables, que tienen la fecha mas reciente de la prueba hace tres meses como muy pronto.
        Primero se consigue los pacientes que tienen alguno de los agrupadores correspondientes con una fecha de hace al menos tres meses y luego se filtra por los que
        tienen los dos agrupadores.
        Devuelve un set de ids
    """
    set_ids=set()
    id_agrs=defaultdict(lambda: defaultdict(set))
    table='variables'
    data_field=agr_table[table][0]
    agrs=agr_table[table][1:]
    sql="select id_cip_sec,{},agrupador from {}.eqa_{} where agrupador in {} and id_cip_sec < 0 and usar=1 ".format(data_field,nod,table,agrs)
    for id,data,agr in getAll(sql,nod):
        if monthsBetween(data, current_date) > 2:
            id_agrs[id][agr].add(data)
    return {id for id in id_agrs if all([agr in id_agrs[id] for agr in agrs])}

def get_ingres(current_date):
    """ Obtiene la fecha de ingreso MAS RECIENTE en la prision en la que ESTA ACTUALMENTE. Esta fecha de ingreso esta definida como 
       la primera fecha de asignacion a un centro penitenciario en la tabla import.moviments en el cual actualmente reside (el registro mas reciente tiene fecha de fin nula)
       Devuelve un diccionario de pacientes con la estructura {'ingres':fecha assignacion,'up':up}
    """
    id_up_ass=defaultdict(lambda: defaultdict(dict))
    dext_s=current_date.strftime('%Y%m%d')

    #en la select estan ordenados los pacientes por orden ascendente y las fechas de inicio y fin de forma descendente, de mas reciente a mas antigua
    #Se espera, pues, que el primer registro sea aquel en el que la fecha final es nula (donde esta ahora mismo el paciente)
    sql = "select id_cip_sec, huab_data_ass,if(huab_data_final = 0, null, huab_data_final), huab_motiu_des, huab_up_codi from moviments where huab_data_ass <= {} order by 1 asc, 2 desc, 3 desc".format(dext_s)
    

    for id, ini, fi, motiu,up in getAll(sql, 'import_jail'):
        #Se utiliza una variable booleana, skip, para pasar al siguiente paciente una vez que se ha obtenido el ingreso definitivo

        #Si el paciente no tiene todavia asignacion, el paramentro skip es False
        if id not in id_up_ass:
            skip=False

        if skip:
            continue

        #Si hemos llegado a un registro en el que el motivo es este, significa que aqui ha salido de la carcel, de forma que fechas de ingreso anteriores no nos interesan
        if motiu == 'PER BAIXA EN EL CENTRE PENINTENCIARI':
            skip=True

        #Sino tiene fecha de fin, significa que en esa up es donde esta ahora mismo, y de primeras asignamos como fecha de ingreso la que se le asigna este registro    
        if not fi:
            id_up_ass[id]['up']=up
            id_up_ass[id]['ingres']=ini
            continue
        
        #Si la up es la misma a la que ha sido asignado, y el motivo del cambio de registro tienen alguna de las strings especificadas, significa
        #que es un cambio de modulo y no de centro, por lo que la fecha de ingreso se actualiza, ya que nos interesa cuando entro en el centro. (la fecha mas antigua de ese centro)
        if up == id_up_ass[id]['up'] and ('BAIXA PER CANVI' in motiu or 'PER BAIXA PROC' in motiu):
            id_up_ass[id]['ingres']=ini

    return id_up_ass   

def get_visites_modul(current_date):
    """ Obtiene los id_cip_sec que han tenido una visita en los ultimos 18 meses en el modul INFEC en el servei MG
        Devuelve un set de ids.
    """
    cipToHash=getCipToHash()
    printTime('cip to hash')
    HashToId=getHashToId()
    printTime('hash to id')
    sql="SELECT visi_data_alta,visi_usuari_cip FROM vistb043 WHERE visi_modul_codi_modul = 'INFEC' AND visi_servei_codi_servei ='MG'"   
    return {HashToId[cipToHash[cip]] for data,cip in getAll(sql,'6951') if monthsBetween(data,current_date) < 18}         


def get_indicador(codi,numerador,denominador,id_ingres):
    """ Obtiene la tabla del indicador a nivel de up y un listado con los pacientes no cumplidores.
        Recibe como argumentos:
        * numerador: un set de ids
        * denominador: un tuple con diferentes elementos para filtrar la poblacion asignada que forma parte del indicador. En este uple tenemos:
            - ini: dias minimos de ingreso en prision
            - fi: meses maximos de ingreso en prision 
            - set_ids_in: set de pacientes en el que tiene que estar el paciente para entrar como denominador
            - set_ids_not: set de pacientes en el que no puede estar el paciente para entrar como denominador
        Devuelve dos listas de tuples, las de nivel de up y las de nivel de paciente no cumplidor.
    """
    indicador=defaultdict(Counter)
    pacient_rows=[]
    ini,fi,set_ids_in,set_ids_not=denominador
    sql="select id_cip_sec,up,uba from {}.jail_assignada".format(nod)
    for id,up,uba in getAll(sql,nod):
        #Solo entran pacientes que tengan fecha de ingreso y que la up de la tabla de ass coincida con la de ingreso
        if id in id_ingres and id_ingres[id]['up'] ==up:
            ingres=id_ingres[id]['ingres']
            
            #En la variable other_filter, se guarda un booleano resultado de la siguiente conjuncion:
            # -> Que el id SI este en el set_ids_in si este existe, sino esta condicion se marca como True
            # -> Que el id NO este en el set_ids_no si este existe, sino esta condicion se marca como True
            other_filter= (id in set_ids_in if set_ids_in else True) and (id not in set_ids_not if set_ids_not else True)

            if  daysBetween(ingres,current_date) > ini and monthsBetween(ingres,current_date) < fi and other_filter:
                indicador[(up,uba)]["DEN"]+=1
                if id in numerador:
                    indicador[(up,uba)]["NUM"]+=1
                    
                else:
                    pacient_rows.append([id,up,uba,codi,ingres])

    return [(up, uba, codi,indicador[(up,uba)]["NUM"], indicador[(up,uba)]["DEN"]) for (up,uba) in indicador], pacient_rows

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

def get_id_hash_val():
    sql="select id_cip_sec,hash_d from import_jail.u11"
    return {id:hash_d for (id,hash_d) in getAll(sql,"import")}

def get_cip_val(hashes):
    sql="select usua_cip,usua_cip_cod from pdptb101 where usua_cip_cod in {}".format(hashes)
    return {hash_d: cip for cip,hash_d in getAll(sql,'pdp')}

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

def export_ecap():
    literals = (("EQA3401", "Cribratge Hepatitis A", True),
                ("EQA3402", "Cribratge Hepatitis B", True),
                ("EQA3403", "Cribratge Hepatitis C", True),
                ("EQA3406", "Continuitat assistencial hepatitis C", False),
                ("EQA3407", "Correcta vacunaci� (A+B) en pacients amb Hepatitis C", True),
                ("EQA3408", "Cobertura vacunal Hepatitis B", True),
                ("EQA3409", "Tractaments espec�fic Hepatitis C", False))
    params = [("uba", "indicador", "EQA34"),
              ("pacient", "grup_codi", "EQA34"),
              ("cataleg", "indicador", "EQA34"),
              ("catalegPare", "pare", "PROCHEP")]
    delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
    for param in params:
        sql = delete.format(*param)
        execute(sql, alt)
    sql = "select up, uba, indicador, sum(num), sum(den), sum(num) / sum(den) \
           from exp_vacAB_hep_new \
           group by up, uba, indicador"
    upload = [row for row in getAll(sql, alt)]
    listToTable(upload, "exp_ecap_iep_uba", alt)
    sql = "select distinct id, up, uba, '', '', indicador, 0, hash_d, codi_sector \
           from exp_vacAB_hep_new_pacient a \
           inner join import_jail.u11 b on id = id_cip_sec"
    upload = [row for row in getAll(sql, alt)]
    listToTable(upload, "exp_ecap_iep_pacient", alt)
    umi = "http://sisap-umi.eines.portalics/indicador/codi/{}"
    upload = [(cod, des, i + 1, "PROCHEP", 1, 0, 0, 0, 0, 1 * show, umi.format(cod), "I", 0)
              for i, (cod, des, show) in enumerate(literals)]
    listToTable(upload, "exp_ecap_iep_cataleg", alt)
    pare = [("PROCHEP", "EQA del proc�s de l'Hepatitis", 9, 0)]
    listToTable(pare, "exp_ecap_iep_catalegPare", alt)


if __name__ == "__main__":
    printTime("Inicio")
    table_name="exp_vacAB_hep_new"
    columns="(up varchar(20),uba varchar(5),indicador varchar(35),num int, den int)"
    createTable(table_name,columns,alt,rm=True)

    pacient_table="exp_vacAB_hep_new_pacient"
    columns_pacient="(id int, up varchar(20), uba varchar(5), indicador varchar(55), ingres date)"
    createTable(pacient_table,columns_pacient,alt,rm=True)

    current_date=get_date_dextraccio()
    printTime("Current date {}".format(current_date))

    #obtenemos los pacientes y su ingreso a una determinada up
    id_ingres=get_ingres(current_date)

    #RAW DATA
    #Serologias/RNA por virus+(antigeno/anticuerpo/carga) y por valor
    serologies_by_cod,rna_pacients,rna_last_year=get_pacient_serolog(current_date)

    #Agrupa en serologies_ABC los datos de serologia por VIRUS, es decir, pasa de las keys como 'V_VHB_SEROLOGIA_AC_S' a keys con solo el nombre del virus
    #'vha','vhb' y 'vhc' para tener todos los pacientes por valor de serlogia +/-/0, independientemente de que serologia sea, antigeno o anticuerpo.
    serologies_ABC=defaultdict(lambda:defaultdict(set))
    for cod in serologies_by_cod:
        for val in serologies_by_cod[cod]:
            serologies_ABC[cod[2:5].lower()][val]|=serologies_by_cod[cod][val]
    
    #Obtiene pacientes positivos de VHB para ciertas serologias especificas de anticuerpo y antigeno
    HbsAc_pos= set.union( *[serologies_by_cod[cod][1] for cod in  serologies_by_cod if cod.startswith('V_VHB_SEROLOGIA_AC_S')])
    HbsAg_pos= set.union( *[serologies_by_cod[cod][1] for cod in  serologies_by_cod if cod.startswith('V_VHB_SEROLOGIA_AG_S')])


    #Vacunados
    vacunats=get_vacunats()
    vacunats['vhc']=set()

    #Inmunizats
    inmmunizats_by_virus=get_inmmunizats()
    inmAB=set.intersection(*[inmmunizats_by_virus[vir] for vir in inmmunizats_by_virus])

    #PROCESS DATA
    #rna vhc postivos y con registro
    rna_pos_pacients={id_rna for id_rna in rna_pacients if rna_pacients[id_rna][0] != 0}
    
  
    #Vacunados de A y B/ serologia + de A y B
    viruses=('vha','vhb')
    vac_AB=set.intersection(*[vacunats[virus] for virus in viruses])
    vh_AB= HbsAc_pos & serologies_ABC['vha'][1]
    

    #En este diccionario las keys son el nombre/codigo del indicador y el valor un tuple en el que el primer elemento es el 
    # argumento 'numerador' y el segundo el argumento 'denominador' para la funcion general get_indicador
    codis_dict= {
        'EQA3407': ( vac_AB | vh_AB | inmAB ,(60,float("inf"),serologies_ABC['vhc'][1],None)),
        'EQA3408':( serologies_ABC['vhb'][1] | vacunats['vhb'] | inmmunizats_by_virus['vhb'], (60,float("inf"),None,HbsAg_pos)),
        'EQA3406':( rna_pacients, (180,12,serologies_ABC['vhc'][1],None)),
        'EQA3409': ( get_pac_tract(current_date), (60,float("inf"), rna_last_year & get_eco_fibro(current_date),None)),
        #'cont ass HepB': (get_visites_modul(current_date), (60,float("inf"),serologies_ABC['vhb'][1],None))
        # este esta comentado porque tarda mucho y creo que no esta definido del todo    
    }
    printTime('cont ass')

    #se anyade al diccionario codis_dict dinamicamente los indicadores de 'cribatge' ya que son lo mismo pero para cada uno de los virus de hepatitis
    codis_ind = {"vha": "EQA3401", "vhb": "EQA3402", "vhc": "EQA3403"}
    for virus in serologies:
        codi=codis_ind[virus]
        pac_ser=set.union(*[serologies_ABC[virus][val] for val in serologies_ABC[virus]])
        excl= rna_pacients.viewkeys() if virus == 'vhc' else set()
        codis_dict[codi]= ( pac_ser, (60,12,None, vacunats[virus] | excl) )


    if validacion:
        id_hash=get_id_hash_val()
        printTime('get hash id')
        #hash_cip=get_cip_val()
        #printTime('get cip val')

    #Para cada uno de los indicadores, se obtienen los argumentos 'numerador' y 'denominador' que se pasan a la funcion get_indicador
    for codi in codis_dict:
        print codi
        numerador,denominador=codis_dict[codi]
        rows,pacient_rows=get_indicador(codi,numerador,denominador,id_ingres)
        listToTable(rows,table_name,alt)

        if validacion:
            k= 15 if len(pacient_rows) >= 15 else len(pacient_rows)
            print k
            listToTable(random.sample(pacient_rows,k),pacient_table,alt)
            sql_val='select id,up from {} where indicador= "{}"'.format(pacient_table,codi)
            rows_txt= [ '{}@{}@@@35031850\n'.format( id_hash[id],up ) for id,up in getAll(sql_val,alt) ]
            with open('{}_validacion.txt'.format(codi),'w') as out_file:
                for row in rows_txt:
                    out_file.write(row)
        else:
            listToTable(pacient_rows,pacient_table,alt)

    export_ecap()

