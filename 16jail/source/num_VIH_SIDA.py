# coding: utf8

"""
Contatge pacients VIH o SIDA un dia a presó a 2018
"""

import sisapUtils as u

class VIH_SIDA(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_pacients_VIH_SIDA()
        self.get_pac_2018()

    def get_pacients_VIH_SIDA(self):
        """ Selecció de pacients de presons amb VIH o SIDA abans de 2019 
        """
        self.poblacio = set()
        sql="select id_cip_sec from eqa_problemes where ps = 101 \
        and dde < 20190101 and id_cip_sec < 0"
        for id in u.getAll(sql, "nodrizas"):
            self.poblacio.add(id)
        print(len(self.poblacio))
                
    def get_pac_2018(self):
        """ Dels individus de self.poblacio seleccionem només els han estat 
            almenys un dia a presons el 2018
        """
        self.pac_2018 = set()
        sql = "select id_cip_sec from moviments where \
        (huab_data_final >= 20180101 and huab_data_final <20190101) \
        or (huab_data_ass <20190101 and huab_data_final=0)"
        for id in u.getAll(sql, "import_jail"):
            self.pac_2018.add(id)
        print(len(self.pac_2018))
        ids=self.poblacio.intersection(self.pac_2018)
        print(ids)
        print(len(ids))
        

if __name__ == "__main__":
    VIH_SIDA()