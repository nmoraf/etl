# coding: utf8

"""
Indicadors Hepatitis EAPP.
"""

import sisapUtils as u
import csv as csv
import collections as c
from collections import defaultdict
import datetime as d
from dateutil.relativedelta import relativedelta

TABLE = "mst_indicadors_hepatitis_eapp"
DATABASE = "jail"
FILE = "HEPATITISEAPP"

LITERALS = (("EQA3401", "Cribratge Hepatitis A"),
                ("EQA3402", "Cribratge Hepatitis B"),
                ("EQA3403", "Cribratge Hepatitis C"),
                ("EQA3405", "Continuitat assistencial hepatitis B"),
                ("EQA3406", "Continuitat assistencial hepatitis C"),
                ("EQA3407", "Correcta vacunació (A+B) en pacients amb Hepatitis C"),
                ("EQA3408", "Cobertura vacunal Hepatitis B"),
                ("EQA3409", "Tractaments específic Hepatitis C"))

#Codis vacunes hepatitis A i hepatitis B per exclusions
agr_vac_dict= {15:'vacuna_hepatitis_b',34:'vacuna_hepatitis_a'}

#Agrupadors d'eco i fibroscan 
agr_table= {"variables": ("data_var",848,849)}

#Llistes d'immunitzats
inm_A=set(['B15','B15.9','B15.0','C01-B15.9','C01-B15.0'])
inm_B=set(['B16','B16.0','B16.1','B16.2','B16.9','B18.0','B18.1','C01-B16.9',\
'C01-B16.0','C01-B16.1','C01-B16.2','C01-B18.0','C01-B18.1'])

#Llista de serologies per càlcul de numeradors
serologies= {
    "vha": set(['S09785','013354','011444','S09885','003847']),
    "vhb": set(['D01785', 'D01885', '011194', '003738', '003740', 'S10085', '011557', '011556', '011315', '003739','003740']),
    "vhc": set(['S10385','S10485','S10685','R23185','S23185','003849','D02085','011448','011449','007144','S10585','003848']),
    "vhc_pos": set(['S10385','S10485','S10685','V01585','R23185','S23185','003849','011448','011449','006808','006383','007144','S10585','001996','003848']),
    "vhc_anticos_pos": set(['S10385','S10485','S10685','R23185','S23185','003849','011448','011449','007144','S10585'])
    }

#Llista de codis que corresponen a rna demanada
rna_codis=set(['V01385','V01485','006807','006382','001995'])

#Llista d'antígens Hbs (Extret de llista Trello)
antigens_HB_codis=set(['D01885','D01785','011315','011194','003738','003739','003740'])
    
def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format('nodrizas')
    return u.getOne(sql,'nodrizas')[0]
    
def get_labels_codis(labels,codis_list):
    """ Crea un diccionario que relaciona cada codigo de codis_list con un label en labels_list.
        Devuelve {codi:label}
    """
    return {codi: name for name,set_codis in zip(labels,codis_list) for codi in set_codis}
    
def getCipToHash():
    #cip_ant=getCipAnt()
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    return {cip: hash for cip, hash in u.getAll(sql, 'pdp') }

def getHashToId():
    hashId={}
    for id,hash_d in u.getAll("select id_cip_sec, hash_d from import_jail.u11", "import_jail"):
        hashId[hash_d]=id
    return hashId
  
def get_visites_modul(current_date):
    """ Obtiene los id_cip_sec que han tenido una visita en los ultimos 18 meses en el modul INFEC en el servei MG
        Devuelve un set de ids.
    """
    cipToHash=getCipToHash()
    HashToId=getHashToId()
    sql="SELECT visi_data_alta,visi_usuari_cip FROM vistb043 \
        WHERE visi_modul_codi_modul = 'INFEC' AND visi_servei_codi_servei ='MG'"   
    return {HashToId[cipToHash[cip]] for data,cip in u.getAll(sql,'6951') if u.monthsBetween(data,current_date) < 18 and cipToHash[cip] in HashToId}         

def get_eco_fibro(current_date):
    """ Obtiene los pacientes con las variables eco y fibroscan en eqa_variables, que tienen la fecha mas reciente de la prueba hace un año como muy pronto.
        Primero se consigue los pacientes que tienen alguno de los agrupadores correspondientes con una fecha de hace al menos un año y luego se filtra por los que
        tienen los dos agrupadores.
        Devuelve un set de ids
    """
    set_ids=set()
    id_agrs=defaultdict(lambda: defaultdict(set))
    table='variables'
    data_field=agr_table[table][0]
    agrs=agr_table[table][1:]
    sql="select id_cip_sec,{},agrupador from {}.eqa_{} where agrupador in {} \
        and id_cip_sec < 0 and usar=1 ".format(data_field,"nodrizas",table,agrs)
    for id,data,agr in u.getAll(sql,"nodrizas"):
        #if u.monthsBetween(data, current_date) > 2:
        if u.monthsBetween(data, current_date) < 12:
            id_agrs[id][agr].add(data)
    return {id for id in id_agrs if all([agr in id_agrs[id] for agr in agrs])}
    
def get_immunitzats():
    """Obtiene los pacientes que presentan inmunizacion segun los codigos en las variables inm_A e inmB y se agrupan en un diccionario a nivel del
       virus para el que estan inmunizados. Esto se consigue utilizando un diccionario, inmmunizats_codis, que a cada codigo de inmunizacion le asigna
       el nombre de virus que corresponde.
       Devuelve un diccionario de sets {virus: set(ids inmunizados)}
    """
    inmmunizats_codis= get_labels_codis(['vha','vhb'],[inm_A,inm_B])
    inmmunizats_by_virus=defaultdict(set)
    sql="select id_cip_sec,imu_cod_ps from import_jail.immunitzats where \
        imu_cod_ps in {};".format(tuple(inm_A | inm_B))
    for id,cod in u.getAll(sql,'import_jail'):
        inmmunizats_by_virus[inmmunizats_codis[cod]].add(id)
    return inmmunizats_by_virus
    
def get_pac_tract(current_date):
    """Obtiene los pacientes que en el ultimo any (fecha de inicio o de fin de hace menos de 12 meses) han estado medicados con los farmacos cuyo codi atc 
       esta en la variable global codis_atc.
       Devuelve un set de ids
    """
    sql="select id_cip_sec,ppfmc_pmc_data_ini,ppfmc_data_fi from \
        import_jail.tractaments where ppfmc_atccodi in {}".format(codis_atc)
    return {id for id,ini,fi in u.getAll(sql,'import_jail') if u.monthsBetween(ini,current_date) < 12 or u.monthsBetween(fi,current_date) < 12}

#Codis ATC dels retrovirals per EQA3409
codis_atc= ('J05AP08','J05AP51','J05AP54','J05AP55','J05AP56','J05AP57')

class HEP_EAPP(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_denom()
        #self.get_denom_EQA3409()        
        self.get_vacunats()
        self.get_serologies()
        self.get_indicadors()
        self.set_master()
        self.export_khalix()
        self.delete_ecap()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()
        self.export_taula()
    
    def get_denom(self):
        """Calcula nombre de dies a presó darrer any"""
        self.poblacio={}
        self.intern={}
        data_ext=get_date_dextraccio()
        data_ini=data_ext-relativedelta(years=1)
        
        sql= "select id_cip_sec, huab_up_codi, huab_uab_codi, huab_data_ass, \
            huab_data_final from moviments order by 1,4 desc"
        actual=0
        for id, up, uba, data_ass, data_final in u.getAll(sql,'import_jail'):
            if data_final is None or data_ass >= data_ini:
                if data_final is None:
                    delta_days=(data_ext-data_ass).days
                    self.intern[id]=id
                else:
                    delta_days=(data_final-data_ass).days
                
                if id !=actual:
                    self.poblacio[id] = (delta_days, up, uba)
                    actual = id
                else:
                    days = self.poblacio[id][0]
                    days += delta_days
                    self.poblacio[id] = (days, up, uba)

        self.poblacio = {key : value for key, value in self.poblacio.iteritems()\
                        if value[0] >= 60}


    def get_denom_EQA3409(self):
        """Obté presos amb mínim 1 dia a presó darrer any"""
        self.poblacio_EQA3409={}
        data_ext=get_date_dextraccio()
        data_ini=data_ext-relativedelta(years=1)
        
        sql= "select id_cip_sec, huab_up_codi, huab_uab_codi, huab_data_ass, \
            huab_data_final from moviments order by 1,4 desc"
        actual=0
        intern=1
        for id, up, uba, data_ass, data_final in u.getAll(sql,'import_jail'):
            if data_final is None or data_ass >= data_ini:
                if data_final is None:
                    delta_days=(data_ext-data_ass).days
                else:
                    delta_days=(data_final-data_ass).days
                    intern=0
                
                if id !=actual:
                    self.poblacio_EQA3409[id] = (delta_days, up, uba)
                    actual = id
                else:
                    days = self.poblacio_EQA3409[id][0] 
                    days += delta_days
                    self.poblacio_EQA3409[id] = (days, up, uba)

        self.poblacio_EQA3409 = {key : value for key, value in \
                                self.poblacio_EQA3409.iteritems() if value[0] > 0}
    
    def get_vacunats(self):
        """ Obtenim pacients vacunats amb una dosi o més d'hep. A o hep. B
            Agrupa fent servir agr de la vacuna
            Retorna {agr_vacuna: set(ids)}
        """
        self.vacunats = defaultdict(set)
        sql="select id_cip_sec, agrupador, datamax from {}.eqa_vacunes \
            where agrupador in {} and dosis >= 1 and \
            id_cip_sec < 0".format('nodrizas',tuple(agr_vac_dict.keys()))
        for id, agrupador, data in u.getAll(sql,'nodrizas'):
            self.vacunats[agr_vac_dict[agrupador]].add(id)
            
    def get_serologies(self):
        """ Obtenim pacients amb serologies, rna demanada i antígensB
        """
        self.serologiesA = {}
        self.serologiesB = {}
        self.serologiesC = {}
        self.serologiesC_pos = {}
        self.serologiesC_anticos_pos = {}
        self.rna_demanada = {}
        self.antigensB = {}
        sql="select id_cip_sec, cod from jail_serologies"
        for id, codi in u.getAll(sql,'nodrizas'):
            
            if id in self.poblacio:            
                if codi in serologies["vha"]:
                    self.serologiesA[id] = (codi)
                if codi in serologies["vhb"]:
                    self.serologiesB[id] = (codi)
                if codi in serologies["vhc"]:
                    self.serologiesC[id] = (codi)
                if codi in serologies["vhc_pos"]:
                    self.serologiesC_pos[id] = (codi)
                if codi in serologies["vhc_anticos_pos"]:
                    self.serologiesC_anticos_pos[id] = (codi)
                if codi in rna_codis:
                    self.rna_demanada[id] = (codi)
                if codi in antigens_HB_codis:
                    self.antigensB[id] = (codi)
                    
        self.rna_demanada_EQA3409 = {}
        sql="select id_cip_sec, cod, dat from jail_serologies"
        for id, codi, data in u.getAll(sql,'nodrizas'):
            data = d.datetime.strptime(data,"%Y%m%d")
            #if id in self.poblacio_EQA3409:
            if id in self.poblacio:
                if u.monthsBetween(data, get_date_dextraccio()) <=12 and \
                u.monthsBetween(data, get_date_dextraccio()) >=6 and \
                codi in rna_codis:            
                    self.rna_demanada_EQA3409[id] = (codi)
               
            
    def get_indicadors(self):
        """."""
        self.master = []
        
        #Indicador EQA3401
        for id in self.poblacio:
            den = 1
            num = 0
            exc_den = 0
            if id in self.vacunats['vacuna_hepatitis_a']:
                exc_den = 1
            elif id in self.serologiesA:
                num = 1
            self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3401", num, den, exc_den))
            
        #Indicador EQA3402
        for id in self.poblacio:
            den = 1
            num = 0
            exc_den = 0
            if id in self.vacunats['vacuna_hepatitis_b']:
                exc_den = 1
            elif id in self.serologiesB:
                num = 1
            self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3402", num, den, exc_den))
            
        #Indicador EQA3403
        for id in self.poblacio:
            den = 1
            num = 0
            exc_den = 0
            if id in self.rna_demanada:
                exc_den = 1
            elif id in self.serologiesC:
                num = 1
            self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3403", num, den, exc_den))
            
        #Indicador EQA3405
        #id's de pacients 
        llista_visitants_moduls=get_visites_modul(get_date_dextraccio())
        for id in self.poblacio:
            #Definir self.antigensB com el conjunt de persones amb antígen + 
            if id in self.antigensB and id in self.intern:
                den = 1
                num = 0
                exc_den = 0
                #Definir self.derivats com els pacients derivats a MG o INFEC
                if id in llista_visitants_moduls:
                    num = 1
                self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3405", num, den, exc_den))
        
        #Indicador EQA3406
        #Definir eco_fibroscan com els pacients amb eco i fibroscan el darrer any
        eco_fibroscan = get_eco_fibro(get_date_dextraccio())
        for id in self.poblacio:
            if self.poblacio[id][0]<=365 and self.poblacio[id][0] >=180 \
            and id in self.intern:
                #denom: Persones amb serologies C+ i rna+ 
                if id in self.serologiesC_pos and id in self.rna_demanada:
                    den = 1
                    num = 0
                    exc_den = 0
                    if id in eco_fibroscan:
                        num = 1
                    self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3406", num, den, exc_den))
        
        #Indicador EQA3408
        llista_immunitzats = get_immunitzats()
        for id in self.poblacio:
            if id in self.intern:
                den = 1
                num = 0
                exc_den = 0
                if id in self.antigensB:
                    exc_den = 1        
                    #Si immunitzats o vacunats amb dosi >=1 de vhb
                elif id in self.vacunats['vacuna_hepatitis_b'] or \
                id in llista_immunitzats['vhb']:
                    num = 1
                self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3408", num, den, exc_den))
            
        #Indicador EQA3407
        llista_immunitzats = get_immunitzats() 
        for id in self.poblacio:
            #Amb anticosos d'hepatitis C positiu
            if id in self.serologiesC_anticos_pos and id in self.intern:
                den = 1
                num = 0
                exc_den = 0        
                #Si immunitzats d'A i B o vacunats amb dosi >=1 per A i B
                if id in self.vacunats['vacuna_hepatitis_a'] and id in self.vacunats['vacuna_hepatitis_b']:
                    num = 1    
                elif id in llista_immunitzats['vha'] and id in llista_immunitzats['vhb']:
                    num = 1
                self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3407", num, den, exc_den))
                
        #Indicador EQA3409
        pacients_retrovirals = get_pac_tract(get_date_dextraccio())
        for id in self.rna_demanada_EQA3409:
            den = 1
            num = 0
            exc_den = 0        
            #Si tenen els codis ATC dels retrovirals definits al set codis_ATC
            if id in pacients_retrovirals:
                num = 1
            self.master.append((id, self.poblacio[id][1], self.poblacio[id][2], "EQA3409", num, den, exc_den))
        
    def set_master(self):
        """."""
        cols = "(id int, up varchar(5), uba varchar(5), ind varchar(8), \
                 num int, den int, exc_den int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, TABLE, DATABASE)
        
    def export_khalix(self):
        """."""
        base = "select ind, 'Aperiodo', ics_codi, '{0}', \
                       'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum({0}) \
                from {1}.{2} a \
                inner join nodrizas.jail_centres b on a.up = b.scs_codi \
                where exc_den = 0 \
                group by ind, ics_codi"
        sql = " union ".join([base.format(analysis, DATABASE, TABLE)
                              for analysis in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """."""
        params = [("uba", "indicador", "EQA34"),
                  ("pacient", "grup_codi", "EQA34"),
                  ("cataleg", "indicador", "EQA34"),
                  ("catalegPare", "pare", "PROCHEP")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)
        
    def export_ecap_uba(self):
        """."""
        sql = "select up, uba, ind, sum(num), sum(den), sum(num) / sum(den) \
               from {} \
               where exc_den = 0 \
               group by up, uba, ind".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """."""
        sql = "select distinct id, up, uba, '', '', ind, 0, \
                               hash_d, codi_sector \
               from {} a \
               inner join import_jail.u11 b on id = id_cip_sec \
               where num = 0 and \
                     exc_den = 0".format(TABLE)
        upload2 = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload2, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """."""
        umi = "http://sisap-umi.eines.portalics/indicador/codi/{}"
        upload = [(cod, des, i + 1, "PROCHEP", 1, 0, 0, 0, 0, 1, umi.format(cod), "I", 0)
                  for i, (cod, des) in enumerate(LITERALS)]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("PROCHEP", "EQA del procés de l'Hepatitis", 9, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)
          
    def export_taula(self):
        upload = self.poblacio.items()
        u.writeCSV(u.tempFolder + "pob_sum_2.csv", upload, sep=";")
        upload = self.vacunats.items()
        u.writeCSV(u.tempFolder + "vacunats.csv", upload, sep=";")
        upload = self.serologiesA.items()
        u.writeCSV(u.tempFolder + "serologiesA.csv", upload, sep=";")
        upload = self.serologiesB.items()
        u.writeCSV(u.tempFolder + "serologiesB.csv", upload, sep=";")
        upload = self.serologiesC.items()
        u.writeCSV(u.tempFolder + "serologiesC.csv", upload, sep=";")
        upload = self.serologiesC_pos.items()
        u.writeCSV(u.tempFolder + "serologiesC_pos.csv", upload, sep=";")
        upload = self.rna_demanada.items()
        u.writeCSV(u.tempFolder + "rna_demanada.csv", upload, sep=";")
        upload = self.intern.items()
        u.writeCSV(u.tempFolder + "interns.csv", upload, sep=";")
        upload = self.antigensB.items()
        u.writeCSV(u.tempFolder + "antigensB.csv", upload, sep=";")
        upload = {}
        eco = get_eco_fibro(get_date_dextraccio())
        for id in eco:
            upload[id] = id
        u.writeCSV(u.tempFolder + "eco_fibro.csv", upload.items(), sep=";")
        pac = list(get_pac_tract(get_date_dextraccio()))
        upload = {}
        for id in pac:
            upload[id] = id
        u.writeCSV(u.tempFolder + "retrovirals.csv", upload.items(), sep=";")
        immu = get_immunitzats()
        u.writeCSV(u.tempFolder + "immunitzats.csv", immu.items(), sep=";")
        upload = self.master
        u.writeCSV(u.tempFolder + "master.csv", upload, sep=";")

if __name__ == "__main__":
    HEP_EAPP()