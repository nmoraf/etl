# coding: iso-8859-1
from sisapUtils import *

db = "jail"

uba = "%s.exp_ecap_IEP_uba" % db
pac = "%s.exp_ecap_IEP_pacient" % db
dext = "nodrizas.dextraccio"  # compte
paceqa = "eqa_ind.exp_ecap_pacient"

cat = [['%s.exp_ecap_IEP_cataleg' % db,'prsCataleg'],
       ['%s.exp_ecap_IEP_catalegPare' % db,'prsCatalegPare'],
       ['%s.exp_ecap_IEP_catalegMetes' % db,'prsCatalegMetes']]

query = "select * from {0} where up <> '' \
        union \
        select up, 'PRS', indicador, sum(numerador), sum(denominador), round(sum(numerador) / sum(denominador), 4) from {0} where up <> '' group by up, indicador".format(uba)
table = "prsIndicadors"
exportPDP(query=query,table=table,dat=True)

query = "select distinct up,uba,'M',grup_codi,hash_d,sector,exclos from %s where uba<>'' " % (pac)
table = "prsLlistats"
exportPDP(query=query, table=table, truncate=True, pkOut=True)

query= "select distinct upinf,ubainf,'I',grup_codi,hash_d,sector,exclos from %s where ubainf<>'' " % (pac)
exportPDP(query=query, table=table, truncate=False, pkOut=True)

'''
query= "select data_ext from %s limit 1" % dext
table= "prsLlistatsData"
exportPDP(query=query,table=table,truncate=True)
'''
for r in cat:
   my, ora = r[0], r[1]
   query = "select * from %s" % my
   exportPDP(query=query, table=ora, datAny=True)

query = "select up,uba,'M',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.jail_centres on up=scs_codi inner join jail.mst_jail_eqaindicadors c on a.grup_codi=c.indicador where id_cip_sec <0 " % (paceqa)
table = "prseqaLlistats"
exportPDP(query=query, table=table, truncate=True, pkOut=True)

query = "select upinf,ubainf,'I',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.jail_centres on upinf=scs_codi inner join jail.mst_jail_eqaindicadors c on a.grup_codi=c.indicador where id_cip_sec <0 " % (paceqa)
table = "prseqaLlistats"
exportPDP(query=query, table=table, truncate=False, pkOut=True)
