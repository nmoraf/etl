from sisapUtils import *
import sys, os.path
from os import makedirs,listdir,remove
from collections import defaultdict,OrderedDict,Counter
from datetime import date,datetime

path = os.path.realpath("./") + "/dades_noesb/"
pathcriteris = path + 'jail_criteris.txt'
pathindicadors = path + 'jail_ind.txt'
patheqaindicadors = path + 'jail_eqa_ind.txt'
pathrelacio = path + 'jail_relacio.txt'
pathmetes = path + 'jail_metesIEP.txt'

imp= 'import_jail'
nod= 'nodrizas'
db= 'jail'
dext= 'dextraccio'
u11 = 'import_jail.u11'
Ordres = 'eapp_vistb016'
Seguiment = 'eapp_vistb220'

fls= {'C':tempFolder + 'jail_criteris.txt','I':tempFolder + 'jail_indicadors.txt','R':tempFolder + 'jail_relacio.txt','V':tempFolder + 'jail_variables.txt','P':tempFolder + 'jail_problemes.txt','M':tempFolder + 'jail_metes', 'H':tempFolder + 'jail_dingres','E':tempFolder + 'jail_eqa_ind.txt'}
tbs= {'C':{'taula':' mst_jail_criteris', 'create': "taula varchar(15) not null default'',inclusio double,criteri_codi varchar(20) not null default'',agrupador_desc varchar(150) not null default'',agrupador double,ps_tancat double"}
      ,'I':{'taula': 'mst_jail_indicadors', 'create': "indicador varchar(10) not null default'', descripcio varchar(150) not null default'', grup varchar(10) not null default'', grup_desc varchar(150) not null default'', baixa int, dbaixa date, llistat int, ordre int, grup_ordre int,subtotal int, tipus varchar(2) not null default'', ncol int"}
      ,'R':{'taula': 'mst_jail_relacio', 'create': "indicador varchar(10),tipus varchar(5),numero_criteri double,agrupador varchar(10),data_min double,data_max double,baixa double,data_baixa VARCHAR(10),valor_min double,valor_max double,var_ult double,var_num double,sense double"}
      ,'V':{'taula': 'mst_jail_variables', 'create': "id_cip_sec int,agrupador int,data_var date,valor double null, usar int"}
      ,'P':{'taula': 'mst_jail_problemes', 'create': "id_cip_sec int,agrupador int,data date, gravetat double null, usar int"}
      ,'M':{'taula': 'exp_ecap_IEP_catalegMetes', 'create': "indicador varchar(10) not null default'', up varchar(5) not null default'', tipus varchar(2) not null default'', metamin double, metamax double"}
      ,'E':{'taula': 'mst_jail_eqaindicadors', 'create': "indicador varchar(10) not null default'', descripcio varchar(150) not null default'', pare varchar(10) not null default'', grup_desc varchar(150) not null default'',llistat int, ordre int, invers int, lit_curt varchar(100) not null default''"}
      }
      
#per nodrizas
tables= {
    'variables': {'dat':'vu_dat_act','var':'vu_cod_vs','val':'vu_val','tip': 'V'}
    ,'activitats': {'dat':'au_dat_act','var':'au_cod_ac','val':'if(au_val="",0,au_val)','tip':'V'} 
    ,'problemes': {'dat':'pr_dde','var':'pr_cod_ps','val':'pr_gra','tip':'P'} 
    }

    
criteris_var,criteris_act,criteris_ps,err = [],[],[],[]

agr_var,agr_act,agr_ps,agr_err = defaultdict(list),defaultdict(list),defaultdict(list),defaultdict(list)

master= "select id_cip_sec,{0},date_format({1},'%Y%m%d'),{2} from {3} where {0} in {4}"

#per indicadors

components = [['den'],['num'],['excl']]


iepFile = tempFolder + 'jail_iep.txt'
iepTable = 'mst_iep_indicadors_pacient'
iepCreate = "id_cip_sec int,  indicador varchar(10), up varchar(5) not null default'', uba varchar(20) not null default'',upinf varchar(5) not null default'', ubainf varchar(20) not null default''\
            ,edat int, sexe varchar(1) not null default'',num double, den double, excl double"

#per Ordres de Tractament
sqlOrdres = 'select id_cip_sec, tract_seq, tract_data_creacio, tract_data_fi from {},nodrizas.dextraccio where  tract_data_creacio between date_add(date_add(data_ext,interval - 6 month),interval + 1 day) and date_add(data_ext,interval - 7 day)'
sqlSeguiment = 'select sgtr_tract_seq from {} where sgtr_num_dosi>0'
ordresFile = tempFolder + 'jail_ordres.txt'
ordresTable = 'mst_iep0702_pacient'
tableDM2 = 'mst_iep0703_pacient'
tablepresc = 'mst_presc_pacient'

#per khalix

indicKhalix = {}
iepkhxFile = tempFolder + 'jail_khalix_iep.txt'
iepkhxTable = 'exp_khalix_iep_uba'
iepkhxCreate = "indicador varchar(10), up varchar(5), tipus varchar(1),uba varchar(20) not null default'',edat varchar(10), sexe varchar(10),analisi varchar(10), valor double"

#export khalix

exportIepUp = "select indicador,concat('A','periodo'),ics_codi,analisi,edat,'NOINSASS',sexe,'N',sum(valor) from {0}.{1} a inner join nodrizas.jail_centres b on a.up=scs_codi where tipus='M' group by indicador,ics_codi,analisi,edat,sexe"
exportIepUBA = "select indicador,concat('A','periodo'),concat(ics_codi,uba),analisi,'NOCAT','NOINSASS','DIM6SET','N',sum(valor) from {0}.{1} a inner join nodrizas.jail_centres b on a.up=scs_codi where tipus='M' and indicador not in ('IEP0102','IEP0103') group by indicador,concat(ics_codi,tipus,uba),analisi"
fileIepUp = "IEP_JAIL"
fileIepUBA = "IEP_UBA_JAIL"

#export a ecap

IEPecap_ind = "exp_ecap_IEP_uba"
IEPecap_pac = "exp_ecap_IEP_pacient"
IEPcataleg = "exp_ecap_IEP_cataleg"
IEPcatalegPare = "exp_ecap_IEP_catalegPare"

