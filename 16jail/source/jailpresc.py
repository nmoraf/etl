# coding: iso-8859-1
import sys
import os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *

nod = 'nodrizas'
db = 'jail'
impj = 'import_jail'

ind_catalogat = 'IEP0102'
ind_recomanat = 'IEP0103'
ind_recomanatP = 'IEP0104'


def get_Poblacio():
    pob = {}
    sql = "select id_cip_sec, up, uba,edat, sexe from jail_assignada"
    for id, up, uba, edat,sexe in getAll(sql, nod):
        pob[id] = {'up': up, 'uba': uba, 'edat': edat, 'sexe': sexe}
    return pob
    
def get_recomanats():
    recomanats = {}
    sql = 'select atc from import.cat_sisap_ct_atc_recomanats_eapp'
    for atc, in getAll(sql, nod):
        recomanats[atc] = True
    return recomanats
     

def get_do(pob, recomanats):
    upload = []
    sql = "select id_cip_sec, ppfmc_pf_codi, ppfmc_atccodi, ppfmc_pmc_amb_cod_up, ppfmc_pmc_amb_num_col from tractaments a, nodrizas.dextraccio where ppfmc_data_fi >data_ext"
    for id, catalogat, atc, up, col in getAll(sql, impj):
        try:
            upass = pob[id]['up']
            ubaass = pob[id]['uba']
            edat = pob[id]['edat']
            sexe = pob[id]['sexe']
        except KeyError:
            continue
        if upass == up:
            excl = 0
            den = 1
            num = 1
            if int(catalogat) == 0:
                num = 0
            upload.append([id, ind_catalogat, up, col, None, None, edat, sexe, num, den, excl])
            excl = 0
            den = 1
            num = 0
            if atc in recomanats:
                num = 1
            upload.append([id, ind_recomanat, up, col, None, None, edat, sexe, num, den, excl])
        excl = 0
        den = 1
        num = 0
        if atc in recomanats:
            num = 1
        upload.append([id, ind_recomanatP, upass, ubaass, None, None, edat, sexe, num, den, excl])
    listToTable(upload, tablepresc, db)
        
create = "(id_cip_sec int,  indicador varchar(10), up varchar(5) not null default'', uba varchar(15) not null default'',upinf varchar(5) not null default'', ubainf varchar(5) not null default'' \
            ,edat int, sexe varchar(1) not null default'',num double, den double, excl double)"
createTable(tablepresc,create,db, rm=True)

pob = get_Poblacio()
recomanats = get_recomanats()
get_do(pob, recomanats)


