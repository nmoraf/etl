# coding: iso-8859-1
import sys, os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *

#4 khalix

sql = "drop table if exists {}".format(iepkhxTable)
execute(sql,db)
sql = "create table {0} ({1})".format(iepkhxTable,iepkhxCreate)
execute(sql,db)

sql = "select id_cip_sec,indicador,up,uba,upinf,ubainf,edat,sexe,num,den from {} where excl=0".format(iepTable)
for id,indicador,up,uba,upinf,ubainf,edat,sexe,num,den in getAll(sql,db):
    if (indicador,up,uba,'M',ageConverter(edat,5),sexConverter(sexe)) in indicKhalix:
        indicKhalix[(indicador,up,uba,'M',ageConverter(edat,5),sexConverter(sexe))]['num'] += num
        indicKhalix[(indicador,up,uba,'M',ageConverter(edat,5),sexConverter(sexe))]['den'] += den
    else:
        indicKhalix[(indicador,up,uba,'M',ageConverter(edat,5),sexConverter(sexe))] = {'num':num,'den':den}
    if (indicador,upinf,ubainf,'I',ageConverter(edat,5),sexConverter(sexe)) in indicKhalix:
        indicKhalix[(indicador,upinf,ubainf,'I',ageConverter(edat,5),sexConverter(sexe))]['num'] += num
        indicKhalix[(indicador,upinf,ubainf,'I',ageConverter(edat,5),sexConverter(sexe))]['den'] += den
    else:
        indicKhalix[(indicador,upinf,ubainf,'I',ageConverter(edat,5),sexConverter(sexe))] = {'num':num,'den':den}

with openCSV(iepkhxFile) as w:        
    for (indicador,up,uba,tipus,edat,sexe),d in indicKhalix.items():
        num = d['num']
        den = d['den']
        w.writerow([indicador,up,tipus,uba,edat,sexe,'NUM',num]) 
        w.writerow([indicador,up,tipus,uba,edat,sexe,'DEN',den]) 
loadData(iepkhxFile,iepkhxTable,db)

error = []
error.append(exportKhalix(exportIepUp.format(db,iepkhxTable),fileIepUp))
error.append(exportKhalix(exportIepUBA.format(db,iepkhxTable),fileIepUBA))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
 
