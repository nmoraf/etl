# coding: iso-8859-1
import sys
import os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *

nod = 'nodrizas'
db = 'jail'

codi_ind = 'IEP0703'

def get_Poblacio():
    pob = {}
    sql = 'select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates from jail_assignada'
    for id, up, uba, upinf, ubainf, edat,sexe,ates in getAll(sql, nod):
        pob[id] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf':ubainf, 'edat': edat, 'sexe': sexe, 'ates': ates}
    return pob

def get_tires():
    tires = {}
    sql = 'select id_cip_sec from tires, nodrizas.dextraccio where fit_data_alta <= data_ext and (fit_dat_baixa = 0 or fit_dat_baixa > data_ext)'
    for id, in getAll(sql, 'import_jail'):
        tires[id] = True
    return tires
    
pob = get_Poblacio()
tires = get_tires()

indicadors = {}
sql = "select distinct id_cip_sec from eqa_problemes where ps=18 and id_cip_sec<0"
for id, in getAll(sql, nod):
    try:
        up = pob[id]['up']
        uba = pob[id]['uba']
        upinf = pob[id]['upinf']
        ubainf = pob[id]['ubainf']
        edat = pob[id]['edat']
        sexe = pob[id]['sexe']
        ates = pob[id]['ates']
    except KeyError:
        continue 
    indicadors[id, up, uba, upinf, ubainf, edat,sexe] = {'den': 1, 'num':0, 'excl': 0}
    if id in tires:
            indicadors[id, up, uba, upinf, ubainf, edat,sexe]['num'] += 1
upload = []
for (id, up, uba, upinf, ubainf, edat,sexe), rec in indicadors.items():
    upload.append([id, codi_ind, up, uba, upinf, ubainf, edat, sexe, rec['num'],rec['den'], rec['excl']])
    

create = "(id_cip_sec int,  indicador varchar(10), up varchar(5) not null default'', uba varchar(5) not null default'',upinf varchar(5) not null default'', ubainf varchar(5) not null default'' \
            ,edat int, sexe varchar(1) not null default'',num double, den double, excl double)"
createTable(tableDM2,create,db, rm=True)

listToTable(upload, tableDM2, db)
