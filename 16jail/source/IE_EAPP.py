# coding: latin1

"""
Indicadors IE EAPP.
"""

import collections as c
import datetime as d

import sisapUtils as u


TABLE = "mst_indicadors_pacient_ie_eapp"
DATABASE = "jail"
FILE = "JAILINFENLL"

INDICADORS = {("IE002", "IE006"): {"den": ["APSegHosp"],
                                   "num": ["PVVisHospD", "PVVisHospD2", "PVVisHospD3"],  # noqa
                                   "ass": ["SVAsistHospit", "SVAsistHospit2", "SVAsistHospit3"]},  # noqa
              ("IE003", "IE007"): {"den": ["APSegCSM"],
                                   "num": ["PVVisSMD"],
                                   "ass": ["SVAsistSM"]},
              ("IE004", "IE008"): {"den": ["APSegCas"],
                                   "num": ["PVServMetadD"],
                                   "ass": ["SVAsistCASS"]},
              ("IE005", "IE009"): {"den": ["APSegPrim"],
                                   "num": ["PVVisMetgeD", "PVVisInfermD"],
                                   "ass": ["SVAsistMetge", "SVAsistInfermer"]}}
EXCLUSIONS = ("NOforaCAT", "NOExpulsio", "NORecollit", "NODemorada", "NOAnticipada")  # noqa
LITERALS = (("IE001", "Criteris d'inclusi� d'infermera d'enlla�"),
            ("IE002", "Visita de seguiment hospitalaria"),
            ("IE003", "Visita de seguiment a XSM"),
            ("IE004", "Visita de seguiment a CAS"),
            ("IE005", "Visita de seguiment a AP"),
            ("IE006", "Assistencia a hospital"),
            ("IE007", "Assistencia a XSM"),
            ("IE008", "Assistencia a CAS"),
            ("IE009", "Assistencia a AP"))


class EAPP(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_xml()
        self.get_exclosos()
        self.get_consentits()
        self.get_ie001()
        self.get_components()
        self.set_master()
        self.export_khalix()
        self.delete_ecap()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()

    def get_poblacio(self):
        """Assignem a cada pacient a l'�ltim EAPP on ha estat durant l'any."""
        self.poblacio = {}
        sql = "select id_cip_sec, huab_data_ass, huab_up_codi, huab_uab_codi \
               from moviments, nodrizas.dextraccio \
               where huab_data_ass <= data_ext and \
                     (huab_data_final = 0 or \
                      huab_data_final > adddate(data_ext, interval -1 year)) \
               order by 1, 2 desc"
        actual = 0
        for id, ini, up, uba in u.getAll(sql, "import_jail"):
            if id != actual:
                if uba != 'M16' and (up != '07674' or up != '07680'):
                    self.poblacio[id] = (up, uba)
                    actual = id

    def get_xml(self):
        """."""
        self.xml = c.defaultdict(dict)
        sql = "select id_cip_sec, xml_data_alta, camp_codi, camp_valor \
               from xml_detall, nodrizas.dextraccio \
               where xml_tipus_orig = 'ENLL_PRES' and \
                     xml_data_alta between adddate(data_ext, interval -1 year)\
                                           and data_ext"
        for id, dat, cod, val in u.getAll(sql, "import_jail"):
            if id in self.poblacio:
                self.xml[(id, dat)][cod] = val

    def get_exclosos(self):
        """."""
        self.exclosos = set()
        for key, dades in self.xml.items():
            if any([dades.get(camp) == "S" for camp in EXCLUSIONS]):
                self.exclosos.add(key)

    def get_consentits(self):
        """."""
        self.consentits = {}
        for key, dades in self.xml.items():
            if dades.get("PVInfSigCI") == "S":
                self.consentits[key] = dades.get("PVCIEcap")

    def get_ie001(self):
        """."""
        self.master = []
        rca = c.defaultdict(set)
        sql = "select id_cip_sec, com22_data_loc from eapp_usutb196a"
        for id, dat in u.getAll(sql, "import_jail"):
            rca[id].add(dat)
        for key, ci in self.consentits.items():
            if ci:
                dci = d.datetime.strptime(ci, "%d/%m/%Y").date()
                dates = rca[key[0]]
                up, uba = self.poblacio[key[0]]
                den = 1
                num = 1 * any([(drca - dci).days < 15 for drca in dates])
                exc = 1 * (key in self.exclosos)
                self.master.append((key[0], key[1], up, uba, "IE001", den, num, exc))  # noqa

    def get_components(self):
        """."""
        for key in self.consentits:
            dades = self.xml[key]
            up, uba = self.poblacio[key[0]]
            for indicadors, components in INDICADORS.items():
                den = 1 * any([dades.get(codi) == "S" for codi in components["den"]])  # noqa
                num = 1 * any([dades.get(codi) for codi in components["num"]])
                ass = 1 * any([dades.get(codi) == "S" for codi in components["ass"]])  # noqa
                exc = 1 * (key in self.exclosos)
                if den:
                    self.master.append((key[0], key[1], up, uba, indicadors[0], den, num, exc))  # noqa
                if num:
                    self.master.append((key[0], key[1], up, uba, indicadors[1], num, ass, exc))  # noqa

    def set_master(self):
        """."""
        cols = "(id int, dat date, up varchar(5), uba varchar(5), \
                 ind varchar(5), den int, num int, excl int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        base = "select ind, 'Aperiodo', ics_codi, '{0}', \
                       'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum({0}) \
                from {1}.{2} a \
                inner join nodrizas.jail_centres b on a.up = b.scs_codi \
                where excl = 0 \
                group by ind, ics_codi"
        sql = " union ".join([base.format(analysis, DATABASE, TABLE)
                              for analysis in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """."""
        params = [("uba", "indicador", "IE0"),
                  ("pacient", "grup_codi", "IE0"),
                  ("cataleg", "indicador", "IE0"),
                  ("catalegPare", "pare", "AGENLLAC")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)
        
    def export_ecap_uba(self):
        """."""
        sql = "select up, uba, ind, sum(num), sum(den), sum(num) / sum(den) \
               from {} \
               where excl = 0 \
               group by up, uba, ind".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """."""
        sql = "select distinct id, up, uba, '', '', ind, 0, \
                               hash_d, codi_sector \
               from {} a \
               inner join import_jail.u11 b on id = id_cip_sec \
               where num = 0 and \
                     excl = 0".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """."""
        umi = "http://sisap-umi.eines.portalics/indicador/codi/{}"
        upload = [(cod, des, i + 1, "AGENLLAC", 1, 0, 0, 0, 0, 1, umi.format(cod), "I", 0)
                  for i, (cod, des) in enumerate(LITERALS)]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("AGENLLAC", "Infermera enlla�", 8, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)

if __name__ == "__main__":
    EAPP()
