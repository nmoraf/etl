from jail_parametres import *

def getTaula(taula):

    if taula == 'variables':
        return criteris_var
    elif taula == 'activitats':
        return criteris_act
    elif taula == 'problemes':
        return criteris_ps
    else:
        return err
        
def getAgr(taula):

    if taula == 'variables':
        return agr_var
    elif taula == 'activitats':
        return agr_act
    elif taula == 'problemes':
        return agr_ps
    else:
        return agr_err
        
def getPoblacio():
    
    assig= {}
    tab = 'jail_assignada'
    sql= 'select id_cip_sec,up,uba,upinf,ubainf,edat,sexe,data_naix, last_visit from {}'.format(tab)
    for id,up,uba,upinf,ubainf,edat,sexe,naix, dingres in getAll(sql, nod):
        assig[id]= {'up':up,'uba':uba,'upinf':upinf,'ubainf':ubainf,'sexe':sexe,'edat':edat, 'dingres': dingres}
    return assig
    
    
def getIndicadors():

    indicadors = []
    tab = 'I'
    sql = "select indicador, grup from {} where baixa=0 and indicador <>'IEP0702'".format(tbs[tab]['taula'])
    for ind,grup in getAll(sql,db):
        indicadors.append([ind,grup])
    return indicadors
        
def getQuery(grup,agr,dmin,dmax,vmin,vmax,vult,vnum):

    if agr == 'ASS':
        t = 'A'
        jtable = 'nodrizas.jail_assignada'
    elif agr == 'USU40':
        sql = 'select id_cip_sec from import_jail.eapp_usutb040 where usua_cip_rca = {}'.format(vmin)
        t = 'res'
    else:
        sql = 'select max(taula) from {0} where agrupador = {1}'.format(tbs['C']['taula'],agr)
        taula,= getOne(sql,db)
        t = tables[taula]['tip']
    try:    
        jtable = tbs[t]['taula']
    except KeyError:
        ok=1
    if t == 'A':
        if dmin == 0:
            sql = 'select id_cip_sec from {}'.format(jtable)
        else:
            sql = "select id_cip_sec from {0},nodrizas.dextraccio where last_visit between date_add(date_add(data_ext,interval - {1} month),interval + 1 day) and date_add(data_ext,interval - {2} day)".format(jtable,int(dmin),int(dmax))
    elif t == 'V' and grup == 'IEP03':
        if vmin == None and vmax == None:
            sql = "select a.id_cip_sec from {0} a inner join nodrizas.jail_assignada b on a.id_cip_sec=b.id_cip_sec where data_var between date_add(last_visit,interval - {1} day) and date_add(last_visit,interval + {2} day) and agrupador={3}".format(jtable,int(dmin),int(dmax),agr)
        else:
            if vult == 1:
                sql = "select id_cip_sec from {0} where usar = 1 and  agrupador = {1} and (valor between {2} and {3})".format(jtable,agr,vmin,vmax)            
            else:
                sql = "select a.id_cip_sec from {0} a inner join nodrizas.jail_assignada b on a.id_cip_sec=b.id_cip_sec where data_var between date_add(last_visit,interval - {1} day) and date_add(last_visit,interval + {2} day) and agrupador={3} and (valor between {4} and {5})".format(jtable,int(dmin),int(dmax),agr,vmin,vmax)            
    elif t == 'V' and grup <> 'IEP03':
        if dmin == 0:
            dmin = 2000
            dmax = 0
        if vmin == None and vmax == None and vult == None:
            sql = "select id_cip_sec from {0},nodrizas.dextraccio where data_var between date_add(date_add(data_ext,interval - {1} month),interval + 1 day) and date_add(data_ext,interval - {2} month) and agrupador={3}".format(jtable,int(dmin),int(dmax),agr)
        else:
            sql = "select id_cip_sec from {0},nodrizas.dextraccio where data_var between date_add(date_add(data_ext,interval - {1} month),interval + 1 day) and date_add(data_ext,interval - {2} month) and agrupador={3} and valor between {4} and {5}".format(jtable,int(dmin),int(dmax),agr,vmin,vmax)
    elif t == 'P':
        sql = "select id_cip_sec from {0} where agrupador ={1}".format(jtable,agr)     
    return sql
