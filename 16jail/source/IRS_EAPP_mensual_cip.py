# coding: latin1

"""
Indicadors RS EAPP.
"""

import sisapUtils as u
import collections as c
import datetime as d
from dateutil.relativedelta import relativedelta

#Posem manualment les dates d'inici i de fi del període a extreure riscos
#El format a introduïr és any, mes, dia
data_ini = d.date(2019,11,01)
data_fi = d.date(2019,11,30)

LITERALS = (("RS001", "Criteris d'inclusió de risc de suicidi"),
            ("RS003", "Seguiment de risc elevat"),
            ("RS005", "Seguiment de risc moderat"))

#Codi per extreure la taula jail_atesa actualitzada fins al dia d'avui
#Modificació del codi per crear jail_atesa amb dies=0
# def jail_atesa_data_fi():
    # from sisapUtils import getAll, createTable, listToTable, getOne

    # table = 'jail_atesa_data_fi'
    # db = 'nodrizas'
    # dies = 0
    # dext, dext_s = getOne("select data_ext, date_format(data_ext, '%Y%m%d') from dextraccio", db)

    # dext = data_fi

    # moviments = []
    # actual = [None, None, None, None, None]
    # sql = "select id_cip_sec, huab_data_ass, if(huab_data_final = 0, null, huab_data_final), huab_up_codi, huab_uab_codi from moviments where huab_data_ass <= {} order by 1 asc, 2 asc, 3 desc".format(dext_s)
    # for id, ini, fi, up, uba in getAll(sql, 'import_jail'):
        # fi = dext if not fi or fi > dext else fi
        # if id == actual[0] and up == actual[3]:
            # actual[2] = fi
        # else:
            # if actual[2] == dext and (dext - actual[1]).days > dies:
                # moviments.append([actual[0], actual[3], actual[1], actual[4]])
            # actual = [id, ini, fi, up, uba]

    # createTable(table, '(id_cip_sec int, up varchar(5), ingres date, uba varchar(5), primary key (id_cip_sec))', db, rm=True)
    # listToTable(moviments, table, db)

def getCipToHash():
    #cip_ant=getCipAnt()
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    return {cip: hash for cip, hash in u.getAll(sql, 'pdp') }
    
def getHashToId():
    hashId={}
    for id,hash_d in u.getAll("select id_cip_sec, hash_d from import_jail.u11", "import_jail"):
        hashId[id]=hash_d
    return hashId

class IRS_EAPP(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_denom_rs001()
        self.get_xml()
        #self.get_rs001()
        self.get_data_derivacio()
        self.get_visites_psi()
        self.get_rs003()
        self.export_taula()

    def get_denom_rs001(self):
        """ Selecció de pacients que ingressen al centre els darrers 6 mesos 
        """
        self.poblacio = {}
        
        #nodrizas.jail_atesa data entrada presó a 30 dies de data extracció
        #nodrizas.jail_atesa_data_fi data entrada presó fins a data fi
        
        #crea la taula jail_atesa actualitzada a data_fi
        #jail_atesa_data_fi()
  
        #passem data a string per a filtrar correctament
        data_start = data_ini.strftime("%Y%m%d")
        
        #passem data a string per a filtrar correctament
        end_data = data_fi.strftime("%Y%m%d")
        
        sql = "select id_cip_sec, ingres, up, uba from jail_atesa_avui \
               where ingres > {} and ingres < {} \
               order by 1,2 desc".format(data_start, end_data)
        
        #A cada individu li assignem només la seva darrera data d'ingrés        
        actual = 0
        for id, ini, up, uba in u.getAll(sql, "nodrizas"):
            if id !=actual:
                self.poblacio[id] = (ini, up, uba)
                actual = id
        print(len(self.poblacio))
                
    def get_xml(self):
        """ Dels individus de self.poblacio seleccionem només els que tenen 
            RiscSeleccionat a la variable XML0000083 de xml_detall
        """
        self.xml = c.defaultdict(dict)
        sql = "select id_cip_sec, xml_data_alta, camp_codi, camp_valor \
               from xml_detall where xml_tipus_orig = 'XML0000083' and \
               camp_codi ='RiscSeleccionat' order by 1,2 asc"
        
        #A cada individu li assignem només el primer valor després 
        #de la data d'ingrés
        #S'obvien els que tenen data de RiscSeleccionat anterior a data ingrés
        actual = 0
        for id, dat, cod, val in u.getAll(sql, "import_jail"):
            if id in self.poblacio:
                if dat >= self.poblacio[id][0] and id != actual:
                    actual = id
                    self.xml[id] = (dat, cod, val)
                    
    def get_data_derivacio(self):
        """ Obtenim taula amb data derivació i servei de derivació per a cada
            pacient amb RiscSeleccionat
        """

        self.id_data = {}
        self.servei = {}
        self.data = {}
        self.data_derivacio = {}
        self.data_psi = {}

        #A 11 de juliol de 2019 només hi ha 148 individus amb DataDerivacio i 90 d'ells 
        #compleixen criteris d'inclusió
        sql = "select id_cip_sec, camp_codi, camp_valor \
               from xml_detall where xml_tipus_orig = 'XML0000083' \
               and camp_codi = 'DataDerivacio'"
        for id, cod, val in u.getAll(sql, "import_jail"):
            if id in self.poblacio:
                self.id_data[id] = val

        sql = "select inf_numid, inf_servei_d_codi from oc2"
        for id, servei in u.getAll(sql, "import_jail"):
            #'50115' és el camp de text del servei de psiquiatria
            if servei == '50115':
                servei = 'PSI'
            self.servei[id] = (servei)
         
        sql = "select oc_numid, oc_data from oc1"
        for id, data in u.getAll(sql, "import_jail"):
            if id in self.servei:
                self.data[id] = (data, self.servei[id])

        for key in self.id_data:
            self.data_derivacio[key] = self.data[int(self.id_data[key])][0]
            if self.data[int(self.id_data[key])][1] == 'PSI':
                self.data_psi[key] = self.data_derivacio[key]

    def get_visites_psi(self):
        """ Obtenim taula amb dates de visita a psiquiatria posteriors a OC
        """
        
        self.data_visita7_psi = {}
        self.data_visita_psi = {}
        
        sql = "select id_cip_sec, visi_data_visita from visites \
               where visi_modul_codi_modul = 'PSI' and visi_situacio_visita = 'R' \
               order by 1,2 asc"
        for id, data in u.getAll(sql, "import_jail"):
            if id in self.xml and id in self.data_derivacio:
                if u.daysBetween(self.data_derivacio[id], data)>=0 and \
                u.daysBetween(self.data_derivacio[id], data)<=7:
                    self.data_visita7_psi[id] = data
                    if u.daysBetween(self.data_derivacio[id], data)<=1:
                        self.data_visita_psi[id] = data 

    def get_rs003(self):
        """ Calcula indicadors RS003 i RS005
            RS003
                Den: Individus amb RiscSeleccionat '3', prioritat URGENT i data OC entre data_ini i data_fi
                Num: Individus amb visita mòdul psiquiatria el mateix dia de la prioritat
            RS005
                Den: Individus amb RiscSeleccionat '2', prioritat PREFERENT i data OC entre data_ini i data_fi
                Num: Individus amb visita mòdul psiquiatria màxim 7 dies després d'OC
        """   
        self.master = []
        
        RiscSeleccionat = {'3','2'}
        #rs005 RiscSeleccionat 2 i 7 dies
        
        sql="select data_ext from {}.dextraccio;".format("nodrizas")
        data_ext= u.getOne(sql,"nodrizas")[0]
        
        for key in self.poblacio:
            den = 1
            exc_den = 0
            data_ingres = self.poblacio[key][0]
            if key in self.xml:
                prior = self.xml[key][2]
                if prior in RiscSeleccionat:
                    #data_registre és la data de la prioritat
                    data_registre = self.xml[key][0]
     
                    #Hi ha individus de self.xml que no tenen data de derivació
                    if key in self.data_derivacio.keys():
                        data_der = self.data_derivacio[key]
                        if key in self.data_psi.keys():
                            data_psi = self.data_psi[key]
                        else:
                            data_psi = 0
                    else:
                        data_der = 0
                        data_psi = 0
                    
                    #data OC entre data_ini i data_fi
                    if data_der !=0 and u.monthsBetween(data_fi, data_der)<6:
                        num1 = 0
                        num2 = 0
                        #data_visita_psi dataset amb individus amb visita mòdul 
                        #psiquiatria el mateix dia o posterior a ordre clínica
                        
                        #data_visita7_psi dataset amb individus amb visita mòdul 
                        #psiquiatria fins als 7 dies posteriors a ordre clínica
                        
                        if prior=='3':
                            if key in self.data_visita_psi:
                                num1 = 1    
                            self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS003", num1, den, exc_den))  
                            
                        if prior=='2':
                            if key in self.data_visita7_psi:
                                num2 = 1
                            self.master.append((key, data_ingres, self.poblacio[key][1], self.poblacio[key][2], "RS005", num2, den, exc_den)) 

    def export_taula(self):
        """."""
        header = [("id", "data_ingres", "up", "uba", "indicador", "num",
                   "den", "exc_den")]
        upload = self.master
        u.writeCSV(u.tempFolder + "master_mes_v3.csv", header + upload, sep=";")

if __name__ == "__main__":
    IRS_EAPP()