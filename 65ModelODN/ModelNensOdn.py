# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb el model d'assignació de d'odntologia de nens

"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter

pat_mental= "('P71','P72','P73','P74','P76','P76','P79','P80','P82','P85','P98')"


etiquetes = "('RODN', 'RODON')"
moduls = "( 'RODN', 'RODN1', 'RODN2', 'RODN3', 'RODN4', 'RODN5', 'RODON', 'RODON1', 'RODON2', 'RODON3', 'RODON4', 'RODON5')"

class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dext, self.dext1a = u.getOne("select date_format(data_ext,'%Y%m%d'), date_format(date_add(date_add(data_ext, interval - 1 year),interval + 1 day), '%Y%m%d') from dextraccio", "nodrizas")
        self.get_centres()
        self.get_poblacio()
        self.get_hash()
        self.get_gma()
        self.get_medea()
        self.get_patsocial()
        self.get_patmental()
        self.get_escolars()
        self.get_visites()
        self.export_dades_pacient()

    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

  
    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tancament
           - Menors de 15 anys
        """
        self.dades = {}
        self.ambulatoris = set()
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        altapob = {id: alta for id,alta in u.getAll("select id_cip_sec, date_format(usua_data_alta,'%Y%m%d') \
                                                from assignada, nodrizas.dextraccio", "import")} 
        pobbefore = {id: True for id, in u.getAll("select concat(id_cip_sec, up) \
                                                from assignadahistorica where dataany<2017", "import")}
        sql = "select id_cip_sec, codi_sector, up,  uba, edat, sexe, nacionalitat, institucionalitzat, nivell_cobertura, centre_codi, centre_classe \
                from assignada_tot \
                where edat <15"
        for id, sec, up, uba, edt, sex, nac, insti, cob, cod, cla in u.getAll(sql, "nodrizas"):
            if up in self.centres and up in self.centres:
                identify = str(id) + str(up) 
                alta = altapob[id]
                nouvinguts = 1 if self.dext1a <= alta <= self.dext and edt > 0 and identify not in pobbefore else 0
                nouvingutsimm = 1 if nac in renta and self.dext1a <= alta <= self.dext and edt > 0 and identify not in pobbefore  else 0
                self.ambulatoris.add((cod, cla))
                cob1 = 1 if cob == 'PN' else 0
                nac1 = 1 if nac in renta else 0
                if edt >= 0:
                    self.dades[id] = {
                                      'hash': None, 'sector': sec, 'edat': edt,
                                      'sex': sex, 'gma_cod': None, 'gma_cmplx': None,
                                      'gma_num': None, 'medea': None, 'insti': insti,
                                      'renta': nac1 ,  'social': 0, 'mental': 0,
                                      'pensio': cob1, 'nous': nouvinguts, 'nousi': nouvingutsimm, 'up': up, '9C': 0, '9R': 0, '9D': 0,
                                      '9T': 0, '9E': 0, 'C': 0, 'D': 0,  'ESC': 0}
    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        for id, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash

    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]

    def get_patsocial(self):
        """Pacients amb patologia social a final del període."""
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio \
                where  (pr_cod_ps like 'Z60%' or pr_cod_ps like 'Z61%' and pr_cod_ps like 'Z62%' and pr_cod_ps like 'Z63%') \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)",
               "import")
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['social'] = 1
    
    def get_patmental(self):
        """Pacients amb patologia mental a final del període."""
        pmentalciap = {}
        sql = "select codi_cim10, codi_ciap from import.cat_md_ct_cim10_ciap where codi_ciap in {}".format(pat_mental)
        for cim10, ciap in u.getAll(sql, "import"):
            pmentalciap[cim10] = True
   
        in_crit = tuple(pmentalciap)
        
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(in_crit),
               "import")
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['mental'] = 1
    
    
    def get_escolars(self):
        """
        Obtenim dades de les revisions escolars segons el que ens va dir A Juvany
        """
        self.rev_escolars = {}
        sql = "select id_cip_sec, date_format(ro_data,'%Y%m%d') from odn510 where ro_lloc='E'"
        for id, dat in u.getAll(sql, "import"):
            self.rev_escolars[(id, dat)] = True
        
    
    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei ODN.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        """
        sql = ("select id_cip_sec, visi_tipus_visita, \
                       visi_lloc_visita, date_format(visi_data_visita,'%Y%m%d'), \
                       if(visi_modul_codi_modul in {} or visi_etiqueta in {}, 1, 0) \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                      s_espe_codi_especialitat in ('10106','10777')".format(moduls, etiquetes), "import")
        for id, tipus, lloc, dat, escolar in u.getAll(*sql):
            if  self.dext1a <= dat <= self.dext:
                if id in self.dades:
                    escolar = 1 if (id, dat) in self.rev_escolars else escolar
                    if escolar == 1:
                        self.dades[id]['ESC'] += 1
                    else:
                        if tipus in ('9C', '9D', '9T', '9R', '9E'):
                            self.dades[id][tipus] += 1
                        elif lloc in ('C', 'D'):
                            self.dades[id][lloc] += 1

                   
    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules."""
        table = 'SISAP_MODEL_ODN_PEDIA_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "permanent"
        u.createTable(table, columns_str, db, rm=True)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        dades = [(d['hash'], d['edat'], d['sex'], d['gma_cod'], d['gma_cmplx'],
                  d['gma_num'], d['medea'], d['insti'], d['renta'],  d['social'], d['mental'],
                  d['pensio'], d['nous'], d['nousi'], d['up'],  
                  d['9C'], d['9R'],d['9D'], d['9T'], d['9E'], d['C'], d['D'], d['ESC'] )
                 for id, d in self.dades.items()]
        columns = ('id varchar(40)', 'edat int', 'sexe varchar(1)',
                   'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'immigracio_baixa_renta int',
                   'patologia_social int', 'patologia_mental int','pensionista int', 'nouvinguts int', 'nouvinguts_imm int', 'up varchar(5)',
                    'visites_capes_9c int','visites_capes_9r int',
                   'visites_capes_9d int', 'visites_capes_9t int',
                    'visites_capes_9e int', 'visites_sequencials_centre int',
                   'visites_sequencials_domicili int', 'revisions_escolars int')
        Frequentacio.export_dades("PACIENT", columns, dades)

if __name__ == '__main__':
    Frequentacio()
