drop table eqaIndicadors;
create table eqaIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
);

commit;

ALTER TABLE eqaIndicadors ADD (
  CONSTRAINT eqaIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table eqaCataleg;
create table eqaCataleg (
  dataAny number
  ,indicador varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(10)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,rsomin number
  ,rsomax number
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE eqaCataleg ADD (
  CONSTRAINT eqaCatalegPK PRIMARY KEY (dataAny,indicador,pantalla));
  
commit;

drop table eqaCatalegPare;
create table eqaCatalegPare (
  dataAny number
  ,pare varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80),
  ,proces varchar2(50)
);

commit;

ALTER TABLE eqaCatalegPare ADD (
  CONSTRAINT eqaCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table eqaCatalegPunts;
create table eqaCatalegPunts (
  dataAny number
  ,indicador varchar2(10)
  ,tipus varchar2(1)
  ,punts number
);

commit;

ALTER TABLE eqaCatalegPunts ADD (
  CONSTRAINT eqaCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table eqaCatalegProcessos;
create table eqaCatalegProcessos (
  dataAny number
  ,codi varchar2(10)
  ,descripcio varchar2(255)
);

commit;

ALTER TABLE eqaCatalegProcessos ADD (
  CONSTRAINT eqaCatalegProcessosPK PRIMARY KEY (dataAny,codi,descripcio));
  
commit;

drop table eqaLlistats;
create table eqaLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(10)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
  ,valor varchar2(255)
);

commit;

ALTER TABLE eqaLlistats ADD (
  CONSTRAINT eqaLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table eqaLlistatsData;
create table eqaLlistatsData (
  dat date
);

commit;

drop table eqaCatalegExclosos;
create table eqaCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
  ,valor int
);

commit;

ALTER TABLE eqaCatalegExclosos ADD (
  CONSTRAINT eqaCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table eqaIndicadorsICS;
create table eqaIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(10)
 ,resolucio number
);

commit;

ALTER TABLE eqaIndicadorsICS ADD (
  CONSTRAINT eqaIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table eqaCatalegDefinicions;
create table eqaCatalegDefinicions (
  indicador varchar2(10)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE eqaCatalegDefinicions ADD (
  CONSTRAINT eqaCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table eqaIndicadorsAgregats;
create table eqaIndicadorsAgregats (
 up varchar2(5)
 ,indicador varchar2(10)
 ,eap number
 ,sap number
 ,ambit number
 ,ics number
 ,medea number
);

commit;

ALTER TABLE eqaIndicadorsAgregats ADD (
  CONSTRAINT eqaIndicadorsAgregatsPK PRIMARY KEY (up,indicador));
  
commit;

drop table eqaSinteticMetes;
create table eqaSinteticMetes (
 dataAny number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,basalAny number
 ,basalMes number
 ,basalValorOrig number
 ,mminOrig number
 ,mintOrig number
 ,mmaxOrig number
 ,dataModi date
 ,basalValor number
 ,mmin number
 ,mint number
 ,mmax number
);

commit;

ALTER TABLE eqaSinteticMetes ADD (
  CONSTRAINT eqaSinteticMetesPK PRIMARY KEY (dataAny,eqa,up,uab,tipus));
  
commit;
 
drop table eqaSintetic;
create table eqaSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE eqaSintetic ADD (
  CONSTRAINT eqaSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;

drop table eqaIndividualitzat;
create table eqaIndividualitzat (
 dataAny number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,punts number
);

commit;

ALTER TABLE eqaIndividualitzat ADD (
  CONSTRAINT eqaIndividualitzatPK PRIMARY KEY (dataAny,up,uab,tipus,indicador));
  
commit;
