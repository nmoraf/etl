drop table assIndicadors;
create table assIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
);

commit;

ALTER TABLE assIndicadors ADD (
  CONSTRAINT assIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table assCataleg;
create table assCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(10)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE assCataleg ADD (
  CONSTRAINT assCatalegPK PRIMARY KEY (dataAny,indicador,pantalla));
  
commit;

drop table assCatalegPare;
create table assCatalegPare (
  dataAny number
  ,pare varchar2(10)
  ,literal varchar2(80)
  ,ordre number,
  ,proces varchar2(50)
);

commit;

ALTER TABLE assCatalegPare ADD (
  CONSTRAINT assCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table assCatalegPunts;
create table assCatalegPunts (
  dataAny number
  ,indicador varchar2(8)
  ,tipus varchar2(1)
  ,punts number
);

commit;

ALTER TABLE assCatalegPunts ADD (
  CONSTRAINT assCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table assLlistats;
create table assLlistats (
  up varchar2(5)
  ,uab varchar2(10)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;


ALTER TABLE assLlistats ADD (
  CONSTRAINT assLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac,sector));
 
commit;

drop table assLlistatsData;
create table assLlistatsData (
  dat date
);

commit;

drop table assCatalegExclosos;
create table assCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
);

commit;

ALTER TABLE assCatalegExclosos ADD (
  CONSTRAINT assCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table assIndicadorsICS;
create table assIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(8)
 ,resolucio number
);

commit;

ALTER TABLE assIndicadorsICS ADD (
  CONSTRAINT assIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table assCatalegDefinicions;
create table assCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE assCatalegDefinicions ADD (
  CONSTRAINT assCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table assIndicadorsAgregats;
create table assIndicadorsAgregats (
 up varchar2(5)
 ,indicador varchar2(8)
 ,eap number
 ,sap number
 ,ambit number
 ,ics number
 ,medea number
);

commit;

ALTER TABLE assIndicadorsAgregats ADD (
  CONSTRAINT assIndicadorsAgregatsPK PRIMARY KEY (up,indicador));
  
commit;


drop table assSinteticMetes;
create table assSinteticMetes (
 dataAny number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,basalAny number
 ,basalMes number
 ,basalValorOrig number
 ,mminOrig number
 ,mintOrig number
 ,mmaxOrig number
 ,dataModi date
 ,basalValor number
 ,mmin number
 ,mint number
 ,mmax number
);

commit;

ALTER TABLE assSinteticMetes ADD (
  CONSTRAINT assSinteticMetesPK PRIMARY KEY (dataAny,eqa,up,uab,tipus));
  
commit;
 
drop table assSintetic;
create table assSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE assSintetic ADD (
  CONSTRAINT assSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;


drop table assIndividualitzat;
create table assIndividualitzat (
 dataAny number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,punts number
);

commit;

ALTER TABLE assIndividualitzat ADD (
  CONSTRAINT assIndividualitzatPK PRIMARY KEY (dataAny,up,uab,tipus,indicador));
  
commit;
