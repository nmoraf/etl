drop table pedIndicadors;
create table pedIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
);

commit;

ALTER TABLE pedIndicadors ADD (
  CONSTRAINT pedIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table pedCataleg;
create table pedCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(10)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,rsomin number
  ,rsomax number
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE pedCataleg ADD (
  CONSTRAINT pedCatalegPK PRIMARY KEY (dataAny,indicador,pantalla));
  
commit;

drop table pedCatalegPare;
create table pedCatalegPare (
  dataAny number
  ,pare varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80),
  ,proces varchar2(50)
);

commit;

ALTER TABLE pedCatalegPare ADD (
  CONSTRAINT pedCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table pedCatalegPunts;
create table pedCatalegPunts (
  dataAny number
  ,indicador varchar2(8)
  ,tipus varchar2(1)
  ,punts number
);

commit;

ALTER TABLE pedCatalegPunts ADD (
  CONSTRAINT pedCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table pedLlistats;
create table pedLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;


ALTER TABLE pedLlistats ADD (
  CONSTRAINT pedLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table pedLlistatsData;
create table pedLlistatsData (
  dat date
);

commit;

drop table pedCatalegExclosos;
create table pedCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
);

commit;

ALTER TABLE pedCatalegExclosos ADD (
  CONSTRAINT pedCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table pedIndicadorsICS;
create table pedIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(8)
 ,resolucio number
);

commit;

ALTER TABLE pedIndicadorsICS ADD (
  CONSTRAINT pedIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table pedCatalegDefinicions;
create table pedCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE pedCatalegDefinicions ADD (
  CONSTRAINT pedCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table pedIndicadorsAgregats;
create table pedIndicadorsAgregats (
 up varchar2(5)
 ,indicador varchar2(8)
 ,eap number
 ,sap number
 ,ambit number
 ,ics number
 ,medea number
);

commit;

ALTER TABLE pedIndicadorsAgregats ADD (
  CONSTRAINT pedIndicadorsAgregatsPK PRIMARY KEY (up,indicador));
  
commit;


drop table pedSinteticMetes;
create table pedSinteticMetes (
 dataAny number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,basalAny number
 ,basalMes number
 ,basalValorOrig number
 ,mminOrig number
 ,mintOrig number
 ,mmaxOrig number
 ,dataModi date
 ,basalValor number
 ,mmin number
 ,mint number
 ,mmax number
);

commit;

ALTER TABLE pedSinteticMetes ADD (
  CONSTRAINT pedSinteticMetesPK PRIMARY KEY (dataAny,eqa,up,uab,tipus));
  
commit;
 
drop table pedSintetic;
create table pedSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE pedSintetic ADD (
  CONSTRAINT pedSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;



drop table pedIndividualitzat;
create table pedIndividualitzat (
 dataAny number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,punts number
);

commit;

ALTER TABLE pedIndividualitzat ADD (
  CONSTRAINT pedIndividualitzatPK PRIMARY KEY (dataAny,up,uab,tipus,indicador));
  
commit;