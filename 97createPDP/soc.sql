drop table socIndicadors;
create table socIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
);

commit;

ALTER TABLE socIndicadors ADD (
  CONSTRAINT socIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table socCataleg;
create table socCataleg (
   dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(10)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE socCataleg ADD (
  CONSTRAINT socCatalegPK PRIMARY KEY (dataAny,indicador,pantalla));
  
commit;

drop table socCatalegPare;
create table socCatalegPare (
  dataAny number
  ,pare varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80),
  ,proces varchar2(50)
);

commit;

ALTER TABLE socCatalegPare ADD (
  CONSTRAINT socCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table socCatalegPunts;
create table socCatalegPunts (
  dataAny number
  ,indicador varchar2(8)
  ,tipus varchar2(1)
  ,punts number
);

commit;

ALTER TABLE socCatalegPunts ADD (
  CONSTRAINT socCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table socLlistats;
create table socLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;


ALTER TABLE socLlistats ADD (
  CONSTRAINT socLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table socLlistatsData;
create table socLlistatsData (
  dat date
);

commit;

drop table socCatalegExclosos;
create table socCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
);

commit;

ALTER TABLE socCatalegExclosos ADD (
  CONSTRAINT socCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table socIndicadorsICS;
create table socIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(8)
 ,resolucio number
);

commit;

ALTER TABLE socIndicadorsICS ADD (
  CONSTRAINT socIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table socCatalegDefinicions;
create table socCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE socCatalegDefinicions ADD (
  CONSTRAINT socCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table socIndicadorsAgregats;
create table socIndicadorsAgregats (
 up varchar2(5)
 ,indicador varchar2(8)
 ,eap number
 ,sap number
 ,ambit number
 ,ics number
 ,medea number
);

commit;

ALTER TABLE socIndicadorsAgregats ADD (
  CONSTRAINT socIndicadorsAgregatsPK PRIMARY KEY (up,indicador));
  
commit;


drop table socSinteticMetes;
create table socSinteticMetes (
 dataAny number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,basalAny number
 ,basalMes number
 ,basalValorOrig number
 ,mminOrig number
 ,mintOrig number
 ,mmaxOrig number
 ,dataModi date
 ,basalValor number
 ,mmin number
 ,mint number
 ,mmax number
);

commit;

ALTER TABLE socSinteticMetes ADD (
  CONSTRAINT socSinteticMetesPK PRIMARY KEY (dataAny,eqa,up,uab,tipus));
  
commit;
 
drop table socSintetic;
create table socSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE socSintetic ADD (
  CONSTRAINT socSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;


drop table socIndividualitzat;
create table socIndividualitzat (
 dataAny number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,punts number
);

commit;

ALTER TABLE socIndividualitzat ADD (
  CONSTRAINT socIndividualitzatPK PRIMARY KEY (dataAny,up,uab,tipus,indicador));
  
commit;

drop table socProfessionals;
create table socProfessionals (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
);

commit;

ALTER TABLE socProfessionals ADD (
  CONSTRAINT socProfessionalsPK PRIMARY KEY (dataAny,dataMes,up, uab));
  
commit;
