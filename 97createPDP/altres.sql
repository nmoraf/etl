drop table altIndicadors;
create table altIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE altIndicadors ADD (
  CONSTRAINT altIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
drop table altCataleg;
create table altCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar2(8)
  ,llistat number
  ,invers number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
);

commit;

ALTER TABLE altCataleg ADD (
  CONSTRAINT altCatalegPK PRIMARY KEY (dataAny,indicador));


commit;

drop table altCatalegPare;
create table altCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,PANTALLA VARCHAR2(20)
);

commit;

ALTER TABLE altCatalegPare ADD (
  CONSTRAINT altCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table altLlistats;
create table altLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;

ALTER TABLE altLlistats ADD (
  CONSTRAINT altLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table altLlistatsData;
create table altLlistatsData (
  dat date
);

commit;

drop table altCatalegExclosos;
create table altCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
);

commit;

ALTER TABLE altCatalegExclosos ADD (
  CONSTRAINT altCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table altIndicadorsICS;
create table altIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(8)
 ,resolucio number
);

commit;

ALTER TABLE altIndicadorsICS ADD (
  CONSTRAINT altIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table altCatalegDefinicions;
create table altCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE altCatalegDefinicions ADD (
  CONSTRAINT altCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;