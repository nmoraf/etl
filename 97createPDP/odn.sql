drop table odnIndicadors;
create table odnIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
);

commit;

ALTER TABLE odnIndicadors ADD (
  CONSTRAINT odnIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table odnCataleg;
create table odnCataleg (
  dataAny number  
  ,indicador varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(10)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE odnCataleg ADD (
  CONSTRAINT odnCatalegPK PRIMARY KEY (dataAny,indicador,pantalla));
  
commit;

drop table odnCatalegPare;
create table odnCatalegPare (
   dataAny number  
  ,pare varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80),
  ,proces varchar2(50)
);

commit;

ALTER TABLE odnCatalegPare ADD (
  CONSTRAINT odnCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table odnCatalegPunts;
create table odnCatalegPunts (
   dataAny number  
  ,indicador varchar2(8)
  ,tipus varchar2(1)
  ,punts number
);

commit;

ALTER TABLE odnCatalegPunts ADD (
  CONSTRAINT odnCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table odnLlistats;
create table odnLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;


ALTER TABLE odnLlistats ADD (
  CONSTRAINT odnLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table odnLlistatsData;
create table odnLlistatsData (
  dat date
);

commit;

drop table odnCatalegExclosos;
create table odnCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
);

commit;

ALTER TABLE odnCatalegExclosos ADD (
  CONSTRAINT odnCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table odnIndicadorsICS;
create table odnIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(8)
 ,resolucio number
);

commit;

ALTER TABLE odnIndicadorsICS ADD (
  CONSTRAINT odnIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table odnCatalegDefinicions;
create table odnCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE odnCatalegDefinicions ADD (
  CONSTRAINT odnCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table odnIndicadorsAgregats;
create table odnIndicadorsAgregats (
 up varchar2(5)
 ,indicador varchar2(8)
 ,eap number
 ,sap number
 ,ambit number
 ,ics number
 ,medea number
);

commit;

ALTER TABLE odnIndicadorsAgregats ADD (
  CONSTRAINT odnIndicadorsAgregatsPK PRIMARY KEY (up,indicador));
  
commit;


drop table odnSinteticMetes;
create table odnSinteticMetes (
 dataAny number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,basalAny number
 ,basalMes number
 ,basalValorOrig number
 ,mminOrig number
 ,mintOrig number
 ,mmaxOrig number
 ,dataModi date
 ,basalValor number
 ,mmin number
 ,mint number
 ,mmax number
);

commit;

ALTER TABLE odnSinteticMetes ADD (
  CONSTRAINT odnSinteticMetesPK PRIMARY KEY (dataAny,eqa,up,uab,tipus));
  
commit;
 
drop table odnSintetic;
create table odnSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE odnSintetic ADD (
  CONSTRAINT odnSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;


drop table odnIndividualitzat;
create table odnIndividualitzat (
 dataAny number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,punts number
);

commit;

ALTER TABLE odnIndividualitzat ADD (
  CONSTRAINT odnIndividualitzatPK PRIMARY KEY (dataAny,up,uab,tipus,indicador));
  
commit;


drop table odnProfessionals;
create table odnProfessionals (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
);

commit;

ALTER TABLE odnProfessionals ADD (
  CONSTRAINT odnProfessionalsPK PRIMARY KEY (dataAny,dataMes,up, uab));
  
commit;