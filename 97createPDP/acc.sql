drop table accIndicadors;
create table accIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE accIndicadors ADD (
  CONSTRAINT accIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));


 
drop table accCataleg;
create table accCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar2(8)
  ,llistat number
  ,invers number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
);

commit;

ALTER TABLE accCataleg ADD (
  CONSTRAINT accCatalegPK PRIMARY KEY (dataAny,indicador));


commit;

drop table accCatalegPare;
create table accCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,PANTALLA VARCHAR2(20)
);

commit;

ALTER TABLE accCatalegPare ADD (
  CONSTRAINT accCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;
