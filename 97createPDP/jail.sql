drop table prsIndicadors;
create table prsIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,indicador varchar2(8)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE prsIndicadors ADD (
  CONSTRAINT prsIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,indicador));
  
drop table prsCataleg;
create table prsCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar2(8)
  ,llistat number
  ,invers number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,tipus varchar2(2)
  ,ncol number
);

commit;

ALTER TABLE prsCataleg ADD (
  CONSTRAINT prsCatalegPK PRIMARY KEY (dataAny,indicador));


commit;

drop table prsCatalegPare;
create table prsCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,subtotal number
);

commit;

ALTER TABLE prsCatalegPare ADD (
  CONSTRAINT prsCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table prsLlistats;
create table prsLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;

ALTER TABLE prsLlistats ADD (
  CONSTRAINT prsLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table prsLlistatsData;
create table prsLlistatsData (
  dat date
);

commit;

drop table prsCatalegDefinicions;
create table prsCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE prsCatalegDefinicions ADD (
  CONSTRAINT prsCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table prsCatalegMetes;
create table prsCatalegMetes (
  dataAny number
  ,indicador varchar2(8)
  ,up varchar2(5)
  ,tipus varchar2(2)
  ,mmin number
  ,mmax number
);

commit;

ALTER TABLE prsCatalegMetes ADD (
  CONSTRAINT prsCatalegMetesPK PRIMARY KEY (dataAny,indicador,up,tipus));

commit;

drop table prseqaIndicadors;
create table prseqaIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
);

commit;

ALTER TABLE prseqaIndicadors ADD (
  CONSTRAINT prseqaIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
drop table prseqaLlistats;
create table prseqaLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(10)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;

ALTER TABLE prseqaLlistats ADD (
  CONSTRAINT prseqaLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table prseqaCatalegPunts;
create table prseqaCatalegPunts (
  dataAny number
  ,indicador varchar2(10)
  ,tipus varchar2(1)
  ,punts number
);

commit;

ALTER TABLE prseqaCatalegPunts ADD (
  CONSTRAINT prseqaCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table prseqaSintetic;
create table prseqaSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE prseqaSintetic ADD (
  CONSTRAINT prseqaSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;