
/* Modificación del script original para su ejecución en el proceso del SISAP */
/*
/* Modificaciones a hacer
/* - Guardar las tablas antes de que las borre el SISAP, ... 
/* - Establecer path relativo del archivo txt de professionales
/* - Revisar que la fecha ahora nom es 01-01-2015
/*
/*
/*
/*
/*

*/





use sidiapProjects;

/*  FACTURACION:
-- facturación no esta en el sisap, hay que traerlo del mariaDB de sidiap,
-- la forma mas sencilla que he encontrado es crear en el mariaDB-sidiap la tabla de facturación con los ISRS
-- y mover fisicamente los ficheros a la bbdd sidiapprojects del sisap

drop table if exists gpcdep.facturacio;
create table gpcdep.facturacio as
select * from imported_data.facturacio where pf_cod_atc like 'N06AB%';

-- ojo que el identificador de SISAP i SIDIAP no coincidiran si el SIDIAP se queda congelado, esta parte hay que tirarla a mano.

drop table if exists gpcdep_gpcdep6_pre1;
create table gpcdep_gpcdep6_pre1 as
select
	a.*, 0 as risc, 0 as control --, date '0000-00-00' presc_ini, date '0000-00-00' as presc_fi, date '0000-00-00' as anyrec
from gpcdep_gpcdep5_pre1 a, nodrizas.dextraccio
where
 pr_dde between date_add(data_ext, interval -15 month) and date_add(data_ext, interval -4 month)
 and (pr_dba is null or pr_dba > date_add(pr_dde, interval 3 month))
;

alter table gpcdep_gpcdep6_pre1 add index (id_cip);

update gpcdep_gpcdep6_pre1 b inner join gpcdep_import_tractaments a on  b.id_cip=a.id_cip
  inner join
    nodrizas.dextraccio
set b.risc=1 --, b.presc_ini=ppfmc_pmc_data_ini, b.presc_fi=ppfmc_data_fi
where (ppfmc_pmc_data_ini<=data_ext and (a.ppfmc_data_fi is null or a.ppfmc_data_fi > date_add(pr_dde,interval 3 month)))
;


update
gpcdep_gpcdep6_pre1 b inner join facturacio a on b.id_cip=a.id_cip
set control = 1, b.anyrec = date_add(date_add(str_to_date(concat(rec_any_mes,'01'),'%Y%m%d'), interval 1 month), interval -1 day)
where
	date_add(date_add(str_to_date(concat(rec_any_mes,'01'),'%Y%m%d'), interval 1 month), interval -1 day) > date_add(pr_dde, interval 3 month)
;

drop table if exists gpcdep_gpcdep6;
create table gpcdep_gpcdep6 as
select
	'201507' as periodo
	,'GPCDEP-6' as ind
	,up
	,uba
	,'M' as tipus
	,sum(control) as num
	,count(control) as den
from
	gpcdep_gpcdep6_pre1
where
	risc=1
group by up, uba
union all
select
	'201507' as periodo
	,'GPCDEP-6' as ind
	,upinf as up
	,ubainf as uba
	,'I' as tipus
	,sum(control) as num
	,count(control) as den
from
	gpcdep_gpcdep6_pre1
where
	risc=1
group by upinf, ubainf
;


set @anymes=(select concat(periode_ext,'_gpcdep-6') from gpcdep_pextraccio);

set @sqlM = concat ("
select periode_ext as periode, p.up, p.cognom1, p.COGNOM2, p.NOM, ind, num, den from gpcdep_gpcdep6 b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.consentiment=1 and p.tipus='M'
order by up, cognom1, cognom2, nom, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_",@anymes,"_professionalsM.txt'
fields terminated by '|'
lines terminated by '\r\n'
");

set @sqlI = concat ("
select periode_ext as periode, p.up, p.cognom1, p.COGNOM2, p.NOM, ind, num, den from gpcdep_gpcdep6 b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.consentiment=1 and p.tipus='I'
order by up, cognom1, cognom2, nom, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_",@anymes,"_professionalsI.txt'
fields terminated by '|'
lines terminated by '\r\n'
");

set @sqlC = concat ("
select periode_ext as periode, p.up, ind, sum(num) as num, sum(den) as den from gpcdep_gpcdep6 b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.tipus='M'
group by up, ind
order by up, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_",@anymes,"_centres.txt'
fields terminated by '|'
lines terminated by '\r\n'
");


PREPARE s1 FROM @sqlM;
EXECUTE s1;
DROP PREPARE s1;

PREPARE s2 FROM @sqlI;
EXECUTE s2;
DROP PREPARE s2;

PREPARE s3 FROM @sqlC;
EXECUTE s3;
DROP PREPARE s3;


*/


/* Fecha de extracción
*******************************************************************************************************************************************/
-- Se utiliza la fecha de extracción que utiliza el SISAP (nodrizas.dextraccio)

/******************************************************************************************************************************************/


/* Periodo de EXTRACCION
*******************************************************************************************************************************************/

drop table if exists gpcdep_pextraccio;
create table gpcdep_pextraccio (periode_ext varchar(10))
;
insert into gpcdep_pextraccio (periode_ext) select date_format(data_ext,'%Y%m') as periode from nodrizas.dextraccio;
/******************************************************************************************************************************************/





/* PROFESSIONALS 
*******************************************************************************************************************************************/

# gpcdep_import_professionals
# 
#(esta query se tira en PDP(redics) y se hace sobre la query boton derecho y 'export from query' a la tabla gpcdep_import_professionals)
/*
select
concat(concat(concat(up,cognom1),' '),cognom2),
up, uab, ide_numcol, tipus, nom, cognom1, cognom2
from
  professionals
where
  tipus='M'
  and up in (
'00360'
,'00342'    
,'00357'    
,'00358'
,'00359'
,'00338'    
,'00356'
,'00389' 
,'00390'    
,'06189'   
,'02038'
,'00361'   

,'00388'
)
order by up, cognom1
;
*/
-- drop table if exists gpcdep_professionals;
-- create table gpcdep_professionals as select a.*, 0 as visitat, 0 as consentiment from gpcdep_import_professionals a
-- ;
-- select * from gpcdep_professionals;



-- Creo un archivo txt que se pueda importar en cada ejecucuión. Utilizo la tabla gpcdep_professionals de la bbdd leo a fecha 25/02/2015

-- Imporatción del archivo txt
drop table if exists gpcdep_professionals;
create table gpcdep_professionals (
  id varchar(211)
  , up	varchar(10)
  , uab	varchar(10)
  , ide_numcol varchar(20)
  , tipus varchar(10)
  , nom	varchar(100)
  , cognom1 varchar(100)
  , cognom2 varchar(100)
  , visitat	int(1)
  , consentiment int(1)
);
load data infile
  'P:/sisap/13sidiap/dades_noesb/gpcdep_professionals.txt'  
into table
  gpcdep_professionals
fields terminated by '|'
lines terminated by '\r\n'
;
/******************************************************************************************************************************************/



/* POBLACIO - Asignada
*******************************************************************************************************************************************/

-- select * from nodrizas.cat_centres where scs_codi in ('00360'
#,'00342'    
#,'00357'    
#,'00358'
#,'00359'
#,'00338'    
#,'00356'
#,'00389' 
#,'00390'    
#,'06189'   
#,'02038'
#,'00361'   
#,'00388');

drop table if exists gpcdep_assignada_tot;
create table gpcdep_assignada_tot as
select * from nodrizas.assignada_tot where up in (
'00360'
,'00342'    
,'00357'    
,'00358'
,'00359'
,'00338'    
,'00356'
,'00389' 
,'00390'    
,'06189'   
,'02038'
,'00361'   
,'00388'
)
;
-- select * from gpcdep_assignada_tot limit 10;

alter table gpcdep_assignada_tot add primary key (id_cip)
;

-- commit;
/******************************************************************************************************************************************/



/* PROBLEMES
*******************************************************************************************************************************************/

drop table if exists gpcdep_import_problemes;
create table gpcdep_import_problemes as
select
	d.*
from
	import.problemes_s6735 d inner join
	gpcdep_assignada_tot a on d.id_cip=a.id_cip
where
	pr_cod_ps in (
	'F32'
	,'F32.0'
	,'F32.1'
	,'F32.2'
	,'F32.3'
	,'F32.8'
	,'F32.9'
	,'F33'
	,'F33.0'
	,'F33.1'
	,'F33.2'
	,'F33.3'
	,'F33.4'
	,'F33.8'
	,'F33.9'
	)
and	pr_hist=1
and pr_cod_o_ps='C'
and pr_data_baixa is null
;

-- commit;


-- script de LEO par aincidentes/prevalentes
-- ----------------------------------------------------------------------------------------------

/*
-- problemas prevalente y problemas incidentes
drop table if exists gpcdep_problemes1;
create table gpcdep_problemes1(
id_cip double null,
ps varchar(5),
-- ps_desc varchar(150) not null default'',
dde DATE NOT NULL DEFAULT 0,
incident double
-- pr_gra VARCHAR(5) NOT NULL DEFAULT '',
-- inclusio double
)
select
	id_cip
	, 'depre' as ps
-- ,agrupador_desc as ps_desc
	, pr_dde as dde
	, if(pr_dde > date_add(data_ext,interval - 1 month),1,0) as incident
-- ,pr_gra
-- ,inclusio
from
	gpcdep_import_problemes a
-- inner join
-- eqa_criteris_problemes b
-- on
-- pr_cod_ps=b.criteri_codi
	, nodrizas.dextraccio
where
	pr_cod_o_ps='C'
	and pr_dde <= data_ext
	and (pr_dba is null or pr_dba > data_ext)
	and (pr_data_baixa is null or pr_data_baixa > data_ext)
	and pr_hist=1
-- and ps_tancat=0
;

drop table if exists gpcdep_problemes1tancats;
create table gpcdep_problemes1tancats(
id_cip double null,
ps varchar(5),
-- ps_desc varchar(150) not null default'',
dde DATE NOT NULL DEFAULT 0,
incident double
-- pr_gra VARCHAR(5) NOT NULL DEFAULT '',
-- inclusio double
)
select
	id_cip
	, 'depre' as ps
	-- ,agrupador_desc as ps_desc
	, pr_dde as dde
	, if(pr_dde > date_add(data_ext,interval -1 month),1,0) as incident
-- ,pr_gra
-- ,inclusio
from
	gpcdep_import_problemes a
-- inner join
-- eqa_criteris_problemes b
-- on
-- if(pr_th in (5780,5781),'E78.0a',if(pr_th=2485,'N39.0a',pr_cod_ps))=b.criteri_codi
	, nodrizas.dextraccio
where
	pr_cod_o_ps='C'
	and pr_dde <= data_ext
	and (pr_dba>0 and pr_dba<=data_ext)
	and (pr_data_baixa is null or pr_data_baixa > data_ext)
	and pr_hist=1
-- and ps_tancat=1
;

insert into gpcdep_problemes1
select * from gpcdep_problemes1tancats
;

create index index_ps on gpcdep_problemes1 (id_cip);

drop table gpcdep_problemes1tancats;

drop table if exists gpcdep_problemes;
CREATE TABLE gpcdep_problemes (
id_cip double null,
ps varchar(5),
-- ps_desc varchar(150) not null default'',
dde DATE NOT NULL DEFAULT 0,
-- pr_gra VARCHAR(5) NOT NULL DEFAULT '',
-- inclusio double,
incident double null,
N double null,
Ninc double null,
up VARCHAR(5) NOT NULL DEFAULT '',
uba VARCHAR(10) NOT NULL DEFAULT '',
centre_codi varchar(15) not null default '',
centre_classe varchar(2) not null default '',
upinf VARCHAR(5) NOT NULL DEFAULT '',
ubainf VARCHAR(10) NOT NULL DEFAULT '',
-- centre_codi_inf varchar(15) not null default '',
-- centre_classe_inf varchar(2) not null default '',
-- grup double NULL,
edat double NULL,
data_naix DATE NOT NULL DEFAULT 0,
primary key(id_cip,ps),
index(id_cip),
index(ps),
index(dde)
)
select
	a.id_cip
	,ps
-- ,ps_desc
	,max(dde) as dde
-- ,max(pr_gra) pr_gra
-- ,inclusio
	,max(incident) as incident
	,count(*) as N
	,sum(incident) as Ninc
	,up
	,uba
	,centre_codi
	,centre_classe
	,upinf
	,ubainf
	-- ,centre_codi_inf
	-- ,centre_classe_inf
	-- ,grup
	,edat
	,data_naix
from
	gpcdep_problemes1 a
inner join
	gpcdep_assignada_tot b
on
	a.id_cip=b.id_cip
	and b.ates=1
group by
	id_cip,ps
;

drop table if exists gpcdep_problemes_incid;
CREATE TABLE gpcdep_problemes_incid (
id_cip double null,
ps varchar(5),
-- ps_desc varchar(150) not null default'',
dde DATE NOT NULL DEFAULT 0,
-- pr_gra VARCHAR(5) NOT NULL DEFAULT '',
index(id_cip),
index(ps),
index(dde)
)
select
	a.id_cip
	,ps
--	,ps_desc
	,dde
--	,pr_gra
from
	gpcdep_problemes1 a
inner join
	gpcdep_assignada_tot b
on
	a.id_cip=b.id_cip
	and b.ates=1
;
*/


/* Problemes seleccionats F32.1 F32.2,F32.3   F33.1, F33.2, F33.3 */

drop table if exists gpcdep_import_problemes_seleccionats;
create table gpcdep_import_problemes_seleccionats as
select * from gpcdep_import_problemes where pr_cod_ps in ('F32.1','F32.2','F32.3','F33.1','F33.2','F33.3')
;
/******************************************************************************************************************************************/










-- VARIABLES
-- -------------------------------------------------------------------------------------------------------------------------

   -- PHQ-9 ---------------------------------------------------------------------------------- (execution time: 3 minuts)
      drop table if exists gpcdep_phq9;
      create table gpcdep_phq9
      select a.* from import.variables a inner join gpcdep_assignada_tot b on a.id_cip=b.id_cip where vu_cod_vs='VP5403';
      

   -- MINI-Interview Riesgo DEPRESION ---------------------------------------------------------(execution time: 3 minuts)
      drop table if exists gpcdep_mini_dep;
      create table gpcdep_mini_dep as 
      select a.* from import.variables a inner join gpcdep_assignada_tot b on a.id_cip=b.id_cip where vu_cod_vs='VP5401';

   -- Escala d’avaluació de l’activitat global (EAAG) -----------------------------------------(execution time: 3 minuts)
      drop table if exists gpcdep_eaag;
      create table gpcdep_eaag as 
      select a.* from import.variables a inner join gpcdep_assignada_tot b on a.id_cip=b.id_cip where vu_cod_vs='VP5404';
      
   -- MINI-Interview Riesgo SUICIDIO) ---------------------------------------------------------(execution time: 3 minuts)
      drop table if exists gpcdep_mini_suc;
      create table gpcdep_mini_suc as 
      select a.* from import.variables a inner join gpcdep_assignada_tot b on a.id_cip=b.id_cip where vu_cod_vs='VP5402';   
-- --------------------------------------------------------------------------------------------------------------------------

/******************************************************************************************************************************************/



/* TRACTAMENT
*******************************************************************************************************************************************/
-- select * from import.tractaments_s6735 where pf_cod_atc like 'N06AB%';

drop table if exists gpcdep_import_tractaments;
create table gpcdep_import_tractaments as
select
	d.*
from
	import.tractaments_s6735 d inner join
	gpcdep_assignada_tot a on d.id_cip=a.id_cip
where
	ppfmc_atccodi like 'N06AB%'
;


































/******************************************************************************************************************************************/





















-- CONSTRUCCIÓ INDICADORS
-- -----------------------------------------------------------------------------------------------------------------------

  -- DENOMINADOR
     drop table if exists gpcdep_den;
     create table gpcdep_den
     select up, uba, 'M' as tipus, count(id_cip) as den
     from
       gpcdep_assignada_tot
     where
       ates=1
     group by
       up, uba
     union all
	 select upinf as up, ubainf as uba, 'I' as tipus, count(id_cip) as den
     from
       gpcdep_assignada_tot
     where
       ates=1
     group by
       upinf, ubainf;
    -- select * from gpcdep_gen2015_den;


  -- INDICADOR 1A
  -- ------------------------------------------------------------------------------------------------
    drop table if exists gpcdep_gpcdep1a_pre1;
    create table gpcdep_gpcdep1a_pre1
    select up, uba, upinf, ubainf, a.id_cip, a.pr_dde, a.pr_dba
    from
      gpcdep_import_problemes a
    inner join      
      nodrizas.dextraccio
    right join
      gpcdep_assignada_tot b
    on
      a.id_cip=b.id_cip
    where   
      ates=1
      and ((pr_dde<=data_ext and pr_dba is null) or (pr_dde<=data_ext and pr_dba>data_ext))  
    ;
    -- select * from gpcdep_gen2015_gpcdep1a_pre1;

    drop table if exists gpcdep_gpcdep1a_pre2;
    create table gpcdep_gpcdep1a_pre2
    select periode_ext as periodo,'GPCDEP-1a' as ind, up, uba, 'M' as tipus, count(distinct(id_cip)) as num
    from
      gpcdep_gpcdep1a_pre1
      , gpcdep_pextraccio
    group by
      up, uba
	union all
	 select periode_ext as periodo,'GPCDEP-1a' as ind, upinf as up, ubainf as uba, 'I' as tipus, count(distinct(id_cip)) as num
    from
      gpcdep_gpcdep1a_pre1
      , gpcdep_pextraccio
    group by
      upinf, ubainf
    ;
    
    drop table if exists gpcdep_gpcdep1a;
    create table gpcdep_gpcdep1a
    select periodo, ind, b.up, b.uba, b.tipus, a.num, b.den
    from
      gpcdep_gpcdep1a_pre2 a
    right join
      gpcdep_den b
    on
      a.up=b.up
      and a.uba=b.uba
	  and a.tipus=b.tipus
    ;

    -- select * from gpcdep_gpcdep1a;
    update gpcdep_gpcdep1a, gpcdep_pextraccio set periodo=periode_ext where periodo is null;
    update gpcdep_gpcdep1a set ind='GPCDEP-1a' where ind is null;
    update gpcdep_gpcdep1a set num=0 where num is null;
  -- ------------------------------------------------------------------------------------------------



  -- INDICADOR 1B
  -- ------------------------------------------------------------------------------------------------

    drop table if exists gpcdep_gpcdep1b_pre1;
    create table gpcdep_gpcdep1b_pre1

    select up, uba, upinf, ubainf, a.id_cip, a.pr_dde, a.pr_dba
    from
      gpcdep_import_problemes a
    inner join      
      nodrizas.dextraccio
    right join
      gpcdep_assignada_tot b
    on
      a.id_cip=b.id_cip
    where   
      ates=1
      and pr_dde between date_add(data_ext, interval -1 month) and data_ext
    ;

    drop table if exists gpcdep_gpcdep1b_pre2;
    create table gpcdep_gpcdep1b_pre2
    select periode_ext as periodo,'GPCDEP-1b' as ind, up, uba, 'M' as tipus, count(distinct(id_cip)) as num
    from
      gpcdep_gpcdep1b_pre1
    inner join      
      gpcdep_pextraccio      
    group by
      up, uba 
	union all
	select periode_ext as periodo,'GPCDEP-1b' as ind, upinf as up, ubainf as uba, 'I' as tipus, count(distinct(id_cip)) as num
    from
      gpcdep_gpcdep1b_pre1
    inner join      
      gpcdep_pextraccio      
    group by
      upinf, ubainf
    ;

    drop table if exists gpcdep_gpcdep1b;
    create table gpcdep_gpcdep1b
    select periodo, ind, b.up, b.uba, b.tipus, a.num, b.den
    from
      gpcdep_gpcdep1b_pre2 a
    right join
      gpcdep_den b
    on
      a.up=b.up
      and a.uba=b.uba
	  and a.tipus=b.tipus
    ;    
    -- select * from gpcdep_gen2015_gpcdep1a;
    update gpcdep_gpcdep1b, gpcdep_pextraccio set periodo=periode_ext where periodo is null;
    update gpcdep_gpcdep1b set ind='GPCDEP-1b' where ind is null;
    update gpcdep_gpcdep1b set num=0 where num is null;

  -- select * from gpcdep_gpcdep1b;
  -- ------------------------------------------------------------------------------------------------


  -- INDICADOR 2
  -- ------------------------------------------------------------------------------------------------

  drop table if exists gpcdep_gpcdep2_pre1;
  create table gpcdep_gpcdep2_pre1
  select * from gpcdep_mini_dep, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext
  union all
  select * from gpcdep_phq9, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext
  union all
  select * from gpcdep_mini_suc, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext
  union all
  select * from gpcdep_eaag, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext
  ;
  -- select * from gpcdep_gpcdep2_pre1;
/* 
  drop table if exists gpcdep_gpcdep2;
  create table gpcdep_gpcdep2
  select periode_ext as periodo,'GPCDEP-2', up, uba, 'M' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_gpcdep2_pre1) a
  inner join
      gpcdep_pextraccio      
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    up, uba
union all
  select periode_ext as periodo,'GPCDEP-2', upinf as up, ubainf as uba, 'I' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_gpcdep2_pre1) a
  inner join
      gpcdep_pextraccio      
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    upinf, ubainf
  ;
  */ 
  
  drop table if exists gpcdep_gpcdep2;
  create table gpcdep_gpcdep2
  select periode_ext as periodo, 'GPCDEP-2' as ind, up, uba, tipus, num, den
  from
  ( 
  select up, uba, 'M' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_gpcdep2_pre1 where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext) a
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    up, uba
union all
  select upinf as up, ubainf as uba, 'I' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
	(select * from gpcdep_gpcdep2_pre1 where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext) a
  right join
	(select * from gpcdep_assignada_tot where ates=1) b
  on
	a.id_cip=b.id_cip
  group by
	upinf, ubainf
   ) g
  inner join
      gpcdep_pextraccio
  ;
  -- ------------------------------------------------------------------------------------------------


  -- INDICADOR 3
  -- ------------------------------------------------------------------------------------------------

  drop table if exists gpcdep_gpcdep3;
  create table gpcdep_gpcdep3
  select periode_ext as periodo, 'GPCDEP-3' as ind, up, uba, tipus, num, den
  from
  ( 
  select up, uba, 'M' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_mini_dep, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext) a
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    up, uba
union all
 select upinf as up, ubainf as uba, 'I' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_mini_dep, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext) a
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    upinf, ubainf
   ) g
  inner join
      gpcdep_pextraccio          
  ;
  -- ------------------------------------------------------------------------------------------------


  -- INDICADOR 4
  -- ------------------------------------------------------------------------------------------------

  drop table if exists gpcdep_gpcdep4;
  create table gpcdep_gpcdep4
select periode_ext as periodo, 'GPCDEP-4' as ind, up, uba, tipus, num, den
  from
  ( 
  select up, uba, 'M' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_phq9, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext) a
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    up, uba
union all
select upinf as up, ubainf as uba, 'I' as tipus, count(distinct(a.id_cip)) as num, count(distinct(b.id_cip)) as den
  from
    (select * from gpcdep_phq9, nodrizas.dextraccio where vu_dat_act between date_add(data_ext, interval -1 month) and data_ext) a
  right join
    (select * from gpcdep_assignada_tot where ates=1) b
  on
    a.id_cip=b.id_cip
  group by
    upinf, ubainf
   ) g
  inner join
      gpcdep_pextraccio          
  ;  
 --  select * from gpcdep_gpcdep4;
  -- ------------------------------------------------------------------------------------------------


  -- INDICADOR 5
  -- ------------------------------------------------------------------------------------------------

  -- select * from gpcdep_import_tractaments;
  -- select * from gpcdep_import_problemes_seleccionats;

  drop table if exists gpcdep_gpcdep5_pre1;
  create table gpcdep_gpcdep5_pre1
  select
    up, uba, upinf, ubainf, b.*
  from
    gpcdep_assignada_tot a
  inner join
    gpcdep_import_problemes_seleccionats b
  on
    a.id_cip=b.id_cip
  where
    ates=1
  ;
  -- select * from gpcdep_gpcdep5_pre1;
  
  
  drop table if exists gpcdep_gpcdep5_pre2;
  create table gpcdep_gpcdep5_pre2
  select
    up, uba, upinf, ubainf, a.*
  from
    gpcdep_import_tractaments a 
  inner join
    nodrizas.dextraccio
  inner join
    gpcdep_gpcdep5_pre1 b
  on
    a.id_cip=b.id_cip
  where
    (ppfmc_pmc_data_ini<=data_ext and (a.ppfmc_data_fi>data_ext or a.ppfmc_data_fi is null))
  ;
  -- select * from gpcdep_gen2015_gpcdep5_pre2;


  drop table if exists gpcdep_gpcdep5_pre3;
  create table gpcdep_gpcdep5_pre3
select
    periode_ext as periodo,'GPCDEP-5' as ind, up, uba, tipus, num
from
  (select
	up, uba, 'M' as tipus, count(id_cip) as num 
   from
    gpcdep_gpcdep5_pre2
   group by
    up, uba
 union all
   select
	upinf as up, ubainf as uba, 'I' as tipus, count(id_cip) as num 
   from
	gpcdep_gpcdep5_pre2
   group by
	upinf, ubainf
  ) g
  , gpcdep_pextraccio
  ;

  drop table if exists gpcdep_gpcdep5_pre4;
  create table gpcdep_gpcdep5_pre4
  select
    up, uba, 'M' as tipus, count(id_cip) as den
  from
    gpcdep_gpcdep5_pre1
  group by
    up, uba
union all
  select
    upinf as up, ubainf as uba, 'I' as tipus, count(id_cip) as den
  from
    gpcdep_gpcdep5_pre1
  group by
    upinf, ubainf
  ;

  drop table if exists gpcdep_gpcdep5_pre5;
  create table gpcdep_gpcdep5_pre5
  select
    periodo, ind, b.up, b.uba, b.tipus, a.num, b.den
  from
    gpcdep_gpcdep5_pre3 a
  right join
    gpcdep_gpcdep5_pre4 b
  on
    a.up=b.up
    and a.uba=b.uba
	and a.tipus=b.tipus
  ;

  drop table if exists gpcdep_gpcdep5;
  create table gpcdep_gpcdep5
  select
    a.periodo, a.ind, b.up, b.uba, b.tipus, a.num, a.den
  from
    gpcdep_gpcdep5_pre5 a
  right join
    gpcdep_den b
  on
    a.up=b.up
    and a.uba=b.uba
	and a.tipus=b.tipus
  ;
  -- select * from gpcdep_gen2015_gpcdep5;
  update gpcdep_gpcdep5, gpcdep_pextraccio set periodo=periode_ext where periodo is null;
  update gpcdep_gpcdep5 set ind='GPCDEP-5' where ind is null;
  update gpcdep_gpcdep5 set num=0 where num is null;
  update gpcdep_gpcdep5 set den=0 where den is null;
  -- select * from gpcdep_gpcdep5;
  -- select * from leo.gpcdep_gen2015_gpcdep5;
  -- ------------------------------------------------------------------------------------------------


  -- INDICADOR 6
  -- ------------------------------------------------------------------------------------------------

  -- ------------------------------------------------------------------------------------------------

-- --------------------------------------------------------------------------------------------------------------------------


-- CREACIÓ TAULA FINAL
-- -----------------------------------------------------------------------------------------------------------------------

drop table if exists gpcdep;
create table gpcdep
select * from gpcdep_gpcdep1a
union all
select * from gpcdep_gpcdep1b
union all                    
select * from gpcdep_gpcdep2 
union all                    
select * from gpcdep_gpcdep3 
union all                    
select * from gpcdep_gpcdep4 
union all                    
select * from gpcdep_gpcdep5 
;



-- -----------------------------------------------------------------------------------------------------------------------






-- SELECCIÓ DE CENTRES I PROFESSIONALS
-- -----------------------------------------------------------------------------------------------------------------------
-- select * from gpcdep_professionals;
-- select up, count(1) as nprof, sum(visitat) as nvis, sum(consentiment) as ncon from gpcdep_professionals group by up;

## Aquesta part de l_script no cal executarla en el seguiment!!!!, només tenia sentit a l_inici de l_estudi

/* marco los equipos visitados y los consentimientos de cada equipo */

update gpcdep_professionals set visitat=1
where
	up='00357'
	or up='00360'  
;

update gpcdep_professionals set consentiment=1
where
	(up='00357' and IDE_NUMCOL='108263168') or
	(up='00357' and IDE_NUMCOL='108430491') or
	(up='00357' and IDE_NUMCOL='108291068') or
	(up='00357' and IDE_NUMCOL='108367491') or
	(up='00357' and IDE_NUMCOL='108290405') or
	(up='00357' and IDE_NUMCOL='108452440') or
	(up='00357' and IDE_NUMCOL='108205862') or
	(up='00357' and IDE_NUMCOL='108187963') or
	(up='00357' and IDE_NUMCOL='108283813') or
	(up='00357' and IDE_NUMCOL='108274374') or
	(up='00360' and IDE_NUMCOL='10834606') or
	(up='00360' and IDE_NUMCOL='108237766') or
	(up='00360' and IDE_NUMCOL='108236427') or
	(up='00360' and IDE_NUMCOL='108450820') or
	(up='00360' and IDE_NUMCOL='108106704') or
	(up='00360' and IDE_NUMCOL='108133840') or
	(up='00360' and IDE_NUMCOL='108288548') or
	(up='00360' and IDE_NUMCOL='108242233') or
	(up='00360' and IDE_NUMCOL='108179391') or
	(up='00360' and IDE_NUMCOL='108272440') or
	(up='00360' and IDE_NUMCOL='108405346') or
	(up='00360' and IDE_NUMCOL='108335776') or
	(up='00360' and IDE_NUMCOL='108300024') or
	(up='00360' and IDE_NUMCOL='10832934') or
	(up='00360' and IDE_NUMCOL='108240040')
	;

-- 2015-02-09: 3 centros nuevos y sus consentimientos
update gpcdep_professionals set visitat=1
where
	up='00342' or
	up='00358' or
	up='00359'
;

update gpcdep_professionals set consentiment=1
where
	(up='00342' and IDE_NUMCOL='108379573') or
	(up='00342' and IDE_NUMCOL='108328791') or
	(up='00342' and IDE_NUMCOL='108453518') or
	(up='00342' and IDE_NUMCOL='108373442') or
	(up='00342' and IDE_NUMCOL='108307268') or
	(up='00342' and IDE_NUMCOL='108205873') or
	(up='00342' and IDE_NUMCOL='108224446') or
	(up='00342' and IDE_NUMCOL='108425315') or
	(up='00342' and IDE_NUMCOL='108243651') or
	(up='00342' and IDE_NUMCOL='108451990') or
	(up='00342' and IDE_NUMCOL='108291226') or
	(up='00342' and IDE_NUMCOL='108305705') or
	(up='00358' and IDE_NUMCOL='10836710') or
	(up='00358' and IDE_NUMCOL='108321128') or
	(up='00358' and IDE_NUMCOL='108336002') or
	(up='00358' and IDE_NUMCOL='108440481') or
	(up='00358' and IDE_NUMCOL='108459098') or
	(up='00358' and IDE_NUMCOL='108174327') or
	(up='00358' and IDE_NUMCOL='108261864') or
	(up='00358' and IDE_NUMCOL='108157576') or
	(up='00358' and IDE_NUMCOL='10837010') or
	(up='00359' and IDE_NUMCOL='108219610') or
	(up='00359' and IDE_NUMCOL='108284105') or
	(up='00359' and IDE_NUMCOL='108389912') or
	(up='00359' and IDE_NUMCOL='108225403') or
	(up='00359' and IDE_NUMCOL='108391475') or
	(up='00359' and IDE_NUMCOL='108151445') or
	(up='00359' and IDE_NUMCOL='108355036') or
	(up='00359' and IDE_NUMCOL='108304164') or
	(up='00359' and IDE_NUMCOL='108448243')
;

update gpcdep_professionals set consentiment=1
where
	(up='00357' and IDE_NUMCOL='308269536') or
	(up='00357' and IDE_NUMCOL='308289955') or
	(up='00357' and IDE_NUMCOL='308342010') or
	(up='00357' and IDE_NUMCOL='343054661') or
	(up='00357' and IDE_NUMCOL='308368481') or
	(up='00357' and IDE_NUMCOL='30841584') or
	(up='00357' and IDE_NUMCOL='308235134') or
	(up='00357' and IDE_NUMCOL='30847119') or
	(up='00357' and IDE_NUMCOL='308097671') or
	(up='00357' and IDE_NUMCOL='308440334') or
	(up='00360' and IDE_NUMCOL='308081335') or
	(up='00360' and IDE_NUMCOL='30846241') or
	(up='00360' and IDE_NUMCOL='30830442') or
	(up='00360' and IDE_NUMCOL='308238328') or
	(up='00360' and IDE_NUMCOL='308352055') or
	(up='00360' and IDE_NUMCOL='308324853') or
	(up='00360' and IDE_NUMCOL='308093913') or
	(up='00360' and IDE_NUMCOL='308435608') or
	(up='00342' and IDE_NUMCOL='30829123') or
	(up='00342' and IDE_NUMCOL='308143942') or
	(up='00342' and IDE_NUMCOL='308285376') or
	(up='00342' and IDE_NUMCOL='308363215') or
	(up='00342' and IDE_NUMCOL='308315954') or
	(up='00342' and IDE_NUMCOL='308221612') or
	(up='00342' and IDE_NUMCOL='308496011') or
	(up='00359' and IDE_NUMCOL='308316112') or
	(up='00359' and IDE_NUMCOL='308228913') or
	(up='00359' and IDE_NUMCOL='308385930') or
	(up='00359' and IDE_NUMCOL='308291125') or
	(up='00359' and IDE_NUMCOL='308324381') or
	(up='00359' and IDE_NUMCOL='308210237') or
	(up='00359' and IDE_NUMCOL='308309992') or
	(up='00358' and IDE_NUMCOL='308315965') or
	(up='00358' and IDE_NUMCOL='30830043') or
	(up='00358' and IDE_NUMCOL='30841121')
;
-- -----------------------------------------------------------------------------------------------------------------------



set @anymes=(select periode_ext from gpcdep_pextraccio);

/*entregable professionals*/
set @sqlM = concat("
select periode_ext as periode, p.up, p.cognom1, p.COGNOM2, p.NOM, ind, num, den from gpcdep b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.consentiment=1 and p.tipus='M'
order by up, cognom1, cognom2, nom, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_",@anymes,"_professionalsM.txt'
fields terminated by '|'
lines terminated by '\r\n'
");



/* 
select periode_ext as periode, p.up, p.cognom1, p.COGNOM2, p.NOM, ind, num, den from gpcdep b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.consentiment=1 and p.tipus='M'
order by up, cognom1, cognom2, nom, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_201504_professionalsM.txt'
fields terminated by '|'
lines terminated by '\r\n'
; 
*/

set @sqlI = concat("
select periode_ext as periode, p.up, p.cognom1, p.COGNOM2, p.NOM, ind, num, den from gpcdep b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.consentiment=1 and p.tipus='I'
order by up, cognom1, cognom2, nom, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_",@anymes,"_professionalsI.txt'
fields terminated by '|'
lines terminated by '\r\n'
");


/*entregable centres*/
set @sqlC = concat("
select periode_ext as periode, p.up, ind, sum(num) as num, sum(den) as den from gpcdep b
inner join gpcdep_pextraccio
inner join gpcdep_professionals p
on b.up=p.up
and b.uba=p.uab
and b.tipus=p.tipus
where p.tipus='M'
group by p.up, ind
order by up, ind
into outfile 'P:/sisap/13sidiap/entregables/gpcdep_",@anymes,"_centres.txt'
fields terminated by '|'
lines terminated by '\r\n'
"
);


PREPARE s1 FROM @sqlM;
EXECUTE s1;
DROP PREPARE s1;

PREPARE s2 FROM @sqlI;
EXECUTE s2;
DROP PREPARE s2;

PREPARE s3 FROM @sqlC;
EXECUTE s3;
DROP PREPARE s3;

