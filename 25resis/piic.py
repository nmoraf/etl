# coding: iso-8859-1
import sisapUtils as u
import sisaptools as t
from datetime import datetime
import hashlib as h
import collections as c
# import csv
import os
import sys
from collections import defaultdict, Counter
path = os.path.realpath("./") + "/source"
sys.path.append(path)

SUFIX = "_resi"
file_name = "PIIC{}".format(SUFIX)
debug = TEST = False
# UsarMorts = True

# OutFile = u.tempFolder + 'pccmaca{}.txt'.format(SUFIX)

imp = 'import'
nod = 'nodrizas'
db = DATABASE = "resis"  # db = 'altres'
TaulaEstats = 'estats'
TaulaPIIC = 'piic'

# taulaAss = 'assignada'
# TaulaMy = 'exp_khalix_pccmaca'
# TaulaMyPre = 'mst_pcc_individual'


ind_opt = {"piic": {"pcc": "RES023A", "maca": "RES024A"},
           "piir": {"pcc": "RES023A1", "maca": "RES024A1"},
           "pirda": {"pcc": "RES023A2", "maca": "RES024A2"}
           }


class Qcresis(object):
    """."""

    def __init__(self):
        """."""

        ts = init = datetime.now()
        self.get_pob()
        print(datetime.now() - ts)
        print("pob_act: {}".format(len(self.pob_act)))
        print("pob_tot: {}".format(len(self.pob_tot)))
        ts = datetime.now()
        self.get_hcovid_map()
        print(datetime.now() - ts)
        print("hcovid_hash_map: {}".format(len(self.hcovid_hash_map)))
        print("hashes: {}".format(len(self.hashes)))
        ts = datetime.now()
        self.get_idcipsec_map()
        print(datetime.now() - ts)
        print("u11: {}".format(len(self.u11)))
        ts = datetime.now()
        self.get_id_resi()
        for k in [k for k in self.id_resi_tot][:3]:
            print(k, self.id_resi_tot[k])
        print("id_resi: {}".format((len(self.id_resi), len(self.id_resi_tot))))
        print(datetime.now() - ts)
        ts = datetime.now()
        print(ts - init)

    def get_pob(self):
        """
        Gets residents from res_master.
        - {pob_act}  # hcovid: (attributes)  # actual residents
        - {pob_tot}  # hcovid: (attributes)  # total residents since 2020-03-01
        """
        db = "redics"
        sch = "data"
        tb = "sisap_covid_res_master"
        cols = ["hash", "actual", "residencia", "entrada", "motiu", "sortida",
                "barthel_dat", "barthel_val", "pfeifer_dat", "pfeiffer_val",
                "situacio", "exitus_data", "vis_ultima",
                ]
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.pob_tot = {}
        self.pob_act = {}
        with t.Database(db, sch) as conn:
            for row in conn.get_all(sql):
                hcovid, actual = row[0], row[1]
                self.pob_tot[hcovid] = row[2:]
                if actual == 1:
                    self.pob_act[hcovid] = row[2:]

    def get_hash(self, cip):
        """Returns hcovid from cip."""
        return h.sha1(cip).hexdigest().upper()

    def get_hcovid_map(self):
        """
        Gets hash_d from hcovid.
        - {hcovid_hash_map} # hcovid: hash_a
        - set(hashes)  # set([hash_a, ...])
        !!! hay unos 67/62k que son hash_a que no ligan con u11.hash_d
        """

        LIMIT = 1000000
        limit = " where rownum <{}".format(LIMIT)
        db = 'redics'
        sch = 'pdp'
        tb = "pdptb101{}".format(limit if TEST else '')
        # conn = t.Database(db, sch)
        sql = 'select usua_cip, usua_cip_cod from {} '.format(tb)
        self.hcovid_hash_map = {}
        self.hashes = set()
        with t.Database(db, sch) as conn:
            for cip, hash_a in conn.get_all(sql):
                hcovid = self.get_hash(cip)
                if hcovid in self.pob_tot:
                    self.hashes.add(hash_a)
                    self.hcovid_hash_map[hcovid] = hash_a

    def get_idcipsec_map(self):
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        db = "p2262"
        sch = "import"
        tb = "u11_all"  # cambio u11 por u11_all
        cols = ["id_cip_sec", "hash_a", "hash_d"]  # cambio hash_d por hash_a
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.u11 = {}
        self.ids = {}
        with t.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d in conn.get_all(sql):
                if hash_a in self.hashes:
                    self.ids[id_cip_sec] = True
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d}

    def get_id_resi(self):
        self.id_resi = {}
        for hcovid, v in self.pob_act.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id] = resi
            except KeyError:
                continue
        self.id_resi_tot = c.defaultdict(set)
        for hcovid, v in self.pob_tot.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            if hash_a in self.u11:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id].add(resi)
        self.id_resi_tot.default_factory = None


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql = "select data_ext from {}.dextraccio;".format(nod)
    return u.getOne(sql, nod)[0]


def get_indicador(codi, num, pob_ids=None):
    """
    Construye el indicador a nivel de up.
    Si pob_ids es dada (un diccionario de ids: up),
    el denominador se construye con los ids que estan dentro de esta poblacion.
    En caso contrario, es el total de personas en la up.
    El numerador se obtiene a partir de num, otro set de ids.
       Devuelve una lista de filas (tuples).
    """
    indicador = defaultdict(Counter)
    if pob_ids is None:
        sql = "select id_cip_sec, up from {}.assignada_tot".format(nod)
        pob_ids = {id: up for id, up in u.getAll(sql, nod)}

    for id, up in pob_ids.items():
        indicador[up]["DEN"] += 1
        if id in num:
            indicador[up]["NUM"] += 1

    return [(up, codi, indicador[up]["NUM"], indicador[up]["DEN"])
            for up in indicador]


def export_table(table, columns, db, rows):
    u.createTable(table, columns, db, rm=True)
    u.listToTable(rows, table, db)


def push_to_khalix(db, taula, taula_centres, file_name, khalix=False):
    """."""
    cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

    cat_cond = "WHERE a.up = b.resi_cod and b.sisap_class = 1"
    cat_cond = " ".join((cataleg, cat_cond))
    cat_cond = """exists (select 1 from {cat_cond})
    """.format(cat_cond=cat_cond)

    query_template_string = """
        SELECT
            indicador, concat('A', 'periodo'),
            concat('R', replace(up,'-','_')) as resi, {strg},
            'NOCAT', 'NOIMP', 'POBACTUAL', 'N', {var}
        FROM
            {db}.{taula} a
        WHERE {cat_cond}
            -- INNER JOIN
            -- nodrizas.{taula_centres} b
            --    on a.up = scs_codi
            and a.{var} != 0
    """
    query_string1 = query_template_string.format(strg="'NUM'",
                                                 var="numerador",
                                                 db=db,
                                                 taula=taula,
                                                 cat_cond=cat_cond,
                                                 taula_centres=taula_centres)

    query_string2 = query_template_string.format(strg="'DEN'",
                                                 var="denominador",
                                                 db=db,
                                                 taula=taula,
                                                 cat_cond=cat_cond,
                                                 taula_centres=taula_centres)

    query_string = query_string1 + " union " + query_string2

    print(query_string)
    if khalix:
        u.exportKhalix(query_string, file_name)


if __name__ == '__main__':
    Resi = Qcresis()
    u.printTime('inici')
    current_date = get_date_dextraccio()
    u.printTime(current_date)
    # Se crea la tabla
    table_name = "exp_khalix_piic{}".format(SUFIX)
    table_columns = "(up varchar(13), indicador varchar(15), numerador int, denominador int)"  # noqa
    u.createTable(table_name, table_columns, DATABASE, rm=True)

    # aqui la ETL
    u.printTime('Estats')
    '''
    getEstats
    '''
    pcc, maca = set(), set()
    piirV, piirT = set(), set()
    piic, piir, pirda = set(), set(), set()
    # estats, estatsInd = {}, {}

    sql = """
    SELECT
        id_cip_sec,
        CASE es_cod
            WHEN 'ER0001' THEN 'PCC'
            WHEN 'ER0002' THEN 'MACA'
            ELSE 'error' END
    FROM {0} {1}
    WHERE es_cod in ('ER0001','ER0002')
    """.format(TaulaEstats, " limit 100" if debug else "")
    for id, estat in u.getAll(sql, imp):
        if estat == 'PCC':
            pcc.add(int(id))
        if estat == 'MACA':
            maca.add(int(id))

    '''
    PIIR criterio 1
    '''
    sql = """
        SELECT
            id_cip_sec,
            CASE left(mi_cod_var, 8)
                WHEN 'T4101014' THEN 'var'
                ELSE 'text' END as var,
            right(mi_cod_var, 2) as val
        FROM {}
        WHERE
            left(mi_cod_var, 8) in ('T4101015', 'T4101014')
            and mi_ddb = 0
        """.format(TaulaPIIC)
    for id, camp, valor in u.getAll(sql, imp):
        id = int(id)
        if camp == 'var':
            piirV.add((id, valor))
        elif camp == 'text':
            piirT.add((id, valor))

    for (id, valor) in piirV:
        if (id, valor) in piirT:
            piir.add(id)

    '''
    PIIR criterio 2
    PIRDA criterio unico
    '''
    sql = """
        SELECT
            id_cip_sec,
            CASE left(mi_cod_var,8)
                WHEN 'T4101001' THEN 'rec'
                WHEN 'T4101002' THEN 'dant'
                WHEN 'T4101017' THEN 'dant'
                ELSE '0' END as tipii
        FROM {}
        WHERE
            mi_ddb = 0
            and left(mi_cod_var, 8) in ('T4101001', 'T4101002', 'T4101017')
        """.format(TaulaPIIC)

    for id, tipii in u.getAll(sql, imp):
        id = int(id)
        piic.add(id)
        if tipii == 'rec':
            piir.add(id)
        elif tipii == 'dant':
            pirda.add(id)

    num_keys = {"piic": piic, "piir": piir, "pirda": pirda}
    num_type = {"pcc": pcc, "maca": maca}
    for key in ind_opt:  # Para cada key (piic, piir, pirda):
        print(key)
        for type_num, codi in ind_opt[key].iteritems():  # c/subtip (pcc, maca)
            # interseccion de los set(ids) de key y de subtipo
            num = num_keys[key].intersection(num_type[type_num])
            # Se obtienen las rows por up para cada tipo de indicador
            # (pcc o maca) dentro de una key (piic, piir, pirda)
            rows = get_indicador(codi, num, Resi.id_resi)

            u.listToTable(rows, table_name, DATABASE)
            u.printTime("Insert into the table {}".format(codi))

    # salida a khalix
    push_to_khalix(db=DATABASE, taula=table_name, taula_centres="cat_centres",
                   file_name=file_name, khalix=True)
