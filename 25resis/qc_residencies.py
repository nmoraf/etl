# qc_residencies

# poblaciones de sisap_covid_res_master (hcovid)
# traer actius por un lado y totals por otro.
import hashlib as h
from datetime import datetime
import collections as c
# import pandas as pd
# from os import path

import sisaptools as t
import sisapUtils as u

TEST = False

DATABASE = 'resis'

# thash_a = ['EC6A7267DB540640A9BB94AE7A10176F67E4C5B3',
#            '21E631C72A2B49937EB25ADB5E0B8C9F180C750B',
#            '37EF7534490C7798D8FDBB416C85901CBFD70B1F',
#            '779CD088DB8EEF1E89DDF9045251A71951840F53',
#            'E522B4F3F97DC86E0B127D222E45A1A69A475BAA',
#            '5D754EADC0894FB01802AD3B3F430BFCE9ACA7D6',
#            '550FF2EDAE58716070DE040DB373F0AB413D7E12',
#            'A9BFF10C4E976A486170941FDDC430747457592D',
#            'D120501FECFF6E94E0A00C32C69EF24A97F5E9AA',
#            '582392D305996FEE5459E97B399529C23D09EB0C',
#            ]

DEXT = str(t.Database("p2262", "nodrizas").
           get_one("select data_ext from dextraccio")[0]
           )

KHALIX_6DIM = ('ind varchar(15)',
               'period varchar(5)',
               'entity varchar(13)',
               'analysys varchar(10)',
               'detail varchar(10)',
               'dim6set varchar(10)',
               'valor int',
               )

KHALIX_5DIM = ('ind varchar(15)',
               'entity varchar(13)',
               'analysys varchar(10)',
               'detail varchar(10)',
               'dim6set varchar(10)',
               'valor int',
               )

KHALIX_4DIM = ('ind varchar(15)',
               'entity varchar(13)',
               'analysys varchar(10)',
               'dim6set varchar(10)',
               'valor int',
               )


class Qcresis(object):
    """."""

    def __init__(self):
        """."""

        ts = init = datetime.now()
        self.get_pob()
        print(datetime.now() - ts)
        print("pob_act: {}".format(len(self.pob_act)))
        print("pob_tot: {}".format(len(self.pob_tot)))
        ts = datetime.now()
        self.get_hcovid_map()
        print(datetime.now() - ts)
        print("hcovid_hash_map: {}".format(len(self.hcovid_hash_map)))
        print("hashes: {}".format(len(self.hashes)))
        ts = datetime.now()
        self.get_idcipsec_map()
        print(datetime.now() - ts)
        print("u11: {}".format(len(self.u11)))
        self.get_id_resi()
        print("id_resi_tot:")
        for k in [k for k in self.id_resi_tot][:3]:
            print(k, self.id_resi_tot[k])
        print("id_resi:")
        for k in [k for k in self.id_resi][:3]:
            print(k, self.id_resi[k])
        # for hash_a in list(self.hashes):
        #     if hash_a not in self.u11:
        #         print(hash_a)
        # for th in thash_a:
        #     for k, v in self.hcovid_hash_map.items():
        #         if th == v:
        #             print(th, k, th in self.hashes, k in self.hashes)
        #     print(th, th in self.hashes)
        ts = datetime.now()
        self.get_centres()
        print("centres: {}".format(len(self.centres)))
        ts = datetime.now()
        # self.get_ubas()
        # print("ubas: {}".format(len(self.ubas)))
        # ts = datetime.now()
        """ OJO, comentado para hacer barthel_pfeiffer
        self.get_longit_dades()
        print("longit: {}".format(len(self.longit_dades)))
        ts = datetime.now()
        for item in [item for item in self.longit_dades.items()][:3]:
            print(item)
        ts = datetime.now()

        # cont
        self.get_cont_dades()
        print("cont0002: {}".format(len(self.cont_dades)))
        for item in [item for item in self.cont_dades.items()][:3]:
            print(item)
        for k in [k for k in self.pcc_maca][:3]:
            print(k, self.pcc_maca[k])

        # covres
        ts = datetime.now()
        self.get_covres_dades()
        print("covres: {}".format(len(self.covres_dades)))
        """
        # barthel_pfeiffer
        ts = datetime.now()
        self.get_pob_barthel_pfeiffer()
        print("barthel_pfeiffer: {}".format(ts))

        print(datetime.now() - ts)
        print(ts - init)

    def get_pob(self):
        """
        Gets residents from res_master.
        - {pob_act}  # hcovid: (attributes)  # actual residents
        - {pob_tot}  # hcovid: (attributes)  # total residents since 2020-03-01
        """
        db = "redics"
        sch = "data"
        tb = "sisap_covid_res_master"
        cols = ["hash", "actual", "residencia", "entrada", "motiu", "sortida",
                "barthel_dat", "barthel_val", "pfeifer_dat", "pfeiffer_val",
                "situacio", "exitus_data", "vis_ultima",
                ]
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.pob_tot = {}
        self.pob_act = {}
        with t.Database(db, sch) as conn:
            for row in conn.get_all(sql):
                hcovid, actual = row[0], row[1]
                self.pob_tot[hcovid] = row[2:]
                if actual == 1:
                    self.pob_act[hcovid] = row[2:]

    def get_hash(self, cip):
        """Returns hcovid from cip."""
        return h.sha1(cip).hexdigest().upper()

    def get_hcovid_map(self):
        """
        Gets hash_d from hcovid.
        - {hcovid_hash_map} # hcovid: hash_a
        - set(hashes)  # set([hash_a, ...])
        !!! hay unos 67/62k que son hash_a que no ligan con u11.hash_d
        """

        LIMIT = 1000000
        limit = " where rownum <{}".format(LIMIT)
        db = 'redics'
        sch = 'pdp'
        tb = "pdptb101{}".format(limit if TEST else '')
        # conn = t.Database(db, sch)
        sql = 'select usua_cip, usua_cip_cod from {} '.format(tb)
        self.hcovid_hash_map = {}
        self.hashes = set()
        with t.Database(db, sch) as conn:
            for cip, hash_a in conn.get_all(sql):
                hcovid = self.get_hash(cip)
                if hcovid in self.pob_tot:
                    self.hashes.add(hash_a)
                    self.hcovid_hash_map[hcovid] = hash_a

    def get_idcipsec_map(self):
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        db = "p2262"
        sch = "import"
        tb = "u11_all"  # cambio u11 por u11_all
        cols = ["id_cip_sec", "hash_a", "hash_d"]  # cambio hash_d por hash_a
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.u11 = {}
        self.ids = {}
        with t.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d in conn.get_all(sql):
                if hash_a in self.hashes:
                    self.ids[id_cip_sec] = True
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d}

    def get_id_resi(self):
        self.id_resi = {}
        for hcovid, v in self.pob_act.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id] = resi
            except KeyError:
                continue
        self.id_resi_tot = c.defaultdict(set)
        for hcovid, v in self.pob_tot.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            if hash_a in self.u11:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id].add(resi)
        self.id_resi_tot.default_factory = None

    def get_pob_barthel_pfeiffer(self):
        file_name = "barthel_pfeiffer_resi"
        barthel_codi = {'lt20': 'RES026A1',
                        '20-59': 'RES026A2',
                        '60-99': 'RES026A3',
                        '100': 'RES026A4'}
        pfeiffer_codi = {'le2': 'RES027A4',
                         '3-4': 'RES027A3',
                         '5-7': 'RES027A2',
                         'ge8': 'RES027A1'}
        self.id_resi_barthel = {'lt20': {},
                                '20-59': {},
                                '60-99': {},
                                '100': {}}
        self.id_resi_pfeiffer = {'le2': {},
                                 '3-4': {},
                                 '5-7': {},
                                 'ge8': {}}
        indicador = c.defaultdict(c.Counter)
        for hcovid, v in self.pob_act.items():
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            if hash_a in self.u11:
                id = self.u11[hash_a]['id_cip_sec']
            resi = v[0]
            indicador[resi]["DEN"] += 1
            barthel = v[5]
            pfeiffer = v[7]
            g_barthel = g_pfeiffer = None
            if barthel < 20:
                g_barthel = 'lt20'
            if 20 <= barthel < 60:
                g_barthel = '20-59'
            if 60 <= barthel < 100:
                g_barthel = '60-99'
            if barthel == 100:
                g_barthel = '100'
            if pfeiffer <= 2:
                g_pfeiffer = 'le2'
            if 3 <= pfeiffer <= 4:
                g_pfeiffer = '3-4'
            if 5 <= pfeiffer <= 7:
                g_pfeiffer = '5-7'
            if 8 <= pfeiffer <= 10:
                g_pfeiffer = 'ge8'
            if g_barthel is not None:
                self.id_resi_barthel[g_barthel][id] = resi
            if g_pfeiffer is not None:
                self.id_resi_pfeiffer[g_pfeiffer][id] = resi
        for k1, v1 in self.id_resi_barthel.items():
            codi = barthel_codi[k1]
            for _k2, resi in v1.items():
                if resi in indicador:
                    indicador[resi][codi] += 1
        for k1, v1 in self.id_resi_pfeiffer.items():
            codi = pfeiffer_codi[k1]
            for _k2, resi in v1.items():
                if resi in indicador:
                    indicador[resi][codi] += 1

        upload = []
        for resi in indicador:
            for codi in ["RES026A1", "RES026A2", "RES026A3", "RES026A4",
                         "RES027A1", "RES027A2", "RES027A3", "RES027A4"]:
                upload.append((codi, resi, "DEN", "POBACTUAL",
                               indicador[resi]["DEN"]))
                try:
                    upload.append((codi, resi, "NUM", "POBACTUAL",
                                   indicador[resi][codi]))
                except KeyError:
                    pass
        tb = 'exp_khalix_{}'.format(file_name)
        with t.Database('p2262', DATABASE) as conn:
            conn.create_table(tb, KHALIX_4DIM, remove=True)
            conn.list_to_table(upload, tb)

        cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

        cat_cond = "WHERE a.entity = b.resi_cod and b.sisap_class = 1"
        cat_cond = " ".join((cataleg, cat_cond))
        cat_cond = """exists (select 1 from {cat_cond})
        """.format(cat_cond=cat_cond)

        query_string = """
            SELECT
                ind, 'Aperiodo',
                concat('R', replace(entity,'-','_')) as resi, analysys,
                'NOCAT', 'NOIMP', dim6set, 'N', valor
            FROM
                {db}.{taula} a
            WHERE {cat_cond}
                -- INNER JOIN
                -- nodrizas.{taula_centres} c
                --    on a.up = scs_codi
                and a.valor <> 0
            """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
                       taula_centres='_dummy')

        u.exportKhalix(query_string, file_name)

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_ubas(self):
        """."""
        sql = "select up, uba, tipus from mst_ubas"
        self.ubas = set([row for row in u.getAll(sql, "eqa_ind")])

    def get_partition(self, db, tb, by):
        """."""
        self.partitions = {}
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        dexta, = u.getOne("""
                          select date_add(data_ext, interval - 1 month)
                          from dextraccio
                          """, "nodrizas")
        sql = "select {} from {} partition({}) limit 1"
        for partition in u.getTablePartitions(tb, db):
            try:
                dvis, = u.getOne(sql.format(by, tb, partition), db)
            except TypeError:
                continue
            if (dvis.year == dext.year and dvis.month == dext.month) or \
                    (dvis.year == dexta.year and dvis.month == dexta.month):
                self.partitions[partition] = True

    def get_longit_dades(self):
        """
        De momento para {id_resi} (actual), !! falta total {id_resi_tot}
            podria ser una segunda pasada...
            pero hace falta sentar el id en varias resis
        """
        # filename = "QcResi_longitudinalitat.xlsx"
        # file = path.join(u.tempFolder, filename)
        file_name = "LONG_resi"

        # db = "p2262"
        sch = "nodrizas"
        tb = "ag_longitudinalitat_new"
        by = "vis_data"

        self.get_partition(db=sch, tb=tb, by=by)

        self.longit_dades = c.Counter()
        for parti in self.partitions:
            sql = "select pac_id, pac_up, pac_uba, pac_ubainf, \
                          pac_pcc, pac_maca, pac_atdom, \
                          modul_serv_homol, modul_eap, \
                          prof_responsable, prof_delegat, modul_relacio, \
                          prof_rol_gestor, modul_serv_gestor, \
                          right(year(vis_data),2),date_format(vis_data,'%m')\
                   from {} partition({}) \
                   where pac_instit = 0 and \
                         prof_numcol <> '' and \
                         vis_laborable = 1".format(tb, parti)
            for (id, _up, _uba, _ubainf, pcc, maca, _atdom, serv,
                    eap, resp, deleg, _rel, gc_rol, gc_serv,
                    anys, mes) in u.getAll(sql, sch):
                if id in self.id_resi:
                    pob = "POBACTUAL"
                    resi = self.id_resi[id]
                    gc = (gc_rol == 1 or gc_serv == 1)
                    periodo = "A" + str(anys) + str(mes)
                    if (eap and serv in ("MG", "PED", "INF", "INFP")) or gc:
                        # br = self.centres[up]
                        tip = "1" if (pcc or maca) else "2"  # quito atdom
                        tipprof = 1 if serv == "MG" else 2 if serv == "PED" else 4  # noqa
                        detalle = "TIPPROF{}".format(tipprof)
                        # entities = [br] # en lugar de equipos...
                        entities = [resi]  # ... vamos por residencia.
                        # entities.extend([resi + entity[2]  # + entity[1]
                        #                  for entity
                        #                  in ((up, uba, "M"),
                        #                      (up, ubainf, "I"))
                        #                  if entity in self.ubas])
                        for entity in entities:
                            cuenta = "LONG000{}".format(tip)
                            self.longit_dades[(cuenta, periodo, entity,
                                               detalle, pob,
                                               "DEN")] += 1
                            if resp == 1 or gc == 1 or deleg == 1:
                                self.longit_dades[("LONG000{}".format(tip),
                                                   periodo, entity,
                                                   detalle, pob,
                                                   "NUM")] += 1
                            # for i in ['A', 'B', 'C']:
                            #     cuenta = "LONG000{}{}".format(tip, i)
                            #     self.longit_dades[(cuenta, periodo, entity,
                            #                        detalle, pob,
                            #                        "DEN")] += 1
                            # if resp == 1:
                            #     self.longit_dades[("LONG000" + tip + "A",
                            #                        periodo, entity,
                            #                        detalle, pob,
                            #                        "NUM")] += 1
                            # elif gc == 1:
                            #     self.longit_dades[("LONG000" + tip + "C",
                            #                        periodo, entity,
                            #                        detalle, pob,
                            #                        "NUM")] += 1
                            # elif deleg == 1:
                            #     self.longit_dades[("LONG000" + tip + "B",
                            #                        periodo, entity,
                            #                        detalle, pob,
                            #                        "NUM")] += 1
        # df = pd.Series(self.longit_dades, name='value').reset_index()
        # columns = ['ind', 'periode', 'entity', 'detalle', 'analisis', 'valor']  # noqa
        # df.columns = columns
        # df.to_excel(file, index=False)

        tb = "exp_khalix_{}".format(file_name)

        longit_dades_ls = []
        for (ind, period, entity, detail, pob, analysys), v in self.longit_dades.items():  # noqa
            if ind in ["LONG0001", "LONG0002"]:  # para evitar sufijos 'A','B','C'  # noqa
                kl = list((ind, period, entity, analysys, detail, pob))
                kl.append(v)
                longit_dades_ls.append(tuple(kl))

        with t.Database('p2262', DATABASE) as conn:
            conn.create_table(tb, KHALIX_6DIM, remove=True)
            conn.list_to_table(longit_dades_ls, tb)

        cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

        cat_cond = "WHERE a.entity = b.resi_cod and b.sisap_class = 1"
        cat_cond = " ".join((cataleg, cat_cond))
        cat_cond = """exists (select 1 from {cat_cond})
        """.format(cat_cond=cat_cond)

        query_string = """
            SELECT
                ind, period,
                concat('R', replace(entity,'-','_')) as resi, analysys,
                'NOCAT', detail, dim6set, 'N', valor
            FROM
                {db}.{taula} a
            WHERE {cat_cond}
                -- INNER JOIN
                -- nodrizas.{taula_centres} c
                --    on a.up = scs_codi
                and a.valor <> 0
            """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
                       taula_centres='_dummy')

        u.exportKhalix(query_string, file_name)

    def get_cont_dades(self):
        """
        Indicador CONT0002 :usual provider of care.
            r2 -> float(vprof) / float(totals)
        """
        file_name = "CONT_resi"

        DETALLES = {'MG': 'TIPPROF1',
                    'PED': 'TIPPROF2',
                    'ODN': 'TIPPROF3',
                    'INF': 'TIPPROF4'}

        """Indicadors de CONT per pcc i maca"""
        db = "p2262"
        sch = "nodrizas"
        tb = "assignada_tot"
        sql = """
                SELECT id_cip_sec
                FROM assignada_tot
                WHERE
                    edat > 14 AND
                    (maca = 1 or pcc = 1)
            """

        self.pcc_maca = {}  # residentes ACTUALES con pcc o maca.
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.id_resi:
                resi = self.id_resi[id]
                self.pcc_maca[id] = resi

        db = "p2262"
        sch = "altres"
        tb = "mst_long_cont_pacient"
        cols = ["id_cip_sec", "servei", "r2"]
        where = "servei in ('MG', 'INF')"

        cols = ", ".join(cols)
        sql = "select {} from {} where {}".format(cols, tb, where)
        self.cont_dades = c.Counter()
        # self.pob_act = {}
        with t.Database(db, sch) as conn:
            for id, servei, r2 in conn.get_all(sql):
                ind = 'CONT0002P' if id in self.pcc_maca else "CONT0002"
                detalle = DETALLES[servei]
                if id in self.id_resi:
                    pob = "POBACTUAL"
                    resi = self.id_resi[id]
                    self.cont_dades[(ind, resi, "NUM", detalle, pob)] += r2
                    self.cont_dades[(ind, resi, "DEN", detalle, pob)] += 1
                if id in self.id_resi_tot:
                    pob = "POBTOTAL"
                    for resi in list(self.id_resi_tot[id]):
                        self.cont_dades[(ind, resi, "NUM", detalle, pob)] += r2
                        self.cont_dades[(ind, resi, "DEN", detalle, pob)] += 1
        # filename = "QcResi_continuitat.xls"
        # file = path.join(u.tempFolder, filename)
        # series = pd.Series(self.cont_dades, name='value').reset_index()
        # columns_ = ['ind', 'resi', 'analisis', 'detalle', 'dim6set', 'valor']
        # series.columns = columns_
        # series[['resi', 'ind', 'analisis', 'detalle', 'dim6set', 'valor']].to_excel(file)  # noqa

        tb = "exp_khalix_{}".format(file_name)

        cont_dades_ls = []
        for k, v in self.cont_dades.items():
            kl = list(k)
            kl.append(v)
            cont_dades_ls.append(tuple(kl))

        with t.Database('p2262', DATABASE) as conn:
            conn.create_table(tb, KHALIX_5DIM, remove=True)
            conn.list_to_table(cont_dades_ls, tb)

        cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

        cat_cond = "WHERE a.entity = b.resi_cod and b.sisap_class = 1"
        cat_cond = " ".join((cataleg, cat_cond))
        cat_cond = """exists (select 1 from {cat_cond})
        """.format(cat_cond=cat_cond)

        query_string = """
            SELECT
                ind, 'Aperiodo',
                concat('R', replace(entity,'-','_')) as resi, analysys,
                'NOCAT', detail, dim6set, 'N', valor
            FROM
                {db}.{taula} a
            WHERE {cat_cond}
                -- INNER JOIN
                -- nodrizas.{taula_centres} c
                --    on a.up = scs_codi
                and a.valor <> 0
            """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
                       taula_centres='_dummy')

        u.exportKhalix(query_string, file_name)

    def get_covres_dades(self):
        """."""
        file_name = "COVRES_resi"

        db = "redics"
        sch = "data"
        tb = "sisap_covid_res_indicadors"
        # COVRES_IND = (
        #     'COVRES30A',  # Percentatge pacients PCC (actius)
        #     'COVRES40A',  # Percentatge pacients PCC (inactius)
        #     'COVRES31A',  # Percentatge pacients MACA (actius)
        #     'COVRES41A',  # Percentatge pacients MACA (inactius)
        #     'COVRES08A',  # Percentatge pacients amb Barthel (actius)
        #     'COVRES18A',  # Percentatge pacients amb Barthel (inactius)
        #     'COVRES09A',  # Percentatge pacients amb Pfeiffer (actius)
        #     'COVRES19A',  # Percentatge pacients amb Pfeiffer (inactius)
        #     )

        COVRES_ACTUAL = {"COVRES30A": "RES018A",
                         "COVRES31A": "RES019A",
                         "COVRES08A": "RES026A",
                         "COVRES09A": "RES027A",
                         }
        COVRES_TOTAL = {"COVRES40A": "COVRES30A",
                        "COVRES41A": "COVRES31A",
                        "COVRES18A": "COVRES08A",
                        "COVRES19A": "COVRES09A",
                        }
        COVRES_IND = tuple(set(COVRES_TOTAL.keys() + COVRES_ACTUAL.keys()))

        header_ind = "case indicador "
        footer_ind = "else indicador end {}"
        when_then = ""
        for k, v in COVRES_TOTAL.items():
            when_then += "when '{}' then '{}' ".format(k, v)

        case_when = header_ind + when_then
        ind_select = case_when + footer_ind.format("as indicador")

        cols = ["resi",
                ind_select,
                "'POBTOTAL' as dim6set",
                "sum(num) as num",
                "sum(den) as den",
                ]
        ind_group = 'resi, ' + case_when + footer_ind.format("")
        where = "indicador in {} and data = date '{}'".format(COVRES_IND, DEXT)

        cols = ", ".join(cols)
        sql1 = """select {} from {} where {} group by {}
        """.format(cols, tb, where, ind_group)

        cols = ["resi",
                "indicador",
                "'POBACTUAL' as dim6set",
                "num",
                "den"]
        where = " indicador in {} and data = date '{}'".format(tuple(COVRES_ACTUAL.keys()), DEXT)  # noqa

        cols = ", ".join(cols)
        sql2 = "select {} from {} where {}".format(cols, tb, where)
        sql = sql1 + " union " + sql2

        self.covres_dades = []
        with t.Database(db, sch) as conn:
            for resi, ind, dim6set, num, den in conn.get_all(sql):
                ind = COVRES_ACTUAL[ind]
                this_num = (ind, resi, "NUM", dim6set, num)
                this_den = (ind, resi, "DEN", dim6set, den)
                self.covres_dades.extend((this_num, this_den))

        tb = "exp_khalix_covres_resi"
        with t.Database('p2262', DATABASE) as conn:
            conn.create_table(tb, KHALIX_4DIM, remove=True)
            conn.list_to_table(self.covres_dades, tb)

        # filename = "QcResi_covres.xls"
        # file = path.join(u.tempFolder, filename)
        # columns = ['ind', 'periode', 'resi', 'analisis', 'nocat', 'detalle',
        #            'dim6set', 'N', 'valor']
        # df = pd.DataFrame(self.covres_dades, columns=columns)
        # df[['resi', 'ind', 'analisis', 'valor']].to_excel(file, index=False)

        cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

        cat_cond = "WHERE a.entity = b.resi_cod and b.sisap_class = 1"
        cat_cond = " ".join((cataleg, cat_cond))
        cat_cond = """exists (select 1 from {cat_cond})
        """.format(cat_cond=cat_cond)

        query_string = """
            SELECT
                ind, 'Aperiodo',
                concat('R', replace(entity,'-','_')) as resi, analysys,
                'NOCAT', 'NOIMP', dim6set, 'N', valor
            FROM
                {db}.{taula} a
            WHERE {cat_cond}
                -- INNER JOIN
                -- nodrizas.{taula_centres} c
                --    on a.up = scs_codi
                and a.valor <> 0
            """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
                       taula_centres='_dummy')

        u.exportKhalix(query_string, file_name)


if __name__ == "__main__":
    Qcresis()
    # long: nodrizas.ag_longitudinalitat_new
    #   (con institucionalitats o assignada_tot da 39k en covid son 51k
    #    concluyo que son residents no assignats)
    #   02nod\source\ag_longitudinalitat_new.py
    #   08alt\source\ag_longitudinalitat_new.py
    # cont: altres.mst_long_cont_pacient (08alt\source\long_and_cont.py)
    #   cont002: r2 -> float(vprof) / float(totals)
    #   cont002p: r2 where pcc = 1 and maca = 1

    # (('CONT0002', '_periodo', '04223', 'NUM', '_NOCAT', 'TIPPROF4', 'POBTOTAL', '_N'), 14.2)  # noqa
    # for ind, _periode, ent, analisi, _nocat, tip, _dim6set, _n, v in self.cont_dades:  # noqa
    #   this = (ent, ind, analisi, tip, v)

    # (('LONG0002', 'A2008', '07139', 'TIPPROF1', 'NUM'), 1)
    # for ind, periode, ent, tip, analisi, v in self.longit_dades:
    #   if periode == 'A2009':
    #       this = (ent, ind, analis, tip, v)

    # (ind, 'periodo', resi, "NUM", "NOCAT", 'detalle', "DIM6SET", "N", num)
    # for ind, periode, ent, analisi, _nocat, _detalle, _dim6set, _n, v in self.covres_dades:  # noqa
    #     this = (ent, ind, analisi, 'NOTIP', v)


# # SCRATCH

# [col for col in t.Database("p2262","import").]
