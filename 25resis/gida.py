# -*- coding: latin1 -*-

"""
copiado de 08alt/source/gida.py
"""
import hashlib as h
from datetime import datetime
import collections as c

import sisaptools as t
import sisapUtils as u

import dateutil.relativedelta as r

TEST = False
SUFIX = "_resi"

MASTER_TB = "mst_gida{}".format(SUFIX)
KHALIX_TB = "exp_khalix_gida{}".format(SUFIX)
KHALIX_UP = "GIDA{}".format(SUFIX)
KHALIX_UBA = "GIDA_UBA{}".format(SUFIX)
CORTES_TB = "exp_khalix_gida_mensual{}".format(SUFIX)
CORTES_UP = "GIDA_MENSUAL{}".format(SUFIX)
QC_TB = "exp_qc_gida{}".format(SUFIX)
# DATABASE = "altres"
DATABASE = "resis"

TAXES = {"PANTIC": 0.478349492, "PCREM": 2.696570544, "PDIARR": 11.71060743,
         "PEPIS": 0.357181663, "PFERID": 19.87282559, "PLDERM": 0.683124112,
         "PMURI": 25.16421292, "PODINO": 10.43890813, "PODON": 0.979618738,
         "PPRESA": 4.036460068, "PRESVA": 16.76125564, "PTURME": 1.176287446,
         "PGRIPA": 1.149038759, "PCONTU": 4.778138334, "PFEBSF": 1.02878384,
         "PMAREI": 7.883834144, "PPICPE": 2.165190017, "PAPZB": 1.610639762,
         "PCOLIC": 0.271560926, "PCREPE": 1.973685066, "PDIAGN": 2.725524082,
         "PFERNE": 19.18352993, "PTOS": 5.11413805, "PMUCO": 3.359588892,
         "PCOSEN": 0.222443472, "PCOSEO": 0.244977952, "PDOLMA": 0.270690131,
         "PFEBAN": 8.777755506, "PPICIN": 2.158291845, "PPLOIN": 0.224289029,
         "PPOLLS": 0.352068596, "PREGUR": 0.164098954, "PRESTN": 0.644351599,
         "PSOSVA": 0.291036088, "PVOMNE": 2.726337056, "PEPISN": 0.315756236,
         "PCONTD": 4.957455211, "PLESC": 1.613940246, "PPELLA": 0.361539749,
         "PMUGN": 0.188537624}

CONVERSIO = {"ACE_A": "ANTIC", "CERVI_A": "CERVI", "CONT_A": "CONTU",
             "CREM_A": "CREM", "C_ANSI_A": "ANSIE", "DIARR_A": "DIARR",
             "LUMB_A": "DML", "EPA_A": "PRESA", "ENTOR_A": "TURME",
             "EPIST_A": "EPIS", "FEBRE_A": "FEBSF", "FERIDA_A": "FERID",
             "PLEC_A": "LDERM", "MAREIG_A": "MAREI", "M_OID_A": "MOLOR",
             "M_ULL_A": "ULL", "M_URI_A": "MURI", "ODIN_A": "ODINO",
             "ODONT_A": "ODON", "PICAD_A": "PICPE", "SRVA_A": "RESVA",
             "VOMIT_A": "DIARR", "ALT_A": "A", "VOMIT": "DIARR",
             "CEFAL": "CEFAL_A", "HERP": "HERPL_A", "ABCES": "ABCES_A",
             "UNG": "UNG_A", "VOLT": "VOLT_A", "RESTR": "RESTRE_A",
             "MVAG": "MVAG_A", "REVAC": "REVAC_A", "AFTA": "AFTA_A",
             "INSOM": "INSOM_A", "URTI": "URTI_A"}


class GIDA(object):
    """."""

    def __init__(self):
        """."""
        init = ts = datetime.now()
        print(ts)
        self.get_pob()
        print("pob_act: {}".format(len(self.pob_act)))
        print("pob_tot: {}".format(len(self.pob_tot)))
        print(datetime.now() - ts)
        ts = datetime.now()
        self.get_hcovid_map()
        print(datetime.now() - ts)
        print("hcovid_hash_map: {}".format(len(self.hcovid_hash_map)))
        print("hashes: {}".format(len(self.hashes)))
        ts = datetime.now()
        self.get_idcipsec_map()
        self.get_id_resi()
        print(ts - init)

        ts = datetime.now()
        self.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        # 5 y 6 *ubainf (taxa poblacional), 3 y 4 *Colegiado (% visitas)
        self.mensual = c.Counter()  # get_aguda: 154 y export_mensual: 296
        self.get_edats()  # categories: adu, ped
        self.get_poblacio()  # {id: (up, ubainf)}, fitro por id_resi
        self.get_centres()
        self.get_visites()
        self.get_usu_to_col()
        self.get_motius()
        self.get_aguda()
        self.get_master()
        self.get_derivats()
        self.export_master()
        print(datetime.now() - ts)
        self.get_col_to_uba()
        # self.get_ubas()
        self.get_khalix()
        self.get_taxes()
        self.export_khalix()
        # self.export_mensual()
        self.export_qc()

    def get_pob(self):  # QcResis
        """
        Gets residents from res_master.
        - {pob_act}  # hcovid: (attributes)  # actual residents
        - {pob_tot}  # hcovid: (attributes)  # total residents since 2020-03-01
        """
        db = "redics"
        sch = "data"
        tb = "sisap_covid_res_master"
        cols = ["hash", "actual", "residencia", "entrada", "motiu", "sortida",
                "barthel_dat", "barthel_val", "pfeifer_dat", "pfeiffer_val",
                "situacio", "exitus_data", "vis_ultima",
                ]
        cols = ", ".join(cols)
        sql = "SELECT {} FROM {}".format(cols, tb)
        self.pob_tot = {}
        self.pob_act = {}
        with t.Database(db, sch) as conn:
            for row in conn.get_all(sql):
                hcovid, actual = row[0], row[1]
                self.pob_tot[hcovid] = row[2:]
                if actual == 1:
                    self.pob_act[hcovid] = row[2:]

    def get_hash(self, cip):  # QcResis
        """Returns hcovid from cip."""
        return h.sha1(cip).hexdigest().upper()

    def get_hcovid_map(self):  # QcResis
        """
        Gets hash_d from hcovid.
        - {hcovid_hash_map} # hcovid: hash_a
        - set(hashes)  # set([hash_a, ...])
        !!! hay unos 67/62k que son hash_a que no ligan con u11.hash_d
        """

        LIMIT = 1000000
        limit = " WHERE rownum <{}".format(LIMIT)
        db = 'redics'
        sch = 'pdp'
        tb = "pdptb101{}".format(limit if TEST else '')
        # conn = t.Database(db, sch)
        sql = 'SELECT usua_cip, usua_cip_cod FROM {} '.format(tb)
        self.hcovid_hash_map = {}
        self.hashes = set()
        with t.Database(db, sch) as conn:
            for cip, hash_a in conn.get_all(sql):
                hcovid = self.get_hash(cip)
                if hcovid in self.pob_tot:
                    self.hashes.add(hash_a)
                    self.hcovid_hash_map[hcovid] = hash_a

    def get_idcipsec_map(self):  # QcResis
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        db = "p2262"
        sch = "import"
        tb = "u11_all"  # cambio u11 por u11_all
        cols = ["id_cip_sec", "hash_a", "hash_d"]  # cambio hash_d por hash_a
        cols = ", ".join(cols)
        sql = "SELECT {} FROM {}".format(cols, tb)
        self.u11 = {}
        self.ids = {}
        with t.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d in conn.get_all(sql):
                if hash_a in self.hashes:
                    self.ids[id_cip_sec] = True
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d}

    def get_id_resi(self):  # QcResis
        self.id_resi = {}
        for hcovid, v in self.pob_act.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id] = resi
            except KeyError:
                pass
        self.id_resi_tot = c.defaultdict(set)
        for hcovid, v in self.pob_tot.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            if hash_a in self.u11:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id].add(resi)
        self.id_resi_tot.default_factory = None

    def get_edats(self):
        """."""
        sql = """
            SELECT
                id_cip_sec,
                (year(data_ext) - year(usua_data_naixement))
                - (date_format(data_ext, '%m%d')
                < date_format(usua_data_naixement, '%m%d'))
            FROM assignada, nodrizas.dextraccio
            """
        self.edats = {id: "ADU" if edat > 14 else "PED"
                      for (id, edat) in u.getAll(sql, "import")
                      if id in self.id_resi_tot}

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "SELECT id_cip_sec, up, ubainf FROM assignada_tot WHERE ates = 1"
        for id, up, uba in u.getAll(sql, "nodrizas"):
            if id in self.id_resi:
                resi = self.id_resi[id]
                grup = self.edats[id]
                for ind in ("GESTINF05", "GESTINF06"):
                    # self.recomptes[ind][(up, uba, "UBA", grup)]["DEN"] += 1
                    self.recomptes[ind][(resi, up, uba, "UBA", grup)]["DEN"] += 1  # noqa
                self.poblacio[id] = (up, uba)

    def get_centres(self):
        """."""
        sql = "SELECT scs_codi, tip_eap, ics_codi FROM cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_visites(self):
        """."""
        sql = """
            SELECT id_cip_sec, visi_up, visi_col_prov_resp
            FROM visites1, nodrizas.dextraccio
            WHERE
                visi_col_prov_resp like '3%' AND
                s_espe_codi_especialitat not in ('EXTRA', '10102') AND
                visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') AND
                visi_situacio_visita = 'R' AND
                visi_data_visita between
                    adddate(data_ext, interval -1 year) AND data_ext
            """
        for id, up, col in u.getAll(sql, "import"):
            if up in self.centres and id in self.edats:
                grup = self.edats[id]
                tipus = self.centres[up][0]
                go = (tipus == "M",
                      (grup == "ADU" and tipus == "A"),
                      (grup == "PED" and tipus == "N"))
                if any(go) and id in self.id_resi:  # QcResi
                    resi = self.id_resi[id]  # QcResi
                    for ind in ("GESTINF03", "GESTINF04"):
                        self.recomptes[ind][(resi,
                                             up, col, "COL", grup)]["DEN"] += 1
                # if any(go) and id in self.id_resi_tot:
                #     for resi in list(self.id_resi_tot[id]):
                #         for ind in ("GESTINF03_TOT", "GESTINF04_TOT"):
                #             self.recomptes[ind][(resi,
                #                                  up, col, "COL", grup)]["DEN"] += 1  # noqa

    def get_usu_to_col(self):
        """."""
        sql = "SELECT codi_sector, ide_usuari, ide_numcol \
               FROM cat_pritb992 \
               WHERE ide_numcol <> ''"
        self.usu_to_col = {row[:2]: row[2] for row in u.getAll(sql, "import")}

    def get_motius(self):
        """."""
        sql = """
            SELECT sym_name
            FROM icsap.klx_master_symbol
            WHERE
                dim_index = 4 AND (
                sym_index IN (
                    SELECT sym_index
                    FROM icsap.klx_parent_child
                    WHERE
                        dim_index = 4 AND
                        parent_index IN (
                            SELECT sym_index
                            FROM icsap.klx_master_symbol
                            WHERE
                                dim_index = 4 AND
                                sym_name IN ('PMOTIUPRPE', 'PMOTIUTEL')
                            )
                    ) OR
                sym_name = 'PALPED')
            """
        self.motius = {motiu: "PED" for motiu, in u.getAll(sql, "khalix")}

    def _get_motiu(self, motiu, grup):
        """."""
        motiu = CONVERSIO.get(motiu, motiu)
        if motiu == "A":
            motiu = "PAL{}".format(grup)
        else:
            motiu = "P{}".format(motiu.replace("_", ""))
        if motiu not in self.motius:
            self.motius[motiu] = "ADU"
        return motiu

    def get_aguda(self):
        """."""
        dext, = u.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        first = (dext - r.relativedelta(months=1)).replace(day=1)
        sql = """
            SELECT
                id_cip_sec, codi_sector, cesp_up, cesp_usu_alta,
                cesp_cod_mce,
                cesp_estat,
                cesp_data_alta
            FROM
                aguda, nodrizas.dextraccio
            WHERE
                cesp_data_alta between
                    adddate(data_ext, interval -1 year) and data_ext
            UNION
            SELECT
                id_cip_sec, codi_sector, pi_up, pi_usu_alta,
                upper(replace(pi_anagrama, ' ', '')),
                if(pi_ser_derivat = '', 'I', 'M'),
                pi_data_alta
            FROM
                ares, nodrizas.dextraccio
            WHERE
                pi_anagrama not in ('', 'TAP_A') and
                pi_data_alta between
                    adddate(data_ext, interval -1 year) and data_ext
            """
        for id, sec, up, usu, motiu, estat, dat in u.getAll(sql, "import"):
            if id in self.edats:
                grup = self.edats[id]
                motiu = self._get_motiu(motiu, grup)
                motiu_tip = self.motius[motiu]
                if grup == motiu_tip:
                    if id in self.id_resi:
                        resi = self.id_resi[id]
                        if id in self.poblacio:
                            up_pac, uba = self.poblacio[id]
                            if up == up_pac:
                                key = (resi, up, uba, "UBA", grup)
                                self.recomptes["GESTINF05"][key][motiu] += 1
                                if estat == "I":
                                    self.recomptes["GESTINF06"][key][motiu] += 1  # noqa
                                if dat >= first:
                                    key = ("B" + dat.strftime("%y%m"), resi, motiu)  # noqa
                                    self.mensual[("GESTINF05",) + key] += 1
                                    if estat == "I":
                                        self.mensual[("GESTINF06",) + key] += 1
                        col = self.usu_to_col.get((sec, usu))
                        key = (resi, up, col, "COL", grup)
                        if key in self.recomptes["GESTINF03"]:
                            self.recomptes["GESTINF03"][key][motiu] += 1
                            if estat == "I":
                                self.recomptes["GESTINF04"][key][motiu] += 1
                            if dat >= first:
                                key = ("B" + dat.strftime("%y%m"), resi, motiu)
                                self.mensual[("GESTINF03",) + key] += 1
                                if estat == "I":
                                    self.mensual[("GESTINF04",) + key] += 1
                    # if id in self.id_resi_tot:
                    #     for resi in list(self.id_resi_tot[id]):

    def get_master(self):
        """."""
        self.master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes.items():
            for (resi, up, prof, tip, grup), minidades in dades.items():
                den = minidades["DEN"]
                for motiu, motiu_tip in self.motius.items():
                    if motiu_tip == grup:
                        key = (resi, ind, up, prof, tip, grup, motiu)
                        self.master[key]["DEN"] = den
                        self.master[key]["NUM"] = minidades[motiu]

    def get_derivats(self):
        """."""
        for (resi, ind, up, prof, tip, grup, motiu), dades in self.master.items():  # noqa
            if ind == "GESTINF03":
                key = (resi, "GESTINF02", up, prof, tip, "MIX", "TIPMOTIU")
                self.master[key]["DEN"] += dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
            if ind in ("GESTINF03", "GESTINF04"):
                key = (resi, "GESTINF07", up, prof, tip, grup, motiu)
                analysis = "DEN" if ind == "GESTINF03" else "NUM"
                self.master[key][analysis] = dades["NUM"]

    def export_master(self):
        """."""
        u.createTable(MASTER_TB, "(resi varchar(13), \
                                   ind varchar(10), up varchar(5), \
                                   prof varchar(10), tip varchar(3), \
                                   grup varchar(3), motiu varchar(10), \
                                   num int, den int)",
                      DATABASE, rm=True)
        upload = [k + (v["NUM"], v["DEN"]) for (k, v) in self.master.items()]
        u.listToTable(upload, MASTER_TB, DATABASE)

    def get_col_to_uba(self):
        """Per passar dades individuals a khalix dels indicadors de NUMCOL."""
        self.col_to_uba = c.defaultdict(set)
        sql = "SELECT ide_numcol, up, uab \
               FROM cat_professionals \
               WHERE tipus = 'I'"
        for col, up, uba in u.getAll(sql, "import"):
            self.col_to_uba[(up, col)].add(uba)

    def get_ubas(self):
        """Nom�s pujarem UBAs amb EQA."""
        self.ubas = c.defaultdict(set)
        for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
            sql = "SELECT up, uba FROM mst_ubas WHERE tipus = 'I'"
            for key in u.getAll(sql, db):
                self.ubas[grup].add(key)
                self.ubas["MIX"].add(key)

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (resi, ind, _up, _prof, _tip, _grup, motiu), dades in self.master.items():  # noqa
            # br = self.centres[up][1]
            for analisi in ("NUM", "DEN"):
                self.khalix[(resi, ind, motiu, analisi)] += dades[analisi]
            # if tip == "COL":
            #     ubas = self.col_to_uba[(up, prof)]
            # else:
            #     ubas = (prof,)
            # for uba in ubas:
            #     if (up, uba) in self.ubas[grup]:
            #         ent = "{}I{}".format(br, uba)
            #         for analisi in ("NUM", "DEN"):
            #             self.khalix[(ent, ind, motiu, analisi)] += dades[analisi]  # noqa

    def get_taxes(self):
        """Hace GESTINF001 a partir de GESTINF005"""
        for (ent, ind, motiu, analisi), n in self.khalix.items():
            if ind == "GESTINF05" and analisi == "DEN" and motiu in TAXES:
                taxa = 1000 * self.khalix[(ent, ind, motiu, "NUM")] / float(n)
                meta = TAXES[motiu]
                good = taxa >= meta
                self.khalix[(ent, "GESTINF01", motiu, "DEN")] = 1
                self.khalix[(ent, "GESTINF01", motiu, "NUM")] = 1 * good

    def export_khalix(self):
        """."""
        cols = """(
                ent varchar(13),
                ind varchar(10),
                motiu varchar(10),
                analisi varchar(3),
                valor int
                )
            """
        u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload,     # (ent, "GESTINF01", motiu, "DEN", v)
                      KHALIX_TB,  # "exp_khalix_gida_resi"
                      DATABASE)
        file = KHALIX_UP          # GIDA_resi
        # for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
        cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

        cat_cond = "WHERE a.ent = b.resi_cod and b.sisap_class = 1"
        cat_cond = " ".join((cataleg, cat_cond))
        cat_cond = """exists (select 1 from {cat_cond})
        """.format(cat_cond=cat_cond)

        sql = """
            SELECT
                ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
                analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N', sum(valor) as val
            FROM
                {db}.{tb} a
            WHERE {cat_cond}
            GROUP BY
                ind, concat('R', replace(ent,'-','_')), analisi
            HAVING
                sum(valor) > 0
        """.format(db=DATABASE, tb=KHALIX_TB, cat_cond=cat_cond)
        u.exportKhalix(sql, file)

    def export_mensual(self):
        """."""
        cols = """(
                ind varchar(10),
                periode varchar(10),
                resi varchar(13),
                motiu varchar(10),
                valor int
                )
            """
        u.createTable(CORTES_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.mensual.items()]
        u.listToTable(upload, CORTES_TB, DATABASE)

        sql = """
                SELECT
                    ind, periode, resi, 'NUM', motiu,
                    'NOIMP', 'DIM6SET', 'N', valor
                FROM
                    {}.{} -- INNER JOIN
                    -- nodrizas.cat_centres
                    --    ON up = scs_codi
              """.format(DATABASE, CORTES_TB)
        u.exportKhalix(sql, CORTES_UP)

    def export_qc(self):
        """."""
        indicadors = ("GESTINF05", "GESTINF06")
        dext, = u.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sqls = []
        #  en la sql, cuando 'ent' es una residencia sera mas larga que 5
        #  el codigo original lo usaba para distinguir las UBAS de khalix
        for x, y in ((pactual, 'ANUAL'), (pactual, 'ACTUAL'), (pprevi, 'PREVI')):  # noqa
            sqls.append("""
                        SELECT
                            ind, '{}',
                            ent, 'DEN', '{}',
                            'TIPPROF4', sum(valor)
                        FROM {}
                        WHERE
                            ind in {} AND
                            -- length(ent) = 5 AND
                            analisi = 'DEN' AND
                            motiu in ('PALADU', 'PALPED')
                        GROUP BY
                            ind, ent
                        """.format(x, y, KHALIX_TB, indicadors))
        sqls.append("""
                    SELECT
                        ind, '{}',
                        ent, 'NUM', 'ANUAL',
                        'TIPPROF4', sum(valor)
                    FROM {}
                    WHERE
                        ind in {} AND
                        -- length(ent) = 5 AND
                        analisi = 'NUM'
                    GROUP BY
                        ind, ent
                    """.format(pactual, KHALIX_TB, indicadors))
        # aqui cambio ics_codi (br) por resi
        # !! habra que transformar a codi kalix de la residencia "-": "_"
        sqls.append("""
                    SELECT
                        ind, replace(periode, 'B', 'A'),
                        resi, 'NUM', if(replace(periode, 'B', 'A') = '{}',
                                            'ACTUAL', 'PREVI'),
                        'TIPPROF4', sum(valor)
                    FROM
                        {} a -- INNER JOIN
                        -- nodrizas.cat_centres b
                        --    on a.up = b.scs_codi
                    WHERE
                        ind in {}
                    GROUP BY
                        ind, periode, resi
                    """.format(pactual, CORTES_TB, indicadors))
        dades = []
        for sql in sqls:
            dades.extend([("Q" + row[0],) + row[1:]
                          for row in u.getAll(sql, DATABASE)])
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(QC_TB, "({})".format(", ".join(cols)), DATABASE, rm=True)
        u.listToTable(dades, QC_TB, DATABASE)


if __name__ == "__main__":
    BASE_DB = ("p2262", "import")
    with t.Database(*BASE_DB) as base:
        base.execute("""create database if not exists {resis}
                     """.format(resis=DATABASE))
    GIDA()
