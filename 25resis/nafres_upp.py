from datetime import datetime
import collections as c
import hashlib as h

import sisapUtils as u
import sisaptools as t
from collections import defaultdict, Counter

TEST = False
SUFIX = "_resi"

file_name = "NAFRESUPP{}".format(SUFIX)
nod = "nodrizas"
imp = "import"
DATABASE = "resis"

#  nafres = 833, 834
num_agrs = {"upp": ('91', '92'),
            "barthel": ('91', '92'),  # era "atdom"
            "nafres": ('833', '834'),
            "uisq": ('885', '886'),  # juntar con 'peu'
            "peu": ('887', '888'),
            "urinaria": ('454', '454'),  # en eqa_criteris solo esta las prevalentes
            "demencia": ('86', '86'),
            "maltracte": ('545', '545'),
            }
# agr_atdom = 45


ind_opt = {
    "upp": {"prev": "RES029A", "inc": "RES030A"},
    "barthel": {"prev": "RES029A1", "inc": "RES030A1"},  # era "atdom"
    "nafres": {"prev": "RES031A", "inc": "RES032A"},
    "uisq": {"prev": "RES033A", "inc": "RES034A"},  # juntar con 'peu'
    "peu": {"prev": "RES033A1", "inc": "RES034A1"},
    "urinaria": {"prev": "RES022A", "inc": "RES038A"},
    "demencia": {"prev": "RES020A"},
    "maltracte": {"prev": "RES054A"},
    }


class Qcresis(object):
    """."""

    def __init__(self):
        """."""

        ts = init = datetime.now()
        self.get_pob()
        print(datetime.now() - ts)
        print("pob_act: {}".format(len(self.pob_act)))
        print("pob_tot: {}".format(len(self.pob_tot)))
        ts = datetime.now()
        self.get_hcovid_map()
        print(datetime.now() - ts)
        print("hcovid_hash_map: {}".format(len(self.hcovid_hash_map)))
        print("hashes: {}".format(len(self.hashes)))
        ts = datetime.now()
        self.get_idcipsec_map()
        print(datetime.now() - ts)
        print("u11: {}".format(len(self.u11)))
        ts = datetime.now()
        self.get_id_resi()
        for k in [k for k in self.id_resi_tot][:3]:
            print(k, self.id_resi_tot[k])
        print("id_resi: {}".format((len(self.id_resi), len(self.id_resi_tot))))
        print(datetime.now() - ts)
        ts = datetime.now()
        self.get_pob_barthel()
        print("id_resi_barthel: {}".format((len(self.id_resi_barthel), len(self.id_resi_barthel_tot))))  # noqa
        print(ts - init)

    def get_pob(self):
        """
        Gets residents from res_master.
        - {pob_act}  # hcovid: (attributes)  # actual residents
        - {pob_tot}  # hcovid: (attributes)  # total residents since 2020-03-01
        """
        db = "redics"
        sch = "data"
        tb = "sisap_covid_res_master"
        cols = ["hash", "actual", "residencia", "entrada", "motiu", "sortida",
                "barthel_dat", "barthel_val", "pfeifer_dat", "pfeiffer_val",
                "situacio", "exitus_data", "vis_ultima",
                ]
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.pob_tot = {}
        self.pob_act = {}
        with t.Database(db, sch) as conn:
            for row in conn.get_all(sql):
                hcovid, actual = row[0], row[1]
                self.pob_tot[hcovid] = row[2:]
                if actual == 1:
                    self.pob_act[hcovid] = row[2:]

    def get_hash(self, cip):
        """Returns hcovid from cip."""
        return h.sha1(cip).hexdigest().upper()

    def get_hcovid_map(self):
        """
        Gets hash_d from hcovid.
        - {hcovid_hash_map} # hcovid: hash_a
        - set(hashes)  # set([hash_a, ...])
        !!! hay unos 67/62k que son hash_a que no ligan con u11.hash_d
        """

        LIMIT = 1000000
        limit = " where rownum <{}".format(LIMIT)
        db = 'redics'
        sch = 'pdp'
        tb = "pdptb101{}".format(limit if TEST else '')
        # conn = t.Database(db, sch)
        sql = 'select usua_cip, usua_cip_cod from {} '.format(tb)
        self.hcovid_hash_map = {}
        self.hashes = set()
        with t.Database(db, sch) as conn:
            for cip, hash_a in conn.get_all(sql):
                hcovid = self.get_hash(cip)
                if hcovid in self.pob_tot:
                    self.hashes.add(hash_a)
                    self.hcovid_hash_map[hcovid] = hash_a

    def get_idcipsec_map(self):
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        db = "p2262"
        sch = "import"
        tb = "u11_all"  # cambio u11 por u11_all
        cols = ["id_cip_sec", "hash_a", "hash_d"]  # cambio hash_d por hash_a
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.u11 = {}
        self.ids = {}
        with t.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d in conn.get_all(sql):
                if hash_a in self.hashes:
                    self.ids[id_cip_sec] = True
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d}

    def get_id_resi(self):
        self.id_resi = {}
        for hcovid, v in self.pob_act.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id] = resi
            except KeyError:
                continue
        self.id_resi_tot = c.defaultdict(set)
        for hcovid, v in self.pob_tot.items():
            resi = v[0]
            hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
            if hash_a in self.u11:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id].add(resi)
        self.id_resi_tot.default_factory = None

    def get_pob_barthel(self):
        self.id_resi_barthel = {}
        for hcovid, v in self.pob_act.items():
            resi = v[0]
            barthel = v[5]
            if barthel >= 40:
                hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
                try:
                    id = self.u11[hash_a]['id_cip_sec']
                    self.id_resi_barthel[id] = resi
                except KeyError:
                    pass
        self.id_resi_barthel_tot = c.defaultdict(set)
        for hcovid, v in self.pob_tot.items():
            resi = v[0]
            barthel = v[5]
            if barthel >= 40:
                hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
                if hash_a in self.u11:
                    id = self.u11[hash_a]['id_cip_sec']
                    self.id_resi_barthel_tot[id].add(resi)
        self.id_resi_barthel_tot.default_factory = None


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql = "select data_ext from {}.dextraccio;".format(nod)
    return u.getOne(sql, nod)[0]


def get_pob_agr(agr):
    """
    Selecciona los pacientes con un determinador agrupador (agr).
    Devuelve un set de ids.
    """
    sql = "select id_cip_sec from {}.eqa_problemes where ps = {}".format(nod, agr)  # noqa
    return {id for (id, ) in u.getAll(sql, nod)}


def get_id_agr_prev_inc(agrs, current_date):
    """
    Selecciona los pacientes que tengan alguno de los agrupadores en agrs.
    Se guardan dos valores, la prevalencia, que son los pacientes que presentan
    el agrupador.
    y la incidencia, que son aquellos pacientes a los que se les ha
    diagnosticado el agr en los ultimos 12 meses.
    Se devuelve un diccionario con el formato:
        {agr: {"prev": set(ids), "inc": set(ids)}
    """
    id_agrs = defaultdict(lambda: defaultdict(set))
    sql = """
        SELECT id_cip_sec, ps, dde
        FROM {}.eqa_problemes where ps in ({})
        """.format(nod, ", ".join(agrs))
    print(sql)

    for id, agr, dde in u.getAll(sql, nod):
        agr = str(agr)
        id_agrs[agr]["prev"].add(id)
        if u.monthsBetween(dde, current_date) < 12:
            id_agrs[agr]["inc"].add(id)
    return id_agrs


def get_indicador(codi, num, pob_ids=None):
    """
    Construye el indicador a nivel de up.
    Si pob_ids es dada (un diccionario de ids: up),
    el denominador se construye con los ids que estan dentro de esta poblacion.
    En caso contrario, es el total de personas en la up.
    El numerador se obtiene a partir de num, otro set de ids.
       Devuelve una lista de filas (tuples).
    """
    indicador = defaultdict(Counter)
    if pob_ids is None:
        sql = "select id_cip_sec, up from {}.assignada_tot".format(nod)
        pob_ids = {id: up for id, up in u.getAll(sql, nod)}

    for id, up in pob_ids.items():
        indicador[up]["DEN"] += 1
        if id in num:
            indicador[up]["NUM"] += 1

    return [(up, codi, indicador[up]["NUM"], indicador[up]["DEN"])
            for up in indicador]


def export_table(table, columns, db, rows):
    u.createTable(table, columns, db, rm=True)
    u.listToTable(rows, table, db)


def push_to_khalix(db, taula, taula_centres, file_name, khalix=False):
    """."""
    cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

    cat_cond = "WHERE a.up = b.resi_cod and b.sisap_class = 1"
    cat_cond = " ".join((cataleg, cat_cond))
    cat_cond = """exists (select 1 from {cat_cond})
    """.format(cat_cond=cat_cond)

    query_template_string = """
        SELECT
            indicador, concat('A', 'periodo'),
            concat('R', replace(up,'-','_')) as resi, {strg},
            'NOCAT', 'NOIMP', 'POBACTUAL', 'N', {var}
        FROM
            {db}.{taula} a
        WHERE {cat_cond}
            -- INNER JOIN
            -- nodrizas.{taula_centres} b
            --    on a.up = scs_codi
            and a.{var} != 0
    """
    query_string1 = query_template_string.format(strg="'NUM'",
                                                 var="numerador",
                                                 db=db,
                                                 taula=taula,
                                                 cat_cond=cat_cond,
                                                 taula_centres=taula_centres)

    query_string2 = query_template_string.format(strg="'DEN'",
                                                 var="denominador",
                                                 db=db,
                                                 taula=taula,
                                                 cat_cond=cat_cond,
                                                 taula_centres=taula_centres)

    query_string = query_string1 + " union " + query_string2

    print(query_string)
    if khalix:
        u.exportKhalix(query_string, file_name)


if __name__ == '__main__':
    Resi = Qcresis()
    u.printTime('inici')
    current_date = get_date_dextraccio()
    u.printTime(current_date)
    # Se crea la tabla
    table_name = "exp_khalix_nafres_upp{}".format(SUFIX)
    table_columns = "(up varchar(13), indicador varchar(15), numerador int, denominador int)"  # noqa
    u.createTable(table_name, table_columns, DATABASE, rm=True)

    # Se genera la lista de agrs para el numerador
    # y se pasa a la funcion que nos dara los ids para cada uno de ellos
    # (prev e inc)
    agrs_list = list(el for tuplee in set(num_agrs.values()) for el in tuplee)
    ids_agr = get_id_agr_prev_inc(agrs_list, current_date)
    u.printTime("ids_agr")

    # Se obtiene la poblacion atdom
    # pob_atdom = get_pob_agr(agr_atdom)
    # u.printTime("atdom_pob")

    for key in ind_opt:
        print(key)
        # Para cada key (upp, atdom, nafres):

        # Diccionario de opciones para determinar si el denominador es:
        #   pob_atdom o el total de gente de la up (opcion None)
        trad_dict = {"upp": Resi.id_resi,
                     "barthel": Resi.id_resi_barthel,
                     "nafres": Resi.id_resi,
                     "uisq": Resi.id_resi,  # juntar con 'peu'
                     "peu": Resi.id_resi,
                     "urinaria": Resi.id_resi,
                     "demencia": Resi.id_resi,
                     "maltracte": Resi.id_resi,
                     }
        current_pob = trad_dict[key]

        # El agrupador que va a determinar el numerador,
        # para cada key, segun el tipo
        num_by_type = {"inc": num_agrs[key], "prev": (num_agrs[key][0],)}

        for type_num, codi in ind_opt[key].iteritems():
            # Una vez obtenido el tipo, selecciona los agrupadores (tuple)
            # que van a dar el numerador
            agrs_for_num = num_by_type[type_num]

            # Obtiene los ids del numerador
            # haciendo una union con los sets de ids
            # correspondientes al agr y al tipo
            num = set.union(*[ids_agr[num_agr][type_num]
                              for num_agr in agrs_for_num])

            # Se obtienen las rows por up para cada tipo de indicador
            # (prev o inc) dentro de una key (upp, upp_barthel, nafres)
            rows = get_indicador(codi, num, current_pob)

            u.listToTable(rows, table_name, DATABASE)
            u.printTime("Insert into the table {}".format(codi))

    push_to_khalix(db=DATABASE, taula=table_name, taula_centres="cat_centres",
                   file_name=file_name, khalix=True)
