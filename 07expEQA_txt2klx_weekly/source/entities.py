import sisapUtils as u
from time import strftime

file = 'Crea_Simbolo.lvpro'
fileTime = file.replace('.',strftime("_%Y%m%d_%H%M%S."))
command = 'Crea_Simbolo.bat && copy {0} .\\Crea_Simbolo_log\\{1}'.format(file,fileTime)

existents = []
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'B%'"
for eap, in u.getAll(sql,'khalix'):
    existents += [eap]
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'PB%'"
for eap, in u.getAll(sql,'khalix'):
    existents += [eap]
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'R%'"
for eap, in u.getAll(sql,'khalix'):
    existents += [eap]
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'E%'"
for eap, in u.getAll(sql,'khalix'):
    existents += [eap]        


saps = []
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'SAP%'"
for sap, in u.getAll(sql,'khalix'):
    saps += [sap]
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'PSAP%'"
for sap, in u.getAll(sql,'khalix'):
    saps += [sap]
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'RSAP%'"
for sap, in u.getAll(sql,'khalix'):
    saps += [sap]
sql = "select sym_name from klx_master_symbol where dim_index=2 and sym_name like 'ESAP%'"
for sap, in u.getAll(sql,'khalix'):
    saps += [sap]
   
nous, nousRBR = [], {} 
sql = "select distinct ics_codi,ics_desc,sap_codi,sap_desc,amb_codi,if(medea='','2U',medea),if(medea='',1,0) from cat_centres"
for br,br_d,sap,sap_d,amb,tipus,prov in u.getAll(sql,'nodrizas'):
    if br not in existents:
        nous += [[br,br_d,'SAP'+sap,sap_d,'AMB'+amb,'TIPUS'+tipus,prov]]
        nous += [['P'+br,br_d,'PSAP'+sap,sap_d,'PAMB'+amb,'TIPUS'+tipus,prov]]
    if 'R' + br not in existents:
        nousRBR['R' + br] =  br_d, 'RSAP'+sap
    if 'E' + br not in existents:
        nousRBR['E' + br] =  br_d, 'ESAP'+sap

sql = """
    select distinct
        concat('R', a.resi_cod)
        , a.resi_des as descripcio_sisap
        , b.sap_codi as sap 
        , b.sap_desc as sap_desc 
        , b.amb_codi as ambit 
        , concat('R', b.ics_codi)
        , 0
    from
        import.cat_sisap_map_residencies a inner join
        cat_centres b
            on a.eap_up=b.scs_codi
        where
            a.sisap_class = 1
    """
for br,br_d,sap,sap_d,amb,tipus,prov in u.getAll(sql,'nodrizas'):
    if br not in existents:
        nous += [[br,br_d,'RSAP'+sap,sap_d,'RAMB'+amb,tipus,prov]]
"""
sql = "select concat('E', codi_escola), nom_escola,sap_codi,sap_desc, amb_codi, concat('E',ics_codi), 0 from nodrizas.cat_escoles a inner join cat_centres b on a.up=b.scs_codi"
for br,br_d,sap,sap_d,amb,tipus,prov in u.getAll(sql,'nodrizas'):
    if br not in existents:
        nous += [[br,br_d,'ESAP'+sap,sap_d,'EAMB'+amb,tipus,prov]]
"""
if len(nous) > 0:
    text = ''
    with open(u.tempFolder + file,'wb') as f:
        f.write('MAINTENANCE ON\r\n')
        for br,br_d,sap,sap_d,amb,tipus,prov in nous:
            if sap not in saps:
                f.write('CREATE SYMBOL ENTITIES {} "{}" Standard 203 0\r\n'.format(sap,sap_d))
                f.write('SET SYMBOL ENTITIES {} DESCRIPTION ES TO "{}"\r\n'.format(sap,sap_d))
                f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(sap,amb))
                saps.append(sap)
                text += sap + '\r\n'
            if br[0] in ('R','E'):
                if tipus in nousRBR:
                    f.write('CREATE SYMBOL ENTITIES {} "{}" Standard 203 0\r\n'.format(tipus,nousRBR[tipus][0]))
                    f.write('SET SYMBOL ENTITIES {} DESCRIPTION ES TO "{}"\r\n'.format(tipus,nousRBR[tipus][0]))
                    f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(tipus,nousRBR[tipus][1]))
                    del nousRBR[tipus]
            f.write('CREATE SYMBOL ENTITIES {} "{}" Standard 203 0\r\n'.format(br,br_d))
            f.write('SET SYMBOL ENTITIES {} DESCRIPTION ES TO "{}"\r\n'.format(br,br_d))
            if br[0] in ('R','E'):
                f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(br,tipus))
            else:    
                f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(br,sap))
            if br[0] == 'B':
                f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(br,tipus))
                f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(br,tipus+'2'))
            if br[0] == 'P':
                for c, des in (('M', 'Medicina'), ('I', 'Infermeria'), ('MP', 'Pediatria'), ('IP', 'Infermeria pediatria'), ('A', 'Antics')):
                    cod = br+c
                    f.write('CREATE SYMBOL ENTITIES {} "{}" Standard 203 0\r\n'.format(cod,des))
                    f.write('SET SYMBOL ENTITIES {} DESCRIPTION ES TO "{}"\r\n'.format(cod,des))
                    f.write('ASSIGN SYMBOL ENTITIES {} PARENT {} "+"\r\n'.format(cod,br))
            text += br + (' (ojo MEDEA provisional)' if prov == 1 else '') + '\r\n'
        f.write('MAINTENANCE OFF\r\n')
    
    
    u.sshPutFile(file,'procesos','khalix')
    status,stdout,stderr = u.sshExecuteCommand(command,'procesos','khalix')
    u.sendSISAP('amercadecosta@gencat.cat','Nuevas entities creadas','Albert',text)