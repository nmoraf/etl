from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

indicadors=[]
resultat=[]
path="./dades_noesb/odn_edats.txt"
temp=tempFolder + "temp.txt"
db="odn"
desti="denominadors"
assignada="nodrizas.assignada_tot"
dextraccio="nodrizas.dextraccio"

with open(path, 'rb') as file:
    ed=csv.reader(file, delimiter='@', quotechar='|')
    for ind in ed:
        indicadors.append([ind[0],ind[1],int(ind[2]),int(ind[3])])

conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()
c.execute("select year(data_ext) from %s limit 1" % dextraccio)
ext,=c.fetchone()
ext=int(ext)
c.close()
c=conn.cursor()
c.execute("drop table if exists %s" % desti)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),data_naix date,edat int,ates int,institucionalitzat int,maca int)" % desti)
c.execute("select id_cip_sec,year(data_naix),edat,ates,institucionalitzat,maca,data_naix from %s" % assignada)
for i in xrange(c.rowcount):
    id,n,a,at,ins,maca,naix,=c.fetchone()
    n=int(n);a=int(a);
    for indicador in indicadors:
        edat= a if indicador[1]=="a" else ext-n
        if indicador[2] <= edat <= indicador[3]:
            ok=1
            if ok == 1:
                resultat.append([int(id),indicador[0],naix,a,int(at),int(ins),int(maca)])

with open('%s' % temp, 'wb') as fp:
    a = csv.writer(fp, delimiter='@')
    a.writerows(resultat)

c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp,desti))
conn.close()    
os.remove(temp)

print strftime("%Y-%m-%d %H:%M:%S")
