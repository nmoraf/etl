# -*- coding: latin1 -*-

"""
.
"""

import collections as c

import sisapUtils as u


codi_indicador = "EQA9109"


class INR(object):
    """."""

    def __init__(self):
        """."""
        self.get_avk()
        self.get_inr()
        self.get_extraccions()
        self.get_poblacio()
        self.get_resultat()
        self.export_tables()

    def get_avk(self):
        """."""
        sql = "select id_cip_sec, pres_orig from eqa_tractaments where farmac = 3"
        self.avk = {id: dat for (id, dat) in u.getAll(sql, "nodrizas")}

    def get_inr(self):
        """."""
        self.inr = c.defaultdict(set)
        sql = "select id_cip_sec, data_var from eqa_variables where agrupador = 195"
        for id, dat in u.getAll(sql, "nodrizas"):
            self.inr[id].add(dat)

    def get_extraccions(self):
        """."""
        self.pacients = set()
        self.no_complidors = set()
        sql = "select id_cip_sec, dtd_data \
               from odn508, nodrizas.dextraccio \
               where dtd_tra in ('EX', 'EXO') and \
                     dtd_cod_p < 50 and \
                     dtd_data between adddate(data_ext, interval -1 year) and data_ext"
        for id, dat in u.getAll(sql, "import"):
            if id in self.avk and self.avk[id] < dat:
                self.pacients.add(id)
                if not any([u.daysBetween(inr, dat) < 4 for inr in self.inr[id]]):
                    self.no_complidors.add(id)

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "select id_cip_sec, ates, institucionalitzat, maca from assignada_tot where edat >14"
        for id, ates, insti, maca  in u.getAll(sql, "nodrizas"):
            if id in self.pacients:
                self.poblacio[id] = ates, insti, maca

    def get_resultat(self):
        """."""
        self.resultats = []
        for id in self.poblacio:
            ates, insti, maca = self.poblacio[id][0], self.poblacio[id][1], self.poblacio[id][2]
            num = 0
            if id not in self.no_complidors:
                num = 1
            self.resultats.append((id, codi_indicador, ates, insti, maca, 1, num, 0))

    def export_tables(self):
        """."""
        db = "odn"
        tb = "odn_inr_indicador"
        u.createTable(tb, "(id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)", db, rm=True)
        u.listToTable(self.resultats, tb, db)

if __name__ == "__main__":
    INR()
