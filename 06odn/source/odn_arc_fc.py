# coding: iso-8859-1


from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


nod = "nodrizas"
db = "odn"

              
indicadors = {"EQA9114": 12,
                "EQA9115": 7}

table = "odn_arc_fc_indicadors"
                
class ARC(object):
    """."""
    
    def __init__(self):
        """."""
        printTime('Inici')
        self.get_date_dextraccio()
        self.get_arc()
        self.get_numerador()
        self.get_indicadors()
        self.export_data()
        printTime('Fi')
        
    def get_date_dextraccio(self):
        """"""
        sql="select data_ext from {0}.dextraccio".format(nod)
        self.current_date = getOne(sql,nod)[0]
        print self.current_date
    
    def get_arc(self):
        """Considerem ARC si variable=1501 o cod>1 o caod>0"""
        
        self.pac_arc = set()
        self.baixes = {}
        sql = "select id_cip_sec, baixa from odn_variables, dextraccio \
                        where agrupador=286 and valor=1501 and dat< date_add(data_ext, interval - 2 month) and \
                        (baixa=0 or baixa> date_add(date_add(data_ext, interval - 1 year), interval + 1 day))"
        for id, baixa in getAll(sql, nod):
            self.pac_arc.add(int(id))
            if baixa != 0:
                self.baixes[id] = baixa

        
    def get_numerador(self):
        """FC"""
        
        self.numerador = set()
        sql = "select id_cip_sec, dat from odn_variables where agrupador=310 and dat<>0"
        for id, dat in getAll(sql, nod):
            if monthsBetween(dat,self.current_date) < 12:
                self.numerador.add(int(id))
            else:
                if id in self.baixes:
                    baixa = self.baixes[id]
                    if monthsBetween(dat,baixa) < 12:
                        self.numerador.add(int(id))
                    
                
    def get_indicadors(self):
        """."""
        self.upload = []
        sql = "select id_cip_sec, year(data_naix), ates, institucionalitzat, maca from ped_assignada"
        for id, anys, ates, insti, maca in getAll(sql, nod):
            edat = self.current_date.year-anys
            for ind, ed in indicadors.items():
                if ed == edat:
                    den = 1 if int(id) in self.pac_arc else 0
                    num = 1 if id in self.numerador else 0
                    if den == 1:
                        self.upload.append([id, ind, ates, insti, maca, den, num, 0])
    
    def export_data(self):
        """Taula per a carregar"""
        columns="(id_cip_sec int,indicador varchar(10),ates int,institucionalitzat int,maca int,den int,num int,excl int)"
        createTable(table, columns, db, rm=True)
        listToTable(self.upload, table, db)
        
if __name__ == '__main__':
    ARC()       