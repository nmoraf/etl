from sisapUtils import createTable, getAll, listToTable


db = 'odn'
tb = 'mst_professionals'
tip = 'O'


createTable(tb, '(up varchar(5), dni varchar(20))', db, rm=True)
sql = "select distinct a.up, left(a.ide_dni, 8) \
       from cat_profvisual a \
       where a.visualitzacio = 1 \
       and exists (select 1 from cat_professionals b where a.ide_usuari = b.ide_usuari and b.tipus = '{}')".format(tip)
listToTable(getAll(sql, 'import'), tb, db)
