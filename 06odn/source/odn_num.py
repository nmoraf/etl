from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="odn"

resultat=[]
resultat2=[]

pathi="./dades_noesb/odn_relacio.txt"
criteris="nodrizas.eqa_criteris"
variables="nodrizas.odn_variables"
dext="nodrizas.dextraccio"

num="odn_numeradors1"
ntemp="num_temporal"
desti_n ="odn_numeradors"
origen ="odn_denominadors"
desti = "odn_indicadors"
relacio1="num_relacio1"
relacio="num_relacio"
denominadors="denominadors"

temp=tempFolder+"temp1.txt"
temp2=tempFolder+"temp2.txt"

conn = connect((db,'aux'))
c=conn.cursor()

c.execute("drop table if exists %s" % num)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,control int)" % num)


c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   taula = "%s_%s" % (desti,nom)
   c.execute("drop table if exists %s" % taula)
   c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int)" % (taula))
   c.execute("insert into %s select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s'" % (taula,origen,nom))
   c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca)" % taula)
 
c.execute("select distinct indicador from %s" % denominadors)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)
   c.execute("create table %s like %s" % (ori,denominadors))
   c.execute("insert into %s select * from %s where indicador='%s'" % (ori,denominadors,nom))
   c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca)" % ori)
 

with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      orig = "%s_%s" % (desti,ind)
      if tip== "n":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr))
         pres=c.fetchall()
         for i in pres:
            t,g= i[0],int(i[1])
            if t.startswith("odn"):
               if agr ==  '286':
                  c.execute("select id_cip_sec,count(*) from %s,%s where agrupador='%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month)) and baixa = 1 group by id_cip_sec" % (variables,dext,agr,tmin,tmax))
               else:
                  c.execute("select id_cip_sec,count(*) from %s,%s where agrupador='%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month)) group by id_cip_sec" % (variables,dext,agr,tmin,tmax))
               var=c.fetchall()
               for o in var:
                  cip,n =o[0],int(o[1])
                  tf="%s_%s" % (desti,ind)
                  c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                  for i in xrange(c.rowcount):
                     id,indic,at,ins,maca,ctl,=c.fetchone()
                     resultat.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])

                    
with open('%s' % temp, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat)
try:
   if os.stat(temp).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp,num))
      os.remove(temp)
except OSError:
   os.remove(temp)   
   
 
c.execute("drop table if exists %s" % ntemp)
c.execute("create table %s like %s" % (ntemp,num))
c.execute("insert into %s select id_cip_sec, indicador,ates,institucionalitzat,maca,sum(control) as control from %s group by id_cip_sec,indicador" % (ntemp,num))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,control)" % ntemp)
c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(1) not null default'',agrupador varchar(8) not null default'',ctl int)" % relacio1)

with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if tip== "n":
         c.execute("insert into %s values('%s','%s','%s','%s')" % (relacio1,ind,tip,agr,f))
c.execute("drop table if exists %s" % relacio)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(1) not null default'',ctl int)" % relacio)
c.execute("insert into %s (select indicador, tipus, max(ctl) as ctl from %s group by indicador,tipus)" % (relacio,relacio1))
c.execute("drop table if exists %s" % relacio1)

c.execute("drop table if exists %s" % desti_n)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int)" % desti_n)
c.execute("select distinct indicador,ctl from %s" % relacio)
denom=c.fetchall()
for r in denom:
   indicador,max= r[0],int(r[1])
   c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s' and control>=%s" % (ntemp,indicador,max))
   for i4 in xrange(c.rowcount):
      id,indic,at,ins,maca,=c.fetchone()
      resultat2.append([int(id),indic,int(at),int(ins),int(maca)])   
      
     
with open('%s' % temp2, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat2)
try:
   if os.stat(temp2).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp2,desti_n))
      os.remove(temp2)
except OSError:
   os.remove(temp2)

c.execute("drop table if exists %s" % num)
c.execute("drop table if exists %s" % ntemp)
c.execute("drop table if exists %s" % relacio)

c.execute("select distinct indicador from %s" % denominadors)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")