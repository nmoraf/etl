from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
#Validation
validation=False
up_codi=("00380","00368","00378","00451")

alt = "odn"
nod = "nodrizas"
assig=nod+".assignada_tot"
odn=nod+".odn_variables"
peces="import.odn508"
#limit=2000
codi="EQA9110"
#Agrupador indicador de ARC
agr_ARC_dict={286:"=1501",
			  307:">1",
			  308:">0"}

#Molares definitvos
molars=(16,17, 26, 27, 36, 37, 46, 47)
necessaris = (16, 26, 36, 46)



#Funciones	

def get_inclosos():
    """."""
    sql = "select id_cip_sec from odn506 where dfd_cod_p in {} and dfd_p_a = 'P'".format(necessaris)
    inclosos = set([id for id, in getAll(sql, "import")])
    return inclosos

def get_nens_ARC(agr_ARC,valor):
	""" Selecciona los id_cip_seq de ninos que presentan un ARC segun el agrupador en la tabla odn_variables
		Devuelve un set de id_cip_seq
	"""
	sql = "select id_cip_sec from {0} where agrupador={1} and valor{2};".format(odn,agr_ARC,valor)
	arc_nens = set([int(id) for id, in getAll(sql, nod) if int(id) in inclosos])
	return arc_nens

def get_nens_SEG(molars):
	""" Selecciona los id_cip_seq de ninos con tto de segellat  en los molares definitivos segun la tabla import.odn508
		Devuelve un set de id_cip_seq
	"""
	sql="select id_cip_sec from {0} where dtd_tra='SEG' and dtd_cod_p in {1} and dtd_et <> 'P';".format(peces,molars)
	seg_molars_nens=set([int(id) for id, in getAll(sql, nod)])
	return seg_molars_nens

def get_rows_by_age (denominador, numerador):
	"""Adaptado de: sisap\08alt\source\prof_transaminases.py
	   De una orden sql que filtra a los ninos por edad, selecciona aquellos id que esten en el denominador y construye una row (un tuple) con columnas: id,
	   up, ates y result. Si este id esta en el numerador, el result tendra valor 1 y si no, tendra valor 0.
	   Devuelve una lista de row(tuples)
	"""
	sql = "select id_cip_sec, upOrigen, ates from {0} where edat>=6 and edat<=14 and ates=1;".format(assig)
	rows = []      
	for id, up,ates in getAll(sql, nod):
		id=int(id)
		if id in denominador:
			if id in numerador:
				rows.append((id, up,ates, 1))
			elif id not in numerador:
				rows.append((id, up,ates, 0))
	return rows

def get_uba_pacient_tables (denominador, numerador):
	"""Adaptado de: sisap\08alt\source\prof_od_inr.py
	"""
	uba_rows=[]
	
	sql = "select id_cip_sec, ates, institucionalitzat, maca from {} where edat>=6 and edat<=14".format(assig)
	   
	for id, ates, insti, maca in getAll(sql, nod):
		id=int(id)
		if id in denominador:
			num = 0
			if id in numerador:
				num = 1
			uba_rows.append((id, codi, ates, insti, maca, 1, num, 0))
	
	return uba_rows
	
def validation_by_up_pacient(up_tuple,table_name,table_columns):
	sql="select * from altres.exp_ecap_{0}_{1} where up in ({2})".format(codi,table_name,",".join(up_tuple))
	up_rows= [(row) for row in getAll(sql,alt)]
	export_table("exp_ecap_{0}_{1}".format(codi,table_name),
				table_columns,
				alt,
				up_rows)
	
	
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)
	
	
if __name__ == '__main__':
# comencem!
	printTime('inici')
	inclosos = get_inclosos()
	#ids de nens con ARC
	arc_nens=set()
	for agr_ARC, valor in agr_ARC_dict.items():
		arc_nens=arc_nens.union(get_nens_ARC(agr_ARC,valor))
		print(len(arc_nens))
		printTime('Get nens amb ARC amb agr {0}, valor {1} OK'.format(agr_ARC,valor))
	print(len(arc_nens))	
	#ids de nens con segellat en las muelas definitivas
	seg_molars_nens=get_nens_SEG(molars)
	printTime('Get nens amb SEG OK')
	
	#get lists of rows
	#rows=get_rows_by_age(arc_nens,seg_molars_nens)
	#printTime('Retrieving rows new table OK')
	#uba_car_columns="(id int, up varchar(15), ates int,numerador int)"
	#export_table("exp_ecap_{}_uba_car".format(codi),
	#			uba_car_columns,
	#			alt,
	#			rows)
				
	
	#conseguir las listas de filas para las dos tablas que se van a crear, la agrupadas por up y la de pacientes no incluidos
	uba_rows=get_uba_pacient_tables(arc_nens,seg_molars_nens)
	printTime('Retrieving rows new table OK')
	
	#exportar las tablas a la base de datos altres
	uba_columns="(id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)"
	export_table("odn_arc_indicador".format(codi),
				uba_columns,
				alt,
				uba_rows)
				
	
	if validation:
		validation_by_up_pacient(up_codi,"uba",uba_columns)
		validation_by_up_pacient(up_codi,"pacient",pacient_columns)
	