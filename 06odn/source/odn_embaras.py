from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="odn"
conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()

path="./dades_noesb/odn_relacio.txt"

ass_emb="nodrizas.ass_embaras"
criteris="nodrizas.eqa_criteris"
embaras="odn_embaras"
variables="nodrizas.odn_variables"
dext="nodrizas.dextraccio"
ind_embaras="embaras_indicador"
den="denominadors"

temps = 294


c.execute("drop table if exists %s" % embaras)
c.execute("create table %s like %s" % (embaras,ass_emb))
c.execute("insert into %s select a.* from %s a, nodrizas.dextraccio where temps <= %s and fi > adddate(data_ext, interval -1 year)" % (embaras,ass_emb, temps))
c.execute("alter table %s add index(id_cip_sec,inici,fi,risc)" % embaras)


with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,tipus,agr,f,tmin,tmax,vmin,vmax = i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]
      if tipus=="e":
         c.execute("alter table %s add column %s int" % (embaras,ind))
         c.execute("update %s set %s=0" % (embaras,ind))
      elif tipus=="num_e":
         c.execute("select min(taula) from %s where agrupador='%s' " % (criteris,agr))
         pres=c.fetchall()
         for a in pres:
            taula= a[0]
            if taula.startswith("odn"):
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and dat between inici and fi" % (embaras,variables,ind,agr))

c.execute("drop table if exists %s" % ind_embaras)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)" % ind_embaras)
c.execute("insert into %s select a.id_cip_sec,indicador,ates,institucionalitzat,maca,1 den,%s num, 0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s'" % (ind_embaras,ind,embaras,den,ind))
              
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")