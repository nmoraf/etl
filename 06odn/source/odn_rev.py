from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import string
import random
import operator

validation=False
up_codi=("00380",'00365')
alt="odn"
nod="nodrizas"
codi="EQA9113"
agrupador=309

def get_revision_odn(agrupador):
	"""Selecciona los pacientes que presenten el agrupador agr
	   Devuelve un set de ids
	"""
	sql="select id_cip_sec from {}.odn_variables where agrupador= {}".format(nod,agrupador)
	return {id for (id,) in getAll(sql,nod)}

def get_uba_pacient_tables (numerador):
	"""Adaptado de: sisap\08alt\source\prof_od_inr.py
	"""
	uba_rows=[]
	 
	
	sql = "select id_cip_sec, ates, institucionalitzat, maca from ped_assignada, dextraccio where year(data_naix) = year(data_ext) - 2"
	   
	for id, ates, insti, maca in getAll(sql, nod):
		num = 0
		if id in numerador:
			num = 1
		uba_rows.append((id, codi, ates, insti, maca, 1, num, 0))
	
	return uba_rows
	
def validation_by_up_pacient(codi,up_tuple,table_name,table_columns):
	sql="select * from altres.exp_ecap_{0}_{1} where up in ({2})".format(codi,table_name,",".join(up_tuple))
	up_rows= [(row) for row in getAll(sql,alt)]
	export_table("exp_ecap_{0}_{1}".format(codi,table_name),
				table_columns,
				alt,
				up_rows)
	
	
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)
	
if __name__ == '__main__':
    printTime('inici')
    
    rev_nens=get_revision_odn(agrupador)
    printTime('Revision odn')
    
    uba_rows=get_uba_pacient_tables(rev_nens)
    printTime('Retrieving rows new table OK')
    
    uba_columns="(id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)"
    export_table("odn_rev_indicador",
					uba_columns,
					alt,
					uba_rows)

    if validation:
		validation_by_up_pacient(codi,up_codi,"uba",uba_columns)
		validation_by_up_pacient(codi, up_codi,"pacient",pacient_columns)    