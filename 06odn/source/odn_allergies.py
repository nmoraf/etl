from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


nod="nodrizas"
imp="import"
list_source_dbs=[nod,imp]
validation=False

indicador="EQA9116"
table_name="odn_indicador_alergias"
db_out="odn"


class AllergiesNens(object):
    """Percentage of children between 0 and 14 years old, treated in the last 12 months with pharmacologic prescription, that have allergic medications registered."""

    def __init__(self):
        self.date_dextraccio= self.get_date_dextraccio()
        self.pacient_allergies=self.get_pacients_allergies()
        self.pacient_no_allergies=self.get_pacient_without_allergies()
        self.pacient_prescripcio=self.multiprocess_prescripcio()

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]
        
    def get_pacient_without_allergies(self):
        sql="select id_cip_sec from import.prealt2 where val_var= 'AL_NO_CON' AND val_hist = 1"
        return {id for (id,) in getAll(sql,imp)}

    def get_pacients_allergies(self):
        """Selection of those patients who are registered with drug allergies.
           The pacient_allergies attribute is defined as a set of these ids.
        """
        sql="select id_cip_sec from {0}.ram;".format(imp)
        return {id for (id,) in getAll(sql,nod)}
    
    def get_prescripcio_dates_by_pacient(self,taula):
        """Selection of those patients who have a drug prescription at the current date.
            Returns a set of identifiers of these patients."""

        sql="select id_cip_sec, ppfmc_pmc_data_ini, ppfmc_data_fi from {0} where left(ppfmc_pmc_amb_num_col, 1) = '2'".format(taula)
        pacient_prescripcio=set(id for id,ini,fin in getAll(sql,imp) if (monthsBetween(ini,self.date_dextraccio) < 12 or monthsBetween(fin,self.date_dextraccio) < 12 ))
        return pacient_prescripcio
    
    def multiprocess_prescripcio(self):
        """get_prescripcio_dates_by_pacient method is performed for each import.tractaments partition.
           Returns the union of all the sets that have resulted from each partition.
        """
        tables = getSubTables('tractaments')
        resul_by_taula = multiprocess(self.get_prescripcio_dates_by_pacient, tables)

        return set.union(*resul_by_taula)
        
    def get_indicador(self):
        """Adaptado de: sisap\08alt\source\prof_od_inr.py. Obtaining final table rows.
           From the ids of children between 0 and 14 years old, select those who are receiving prescription (their id is in self.pacient_prescripcio) and mark as numerator
           those who have drug allergies (their id is in self.pacient_allergies).
           Return rows list grouped by id.
        """
        table_rows=[]
        sql = "select id_cip_sec, ates, institucionalitzat,maca from {0}.assignada_tot where edat >= 15 and edat <=80 ;".format(nod)
        
        for id, ates, inst, maca in getAll(sql,nod):
            if id in self.pacient_prescripcio:
                denominador=1
                numerador=0
                if id in self.pacient_allergies or id in self.pacient_no_allergies:
                    numerador=1
                table_rows.append((id,indicador,ates,inst,maca,denominador,numerador,0))
        return table_rows
    
    def export_table(self,table,columns,db,rows):
        """Create a table from a list of rows in the specified db."""
        createTable(table, columns, db, rm=True)
        listToTable(rows, table, db)

if __name__=="__main__":

    eqa=AllergiesNens()
    printTime("Datos prescripcion")
    
    rows=eqa.get_indicador()
    printTime("Indicador")
    
    eqa.export_table("odn_indicador_alergias",
                     "(id_cip_sec int(11), indicador varchar(11), ates int, institucionalitzat int,maca int,den int,num int, exclos int)",
                     db_out,
                     rows)
