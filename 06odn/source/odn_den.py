from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

resultat=[]
resultat2=[]
resultat3=[]
resultat0=[]
resultat4=[]
resultat5=[]

pathi="./dades_noesb/odn_relacio.txt"
db="odn"
conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()

den="odn_denominadors1"
origen="denominadors"
dtemp="odn_temporal"
desti_d ="odn_denominadors"
relacio1="odn_relacio1"
relacio="odn_relacio"

criteris="nodrizas.eqa_criteris"
nododn="nodrizas.odn_"
problemes="nodrizas.eqa_problemes"
tabac="nodrizas.eqa_tabac"
dext="nodrizas.dextraccio"

temp=tempFolder+"temp1.txt"
temp2=tempFolder+"temp2.txt"
temp3=tempFolder+"temp3.txt"
temp0=tempFolder+"temp0.txt"
temp4=tempFolder+"temp0.txt"
temp5=tempFolder+"temp5.txt"

c.execute("select data_ext,date_add(date_add(data_ext, interval -1 year), interval +1 day) from %s" % dext)
d=c.fetchone()
ara,fa1any = d[0],d[1]
c.close()
c=conn.cursor()

c.execute("drop table if exists %s" % den)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,control int)" % den)

 
c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)
   c.execute("create table %s like %s" % (ori,origen))
   c.execute("insert into %s select * from %s where indicador='%s'" % (ori,origen,nom))
   c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca)" % ori)

         
with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      orig = "denominadors_%s" % (ind)
      if tip== "d":
         if agr== "z":
            c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where indicador='%s'" % (orig,ind))
            for i in xrange(c.rowcount):
               id,indic,at,ins,maca,ctl,=c.fetchone()
               resultat.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
         elif agr=="tabac":
            c.execute("select id_cip_sec from %s,%s where tab=1 and (data_ext between dalta and dbaixa)" % (tabac,dext))
            r=c.fetchall()
            for odn in r:
               id_cip=int(odn[0])
               c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
               for i0 in xrange(c.rowcount):
                  id,indic,at,ins,maca,ctl,=c.fetchone()
                  resultat0.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
         else:
            c.execute("select distinct taula,agrupador from %s where agrupador='%s' " % (criteris,agr))
            pres=c.fetchall()
            for i in pres:
               taula,g= i[0],int(i[1])
               if taula.startswith("odn"):
                  t="%svariables" % (nododn)
                  c.execute("select id_cip_sec from %s,%s where agrupador='%s' and (valor between %s and %s) and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month))" % (t,dext,agr,vmin,vmax,tmin,tmax))
                  r=c.fetchall()
                  for odn in r:
                     id_cip=int(odn[0])
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                     for i2 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultat2.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
               elif taula=="problemes":
                  c.execute("select id_cip_sec from %s where ps='%s'" % (problemes,agr))
                  r=c.fetchall()
                  for ps in r:
                     id_cip=int(ps[0])
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                     for i3 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultat3.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
              

with open('%s' % temp, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat)
try:
   if os.stat(temp).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp,den))
      os.remove(temp)
except OSError:
   os.remove(temp)

with open('%s' % temp0, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat0)
try:
   if os.stat(temp0).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp0,den))
      os.remove(temp0)
except OSError:
   os.remove(temp0)
   
with open('%s' % temp2, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat2)
try:
   if os.stat(temp2).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp2,den))
      os.remove(temp2)
      
except OSError:
   os.remove(temp3)
with open('%s' % temp3, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat3)
try:
   if os.stat(temp3).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp3,den))
      os.remove(temp3)
except OSError:
   os.remove(temp3)

   
c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)

c.execute("drop table if exists %s" % dtemp)
c.execute("create table %s like %s" % (dtemp,den))
c.execute("insert into %s select id_cip_sec, indicador,ates,institucionalitzat,maca,sum(control) as control from %s group by id_cip_sec,indicador" % (dtemp,den))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,control)" % dtemp)
c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(1) not null default'',agrupador varchar(8) not null default'',ctl int)" % relacio1)

with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if tip== "d":
         c.execute("insert into %s values('%s','%s','%s','%s')" % (relacio1,ind,tip,agr,f))

c.execute("drop table if exists %s" % relacio)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(1) not null default'',ctl int)" % relacio)
c.execute("insert into %s (select indicador, tipus, max(ctl) as ctl from %s group by indicador,tipus)" % (relacio,relacio1))
c.execute("drop table if exists %s" % relacio1)

c.execute("drop table if exists %s" % desti_d)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int)" % desti_d)
c.execute("select distinct indicador,ctl from %s" % relacio)
denom=c.fetchall()
for r in denom:
   indicador,max= r[0],int(r[1])
   if max==1:
      c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s'" % (dtemp,indicador))
      for i4 in xrange(c.rowcount):
         id,indic,at,ins,maca,=c.fetchone()
         resultat4.append([int(id),indic,int(at),int(ins),int(maca)])
   else:
      c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s' and control>=%s" % (dtemp,indicador,max))
      for i5 in xrange(c.rowcount):
         id,indic,at,ins,maca,=c.fetchone()
         resultat5.append([int(id),indic,int(at),int(ins),int(maca)])   
      
   
with open('%s' % temp4, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat4)
try:
   if os.stat(temp4).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp4,desti_d))
      os.remove(temp4)
except OSError:
   os.remove(temp4)
   
with open('%s' % temp5, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat5)
try:
   if os.stat(temp5).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp5,desti_d))
      os.remove(temp5)
except OSError:
   os.remove(temp5)
 
c.execute("drop table if exists %s" % den)
c.execute("drop table if exists %s" % dtemp)
c.execute("drop table if exists %s" % relacio)
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")