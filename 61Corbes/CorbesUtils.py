from sisapUtils import *
from numpy import *


test = 'corbes'
table = 'mst_corbes'
file_data = 'percentils_corbes.txt'
variables = ['TT101', 'TT102', 'TT103', 'TL201']

anysCorbes = [2013, 2014, 2015, 2016, 2017]


tempsDict ={
        'anys': {
                'columna': 'edat_a',
                'edatmin': 0,
                'edatmax': 19,
        },
        'mesos': {
                'columna': 'edat_m',
                'edatmin': 0,
                'edatmax': 24,
        },
        'dies': {
                'columna': 'edat_d',
                'edatmin': 0,
                'edatmax': 31,
        },
        'setmanes': {
                'columna': 'edat_s',
                'edatmin': 0,
                'edatmax': 7,
        },
        'tresDies': {
                'columna': 'edat_3d',
                'edatmin': 0,
                'edatmax': 10,
        },
}

def getAgrupador(agr):
    if agr == 'TT102':
        return 'Pes'
    elif agr == 'TT101':
        return 'Talla'
    elif agr == 'TT103':
        return 'IMC'
    elif agr == 'TL201':
        return 'PC'
    else:
        return 'error'
        
def getPercentil(valors, perc):
    percentils = round(percentile(valors, perc), 2)
    return percentils 