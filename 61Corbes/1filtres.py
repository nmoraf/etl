#necessitem que a test hi hagi mst_corbes
from sisapUtils import *
from collections import defaultdict
from numpy import *
from CorbesUtils import *


def get_mins_max():
    punts_tall = {}
    for variable, unitat, edats, minim, maxim in readCSV('puntstall.txt', sep='@'):
        punts_tall[variable, tempsDict[unitat]['columna'], int(edats)] = {'min': float(minim), 'max': float(maxim)}
    return punts_tall
    

def getValorsA(punts_tall):
    dades = []
    sql = "select id_cip_sec, agrupador, val, dat, naix, sexe, edat_a, edat_m, edat_s, edat_3d, edat_d from {0}".format(table)
    for id, agr, val, dat, naix, sexe, edat_a, edat_m, edat_s, edat_3d, edat_d in getAll(sql, test):
        inclos = 0
        eszero = 0
        if val != 0:
            eszero = 1
            agrupador = getAgrupador(agr)
            try:
                minim = punts_tall[agrupador, 'edat_s', edat_s]['min']
                maxim = punts_tall[agrupador, 'edat_s', edat_s]['max']
            except KeyError:
                try:
                    minim = punts_tall[agrupador, 'edat_m', edat_m]['min']
                    maxim = punts_tall[agrupador, 'edat_m', edat_m]['max']
                except KeyError:
                    try:
                        minim = punts_tall[agrupador, 'edat_a', edat_a]['min']
                        maxim = punts_tall[agrupador, 'edat_a', edat_a]['max']
                    except KeyError:
                        minim = 0
                        maxim = 20000
            if minim <= val <= maxim:
                inclos = 1
        dades.append([id, agrupador, val, dat, naix, sexe, edat_a, edat_m, edat_s, edat_3d, edat_d, eszero, inclos])
                
    return dades
  
printTime()

   
tableMy = 'mst_corbes_filtrat'
create = "(id_cip_sec int, agrupador varchar(10), val double, dat date, naix date, sexe varchar(1), edat_a int, edat_m int, edat_s int,  edat_3d int, edat_d int, eszero int, inclos int)"

createTable(tableMy,create,test, rm=True)

punts_tall = get_mins_max()
upload = getValorsA(punts_tall)  
listToTable(upload, tableMy, test)

printTime()