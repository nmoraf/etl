1@EQA0701@Cribratge de metabolopaties cong�nites@EQAG13@Activitats preventives@@@1@1@1
2@EQA0702@Cobertura vacunal sistem�tica infantil@EQAG13@Activitats preventives@@@1@2@1
36@EQA0715@Cobertura vacunal sistem�tica 0-1 anys@EQAG13@Activitats preventives@@@1@3@1
37@EQA0716@Cobertura vacunal sistem�tica 4-7 anys@EQAG13@Activitats preventives@@@1@4@1
38@EQA0717@Cobertura vacunal sistem�tica 10-14 anys@EQAG13@Activitats preventives@@@1@5@1
3@EQA0703@Cobertura PAPPS pedi�tric el primer any de vida@EQAG13@Activitats preventives@@@1@6@1
4@EQA0704@Cobertura PAPPS pedi�tric el segon any de vida@EQAG13@Activitats preventives@@@1@7@1
5@EQA0705@Cribratge de tabaquisme passiu a menors de 3 anys@EQAG13@Activitats preventives@@@1@8@1
6@EQA0706@Manteniment de la lact�ncia materna fins als 3 mesos@EQAG13@Activitats preventives@@@1@9@1
7@EQA0707@Immunitzaci� enfront la varicel�la als 12 anys@EQAG13@Activitats preventives@@@1@10@1
8@EQA0708@Cribratge obesitat entre els 6 i 14 anys@EQAG13@Activitats preventives@@@1@11@1
9@EQA0709@Cribratge opacitats oculars abans dels 3 mesos@EQAG13@Activitats preventives@@@1@12@1
10@EQA0710@Cribratge estrabisme abans dels 18 mesos@EQAG13@Activitats preventives@@@1@13@1
11@EQA0711@Cribratge ocular abans dels 7 anys@EQAG13@Activitats preventives@@@1@14@1
12@EQA0712@Cobertura vacunaci� antigripal en infants de risc@EQAG13@Activitats preventives@@@1@15@1
13@EQA0713@Cribratge h�bits t�xics en adolescents@EQAG13@Activitats preventives@@@1@16@1
41@EQA0718@Registre de les al�l�rgies medicamentoses@EQAG13@Activitats preventives@@@1@17@1
14@EQA0801@Tractament preventiu de c�ries als infants de 7 anys@EQAG14@Salut bucodental@@@1@1@2
15@EQA0802@Exploraci� bucodental als infants de 12 anys@EQAG14@Salut bucodental@@@1@2@2
16@EQA0803@Exploraci� bucodental als infants de 7 anys@EQAG14@Salut bucodental@@@1@3@2
33@EQA0804@Exploraci� bucodental als infants de 5 anys@EQAG14@Salut bucodental@@@1@4@2
17@EQA0901@No derivaci� a cirurgia de l\'h�rnia umbilical abans dels 3 anys@EQAG15@Capacitat resolutiva@1@@0@1@3
18@EQA0902@No derivaci� a dermatologia del mol�lusc contagi�s o berruga v�rica@EQAG15@Capacitat resolutiva@1@@0@2@3
19@EQA0903@No derivaci� a urologia de la fimosi en menors de 5 anys@EQAG15@Capacitat resolutiva@1@@0@3@3
27@EQA0904@Evitar la derivaci� inadequada a otorrinolaringologia dels infants amb hipertr�fia adenoide@EQAG15@Capacitat resolutiva@1@@0@4@3
20@EQA1001@Tractament de la gastroenteritis aguda@EQAG16@Patologia aguda@@@1@1@4
21@EQA1002@Tractament bronquiolitis/bronquitis aguda@EQAG16@Patologia aguda@@@1@2@4
22@EQA1003@Tractament catarros de vies respirat�ries altes (CVA) o grip@EQAG16@Patologia aguda@@@1@3@4
23@EQA1004@Tractament otitis mitjana aguda no supurativa (2 a 14 anys)@EQAG16@Patologia aguda@@@1@4@4
28@EQA1005@Tractament impetigen en poblaci� infantil@EQAG16@Patologia aguda@@@1@5@4
29@EQA1006@Maneig de les ITU en poblaci� infantil@EQAG16@Patologia aguda@@@1@6@4
30@EQA1007@Tractament de les pneum�nies en pediatria@EQAG16@Patologia aguda@@@1@7@4
34@EQA1008@Criteris de Centor en les amigdalitis agudes en pediatria@EQAG16@Patologia aguda@@@1@8@4
35@EQA1009@�s adequat de l\'estreptotest en pediatria@EQAG16@Patologia aguda@@@1@9@4
35@EQA1010@Tractament adequat de la faringoamigdalitis segons resultat estreptotest@EQAG16@Patologia aguda@@@1@10@4
24@EQA1101@Diagn�stic nous casos asma entre 6 i 14 anys@EQAG17@Patologia cr�nica@@@1@1@5
25@EQA1102@Registre de la gravetat de l asma entre 6 i 14 anys@EQAG17@Patologia cr�nica@@@1@2@5
31@EQA1103@Criteris diagn�stics obesitat@EQAG17@Patologia cr�nica@@@1@3@5
32@EQA1104@Adequaci� diagn�stica obesitat@EQAG17@Patologia cr�nica@@@1@4@5
36@EQA1105@Tractament de l\'asma adequat a la gravetat@EQAG17@Patologia cr�nica@@@1@5@5
37@EQA1106@Manteniment o reducci� del pes en els nens amb obesitat@EQAG17@Patologia cr�nica@@@1@6@5
26@EQA1201@Valoraci� social en nens amb discapacitat@EQAG18@Valoraci� social@@@1@1@6
40@EQA0719@Detecci� preco� dels trastorns de l\'esfera autista@EQAG20@Altres indicadors no EQA@@@1@2@7
