##vacsistemic
#creo variables amb els agrupadors de les vacunes
use pedia;


set @ind='EQA0702';
set @vhb=15;
set @PO=311;
set @dtp=49;
set @hib=312;
set @x=36;
set @r=37;
set @p=38;
set @mcc=313;
set @varic=314;
set @a=34;

DROP TABLE IF EXISTS mst_vacsist;
CREATE TABLE mst_vacsist (
  id_cip_sec int,
  indicador varchar(10) not null default'',
  up VARCHAR(5) NOT NULL DEFAULT '',
  uba VARCHAR(5) NOT NULL DEFAULT '',
  upinf VARCHAR(5) NOT NULL DEFAULT '',
  ubainf VARCHAR(5) NOT NULL DEFAULT '',
  data_naix DATE NOT NULL DEFAULT 0,
  edat_a double NULL,
  edat_m double NULL,
  edat_d double NULL,
  sexe VARCHAR(1) NOT NULL DEFAULT '',
  ates double null,
  institucionalitzat double null,
  maca double null,
  VHB INT NOT NULL DEFAULT 0,
  MCC INT NOT NULL DEFAULT 0,
  HIB INT NOT NULL DEFAULT 0,
  PO INT NOT NULL DEFAULT 0,
  XRP INT NOT NULL DEFAULT 0,
  DTP INT NOT NULL DEFAULT 0,
  VARICE INT NOT NULL DEFAULT 0,
  VHA INT NOT NULL DEFAULT 0,
  num INT NOT NULL DEFAULT 0,
  excl INT NOT NULL DEFAULT 0,
  unique (id_cip_sec)
)
SELECT
	id_cip_sec
   ,@ind as indicador
   ,up
   ,uba
   ,upinf
   ,ubainf
   ,data_naix
   ,edat_a
   ,edat_m
   ,edat_d
   ,sexe
   ,ates
   ,institucionalitzat
   ,maca
	,0 AS VHB
	,0 AS MCC
	,0 AS HIB
	,0 AS PO
	,0 AS XRP
	,0 AS DTP
	,0 AS VARICE
    ,0 AS VHA
    ,0 as num
    ,0 as excl
FROM
	nodrizas.ped_assignada
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vhb = 1
WHERE
	edat_m<4
	OR (edat_m>=4 and edat_m<6 and agrupador=@vhb and dosis>=1)
	OR (edat_m>=6 and edat_m<13 and agrupador=@vhb and dosis>=2)
	OR (edat_m>=13 and agrupador=@vhb and dosis>=3)
	OR data_naix<'2003/01/01'
	OR (agrupador=@vhb and temps<=2 and dosis=1)
	OR (agrupador=@vhb and temps<=3 and dosis=2)
	OR (agrupador=@vhb and dosisind<=dosis)
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_immunitzats b  ON a.id_cip_sec=b.id_cip_sec
SET a.vhb = 1
WHERE
    agrupador = 515
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_problemes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vhb = 1
WHERE
    ps in (13, 14)   
;

UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.po = 1
WHERE
	edat_m<4
	OR (edat_m>=4 And edat_m<6 AND agrupador=@PO AND dosis>=1) 
	OR (edat_m>=6 And edat_m<13 AND agrupador=@PO AND dosis>=2)
	OR (edat_m>=13 and edat_m<84 AND agrupador=@PO AND dosis>=3)
	OR (edat_m>=84 AND agrupador=@PO AND dosis>=4)
	OR (maxedat>=48 AND agrupador=@PO AND dosis>=3)
	OR (minedat>=84 AND agrupador=@PO AND dosis>=3)
	OR (agrupador=@PO AND temps<=2)
   OR (agrupador=@PO AND temps<=3 and dosis=2)
	OR (agrupador=@PO AND dosisind<=dosis)
;



UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.dtp = 1
WHERE
	edat_m<4
	OR (edat_m>=4 AND edat_m<6 AND agrupador=@dtp AND dosis>=1)
	OR (edat_m>=6 AND edat_m<13 AND agrupador=@dtp AND dosis>=2)
	OR (edat_m>=13 AND agrupador=@dtp AND dosis>=3)
	OR (maxedat>=48 AND agrupador=@dtp AND dosis>=3)
	OR (minedat>=84 AND agrupador=@dtp AND dosis>=3)
	OR (agrupador=@dtp AND temps<=2 and dosis=1)
	OR (agrupador=@dtp AND temps<=2 and dosis=2 and edat_m<84)
	OR (agrupador=@dtp AND temps<=3 and dosis=2 and edat_m>=84)
	OR (agrupador=@dtp AND temps<=7 and dosis>2)
	OR (agrupador=@dtp AND dosisind<=dosis)
;



UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.hib = 1
WHERE
	edat_m<4
	OR (edat_m>=4 And edat_m<6 AND agrupador=@hib AND dosis>=1)
	OR (edat_m>=6 And edat_m<13 AND agrupador=@hib AND dosis>=2)
	OR (edat_m>=13 AND agrupador=@hib AND dosis>=3)
	OR edat_m>=60
	OR (maxedat>=15 AND agrupador=@hib)
	OR (maxedat>=12 and maxedat<15 AND agrupador=@hib and dosis=3)
	OR (agrupador=@hib AND dosis=1 and minedat<12 AND temps<=2)
	OR (agrupador=@hib AND dosis=1 and minedat>=12 AND temps<=3)
	OR (agrupador=@hib AND dosis=2 and edat_m<12 AND temps<=2)
	OR (agrupador=@hib AND dosis=2 and edat_m>=12 AND temps<=3)
	OR (agrupador=@hib AND dosis=3 AND temps<=3)
	OR (agrupador=@hib AND dosis>=2 and minedat>=12)
    OR (agrupador=@hib AND dosis>=1 and minedat>=15)
    OR (agrupador=@hib AND dosisind<=dosis)
;



UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.xrp = xrp+1
WHERE
	edat_m<15
	OR (edat_m>=15 And edat_m<60 AND agrupador=@x AND dosis>=1)
	OR (edat_m>=60 AND agrupador=@x  AND dosis>=2)
	OR (agrupador=@x  AND temps<=2)
	OR (agrupador=@x  AND dosisind<=dosis)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.xrp = xrp+1
WHERE
	edat_m<15
	OR (edat_m>=15 And edat_m<60 AND agrupador=@r AND dosis>=1)
	OR (edat_m>=60 AND agrupador=@r  AND dosis>=2)
	OR (agrupador=@r  AND temps<=2)
	OR (agrupador=@r  AND dosisind<=dosis)
;

UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.xrp = xrp+1
WHERE
	edat_m<15
	OR (edat_m>=15 And edat_m<60 AND agrupador=@p AND dosis>=1)
	OR (edat_m>=60 AND agrupador=@p  AND dosis>=2)
	OR (agrupador=@p  AND temps<=2)
	OR (agrupador=@p  AND dosisind<=dosis)
;

update mst_vacsist
set xrp=0 where xrp=1
;

update mst_vacsist
set xrp=1 where xrp>=3
;

update mst_vacsist
set xrp=0 where xrp<>1
;

#es considera ben vacunat de XRP si te una serie de PS (segons wiki)
alter table mst_vacsist add index(id_cip_sec)
;

update mst_vacsist a inner join nodrizas.ped_problemes b on a.id_cip_sec=b.id_cip_sec
set xrp=1 where ps in ('39','40','42')
;

UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.mcc = 1
WHERE
	edat_m<6
	OR (edat_m>=6 And edat_m<14 AND agrupador=@mcc AND dosis>=1)
	OR (edat_m>=14 AND agrupador=@mcc AND dosis>=2)
	OR data_naix<'2001/01/01' #sin? fa fallar molta gent
	OR (maxedat>=12 AND agrupador=@mcc)
	OR (agrupador=@mcc AND temps<=3)
    OR (agrupador=@mcc AND dosis>=1 and minedat>=12)
	OR (agrupador=@mcc AND dosisind<=dosis)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.VARICE = 1
WHERE
	edat_m<12
	OR (edat_m>=12 AND edat_m<144 AND agrupador=@varic AND dosis>=1)
	OR (edat_m>=144 AND agrupador=@varic AND dosis>=2)
	OR (agrupador=@varic AND temps<=3)
	OR (agrupador=@varic AND dosisind<=dosis)
;	


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vha = vha+1
WHERE
	edat_m<15
	OR (edat_m>=15 AND agrupador=@a AND dosis>=1)
	OR (agrupador=@a  AND dosisind<=dosis)
;

update mst_vacsist a
set num=1 where VHB+MCC+HIB+po+XRP+DTP =6
;

update mst_vacsist a inner join nodrizas.ped_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps=557
;
