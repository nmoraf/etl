from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")


indicadors=[]
resultat=[]
resultat2=[]
resultatodn=[]
resultatll=[]
resultatps=[]
resultatv=[]
resultati=[]
resultat708=[]
resultate=[]

db="pedia"

agrupadorsambvalors= ['285', '764', '765'] #poso agrupadors que usem valor no nomes data
ind_derivacions="('EQA0901','EQA0902','EQA0903','EQA0904')" #posem indicadors de derivacions
ind708=[['6','8','30','0'],['9','10','40','0'],['11','14','80','0']] #indicador EQA0708 te diferents criteris de compliment. els poso aqui!

pathi="./dades_noesb/ped_num.txt"
criteris="nodrizas.eqa_criteris"
criterisped="nodrizas.ped_criteris_var"
origen ="ped_denominadors"
origen_agudes = "ped_denominadors_agudes"
desti = "ped_indicadors"
nodriza="nodrizas.eqa_"
nodped="nodrizas.ped_"
nododn="nodrizas.odn_"
imm="immunitzats"
dext="nodrizas.dextraccio"
temp=tempFolder+"temp1.txt"
temp2=tempFolder+"temp2.txt"
temp3=tempFolder+"temp3.txt"
temp4=tempFolder+"temp4.txt"
temp5=tempFolder+"temp5.txt"
temp6=tempFolder+"temp6.txt"
temp7=tempFolder+"temp7.txt"
temp8=tempFolder+"temp8.txt"
temp10=tempFolder+"temp10.txt"
condiciononull="val<>'NUL'"
num="ped_numeradors1"
ntemp="num_temporal"
desti_n ="ped_numeradors"
desti_agudes ="ped_indicadors_agudes"
desti_der="ped_indicadors_der"
relacio1="pedia.num_relacio1"
relacio="pedia.num_relacio"
den="pedia.denominadors"
denominadors="denominadors"



conn = connect((db,'aux'))
c=conn.cursor()


c.execute("drop table if exists %s" % num)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,control int)" % num)

c.execute("drop table if exists %s" % desti_agudes)
c.execute("create table %s (id_cip_sec int,dde DATE NOT NULL DEFAULT 0,indicador varchar(8),ates int,institucionalitzat int,maca int,num int,excl int)" % desti_agudes)
c.execute("insert into %s select id_cip_sec,dde,indicador,ates,institucionalitzat,maca,1 as num, 0 as excl from %s" % (desti_agudes,origen_agudes))
c.execute("alter table %s add index (id_cip_sec,dde,indicador,ates,institucionalitzat,maca,num,excl)" % desti_agudes)

c.execute("drop table if exists %s" % desti_der)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,num int,excl int)" % desti_der)
c.execute("insert into %s select id_cip_sec,indicador,ates,institucionalitzat,maca,1 as num, 0 as excl from %s where indicador in %s" % (desti_der,origen,ind_derivacions))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,num,excl)" % desti_der)

c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   taula = "%s_%s" % (desti,nom)
   c.execute("drop table if exists %s" % taula)
   c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int)" % (taula))
   c.execute("insert into %s select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s'" % (taula,origen,nom))
   c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca)" % taula)
   
c.execute("select distinct indicador from %s" % denominadors)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)
   c.execute("create table %s like %s" % (ori,denominadors))
   c.execute("insert into %s select * from %s where indicador='%s'" % (ori,denominadors,nom))
   c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca)" % ori)

with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,nvar,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8]
      orig = "%s_%s" % (desti,ind)
      if tip== "n":
         nvar = int(nvar)
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            t,g= i[0],int(i[1])
            if g in agrupadorsambvalors:
               nenvar="%svariables" % (nodped)
               c.execute("select id_cip_sec,count(*) from %s where agrupador='%s' and (edat_m between %s and %s) and (val between %s and %s) group by id_cip_sec" % (nenvar,agr,tmin,tmax,vmin,vmax))
               var=c.fetchall()
               for v in var:
                  cip,n =v[0],int(v[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i7 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatll.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
            elif t=="nen11" or t=="nen12" or t=="variables" or t=="activitats" or t=="social":
               nenvar="%svariables" % (nodped)
               c.execute("select id_cip_sec,count(*) from %s where agrupador='%s' and edat_m between %s and %s group by id_cip_sec" % (nenvar,agr,tmin,tmax))
               var=c.fetchall()
               for v in var:
                  cip,n =v[0],int(v[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultat.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
            elif t.startswith("odn"):
               odn="%svariables" % (nododn)
               if agr ==  '286':
                  c.execute("select id_cip_sec,count(*) from %s,%s where agrupador='%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month)) and baixa = 1 group by id_cip_sec" % (odn,dext,agr,tmin,tmax))
               else:
                  c.execute("select id_cip_sec,count(*) from %s,%s where agrupador='%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month)) group by id_cip_sec" % (odn,dext,agr,tmin,tmax))
               var=c.fetchall()
               for o in var:
                  cip,n =o[0],int(o[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i3 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatodn.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
            elif t=="problemes":
               ps="%sproblemes" % (nodped)
               if vmin:
                  c.execute("select id_cip_sec,count(*) from %s where ps='%s' and pr_gra between %s and %s group by id_cip_sec" % (ps,agr,vmin,vmax))
               elif tmax:
                  c.execute("select id_cip_sec,count(*) from %s where ps='%s' and (edat_m between %s and %s) group by id_cip_sec" % (ps,agr,tmin,tmax))   
               else:
                  c.execute("select id_cip_sec,count(*) from %s where ps='%s' group by id_cip_sec" % (ps,agr))
               var=c.fetchall()
               for p in var:
                  cip,n =p[0],int(p[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i5 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatps.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
            elif t=="vacunesped":
               vac="%svacunes" % (nodped)
               c.execute("select id_cip_sec,count(*) from %s where agrupador='%s' and dosis>='%s' group by id_cip_sec" % (vac,agr,vmin))
               var=c.fetchall()
               for vac in var:
                  cip,n =vac[0],int(vac[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i6 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatv.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
            elif t=="exclusions":
               exc="%s%s" % (nodped,t)
               c.execute("select id_cip_sec,count(*) from %s where agrupador='%s' group by id_cip_sec" % (exc,agr))
               var=c.fetchall()
               for vac in var:
                  cip,n =vac[0],int(vac[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i9 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultate.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])  
            elif t=="inatural":
               inat="%s%s" % (nodped,imm)
               c.execute("select id_cip_sec,count(*) from %s where agrupador='%s' group by id_cip_sec" % (inat,agr))
               var=c.fetchall()
               for vac in var:
                  cip,n =vac[0],int(vac[1])
                  if n>=nvar:
                     tf="%s_%s" % (desti,ind)
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                     for i10 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultati.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])                              
            elif t=="prescripcions":
               t="%stractaments" % (nodriza)
               c.execute("select id_cip_sec from %s, nodrizas.dextraccio where farmac='%s' and pres_orig between date_add(data_ext, interval - %s month) and data_ext" % (t,agr,tmin))   
               var=c.fetchall()
               for p in var:
                  cip=p[0]
                  tf="%s_%s" % (desti,ind)
                  c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (tf,cip))
                  for i5 in xrange(c.rowcount):
                     id,indic,at,ins,maca,ctl,=c.fetchone()
                     resultatps.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
      elif tip=="na":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         tt=c.fetchall()
         for i2 in tt:
            t,g= i2[0],int(i2[1])
            if t=="prescripcions":
               tractaments ="%stractaments" % (nodriza)
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=0 where indicador='%s' and farmac='%s' and (pres_orig between date_add(dde,interval - %s day) and date_add(dde,interval + %s day))" % (desti_agudes,tractaments,ind,agr,tmin,tmax))
            elif t=="nen11" or t=="nen12" or t=="variables" or t=="activitats" or t=="social":
               nenvar="%svariables" % (nodped)
               c.execute("update %s set num=0 where indicador='%s'" % (desti_agudes,ind))
               sql = "update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s set num=1 where indicador='%s' and agrupador='%s' and (dat between date_add(dde,interval - %s day) and date_add(dde,interval + %s day))" % (desti_agudes,nenvar,dext,ind,agr,tmin,tmax)
               if vmin and vmax:
                  sql += " and val between {} and {}".format(vmin, vmax)
               c.execute(sql)
            elif t=="laboratori":
               nenvar="%svariables" % (nodped)
               c.execute("update %s set num=0 where indicador='%s'" % (desti_agudes,ind))
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where indicador='%s' and agrupador='%s' and (dat between date_add(dde,interval - %s month) and date_add(dde,interval + %s month))" % (desti_agudes,nenvar,ind,agr,tmin,tmax))
      elif tip=="nat":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         tt=c.fetchall()
         for i2 in tt:
            t,g= i2[0],int(i2[1])
            if t=="prescripcions":
               tractaments ="%stractaments" % (nodriza)
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=num+1 where indicador='%s' and farmac='%s' and (pres_orig between date_add(dde,interval - %s day) and date_add(dde,interval + %s day))" % (desti_agudes,tractaments,ind,agr,tmin,tmax))
            elif t=="nen11" or t=="nen12" or t=="variables" or t=="activitats" or t=="social":
               nenvar="%svariables" % (nodped)
               c.execute("update %s set num=0 where indicador='%s'" % (desti_agudes,ind))
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s set num=1 where indicador='%s' and agrupador='%s' and (dat between date_add(data_ext,interval - %s day) and date_add(data_ext,interval + %s month))" % (desti_agudes,nenvar,dext,ind,agr,tmin,tmax))
      elif tip=="nd": 
         tder="%sderivacions" % (nodped)
         c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec,%s set num=0 where indicador='%s' and agrupador='%s' and motiu='%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month)) and (edat_m between %s and %s)" % (desti_der,tder,dext,ind,agr,nvar,tmin,tmax,vmin,vmax))
      elif tip=="no": 
            for r in ind708:
               emin,emax,tempsmin,tempsmax=r[0],r[1],r[2],r[3]
               nenvar="%svariables" % (nodped)
               c.execute("select id_cip_sec from %s,%s where agrupador='%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month)) group by id_cip_sec" % (nenvar,dext,agr,tempsmin,tempsmax))
               var=c.fetchall()
               for v in var:
                  d708 = "denominadors_%s" % (ind)
                  cip =v[0]
                  c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s' and indicador='%s' and anys between %s and %s" % (d708,cip,ind,emin,emax))
                  for i8 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultat708.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
with open('%s' % temp4, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatll)
try:
   if os.stat(temp4).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp4,num))
      os.remove(temp4)
except OSError:
   os.remove(temp4)
try:
   os.remove(temp4)
except:
   pass
   
with open('%s' % temp, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat)
try:
   if os.stat(temp).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp,num))
      os.remove(temp)
except OSError:
   os.remove(temp)   
   
with open('%s' % temp3, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatodn)
try:
   if os.stat(temp3).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp3,num))
      os.remove(temp3)
except OSError:
   os.remove(temp3)

with open('%s' % temp5, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatps)
try:
   if os.stat(temp5).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp5,num))
      os.remove(temp5)
except OSError:
   os.remove(temp5)

with open('%s' % temp6, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatv)
try:
   if os.stat(temp6).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp6,num))
      os.remove(temp6)
except OSError:
   os.remove(temp6)   

   
with open('%s' % temp8, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultate)
try:
   if os.stat(temp8).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp8,num))
      os.remove(temp8)
except OSError:
   os.remove(temp8)   
   
with open('%s' % temp10, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultati)
try:
   if os.stat(temp10).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp10,num))
      os.remove(temp10)
except OSError:
   os.remove(temp10) 
   
with open('%s' % temp7, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat708)
try:
   if os.stat(temp7).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp7,num))
      os.remove(temp7)
except OSError:
   os.remove(temp7) 

c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   taula = "%s_%s" % (desti,nom)
   c.execute("drop table if exists %s" % taula)
 
 
c.execute("drop table if exists %s" % ntemp)
c.execute("create table %s like %s" % (ntemp,num))
c.execute("insert into %s select id_cip_sec, indicador,ates,institucionalitzat,maca,sum(control) as control from %s group by id_cip_sec,indicador" % (ntemp,num))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,control)" % ntemp)
c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(7) not null default'',agrupador varchar(8) not null default'',ctl int)" % relacio1)

with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,nvar,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7],ind[8]
      if tip== "n" or tip=="no" or tip=="na" or tip=="nat":
         c.execute("insert into %s values('%s','%s','%s','%s')" % (relacio1,ind,tip,agr,f))
c.execute("drop table if exists %s" % relacio)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(7) not null default'',ctl int)" % relacio)
c.execute("insert into %s (select indicador, tipus, max(ctl) as ctl from %s group by indicador)" % (relacio,relacio1))
c.execute("drop table if exists %s" % relacio1)

c.execute("drop table if exists %s" % desti_n)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int)" % desti_n)
c.execute("select distinct indicador,tipus, ctl from %s" % relacio)
denom=c.fetchall()
for r in denom:
   indicador,tipusn,max= r[0],r[1],int(r[2])
   if tipusn=="n" or tipusn=="no":
      c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s' and control>=%s" % (ntemp,indicador,max))
      for i4 in xrange(c.rowcount):
         id,indic,at,ins,maca,=c.fetchone()
         resultat2.append([int(id),indic,int(at),int(ins),int(maca)])
   else:   
      c.execute("update %s set num=0 where indicador='%s' and num<%s" % (desti_agudes,indicador,max))
      c.execute("update %s set num=1 where indicador='%s' and num>=%s" % (desti_agudes,indicador,max))
        
with open('%s' % temp2, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat2)
try:
   if os.stat(temp2).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp2,desti_n))
      os.remove(temp2)
except OSError:
   os.remove(temp2)

c.execute("drop table if exists %s" % num)
c.execute("drop table if exists %s" % ntemp)
c.execute("drop table if exists %s" % relacio)

c.execute("select distinct indicador from %s" % denominadors)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")