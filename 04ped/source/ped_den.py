from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

indicadors=[]
resultat=[]
resultatp=[]
resultatd=[]
resultatd2=[]
resultatag=[]
resultatodn=[]
resultatder=[]
pathi="./dades_noesb/ped_den.txt"
db="pedia"
dext="nodrizas.dextraccio"
den="ped_denominadors1"
dtemp="den_temporal"
desti_d ="ped_denominadors"
aguda_d ="ped_denominadors_agudes"
criteris="nodrizas.eqa_criteris"
criterisped="nodrizas.ped_criteris_var"
nodriza="nodrizas.eqa_"
nodped="nodrizas.ped_"
nododn="nodrizas.odn_"
origen="denominadors"
temp=tempFolder+"temp1.txt"
temp2=tempFolder+"temp2.txt"
temp3=tempFolder+"temp3.txt"
temp4=tempFolder+"temp4.txt"
temp5=tempFolder+"temp5.txt"
temp6=tempFolder+"temp6.txt"
temp7=tempFolder+"temp7.txt"
relacio1="pedia.ped_relacio1"
relacio="pedia.ped_relacio"


conn = connect((db,'aux'))
c=conn.cursor()
e=conn.cursor()
c.execute("drop table if exists %s" % den)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,control int)" % den)
c.execute("drop table if exists %s" % aguda_d)
c.execute("create table %s (id_cip_sec int,dde DATE NOT NULL DEFAULT 0,indicador varchar(8),ates int,institucionalitzat int,maca int)" % aguda_d)

c.execute("select data_ext,date_add(date_add(data_ext, interval -1 year), interval +1 day) from %s" % dext)
d=c.fetchone()
c.close()
ara,fa1any = d[0],d[1]
c= conn.cursor()
c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)
   c.execute("create table %s like %s" % (ori,origen))
   c.execute("insert into %s select * from %s where indicador='%s'" % (ori,origen,nom))
   c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca)" % ori)
          
with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      orig = "denominadors_%s" % (ind)
      if tip== "d":
         if agr== "z":
            c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where indicador='%s'" % (orig,ind))
            for i in xrange(c.rowcount):
               id,indic,at,ins,maca,ctl,=c.fetchone()
               resultat.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
         else:
            c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
            pres=c.fetchall()
            for i in pres:
               taula,g= i[0],int(i[1])
               if taula=="prescripcions":
                  t="%stractaments" % nodriza
                  err="Falta %s" % taula          
                  print err
               elif taula=="problemes":
                  t="%s%s" % (nodped,taula)
                  c.execute("select id_cip_sec from %s where ps='%s'" % (t,agr))
                  r=c.fetchall()
                  for ps in r:
                     id_cip=int(ps[0])
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                     for i2 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatp.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
               elif taula=="vag":
                  t="%s%s" % (nodriza,taula)
                  c.execute("select id_cip_sec from %s where agrupador='%s'" % (t,agr))
                  r=c.fetchall()
                  for ps in r:
                     id_cip=int(ps[0])
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                     for i2 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatp.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
               elif taula.startswith("odn"):
                  t="%svariables" % (nododn)
                  c.execute("select id_cip_sec from %s,%s where agrupador='%s' and (valor between %s and %s) and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month))" % (t,dext,agr,vmin,vmax,tmin,tmax))
                  r=c.fetchall()
                  for odn in r:
                     id_cip=int(odn[0])
                     c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                     for i9 in xrange(c.rowcount):
                        id,indic,at,ins,maca,ctl,=c.fetchone()
                        resultatodn.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
               elif taula.startswith("nen") or taula == 'variables':
                  t="%svariables" % (nodped)
                  if agr == '815':
                     c.execute("select id_cip_sec from %s,%s where agrupador='%s' and val = '%s' and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month))" % (t,dext,agr,vmax,tmin,tmax))
                     r=c.fetchall()
                     for odn in r:
                        id_cip=int(odn[0])
                        c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                        for i9 in xrange(c.rowcount):
                           id,indic,at,ins,maca,ctl,=c.fetchone()
                           resultatodn.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])      
                  else:
                     c.execute("select id_cip_sec from %s,%s where agrupador='%s' and (val between %s and %s) and (dat between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month))" % (t,dext,agr,vmin,vmax,tmin,tmax))
                     r=c.fetchall()
                     for odn in r:
                        id_cip=int(odn[0])
                        c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
                        for i9 in xrange(c.rowcount):
                           id,indic,at,ins,maca,ctl,=c.fetchone()
                           resultatodn.append([int(id),indic,int(at),int(ins),int(maca),int(ctl)])
               else:     
                  t="%s%s" % (nodriza,taula)
                  err="Falta %s" % taula          
                  print err
      elif tip=="da":
         if agr=="z":
            ok=1
         else:
            c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
            ag=c.fetchall()
            for ia in ag:  
               taula,g= ia[0],int(ia[1])
               if taula=="problemes":
                  t="%s%s_incid" % (nodped,taula)
                  c.execute("select id_cip_sec,dde from %s where ps='%s' and edat_m between %s and %s and dde between '%s' and '%s'" % (t,agr,tmin,tmax,fa1any,ara))
                  r=c.fetchall()
                  for ps in r:
                     id_cipa,data=int(ps[0]),ps[1]
                     c.execute("select id_cip_sec,'%s',indicador,ates,institucionalitzat,maca from %s where id_cip_sec='%s'" % (data,orig,id_cipa))
                     for i7 in xrange(c.rowcount):
                        id,dde,indic,at,ins,maca,=c.fetchone()
                        resultatag.append([int(id),dde,indic,int(at),int(ins),int(maca)])
               elif taula=="variables":
                  t="%s%s" % (nodped,taula)
                  sql = "select id_cip_sec,dat from %s where agrupador='%s' and edat_m between %s and %s and dat between '%s' and '%s'" % (t,agr,tmin,tmax,fa1any,ara)
                  if vmin and vmax:
                    sql += " and val between {} and {}".format(vmin, vmax)
                  c.execute(sql)
                  r=c.fetchall()
                  for ps in r:
                     id_cipa,data=int(ps[0]),ps[1]
                     c.execute("select id_cip_sec,'%s',indicador,ates,institucionalitzat,maca from %s where id_cip_sec='%s'" % (data,orig,id_cipa))
                     for i7 in xrange(c.rowcount):
                        id,dat,indic,at,ins,maca,=c.fetchone()
                        resultatag.append([int(id),dat,indic,int(at),int(ins),int(maca)])
      elif tip=="dd":            
         if agr=="z":
            ok=1
         else:
            t="%sproblemes" % (nodped)
            c.execute("select id_cip_sec from %s,%s where ps='%s' and edat_m between %s and %s and (dde between date_add(data_ext,interval - %s month) and date_add(data_ext,interval + %s month))" % (t,dext,agr,vmin,vmax,tmin,tmax))
            r=c.fetchall()
            for d in r:
               id_cip=int(d[0])
               c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca, 1 from %s where id_cip_sec='%s'" % (orig,id_cip))
               for i8 in xrange(c.rowcount):
                  id,dde,indic,at,ins,maca,=c.fetchone()
                  resultatder.append([int(id),dde,indic,int(at),int(ins),int(maca)])
with open('%s' % temp, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultat)
try:
   if os.stat(temp).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp,den))
      os.remove(temp)
except OSError:
   os.remove(temp)

   
with open('%s' % temp2, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatp)
try:
   if os.stat(temp2).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp2,den))
      os.remove(temp2)
except OSError:
   os.remove(temp2)

    
with open('%s' % temp6, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatodn)
try:
   if os.stat(temp6).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp6,den))
      os.remove(temp6)
except OSError:
   os.remove(temp6)
                  
with open('%s' % temp5, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatag)
try:
   if os.stat(temp5).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp5,aguda_d))
      os.remove(temp5)
except OSError:
   os.remove(temp5)

     
with open('%s' % temp6, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatder)
try:
   if os.stat(temp6).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp6,den))
      os.remove(temp6)
except OSError:
   os.remove(temp6)

   
c.execute("select distinct indicador from %s" % origen)
o=c.fetchall()
for x in o:
   nom = x[0]
   ori = "denominadors_%s" % (nom)
   c.execute("drop table if exists %s" % ori)


c.execute("drop table if exists %s" % dtemp)
c.execute("create table %s like %s" % (dtemp,den))
c.execute("insert into %s select id_cip_sec, indicador,ates,institucionalitzat,maca,sum(control) as control from %s group by id_cip_sec,indicador" % (dtemp,den))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,control)" % dtemp)
c.execute("drop table if exists %s" % relacio1)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(1) not null default'',agrupador varchar(8) not null default'',ctl int)" % relacio1)

with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if tip== "d" or tip=="dd":
         c.execute("insert into %s values('%s','%s','%s','%s')" % (relacio1,ind,tip,agr,f))
c.execute("drop table if exists %s" % relacio)
c.execute("create table %s (indicador varchar(8) not null default'', tipus varchar(1) not null default'',ctl int)" % relacio)
c.execute("insert into %s (select indicador, tipus, max(ctl) as ctl from %s group by indicador,tipus)" % (relacio,relacio1))
c.execute("drop table if exists %s" % relacio1)

c.execute("drop table if exists %s" % desti_d)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int)" % desti_d)
c.execute("select distinct indicador,ctl from %s" % relacio)
denom=c.fetchall()
for r in denom:
   indicador,max= r[0],int(r[1])
   if max==1:
      c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s'" % (dtemp,indicador))
      for i3 in xrange(c.rowcount):
         id,indic,at,ins,maca,=c.fetchone()
         resultatd.append([int(id),indic,int(at),int(ins),int(maca)])
   else:
      c.execute("select id_cip_sec,indicador,ates,institucionalitzat,maca from %s where indicador='%s' and control>=%s" % (dtemp,indicador,max))
      for i4 in xrange(c.rowcount):
         id,indic,at,ins,maca,=c.fetchone()
         resultatd2.append([int(id),indic,int(at),int(ins),int(maca)])   
      
   
with open('%s' % temp3, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatd)
try:
   if os.stat(temp3).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp3,desti_d))
      os.remove(temp3)
except OSError:
   os.remove(temp3)
   
with open('%s' % temp4, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   a.writerows(resultatd2)
try:
   if os.stat(temp4).st_size > 0:
      c.execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp4,desti_d))
      os.remove(temp4)
except OSError:
   os.remove(temp4)
try:
   os.remove(temp4)
except:
   pass
   
c.execute("drop table if exists %s" % den)
c.execute("drop table if exists %s" % dtemp)
c.execute("drop table if exists %s" % relacio)

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")