from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="pedia"
pathd="./dades_noesb/ped_den.txt"
pathi="./dades_noesb/ped_excl.txt"
pathc="./dades_noesb/ped_ind.txt"
pathesp="./dades_noesb/eqaped_prevalences_anual.txt"
pathespeorig="./dades_noesb/eqaped_poresper_anual.txt"
pathmetesres="./dades_noesb/eqaped_metesres_subind_anual.txt"
pathpond="./dades_noesb/eqaped_ponderacio_anual.txt"

conn = connect((db,'aux'))
c=conn.cursor()

c.execute("select grip,grip_ind from nodrizas.dextraccio")
gripp,gripInd,=c.fetchone()
c.close()
c=conn.cursor()
grip= True if gripp==1 else False


pac = "mst_indicadors_pacient"
ecap_pac_pre = "ped_ecap_pacient_pre"
ecap_pac = "exp_ecap_pacient"
ecap_marca_exclosos = "if(institucionalitzat=1,2,if(excl=1,4,if(ates=0,5,if(maca=1,1,0))))"
ecap_cataleg_exclosos = [[0,'Pacients que formen part de l&#39;indicador',0],[1,'Pacients MACA',2],[2,'Pacients institucionalitzats',3],[3,'Pacients exclosos per edat',4],[4,'Pacients exclosos per motius cl&iacute;nics',5],[5,'Pacients no atesos el darrer any',1]]
condicio_ecap_llistat = "llistat=1 and excl=0 and num=0"

u11 = "mst_u11"
u11nod = "nodrizas.eqa_u11"
indi = "eqa_ind"

den="ped_denominadors"
num="ped_numeradors"
indicadors="ped_indicadors"
indicadorsagudes="ped_indicadors_agudes"
indicadorsder="ped_indicadors_der"
vac="mst_vacsist"
criteris="nodrizas.eqa_criteris"
criterisped="nodrizas.ped_criteris_var"
nodped="nodrizas.ped_"
nod="nodrizas.eqa_"
assignada="nodrizas.ped_assignada"
edats_k = "nodrizas.khx_edats5a"

up = "eqa_khalix_up_pre"
pobk = "eqa_poblacio_khx"
khalix_pre = "eqa_khalix_up_ind"
khalix = "exp_khalix_up_ind"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
condicio_khalix = "excl=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
condicio_khalix_pob = "institucionalitzat=1 or (institucionalitzat=0 and maca=0)"
khalix_pob = "exp_khalix_up_pob"

conceptes=[['NUM','num'],['DEN','den']]

catalegKhx = "exp_khalix_cataleg"


pob = "eqa_poblacio"
condicio_poblacio = "ates=1 and institucionalitzat=0 and maca=0"
uba_in = "mst_ubas"
uba1 = "eqa_uba_1"
medea = "nodrizas.cat_centres"  #si es '' (o no existis a centres) posem 2U (compte que khalix no ho fa, i per tant no tindran agregat de medea en el detall)
minPob = 50
tipus=[['M','up','uba'],['I','upinf','ubainf']]
uba2 = "eqa_uba_2"
uba3 = "eqa_uba_3"
uba4 = "eqa_uba_4"
uba5 = "eqa_uba_5"
condicio_ecap_ind = "ates=1 and institucionalitzat=0 and maca=0 and excl=0"
ecap_ind = "exp_ecap_uba"  
esperades = "eqa_esperades"
esperadesFactor = 0.8
esperadesCond = "NOINSAT"
metesOrig = "eqaped_metes_subind_anual"
metesOrigres = "eqaped_metesres_subind_anual"
metes = "eqa_metes"
metesres = "eqa_metesres"
ponderacio = "ped_ponderacio"

khalix_uba = "exp_khalix_uba_ind"
dimensions = [['NUM','resolts', 1],['DEN','detectats', 1],['DENESPER','esperats', 1],['AGNORESOL','llistat', 1],['AGDETECTR','deteccio', 100],['AGDETECT','deteccioPerResultat', 100],['AGRESOLTR','resolucio', 100],['AGRESOLT','resolucioPerPunts', 100],['AGRESULT','resultatPerPunts', 100],['AGASSOLP','resultatPerPunts', 100],['AGASSOL','punts', 1],['AGMMINRES','mmin_p', 100],['AGMMAXRES','mmax_p', 100]]
condicio_khalix_uba=""
if not grip:
    condicio_khalix += " and indicador not in %s" % gripInd
    condicio_khalix_uba = condicio_khalix_uba + " where indicador not in %s" % gripInd

cataleg = "exp_ecap_cataleg"
catalegPare = "exp_ecap_catalegPare"
catalegPunts = "exp_ecap_catalegPunts"
catalegExclosos = "exp_ecap_catalegExclosos"
catalegKhx = "exp_khalix_cataleg"

c.execute("drop table if exists %s" % indicadors)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)" % indicadors)
with open(pathd, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for a in p:
      ind,tip,agr,f,tmin,tmax,vmin,vmax = a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7]
      if tip== "d" and agr=="z":
         c.execute("insert into %s select id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den, 0 as num, 0 as excl from %s where indicador='%s'" % (indicadors,den,ind))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,den,num,excl)" % indicadors)


with open(pathi, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for a in p:
      ind,tip,agr,f,n = a[0],a[1],a[2],a[3],a[4]
      if tip== "d":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            t,g= i[0],int(i[1])
            if t=="problemes":
               ps="%s%s" % (nodped,t)
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where indicador='%s' and ps='%s'" % (indicadors,ps,ind,agr))
      elif tip== "da":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            t,g= i[0],int(i[1])
            if t=="problemes":
               ps="%s%s" % (nodped,t)   
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where indicador='%s' and ps='%s'" % (indicadorsagudes,ps,ind,agr))  
      elif tip== "dai":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            t,g= i[0],int(i[1])
            if t=="problemes":
               ps="%sproblemes_incid" % (nodped)   
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where indicador='%s' and ps='%s' and (b.dde between date_add(a.dde,interval - '%s' month) and date_add(a.dde,interval + '%s' month)) " % (indicadorsagudes,ps,ind,agr, n, n))  
      elif tip== "dd":
         c.execute("select distinct taula,agrupador from %s where agrupador='%s' union select distinct taula,agrupador from %s where agrupador='%s'" % (criteris,agr,criterisped,agr))
         pres=c.fetchall()
         for i in pres:
            t,g= i[0],int(i[1])
            if t=="problemes":
               if n:
                  ps="%s%s" % (nod,t)
                  c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where indicador='%s' and ps='%s' and Ninc>%s" % (indicadorsder,ps,ind,agr,n))                    
               else:
                  ps="%s%s" % (nodped,t)
                  c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where indicador='%s' and ps='%s'" % (indicadorsder,ps,ind,agr))                    

#afegeixo a la taula indicadors els indicadors de agudes, derivacions, vacsistematic i eqd
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec and a.indicador=b.indicador set num=1" % (indicadors,num))  
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,excl from %s" % (indicadors,indicadorsagudes))  
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,excl from %s" % (indicadors,indicadorsder))
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,excl from %s" %  (indicadors,vac))
c.execute("insert into %s select  id_cip_sec,'EQA0715',ates,institucionalitzat,maca,1 as den,num,excl from %s where edat_a between 0 and 1" %  (indicadors,vac))
c.execute("insert into %s select  id_cip_sec,'EQA0716',ates,institucionalitzat,maca,1 as den,num,excl from %s where edat_a between 4 and 7" %  (indicadors,vac))
c.execute("insert into %s select  id_cip_sec,'EQA0717',ates,institucionalitzat,maca,1 as den,num,excl from %s where edat_a between 10 and 14" %  (indicadors,vac))
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,excl from %s" %  (indicadors,'ped_eqd'))
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,excl from %s" %  (indicadors,'ped_bolets'))
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,exclos from %s" %  (indicadors,'ped_indicador_mant_pes'))
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,exclos from %s" %  (indicadors,'ped_indicador_alergias'))
c.execute("insert into %s select  id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den,num,exclos from %s" %  (indicadors,'ped_indicador_faring'))
c.execute("alter table %s add index(id_cip_sec,indicador,ates,institucionalitzat,maca,den,num,excl)" %  (indicadors))


sql = "update {0} a inner join ped_familiars b on a.id_cip_sec=b.id_cip_sec and a.indicador=b.indicador set excl=1".format(indicadors)
execute(sql, db)


c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double,llistat double)" % pac)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa <> '1':
        c.execute("insert into %s select a.id_cip_sec,indicador,up,uba,upinf,ubainf,edat_a,sexe,a.ates,a.institucionalitzat,a.maca,num,den,excl,'%s' as llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s' " % (pac,llistat,indicadors,assignada,i))

c.execute("drop table if exists %s" % ecap_pac_pre)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',indicador varchar(10) not null default '',maca int,institucionalitzat int,excl int,exclos int,toShow int)" % ecap_pac_pre)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,indicador,maca,institucionalitzat,excl,%s,1 from %s where %s" % (ecap_pac_pre,ecap_marca_exclosos,pac,condicio_ecap_llistat))
c.execute("alter table %s add index(id_cip_sec)" % ecap_pac_pre)


c.execute("drop table if exists %s" % ecap_pac)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("drop table if exists %s" % u11)
c.execute("create table %s like %s" % (u11,u11nod))
c.execute("insert into %s select * from %s a where exists (select 1 from %s b where toShow=1 and a.id_cip_sec=b.id_cip_sec)" % (u11,u11nod,ecap_pac_pre))
c.execute("alter table %s add unique(id_cip_sec,hash_d,codi_sector)" % u11)
c.execute("insert into %s select a.id_cip_sec,up,uba,upinf,ubainf,indicador as grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1 group by a.id_cip_sec,up,uba,upinf,ubainf,indicador,exclos,hash_d,codi_sector" % (ecap_pac,ecap_pac_pre,u11))

c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,comb,pac,edats_k,condicio_khalix))

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))

c.execute("drop table if exists %s" % pobk)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '')" % pobk)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb from %s a inner join %s b on a.edat_a=b.edat where %s" % (pobk,sexe,comb,assignada,edats_k,condicio_khalix_pob))
c.execute("drop table if exists %s" % khalix_pob)
c.execute("create table %s (up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',n int)" % khalix_pob)
c.execute("insert into %s select up,edat,sexe,comb,count(1) n from %s where comb like '%%AT%%' group by 1,2,3,4" % (khalix_pob,pobk))
c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),count(1) n from %s group by 1,2,3,4" % (khalix_pob,pobk))
    
c.execute("drop table if exists %s" % pob)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',uba varchar(5) not null default '',upinf varchar(5) not null default '',ubainf varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '')" % pob)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,khalix edat,%s sexe from %s a inner join %s b on a.edat_a=b.edat where %s" % (pob,sexe,assignada,edats_k,condicio_poblacio))
c.execute("drop table if exists %s" % uba_in)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',primary key(up,uba,tipus))" % uba_in)
c.execute("drop table if exists %s" % uba1)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',n int)" % uba1)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s up,%s uba,'%s' tipus from %s where %s <> '' and %s <> '' group by %s,%s having count(1)>=%d" % (uba_in,up,uba,tip,pob,up,uba,up,uba,minPob))
    c.execute("insert into %s select %s up,%s uba,'%s' tipus,'TIPUS2U2' medea,edat,sexe,count(1) n from %s group by %s,%s,edat,sexe" % (uba1,up,uba,tip,pob,up,uba))
c.execute("delete from %(uba1)s where not exists (select 1 from %(uba_in)s where %(uba1)s.up=%(uba_in)s.up and %(uba1)s.uba=%(uba_in)s.uba and %(uba1)s.tipus=%(uba_in)s.tipus)" % {'uba1': uba1,'uba_in':uba_in})
c.execute("update %s a inner join %s b on a.up=b.scs_codi set a.medea=concat('TIPUS',b.medea,'2') where b.medea<>''" % (uba1,medea))
c.execute("drop table if exists %s" % uba2)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',poblacio int)" % uba2)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      c.execute("insert into %s select up,uba,tipus,'%s' indicador,medea,edat,sexe,n poblacio from %s" % (uba2,i,uba1))
c.execute("drop table if exists %s" % uba3)
c.execute("create table %s like %s" % (uba3,uba2))
c.execute("alter table %s add column esperats double null" % uba3)

c.execute("insert into {} select a.*, 0 esperats from {} a".format(uba3, uba2))
c.execute("alter table {} add index esperades (indicador, medea, edat, sexe)".format(uba3))
with open(pathesp, 'rb') as file:
   e=csv.reader(file, delimiter='{', quotechar='|')
   for esp in e:
      i,periode,medea,ct,edat,pob,sexe,prev = esp[0],esp[1],esp[2],esp[3],esp[4],esp[5],esp[6],esp[7]
      if pob==esperadesCond:
        c.execute("update %s set esperats=poblacio*%s where indicador='%s' and medea='%s' and edat='%s' and sexe='%s'" % (uba3,prev,i,medea,edat,sexe))
c.execute("drop table if exists %s" % uba4)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(5) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',poblacio int,esperats double)" % uba4)
c.execute("insert into %s select up,uba,tipus,indicador,sum(poblacio) poblacio,sum(esperats) esperats from %s group by 1,2,3,4" % (uba4,uba3))
c.execute("drop table if exists %s" % uba5)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',detectats int,resolts int)" % uba5)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,indicador,sum(den) detectats,sum(num) resolts from %s where %s group by 1,2,4" % (uba5,up,uba,tip,pac,condicio_ecap_ind))
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % uba4)
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % uba5)

c.execute("drop table if exists %s" % ecap_ind)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int,resolucioPerPunts double, invers int, mmin_p double, mmax_p double)" % ecap_ind)
c.execute("insert into %s select a.up,a.uba,a.tipus,a.indicador,esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from %s a left join %s b on a.up=b.up and a.uba=b.uba and a.tipus=b.tipus and a.indicador=b.indicador" % (ecap_ind,uba4,uba5))
c.execute("update %s set deteccio=if(esperats=0,0,detectats/esperats),resolucio=if(detectats=0,0,resolts/detectats)" % ecap_ind)

c.execute("drop table if exists %s" % esperades)
c.execute("create table %s (indicador varchar(8) not null default '',prof double,unique (indicador,prof))" % esperades)

with open(pathespeorig, 'rb') as file:
   eo=csv.reader(file, delimiter='{', quotechar='|')
   for espo in eo:
      indicador,z1,z2,z3,z4,grup,z5,eap = espo[0],espo[1],espo[2],espo[3],espo[4],espo[5],espo[6],espo[7]
      if grup==esperadesCond:
         c.execute("insert into %s values('%s',%s*%s/100)" % (esperades,indicador,esperadesFactor,eap))
c.execute("alter table %s add unique (indicador)" % esperades)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert ignore into %s values('%s', 0)" %(esperades,i))
c.execute("update %s a inner join %s b on a.indicador=b.indicador set deteccioPerResultat=if(deteccio>=prof or prof=0,1,deteccio/prof)" % (ecap_ind,esperades))
c.execute("update %s set resultat=resolucio" % ecap_ind)

c.execute("drop table if exists %s" % metesres)
c.execute("create table %s (indicador varchar(8) not null default '',mmin double,mint double,mmax double,unique (indicador,mmin,mmax))" % metesres)
c.execute("drop table if exists %s" % metesOrigres)
c.execute("create table %s(indicador varchar(8) not null default '',z1 varchar(10) not null default '',z2 varchar(10) not null default '',meta varchar(10) not null default '',z3 varchar(10) not null default '',z4 varchar(10) not null default '',z5 varchar(10) not null default '',z6 varchar(10) not null default '',valor double)" % metesOrigres)
with open(pathmetesres, 'rb') as file:
   m=csv.reader(file, delimiter='{', quotechar='|')
   for met in m:
      indicador,z1,z2,meta,z3,z4,z5,valor = met[0],met[1],met[2],met[3],met[4],met[5],met[6],met[7]
      if z4==esperadesCond:
          c.execute("insert into %s Values('%s','%s','%s','%s','%s','%s','%s','',%s)" % (metesOrigres,indicador,z1,z2,meta,z3,z4,z5,valor))

c.execute("insert into %s select a.indicador,ifnull(c.valor,0)/100,ifnull(b.valor,0)/100,a.valor/100 from (select indicador,valor from %s where meta='AGMMAXRES') a left join (select indicador,valor from %s where meta='AGMINTRES') b on a.indicador=b.indicador left join (select indicador,valor from %s where meta='AGMMINRES') c on a.indicador=c.indicador" % (metesres,metesOrigres,metesOrigres,metesOrigres))
c.execute('update {} set mmin = mmax * 0.9 where mmin > mmax * 0.9'.format(metesres))
c.execute('update {} set mint = (mmax + mmin) / 2'.format(metesres))
c.execute("update {} a inner join {} b on a.indicador = b.indicador set mmin_p = floor(mmin * detectats), mmax_p = floor(mmax * detectats)".format(ecap_ind, metesres))
c.execute('update {} set mmin_p = mmin_p - 1 where mmin_p = mmax_p and mmax_p > 0'.format(ecap_ind))
c.execute("update {} set resolucioPerPunts=if(resolts>=mmax_p,1,if(resolts<mmin_p,0,(resolts-mmin_p)/(mmax_p-mmin_p)))".format(ecap_ind))
c.execute('update {} set mmin_p = mmin_p / detectats, mmax_p = mmax_p / detectats where detectats > 0'.format(ecap_ind))
c.execute("update %s a set resultatPerPunts=resolucioPerPunts*deteccioPerResultat" % (ecap_ind))
c.execute("update %s a set llistat=detectats-resolts" % (ecap_ind))

c.execute("drop table if exists %s" % ponderacio)
c.execute("create table %s (indicador varchar(8) not null default '',tipus varchar(1) not null default '',valor double,unique (indicador,tipus,valor))" % ponderacio)
with open(pathpond, 'rb') as file:
   pon=csv.reader(file, delimiter='{', quotechar='|')
   for indicador, _z, _z, _z, tipus, _z, _z, valor in pon:
      if float(valor) > 0:
          c.execute("insert into %s values('%s','%s',%s)" % (ponderacio,indicador,tipus[0],valor))
c.execute("update %s a left join %s b on a.indicador=b.indicador and a.tipus=b.tipus set punts=resolucioPerPunts*deteccioPerResultat*ifnull(valor,0)" % (ecap_ind,ponderacio))
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % ecap_ind)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts,0 invers,0 mmin_p,0 mmax_p from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in))

c.execute("drop table if exists %s" % khalix_uba)
c.execute("create table %s (up varchar(5),uba varchar(5),tipus varchar(1),indicador varchar(10),analisis varchar(12),detalle varchar(12),valor double)" % khalix_uba)
for r in dimensions:
    analisis,variable, coeficient=r[0],r[1], r[2]
    c.execute("insert into %s select up,uba,tipus,replace(indicador,'EQA','EQAU') indicador,'%s' analisis,'NOINSAT' detalle,round(%s,4)*%s valor from %s%s" % (khalix_uba,analisis,variable,coeficient,ecap_ind,condicio_khalix_uba))

c.execute("drop table if exists %s,%s,%s,%s,%s" % (cataleg,catalegPare,catalegPunts,catalegExclosos,catalegKhx))
c.execute("create table %s (indicador varchar(8),literal varchar(80),ordre int,pare varchar(8),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),curt varchar(80),rsomin double,rsomax double,pantalla varchar(50))" % cataleg)
c.execute("drop table if exists %s" % indi)
c.execute("create table %s (id int, indicador varchar(8), literal varchar(80),grup varchar(8),grup_desc varchar(80),baixa_ int, dbaixa varchar(80),llistats int,ordre int,grup_ordre int)" % indi)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (indi,id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre))
c.execute("insert into %s select distinct a.indicador indicador,literal,ordre,grup pare,llistats llistat,0 invers,prof mdet,if(mmin is null,0,mmin),if(mint is null,0,mint),if(mmax is null,0,mmax),1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',a.indicador) wiki,'' curt,''rsomin,''rsomax,'GENERAL' pantalla from %s a inner join %s b on a.indicador=b.indicador left join %s c on a.indicador=c.indicador where baixa_=0" % (cataleg,indi,esperades,metesres))
if not grip:
    c.execute("update %s set toShow=0 where indicador in %s" % (cataleg,gripInd))
c.execute("create table %s (pare varchar(10) not null default '',literal varchar(80) not null default '',ordre int,curt varchar(80),proces varchar(50))" % catalegPare)
c.execute("insert into %s select distinct grup pare,grup_desc literal,grup_ordre ordre,'' curt,'' proces from %s where baixa_=0" % (catalegPare,indi))
c.execute("create table %s like %s" % (catalegPunts,ponderacio))
c.execute("insert into %s select * from %s" % (catalegPunts,ponderacio))
for tipus in ('M', 'I'):
    c.execute("insert into {0} select indicador, '{1}', 0 from {2} a where not exists (select 1 from {0} b where a.indicador = b.indicador and tipus = '{1}')".format(catalegPunts, tipus, cataleg))
c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s Values('%s','%s','%s','%s')" % (catalegKhx,i,desc,grup,grupdesc))      
         
c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos)
for r in ecap_cataleg_exclosos:
    codi,desc,ordre=r[0],r[1],r[2]
    c.execute("insert into %s VALUES (%d,'%s',%d)" % (catalegExclosos,codi,desc,ordre))

with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         c.execute("delete a.* from %s a where indicador='%s'" % (ecap_ind,i))
         c.execute("delete a.* from %s a where indicador='%s'" % (khalix,i))
         c.execute("delete a.* from %s a where indicador=replace('%s','EQA','EQAU')" % (khalix_uba,i))

#c.execute("update exp_ecap_catalegpunts set valor=0")
#c.execute("update exp_ecap_cataleg set mdet=0")

sql="update {0} a inner join {1} b  on a.indicador=b.indicador set rsomin=round(valor/100,2) where meta='AGMMINRES'".format(cataleg,metesOrigres)
execute(sql,db)
sql="update {0} a inner join {1} b  on a.indicador=b.indicador set rsomax=round(valor/100,2) where meta='AGMMAXRES'".format(cataleg,metesOrigres)
execute(sql,db) 
taula="eqa_ind.mst_eqa_literals_curts"
sql="update {0} a inner join {1} b on a.indicador=b.indicador set a.curt=b.curt".format(cataleg,taula)
execute(sql,db)
sql="update {0} a inner join {1} b on a.pare=b.indicador set a.curt=b.curt".format(catalegPare,taula)
execute(sql,db)

conn.close()
print strftime("%Y-%m-%d %H:%M:%S")
