# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import simplejson as j

import sisapUtils as u


tb = 'ped_eqd'
db = 'pedia'
nod = 'nodrizas'


class Main(object):
    """."""

    def __init__(self):
        """."""
        u.createTable(tb, '(id_cip_sec int, indicador varchar(8), \
                            ates int, institucionalitzat int, maca int, \
                            num int, excl int)', db, rm=True)
        for id, codis in j.load(open("./dades_noesb/ped_eqd.json")).items():
            Indicador(id, codis)


class Indicador(object):
    """."""

    conceptes = {('ped_problemes', 'cod'): 'ps',
                 ('ped_problemes', 'dat'): 'dde',
                 ('ped_variables', 'cod'): 'agrupador',
                 ('ped_variables', 'dat'): 'dat',
                 ('ped_variables', 'val'): 'val'}

    def __init__(self, id, codis):
        """."""
        self.id = id
        self.codis = codis
        self.get_poblacio()
        self.get_querys()
        self.get_denominador()
        self.get_numerador()
        self.get_indicador()
        self.upload_data()

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, ates, institucionalitzat, maca \
               from denominadors where indicador = '{}'"
        self.poblacio = {row[0]: row[1:] for row
                         in u.getAll(sql.format(self.id), db)}

    def get_querys(self):
        """."""
        self.querys = {}
        for concepte in ('num', 'den'):
            taula = self.codis[concepte]['origen']
            agrupadors = self.codis[concepte]['agrupadors']
            temps = self.codis[concepte].get('temps')
            valors = self.codis[concepte].get('valors')
            sql = "select id_cip_sec, {}, {} \
                  from {}, nodrizas.dextraccio \
                  where {} in {}".format(
                            Indicador.conceptes[(taula, 'dat')],
                            Indicador.conceptes.get((taula, 'val'), '\'\''),
                            taula,
                            Indicador.conceptes[(taula, 'cod')],
                            Indicador.get_tuple(agrupadors))
            if temps:
                sql += " and \
                         {} between adddate(data_ext, interval {} {}) and \
                                    adddate(data_ext, interval {} {})".format(
                            Indicador.conceptes[(taula, 'dat')],
                            temps[1], temps[0],
                            temps[2], temps[0])
            if valors and not self.codis[concepte].get('last'):
                sql += " and {} in {}".format(
                            Indicador.conceptes[(taula, 'val')],
                            Indicador.get_tuple(valors))
            self.querys[concepte] = sql

    @staticmethod
    def get_tuple(codis):
        """."""
        if len(codis) == 1:
            return "('{}')".format(codis[0])
        else:
            return tuple(codis)

    def get_denominador(self):
        """."""
        sql = self.querys['den']
        if self.codis['den'].get('last'):
            valors = set(self.codis['den']['valors'])
            dades = {}
            for id, dat, val in u.getAll(sql, nod):
                if id in self.poblacio:
                    if id not in dades or (dat, val) > dades[id]:
                        dades[id] = (dat, val)
            self.denominador = {}
            for id, (dat, val) in dades.items():
                if val in valors:
                    self.denominador[id] = dat
        else:
            self.denominador = {id: dat for (id, dat, val)
                                in u.getAll(sql, nod)
                                if id in self.poblacio}

    def get_numerador(self):
        """."""
        sql = self.querys['num']
        relacio = self.codis['num'].get('relacio')
        self.numerador = set()
        for id, dat, val in u.getAll(sql, nod):
            if id in self.denominador:
                if relacio and relacio[0] == 'month':
                    diff = u.monthsBetween(self.denominador[id], dat)
                if not relacio or relacio[1] <= diff < relacio[2]:
                    self.numerador.add(id)

    def get_indicador(self):
        """."""
        self.indicador = [(id, self.id) +
                          self.poblacio[id] +
                          (1 * (id in self.numerador), 0)
                          for id in self.denominador]

    def upload_data(self):
        """."""
        u.listToTable(self.indicador, tb, db)


if __name__ == '__main__':
    Main()
