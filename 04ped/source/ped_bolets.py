# coding: utf8

"""
Indicadors bolet per pedia que no poden introduir-se en el càlcul general pq
volen coses com ultim valor
"""

import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'pedia'
nod = 'nodrizas'
imp = 'import'
indicadorA = 'EQA1105'
tb = 'ped_bolets'

valida = False

profvalida = {
            1: {'tip': 'M','up': '00349', 'uba': '5'},
            2: {'tip': 'M','up': '05827', 'uba': 'MOVI'},
            3: {'tip': 'I','up': '00280', 'uba': 'INF06'},
            4: {'tip': 'M','up': '07446', 'uba': '408T'},
            5: {'tip': 'I','up': '00120', 'uba': 'INFV'},
            6: {'tip': 'I','up': '05090', 'uba': 'INF5'},
            7: {'tip': 'I','up': '00309', 'uba': 'INF08'},
            8: {'tip': 'I','up': '00055', 'uba': '11'}
            }

u.createTable(tb, '(id_cip_sec int, up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5), indicador varchar(8), \
                            ates int, institucionalitzat int, maca int, \
                            num int, excl int)', db, rm=True)
gravetat = {}
sql = 'select id_cip_sec, dat, val from ped_variables where agrupador=278 union select id_cip_sec, dde, pr_gra from ped_problemes where ps = 58 and pr_gra > 0'
for id, dat, val in u.getAll(sql, nod):
    if (id) in gravetat:
        dat2 = gravetat[(id)]['dat']
        if dat>dat2:
            gravetat[(id)]['dat'] = dat
            gravetat[(id)]['valor'] = val
    else:
        gravetat[(id)] = {'dat':dat, 'valor':val}


asma = {} 
sql = 'select id_cip_sec from ped_problemes where ps=58'
for id, in u.getAll(sql, nod):
    if (id) in gravetat:
        valor = gravetat[(id)]['valor']
        if 2 <= float(valor) <=4:
            asma[(id)] = True

tto = {}
sql = 'select id_cip_sec from eqa_tractaments where farmac=642'
for id, in u.getAll(sql, nod):
    tto[id] = True

upload = []    
sql = 'select id_cip_sec, up, uba, upinf, ubainf, ates, institucionalitzat, maca from ped_assignada where edat_a between 6 and 14'
for id, up, uba, upinf, ubainf, ates, insti, maca in u.getAll(sql, nod):
    if id in asma:
        num = 0
        if id in tto:
            num = 1
        upload.append([id, up, uba,upinf, ubainf,indicadorA, ates, insti, maca, num, 0])
        
u.listToTable(upload, tb, db)

hash = {}
sql = 'select id_cip_sec, hash_d from u11'
for id, hashos in u.getAll(sql, imp):
    hash[id] = hashos
    
uploadnum = []
uploadden = []
if valida:
    for professional in profvalida:
        up = profvalida[professional]['up']
        uba = profvalida[professional]['uba']
        tip = profvalida[professional]['tip']
        if tip == 'M':
            sql = "select id_cip_sec, up, uba from {0} where up = '{1}' and uba = '{2}' and num=1 and ates=1 and institucionalitzat=0".format(tb, up, uba)
            for a,b,c in u.getAll(sql, db):
                if a in hash:
                    h = hash[a]
                    uploadnum.append([h, b, c,''])
            sql = "select id_cip_sec, up, uba from {0} where up = '{1}' and uba = '{2}' and num=0 and ates=1 and institucionalitzat=0".format(tb, up, uba)
            for a,b,c in u.getAll(sql, db):
                if a in hash:
                    h = hash[a]
                    uploadden.append([h, b, c,''])
        elif tip == 'I':
            sql = "select id_cip_sec, upinf, ubainf from {0} where upinf = '{1}' and ubainf = '{2}' and num=1 and ates=1 and institucionalitzat=0".format(tb, up, uba)
            for a,b,c in u.getAll(sql, db):
                if a in hash:
                    h = hash[a]
                    uploadnum.append([h, b, '',c])
            sql = "select id_cip_sec, upinf, ubainf from {0} where upinf = '{1}' and ubainf = '{2}' and num=0 and ates=1 and institucionalitzat=0".format(tb, up, uba)
            for a,b,c in u.getAll(sql, db):
                if a in hash:
                    h = hash[a]
                    uploadden.append([h, b, '',c])

    file = u.tempFolder + 'EQA1105_num1.txt'
    u.writeCSV(file, uploadnum, sep='@')
    file = u.tempFolder + 'EQA1105_num0.txt'
    u.writeCSV(file, uploadden, sep='@')
