from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from validacio import run_validacio

nod="nodrizas"
validation=False

#Output table
table_name="ped_indicador_mant_pes"
db_out="pedia"

#Codis UMI
obes_codi="E66"
pes_codi="TT103"
espina_codi="Q05"
paralisi_codi="G80"
indicador="EQA1106"

##Functions and clases:
class PesNensObesos(object):
    """Weight maintenance or reduction in obese children"""
    def __init__(self):
        self.obesitat_agrupador=self.get_agrupador(obes_codi)
        self.pes_agrupador=self.get_agrupador(pes_codi)
        self.date_dextraccio= self.get_date_dextraccio()
        self.paralisi_agrupador=self.get_agrupador(paralisi_codi)
        self.espina_agrupador=self.get_agrupador(espina_codi)
        printTime("Dia de hoy y agrupadores") 
      

    def get_agrupador(self,codi):
        """Get the agrupador from nodizas.eqa_criteris according with the UMI code"""
        sql="select agrupador from nodrizas.eqa_criteris where criteri_codi= \"{}\";".format(codi)
        return getOne(sql,nod)[0]

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]
        

    def get_nen_dat_pes(self):
        """Search for weight and measurement date by patient. It returns a dictionary with the keys as patient id that points to another dictionary with
           with the keys as measurement dates and the values as weights. {id : {date: weight}}"""
        nen_dat_pes=defaultdict(dict)
        sql="select id_cip_sec,val,dat from {}.ped_variables where agrupador={};".format(nod,self.pes_agrupador)
        for id,pes,dat in getAll(sql,nod):
            nen_dat_pes[id][dat]=pes
        return nen_dat_pes
        
    def get_numerador(self):
        """Get children that have mantained or reduced their weight in the last year. It iterates through the dictionary of : {id : {date: weight}} (algorithm description above)
           It returns a set with the patient id that have reduced or mantained the weight."""
        nen_dat_pes=self.get_nen_dat_pes()
        numerador=set()
        for nen,date_dic in nen_dat_pes.items():
            #de la lista de fechas de medida que se tienen por paciente, consigue la mas reciente.
            most_recent_date=max(set(date_dic.keys()))

            #tiene mas de una fecha y hay menos de 12 meses desde la mas reciente
            if len(date_dic) > 1 and monthsBetween(most_recent_date,self.date_dextraccio) < 12:
                
                #De las fechas que tenemos, selecciona de hace 12 o mas meses 
                dates_12_months_ago=set(date for date in date_dic.keys() if monthsBetween(date, self.date_dextraccio) >= 12)

                if dates_12_months_ago:
                    #si tiene mas de una fecha de hace 12 o mas meses, calcula el cambio de peso desde la mas reciente de esta lista y la fecha mas reciente registrada
                    cambi_pes=self.get_diferencies_pes(date_dic,max(dates_12_months_ago),most_recent_date)

                    if cambi_pes <= 1:
                        #Si el cambio de peso es menor o igual a 0, el id forma parte del numerador
                        numerador.add(nen)
      
        return numerador

    def get_diferencies_pes(self,date_dic,date_antic,date_act):
        """It calculates the difference between current weight and past weight"""
        return float(date_dic[date_act]) - float(date_dic[date_antic])

    def get_exclosos(self):
        """Look for children with espina bifida and paralisis cerebral.
           Returns a set with all the ids that have one or the other condition
        """
        sql="select id_cip_sec from {}.ped_problemes where ps in ({},{});".format(nod,self.espina_agrupador,self.paralisi_agrupador)
        return {id for (id,) in getAll(sql,nod)}
        

    def get_obesos_12meses(self):
        """Look for children diagnosed with obesity more than 11 months ago
           Returns a set of these ids
        """
        sql="select id_cip_sec, dde from {}.ped_problemes where ps={};".format(nod,self.obesitat_agrupador)
        obesos_12_meses=set(id for id,dde in getAll(sql,nod) if monthsBetween(dde,self.date_dextraccio) > 11)
        return obesos_12_meses

    def get_indicador(self):
        """Adaptado de: sisap\08alt\source\prof_od_inr.py  Obtaining final table rows.
           From the ids of children between 6 and 14 years old, select those who are diagnosed with obesity (their id is in obesos_12meses) and mark as numerator
           those who have reduced or mantained weigth in the last 12 months. Exclude those in exclosos set.
           Return rows list grouped by id."""
        table_rows=[]

        #Ids de pacientes con cambio de peso en el ultimo any 
        nens_cambi_pes=self.get_numerador()
        printTime("Cambio Peso")

        #Ids the pacientes diagnosticados con obesidad hace mas de 11 meses
        obesos_12meses=self.get_obesos_12meses()
        printTime("Obesos 12 meses")

        #Ids excluidos
        exclosos=self.get_exclosos()
        printTime("Exclosos")

        sql = "select id_cip_sec, ates, institucionalitzat,maca from {0}.ped_assignada where edat_a>=6 and edat_a<=14;".format(nod)
        for id, ates, inst, maca in getAll(sql,nod):
            if id in obesos_12meses:
                numerador=0
                exclos=0
                if id in nens_cambi_pes:
                    numerador=1
                if id in exclosos:
                    exclos=1
                table_rows.append((id,indicador,ates,inst,maca,numerador,exclos))
        return table_rows
    
    
    def export_table(self,table,columns,db,rows):
        createTable(table, columns, db, rm=True)
        listToTable(rows, table, db)

if __name__=="__main__":
    eqa= PesNensObesos()
    rows=eqa.get_indicador()
    printTime("Indicador")
    eqa.export_table(table_name,
                     "(id_cip_sec int(11), indicador varchar(11), ates int, institucionalitzat int,maca int,num int, exclos int)",
                     db_out,
                     rows)


    if validation:
        validation_dict= {
        1: {'tip': 'M','up': '00349', 'uba': '5'},
        2: {'tip': 'M','up': '05827', 'uba': 'MOVI'},
        3: {'tip': 'I','up': '00280', 'uba': 'INF06'},
        4: {'tip': 'M','up': '07446', 'uba': '408T'},
        5: {'tip': 'I','up': '00120', 'uba': 'INFV'},
        6: {'tip': 'I','up': '05090', 'uba': 'INF5'},
        7: {'tip': 'I','up': '00309', 'uba': 'INF08'},
        8: {'tip': 'I','up': '00055', 'uba': '11'}
        }

        run_validacio(validation_dict,db_out,table_name,indicador)


            


            
