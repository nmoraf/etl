# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re
from validacio import run_validacio

nod="nodrizas"
imp="import"
validation=False

indicador="EQA1010"
table_name="ped_indicador_faring"
db_out="pedia"
agr_faring=(250,251)
agr_antib=(139,140)
agr_num=(759,760)
agr_excl=(761,40)


class FaringNens(object):
    

    def __init__(self):
        self.date_dextraccio= self.get_date_dextraccio()
        self.faring=defaultdict(set)
        self.faring_antib=defaultdict(set)
        self.num=defaultdict(lambda: defaultdict(set))
        self.table_rows=[]

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]
        
    def get_diagnostics(self):
        sql="select id_cip_sec,dde from {}.ped_problemes where ps in {}".format(nod,agr_faring)
        pre_faring=defaultdict(set)
        for (id,dde) in getAll(sql,nod):
            if monthsBetween(dde,self.date_dextraccio) < 12:
                pre_faring[id].add(dde)
        printTime('pre faring')

      
        for id, set_ddes in pre_faring.iteritems():
            sorted_ddes=sorted(set_ddes)
            for i, dde in enumerate(sorted_ddes):
                if i < len(set_ddes) - 1:
                    if daysBetween(sorted_ddes[i],sorted_ddes[i+1]) > 30:
                        self.faring[id].add(sorted_ddes[i])
                else:
                    self.faring[id].add(sorted_ddes[i])
        pre_faring=None



    def get_diagnostics_amb_ant(self):
        sql="select id_cip_sec,pres_orig from {}.eqa_tractaments where farmac in {}".format(nod,agr_antib)
        for (id,pres_orig) in getAll(sql,nod):
            if id in self.faring:
                for dde in self.faring[id]:
                    if -2 <= daysBetween(dde,pres_orig) <= 2:
                        self.faring_antib[id].add(dde)
        self.faring=None


    def get_numerador(self):
        sql="select id_cip_sec,agrupador,dat,val from {}.ped_variables where agrupador in {}".format(nod,agr_num)
        for id, agr,dat,val in getAll(sql,nod):
            val=int(float(val))
            trad={760: val == 1, 759: val >= 3}
            if trad[agr]:
                self.num[agr][id].add(dat)

    def get_exclusions(self):
        sql="select id_cip_sec,dde from {}.eqa_problemes where ps in {}".format(nod,agr_excl)
        self.excl={id for (id,dde) in getAll(sql,nod) }
        
    def get_indicador(self):
        sql = "select id_cip_sec, ates, institucionalitzat,maca from {0}.ped_assignada where edat_a>=3 and edat_a<=14;".format(nod)
        num_intersect=set.intersection(*[ set(self.num[agr].iterkeys()) for agr in agr_num])
        print len(self.num[760]), len(self.num[759])
        for id, ates, inst, maca in getAll(sql,nod):
            if id in self.faring_antib and id not in self.excl:
                denominador=1
                numerador=0
                if id in num_intersect:
                    cumple_tot={}
                    for dde_far in self.faring_antib[id]:
                        cumple={}
                        for agr in agr_num:
                            cumple[agr] = False
                            for dde_num in self.num[agr][id]:
                                if -2 <= daysBetween(dde_far,dde_num) <= 2 or cumple[agr]:
                                    cumple[agr] = True
                        if all(cumple.values()):
                            cumple_tot[dde_far] = True
                        else:
                            cumple_tot[dde_far] = False
                    if all(cumple_tot.values()):
                        numerador=1
                self.table_rows.append((id,indicador,ates,inst,maca,denominador,numerador,0))
      
    
    def export_table(self,table,columns,db):
        """Create a table from a list of rows in the specified db."""
        createTable(table, columns, db, rm=True)
        listToTable(self.table_rows, table, db)

    def execute(self):
        self.get_diagnostics()
        printTime('faring')
        self.get_diagnostics_amb_ant()
        printTime('antib')
        self.get_exclusions()
        printTime('exclos')
        self.get_numerador()
        printTime('num')
        self.get_indicador()
        printTime('indicador')
        

if __name__=="__main__":
    
    eqa=FaringNens()
    eqa.execute()
    
    eqa.export_table(table_name,
                     "(id_cip_sec int(11), indicador varchar(11), ates int, institucionalitzat int,maca int,den int, num int,exclos int)",
                     "pedia")

    if validation:
        validation_dict= {
        1: {'tip': 'M','up': '07446', 'uba': '408T'},
        2: {'tip': 'M','up': '07446', 'uba': '403T'},
        3: {'tip': 'M','up': '05827', 'uba': 'MOVI'},
        4: {'tip': 'M','up': '00349', 'uba': '5'},
        5: {'tip': 'M','up': '00276', 'uba': 'UAB-G'},
        6: {'tip': 'I','up': '00120', 'uba': 'INFV'},
        7: {'tip': 'I','up': '00309', 'uba': 'INF08'},
        8: {'tip': 'I','up': '00055', 'uba': '11'},
        9: {'tip': 'I','up': '05090', 'uba': 'INF5'},

        }

        run_validacio(validation_dict,db_out,table_name,indicador)


    