from sisapUtils import *
import csv, os
#from time import strftime
#print strftime("%Y-%m-%d %H:%M:%S")

indicadors=[]
resultat=[]
path="./dades_noesb/ped_edats.txt"
temp= tempFolder + "temp.txt"
db="pedia"
desti="denominadors"
assignada="nodrizas.ped_assignada"
visites="nodrizas.ped_visites"
dextraccio="nodrizas.dextraccio"

conn= connect((db,'aux'))
e= conn.cursor()

with open(path, 'rb') as file:
    ed=csv.reader(file, delimiter='@', quotechar='|')
    for ind in ed:
        indicadors.append([ind[0],ind[1],int(ind[2]),int(ind[3]),ind[4],int(ind[5]),int(ind[6])])

sql= "select year(data_ext) from %s limit 1" % dextraccio
ext,= getOne(sql,db)
ext=int(ext)
execute("drop table if exists %s" % desti,db)
execute("create table %s (id_cip_sec int,indicador varchar(8),data_naix date,anys int,mesos int,dies int,ates int,institucionalitzat int,maca int)" % desti,db)
sql= "select id_cip_sec,year(data_naix),edat_a,edat_m,edat_d,ates,institucionalitzat,maca,data_naix from %s" % assignada
for id,n,a,m,d,at,ins,maca,naix in getAll(sql,db):
    n=int(n);a=int(a);m=int(m);d=int(d);
    for indicador in indicadors:
        edat= a if indicador[1]=="a" else m if indicador[1]=="m" else d if indicador[1]=="d" else ext-n
        if indicador[2] <= edat <= indicador[3]:
            if indicador[4] == "z":
                ok=1
            else:
                e.execute("select 1 from %s where id_cip_sec=%d and %s between %d and %d limit 1" % (visites,id,'edat_m' if indicador[4]=="m" else "error",int(indicador[5]),int(indicador[6])))
                ok = e.rowcount
            if ok == 1:
                resultat.append([int(id),indicador[0],naix,a,m,d,int(at),int(ins),int(maca)])

with open('%s' % temp, 'wb') as fp:
    a = csv.writer(fp, delimiter='@')
    a.writerows(resultat)

execute("LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (temp,desti),db)
os.remove(temp)
conn.close()
#print strftime("%Y-%m-%d %H:%M:%S")
