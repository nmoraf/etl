from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from validacio import run_validacio

nod="nodrizas"
imp="import"
agr=832
validation=True

codi_indicador="EQA0805"
table_name="ped_indicador_bucodental"
db_out="pedia"


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from nodrizas.dextraccio;"
    return getOne(sql,nod)[0]

def get_cribatge_nens(agr,current_date):
    """Selecciona los pacientes que presenten el agrupador agr y su fecha de diagnostico sea en los ultimos 12 meses.
	   Devuelve un set de ids
	"""
    sql="select id_cip_sec,dat from {}.ped_variables where agrupador={};".format(nod,agr)
    return {id for (id,dat) in getAll(sql,nod) if monthsBetween(dat,current_date) < 12}

def get_rows(numerador):
    sql = "select id_cip_sec, up, upinf, uba, ubainf, edat_m, sexe, ates, institucionalitzat, maca  from {0}.ped_assignada where edat_m>=12 and edat_m<=36;".format(nod)
    rows=[]
    for  id, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca in getAll(sql,nod):
        num=0
        if id in numerador:
            num=1
        rows.append((int(id), codi_indicador,up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca, num,1))
    return rows

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

if __name__ == '__main__':
    printTime("Inicio")
    current_date=get_date_dextraccio()
    print(current_date)

    numerador=get_cribatge_nens(agr,current_date)
    printTime("Numerador")

    rows=get_rows(numerador)
    printTime("Rows OK")

    taula_name="exp_ecap_{}_uba".format(codi_indicador)
    columns="(id_cip_sec int, codi varchar(10),up varchar(8), upinf varchar(8), uba varchar(8),ubainf varchar(8),edat int, sexe varchar(5),ates int, institucionalitzat int, maca int,num int, den int)"
    export_table(taula_name,
				columns,
				db_out,
				rows)