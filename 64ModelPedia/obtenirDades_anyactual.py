# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb el model d'assignació de pediatria. 
Les dades es refereixen al 2017 però com que no tenim la base de dades de 
tancament del 2017 cal treure-ho de l'actual

Només caldria canviar data extraccio a 31/12/2017
"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter

pat_mental= "('P71','P72','P73','P74','P76','P76','P79','P80','P82','P85','P98')"


class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dext, self.dext1a = u.getOne("select date_format(data_ext,'%Y%m%d'), date_format(date_add(date_add(data_ext, interval - 1 year),interval + 1 day), '%Y%m%d') from dextraccio", "nodrizas")
        self.get_centres()
        self.get_nensa()
        self.get_poblacio()
        self.get_hash()
        self.get_gma()
        self.get_medea()
        self.get_atdom()
        self.get_patsocial()
        self.get_patmental()
        self.get_codi_postal()
        self.get_visites()
        self.get_virtuals()
        self.export_dades_pacient()
        self.export_dades_centre()

    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_nensa(self):
        """Busquem els registres de RNSA"""
        self.rnsa = {}
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if dat == 2018 or dat== 2017:
                print table
                sql = "select id_cip_sec, vu_dat_act, vu_up from {} where vu_cod_vs='RNSA'".format(table)
                for id, dat, up in u.getAll(sql, "import"):
                    self.rnsa[(id, dat, up)] = True
    
    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tancament
           - Menors de 15 anys
        """
        self.dades = {}
        self.ambulatoris = set()
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        altapob = {id: alta for id,alta in u.getAll("select id_cip_sec, year(usua_data_alta) \
                                                from assignada, nodrizas.dextraccio", "import")} 
        pobbefore = {id: True for id, in u.getAll("select concat(id_cip_sec, up) \
                                                from assignadahistorica where dataany<2017", "import")}
        sql = "select id_cip_sec, codi_sector, up,  uba, edat, sexe, nacionalitat, institucionalitzat, nivell_cobertura, centre_codi, centre_classe \
                from assignada_tot \
                where edat <15"
        for id, sec, up, uba, edt, sex, nac, insti, cob, cod, cla in u.getAll(sql, "nodrizas"):
            if up in self.centres and up in self.centres:
                identify = str(id) + str(up) 
                alta = altapob[id]
                nouvinguts = 1 if alta == 2018 and edt > 0 and identify not in pobbefore else 0
                nouvingutsimm = 1 if nac in renta and alta == 2018 and edt > 0 and identify not in pobbefore  else 0
                self.ambulatoris.add((cod, cla))
                cob1 = 1 if cob == 'PN' else 0
                nac1 = 1 if nac in renta else 0
                if edt >= 0:
                    self.dades[id] = {
                                      'hash': None, 'sector': sec, 'edat': edt,
                                      'sex': sex, 'gma_cod': None, 'gma_cmplx': None,
                                      'gma_num': None, 'medea': None, 'insti': insti,
                                      'renta': nac1 , 'atdom': 0, 'social': 0, 'mental': 0,
                                      'pensio': cob1, 'nous': nouvinguts, 'nousi': nouvingutsimm, 'up': up,  'uba': uba, 'cp': None, '9C_R': 0, '9D': 0,
                                      '9T': 0, '9E': 0,  'D': 0, 'V': 0, 'RNSA': 0}
        
    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        for id, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash

    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]

    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        sql = ("select id_cip_sec from eqa_problemes where ps = 45",
               "nodrizas")
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['atdom'] = 1

    def get_patsocial(self):
        """Pacients amb patologia social a final del període."""
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio \
                where  (pr_cod_ps like 'Z60%' or pr_cod_ps like 'Z61%' and pr_cod_ps like 'Z62%' and pr_cod_ps like 'Z63%') \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)",
               "import")
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['social'] = 1
    
    def get_patmental(self):
        """Pacients amb patologia mental a final del període."""
        
        pmentalciap = {}
        sql = "select codi_cim10, codi_ciap from import.cat_md_ct_cim10_ciap where codi_ciap in {}".format(pat_mental)
        for cim10, ciap in u.getAll(sql, "import"):
            pmentalciap[cim10] = True
   
        in_crit = tuple(pmentalciap)
        
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(in_crit),
               "import")
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['mental'] = 1
   
    
    def get_codi_postal(self):
        """
        Obté el codi postal dels pacients executant en paral·lel un worker
        contra cadascun dels 21 sectors. Els workers tornen la informació
        en HASH, que convertim a ID per poder creuar amb la resta de dades.
        """
        codi_postal = u.multiprocess(get_codi_postal_worker, u.sectors)
        hash_to_id = {(sector, hash): id for (id, sector, hash)
                      in u.getAll("select id_cip_sec, codi_sector, hash_d \
                                   from u11", "import")}
        for worker in codi_postal:
            for sector, hash, codi in worker:
                if (sector, hash) in hash_to_id:
                    id = hash_to_id[(sector, hash)]
                    if id in self.dades:
                        self.dades[id]['cp'] = codi

    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG o ped.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        Per comptar temps del nen sa només tenim en compte si té una visita coincident amb RNSA
        """
        visitesCentre = {}
        tempsNens = Counter()
        
        sql = "select id_cip_sec, visi_up, visi_tipus_visita, visi_lloc_visita, visi_data_visita, date_format(visi_data_visita,'%Y%m%d') \
                        from visites1 \
                        where visi_situacio_visita = 'R' and visi_servei_codi_servei in ('MG','PED')"
        for id, up, tipus, lloc, data, dat2 in u.getAll(sql, "import"):
            if  self.dext1a <= dat2 <= self.dext:
                if id in self.dades and up in self.centres:
                    if tipus in ('9D', '9T', '9E'):
                        self.dades[id][tipus] += 1
                    elif lloc in ('D'):
                        self.dades[id][lloc] += 1
                    elif tipus in ('9C', '9R') or lloc in ('C'):
                        if (id, data, up) in self.rnsa and (id, data, up) not in visitesCentre:
                            visitesCentre[(id, data, up)] = True
                            tempsNens[(id, 'RNSA')] += 1
                        else:
                            tempsNens[(id, '9C_R')] += 1
                                    
        for (id, tipus), recompte in tempsNens.items():
            self.dades[id][tipus] = recompte   
                            
    
    def get_virtuals(self):
        """
        Agafem les visites virtuals del CMBD-AP, quedant-nos només amb
        les fetes en un EAP de l'ICS per un MF.
        """
        sql = ("select id_cip_sec, cp_up \
                from cmbdap \
                where cp_t_act = 44 and \
                      cp_ecap_dvis between '{}' and '{}' and \
                      cp_ecap_categoria in ('10999','10888')".format(self.dext1a, self.dext),
               "import")
        for id, up in u.getAll(*sql):
            if id in self.dades and up in self.centres:
                self.dades[id]['V'] += 1

    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules."""
        table = 'SISAP_MODEL_PEDIA_uba_{}_2018_prova'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db, rm=True)
        #u.grantSelect(table, ('PREDUMMP', 'PREDUPRP',
        #                      'PREDUECR'), db)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        dades = [(d['hash'], d['edat'], d['sex'], d['gma_cod'], d['gma_cmplx'],
                  d['gma_num'], d['medea'], d['insti'], d['renta'], d['atdom'], d['social'], d['mental'],
                  d['pensio'], d['nous'], d['nousi'], d['up'], d['uba'], d['cp'],
                  d['9C_R'], d['9D'], d['9T'], d['9E'], d['D'], d['V'], d['RNSA'], )
                 for id, d in self.dades.items()]
        columns = ('id varchar(40)', 'edat int', 'sexe varchar(1)',
                   'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'immigracio_baixa_renta int',
                   'atdom int', 'patologia_social int', 'patologia_mental int','pensionista int', 'nouvinguts int', 'nouvinguts_imm int', 'up varchar(5)', 'uba varchar(5)',
                   'codi_postal varchar(5)', 'visites_centre_normals int',
                   'visites_capes_9d int', 'visites_capes_9t int',
                    'visites_capes_9e int',
                   'visites_sequencials_domicili int', 'visites_virtuals int', 'visites_centre_RNSA int')
        Frequentacio.export_dades("PACIENT", columns, dades)

    def export_dades_centre(self):
        """Extreu i exporta les dades de nivell centre."""
        codi_postal = {centre: codi for (centre, codi)
                       in u.getAll("select edce_codi_centre, edce_cpostal \
                                    from cat_pritb120", "import")}
        sql = ("select cent_codi_centre, cent_classe_centre, cent_codi_up \
                from cat_pritb010", "import")
        dades = [(codi, classe, self.centres[up][1], self.centres[up][2],
                  codi_postal[codi], up)
                 for (codi, classe, up)
                 in u.getAll(*sql)
                 if (codi, classe) in self.ambulatoris and up in self.centres]
        columns = ('codi varchar(10)', 'classe varchar(2)',
                   'ambit varchar(2)', 'sap varchar(2)',
                   'codi_postal varchar(5)', 'up varchar(5)')
        Frequentacio.export_dades("CENTRE", columns, dades)

    
def get_codi_postal_worker(sector):
    """Worker per capturar les dades de codi postal d'un sector."""
    c2h = {cip: hash for (cip, hash)
           in u.getAll("select usua_cip, usua_cip_cod from pdptb101",
                       sector + 'a')}
    codi_postal = [(sector, c2h[cip], codi) for (cip, codi)
                   in u.getAll("select usua_cip, usua_codi_postal \
                                from usutb040", sector) if cip in c2h]
    return codi_postal


if __name__ == '__main__':
    Frequentacio()
