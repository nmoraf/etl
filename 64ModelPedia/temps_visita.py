# -*- coding: utf8 -*-

"""
Mirem els temps de visita
"""

any_encurs = False

year = '2017'

tb = "pedia_temps_visita"
db = "permanent"
import sisapUtils as u
from collections import Counter

class temps_pedia(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """."""
        
        self.get_centres()    
        self.get_particions()
        self.get_visites()
        self.export_data()
        
    def get_centres(self):
        """Centres ICS."""
        sql = "select scs_codi, ics_desc \
               from cat_centres where ep = '0208'"
        self.centres = {row[0]: row[1:] for row
                        in  u.getAll(sql, 'nodrizas')}


    def get_particions(self):
        """."""
        self.particions = {}
        sql = "select partition_name, table_name from all_tab_partitions where table_name = 'VISTB043R'"
        for p_name, tb_name in u.getAll(sql, 'redics'):
            self.particions[(p_name)] = True

        
    def get_visites(self):
        """Any de la particio"""
        self.part_vis = {}
        self.visites = []
        for (part), n in self.particions.items():
            print part
            sql = "select to_char(visi_data_visita, 'YYYY') \
                   from vistb043 partition ({}) \
                   where rownum < 2".format(part)
            for anys, in u.getAll(sql, 'redics'):
                if anys == year:
                    print part
                    sql = "select VISI_UP as up, visi_tipus_visita as tipus, visi_lloc_visita as lloc,visi_modul_codi_modul as agenda, VISI_DATA_UPDA as dvisita, \
                        lead (VISI_DATA_UPDA,1) OVER(partition by to_CHAR(VISI_DATA_UPDA, 'DDMMYY'),VISI_UP,visi_modul_codi_modul ORDER BY VISI_UP,visi_modul_codi_modul,VISI_DATA_UPDA) AS VISI_post, \
                        ((lead (VISI_DATA_UPDA,1) OVER(partition by VISI_UP,visi_modul_codi_modul ORDER BY VISI_UP,visi_modul_codi_modul,VISI_DATA_UPDA)-VISI_DATA_UPDA)*1440) as temps, \
                        to_CHAR(VISI_DATA_UPDA, 'HH24:MI') AS HORA, to_CHAR(VISI_DATA_UPDA, 'DDMMYY') AS DIA \
                        from vistb043 partition ({}) \
                        where  VISI_SITUACIO_VISITA='R' AND vISI_SERVEI_CODI_SERVEI='PED' AND MODU_CODI_UAB IS NOT NULL \
                        ORDER BY VISI_UP,visi_modul_codi_modul,VISI_data_upda ASC".format(part)
                    for up, tipus, lloc, agenda, dvisita, visi_post, temps, hora, dia in u.getAll(sql, 'redics'):
                        if up in self.centres:
                            if tipus not in ('9C', '9D', '9R', '9T', '9E'):
                                if lloc == 'C':
                                    tipus = '9C'
                                elif lloc == 'D':
                                    tipus = '9D'      
                            if 1 <= temps <= 45:
                                self.visites.append([up, tipus, dvisita, hora, dia, temps]) 
                        
                        
    def export_data(self):
        columns = ["up varchar(5)", "tipus varchar(10)",  "dvisita varchar(10)",
                   "hora varchar(10)", "dia varchar(10)", "temps double"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.visites, tb, db)
 
    
if __name__ == '__main__':
    temps_pedia()