# coding: iso-8859-1
import sisapUtils as su
import csv, os, sys
from time import strftime
from collections import defaultdict, Counter

db = "altres"
nod = "nodrizas"
imp = "import"

origens = {
    'general': {'assig':'assignada_tot','centres':'cat_centres'},
    'jail': {'assig':'jail_assignada','centres':'jail_centres'}
}

OutFile = su.tempFolder + 'pob.txt'
taula = "exp_ecap_pob"

sql="select data_ext,date_format(data_ext,'%Y%m') from dextraccio"
for d1,d in su.getAll(sql, nod):
    dcalcul=d
    data_ext=d1

OutFileFarm = su.ecapFolder + 'farmacia/POB_FARM_' + dcalcul + '.txt'
FTPFARM = 'POB_FARM_' + dcalcul + '.txt'

OutFileVistb071 = su.ecapFolder + 'vistb071_' + dcalcul + '.txt'
OutFilevistb062 = su.ecapFolder + 'vistb062_' + dcalcul + '.txt'
FTPVISTB = 'vistb062.txt i vistb071.txt ' + dcalcul  

def KhxNacUBA(nac):
    if nac == 724:
        return 'ORIG1'
    elif nac == '':
        return 'ORIG1'
    else:
        return 'ORIG2'

def KhxCobertura(cobertura):
    if cobertura == 'AC':
        return 'USUACT'
    elif cobertura == 'PN':
        return 'USUPEN'
    elif cobertura == 'FG':
        return 'USUPEN'
    else:
        return 'USUALT'

def FarmMajorsConverter(edat):
    return 1 if edat>64 else 0
    
def FarmPediaConverter(edat):
    return 1 if edat<15 else 0

def siapConverter(edat):
    if edat > 74:
        return 75
    elif 65<= edat <= 74:
        return 65
    elif 45<= edat <= 64:
        return 45
    elif 15<= edat <= 44:
        return 15
    elif 8<= edat <= 14:
        return 8
    elif 3<= edat <= 7:
        return 3
    elif 0<= edat <= 2:
        return 0

atdom = {}         
sql = "select id_cip_sec from eqa_problemes where ps=45"
for id, in su.getAll(sql,nod):
    atdom[id] = True
    
nacionalitat = {}
sql = 'select codi_k,codi_nac from cat_nacionalitat'
for k,n in su.getAll(sql,nod):
    nacionalitat[int(n)] = k

tipus = [['M','up','uba'],['I','upinf','ubainf']]

edatsKHX,grupsSiap = {},{}
sql = 'select khalix from khx_edats5a'
for edat, in su.getAll(sql,nod):
    edatsKHX[edat] = True
    
grupsSiap = {75: True,65:True,45:True,15:True,8:True,3:True,0:True}

professionals = {}
sql = "select sector,up, uab, tipus,servei, concat(cognom1,concat(' ',concat(cognom2,concat(', ',nom)))),ide_numcol from cat_professionals where tipus in ('M','I')"
for sector,up,uba,tip,servei,nom,numcol in su.getAll(sql,imp):
    if (up,uba,tip) in professionals:
        continue
    else:
        professionals[(up,uba,tip)] = {'servei':servei,'nom':nom,'numcol':numcol,'sector':sector}
dadesCentres = {}
sql = "select scs_codi,ics_desc,amb_codi,amb_desc,sap_codi,sap_desc from cat_centres"
for up,desc,amb,damb,sap,dsap in su.getAll(sql,nod):
    dadesCentres[up] = {'desc':desc,'amb':amb,'amb_desc':damb,'sap':sap,'sap_desc':dsap}

sectorU11 = {}
sql = "select id_cip_sec,codi_sector from u11"
for id,sec in su.getAll(sql,imp):
    sectorU11[id] = {'sector':sec}

serveisINF = {}
sql ='select codi_sector,uni_codi_up,uni_codi_unitat,uni_servei from cat_vistb059'
for s,up,uba,servei in su.getAll(sql,imp):
    serveisINF[(s,up,uba)] = {'servei':servei}

pobUrg = dict()
sql = "select id_cip_sec,up from urg_poblacio"
for id,up in su.getAll(sql,nod):
    if id in pobUrg:
        pobUrg[id].append(up)
    else:
        pobUrg[id] = [up]    
su.printTime('Inici procés')

su.execute('drop table if exists {}'.format(taula),db)
su.execute("create table {}(up VARCHAR(5) NOT NULL DEFAULT'',uba VARCHAR(5) NOT NULL DEFAULT'', tipus varchar(1) not null default'',sexe varchar(1) NOT NULL DEFAULT'',edat VARCHAR(10) NOT NULL DEFAULT'',ass double NULL default 0, at double NULL default 0,assup double NULL default 0, atup double NULL default 0)".format(taula),db)
pobFarm, vistb071,vistb062,Pobvistb071,Pobvistb062 = {},{},{},{},{}
for r in tipus:
    t,u,b = r[0],r[1],r[2]
    ecapPob,ecapUBA,ecapUP = {},{},{}
    sql = "select id_cip_sec,{0},{1},'{2}',sexe,edat,ates from assignada_tot".format(u,b,t)
    for id,up,uba,tipus,sexe,edat,ates in su.getAll(sql,nod):
        majors = FarmMajorsConverter(edat)
        pedia =  FarmPediaConverter(edat)
        ates = int(ates)
        atdomPac = 0
        sector = sectorU11[id]['sector']
        try:
            if atdom[id]:
                atdomPac = 1
        except KeyError:
            atdomPac = 0
        try:
            if (up,uba,tipus,sexe,su.ageConverter(edat,5)) in ecapUBA:
                ecapUBA[(up,uba,tipus,sexe, su.ageConverter(edat,5))]['ass'] += 1
                ecapUBA[(up,uba,tipus,sexe, su.ageConverter(edat,5))]['at'] += ates
            else:
                ecapUBA[(up,uba,tipus,sexe,su.ageConverter(edat,5))] = {'ass':1, 'at':ates}
        except KeyError:
            ok=1
        try:
            if (up,tipus,sexe,su.ageConverter(edat,5)) in ecapUP:
                ecapUP[(up,tipus,sexe, su.ageConverter(edat,5))]['ass'] += 1
                ecapUP[(up,tipus,sexe, su.ageConverter(edat,5))]['at'] += ates
            else:
                ecapUP[(up,tipus,sexe,su.ageConverter(edat,5))] = {'ass':1, 'at':ates}
        except KeyError:
            ok=1
        for (khalix),d in edatsKHX.items():
            if (up,uba,tipus,sexe,khalix) in ecapPob:
                continue
            else:
                ecapPob[(up,uba,tipus,sexe,khalix)] = True
        if t =='M':
            for (grupsiap),d in grupsSiap.items():
                if (sector,up,uba,grupsiap) in Pobvistb071:
                    continue
                else:
                    Pobvistb071[(sector,up,uba,grupsiap)] = True
            try:
                if (up,uba) in pobFarm:
                    pobFarm[(up,uba)]['total'] += 1
                    pobFarm[(up,uba)]['majors'] += majors
                    pobFarm[(up,uba)]['pedia'] += pedia
                else:
                    pobFarm[(up,uba)] = {'total':1,'majors':majors,'pedia':pedia}
            except KeyError:
                ok=1
            try:
                if (sector,up,uba,siapConverter(edat)) in vistb071:
                    vistb071[(sector,up,uba,siapConverter(edat))] += 1
                else:
                    vistb071[(sector,up,uba,siapConverter(edat))] = 1
            except KeyError:
                ok = 1
        elif t == 'I':
            try:
                servei = serveisINF[(sector,up,uba)]['servei']
            except KeyError:
                ok = 1
            for (grupsiap),d in grupsSiap.items():
                if (sector,up,uba,servei,grupsiap) in Pobvistb062:
                    continue
                else:
                    Pobvistb062[(sector,up,uba,servei,grupsiap)] = True
            try:
                if (sector,up,uba,servei,siapConverter(edat)) in vistb062:
                    vistb062[(sector,up,uba,servei,siapConverter(edat))]['n'] += 1
                    vistb062[(sector,up,uba,servei,siapConverter(edat))]['atdom'] += atdomPac
                else:
                    vistb062[(sector,up,uba,servei,siapConverter(edat))] = {'n':1,'atdom':atdomPac}
            except KeyError:
                ok = 1
    with su.openCSV(OutFile) as c:
        for (u,b,t,s,e),d in ecapPob.items():
            try:
                ass = ecapUBA[(u,b,t,s,e)]['ass']
            except KeyError:
                ass = 0
            try:
                at = ecapUBA[(u,b,t,s,e)]['at']                
            except KeyError:
                at = 0
            try:
                assup = ecapUP[(u,t,s,e)]['ass']
            except KeyError:
                assup = 0
            try:
                atup = ecapUP[(u,t,s,e)]['at']
            except KeyError:
                atup = 0
            c.writerow([u,b,t,s,e,ass,at,assup,atup])
    su.loadData(OutFile,taula,db)

try:
    su.remove(OutFileFarm)
except:
    pass
with su.openCSV(OutFileFarm) as c:
    for (up,uba),d in pobFarm.items():
        tip = 'M'
        try:
            nom = professionals[(up,uba,tip)]['nom']
            numcol = professionals[(up,uba,tip)]['numcol']
        except KeyError:
            continue
        try:
            desc = dadesCentres[up]['desc']
            ambit = dadesCentres[up]['amb']
            ambit_desc = dadesCentres[up]['amb_desc']
            sap = dadesCentres[up]['sap']
            sap_desc = dadesCentres[up]['sap_desc']
        except KeyError:
            continue
        c.writerow([ambit,ambit_desc,sap,sap_desc,up,desc,numcol,nom,d['total'],d['majors'],d['pedia']])
try:
    su.remove(OutFileVistb071)
except:
    pass
with su.openCSV(OutFileVistb071) as c:
    for (sector,up,uba,grupsiap),d in Pobvistb071.items():
        try:
            num = vistb071[(sector,up,uba,grupsiap)]
        except KeyError:
            num = 0
        zero = 0
        c.writerow([sector,up,uba,grupsiap,num,zero,data_ext])
try:
    su.remove(OutFilevistb062)
except:
    pass
with su.openCSV(OutFilevistb062) as c:
    for (sector,up,uba,servei,grupsiap),d in Pobvistb062.items():
        try:
            num = vistb062[(sector,up,uba,servei,grupsiap)]['n']
        except KeyError:
            num = 0
        try:
            atdom = vistb062[(sector,up,uba,servei,grupsiap)]['atdom']
        except KeyError:
            atdom = 0
        zero = 0
        c.writerow([sector,up,uba,servei,grupsiap,zero,num,zero,atdom,data_ext])

cataleg="exp_ecap_cataleg"
pathc="./dades_noesb/pob_cataleg.txt"

su.execute("drop table if exists {}".format(cataleg),db)
su.execute("create table {} (indicador varchar(10) not null default'', literal varchar(1000) not null default'', ordre int)".format(cataleg),db)
with open(pathc, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,desc,ordre= i[0],i[1],i[2]
      su.execute("insert into {0} values('{1}','{2}','{3}')".format(cataleg,ind,desc,ordre),db)

new = []
for origen in origens.keys():
    khalix,khalixUBA,khalixsc,khalixresi,khalixurgencies = {},{},{},{},{}
    sql = "select id_cip_sec,up,uba,upinf,ubainf,sexe,edat,ates,if(nacionalitat='',724,nacionalitat),nivell_cobertura, seccio_censal,up_residencia  from {}".format(origens[origen]['assig'])
    for id,up,uba,upinf,ubainf,sexe,edat,ates,nac,cob,seccio,up_residencia in su.getAll(sql,nod):
        ates = int(ates)
        nac = int(nac)
        tipusM = 'M'
        tipusI = 'I'
        try:
            nacio = nacionalitat[nac]
        except KeyError:
            nacio = 'NAC19'
            if (nac) not in new:
                new.append((nac))
        try:
            if (up,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob)) in khalix:
                khalix[(up,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['ass'] += 1
                khalix[(up,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['at'] += ates
            else:
                khalix[(up,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))] = {'ass':1, 'at':ates}
        except KeyError:
            ok=1
        if origen == 'general':
            try:
                if (seccio,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob)) in khalixsc:
                    khalixsc[(seccio,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['ass'] += 1
                    khalixsc[(seccio,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['at'] += ates
                else:
                    khalixsc[(seccio,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))] = {'ass':1, 'at':ates}
            except KeyError:
                ok=1  
            try:
                if (up_residencia,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob)) in khalixresi:
                    khalixresi[(up_residencia,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['ass'] += 1
                    khalixresi[(up_residencia,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['at'] += ates
                else:
                    khalixresi[(up_residencia,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))] = {'ass':1, 'at':ates}
            except KeyError:
                ok=1  
            if id in pobUrg:
                ups = pobUrg[id] 
                for upcuap in ups:
                    try:
                        if (upcuap,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob)) in khalixurgencies:
                            khalixurgencies[(upcuap,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))]['at'] += 1
                        else:
                            khalixurgencies[(upcuap,su.ageConverter(edat,1),su.sexConverter(sexe),nacio,KhxCobertura(cob))] = {'at':1}
                    except KeyError:
                        ok=1 
        try:
            if (up,tipusM,uba,su.ageConverter(edat,1),KhxNacUBA(nac)) in khalixUBA:
                khalixUBA[(up,tipusM,uba,su.ageConverter(edat,1),KhxNacUBA(nac))]['ass'] += 1
                khalixUBA[(up,tipusM,uba,su.ageConverter(edat,1),KhxNacUBA(nac))]['at'] += ates
            else:
                khalixUBA[(up,tipusM,uba,su.ageConverter(edat,1),KhxNacUBA(nac))] = {'ass':1, 'at':ates}
        except KeyError:
            ok=1
        try:
            if (upinf,tipusI,ubainf,su.ageConverter(edat,1),KhxNacUBA(nac)) in khalixUBA:
                khalixUBA[(upinf,tipusI,ubainf,su.ageConverter(edat,1),KhxNacUBA(nac))]['ass'] += 1
                khalixUBA[(upinf,tipusI,ubainf,su.ageConverter(edat,1),KhxNacUBA(nac))]['at'] += ates
            else:
                khalixUBA[(upinf,tipusI,ubainf,su.ageConverter(edat,1),KhxNacUBA(nac))] = {'ass':1, 'at':ates}
        except KeyError:
            ok=1
    taulaK = "exp_khalix_pob" 
    OutFileK = su.tempFolder + taulaK + origen + '.txt'
    with su.openCSV(OutFileK) as c:
        for (up,e,s,n,cob),d in khalix.items():
            c.writerow([up,e,s,n,cob,d['ass'],d['at']])
    su.execute('drop table if exists {}'.format(taulaK + origen),db)
    su.execute("create table {} (up VARCHAR(5) NOT NULL DEFAULT '', edat VARCHAR(10) NOT NULL DEFAULT '', sexe VARCHAR(5) NOT NULL DEFAULT '', nacionalitat varchar(10) NOT NULL DEFAULT'',\
    nivell_cobertura  VARCHAR(6) NOT NULL DEFAULT '',  ass double null, at double null)".format(taulaK + origen),db)
    su.loadData(OutFileK,taulaK + origen,db)
    
    taulaKUBA = "exp_khalix_pobuba" 
    OutFileK = su.tempFolder + taulaKUBA + origen + '.txt'
    with su.openCSV(OutFileK) as c:
        for (up,tipus,uba,e,nuba),d in khalixUBA.items():
            c.writerow([up,tipus,uba,e,nuba,d['ass'],d['at']])
   
    su.execute('drop table if exists {}'.format(taulaKUBA + origen),db)
    su.execute("create table {} (up VARCHAR(5) NOT NULL DEFAULT '',tipus varchar(1) not null default'',uba VARCHAR(5) NOT NULL DEFAULT '',\
    edat VARCHAR(10) NOT NULL DEFAULT '', nacUBA varchar(10) NOT NULL DEFAULT'', ass double null, at double null)".format(taulaKUBA + origen),db)
    su.loadData(OutFileK,taulaKUBA + origen,db)
    
    error = []
    sql = "select 'POBASS',concat('A','periodo'),ics_codi,nacionalitat,edat,nivell_cobertura,sexe,'N', ass from altres.{0} a inner join nodrizas.{1} b on a.up=scs_codi".format(taulaK + origen,origens[origen]['centres'])
    file = 'POBL_ASS' + ('_JAIL' if origen == 'jail' else '')
    error.append(su.exportKhalix(sql,file))
    
    if origen == 'general':
        sql = "select 'POBASS',concat('A','periodo'),concat(ics_codi,a.tipus,a.uba),nacuba,edat,'NOIMP','DIM6SET','N',ass from altres.{0} a inner join nodrizas.{1} b on a.up=scs_codi {2} where a.uba<>''\
            ".format(taulaKUBA + origen,origens[origen]['centres']," inner join export.khx_ubas c on a.up=c.up and a.uba=c.uba and a.tipus=c.tipus" if origen == 'general' else '')
    elif origen == 'jail':
        sql = "select 'POBASS',concat('A','periodo'),concat(ics_codi,a.uba),nacuba,edat,'NOIMP','DIM6SET','N',ass from altres.{0} a inner join nodrizas.{1} b on a.up=scs_codi {2} where a.uba<>'' and tipus='M'\
            ".format(taulaKUBA + origen,origens[origen]['centres']," inner join export.khx_ubas c on a.up=c.up and a.uba=c.uba and a.tipus=c.tipus" if origen == 'general' else '')
    file="POBL_ASS_UBA" + ('_JAIL' if origen == 'jail' else '')
    error.append(su.exportKhalix(sql,file))
    
    sql = "select 'POBASSAT',concat('A','periodo'),ics_codi,nacionalitat,edat,nivell_cobertura,sexe,'N', at from altres.{0} a inner join nodrizas.{1} b on a.up=scs_codi".format(taulaK + origen,origens[origen]['centres'])
    file = 'POBL_AT' + ('_JAIL' if origen == 'jail' else '')
    error.append(su.exportKhalix(sql,file))
    
    if origen == 'general':
        sql = "select 'POBASSAT',concat('A','periodo'),concat(ics_codi,a.tipus,a.uba),nacuba,edat,'NOIMP','DIM6SET','N',at from altres.{0} a inner join nodrizas.{1} b on a.up=scs_codi {2} where a.uba<>''\
            ".format(taulaKUBA + origen,origens[origen]['centres']," inner join export.khx_ubas c on a.up=c.up and a.uba=c.uba and a.tipus=c.tipus" if origen == 'general' else '')
    elif origen == 'jail':
        sql = "select 'POBASSAT',concat('A','periodo'),concat(ics_codi,a.uba),nacuba,edat,'NOIMP','DIM6SET','N',at from altres.{0} a inner join nodrizas.{1} b on a.up=scs_codi {2} where a.uba<>'' and tipus='M'\
            ".format(taulaKUBA + origen,origens[origen]['centres']," inner join export.khx_ubas c on a.up=c.up and a.uba=c.uba and a.tipus=c.tipus" if origen == 'general' else '')
    file="POBL_AT_UBA" + ('_JAIL' if origen == 'jail' else '')
    error.append(su.exportKhalix(sql,file))
    
taulaKsc = "exp_khalix_pob_sc" 
OutFileKsc = su.tempFolder + taulaKsc + '.txt'
with su.openCSV(OutFileKsc) as c:
    for (seccio,e,s,n,cob),d in khalixsc.items():
        c.writerow([seccio,e,s,n,cob,d['ass'],d['at']])
su.execute('drop table if exists {}'.format(taulaKsc),db)
su.execute("create table {} (seccio_censal VARCHAR(10) NOT NULL DEFAULT '', edat VARCHAR(10) NOT NULL DEFAULT '', sexe VARCHAR(5) NOT NULL DEFAULT '', nacionalitat varchar(10) NOT NULL DEFAULT'',\
nivell_cobertura  VARCHAR(6) NOT NULL DEFAULT '',  ass double null, at double null)".format(taulaKsc),db)
su.loadData(OutFileKsc,taulaKsc,db)

taularesi= "exp_khalix_pob_residencies" 
OutFileresi = su.tempFolder + taularesi + '.txt'
with su.openCSV(OutFileresi) as c:
    for (up,e,s,n,cob),d in khalixresi.items():
        c.writerow([up,e,s,n,cob,d['ass'],d['at']])
su.execute('drop table if exists {}'.format(taularesi),db)
su.execute("create table {} (up_residencia varchar(13) NOT NULL DEFAULT '', edat VARCHAR(10) NOT NULL DEFAULT '', sexe VARCHAR(5) NOT NULL DEFAULT '', nacionalitat varchar(10) NOT NULL DEFAULT'',\
nivell_cobertura  VARCHAR(6) NOT NULL DEFAULT '',  ass double null, at double null)".format(taularesi),db)
su.loadData(OutFileresi,taularesi,db)

taulaurg= "exp_khalix_pob_urgencies" 
OutFileurg = su.tempFolder + taulaurg + '.txt'
with su.openCSV(OutFileurg) as c:
    for (up,e,s,n,cob),d in khalixurgencies.items():
        c.writerow([up,e,s,n,cob,d['at']])
su.execute('drop table if exists {}'.format(taulaurg),db)
su.execute("create table {} (up_cuap VARCHAR(5) NOT NULL DEFAULT '', edat VARCHAR(10) NOT NULL DEFAULT '', sexe VARCHAR(5) NOT NULL DEFAULT '', nacionalitat varchar(10) NOT NULL DEFAULT'',\
nivell_cobertura  VARCHAR(6) NOT NULL DEFAULT '',  at double null)".format(taulaurg),db)
su.loadData(OutFileurg,taulaurg,db)

error = []

sql = "select 'POBASS',concat('A','periodo'),if(seccio_censal ='','NOSC',concat('S',right(seccio_censal,9))),nacionalitat,edat,nivell_cobertura,sexe,'N', ass from altres.{}".format(taulaKsc)
file = 'POBL_ASS_SECCIO_CENSAL'
error.append(su.exportKhalix(sql,file))
    
sql = "select 'POBASSAT',concat('A','periodo'),if(seccio_censal ='','NOSC',concat('S',right(seccio_censal,9))),nacionalitat,edat,nivell_cobertura,sexe,'N', at from altres.{}".format(taulaKsc)
file = 'POBL_AT_SECCIO_CENSAL'
error.append(su.exportKhalix(sql,file))

sql = "select 'POBASS',concat('A','periodo'),if(seccio_censal ='','NOSC',concat('S',left(seccio_censal,7))),nacionalitat,edat,nivell_cobertura,sexe,'N', sum(ass) from altres.{} \
        group by if(seccio_censal ='','NOSC',concat('S',left(seccio_censal,7))),nacionalitat,edat,nivell_cobertura,sexe".format(taulaKsc)
file = 'POBL_ASS_DISTRICTE'
error.append(su.exportKhalix(sql,file))
    
sql = "select 'POBASSAT',concat('A','periodo'),if(seccio_censal ='','NOSC',concat('S',left(seccio_censal,7))),nacionalitat,edat,nivell_cobertura,sexe,'N', sum(at) from altres.{} \
        group by if(seccio_censal ='','NOSC',concat('S',left(seccio_censal,7))),nacionalitat,edat,nivell_cobertura,sexe".format(taulaKsc)
file = 'POBL_AT_DISTRICTE'
error.append(su.exportKhalix(sql,file))

sql = """
        select
            'POBASS',
            concat('A', 'periodo'),
            concat('R', replace(a.up_residencia,'-','_')),
            nacionalitat, edat, nivell_cobertura, sexe, 'N', ass
        from
            altres.{} a
        where
            exists (select 1
                    from
                        import.cat_sisap_map_residencies b
                    where
                        a.up_residencia = b.resi_cod
                        and b.sisap_class = 1
                    )
        """.format(taularesi)
file = 'POBL_ASS_RESIDENCIES'
error.append(su.exportKhalix(sql,file))

sql = """
        select
            'POBASSAT',
            concat('A', 'periodo'),
            concat('R', replace(a.up_residencia,'-','_')),
            nacionalitat, edat, nivell_cobertura, sexe, 'N', at
        from
            altres.{} a
        where
            exists (select 1
                    from
                        import.cat_sisap_map_residencies b
                    where
                        a.up_residencia = b.resi_cod
                        and b.sisap_class = 1
                    )
        """.format(taularesi)
file = 'POBL_AT_RESIDENCIES'
error.append(su.exportKhalix(sql,file))

sql = "select 'POBASSAT',concat('A','periodo'),ics_codi,nacionalitat,edat,nivell_cobertura,sexe,'N', at from altres.{} a inner join nodrizas.urg_centres\
    on up_cuap=scs_codi".format(taulaurg)
file = 'POBL_AT_URGENCIES'
error.append(su.exportKhalix(sql,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

if su.IS_MENSUAL:
    text= 'Ja teniu disponible al FTP el fitxer amb les dades actualitzades. Sisplau, esborreu-lo un cop descarregat.'
    su.sendPolite(['rpdehesa@gencat.cat','far.ics@gencat.cat'],FTPFARM,text)
    su.sendPolite('dpanella.bcn.ics@gencat.cat',FTPVISTB,text)

su.printTime('Fi procés')
