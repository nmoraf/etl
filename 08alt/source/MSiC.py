# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict, Counter
import datetime


nod = 'nodrizas'
imp = 'import'

db = 'redics'
table_redics = 'sisap_dades_ms'

try:
    sql = "create table {0}(periode varchar2(6), rs varchar2(10), aga varchar2(10), indicador varchar2(10), recompte int)".format(table_redics)
    execute(sql,db)
    users= ['PREDUECR','PREDUMMP','PREDUPRP']
    for user in users:
        execute("grant select on {} to {}".format(table_redics,user),db)
except:
    pass

#VISITES

def get_servei(servei, espe):

    if servei == 'MG':
        serdesc = 'MSVISMF'
    elif servei == 'PED':
        serdesc = 'MSVISPED'
    elif servei in ('INFP', 'INFPD','INF','INFMG','ENF','INFG','ATS','GCAS'):
        serdesc = 'MSVISENF'
    elif espe == '05999':
        serdesc = 'MSVISTS'
    elif espe in ('10106', '10777'):
        serdesc = 'MSVISTODN'
    elif espe in ('30084', '30085'):
        serdesc = 'MSVISMATR'
    elif servei == 'LLEV':
        serdesc = 'MSVISMATR'
    else:
        serdesc = 'ERR'
        
    return serdesc
    
    
sql = 'select extract(year_month from data_ext), year(data_ext), month(data_ext) from dextraccio'
for actual, anyactual, mes in getAll(sql, nod):
    anterior = actual -1
    anyanterior = anyactual - 1
    if str(actual)[4:] == '01':
        anterior = str(anyanterior) + '12'
    if mes == 1:
        mes_anterior = 12
    else:
        mes_anterior = mes -1
    anterior = int(anterior)    
    actualfa1a = actual - 100
    anteriorfa1a = anterior - 100

 
inici = datetime.date(2016, 1, 1)
final = datetime.date(2029, 1, 1)

dies_lab = Counter()
for single_date in dateRange(inici, final):
    mes1 = single_date.strftime('%m')
    anys = single_date.year
    periode = str(anys) + str(mes1)
    work = 0
    if isWorkingDay(single_date):
        work = 1
    dies_lab[(periode)] += work

l_odon = {
            '08082':{'rs': 'RS78', 'aga': 'AGA00046'},
            '08083':{'rs': 'RS78', 'aga': 'AGA00070'},
            '02509':{'rs': 'RS62', 'aga': 'AGA00009'},
            '01410':{'rs': 'RS62', 'aga': 'AGA00007'},
          }  
          
l_assir = {
            '01323':{'rs': 'RS64', 'aga': 'AGA00051'},
            '01884':{'rs': 'RS64', 'aga': 'AGA00053'},
            '01315':{'rs': 'RS62', 'aga': 'AGA00007'},
            '01314':{'rs': 'RS62', 'aga': 'AGA00007'},
            '00318':{'rs': 'RS78', 'aga': 'AGA00030'},
            '06754':{'rs': 'RS78', 'aga': 'AGA00066'},
            '00975':{'rs': 'RS63', 'aga': 'AGA00050'},
            '05915':{'rs': 'RS78', 'aga': 'AGA00046'},
            '07262':{'rs': 'RS78', 'aga': 'AGA00071'},
            '07073':{'rs': 'RS78', 'aga': 'AGA00033'},
            '07263':{'rs': 'RS78', 'aga': 'AGA00071'},
          }  
          
l_rhb = {
            '00505':{'rs': 'RS78', 'aga': 'AGA00047'},
            '00504':{'rs': 'RS78', 'aga': 'AGA00071'},
            '07259':{'rs': 'RS78', 'aga': 'AGA00047'},
         }   

centres, saps, aga_rs, rs_c, rs_c2 = {}, {}, {}, {},{}
sql = "select scs_codi, ics_codi, rs, aga, sap_desc from cat_centres where rs<>''"
for up, br, rs, aga, sap in getAll(sql, nod):
    centres[up] = {'br': br, 'rs': rs, 'aga': aga}
    saps[sap] = {'rs': rs, 'aga': aga}
    aga_rs[aga] = rs
    rs_c[rs] = True
  
rs_c[('RS000')] = 'CATALUNYA'
  
sql = 'select rs_cod, rs_des from cat_sisap_agas'
for rs, descrs in getAll(sql, imp):
    rs = 'RS' + str(rs)
    rs_c2[(rs)] = descrs
rs_c2[('RS000')] = 'CATALUNYA'    
for linia in l_odon:
    rs = l_odon[linia]['rs']
    aga = l_odon[linia]['aga']
    centres[linia] = {'br': linia, 'rs': rs, 'aga': aga}
    aga_rs[aga] = rs


ass_centres = {}    
upload_no = []
sql = "select sap_desc, up, br from ass_centres where up not in (select scs_codi from nodrizas.cat_centres)"
for sap, up, br in getAll(sql, nod):
    if sap in saps:
        rs = saps[sap]['rs']
        aga = saps[sap]['aga']
        ass_centres[up] = {'br': br, 'rs': rs, 'aga': aga}
    else:
        for linia in l_assir:
            rs = l_assir[linia]['rs']
            aga = l_assir[linia]['aga']
            aga_rs[aga] = rs
            if linia not in ass_centres:
                ass_centres[linia] = {'br': linia, 'rs': rs, 'aga': aga}
    if up not in ass_centres:
        upload_no.append([up])
 
c_rhb = {}
sql = 'select	a.amb_codi_amb	,a.amb_desc_amb	,s.dap_codi_dap	,s.dap_desc_dap	,c.up_codi_up_scs	,u.up_codi_up_ics,u.up_desc_up_ics \
        from	cat_gcctb006 s,cat_gcctb008 c,cat_gcctb007 u,cat_gcctb005 a\
        where u.dap_codi_dap = s.dap_codi_dap\
        and c.up_codi_up_ics = u.up_codi_up_ics\
		and s.amb_codi_amb=a.amb_codi_amb\
        and c.up_data_baixa = 0 \
        and dap_data_baixa = 0 \
        and u.up_data_baixa = 0 \
        and amb_data_baixa = 0'    
 
for amb, ambdesc,dap,dapdesc,up,br,desc in getAll(sql, 'import'):
    if dapdesc in saps:
        rs = saps[dapdesc]['rs']
        aga = saps[dapdesc]['aga']
        c_rhb[up] = {'dapdesc':dapdesc,'br':br, 'rs':rs, 'aga':aga}
       
       
periodes = {}
recomptes = Counter()
for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), imp)
    if dat == anyactual or (dat == anyanterior and mes == 1):
        sql = "select  codi_sector, visi_up, visi_servei_codi_servei,s_espe_codi_especialitat, extract(year_month from visi_data_visita), visi_tipus_visita, visi_lloc_visita\
                   from {} a, nodrizas.dextraccio where visi_data_baixa = 0 and  visi_situacio_visita ='R' and visi_data_visita <= data_ext".format(table)
        for sector, up, servei, espe, periode, tipusv, llocv in getAll(sql, imp):
            if periode == actual or periode == anterior:
                indicador = get_servei(servei, espe)
                periodes[(periode, indicador)] = True
                if indicador <> 'ERR':
                    if up in centres:
                        rs = centres[up]['rs']
                        aga = centres[up]['aga']
                        recomptes[(periode, rs, aga, indicador)] += 1
                        if indicador == 'MSVISENF' and (tipusv == '9D' or llocv == 'D'):
                            indicador = 'MSVISDOMEN'
                            recomptes[(periode, rs, aga, indicador)] += 1
                    if up in ass_centres:
                        rs = ass_centres[up]['rs']
                        aga = ass_centres[up]['aga']
                        if indicador == 'MSVISMATR':
                            recomptes[(periode, rs, aga, indicador)] += 1 


rhbc = {}
sql = "select id_cip_sec, pf_periode, pf_num_visites_r, pf_num_sessions_r, pf_codi_up from rhb16"
for id, periode, visites, sessions, up in getAll(sql, imp):
    if int(periode) == actual or int(periode) == anterior:
        total = int(visites) + int(sessions)
        if up in centres:
            rs = centres[up]['rs']
            aga = centres[up]['aga']
            recomptes[(periode, rs, aga, 'MSVISFISIO')] += total
        elif up in c_rhb:
            rs = c_rhb[up]['rs']
            aga = c_rhb[up]['aga']
            recomptes[(periode, rs, aga, 'MSVISFISIO')] += total
        elif up in l_rhb:
            rs = l_rhb[up]['rs']
            aga = l_rhb[up]['aga']
            recomptes[(periode, rs, aga, 'MSVISFISIO')] += total
        else:
            rhbc[up] = True
        periodes[(periode, 'MSVISFISIO')] = True

#professionals

def get_prof(tip):

    if tip in ('TIPOMEDA', 'TIPOMEDM'):
        serdesc = 'MSPROFMF'
    elif tip in ('TIPOMEDP'):
        serdesc = 'MSPROFPED'
    elif tip in ('TIPOENFA', 'TIPOENFP'):
        serdesc = 'MSPROFENF'
    else:
        serdesc = 'ERR'
        
    return serdesc

sql = "select up, tipus, count(distinct up, uba) from freq a inner join nodrizas.cat_centres on up=scs_codi group by up, tipus"
for up, tip, n in getAll(sql, "altres"):
    if up in centres:
        n = int(n)
        rs = centres[up]['rs']
        aga = centres[up]['aga']
        indicador = get_prof(tip)
        if indicador <> 'ERR':
            recomptes[(actual, rs, aga, indicador)] += n
            periodes[(actual, indicador)] = True
#assignada

sql = 'select id_cip_sec, edat, up from assignada_tot'
for id, edat, up in getAll(sql, nod):
    if up in centres:
        rs = centres[up]['rs']
        aga = centres[up]['aga']
        recomptes[(actual, rs, aga, 'MSTARJENF')] += 1
        if edat < 15:
            recomptes[(actual, rs, aga, 'MSTARJPED')] += 1
        else:
            recomptes[(actual, rs, aga, 'MSTARJMF')] += 1
        periodes[(actual, 'MSTARJENF')] = True
        periodes[(actual, 'MSTARJPED')] = True
        periodes[(actual, 'MSTARJMF')] = True
        
#carreguem      
upload = []

for (periode, rs, aga, indicador),r in recomptes.items():
    upload.append([periode, rs, aga, indicador, int(r)])

for (periode, indicador), res in periodes.items():
    sql = "delete from {0} where periode='{1}' and indicador = '{2}'".format(table_redics,periode, indicador)
    execute(sql,db)

uploadOra(upload,table_redics,db)


for (up), res in rhbc.items():
    upload_no.append([up])
if upload_no:
    file = tempFolder + 'Assirs_i rhb_sense_AGA.txt'
    writeCSV(file, upload_no, sep=';')   
    text = 'Us fem arribar els centres assir i de rhb que no tenim classificats per aga ni rs'
    sendPolite('ecomaredon@gencat.cat',"ASSIRs sense AGA", text, file)
    os.remove(file)

#Fitxer pel Ministeri

diccionari = {
                'MSPROFMF': {'desc': 'PROFESIONALES MEDICO FAMILIA', 'acumulat': 'promig'},
                'MSPROFPED': {'desc': 'PROFESIONALES PEDIATRIA','acumulat': 'promig'},
                'MSPROFENF': {'desc': 'PROFESIONALES ENFERMERIA','acumulat': 'promig'},
                'MSTARJMF': {'desc': 'TARJETAS MEDICO FAMILIA','acumulat': 'promig'},
                'MSTARJPED': {'desc': 'TARJETAS PEDIATRIA','acumulat': 'promig'},
                'MSTARJENF': {'desc': 'TARJETAS ENFERMERIA','acumulat': 'promig'},
                'MSVISMF': {'desc': 'CONSULTAS MEDICO FAMILIA','acumulat': 'suma'},
                'MSVISPED': {'desc': 'CONSULTAS PEDIATRIA','acumulat': 'suma'},
                'MSVISENF': {'desc': 'CONSULTAS ENFERMERIA','acumulat': 'suma'},
                'MSVISDOMEN': {'desc': 'CONSULTAS DOMICILIO ENFERMERIA','acumulat': 'suma'},
                'MSVISFISIO': {'desc': 'CONSULTAS FISIOTERAPIA','acumulat': 'suma'},
                'MSVISMATR': {'desc': 'CONSULTAS MATRONAS','acumulat': 'suma'},
                'MSVISTS': {'desc': 'CONSULTAS TRABAJADORES SOCIALES','acumulat': 'suma'},
                'MSVISTODN': {'desc': 'CONSULTAS SALUD BUCODENTAL','acumulat': 'suma'},
                'MSRTTAMF': {'desc': 'RATIO MEDICO AP', 'num': 'MSTARJMF', 'den': 'MSPROFMF', 'dies': False,'acumulat': 'promig'},
                'MSRTTAPED': {'desc': 'RATIO PEDIATRIA AP', 'num': 'MSTARJPED', 'den': 'MSPROFPED', 'dies': False,'acumulat': 'promig'},
                'MSRTTAENF': {'desc': 'RATIO ENFERMERIA AP', 'num': 'MSTARJENF', 'den': 'MSPROFENF', 'dies': False,'acumulat': 'promig'},
                'MSPRASMF': {'desc': 'PRESION ASISTENCIAL MEDICO FAMILIA', 'num': 'MSVISMF', 'den': 'MSPROFMF', 'dies': True,'acumulat': 'promig'},
                'MSPRASENF': {'desc': 'PRESION ASISTENCIAL ENFERMERIA', 'num': 'MSVISENF', 'den': 'MSPROFENF', 'dies': True,'acumulat': 'promig'},
                'MSPRASPED': {'desc': 'PRESION ASISTENCIAL PEDIATRIA', 'num': 'MSVISPED', 'den': 'MSPROFPED', 'dies': True,'acumulat': 'promig'},
                'MSFRECMF': {'desc': 'FRECUENTACION MEDICO FAMILIA', 'num': 'MSVISMF', 'den': 'MSTARJMF','dies': False,'acumulat': 'suma'},
                'MSFRECPED': {'desc': 'FRECUENTACION PEDIATRIA', 'num': 'MSVISPED', 'den': 'MSTARJPED','dies': False,'acumulat': 'suma'},
                'MSFRECENF': {'desc': 'FRECUENTACION ENFERMERIA', 'num': 'MSVISENF', 'den': 'MSTARJENF','dies': False,'acumulat': 'suma'},
                
             }
actuals = {}
exp_periodes = {}
sql = "select periode, rs, indicador, sum(recompte) from {0} where rs is not null group by periode, rs, indicador".format(table_redics)
for periode, rs, indicador, n in getAll(sql, db):
    actuals[(rs, periode, indicador)] = n
    exp_periodes[periode] = True

sql = "select periode,  indicador, sum(recompte) from {0} group by periode,  indicador".format(table_redics)
for periode,  indicador, n in getAll(sql, db):
    actuals[('RS000', periode, indicador)] = n


for rs in rs_c:
    for periode in exp_periodes:
        for indicador in diccionari:
            try:
                num = diccionari[indicador]['num']
                den = diccionari[indicador]['den']
                dies = diccionari[indicador]['dies']
            except KeyError:
                continue
            try:
                numactual = actuals[(rs, periode, num)]
                denactual = actuals[(rs,  periode, den)]
                resactual = float(numactual) / float(denactual)
            except KeyError:
                continue
            if dies:
                d_lab = dies_lab[str(periode)]
                resactual = resactual/d_lab
            actuals[(rs,  periode, indicador)] = resactual

        
upload1, upload2 = {},{}
acumulats = Counter()
for (rs, periode, indicador),nc in actuals.items():
    desc = diccionari[indicador]['desc']
    if periode == str(actual):
        n = nc
        n1 = actuals.get((rs, str(actualfa1a), indicador))
        variacio = ((float(n)- float(n1))/float(n1)) if n1 else None
        upload1[(rs, desc)] = {'n': n, 'n1':n1, 'var':variacio}
    if str(periode)[:4] == str(anyactual) and int(periode) <= int(actual):
        acumulats[rs, desc, indicador, 'actual'] += nc
    if str(periode)[:4] == str(anyanterior) and int(periode) <= int(actualfa1a):
        acumulats[rs, desc, indicador, 'fa1any'] += nc

for (rs,desc,indicador, tip), d in acumulats.items():
    if tip == 'actual':
        acumulat = diccionari[indicador]['acumulat']
        d1 = acumulats[(rs, desc,indicador, 'fa1any')]
        if acumulat == 'suma':
            n = d
            n1 = d1
        elif acumulat == 'promig':
            n = float(d)/int(mes)
            n1 = float(d1)/int(mes)
        variacio = ((float(n)- float(n1))/float(n1)) if n1 != 0 else None
        upload2[(rs, desc)] = {'n': n, 'n1':n1, 'var':variacio}
upload = []
for (rs, desc), t in upload1.items():
    descrs = rs_c2[(rs)]
    n = t['n']
    n1 = t['n1']
    var = t['var']
    ac = upload2[(rs,desc)]['n']
    ac1 = upload2[(rs,desc)]['n1']
    acvar = upload2[(rs,desc)]['var']
    upload.append([descrs,desc,str(n).replace('.', ','),str(n1).replace('.', ','),str(var).replace('.', ','),str(ac).replace('.', ','),str(ac1).replace('.', ','),str(acvar).replace('.', ',')])
    
filecsv = tempFolder + 'Indicadores Primaria Ministerio.csv'
writeCSV(filecsv, upload, sep='@') 

#Mes anterior
upload1a, upload2a = {},{}
acumulatsa = Counter()
for (rs, periode, indicador),nc in actuals.items():
    desc = diccionari[indicador]['desc']
    if periode == str(anterior):
        n = nc
        n1 = actuals.get((rs, str(anteriorfa1a), indicador))
        variacio = ((float(n)- float(n1))/float(n1)) if n1 else None
        upload1a[(rs, desc)] = {'n': n, 'n1':n1, 'var':variacio}
    if str(periode)[:4] == str(anterior)[:4] and int(periode) <= int(anterior):
        acumulatsa[rs, desc, indicador, 'actual'] += nc
    if str(periode)[:4] == str(int(str(anterior)[:4]) - 1) and int(periode) <= int(anteriorfa1a):
        acumulatsa[rs, desc, indicador, 'fa1any'] += nc

for (rs,desc,indicador, tip), d in acumulatsa.items():
    if tip == 'actual':
        acumulat = diccionari[indicador]['acumulat']
        d1 = acumulatsa[(rs, desc,indicador, 'fa1any')]
        if acumulat == 'suma':
            n = d
            n1 = d1
        elif acumulat == 'promig':
            n = float(d)/int(mes_anterior)
            n1 = float(d1)/int(mes_anterior)
        variacio = ((float(n)- float(n1))/float(n1)) if n1 != 0 else None
        upload2a[(rs, desc)] = {'n': n, 'n1':n1, 'var':variacio}
uploada = []
for (rs, desc), t in upload1a.items():
    descrs = rs_c2[(rs)]
    n = t['n']
    n1 = t['n1']
    var = t['var']
    ac = upload2a[(rs,desc)]['n']
    ac1 = upload2a[(rs,desc)]['n1']
    acvar = upload2a[(rs,desc)]['var']
    uploada.append([descrs,desc,str(n).replace('.', ','),str(n1).replace('.', ','),str(var).replace('.', ','),str(ac).replace('.', ','),str(ac1).replace('.', ','),str(acvar).replace('.', ',')])
    
filecsva = tempFolder + 'Indicadores Primaria Ministerio mes anterior.csv'
writeCSV(filecsva, uploada, sep='@') 

#fitxer a khalix

table_my = 'exp_sisap_dades_ms'
createTable(table_my, '(periode varchar(6), rs varchar(10), aga varchar(10), indicador varchar(10), recompte int)', "altres", rm=True)

upload = []
sql = "select periode, rs, aga, indicador, recompte from {0} where periode in ('{1}', '{2}')".format(table_redics, actual, anterior)
for periode, rs, aga, indicador, num in getAll(sql, db):
    anys = periode[2:4]
    mesk = periode[4:]
    khal = 'A' + anys + mesk
    upload.append([khal, rs, aga, indicador, num])
listToTable(upload, table_my, "altres")
    
error = []
sql = "select  indicador, periode, aga, 'AGRESULT', 'NOCAT', 'NOIMP','DIM6SET' from {0}.{1}".format("altres", table_my)
file = 'MS_'
error.append(exportKhalix(sql, file))

#enviament


def get_mesenTEXT(mes1):

    if mes1 == 1:
        mes_actual = 'Gener'
    elif mes1 == 2:
        mes_actual = 'Febrer'
    elif mes1 == 3:
        mes_actual = 'Març'
    elif mes1 == 4:
        mes_actual = 'Abril'
    elif mes1 == 5:
        mes_actual = 'Maig'
    elif mes1 == 6:
        mes_actual = 'Juny'
    elif mes1 == 7:
        mes_actual = 'Juliol'
    elif mes1 == 8:
        mes_actual = 'Agost'
    elif mes1 == 9:
        mes_actual = 'Setembre'
    elif mes1 == 10:
        mes_actual = 'Octubre'
    elif mes1 == 11:
        mes_actual = 'Novembre'
    elif mes1 == 12:
        mes_actual = 'Desembre'

    return mes_actual
  

mesentext = get_mesenTEXT(int(mes))



text = 'Us fem arribar les dades dels Indicadors del Ministeri'
assumpte = 'Indicadors Primària Ministeri ' + mesentext 
sendPolite('lgarciaeroles@catsalut.cat', assumpte,text, filecsv)   
os.remove(filecsv)

mesentext = get_mesenTEXT(int(mes_anterior))
print mesentext


text = 'Us fem arribar les dades dels Indicadors del Ministeri del mes anterior'
assumpte = 'Indicadors Primària Ministeri ' + mesentext 
sendPolite('lgarciaeroles@catsalut.cat', assumpte,text, filecsva)  
os.remove(filecsva)

'''
#dades basals, no descomentar. Fitxer a 96informes
upload = []
file = 'dades_basals_MS.txt'
for aga, indicador, periode, n in readCSV(file):
    if aga in aga_rs:
        n = int(n)
        rs = aga_rs[aga]
        upload.append([periode, rs, aga, indicador, n])
         
uploadOra(upload,table_redics,db)
'''  