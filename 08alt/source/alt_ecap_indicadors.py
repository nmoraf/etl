from sisapUtils import *

db = "altres"
pf="alt"
uba = "%s.exp_ecap_alt_uba" % db
tirdetall="%s.exp_ecap_tir_uba_detall" % db
dag = "%s.mst_dag" % db
indicadorDAG="DAG001"
dext = "nodrizas.dextraccio"    #compte
cat = [['%s.exp_ecap_alt_cataleg' % db,pf + 'Cataleg'],['%s.exp_ecap_alt_catalegPare' % db,pf + 'CatalegPare'],['%s.exp_ecap_alt_catalegExclosos' % db,pf + 'CatalegExclosos'],['%s.exp_ecap_tir_catalegDetall' % db,'Tirdetallcataleg'],['%s.exp_ecap_tir_catalegDetallcolumna' % db,'TirdetallcatalegColumna'],
        ['%s.exp_ecap_forats_cataleg' % db, 'accCataleg'], ['%s.exp_ecap_forats_catalegpare' % db, 'accCatalegpare']]

query= "select * from %s" % uba
table= "altIndicadors"
exportPDP(query=query,table=table,dat=True)

query= "select indicador,sum(numerador)/sum(denominador) from %s where tipus='M' group by indicador" % uba
table= "altIndicadorsICS"
exportPDP(query=query,table=table,dat=True)

query= "select * from %s where uba<>''" % tirdetall
table= "TirDetall"
exportPDP(query=query,table=table,dat=True)

query= "select up,usuari,'I','%s',motiu,sum(num),sum(den) from %s group by up,usuari,motiu" % (indicadorDAG,dag)
table= "infIndicadors"
exportPDP(query=query,table=table,dat=True)

query= "select data_ext from %s limit 1" % dext
table= "infindicadorsData"
exportPDP(query=query,table=table,truncate=True)

query= "select * from altres.exp_ecap_forats union \
        select * from altres.exp_ecap_organitzacio union \
        select d0, d1, d2, d3, sum(num), sum(den), sum(num) / sum(den) from permanent.exp_ecap_long_uba where d3 = 'CONT0002' group by d0, d1, d2, d3"
table= "accIndicadors"
exportPDP(query=query,table=table,dat=True)

for r in cat:
   my,ora= r[0],r[1]
   query= "select * from %s" % my
   if my.endswith("alt_cataleg") or my.endswith("forats_cataleg"):
      exportPDP(query=query,table=ora,datAny=True)
   elif my.endswith("exp_ecap_alt_catalegPare") or my.endswith("exp_ecap_forats_catalegpare"):
      exportPDP(query=query,table=ora,datAny=True)
   else:
      exportPDP(query=query,table=ora,truncate=True)
