# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

printTime("Inici")

taules = [('eqa_ind.exp_ecap_uba', 'M', 'EQA adults'),('pedia.exp_ecap_uba', 'M', 'EQA pediatria'),('odn.exp_ecap_uba', 'O', 'EQA Odontologia'),('social.exp_ecap_uba', 'T', 'EQA Treball social'),('ass.exp_ecap_uba', 'G', 'EQA assir')]

catalegs = ['eqa_ind.exp_ecap_cataleg', 'pedia.exp_ecap_cataleg', 'odn.exp_ecap_cataleg', 'social.exp_ecap_cataleg', 'ass.exp_ecap_cataleg']

periode,= getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')

class Control_EQA(object):
    """."""

    def __init__(self):
        """."""
        self.indicadors = []
        self.get_territoris()
        self.get_indicadors()
        self.get_dades()
        self.export_data()

    def get_territoris(self):
        """Obtenim àmbits i sector"""
        
        self.centres = {}
        sql = 'select scs_codi, amb_desc, sector from cat_centres'
        for up, amb, sector in getAll(sql, 'nodrizas'):
            self.centres[up] = {'sector': sector, 'ambit': amb}
        sql = "select amb_desc, up from ass_centres"
        for amb, up in getAll(sql, 'nodrizas'):
            if up not in self.centres:
                self.centres[up] = {'sector': None, 'ambit': amb}
            
    def get_indicadors(self):
        """agafem indicadors del catàleg"""
        
        self.cataleg = {}
        for cat in catalegs:
            sql = "select indicador, literal from {} where pantalla = 'GENERAL'".format(cat)
            for ind, literal in getAll(sql, 'nodrizas'):
                self.cataleg[ind] = literal
                self.indicadors.append(ind)

    def get_dades(self):
        """Obtenim resultats dels indicadors per territori"""
        
        self.resultats = Counter()
        for (taula, tip, eqa) in taules:
            if tip in ('O', 'T'):
                sql = "select  up, indicador, detectats, resolts from {} where tipus = '{}' and uba = '0'".format(taula, tip)
            else:    
                sql = "select   up, indicador, detectats, resolts from {} where tipus = '{}'".format(taula, tip) 
            for  up, ind, det, res in getAll(sql, 'nodrizas'):
                try:
                    ambit = self.centres[up]['ambit']
                    sector = self.centres[up]['sector']
                except KeyError:
                    continue
                try:
                    desc_ind =self.cataleg[ind]
                except KeyError:
                    continue
                self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'detectats'] += det
                self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'resolts'] += res

    def export_data(self):
        """."""
        upload = []
        catal = {}
        for (period, sector, ambit, ind, desc_ind, eqa, tipus), n in self.resultats.items():
            if tipus == 'detectats':
                resolts = self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'resolts']
                upload.append([int(period), sector, ambit, ind, resolts, n])
                catal[ind, desc_ind, eqa] = True
        
        table = 'mst_control_eqa'
        
        indicadors = tuple(self.indicadors)
        sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(table, periode, indicadors)
        execute(sql, "permanent")
        listToTable(upload, table, 'permanent')
        
        tb_cat = 'cat_control_indicadors'
        upload_cat = []
        for (ind, desc_ind, eqa), dadesc in catal.items():
            upload_cat.append([ind, desc_ind, eqa])

        sql = "delete a.* from {0} a where indicador in {1}".format(tb_cat, indicadors)
        execute(sql, 'permanent')
        listToTable(upload_cat, tb_cat, 'permanent')
      
if __name__ == "__main__":
    Control_EQA()  

printTime("Fi")