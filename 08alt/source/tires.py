from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="altres"
conn = connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/tir_indicadors.txt"

dm="tir_pacient"
pac="mst_tir_pacient"
ecap_pac_pre = "tir_ecap_pacient_pre"
ecap_pac = "exp_ecap_tir_pacient"
ecap_marca_exclosos = "if(maca=1,1,if(institucionalitzat=1,2,if(excl=1,4,if(ates=0,5,0))))"
condicio_ecap_llistat = "llistat=1 and excl=0 and num=0"
condicio_ecap_ind = "ates=1 and institucionalitzat=0 and maca=0 and excl=0"
ecap_ind = "exp_ecap_tir_uba" 
ecap_detall1 = "ecap_tir_uba_detall" 
ecap_detall = "exp_ecap_tir_uba_detall" 
tipus=[['M','up','uba'],['I','upinf','ubainf']] 
uba_in="mst_tir_ubas"

up = "eqa_tir_khalix_up_pre"
upuba = "eqa_tir_khalix_uba_pre"
khalix_pre = "eqa_tir_khalix_up_ind"
khalix_preuba = "eqa_tir_khalix_uba_ind"
khalix = "exp_khalix_tir_up_ind"
khalixuba = "exp_khalix_tir_uba_ind"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
condicio_khalix = "excl=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
condicio_khalix_uba=""
conceptes=[['NUM','num'],['DEN','den'],['PACTIRES','tires'],['NTIRES','ntires']]
ecap_cataleg_exclosos = [[0,'Pacients que formen part de l&#39;indicador',0],[1,'Pacients MACA',2],[2,'Pacients institucionalitzats',3],[3,'Pacients exclosos per edat',4],[4,'Pacients exclosos per motius cl&iacute;nics',5],[5,'Pacients no atesos el darrer any',1],[9,'Pacients que compleixen els criteris de l&#39;indicador',9]]

precat="tir_indicadors"

problemes="nodrizas.eqa_problemes"
tractaments="nodrizas.eqa_tractaments"
tires="nodrizas.eqa_tires"
assignada="nodrizas.assignada_tot_with_jail"
u11 = "mst_tir_u11"
u11nod = "nodrizas.eqa_u11"
edats_k = "nodrizas.khx_edats5a"


catalegKhx = "exp_khalix_tir_cataleg"
cataleg = "exp_ecap_tir_cataleg"
catalegDetall = "exp_ecap_tir_catalegDetall"
catalegDetallcolumna = "exp_ecap_tir_catalegDetallcolumna"
catalegPare = "exp_ecap_tir_catalegPare"
catalegExclosos = "exp_ecap_tir_catalegExclosos"



agrupadorDM="ps in ('18','24','487')"
dmtipo2="ps=18"
dmtipo1="ps=24"
dmtipog="ps=487"

concepte = {"TIR001":"D","TIR002":"AdoNS","TIR003":"AdoS","TIR004":"InsL","TIR005":"InsR","TIR006":"DM1","TIR007":"DG"}
farmac = {"TIR001":"farmac=0","TIR002":"farmac=22","TIR003":"farmac=414","TIR004":"farmac=413","TIR005":"farmac=412","TIR006":"farmac=0","TIR007":"farmac=0"}
psdm = {"TIR001":"ps=18","TIR002":"ps=18","TIR003":"ps=18","TIR004":"ps=18","TIR005":"ps=18","TIR006":"ps=24","TIR007":"ps=487"}
dmtipo= {"TIR001":"AdoNS=0 and AdoS=0 and InsL=0 and InsR=0","TIR002":"AdoNS=1 and AdoS=0 and InsL=0 and InsR=0","TIR003":"AdoS=1 and InsL=0 and InsR=0","TIR004":"InsL=1 and InsR=0","TIR005":"InsR=1","TIR006":"DM1=1","TIR007":"DG=1"}
control_tires= {"TIR001":"0","TIR002":"0","TIR003":"3","TIR004":"7","TIR005":"14","TIR006":"49","TIR007":"49"}

agrupatires = [['tires=0','ST'],['tires between 0.01 and 3','T13'],['tires between 3.01 and 7','T47'],['tires between 7.01 and 14','T814'],['tires between 14.01 and 21','T1521'],['tires > 21','T22']]

tipus_dm = [['D','Dieta','1'],['AdoNS','ADO no secretagogs','2'],['AdoS','ADO secretagogs','3'],['InsL','Insulina lenta','4'],['InsR','Insulina rapida','5']]
cat_agrupatires =[['ST','Sense Tires','C1'],['T13','Entre 1 i 3','C2'],['T47','Entre 4 i 7','C3'],['T814','Entre 8 i 14','C4'],['T1521','Entre 15 i 21','C5'],['T22','22 o +','C6']]
total_tires="C1+C2+C3+C4+C5+C6"

criteri_edat="edat>14"

c.execute("drop table if exists %s" % dm)
c.execute("create table %s (id_cip_sec int, ps int,DM2 int)" % dm)
c.execute("insert into %s select id_cip_sec ,ps,1 from %s where %s" % (dm,problemes,dmtipo2))
c.execute("alter table %s add primary key(id_cip_sec)" % dm)
c.execute("insert ignore into %s select id_cip_sec,ps,0 from %s where %s" % (dm,problemes,dmtipo1))
c.execute("insert ignore into %s select id_cip_sec,ps,0 from %s, nodrizas.dextraccio where %s and (dde >= date_add(data_ext,interval -9 month))" % (dm,problemes,dmtipog))
      

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("alter table %s add column %s int" % (dm,concepte[ind]))
      c.execute("update %s set %s=0" % (dm,concepte[ind]))
      c.execute("select distinct index_name from information_schema.statistics where table_schema='%s' and table_name='%s'" % (db,dm))
      idxs = c.fetchall()
      for idx in idxs:
        c.execute("alter table %s drop %s" % (dm,"PRIMARY KEY" if idx[0] == "PRIMARY" else ("index " + idx[0])))
      c.execute("alter table %s add index (id_cip_sec,%s)" % (dm,concepte[ind]))
      c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where %s" % (dm,tractaments,concepte[ind],farmac[ind]))
      if ind=="TIR006" or ind=="TIR007":
        c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where %s" % (dm,tractaments,concepte[ind],psdm[ind]))
      

c.execute("alter table %s add column tipodm varchar(5)" % dm)
c.execute("update %s set tipodm=0" % dm)  


with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("update %s set tipodm='%s' where %s and %s" % (dm,concepte[ind],dmtipo[ind],psdm[ind]))    
 

c.execute("alter table %s add column tires double" % dm)
c.execute("update %s set tires=0" % dm)
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set a.tires=b.tires" % (dm,tires))

c.execute("alter table %s add column num double" % dm)
c.execute("update %s set num=0" % dm)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("update %s set num=1 where tipodm='%s' and tires<=%s" % (dm,concepte[ind],control_tires[ind]))

c.execute("alter table %s add column gruptires varchar(20)" % dm)
c.execute("update %s set gruptires=0" % dm)
      
for r in agrupatires:
    where,ntires=r[0],r[1]
    c.execute("update %s set gruptires='%s' where %s" % (dm,ntires,where))
 
     
c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',tipodm varchar(5),tires double, gruptires varchar(20), up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double,llistat double)" % pac)

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      criteri = "1 = 1" if ind == "TIR006" else criteri_edat
      c.execute("insert into %s select a.id_cip_sec,concat('%s', if(edat < 15, 'P', '')),tipodm,tires,gruptires,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,num,1 den, 0 excl,'%s' llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where tipodm='%s' and %s" % (pac,ind,llistat,dm,assignada,concepte[ind],criteri))

c.execute("drop table if exists %s" % ecap_pac_pre)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',indicador varchar(10) not null default '',maca int,institucionalitzat int,excl int,exclos int,toShow int)" % ecap_pac_pre)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,indicador,maca,institucionalitzat,excl,%s,1 from %s where %s and %s" % (ecap_pac_pre,ecap_marca_exclosos,pac,condicio_ecap_llistat,criteri_edat))
c.execute("alter table %s add index(id_cip_sec)" % ecap_pac_pre)


c.execute("drop table if exists %s" % ecap_pac)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("drop table if exists %s" % u11)
c.execute("create table %s like %s" % (u11,u11nod))
c.execute("insert into %s select * from %s a where exists (select 1 from %s b where toShow=1 and a.id_cip_sec=b.id_cip_sec)" % (u11,u11nod,ecap_pac_pre))
c.execute("alter table %s add unique(id_cip_sec,hash_d,codi_sector)" % u11)
c.execute("insert into %s select a.id_cip_sec,up,uba,upinf,ubainf,indicador as grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1" % (ecap_pac,ecap_pac_pre,u11))


c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double, tires double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,indicador,num,den,tires from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,comb,pac,edats_k,condicio_khalix))

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double,tires double, ntires double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num,sum(den) den,sum(if(tires>0,1,0)) tires,round(sum(tires),2) ntires from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(10) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))      

c.execute("drop table if exists %s" % ecap_ind)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',numerador int,denominador int,resultat double)" % ecap_ind)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,indicador,sum(num) numerador,sum(den) denominador,sum(num)/sum(den) resultat from %s inner join nodrizas.cat_centres on up=scs_codi where %s and %s group by 1,2,3,4" % (ecap_ind,up,uba,tip,pac,condicio_ecap_ind,criteri_edat))

c.execute("drop table if exists %s" % uba_in)
c.execute("create table %s(up varchar(5) not null default '', uba varchar(7) not null default '',tipus varchar(1) not null default '')" % uba_in)
c.execute("insert into %s select up,uba,tipus from %s group by up,uba,tipus" % (uba_in,ecap_ind))

c.execute("alter table %s add unique(up,uba,tipus,indicador)" % ecap_ind)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 numerador,0 denominador, 0 resultat from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in)) 


c.execute("drop table if exists %s" % ecap_detall1)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',dm varchar(10) not null default '',tires varchar(10) not null default '',valor double)" % ecap_detall1)

for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,tipodm dm,gruptires tires, count(*) valor from %s where %s and %s group by 1,2,3,4,5" % (ecap_detall1,up,uba,tip,pac,condicio_ecap_ind,criteri_edat))

c.execute("alter table %s add index (up,uba,tipus,dm,tires,valor)" % ecap_detall1)


c.execute("drop table if exists %s" % ecap_detall)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',dm varchar(10) not null default '',total double)" % ecap_detall)

for s in tipus_dm:
   codi,literal,ordre=s[0],s[1],s[2]
   c.execute("insert into %s select up,uba,tipus,'%s' dm,0 total from %s group by 1,2,3,4" % (ecap_detall,codi,ecap_detall1))

for s in cat_agrupatires:
   codi,literal,columna=s[0],s[1],s[2]
   c.execute("alter table %s add column %s double" % (ecap_detall,columna))
   c.execute("update %s set %s=0" % (ecap_detall,columna))
   c.execute("update %s a inner join %s b on a.up=b.up and a.uba=b.uba and a.tipus=b.tipus and a.dm=b.dm set %s=valor where tires='%s'" % (ecap_detall,ecap_detall1,columna,codi))

c.execute("update %s set total=%s" % (ecap_detall,total_tires)) 

c.execute("drop table if exists %s" % upuba)
c.execute("create table %s (id_cip_sec double, up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double, tires double)" % upuba)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,khalix edat,%s sexe,%s comb,indicador,num,den,tires from %s a inner join %s b on a.edat=b.edat where %s" % (upuba,sexe,comb,pac,edats_k,condicio_khalix))

c.execute("drop table if exists %s" % khalix_preuba)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double,tires double, ntires double)" % khalix_preuba)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,comb,indicador,sum(num) num,sum(den) den,sum(if(tires>0,1,0)) tires,round(sum(tires),2) ntires from %s group by 1,2,3,4,5" % (khalix_preuba,up,uba,tip,upuba))

c.execute("drop table if exists %s" % khalixuba)
c.execute("create table %s (up varchar(5) not null default'',uba varchar(7) not null default '',tipus varchar(1) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(10) not null default '',n double)" % khalixuba)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,uba,tipus,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalixuba,conc,var,khalix_preuba))
    c.execute("insert into %s select up,uba,tipus,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalixuba,conc,var,khalix_preuba))      

      
c.execute("drop table if exists %s,%s,%s,%s,%s,%s" % (cataleg,catalegPare,catalegExclosos,catalegKhx,catalegDetall,catalegDetallcolumna))
c.execute("create table %s (indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),llistat int,invers int,mmin double,mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))" % cataleg)
c.execute("drop table if exists %s" % precat)
c.execute("create table %s (indicador varchar(8), literal varchar(300),grup varchar(8),grup_desc varchar(300),baixa_ int, dbaixa varchar(80),llistats int,ordre int,grup_ordre int,tipusvalor varchar(5), pantalla varchar(20))" % precat)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor, pantalla = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9],ti[10]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (precat,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor, pantalla))
c.execute("insert into %s select distinct indicador,literal,ordre,grup pare,llistats llistat,0 invers,0 mmin,0 mint,0 mmax,1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',indicador) wiki,tipusvalor from %s  where baixa_=0" % (cataleg,precat))
c.execute("create table %s (pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))" % catalegPare)
c.execute("insert into %s select distinct grup pare,grup_desc literal,grup_ordre ordre, pantalla from %s where baixa_=0" % (catalegPare,precat))
c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("insert into %s Values('%s','%s','%s','%s')" % (catalegKhx,i,desc,grup,grupdesc))      
         
c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos)
for r in ecap_cataleg_exclosos:
    codi,desc,ordre=r[0],r[1],r[2]
    c.execute("insert into %s VALUES (%d,'%s',%d)" % (catalegExclosos,codi,desc,ordre))
    
c.execute("create table %s (codi varchar(10) not null default'',literal varchar(30) not null default'',ordre int)" % catalegDetall)
c.execute("create table %s (columna varchar(10) not null default'',literal varchar(30) not null default'')" % catalegDetallcolumna)
for s in tipus_dm:
   codi,literal,ordre=s[0],s[1],s[2]
   c.execute("insert into %s values('%s','%s','%s')" % (catalegDetall,codi,literal,ordre))
for s in cat_agrupatires:
   codi,literal,columna=s[0],s[1],s[2]
   c.execute("insert into %s values('%s','%s')" % (catalegDetallcolumna,columna,literal))
   
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")