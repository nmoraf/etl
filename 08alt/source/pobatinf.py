# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import itertools as it

import sisapUtils as u


DEBUG = False

ORIG_TB = "ag_longitudinalitat_new"
ORIG_DB = "nodrizas"
MASTER_TB = "mst_pobatinf"
KHALIX_TB = "exp_khalix_pobatinf"
DATABASE = "altres"
FILENAME = "POBATESA_INF"

CATEG_EAP = ("30999", "10117", "10999", "10888", "10116", "10777",
             "05999", "30888", "10106", "10112", "10113")
CATEG_INF = "30999"
CATEG_EXTRA = "EXTRA"

KEYS = list(it.product(("POBATINF01", "POBATINF02"), ("NUM", "DEN")))


class Atesa(object):
    """."""

    def __init__(self):
        """."""
        self.get_visites()
        self.get_master()
        self.set_master()
        self.get_centres()
        self.get_ubas()
        self.get_khalix()
        self.set_khalix()
        self.exp_khalix()

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(set)
        if DEBUG:
            visites = [self._get_visites_w("p0")]
        else:
            partitions = u.getTablePartitions(ORIG_TB, ORIG_DB)
            visites = u.multiprocess(self._get_visites_w, partitions)
        for worker in visites:
            for pac, dades in worker:
                self.visites[pac] |= dades

    def _get_visites_w(self, particio):
        """."""
        visites = c.defaultdict(set)
        sql = "select pac_id, pac_up, pac_ubainf, pac_edat, \
                      prof_categ, prof_responsable, prof_delegat,\
                      modul_up, modul_espe, modul_relacio \
               from {} partition({})".format(ORIG_TB, particio)
        for id, up, uba, edat, categ, resp, deleg, up_vis, espe, rel in u.getAll(sql, ORIG_DB):  # noqa
            keys = []
            if up == up_vis and categ in CATEG_EAP:
                keys.append(KEYS[3])
                if categ == CATEG_INF:
                    keys.append(KEYS[2])
                    if espe != CATEG_EXTRA:
                        keys.append("ESIAP")
                if (resp == 1 or deleg == 1 or rel == "1") and espe != CATEG_EXTRA:  # noqa
                    keys.append(KEYS[1])
                    if categ == CATEG_INF:
                        keys.append(KEYS[0])
            for key in keys:
                visites[(id, up, uba, edat)].add(key)
        return visites.items()

    def get_master(self):
        """."""
        self.master = []
        for pac, dades in self.visites.items():
            esiap = (1 * ("ESIAP" in dades),)
            keys = tuple([1 * (key in dades) for key in KEYS])
            self.master.append(pac + esiap + keys)

    def set_master(self):
        """."""
        cols = ["id int", "up varchar(5)", "uba varchar(5)",
                "edat int", "esiap int"]
        cols += ["{}_{} int".format(*key) for key in KEYS]
        cols_str = "({})".format(", ".join(cols))
        u.createTable(MASTER_TB, cols_str, DATABASE, rm=True)
        u.listToTable(self.master, MASTER_TB, DATABASE)

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_ubas(self):
        """."""
        sql = "select up, uba from mst_ubas where tipus = 'I'"
        self.ubas = set([row for row in u.getAll(sql, "eqa_ind")])
        self.ubas |= set([row for row in u.getAll(sql, "pedia")])

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for row in self.master:
            id, up, uba, edat, esiap = row[:5]
            dades = row[5:]
            br = self.centres[up]
            ents = [br]
            if (up, uba) in self.ubas:
                ents.append("{}I{}".format(br, uba))
            for i, key in enumerate(KEYS):
                if dades[i]:
                    for ent in ents:
                        self.khalix[(ent,) + key] += 1

    def set_khalix(self):
        """."""
        cols = "(entity varchar(11), account varchar(10), analysis varchar(3),\
                 value int)"
        u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, KHALIX_TB, DATABASE)

    def exp_khalix(self):
        """."""
        for op, file in (("=", FILENAME), (">", FILENAME + "_UBA")):
            sql = "select account, 'Aperiodo', entity, analysis, \
                          'NOCAT', 'NOIMP', 'DIM6SET', 'N', value \
                   from {}.{} \
                   where length(entity) {} 5".format(DATABASE, KHALIX_TB, op)
            u.exportKhalix(sql, file)


if __name__ == "__main__":
    Atesa()
