from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import random

nod="nodrizas"
imp="import"
limit=""
table_name="exp_khalix_reh"
alt="altres"

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_fitxa_tancada_ult_any_i_actuals(current_date):
    """Conseguir las fichas cerradas en el anterior any y las actuales. Si la ficha tiene la fecha de cierre con menos de 1 mess de diferencia respecto a la actual, se anyade el id (sector-ficha_id)
       a un set y si esta es "None" (en sql "0000-00-00") tambien se anyade al set ya que es actual.
       Devuelve un set de sector-ficha_id
    """
    sql="select concat(codi_sector,'-',fit_id), fit_data_tanca,fit_up from {}.rhb07 {}".format(imp,limit)
    denominador=dict()
    for sector_fit_id,fit_data_tanca,fit_up in getAll(sql,imp):
        if fit_data_tanca == None or monthsBetween(fit_data_tanca,current_date) == 0:
            denominador[sector_fit_id]=fit_up
    return denominador


def get_vis_per_fitxa(current_date,fichas_12meses):
    """Consigue el numero de visitas realizadas por proceso rehabilitador. Se construye un diccionario con las keys igual a los identificadores de fichas (sector-id) y los valores al numero
    de visitas realizadas. Si no hay visitas realizadas no se guarda la key en el diccionario y es como si tuviera 0 visitas realizadas"""
    numerador=defaultdict(Counter)
    sql="select concat(codi_sector,'-',vis_fit_id),vis_data,vis_realitzada from {}.rhb09 ".format(imp) 
    print(sql)
    for sector_fit_id,vis_data,vis_real in getAll(sql,"import"):
        if sector_fit_id in fichas_12meses:
            numerador[sector_fit_id][vis_real]+=1
    return numerador

def get_fitxa_tancada_ult_any(current_date):
    """Conseguir las fichas cerradas en el anterior any. Si la ficha tiene la fecha de cierre con menos de 1 mes de diferencia respecto a la actual, se anyade un tuple con el id (sector-ficha_id)
       y la up a un set.
       Devuelve un set de (sector-ficha_id,fit_up)
    """
    sql="select concat(codi_sector,'-',fit_id), fit_data_tanca,fit_up from {}.rhb07 where fit_data_tanca != '0000-00-00'{};".format(imp,limit)
    denominador=dict()
    for sector_fit_id,fit_data_tanca,fit_up in getAll(sql,"import"):
        if monthsBetween(fit_data_tanca,current_date) == 0:
            denominador[sector_fit_id]=fit_up
    return denominador


def get_indicador_interval(numerador,denominador,codi,interval,current_date):
    indicador=defaultdict(Counter)
    rows=[]
    counter=0
    for id,up in denominador.iteritems():
        indicador[up]["DEN"]+=1
        if id in numerador and interval[0] <= numerador[id] <= interval[1]:
            if up =='07259' and counter < 3:
                print id,numerador[id]
                counter+=1
            indicador[up]["NUM"]+=1
    
    rows= [(current_date.month, current_date.year, up, codi, indicador[up]["NUM"], indicador[up]["DEN"])
                          for up in indicador]   
    return rows

def get_fitxa_inici_tanca(current_date):
    """ Obtiene las fichas que tienen fecha de cierre pero ninguna fecha de inicio, cuyo cierre ha sido hace menos de 1 mes, agrupandolas por sector-id ficha.
        Devuelve un counter a nivel de sector-id ficha. 
    """
    sql="select concat(codi_sector,'-',fit_id), fit_data_tanca from {}.rhb07 where fit_data_inici='' and fit_data_tanca != '0000-00-00'".format(imp)
    numerador=Counter()
    for sector_fit_id,fit_data_tanca in getAll(sql,imp):
        if monthsBetween(fit_data_tanca,current_date) ==0:
            numerador[sector_fit_id]+=1
    return numerador

def get_indicador(numerador,denominador,codi_indicador,current_date):

    indicador=defaultdict(Counter)
    rows=[]

    for (id, up) in denominador.iteritems():
        indicador[up]["DEN"]+=1
        if id in numerador:
            indicador[up]["NUM"]+=numerador[id]
    
    rows= [(current_date.month, current_date.year, up, codi_indicador, indicador[up]["NUM"], indicador[up]["DEN"])
                          for up in indicador]   
    return rows

def get_indicador_up(fitxas12meses,numerador,denominador,codi_indicador,current_date):

    indicador=defaultdict(Counter)
    rows=[]

    for id, up in fitxas12meses.iteritems():
        if id in denominador:
            indicador[up]["DEN"]+=denominador[id]
            if id in numerador:
                indicador[up]["NUM"]+=numerador[id]
    
    rows= [(current_date.month, current_date.year, up, codi_indicador, indicador[up]["NUM"], indicador[up]["DEN"])
                          for up in indicador]   

    return rows

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

def get_sessiones(current_date,fichas_12meses):
    sesiones_fichas=get_vis_per_fitxa(current_date,fichas_12meses)
    printTime("Numerador")

    sesiones_fichas_agrupado= {id_ficha: sum(sesiones_fichas[id_ficha][vis_real] for vis_real in sesiones_fichas[id_ficha]) for id_ficha in sesiones_fichas}
    sesiones_no_realizadas= {id_ficha: sesiones_fichas[id_ficha][vis_real] for id_ficha in sesiones_fichas for vis_real in sesiones_fichas[id_ficha] if vis_real !='S' }

    sesiones_realizadas= {id_ficha: sesiones_fichas[id_ficha]['S'] for id_ficha in sesiones_fichas}
    print len(sesiones_fichas_agrupado), len(sesiones_no_realizadas), len(sesiones_realizadas)
    return sesiones_fichas_agrupado,sesiones_no_realizadas,sesiones_realizadas

if __name__ == '__main__':
    printTime('inici')
    """
    current_date=get_date_dextraccio()
    printTime(current_date,"Dia actual")
    """

    
    anymes = []
    current=datetime.date(2017,11,30)
    end=datetime.date(2018,11,30)

    while current < end:
        anymes.append(current)
        current += relativedelta(months=1)

    list_any_mes=[data.strftime('%Y%m') for data in anymes]

    columns="(mes int(10) ,any int(10), up varchar(5),indicador varchar(11),numerador int, denominador int)"
    createTable(table_name,columns,alt,rm=True)

    for current_date in anymes:
        fichas_12meses=get_fitxa_tancada_ult_any(current_date)
        printTime("Denominador",len(fichas_12meses))
        fichas_alt_y_prev=get_fitxa_tancada_ult_any_i_actuals(current_date)
        print(len(fichas_alt_y_prev))
        
        sesiones_fichas_agrupado,sesiones_no_realizadas,sesiones_realizadas=get_sessiones(current_date,fichas_12meses)
        sesiones_fprev_agrupado,sesiones_no_realizadas_prev,sesiones_realizadas_prev=get_sessiones(current_date,fichas_alt_y_prev)

        
        codi_intervals={
        "REH002A":(0,0),   
        "REH002B": (1,4),
        "REH002C":(5,float("inf"))
        }


        
        for codi,interval in codi_intervals.items():
            rows=get_indicador_interval(sesiones_realizadas,fichas_12meses,codi,interval,current_date)
            printTime('Retrieving rows new table OK')
            listToTable(rows,table_name,alt)
        
        
            
        fit_ini_tanca= get_fitxa_inici_tanca(current_date)
        rows=get_indicador(fit_ini_tanca,fichas_12meses,"REH002",current_date)
        listToTable(rows,table_name,alt)

        codi_indicador="REH005"
        rows=get_indicador(sesiones_realizadas,fichas_12meses,codi_indicador,current_date)
        printTime('Retrieving rows new table OK')
        listToTable(rows,table_name,alt)

        codi_indicador="REH005B"
        rows=get_indicador(sesiones_realizadas_prev,fichas_alt_y_prev,codi_indicador,current_date)
        printTime('Retrieving rows new table OK')
        listToTable(rows,table_name,alt)

        codi_indicador="REH007"
        rows=get_indicador_up(fichas_12meses,sesiones_no_realizadas,sesiones_fichas_agrupado,codi_indicador,current_date)
        listToTable(rows,table_name,alt)
    