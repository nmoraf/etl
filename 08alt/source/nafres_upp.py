import sisapUtils as u
from collections import defaultdict, Counter


file_name = "NAFRESUPP"
nod = "nodrizas"
imp = "import"
alt = "altres"

#  nafres = 833, 834
num_agrs = {"upp": ('91', '92'),
            "atdom": ('91', '92'),
            "nafres": ('833', '834'),
            "uisq": ('885','886'),
            "peu": ('887','888'),
            "einf": ('884','889')}
agr_atdom = 45


ind_opt = {
    "upp": {"prev": "NFC0001", "inc": "NFC0003"},
    "atdom": {"prev": "NFC0002", "inc": "NFC0004"},
    "nafres": {"prev": "NFC0005", "inc": "NFC0006"},
    "uisq": {"prev": "NFC0007", "inc": "NFC0008"},
    "peu": {"prev": "NFC0009", "inc": "NFC0010"},
    "einf": {"prev": "NFC0011", "inc": "NFC0012"}
}


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql = "select data_ext from {}.dextraccio;".format(nod)
    return u.getOne(sql, nod)[0]


def get_pob_agr(agr):
    """
    Selecciona los pacientes con un determinador agrupador (agr).
    Devuelve un set de ids.
    """
    sql = "select id_cip_sec from {}.eqa_problemes where ps = {}".format(nod, agr)  # noqa
    return {id for (id, ) in u.getAll(sql, nod)}


def get_id_agr_prev_inc(agrs, current_date):
    """
    Selecciona los pacientes que tengan alguno de los agrupadores en agrs.
    Se guardan dos valores, la prevalencia, que son los pacientes que presentan
    el agrupador.
    y la incidencia, que son aquellos pacientes a los que se les ha
    diagnosticado el agr en los ultimos 12 meses.
    Se devuelve un diccionario con el formato:
        {agr: {"prev": set(ids), "inc": set(ids)}
    """
    id_agrs = defaultdict(lambda: defaultdict(set))
    sql = """
        SELECT id_cip_sec, ps, dde
        FROM {}.eqa_problemes where ps in ({})
        """.format(nod, ", ".join(agrs))
    print(sql)

    for id, agr, dde in u.getAll(sql, nod):
        agr = str(agr)
        id_agrs[agr]["prev"].add(id)
        if u.monthsBetween(dde, current_date) < 12:
            id_agrs[agr]["inc"].add(id)
    return id_agrs


def get_indicador(codi, num, atdom_pob=None):
    """
    Construye el indicador a nivel de up. Si atdom_pob es dada (un set de ids),
    el denominador se construye con los ids que estan dentro de esta poblacion.
    En caso contrario, es el total de personas en la up.
    El numerador se obtiene a partir de num, otro set de ids.
       Devuelve una lista de filas (tuples).
    """
    indicador = defaultdict(Counter)
    sql = "select id_cip_sec, up from {}.assignada_tot".format(nod)

    for id, up in u.getAll(sql, nod):
        denominador = True
        if atdom_pob:
            denominador = id in atdom_pob

        if denominador:
            indicador[up]["DEN"] += 1
            if id in num:
                indicador[up]["NUM"] += 1

    return [(up, codi, indicador[up]["NUM"], indicador[up]["DEN"])
            for up in indicador]


def export_table(table, columns, db, rows):
    u.createTable(table, columns, db, rm=True)
    u.listToTable(rows, table, db)


def push_to_khalix(taula, taula_centres, file_name, khalix=False):
    query_template_string = """
        SELECT
            indicador, concat('A', 'periodo'), ics_codi, {strg},
            'NOCAT', 'NOIMP', 'DIM6SET', 'N', {var}
        FROM
            altres.{taula} a INNER JOIN
            nodrizas.{taula_centres} b
                on a.up = scs_codi
        WHERE a.{var} <> 0
    """
    query_string1 = query_template_string.format(strg="'NUM'",
                                                 var="numerador",
                                                 taula=taula,
                                                 taula_centres=taula_centres)

    query_string2 = query_template_string.format(strg="'DEN'",
                                                 var="denominador",
                                                 taula=taula,
                                                 taula_centres=taula_centres)

    query_string = query_string1 + " union " + query_string2

    print(query_string)
    if khalix:
        u.exportKhalix(query_string, file_name)


if __name__ == '__main__':
    u.printTime('inici')
    current_date = get_date_dextraccio()
    u.printTime(current_date)
    # Se crea la tabla
    table_name = "exp_khalix_nafres_upp_uba"
    uba_columns = "(up varchar(5), indicador varchar(15), numerador int, denominador int)"  # noqa
    u.createTable(table_name, uba_columns, alt, rm=True)

    # Se genera la lista de agrs para el numerador
    # y se pasa a la funcion que nos dara los ids para cada uno de ellos
    # (prev e inc)
    agrs_list = list(el for tuplee in set(num_agrs.values()) for el in tuplee)
    ids_agr = get_id_agr_prev_inc(agrs_list, current_date)
    u.printTime("ids_agr")

    # Se obtiene la poblacion atdom
    pob_atdom = get_pob_agr(agr_atdom)
    u.printTime("atdom_pob")

    for key in ind_opt:
        print(key)
        # Para cada key (upp, atdom, nafres):

        # Diccionario de opciones para determinar si el denominador es:
        #   pob_atdom o el total de gente de la up (opcion None)
        trad_dict = {"upp": None, "atdom": pob_atdom, "nafres": None, "uisq": None, "peu": None, "einf": None}
        atdom_pob = trad_dict[key]

        # El agrupador que va a determinar el numerador,
        # para cada key, segun el tipo
        num_by_type = {"inc": num_agrs[key], "prev": (num_agrs[key][0],)}

        for type_num, codi in ind_opt[key].iteritems():
            # Una vez obtenido el tipo, selecciona los agrupadores (tuple)
            # que van a dar el numerador
            agrs_for_num = num_by_type[type_num]

            # Obtiene los ids del numerador
            # haciendo una union con los sets de ids
            # correspondientes al agr y al tipo
            num = set.union(*[ids_agr[num_agr][type_num]
                              for num_agr in agrs_for_num])

            # Se obtienen las rows por up para cada tipo de indicador
            # (prev o inc) dentro de una key (upp, atdom, nafres)
            rows = get_indicador(codi, num, atdom_pob)

            u.listToTable(rows, table_name, alt)
            u.printTime("Insert into the table {}".format(codi))

    push_to_khalix(table_name, "cat_centres", file_name, khalix=True)
