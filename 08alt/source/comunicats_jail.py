from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
#Validation
file_name="COMUNICATS_JAIL"
validation=True
up_codi=("00380","00368","00378")

nod="nodrizas"
alt = "altres"
db = "6951"
codi_indicador="COMJUTJAT"

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_rows(current_date):
	sql="select com_data,com_up, com_assist_per_fop,com_assist_per_altres,com_relat_pacient,com_desc_lesio,com_recom_observ,com_pronostic from vistb018"
	
	indicador=defaultdict(Counter)

	for com_data,com_up,com_assist_per_fop,com_assist_per_altres,com_relat_pacient,com_desc_lesio,com_recom_observ,com_pronostic in getAll(sql,db):

		descripciones=[field if field and len(field)>10 else None for field in com_relat_pacient,com_desc_lesio]
		more_fields=[com_recom_observ,com_pronostic]+descripciones
		
		if monthsBetween(com_data,current_date) < 12:
			indicador[com_up]["DEN"]+=1
			if None not in more_fields and (com_assist_per_fop or com_assist_per_altres):
				indicador[com_up]["NUM"]+=1
	
	rows= [(up, codi_indicador,indicador[up]["NUM"], indicador[up]["DEN"])
                          for up in indicador]
	return rows


def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)
	
	
if __name__ == '__main__':

	rows=get_rows(get_date_dextraccio())
	
	for row in rows[:3]:
		print(row)
	taula_name="exp_ecap_{}_uba".format(codi_indicador)
	columns="(up varchar(5),indicador varchar(11),numerador int, denominador int)"
	export_table(taula_name,
				columns,
				alt,
				rows)
	
	query_template_string="select indicador,concat('A','periodo'),ics_codi,{strg},'NOCAT','NOIMP','DIM6SET','N',{var} from altres.{taula} a inner join nodrizas.jail_centres b on a.up=scs_codi where a.{var} != 0"
	query_string=query_template_string.format(strg="'NUM'",var="numerador",taula=taula_name)+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=taula_name)
	print(query_string)
	exportKhalix(query_string,file_name)