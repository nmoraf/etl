# coding: utf8

"""
Procés per obtenir les variables bàsiques per a anàlisis de factors que influeixen en els indicadors
"""

import urllib as w
import sys,datetime
from time import strftime
import sisapUtils as u
from collections import Counter

db = 'permanent'


class VariAnalisi(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_gma()
        self.get_medea()
        self.get_atdom()
        self.get_poblacio()
        self.export_data()        

    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi, ep \
                from cat_centres", "nodrizas")
        self.centres = {up: (br, ep) for (up, br, amb, sap, ep)
                        in u.getAll(*sql)}     
    
    def get_gma(self):
        """Obté la complexitat del gma"""
        self.gma = {}
        sql = ("select id_cip_sec, gma_ind_cmplx \
                from gma", "import")
        for id, cmplx in u.getAll(*sql):
            self.gma[id] = cmplx
            
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        self.medea = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if sector in valors:
                self.medea[id] = valors[sector]

    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdom = {}
        sql = ("select id_cip_sec from eqa_problemes where ps = 45",
               "nodrizas")
        for id, in u.getAll(*sql):
            self.atdom[id] = True
  
    def get_poblacio(self):
        """."""
        self.dades = Counter()

        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, up, edat, sexe, ates, \
                       institucionalitzat, nacionalitat, nivell_cobertura, pcc, maca \
                from assignada_tot", "nodrizas")
        for id, up, edat, sexe, ates, insti, nac, cob, pcc, maca in u.getAll(*sql):
            if up in self.centres:
                br = self.centres[up][0]
                ep = self.centres[up][1]
                cob1 = 1 if cob == 'PN' else 0
                nac1 = 1 if nac in renta else 0
                atdom1 = self.atdom[id] if id in self.atdom else 0
                edat_k = u.ageConverter(edat)
                sexe_k = u.sexConverter(sexe)
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'rec')]+= 1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'edat')]+= edat
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'insti')]+= insti
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'immi')]+= nac1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'cob')]+= cob1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'pcc')]+= pcc
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'maca')]+= maca
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'atdom')]+= atdom1
                if id in self.gma:
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'den_gma')]+= 1
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'num_gma')]+= self.gma[id]
                if id in self.medea:
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'den_medea')]+= 1
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'num_medea')]+= self.medea[id]
    
    def export_data(self):
        """."""
        upload = []
        for (up, br, ep, ates, edat, sexe, tipus), n in self.dades.items():
            if tipus == 'rec':
                sum_dona = n if sexe == 'DONA' else 0
                upload.append([up, br, ep, ates, edat, sexe, n, sum_dona, self.dades[(up, br, ep, ates, edat, sexe,'edat')], self.dades[(up, br, ep, ates, edat, sexe,'insti')],
                self.dades[(up, br, ep, ates, edat, sexe,'immi')], self.dades[(up, br, ep, ates, edat, sexe,'cob')], self.dades[(up, br, ep, ates, edat, sexe,'pcc')],
                self.dades[(up, br, ep, ates, edat, sexe,'maca')], self.dades[(up, br, ep, ates, edat, sexe,'atdom')], self.dades[(up, br, ep, ates, edat, sexe,'den_gma')],
                self.dades[(up, br, ep, ates, edat, sexe,'num_gma')], self.dades[(up, br, ep, ates, edat, sexe,'den_medea')], self.dades[(up, br, ep, ates, edat, sexe,'num_medea')]])
        u.listToTable(upload, tb, db)
                
if __name__ == '__main__':
    u.printTime("Inici")
    
    tb = "mst_variables_analisi"
    columns =  ["up varchar(5)", "br varchar(5)", "ep varchar(10)", "ates int", "edat varchar(10)", "sexe varchar(5)", "poblacio int", "sum_dona double", "sum_edat double", "institucionalitzats int",
                "immigrants_renta_baixa int", "pensionistes int", "pcc int", "maca int", "atdom int", "den_gma int", "num_gma double", 
                "den_medea int", "num_medea double"]     
    u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    
    VariAnalisi()
    
    u.printTime("Fi")
