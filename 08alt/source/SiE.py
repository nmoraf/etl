# coding=latin1
from sisapUtils import *
import sys,datetime
import string
from time import strftime
from collections import defaultdict,Counter

prstb690="SISAP_XML"
redics="redics"
nod="nodrizas"
imp='import'
alt="altres"
file_name='SALUTIESCOLA'

cursos="N_1_ESO,N_2_ESO,N_3_ESO,N_4_ESO,N_1_BATX,N_2_BATX"

#Diccionarios en el que se traduce el valor del xml y su significado para ciertos campos del xml que nos interesa
homolog_cursos= {   '2':'ESO1',
                    '3':'ESO2',
                    '4':'ESO3',
                    '5':'ESO4',
                    '6':'BATX1',
                    '7':'BATX2'

}

homolog_orig={ 
'2':	'TORIGEN03',
'7':	'TORIGEN08',
'1':	'TORIGEN02',
'5':	'TORIGEN06',
'3':	'TORIGEN04',
'0':	'TORIGEN01',
'8':	'TORIGEN09',
'4':	'TORIGEN05',
'6'	:'TORIGEN07'}


homolog_der= {
'1':	'Metge AP',
'8':	'Equips directiu i psicopedag�gic escolar',
'2':	'Infermera AP',
'5':	'ASSIR',
'7':	'Serveis socials',
'3':	'CSMIJ',
'6':	'CAS',
'4':	'CSMA',
'Altres14':'Altres'
}

#en esta variable se guardan los campos de la tabla que nos obtener los xml que lo tienen registrado, pero el valor da igual
camps_codi_out=['SiE_Aliment_A',"SiE_Aliment_Altres",
            "SiE_Aliment_HabitIna","SiE_SexiAfec_A","SiE_SexiAfec_Altres",
            "SiE_SexiAfec_Anticon","SiE_SexiAfec_Conturg","SiE_SexiAfec_ITS", "SiE_Consum_A", 
            "SiE_Consum_Altres_1","SiE_Consum_Tabac","SiE_Consum_Alcohol","SiE_Consum_Cannabis","SiE_Consum_Psicofarm",
            "SiE_AltresTemes"]

#Creando dinamicamente algunos campos que son subtipos y repiten parte del nombre 
for motiu in ["BenestarE", "SalutSoc"]:
    camps_codi_out.append("SiE_{}".format(motiu))
    camps_codi_out.append("SiE_{}_Altres".format(motiu))

for tipo_maltrat in ["Familiar","Carrer","Parella","ASexual","A","Altres"]:
    camps_codi_out.append("SiE_Maltrat_{}".format(tipo_maltrat))

#DICCIONARIO PARA AGRUPAR TODOS LOS CAMP_CODI QUE HACEN REFERENCIA A LO MISMO (Por ejemplo, todos los SiE_Maltrat_xxxx) EN UNA MISMA ETIQUETA
#SE USA LUEGO
keys={ "Aliment": "Salut aliment�ria","SexiA":"Sexualitat i afectivitat","Bene":"Benestar Emocional","SalutSoc":"Salut social i entorn",
      "Consum":"Consum subst�ncies", "AltresT":"Altres temes", "Maltrat":"Maltractament i viol�ncia"}


class SiE(object):
    def __init__(self,table):
        self.codis=['CONSOB01','CONSOB02','CONSOB03']
        self.subindicadores= self.get_subindicadors()
       
        self.table=table
        self.current_date=self.get_date_dextraccio()
        #Los campos de los que queremos obtener datos del xml
        self.camp_codis= self.get_camps_codi()
        
        #Los campos del xml que nos van a servir como dimensaiones de la tabla
        self.camp_dimensions= {'SiE_Origen_Visita','SexePacient'}
     
        self.up_desc={}
        self.esc_desc={}

        #Obtenemos las enfermeras del catalogo
        self.infs=self.get_infs()

        #Para cada xml, que enfermera se ha encargado
        self.xml_inf=self.get_id_inf()

        #Para cada xml, los campos que nos interesan
        self.xml_data=self.get_xml_data()

        #Cada agrupar para cada up, las escuelas asociadas y el numero de nens de los cursos especificados
        self.up_escola=self.get_escolas_up()

        #Asocia para cada escuela, curso y nen los xml_id que ha realizado de SiE
        self.id_per_escola=self.get_ind_pac()
       
    def get_camps_codi(self):
        """ Funcion para obtener para cada codigo exacto de la tabla de xml, y, si procede, vincularlo al diccionario de homologacion
            de los valores del xml a las etiquetas de khalix.
            Devuelve un {camp_codi: {val_xml:etiqueta}}
        """
        camp_codis= {  'SiE_Curs':homolog_cursos,
                            'SiE_TipVisita_A': {'0':'Primera','1':'Seguiment'},
                            'SiE_TipVisita_B': {'0':'Presencial','1':'Tlf','2':'Virtual'},
                            'SiE_Origen_Visita':homolog_orig,
                            'SexePacient':{'Home': 'HOME', 'Dona': 'DONA'},
                            'SiE_DerivatA':homolog_der}
        for camp_codi in camps_codi_out:
            camp_codis[camp_codi]=None
        return camp_codis

    def get_subindicadors(self):
        """ Obtiene un diccionario para cada uno de los indicadores, que senyala a un tuple con el subindex, el campo del xml, 
            y si se quiere xmls que tengan registrado ese campo (in) o que no lo tengan (out). Este tuple es la key para el valor que se quiere para ese campo en concreto.
            Si solo se quiere que se tenga el campo, da igual el valor, (CONSOB05), se pone como valor un 1.
            Sino quiere que se tenga el campo, se pone como valor None
            Devuelve un diccionario { indidcador: {(subindex,camp_codi,'in'/'out'): val}}
        """
        #predefinidos
        subindicadores={'CONSOB04':{ ('a','SiE_TipVisita_A','in'):'Primera',('b','SiE_TipVisita_A','in'):'Seguiment',
                                     ('p','SiE_TipVisita_B','in'):'Presencial', ('t','SiE_TipVisita_B','in'):'Tlf', ('v','SiE_TipVisita_B','in'):'Virtual'},
                         'CONSOB05':{},
                         'CONSOB06': {}}
        
        #dinamicamente anyado los campos que interesa que esten registrados, pero da igual el valor, y se asocia a un indicador y a un subindice concreto
        #COMO VEIS AQUI LAS KEYS NO SON EXACTAMENTE EL camp_codi de la tabla de xml, UTILIZO LOS AGRUPADORES DEL DICCIONARIO GLOBAL KEYS.
        #EL VALOR SE PONE A 1. Esto es importante, porque luego cuando se saca los valores brutos, se le pone valor por defecto 1 a todos estos agrupadores
        for i,key in enumerate(['Sexualitat i afectivitat',"Salut aliment�ria","Consum subst�ncies","Maltractament i viol�ncia","Benestar Emocional","Salut social i entorn","Altres temes"]):
            letter=string.ascii_lowercase[i]
            subindicadores['CONSOB05'][(letter,key,'in')]=1
        
        #dinamicamente anyado los campos que interesa que esten registrados con un valor  y se asocia a un indicador y a un subindice concreto
        for i,val in enumerate(['Metge AP',"Infermera AP","CSMIJ","CSMA","ASSIR","CAS","Serveis socials",'Equips directiu i psicopedag�gic escolar','Altres']):
            letter=string.ascii_lowercase[i]
            subindicadores['CONSOB06'][(letter,'SiE_DerivatA','in')]=val
        
        #se anyade los campos que interesa que NO esten registrados con un valor  y se asocia a un indicador y a un subindice concreto
        letter=string.ascii_lowercase[i+1]
        subindicadores['CONSOB06'][(letter,'SiE_DerivatA','out')]=None

        return subindicadores



    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]

    def get_infs(self):
        num_col_ide=defaultdict(set)
        sql="select ide_numcol,ide_dni from {}.cat_professionals where tipus='I' ".format(imp)
        return {num_col[:8] for num_col,ide_dni in getAll(sql,imp)}

    def get_escolas_up(self):
        """ Obtiene usando el catalogo cat_escoles_sie para cada equipo, sus escuelas asociadas y el numero de nens de los cursos en la variable cursos.
            Solo entran las escuelas con algun numero de nens.
            Devuelve {up: {escuela: {curs:n_nens}}}
        """
        up_escola=defaultdict(lambda: defaultdict(dict))

        sql="select up,up_desc,codi_escola,nom_escola,{} from nodrizas.cat_escoles where up != '' and codi_escola !=''".format(cursos)
        for fields in getAll(sql,imp):
            up,up_desc,codi_escola,nom_escola=fields[:4]
            self.up_desc[up]=up_desc
            self.esc_desc[int(codi_escola)]=nom_escola
            total_nens= sum([int(n_nens) if n_nens else 0 for n_nens in fields[4:]])
            if total_nens:
                for i,n_nens in enumerate(fields[4:]):
                    curs_etiq=self.camp_codis['SiE_Curs'][str(i+2)]
                    up_escola[up][int(codi_escola)][curs_etiq]=n_nens

        
        return up_escola

    def get_ind_pac(self):
        """Obtiene para cada escuela,curso y paciente los xml_id realizados.
           Devuelve un {escola:{curs: {cip: (sector,xml_id)}}}
        """
        sql="select xml_cip,codi_sector,xml_id,camp_valor,xml_data_alta from {} where XML_TIPUS_ORIG='XML0000023' and camp_codi='SiE_Escola'".format(prstb690)
        
        indicador=defaultdict(lambda: defaultdict(lambda: defaultdict(set)))
        for cip, codi_sector,xml_id, escola,data in getAll(sql,redics):
            if monthsBetween(data,self.current_date) < 12:
                try:
                    curs,=self.xml_data[(codi_sector,xml_id)]['SiE_Curs']
                    indicador[int(escola)][curs][cip].add((codi_sector,xml_id))
                except:
                    continue
        return indicador

  

    def get_xml_data(self):
        """ Obtiene los datos brutos para cada xml_id, anyadiendo para cada AGRUPADOR de camp_codi (si procede) o camp_codi simplemente un set de los valores que puede tener
            dicho campo. En el caso de que interese el valor, solo entran los xml_id con el camp_codi determinado si se encuentra una key en el diccionario de homologadicon
            Devuelve un {xml_id: {codi/agr: set(valores)}}
        """
        sql="select xml_cip,codi_sector,xml_id,camp_valor,camp_codi,xml_data_alta from {} where XML_TIPUS_ORIG='XML0000023' and camp_codi in {}".format(prstb690,tuple(self.camp_codis.keys()))
        print sql
        xml_data=defaultdict(lambda:defaultdict(set))
        #xml_dim=defaultdict(dict)
        for cip, codi_sector,xml_id, valor,codi,data in getAll(sql,redics):
            if monthsBetween(data,self.current_date) < 12:
                #si este campo evalua para True, significa que tiene valores que nos interesa
                if self.camp_codis[codi]:
                    valores= valor.split('|') if (codi == 'SiE_DerivatA' and '|' in valor) else [valor]
                    for val in valores:
                        try:
                            #solo entran los valores en el diccionario de homologacion para ese camp_codi
                            xml_data[(codi_sector,xml_id)][codi].add(self.camp_codis[codi][val])
                        except:
                            continue
                else:
                    #los codis que entren aqui, son los que nos interesa agrupar por los agrupadores en keys, y como valor por defecto el 1, para que coincida con
                    #el que se le ha puesto como 'deseado' en el self.subindicadores
                    for key in keys:
                        if key in codi:
                            xml_data[(codi_sector,xml_id)][keys[key]]=set([1])              
        #print {xml_id for xml_id in xml_data if len(xml_data[xml_id]['O']) > 1 }
        return xml_data


    def get_id_inf(self):
        """Se obtiene para cada xml_id la enfermera que se ha encargado de ese xml. Se hace aparte porque se tiene que procesar la informacion para obtener
           el numero de colegiado.
        """
        sql="select xml_id,codi_sector,camp_valor,xml_data_alta from {} where XML_TIPUS_ORIG='XML0000023' and camp_codi='DadesIdentifProfessional'".format(prstb690)
        xml_inf=defaultdict()
        for xml_id,codi_sector,valor,data in getAll(sql,redics):
            i=valor.find('at:')
            num_col=valor[i+6:i+14]
            if monthsBetween(data,self.current_date) < 12:
                if num_col in self.infs:
                    xml_inf[(codi_sector,xml_id)]=num_col
        printTime(len(xml_inf))
        return xml_inf


    def get_indicador_1(self):
        codi1,codi2,codi3=self.codis
        rows=[]
        #para cada up con escuela asociada
        for up in self.up_escola:
            num3,den3,den2=0,0,0
            num2=set()
            #para cada escuela asociada a esa up
            for escola in self.up_escola[up]:
                den3+=1

                #si hay escuela con datos de SiE
                if escola in self.id_per_escola:
                    num3+=1

                #para cada curso asociado a la up y a la escuela
                for curs in self.up_escola[up][escola]:

                    #Si hay nens en esa up
                    if self.up_escola[up][escola][curs]:
                        num1,num_visites,den_visites,sets04=set(),Counter(),Counter(),defaultdict(set)
                        #den_cips=defaultdict(set)
                        den2+=self.up_escola[up][escola][curs]

                        #Si hay datos para esa escuela y ese curso de SiE
                        if curs in self.id_per_escola[escola]:
                
                            #Para cada cip en esa escuela y curso con datos de SiE
                            for cip in self.id_per_escola[escola][curs]:
                                num1.add(cip)

                                #para cada una de las visitas que ha realizado en SiE
                                for xml_id in self.id_per_escola[escola][curs][cip]:

                                    #Si el xml_id ha sido llevado por una enfermera
                                    if xml_id in self.xml_inf:
                                        num2.add(self.xml_inf[xml_id])
                                    
                                    camps_codi_xml=self.xml_data[xml_id]
                                    #Si ese xml tiene registros para todas las dimensiones que se requieren en los indicadores de vistas
                                    if all( camp_codi in camps_codi_xml for camp_codi in self.camp_dimensions):
                                        #se consigue el sexe y el origen (ESTO PODRIA AUTOMATIZARSE)
                                        sexe_set,origen_set= [ self.xml_data[xml_id][camp_codi] for camp_codi in sorted(self.camp_dimensions)]
                                        sexe,=sexe_set
                                        origen,=origen_set
                                        den_visites[(origen,sexe)]+=1

                                        #para cada uno de los camp_cod/agrupador de los que nos interesa construir un sunindicador
                                        for codi,subindex_dict in self.subindicadores.iteritems():
                                            for sub,sub_camp_codi,in_out in subindex_dict:
                                                #si queremos que tenga registros de ese camp_codi/agr   
                                                if in_out =='in':
                                                    if sub_camp_codi in camps_codi_xml:
                                                        #miramos que el valor que tiene para el camp_codi sea el que cuenta el indicador
                                                        for val in self.xml_data[xml_id][sub_camp_codi]:
                                                            if val == subindex_dict[(sub,sub_camp_codi,in_out)]:
                                                                if '04' not in codi or sub in ('a', 'b'):
                                                                    num_visites[(str(codi+sub),origen,sexe)]+=1
                                                                if '04' in codi:
                                                                    sets04[(sub,origen,sexe)].add(xml_id)
                                                #si queremos que no tenga registros de ese camp_codi/agr                
                                                elif in_out =='out':
                                                    if sub_camp_codi not in camps_codi_xml:
                                                         num_visites[(str(codi+sub),origen,sexe)]+=1
                                                 
                        existeixen = set()
                        den04 = defaultdict(int)
                        for (sub, origen, sexe), ids in sets04.items():
                            if sub in ('a', 'b'):
                                for sub_ in ('p', 't', 'v'):
                                    key = ('CONSOB04' + sub + sub_, origen, sexe)
                                    value = len(ids & sets04[(sub_, origen, sexe)])
                                    num_visites[key] = value      
                                    existeixen.add('CONSOB04' + sub + sub_)
                                    den04[('CONSOB04' + sub + sub_, origen, sexe)] = len(ids)
                        
                        for origen,sexe in den_visites:
                            for codi in self.subindicadores:
                                for sub_index in self.subindicadores[codi]:
                                    if codi <> 'CONSOB04' or sub_index[0] in ('a', 'b'):
                                        codi_sub=str(codi+sub_index[0])
                                        rows.append([up, self.up_desc[up], escola, self.esc_desc[escola],curs,origen,sexe, codi_sub, num_visites[(codi_sub,origen,sexe)] , den_visites[(origen,sexe)]])
                            for ind in existeixen:
                                rows.append([up, self.up_desc[up], escola, self.esc_desc[escola],curs,origen,sexe, ind, num_visites[(ind,origen,sexe)], den04[(ind, origen, sexe)]])
                      
                        rows.append([up, self.up_desc[up], escola, self.esc_desc[escola],curs]+ ['']*len(self.camp_dimensions) + [codi1, len(num1) , self.up_escola[up][escola][curs]])
            rows.append([up, self.up_desc[up], '', '','']+['']*len(self.camp_dimensions)+[codi2, len(num2),den2]) 
            rows.append([up, self.up_desc[up] ,'', '','']+['']*len(self.camp_dimensions)+[codi3, num3, den3])
        listToTable(rows, self.table, alt)


    def get_cataleg_khalix(self):
      
        sql='select scs_codi,ics_desc,amb_codi,amb_desc,sap_codi,sap_desc from nodrizas.cat_centres;'
        rows=[]
        for up,up_desc,amb,amb_desc,sap,sap_desc in getAll(sql,nod):
            if up in self.up_escola:
                for escola in self.up_escola[up]:
                    rows.append((escola, self.esc_desc[escola], up, up_desc, amb, amb_desc, sap, sap_desc))
        self.export_table('cat_sie_khalix',
                          ('(codi_escola varchar(20), escola_desc varchar(200), up varchar(20), up_desc  varchar(200), amb varchar(20), amb_desc varchar(200), sap varchar(20), sap_desc varchar(200))'),
                          'altres',rows)
    

    def export_table(self,table,columns,db,rows):
        createTable(table, columns, db, rm=True)
        listToTable(rows, table, db)
   
def push_to_khalix(table_name,taula_centres,file_name,khalix=False):
    query_template_string="select codi_indicador,concat('A','periodo'),ics_codi, {strg}, if(curs!='',curs,'NOCAT'),if(origen_visita!='',origen_visita,'NOIMP'),if(sexe!='',sexe,'DIM6SET'),'N',sum({var}) from altres.{taula} a inner join nodrizas.{taula_centres} b on a.up=scs_codi where a.{var} != 0 group by codi_indicador,up,curs,origen_visita,sexe"
    query_string=query_template_string.format(strg="'NUM'",var="numerador",taula=table_name,taula_centres=taula_centres)+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=table_name,taula_centres=taula_centres)
    print(query_string)
    if khalix:
        exportKhalix(query_string,file_name)

    query_template_string="select codi_indicador,concat('A','periodo'),concat('E', codi_escola), {strg}, if(curs!='',curs,'NOCAT'),if(origen_visita!='',origen_visita,'NOIMP'),if(sexe!='',sexe,'DIM6SET'),'N',sum({var}) from altres.{taula} a where a.{var} != 0 and codi_indicador not in ('CONSOB02', 'CONSOB03') group by codi_indicador,codi_escola,curs,origen_visita,sexe"
    query_string=query_template_string.format(strg="'NUM'",var="numerador",taula=table_name,taula_centres=taula_centres)+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=table_name,taula_centres=taula_centres)
    print(query_string)
    if khalix:
        exportKhalix(query_string,file_name + '_ESCOLA')


if __name__ == '__main__':
    printTime('inici')
    table_name='exp_khalix_sie'
    columns='( up varchar(10), up_desc varchar(50), codi_escola varchar(20), nom_escola varchar(50), curs varchar(200), origen_visita varchar(200), sexe varchar(200), codi_indicador varchar(50), numerador int, denominador int)'
    createTable(table_name, columns, alt, rm=True)
    sie=SiE(table_name)

    sie.get_cataleg_khalix()
    sie.get_indicador_1()
    push_to_khalix(table_name,'cat_centres',file_name,True)

   