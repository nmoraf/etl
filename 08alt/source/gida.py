# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import dateutil.relativedelta as r

import sisapUtils as u


MASTER_TB = "mst_gida"
KHALIX_TB = "exp_khalix_gida"
KHALIX_UP = "GIDA"
KHALIX_UBA = "GIDA_UBA"
CORTES_TB = "exp_khalix_gida_mensual"
CORTES_UP = "GIDA_MENSUAL"
QC_TB = "exp_qc_gida"
DATABASE = "altres"

TAXES = {"PANTIC": 0.478349492, "PCREM": 2.696570544, "PDIARR": 11.71060743,
         "PEPIS": 0.357181663, "PFERID": 19.87282559, "PLDERM": 0.683124112,
         "PMURI": 25.16421292, "PODINO": 10.43890813, "PODON": 0.979618738,
         "PPRESA": 4.036460068, "PRESVA": 16.76125564, "PTURME": 1.176287446,
         "PGRIPA": 1.149038759, "PCONTU": 4.778138334, "PFEBSF": 1.02878384,
         "PMAREI": 7.883834144, "PPICPE": 2.165190017, "PAPZB": 1.610639762,
         "PCOLIC": 0.271560926, "PCREPE": 1.973685066, "PDIAGN": 2.725524082,
         "PFERNE": 19.18352993, "PTOS": 5.11413805, "PMUCO": 3.359588892,
         "PCOSEN": 0.222443472, "PCOSEO": 0.244977952, "PDOLMA": 0.270690131,
         "PFEBAN": 8.777755506, "PPICIN": 2.158291845, "PPLOIN": 0.224289029,
         "PPOLLS": 0.352068596, "PREGUR": 0.164098954, "PRESTN": 0.644351599,
         "PSOSVA": 0.291036088, "PVOMNE": 2.726337056, "PEPISN": 0.315756236,
         "PCONTD": 4.957455211, "PLESC": 1.613940246, "PPELLA": 0.361539749,
         "PMUGN": 0.188537624}
CONVERSIO = {"ACE_A": "ANTIC", "CERVI_A": "CERVI", "CONT_A": "CONTU",
             "CREM_A": "CREM", "C_ANSI_A": "ANSIE", "DIARR_A": "DIARR",
             "LUMB_A": "DML", "EPA_A": "PRESA", "ENTOR_A": "TURME",
             "EPIST_A": "EPIS", "FEBRE_A": "FEBSF", "FERIDA_A": "FERID",
             "PLEC_A": "LDERM", "MAREIG_A": "MAREI", "M_OID_A": "MOLOR",
             "M_ULL_A": "ULL", "M_URI_A": "MURI", "ODIN_A": "ODINO",
             "ODONT_A": "ODON", "PICAD_A": "PICPE", "SRVA_A": "RESVA",
             "VOMIT_A": "DIARR", "ALT_A": "A", "VOMIT": "DIARR",
             "CEFAL": "CEFAL_A", "HERP": "HERPL_A", "ABCES": "ABCES_A",
             "UNG": "UNG_A", "VOLT": "VOLT_A", "RESTR": "RESTRE_A",
             "MVAG": "MVAG_A", "REVAC": "REVAC_A", "AFTA": "AFTA_A",
             "INSOM": "INSOM_A", "URTI": "URTI_A"}


class GIDA(object):
    """."""

    def __init__(self):
        """."""
        self.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.mensual = c.Counter()
        self.get_edats()
        self.get_poblacio()
        self.get_centres()
        self.get_visites()
        self.get_usu_to_col()
        self.get_motius()
        self.get_aguda()
        self.get_master()
        self.get_derivats()
        self.export_master()
        self.get_col_to_uba()
        self.get_ubas()
        self.get_khalix()
        self.get_taxes()
        self.export_khalix()
        self.export_mensual()
        self.export_qc()

    def get_edats(self):
        """."""
        sql = "select id_cip_sec, \
               (year(data_ext) - year(usua_data_naixement)) - \
               (date_format(data_ext, '%m%d') < \
                date_format(usua_data_naixement, '%m%d')) \
               from assignada, nodrizas.dextraccio"
        self.edats = {id: "ADU" if edat > 14 else "PED"
                      for (id, edat) in u.getAll(sql, "import")}

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "select id_cip_sec, up, ubainf from assignada_tot where ates = 1"
        for id, up, uba in u.getAll(sql, "nodrizas"):
            grup = self.edats[id]
            for ind in ("GESTINF05", "GESTINF06"):
                self.recomptes[ind][(up, uba, "UBA", grup)]["DEN"] += 1
            self.poblacio[id] = (up, uba)

    def get_centres(self):
        """."""
        sql = "select scs_codi, tip_eap, ics_codi from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_visites(self):
        """."""
        sql = "select id_cip_sec, visi_up, visi_col_prov_resp \
               from visites1, nodrizas.dextraccio \
               where visi_col_prov_resp like '3%' and \
                     s_espe_codi_especialitat not in ('EXTRA', '10102') and \
                     visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') and \
                     visi_situacio_visita = 'R' and \
                     visi_data_visita between adddate(data_ext, interval -1 year) and data_ext"  # noqa
        for id, up, col in u.getAll(sql, "import"):
            if up in self.centres and id in self.edats:
                grup = self.edats[id]
                tipus = self.centres[up][0]
                go = (tipus == "M",
                      (grup == "ADU" and tipus == "A"),
                      (grup == "PED" and tipus == "N"))
                if any(go):
                    for ind in ("GESTINF03", "GESTINF04"):
                        self.recomptes[ind][(up, col, "COL", grup)]["DEN"] += 1

    def get_usu_to_col(self):
        """."""
        sql = "select codi_sector, ide_usuari, ide_numcol \
               from cat_pritb992 \
               where ide_numcol <> ''"
        self.usu_to_col = {row[:2]: row[2] for row in u.getAll(sql, "import")}

    def get_motius(self):
        """."""
        sql = """select sym_name
                 from icsap.klx_master_symbol
                 where dim_index = 4 and (
                    sym_index in (
                        select sym_index from icsap.klx_parent_child
                        where dim_index = 4 and parent_index in (
                            select sym_index from icsap.klx_master_symbol
                            where dim_index = 4 and
                                  sym_name in ('PMOTIUPRPE', 'PMOTIUTEL')
                             )
                        ) or
                    sym_name = 'PALPED')"""
        self.motius = {motiu: "PED" for motiu, in u.getAll(sql, "khalix")}

    def _get_motiu(self, motiu, grup):
        """."""
        motiu = CONVERSIO.get(motiu, motiu)
        if motiu == "A":
            motiu = "PAL{}".format(grup)
        else:
            motiu = "P{}".format(motiu.replace("_", ""))
        if motiu not in self.motius:
            self.motius[motiu] = "ADU"
        return motiu

    def get_aguda(self):
        """."""
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        first = (dext - r.relativedelta(months=1)).replace(day=1)
        sql = "select id_cip_sec, codi_sector, cesp_up, cesp_usu_alta, \
                      cesp_cod_mce, cesp_estat, cesp_data_alta \
               from aguda, nodrizas.dextraccio \
               where cesp_data_alta between \
                            adddate(data_ext, interval -1 year) and data_ext \
               union \
               select id_cip_sec, codi_sector, pi_up, pi_usu_alta, \
                      upper(replace(pi_anagrama, ' ', '')), \
                      if(pi_ser_derivat = '', 'I', 'M'), pi_data_alta \
               from ares, nodrizas.dextraccio \
               where pi_anagrama not in ('', 'TAP_A') and \
                     pi_data_alta between \
                            adddate(data_ext, interval -1 year) and data_ext"
        for id, sec, up, usu, motiu, estat, dat in u.getAll(sql, "import"):
            if id in self.edats:
                grup = self.edats[id]
                motiu = self._get_motiu(motiu, grup)
                motiu_tip = self.motius[motiu]
                if grup == motiu_tip:
                    if id in self.poblacio:
                        up_pac, uba = self.poblacio[id]
                        if up == up_pac:
                            key = (up, uba, "UBA", grup)
                            self.recomptes["GESTINF05"][key][motiu] += 1
                            if estat == "I":
                                self.recomptes["GESTINF06"][key][motiu] += 1
                            if dat >= first:
                                key = ("B" + dat.strftime("%y%m"), up, motiu)
                                self.mensual[("GESTINF05",) + key] += 1
                                if estat == "I":
                                    self.mensual[("GESTINF06",) + key] += 1
                    col = self.usu_to_col.get((sec, usu))
                    key = (up, col, "COL", grup)
                    if key in self.recomptes["GESTINF03"]:
                        self.recomptes["GESTINF03"][key][motiu] += 1
                        if estat == "I":
                            self.recomptes["GESTINF04"][key][motiu] += 1
                        if dat >= first:
                            key = ("B" + dat.strftime("%y%m"), up, motiu)
                            self.mensual[("GESTINF03",) + key] += 1
                            if estat == "I":
                                self.mensual[("GESTINF04",) + key] += 1

    def get_master(self):
        """."""
        self.master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes.items():
            for (up, prof, tip, grup), minidades in dades.items():
                den = minidades["DEN"]
                for motiu, motiu_tip in self.motius.items():
                    if motiu_tip == grup:
                        key = (ind, up, prof, tip, grup, motiu)
                        self.master[key]["DEN"] = den
                        self.master[key]["NUM"] = minidades[motiu]

    def get_derivats(self):
        """."""
        for (ind, up, prof, tip, grup, motiu), dades in self.master.items():
            if ind == "GESTINF03":
                key = ("GESTINF02", up, prof, tip, "MIX", "TIPMOTIU")
                self.master[key]["DEN"] += dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
            if ind in ("GESTINF03", "GESTINF04"):
                key = ("GESTINF07", up, prof, tip, grup, motiu)
                analysis = "DEN" if ind == "GESTINF03" else "NUM"
                self.master[key][analysis] = dades["NUM"]

    def export_master(self):
        """."""
        u.createTable(MASTER_TB, "(ind varchar(10), up varchar(5), \
                                   prof varchar(10), tip varchar(3), \
                                   grup varchar(3), motiu varchar(10), \
                                   num int, den int)",
                      DATABASE, rm=True)
        upload = [k + (v["NUM"], v["DEN"]) for (k, v) in self.master.items()]
        u.listToTable(upload, MASTER_TB, DATABASE)

    def get_col_to_uba(self):
        """Per passar dades individuals a khalix dels indicadors de NUMCOL."""
        self.col_to_uba = c.defaultdict(set)
        sql = "select ide_numcol, up, uab \
               from cat_professionals \
               where tipus = 'I'"
        for col, up, uba in u.getAll(sql, "import"):
            self.col_to_uba[(up, col)].add(uba)

    def get_ubas(self):
        """Nom�s pujarem UBAs amb EQA."""
        self.ubas = c.defaultdict(set)
        for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
            sql = "select up, uba from mst_ubas where tipus = 'I'"
            for key in u.getAll(sql, db):
                self.ubas[grup].add(key)
                self.ubas["MIX"].add(key)

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (ind, up, prof, tip, grup, motiu), dades in self.master.items():
            br = self.centres[up][1]
            for analisi in ("NUM", "DEN"):
                self.khalix[(br, ind, motiu, analisi)] += dades[analisi]
            if tip == "COL":
                ubas = self.col_to_uba[(up, prof)]
            else:
                ubas = (prof,)
            for uba in ubas:
                if (up, uba) in self.ubas[grup]:
                    ent = "{}I{}".format(br, uba)
                    for analisi in ("NUM", "DEN"):
                        self.khalix[(ent, ind, motiu, analisi)] += dades[analisi]  # noqa

    def get_taxes(self):
        """."""
        for (ent, ind, motiu, analisi), n in self.khalix.items():
            if ind == "GESTINF05" and analisi == "DEN" and motiu in TAXES:
                taxa = 1000 * self.khalix[(ent, ind, motiu, "NUM")] / float(n)
                meta = TAXES[motiu]
                good = taxa >= meta
                self.khalix[(ent, "GESTINF01", motiu, "DEN")] = 1
                self.khalix[(ent, "GESTINF01", motiu, "NUM")] = 1 * good

    def export_khalix(self):
        """."""
        cols = "(ent varchar(11), ind varchar(10), motiu varchar(10), \
                 analisi varchar(3), valor int)"
        u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, KHALIX_TB, DATABASE)
        for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
            sql = "select ind, 'Aperiodo', ent, analisi, motiu, \
                          'NOIMP', 'DIM6SET', 'N', valor \
                   from {}.{} \
                   where length(ent) {} 5 and \
                         valor > 0".format(DATABASE, KHALIX_TB, param)
            u.exportKhalix(sql, file)

    def export_mensual(self):
        """."""
        cols = "(ind varchar(10), periode varchar(10), up varchar(10), \
                 motiu varchar(10), valor int)"
        u.createTable(CORTES_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.mensual.items()]
        u.listToTable(upload, CORTES_TB, DATABASE)
        sql = "select ind, periode, ics_codi, 'NUM', motiu, \
                      'NOIMP', 'DIM6SET', 'N', valor \
                   from {}.{} inner join nodrizas.cat_centres on up = scs_codi"
        u.exportKhalix(sql.format(DATABASE, CORTES_TB), CORTES_UP)

    def export_qc(self):
        """."""
        indicadors = ("GESTINF05", "GESTINF06")
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sqls = []
        for x, y in ((pactual, 'ANUAL'), (pactual, 'ACTUAL'), (pprevi, 'PREVI')):  # noqa
            sqls.append("select ind, '{}', ent, 'DEN', '{}', 'TIPPROF4', \
                                sum(valor) \
                         from {} \
                         where ind in {} and \
                               length(ent) = 5 and analisi = 'DEN' and \
                               motiu in ('PALADU', 'PALPED')\
                         group by ind, ent".format(x, y, KHALIX_TB, indicadors))  # noqa
        sqls.append("select ind, '{}', ent, 'NUM', 'ANUAL', 'TIPPROF4', \
                            sum(valor) \
                     from {} \
                     where ind in {} and \
                           length(ent) = 5 and analisi = 'NUM' \
                     group by ind, ent".format(pactual, KHALIX_TB, indicadors))
        sqls.append("select ind, replace(periode, 'B', 'A'), ics_codi, 'NUM', \
                            if(replace(periode, 'B', 'A') = '{}', 'ACTUAL', 'PREVI'), \
                            'TIPPROF4', sum(valor) \
                     from {} a \
                     inner join nodrizas.cat_centres b on a.up = b.scs_codi \
                     where ind in {} \
                     group by ind, periode, up".format(pactual, CORTES_TB, indicadors))  # noqa
        dades = []
        for sql in sqls:
            dades.extend([("Q" + row[0],) + row[1:]
                          for row in u.getAll(sql, DATABASE)])
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(QC_TB, "({})".format(", ".join(cols)), DATABASE, rm=True)
        u.listToTable(dades, QC_TB, DATABASE)


if __name__ == "__main__":
    GIDA()
