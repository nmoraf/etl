
# coding: utf-8

# In[ ]:




# In[9]:



import sisapUtils as u
import datetime
from datetime import date

valida = False

profvalida = {
           1: {'tip': 'I','up': '00488', 'uba': '25888'},
           2: {'tip': 'I','up': '00474', 'uba': 'INF25'},
           3: {'tip': 'I','up': '00475', 'uba': 'INF25'}
           }

class Disneampoc(object):
   
   def __init__(self):
       self.get_poblacio()
       self.get_dades()
       self.get_hash()
       self.get_grav()
       self.get_escala()
       self.get_databmrc()
       self.get_tdesde()
       self.get_Indicador()
       self.export_dades_pacient()
       self.export_dades_pacient_no()

   def get_poblacio(self):
       sql = ("select id_cip_sec                from eqa_problemes                where (ps = 62 or ps = 450) and pr_gra != 0", "nodrizas")
       self.poblacio = {id for id, in u.getAll(*sql)}
       
   def get_dades(self):
       self.dades = {}
       sql = ("select id_cip_sec, up, uba, upinf, ubainf                from assignada_tot                where edat > 14 and ates=1 and institucionalitzat=0", "nodrizas")
       for id, up, uba, upinf, ubainf in u.getAll(*sql):
           if id in self.poblacio:
               self.dades[id] = {
                           'id' : id, 'up' : up, 'uba' : uba, 'upinf' : upinf, 'ubainf' : ubainf,
                           'grav' : None, 'BMRC' : 0, 
                           'databmrc' : datetime.date(1900, 1, 1), 'tdesde' : datetime.timedelta(0), 'Imepoc' : None,
                           'Indicador' : 'IAD0006', 'hash' : None, 'sector' : None}
               
   def get_hash(self):
       sql = ("select id_cip_sec, codi_sector, hash_d from u11", "import")
       for id, sector, hash in u.getAll(*sql):
           if id in self.dades:
               self.dades[id]['hash'] = hash
               self.dades[id]['sector'] = sector
   
   def get_grav(self):
       sql = ("select id_cip_sec, pr_gra                from eqa_problemes                where (ps = 62 or ps = 450) and pr_gra != 0", "nodrizas")
       for id, gra in u.getAll(*sql):
           if id in self.dades:
               self.dades[id]['grav'] = gra

   def get_escala(self):
       sql = ("select id_cip_sec                from eqa_variables                where agrupador=795", "nodrizas")
       for id, in u.getAll(*sql):
           if id in self.dades:
               self.dades[id]['BMRC'] = 1

   def get_databmrc(self):
       sql = ("select id_cip_sec, data_var                from eqa_variables                where agrupador=795", "nodrizas")
       for id, time in u.getAll(*sql):
           if id in self.dades and time > self.dades[id]['databmrc']:
                   self.dades[id]['databmrc'] = time                                      
                
   def get_tdesde(self):
       for id, value in self.dades.iteritems():
           if 'databmrc' in value:
               self.dades[id]['tdesde'] = date.today() - self.dades[id]['databmrc']
       
   def get_Indicador(self):
       for id, value in self.dades.iteritems():
           if value['grav'] == 1 and value['BMRC'] == 1:
               self.dades[id]['Imepoc'] = 1
           elif value['grav'] == 2 and value['BMRC'] == 1 and value['tdesde'] < datetime.timedelta(365):
               self.dades[id]['Imepoc'] = 1
           elif value['grav'] == 3 and value['BMRC'] == 1 and value['tdesde'] < datetime.timedelta(180):
               self.dades[id]['Imepoc'] = 1
           elif value['grav'] == 4 and value['BMRC'] == 1 and value['tdesde'] < datetime.timedelta(180):
               self.dades[id]['Imepoc'] = 1
           else:
               self.dades[id]['Imepoc'] = 0

   @staticmethod
   def export_dades(tb, columns, dades):
       table = 'mst_esc_disnea_mpoc'.format(tb)
       columns_str = "({})".format(', '.join(columns))
       db = "altres"
       u.createTable(table, columns_str, db, rm=True)
       u.listToTable(dades, table, db)

   def export_dades_pacient(self):
       dades = [(d['id'], d['up'], d['uba'], d['upinf'], d['ubainf'],
                 d['grav'], d['BMRC'], d['databmrc'], d['tdesde'], d['Imepoc'], d['Indicador'], d['hash'], d['sector'])
                for id, d in self.dades.items()]
       columns = ('id varchar(40)', 'up varchar(6)', 'uba varchar(5)', 'upinf varchar(6)', 'ubainf varchar(6)', 
                  'grav int', 'BMRC int', 'databmrc varchar(16)', 'tdesde int', 'Imepoc int', 'Indicador varchar(7)',
                  'Hash varchar(40)', 'Sector varchar(8)')
       Disneampoc.export_dades("pacient", columns, dades)

   def export_dades_pacient_no(self):        
       dadesp_no = []
       for id, d in self.dades.iteritems():
           if d['Imepoc'] == 0:
               dadesp_no.append((d['id'], d['up'], d['uba'], d['upinf'], d['ubainf'], 0))
       
       tablep = 'exp_ecap_IAD0006_pacient'
       u.createTable(tablep, "(id_cip_sec int, up varchar(6), uba varchar(5), upinf varchar(6), ubainf varchar(6), exclos int)", 'altres', rm=True)
       u.listToTable(dadesp_no, tablep, 'altres')
           
if __name__ == '__main__':
   Disneampoc()

class Datosunidad(object):
   
   def __init__(self):
       self.dadesup()
       self.dadesupinf()
       self.export_dades_unidad()
       
   def dadesup(self):
       tipus = 'M'
       self.dades = []
       sql = ("select up, uba, sum(Imepoc), count(*), sum(Imepoc)/count(*)               from mst_esc_disnea_mpoc group by up, uba", "altres")
       for up, uba, num, den, perc in u.getAll(*sql):
           self.dades.append((up, uba, tipus, 'IAD0006', num, den, perc))

   def dadesupinf(self):
       tipus = 'I'
       sql = ("select upinf, ubainf, sum(Imepoc), count(*), sum(Imepoc)/count(*)               from mst_esc_disnea_mpoc group by upinf, ubainf", "altres")
       for upinf, ubainf, num, den, perc in u.getAll(*sql):
           self.dades.append((upinf, ubainf, tipus, 'IAD0006', num, den, perc))
           
   @staticmethod
   def export_dades(tb, columns, dades):
       table = 'exp_ecap_IAD0006_uba'.format(tb)
       columns_str = "({})".format(', '.join(columns))
       db = "altres"
       u.createTable(table, columns_str, db, rm=True)
       u.listToTable(dades, table, db)

   def export_dades_unidad(self):
       dades = self.dades
       columns = ('up varchar(6)', 'uba varchar(6)', 'tipus varchar(1)', 'indicador varchar(8)', 'numerador int', 'denominador int',  
                  'resultat float')
       Datosunidad.export_dades("Unidad", columns, dades)

if __name__ == '__main__':
   Datosunidad()

if valida:
   uploadvalp = []
   uploadval = []
   for professional in profvalida:
       up = profvalida[professional]['up']
       uba = profvalida[professional]['uba']
       tip = profvalida[professional]['tip']
       if tip == 'M':
           sql = "select id, up, uba, upinf, ubainf, if(Imepoc =1,9,0)   from mst_esc_disnea_mpoc where up = '{0}' and uba = '{1}'".format(up, uba)
       elif tip == 'I':
           sql = "select id, up, uba, upinf, ubainf, if(Imepoc =1,9,0)    from mst_esc_disnea_mpoc where upinf = '{0}' and ubainf = '{1}'".format(up, uba)
       for a,b,c,d,e,f in u.getAll(sql, 'altres'):
           uploadvalp.append([a,b,c,d,e,f])
       sql = "select up, uba, tipus, numerador, denominador, resultat  from exp_ecap_IAD0006_uba where up = '{0}' and uba = '{1}' and tipus = '{2}'".format(up, uba, tip)
       for a,b,c,d,e,f in u.getAll(sql, 'altres'):
           uploadval.append([a,b,c,d,e,f])    

   tablep = 'exp_ecap_IAD0006_pacient'
   u.createTable(tablep, "(id_cip_sec int,up varchar(5),uba varchar(5),upinf varchar(5), ubainf  varchar(5), exclos int)", 'altres', rm=True)
   u.listToTable(uploadvalp, tablep, 'altres')

   table = 'exp_ecap_IAD0006_uba'
   u.createTable(table, "(up varchar(5),uba varchar(5),tipus varchar(1), numerador int,    denominador int, resultat double)", 'altres', rm=True)
   u.listToTable(uploadval, table, 'altres')

