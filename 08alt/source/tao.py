from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="altres"
conn = connect((db,'aux'))
c=conn.cursor()

path="./dades_noesb/tao_indicadors.txt"

tractaments="nodrizas.eqa_tractaments"
variables="nodrizas.eqa_variables"
assignada="nodrizas.assignada_tot_with_jail"
dext="nodrizas.dextraccio"
u11 = "mst_tao_u11"
u11nod = "nodrizas.eqa_u11"
edats_k = "nodrizas.khx_edats5a"

coag = "tao_pac_coag"
criteri_coag= "farmac = 3"
inr ="inr0"
inr61 ="inr_6control1"
inr6 ="inr_6control"
taoinr = "tao_inr"
trang= "inr_Rang"
inr1="inr1"
inr2="inr2"
inr3 = "inr3"
inr4 = "inr4"
inr5 = "inr5"
inr6 = "mst_tao_controls_pacient"

criteri_inr = "agrupador in (195,262,263)"
#criteri_segAP="agrupador=195 and usar=6"
criteri_controls="agrupador=195"
agrRanginf="agrupador=262"
agrRangsup="agrupador=263"

marge="0.2"

criteri_edat="edat>14"

pac1="pre_tao_pacient"
pac="mst_tao_pacient"
ecap_pac_pre = "tao_ecap_pacient_pre"
ecap_pac = "exp_ecap_tao_pacient"
ecap_marca_exclosos = "if(maca=1,1,if(institucionalitzat=1,2,if(excl=1,4,if(ates=0,5,0))))"
condicio_ecap_llistat = "llistat=1 and excl=0 and num=0"
condicio_ecap_ind = "ates=1 and institucionalitzat=0 and maca=0 and excl=0"
ecap_ind = "exp_ecap_tao_uba" 
tipus=[['M','up','uba'],['I','upinf','ubainf']] 
uba_in="mst_tao_ubas"

up = "eqa_tao_khalix_up_pre"
upuba = "eqa_tao_khalix_uba_pre"
khalix_pre = "eqa_tao_khalix_up_ind"
khalix_preuba = "eqa_tao_khalix_uba_ind"
khalix = "exp_khalix_tao_up_ind"
khalixuba = "exp_khalix_tao_uba_ind"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
condicio_khalix = "excl=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
conceptes=[['NUM','num'],['DEN','den']]
ecap_cataleg_exclosos = [[0,'Pacients que formen part de l&#39;indicador',0],[1,'Pacients MACA',2],[2,'Pacients institucionalitzats',3],[3,'Pacients exclosos per edat',4],[4,'Pacients exclosos per motius cl&iacute;nics',5],[5,'Pacients no atesos el darrer any',1]]

catalegKhx = "exp_khalix_tao_cataleg"
cataleg = "exp_ecap_tao_cataleg"
catalegPare = "exp_ecap_tao_catalegPare"
catalegExclosos = "exp_ecap_tao_catalegExclosos"

precat="tao_indicadors"

indicadors = [['TAO001','tao_pac_coag','segAP','1',''],['TAO002','tao_pac_coag','ncontrols','1','where segAP=1'],['TAO003','mst_tao_controls_pacient','sum(dies)','count(*)','where  ok <> -1'],['TAO004','mst_tao_controls_pacient','sum(ok)','count(ok)','where  ok <> -1'],['TAO005','mst_tao_controls_pacient','sum(if(ok=1,dies,0))','sum(dies)','where  ok <> -1']]

c.execute("select date_add(date_add(data_ext, interval -1 year), interval +1 day),date_add(date_add(date_add(data_ext, interval -1 year), interval +1 day), interval -3 month),data_ext,date_add(data_ext, interval +1 day) from %s" % (dext))
fa1any,fa15mesos,avui,dema,=c.fetchone()

c.execute("drop table if exists %s" % coag)
c.execute("create table %s (id_cip_sec int,segAP double,ncontrols double)" % coag)
c.execute("insert into %s select id_cip_sec,0 segAP,0 ncontrols from %s where %s group by id_cip_sec" % (coag,tractaments,criteri_coag))
c.execute("alter table %s add unique (id_cip_sec,segAP,ncontrols)" % coag)

c.execute("drop table if exists %s" % inr)
c.execute("create table %s like %s" %(inr,variables))
c.execute("insert into %s select * from %s where %s" % (inr,variables,criteri_inr))

c.execute("drop table if exists %s" % inr61)
c.execute("create table %s(id_cip_sec int)" %(inr61))
c.execute("insert into %s select id_cip_sec from nodrizas.eqa_variables_tao where agrupador='DSINT' and usar=6 and (data between '%s' and '%s') group by id_cip_sec" % (inr61,fa1any,avui))
c.execute("alter table %s add index(id_cip_sec)" % inr61)

c.execute("drop table if exists %s" % inr6)
c.execute("create table %s(id_cip_sec int,controls double)" %(inr6))
c.execute("insert into %s select a.id_cip_sec,count(*) controls from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where %s and (data_var between '%s' and '%s') group by id_cip_sec" % (inr6,inr,inr61,criteri_controls,fa1any,avui))

c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set segAP=1" % (coag,inr6))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set ncontrols=controls" % (coag,inr6))

 
c.execute("drop table if exists %s" % taoinr)
c.execute("create table %s (id_cip_sec int)" % taoinr)
c.execute("insert into %s select id_cip_sec from %s where segAP=1 group by id_cip_sec" % (taoinr,coag))
c.execute("alter table %s add unique (id_cip_sec)" % taoinr)

c.execute("drop table if exists %s" % trang)
c.execute("create table %s (id_cip_sec double,inf double not null default 2,sup double not null default 3,canviat int)" % trang)
c.execute("insert into %s (id_cip_sec) select id_cip_sec from %s" % (trang,taoinr))
c.execute("alter table %s add index (id_cip_sec,inf,sup,canviat)" % trang)
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set inf = valor,canviat=1 where %s and usar=1" % (trang,inr,agrRanginf))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set sup = valor,canviat=1 where %s and usar=1" % (trang,inr,agrRangsup))

c.execute("drop table if exists %s" % inr1)
c.execute("create table %s (id_cip_sec double,data_var date,valor double,usar int)" % inr1)
c.execute("insert into %s select id_cip_sec,data_var,valor,usar from  %s a where exists (select 1 from %s b where a.id_cip_sec=b.id_cip_sec)and agrupador = 195" % (inr1,inr,taoinr))

c.execute("drop table if exists %s" % inr2)
c.execute("create table %s like %s" % (inr2,inr1))
c.execute("insert into %s select a.* from %s a where data_var between '%s' and '%s'" % (inr2,inr1,fa1any,avui))

c.execute("drop table if exists %s" % inr3)
c.execute("create table %s (id_cip_sec double,usar int,primary key (id_cip_sec,usar))" % inr3)
c.execute("insert into %s select id_cip_sec,max(usar)+1 usar from %s group by id_cip_sec having min(data_var) > %s" % (inr3,inr2,fa1any))

c.execute("drop table if exists %s" % inr4)
c.execute("create table %s like %s" % (inr4,inr2))
c.execute("insert into %s select id_cip_sec,'%s' data_var,valor,usar from %s a where exists (select 1 from %s b where a.id_cip_sec=b.id_cip_sec and a.usar=b.usar and data_var between '%s' and '%s')" % (inr4,fa1any,inr1,inr3,fa15mesos,fa1any))

c.execute("drop table if exists %s" % inr5)
c.execute("create table %s like %s" % (inr5,inr4))
c.execute("insert into %s select id_cip_sec,'%s' data_var,-1 valor,0 usar from %s" % (inr5,dema,inr1))

c.execute("drop table if exists %s" % inr6)
c.execute("create table %s (id bigint not null AUTO_INCREMENT,id_cip_sec double,data_var date,valor double,origen int,dies int,ok int,ok_estricte int,primary key (id)) select * from (select id_cip_sec,data_var,valor,1 origen,-1 dies,-1 ok, -1 ok_estricte from %s union select id_cip_sec,data_var,valor,0 origen,-1 dies,-1 ok, -1 ok_estricte from %s union select id_cip_sec,data_var,valor,2 origen,-1 dies,-1 ok, -1 ok_estricte from %s) a order by id_cip_sec,data_var" % (inr6,inr2,inr4,inr5))

c.execute("select id_cip_sec,data_var,id from %s order by id" % inr6)
var=c.fetchall()
for v in var:
   cip_act,data_act,id =v[0],v[1],v[2]
   if id>1:
      c.execute("select id_cip_sec,data_var,id from %s where id=%s-1" % (inr6,id))
      var2=c.fetchall()
      for x in var2:
         cip_ant,data_ant,id_ant =x[0],x[1],x[2]
         if cip_act==cip_ant:
            c.execute("select inf - %s, sup + %s, inf, sup from %s where id_cip_sec = %s" % (marge, marge, trang, cip_act))
            rang_i, rang_s, rang_i_estricte, rang_s_estricte, = c.fetchone()
            c.execute("update %s set dies=datediff('%s','%s'), ok = if(valor between %s and %s, 1, 0), ok_estricte = if(valor between %s and %s, 1, 0) where id=%s" % (inr6, data_act, data_ant, rang_i, rang_s, rang_i_estricte, rang_s_estricte, id_ant))
         else:
            zz="res"

c.execute("alter table %s add index(id_cip_sec,dies,ok)" % inr6)
            
c.execute("drop table if exists %s" % pac1)
c.execute("create table %s (id_cip_sec int,indicador varchar(10) not null default'', up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double,llistat double)" % pac1)            

for r in indicadors:
   ind,taula,num,den,where= r[0],r[1],r[2],r[3],r[4]
   c.execute("insert into %s select a.id_cip_sec,'%s',up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,%s num,%s den, 0 excl, 0 llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec %s group by id_cip_sec" % (pac1,ind,num,den,taula,assignada,where))

c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec int,indicador varchar(10) not null default'', up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double,llistat double)" % pac)

with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("insert into %s select id_cip_sec,indicador,up,uba,upinf,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den, excl,'%s' llistat from %s where indicador='%s' and %s" % (pac,llistat,pac1,ind,criteri_edat))

c.execute("drop table if exists %s" % ecap_pac_pre)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',indicador varchar(10) not null default '',maca int,institucionalitzat int,excl int,exclos int,toShow int)" % ecap_pac_pre)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,indicador,maca,institucionalitzat,excl,%s,1 from %s where %s" % (ecap_pac_pre,ecap_marca_exclosos,pac,condicio_ecap_llistat))
c.execute("alter table %s add index(id_cip_sec)" % ecap_pac_pre)

c.execute("drop table if exists %s" % ecap_pac)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("drop table if exists %s" % u11)
c.execute("create table %s like %s" % (u11,u11nod))
c.execute("insert into %s select * from %s a where exists (select 1 from %s b where toShow=1 and a.id_cip_sec=b.id_cip_sec)" % (u11,u11nod,ecap_pac_pre))
c.execute("alter table %s add unique(id_cip_sec,hash_d,codi_sector)" % u11)
c.execute("insert into %s select a.id_cip_sec,up,uba,upinf,ubainf,indicador as grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1" % (ecap_pac,ecap_pac_pre,u11))


c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,comb,pac,edats_k,condicio_khalix))

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))      

c.execute("drop table if exists %s" % ecap_ind)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',numerador int,denominador int,resultat double)" % ecap_ind)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,indicador,sum(num) numerador,sum(den) denominador,sum(num)/sum(den) resultat from %s inner join nodrizas.cat_centres on up=scs_codi where %s group by 1,2,3,4" % (ecap_ind,up,uba,tip,pac,condicio_ecap_ind))

c.execute("drop table if exists %s" % uba_in)
c.execute("create table %s(up varchar(5) not null default '', uba varchar(7) not null default '',tipus varchar(1) not null default '')" % uba_in)
c.execute("insert into %s select up,uba,tipus from %s group by up,uba,tipus" % (uba_in,ecap_ind))

c.execute("alter table %s add unique(up,uba,tipus,indicador)" % ecap_ind)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 numerador,0 denominador, 0 resultat from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in)) 

 
c.execute("drop table if exists %s" % upuba)
c.execute("create table %s (id_cip_sec double, up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % upuba)
c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,khalix edat,%s sexe,%s comb,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (upuba,sexe,comb,pac,edats_k,condicio_khalix))

c.execute("drop table if exists %s" % khalix_preuba)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_preuba)
for r in tipus:
    tip,up,uba=r[0],r[1],r[2]
    c.execute("insert into %s select %s,%s,'%s' tipus,comb,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4,5" % (khalix_preuba,up,uba,tip,upuba))

c.execute("drop table if exists %s" % khalixuba)
c.execute("create table %s (up varchar(5) not null default'',uba varchar(7) not null default '',tipus varchar(1) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(10) not null default '',n double)" % khalixuba)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,uba,tipus,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalixuba,conc,var,khalix_preuba))
    c.execute("insert into %s select up,uba,tipus,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalixuba,conc,var,khalix_preuba))   

c.execute("drop table if exists %s,%s,%s,%s" % (cataleg,catalegPare,catalegExclosos,catalegKhx))
c.execute("create table %s (indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),llistat int,invers int,mmin double,mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))" % cataleg)
c.execute("drop table if exists %s" % precat)
c.execute("create table %s (indicador varchar(8), literal varchar(300),grup varchar(8),grup_desc varchar(300),baixa_ int, dbaixa varchar(80),llistats int,ordre int,grup_ordre int,tipusvalor varchar(5), pantalla varchar(20))" % precat)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor, pantalla = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9], ti[10]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (precat,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor, pantalla))
c.execute("insert into %s select distinct indicador,literal,ordre,grup pare,llistats llistat,0 invers,0 mmin,0 mint,0 mmax,1 toShow,concat('http://sisap-umi.eines.portalics/indicador/codi/',indicador) wiki,tipusvalor from %s  where baixa_=0" % (cataleg,precat))
c.execute("create table %s (pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))" % catalegPare)
c.execute("insert into %s select distinct grup pare,grup_desc literal,grup_ordre ordre, pantalla from %s where baixa_=0" % (catalegPare,precat))
c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ti in p:
      i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s Values('%s','%s','%s','%s')" % (catalegKhx,i,desc,grup,grupdesc))      
         
c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos)
for r in ecap_cataleg_exclosos:
    codi,desc,ordre=r[0],r[1],r[2]
    c.execute("insert into %s VALUES (%d,'%s',%d)" % (catalegExclosos,codi,desc,ordre))
    
      
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")
