# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import os

import sisapUtils as u


db = '6951'
file = u.tempFolder + 'eapp_mdo.csv'


class MDO(object):
    """."""

    def __init__(self):
        """."""
        self.get_dates()
        if self.has_to_be_sent():
            self.get_centres()
            self.get_malalties()
            self.get_numeriques()
            self.get_individuals()
            self.export_csv()

    def get_dates(self):
        """."""
        sql = "select data_ext from dextraccio"
        data, = u.getOne(sql, 'nodrizas')
        # data = d.date(2017, 7, 20)  # test
        self.year, self.week = data.isocalendar()[:2]

    def has_to_be_sent(self):
        """."""
        return (self.week - 1) % 4 == 0 or self.week == 1

    def get_centres(self):
        """."""
        sql = "select edce_codi_centre, edce_descripcio from pritb120"
        self.centres = {cod: des for (cod, des) in u.getAll(sql, db)}

    def get_malalties(self):
        """."""
        sql = "select md_cod, md_des from prstb020"
        self.malalties = {cod: des for (cod, des) in u.getAll(sql, db)}

    def is_good(self, year, week):
        """."""
        return ((year == self.year and week < self.week) or
                (self.week == 1 and year == self.year - 1))

    def get_grup(self, week):
        """."""
        return (week - 1) / 4

    def get_numeriques(self):
        """."""
        numeriques = c.Counter()
        sql = "select mdn_any, mdn_setmana, \
                      mdn_centre, mdn_codi_mdo, \
                      mdn_quantitat \
               from prstba15 \
               where mdn_quantitat > 0"
        for year, week, centre, malaltia, n in u.getAll(sql, db):
            if self.is_good(year, week):
                grup = self.get_grup(week)
                numeriques[(year, grup, centre, malaltia)] += n
        self.numeriques = [(year, 0 - grup, centre, malaltia, n)
                           for (year, grup, centre, malaltia), n
                           in numeriques.items()]

    def get_individuals(self):
        """."""
        sql = "select mdi_pr_cod_u, mdi_v_cen, mdi_codi_mdi, mdi_data_ins \
               from prstb315 \
               where mdi_tipus_mov = 'A'"
        individuals = c.defaultdict(set)
        for pacient, centre, malaltia, data in u.getAll(sql, db):
            year, week = data.isocalendar()[:2]
            if self.is_good(year, week):
                grup = self.get_grup(week)
                malaltia = int(malaltia)
                individuals[(year, grup, centre, malaltia)].add(pacient)
        self.individuals = [(year, 0 - grup, centre, malaltia, len(pacients))
                            for (year, grup, centre, malaltia), pacients
                            in individuals.items()]

    def get_literal_grup(self, grup):
        """."""
        grup = 0 - grup
        return '{} a {}'.format((grup * 4) + 1, (grup * 4) + 4)

    def export_csv(self):
        """."""
        header = [('any', 'setmanes', 'centre_cod', 'centre_des',
                   'mdo_cod', 'mdo_des', 'n')]
        dades = [(year, self.get_literal_grup(grup),
                  centre, self.centres.get(centre),
                  mdo, self.malalties.get(mdo), n)
                 for (year, grup, centre, mdo, n)
                 in sorted(self.numeriques + self.individuals)]
        u.writeCSV(file, header + dades, sep=';')
        text = "Us fem arribar el fitxer de MDO."
        u.sendPolite('rguerrero@gencat.cat', 'EAPP - MDO', text, file)
        os.remove(file)


if __name__ == '__main__':
    MDO()
