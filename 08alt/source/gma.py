from sisapUtils import *

debug = False

db = 'altres'


def gmaConverter(ncron):
    if ncron > 30:
        return 'GMANC30M'
    else:
        return 'GMANC' + str(ncron)


def do_it(sector):
    data = []
    dext, = getOne('select data_ext from dextraccio', 'nodrizas')
    sql = 'select id_cip_sec, usua_uab_up, usua_uab_codi_uab, ass_codi_unitat, usua_sexe, usua_data_naixement from assignada_s{}'.format(sector)
    for id, up, uba, ubainf, sexe, naix in getAll(sql, 'import'):
        edat = yearsBetween(naix, dext)
        data.append([id, up, uba, ubainf,  sexe, edat])
    return data


if __name__ == '__main__' and IS_MENSUAL:
    printTime()
    mega_data = multiprocess(do_it, sectors, 8)
    printTime()
    poblacio = {}
    for data in mega_data:
        for id, up, uba, ubainf, sexe, edat in data:
            poblacio[id, up] = {'uba': uba, 'ubainf': ubainf, 'sexe': sexConverter(sexe), 'edat': ageConverter(edat, 5)}
    printTime()

    brs = {}
    sql = 'select distinct scs_codi,ics_codi from cat_centres'
    for up1, khx in getAll(sql, 'nodrizas'):
        brs[up1] = {'br': khx}

    sql = 'select left(gma_any,4) as any, right(gma_any, 2) as mes from gma limit 1'
    for a, m in getAll(sql, 'import'):
        KhxAny = a
        KhxMes = m

    file = 'GMA' 
    TableMy = 'exp_khalix_gma'
    OutFile = tempFolder + 'gma.txt'
    
    fileUBA = 'GMA_UBA'
    TableMyUBA = 'exp_khalix_gma_uba'
    OutFileUBA = tempFolder + 'gmauba.txt'
    
    gma = {}
    gmaUBA = {}
    sql = 'select id_cip_sec, gma_any, gma_up, gma_cod, gma_ind_cmplx, gma_num_cron from gma{}'.format(' limit 10' if debug else ' ')
    for id, anys, up, codi, complex, ncron in getAll(sql, 'import'):
        try:
            sexe = poblacio[id, up]['sexe']
            edat = poblacio[id, up]['edat']
            uba = poblacio[id, up]['uba']
            ubainf = poblacio[id, up]['ubainf']
        except KeyError:
            continue
        try:
            br = brs[up]['br']
        except KeyError:
            continue
        periodeA = anys[2:-2]
        periodeM = anys[4:]
        periode = 'A' + str(periodeA) + str(periodeM)
        cron = gmaConverter(ncron)
        codgma = 'GMA' + codi
        if (codgma, periode, br, edat, sexe, cron) in gma:
            gma[(codgma, periode, br, edat, sexe, cron)]['npacients'] += 1
            gma[(codgma, periode, br, edat, sexe, cron)]['complex'] += complex
        else:
            gma[(codgma, periode, br, edat, sexe, cron)] = {'npacients': 1, 'complex': complex}
        if (codgma, br, 'M', uba, cron) in gmaUBA:
            gmaUBA[(codgma, br, 'M', uba, cron)]['npacients'] += 1
            gmaUBA[(codgma, br, 'M', uba, cron)]['complex'] += complex
        else:
            gmaUBA[(codgma, br,  'M', uba,  cron)] = {'npacients': 1, 'complex': complex}
        if (codgma, br, 'I', ubainf, cron) in gmaUBA:
            gmaUBA[(codgma, br, 'I', ubainf, cron)]['npacients'] += 1
            gmaUBA[(codgma, br, 'I', ubainf, cron)]['complex'] += complex
        else:
            gmaUBA[(codgma, br, 'I', ubainf, cron)] = {'npacients': 1, 'complex': complex}

    execute("drop table if exists {}".format(TableMy), db)
    execute("create table {} (codi_gma varchar(10) not null default'', periode varchar(10) not null default'', br varchar(5) not null default'', edat varchar(10) not null default'', \
    ncroniques varchar(10) not null default'', sexe varchar(10) not null default'', npacients double, complex double)".format(TableMy), db)
    
    execute("drop table if exists {}".format(TableMyUBA), db)
    execute("create table {} (codi_gma varchar(10) not null default'', periode varchar(10) not null default '', br varchar(5) not null default'', tipus varchar(1) not null default'', uba varchar(5) not null default'',  ncroniques varchar(10) not null default'', npacients double, complex double)".format(TableMyUBA), db)

    with openCSV(OutFile) as c:
        for (codgma, periode, br, edat, sexe, cron), d in gma.items():
            c.writerow([codgma, periode, br, edat, cron, sexe, d['npacients'], d['complex']])
    loadData(OutFile, TableMy, db)
    
    with openCSV(OutFileUBA) as c:
        for (codgma, br, tipus, uba, cron), d in gmaUBA.items():
            c.writerow([codgma, periode, br, tipus, uba, cron, d['npacients'], d['complex']])
    loadData(OutFileUBA, TableMyUBA, db)
    
    sql = "select codi_gma, periode, br, 'NPACIENTS', edat, ncroniques, sexe, 'N', npacients from {0}.{1} \
            union select codi_gma, periode, br, 'COMPLEX', edat, ncroniques, sexe, 'N', complex from {0}.{1}".format(db, TableMy)
    exportKhalix(sql, file)
    
    sqlUBA = "select codi_gma, periode, concat(br,a.tipus,a.uba), 'NPACIENTS', 'NOCAT', ncroniques, 'DIM6SET', 'N', npacients from {0}.{1} a inner join nodrizas.cat_centres b on a.br=ics_codi inner join export.khx_ubas c on b.scs_codi=c.up and a.uba=c.uba and a.tipus=c.tipus where a.uba<>'' \
            union select codi_gma, periode, concat(br,a.tipus,a.uba), 'COMPLEX', 'NOCAT', ncroniques, 'DIM6SET', 'N', complex from {0}.{1} a inner join nodrizas.cat_centres b on a.br=ics_codi inner join export.khx_ubas c on b.scs_codi=c.up and a.uba=c.uba and a.tipus=c.tipus where a.uba<>''  ".format(db, TableMyUBA)
    exportKhalix(sqlUBA,fileUBA)
    
    printTime()
