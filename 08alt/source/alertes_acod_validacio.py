# coding: latin1

"""
.
"""

import collections as c
import sisapUtils as u


VALIDADORS_UP = ("00004", "00015", "00054", "00065", "00093",
                 "00175", "00176", "00186", "00373", "07927",
                 "00441")

VALIDADORS = (("00004", "4883"), ("00015", "5359"), ("00054", "B"),
              ("00065", "E1"), ("00065", "I"), ("00093", "9"),
              ("00175", "MG005"), ("00176", "MG04"), ("00186", "15"),
              ("00373", "A"), ("07927", "897"), ("00441", "4T"))

TABLE = "mst_alertes_pacient"
DATABASE = "altres"


class Validacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_pacients()
        self.get_indicadors()
        self.get_cataleg()
        self.get_dext()
        self.get_export()
        self.delete_data()
        self.export_data()

    def get_pacients(self):
        """."""
        sql = "select id_cip_sec, hash_d, sector, up, uba, grup_codi, 0 \
               from exp_ecap_alertes_pacient \
               where grup_codi in ('FARM0010','FARM0011')"
        self.pacients = [row for row in u.getAll(sql, DATABASE)]

    def get_indicadors(self):
        """."""
        sql = "select up, uba, indicador, sum(numerador), sum(denominador), \
                      sum(numerador) / sum(denominador) \
               from {} \
               where up in {} \
               and tipus='M' \
               and indicador in ('FARM0010','FARM0011') \
               group by up, uba, indicador".format('exp_ecap_alertes_uba', VALIDADORS_UP)  # noqa
        self.indicadors = [row for row in u.getAll(sql, DATABASE)]

    def get_cataleg(self):
        """ . """
        self.cataleg = {}
        self.cataleg['FARM0010'] = {'Literal': 'Sobredosificacio ACOD en FA no valvular'}  # noqa
        self.cataleg['FARM0011'] = {'Literal': 'Infradosificacio ACOD en FA no valvular'}  # noqa

    def get_dext(self):
        """ . """
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        self.y = dext.year
        self.m = dext.month

    def get_export(self):
        """ . """
        self.export = c.defaultdict(list)
        for _id, hash, sector, up, uba, ind, flag in self.pacients:
            this = (up, uba, "M", ind, hash, sector, flag)
            if (up, uba) in VALIDADORS:
                self.export["altllistats"].append(this)
        for up, uba, ind, num, den, res in self.indicadors:
            this = (self.y, self.m, up, uba, "M", ind, num, den, res)
            if (up, uba) in VALIDADORS:
                self.export["altindicadors"].append(this)
        pare = "FARM04"
        ordre = 0
        for codi in self.cataleg:
            literal = self.cataleg[codi]['Literal']
            ordre += 1
            this = (self.y, codi, literal, ordre, pare, 1, 1, 0, 0, 0, 1, "", "PCT")  # noqa
            self.export["altcataleg"].append(this)
        self.export["altcatalegpare"].append((self.y, pare,
                                              "Alertes de seguretat: ACOD en FA", 3, "VALIDACIO"))   # noqa

    def delete_data(self):
        """ . """
        sqls = ("delete from altllistats \
                 where indicador in ('FARM0010','FARM0011')",
                "delete from altindicadors \
                 where indicador in ('FARM0010','FARM0011')",
                "delete from altcataleg \
                 where indicador in ('FARM0010','FARM0011')",
                "delete from altcatalegpare where pare = 'FARM04'")
        for sql in sqls:
            u.execute(sql, "pdp")

    def export_data(self):
        """ . """
        for table, dades in self.export.items():
            u.listToTable(dades, table, "pdp")


if __name__ == "__main__":
    Validacio()
