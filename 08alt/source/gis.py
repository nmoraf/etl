# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


SERVEIS = ("MG", "PED", "INF", "ENF", "INFPD", "INFP", "INFG", "URGEN", "ESP")
USUARIS = ("04104", "04416", "04302")


class GIS001(object):
    """."""

    def __init__(self):
        """."""
        self.get_tasques()
        self.get_numerador()
        self.get_pim()
        self.get_centres()
        self.get_usuaris()
        self.get_indicador()
        self.export_data()

    def get_tasques(self):
        """."""
        self.tasques = set()
        sql = "select id_cip_sec, alv_data_al \
               from alertes_s{}, nodrizas.dextraccio \
               where alv_tipus = 'TASCAD' and \
                     alv_data_al between adddate(data_ext, interval -1 year) and data_ext"
        jobs = [(sql.format(sector), "import") for sector in u.sectors]
        for sector in u.multiprocess(_worker, jobs, 8):
            self.tasques |= set(sector)

    def get_numerador(self):
        """."""
        self.numerador = set()
        sql = "select visi_id \
               from motius_s{}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     (visi_motiu_prior not in ('', 'CAP') or \
                      visi_usuari_text <> '' or \
                      visi_motiu_lliure <> '')"
        jobs = [(sql.format(sector), "import") for sector in u.sectors]
        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.numerador |= pacients

    def get_pim(self):
        """."""
        self.pim = set()
        sql = "select visi_id \
               from motius_s{}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_ticket_teseo <> ''"
        jobs = [(sql.format(sector), "import") for sector in u.sectors]
        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.pim |= pacients

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_desc, sap_desc, ics_codi, ics_desc \
               from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_usuaris(self):
        """."""
        sql = "select codi_sector, ide_usuari from cat_pritb992 \
               where ide_categ_prof_c in {}".format(USUARIS)
        self.usuaris = set(u.getAll(sql, "import"))

    def get_indicador(self):
        """."""
        self.indicador = c.defaultdict(c.Counter)
        sql = "select visi_id, id_cip_sec, visi_data_visita, visi_up, \
                      codi_sector, visi_assign_visita \
               from {{}}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_up in {} and \
                     visi_servei_codi_servei in {} and \
                     visi_tipus_visita not in ('9T','9E') and \
                     visi_forcada_s_n = 'N' and \
                     visi_internet <> 'S'".format(tuple(self.centres), SERVEIS)
        crt = "show create table visites1"
        tables = u.getOne(crt, "import")[1].split("UNION=(")[1][:-1].split(",")
        jobs = [(sql.format(table), "import") for table in tables]
        for mes in u.multiprocess(_worker, jobs):
            for id, pac, dat, up, sec, usu in mes:
                if (sec, usu) in self.usuaris:
                    if (pac, dat) not in self.tasques and id not in self.pim:
                        self.indicador[up]["DEN"] += 1
                        self.indicador[up]["NUM"] += id in self.numerador

    def export_data(self):
        """."""
        self.export = []
        for up, dades in self.indicador.items():
            num = dades["NUM"]
            den = dades["DEN"]
            ind = str(round(100 * num / float(den), 2)).replace(".", ",")
            this = self.centres[up] + (num, den, ind)
            self.export.append(this)
        u.writeCSV(u.tempFolder + "prog_motius.csv", self.export, sep=";")


def _worker(params):
    return list(u.getAll(*params))


if __name__ == "__main__":
    GIS001()
