# coding: iso-8859-1

from sisapUtils import *
import csv
import os
import sys
from time import strftime

db = 'altres'
imp = 'import'
nod = 'nodrizas'

centres = 'export.khx_centres'
OutFile = tempFolder + 'altesRCA.txt'
TaulaMy = 'exp_khalix_altes_rca'

sql = "select data_ext from dextraccio"
dext, = getOne(sql, nod)

altes2 = {}
sql = 'select id_cip_sec, codi_sector from altes_rca_2'
for id, sector in getAll(sql, imp):
    altes2[(sector, id)] = True

altes = {}
sql = "select id_cip_sec, codi_sector, usua_uab_up, usua_cip_rca, usua_nass1, usua_data_alta from altes_rca_1"
for id, sector, up, rca, nass, data in getAll(sql, imp):
    mesos = monthsBetween(data, dext)
    if 2 <= mesos <= 13:
        calc = False
        if nass == 1:
            calc = True
        else:
            if (sector, id) in altes2:
                calc = True
            else:
                continue
        if calc:
            if up in altes:
                altes[up]['den'] += 1
                altes[up]['num'] += rca
            else:
                altes[up] = {'num': rca, 'den': 1}

with openCSV(OutFile) as c:
    for (up), count in altes.items():
        c.writerow([up, count['num'], count['den']])

execute('drop table if exists {}'.format(TaulaMy), db)
execute('create table {} (up varchar(5), num double, den double)'.format(TaulaMy), db)
loadData(OutFile, TaulaMy, db)

error = []
sql = "select 'NACTRCA',concat('A','periodo'),ics_codi,'NUM','NOCAT','NOIMP','DIM6SET','N',num from {0}.{1} inner join {2} on up=scs_codi \
        union select 'NACTRCA',concat('A','periodo'),ics_codi,'DEN','NOCAT','NOIMP','DIM6SET','N',den from {0}.{1} inner join {2} on up=scs_codi".format(db, TaulaMy, centres)
file = 'ALTES_RCA'
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
