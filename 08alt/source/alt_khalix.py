from sisapUtils import *
import sys

error= []
db="altres"
centres="export.khx_centres"
ubas="export.khx_ubas"
centreswithjail = 'nodrizas.cat_centres_with_jail'
centresjail = 'nodrizas.jail_centres'

taula="%s.exp_khalix_tir_up_ind" % db
query="select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres':centreswithjail}
file="TIRES_NOU"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tir_uba_ind" % db
query="select indicador,concat('A','periodo'),concat(ics_codi,a.tipus,a.uba),conc,'NOCAT',comb,'DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi inner join %(ubas)s c on a.up=c.up and a.uba=c.uba and a.tipus=c.tipus" % {'taula': taula,'centres':centres,'ubas':ubas}
file="TIRES_NOU_UBA"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tir_uba_ind" % db
query="select indicador,concat('A','periodo'),concat(ics_codi,a.uba),conc,'NOCAT',comb,'DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi where a.tipus = 'M'" % {'taula': taula,'centres':centresjail}
file="TIRES_NOU_UBA_JAIL"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tir_cataleg" % db
query="select * from %s" % taula
file="TIRES_CAT"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tao_up_ind" % db
query="select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres':centreswithjail}
file="TAO_NOU"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tao_uba_ind" % db
query="select indicador,concat('A','periodo'),concat(ics_codi,a.tipus,a.uba),conc,'NOCAT',comb,'DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi inner join %(ubas)s c on a.up=c.up and a.uba=c.uba and a.tipus=c.tipus" % {'taula': taula,'centres':centres,'ubas':ubas}
file="TAO_NOU_UBA"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tao_uba_ind" % db
query="select indicador,concat('A','periodo'),concat(ics_codi,a.uba),conc,'NOCAT',comb,'DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi where a.tipus = 'M'" % {'taula': taula,'centres':centresjail}
file="TAO_NOU_UBA_JAIL"
error.append(exportKhalix(query,file))

taula="%s.exp_khalix_tao_cataleg" % db
query="select * from %s" % taula
file="TAO_CAT"
error.append(exportKhalix(query,file))

taula="%s.exp_ecap_alertes_uba" % db
query="select indicador,concat('A','periodo'),ics_codi,'NUM','NOCAT','NOIMP','DIM6SET','N', sum(numerador) from %(taula)s a inner join %(centres)s b on a.up=scs_codi where tipus='M' group by indicador, ics_codi \
            union select indicador,concat('A','periodo'),ics_codi,'DEN','NOCAT','NOIMP','DIM6SET','N', sum(denominador) from %(taula)s a inner join %(centres)s b on a.up=scs_codi where tipus='M' group by indicador, ics_codi " % {'taula': taula,'centres':centres,'ubas':ubas}
file="ALERTES"
error.append(exportKhalix(query,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
