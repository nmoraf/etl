# coding: utf8
"""
Bot que envia a trello up_residencias para revisión manual,
desde Covid lo revisa Paqui en BBDD, dejo comentado todo el script y en pila de ejecución hasta migrarlo.
"""

# aqui comienza el script:
"""
from sisapUtils import createTable, getAll, sectors, multiprocess, listToTable, execute, TrelloCard
from collections import defaultdict

    
res_ecap = {}
sql = 'select residencia, inclos from cat_residencies'
for re, inclos in getAll(sql, 'import'):
    res_ecap[re] = inclos


news = []
sql = 'select gu_up_residencia, gu_desc from cat_ppftb011_def'
for residencia, desc in getAll(sql, 'import'):
    sisap = 0
    if residencia in res_ecap:
        sisap = 1
    if sisap == 0:    
        news.append([residencia,desc])
        

if len(news) >0:
    card = TrelloCard('definició', 'CATALEGS', 'Noves residències')
    card.add_description("Noves residències d'ecap **no incloses al nostre catàleg**")
    for cod in news:
        card.add_to_checklist('Nous codis a validar', ', '.join(map(str, (cod[0], cod[1]))))
    card.add_members('fredericmuniente')
    card.add_comment('Nous codis')
"""
