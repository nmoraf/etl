# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u

import datetime as d

nod = 'nodrizas'
db = 'altres'

# Llista de problemes fibrilació auricular
FA = set(['C01-I48.0', 'C01-I48.1', 'C01-I48.2', 'C01-I48.91', 'I48'])

# Llista de codis anticoagulants orals directes (agrupadors: 6, 794)
# nodrizas.eqa_criteris where taula = 'prescripcions' and agrupador in (6, 794)
anticoagulants = set(['B01AE07', 'B01AF01', 'B01AF02', 'B01AF03'])

# Codis Verapamil (agrupador: 231)
verapamil = set(['C08DA01', 'C09BB10'])

# Codis (en MAJUSCULES, formen part d'un agrupador sisap, crearlos?)
# CICLOSPORINA: (part de L04AD	'ImmunoS Farm':	41)
# DRONEDARONA: (part de C01BD 'Antiarritmics classe III':	657)
# eritromicina: (agrupador: 535, 536(tancat=1))
ciclo_drone_eri = set(['L04AD01', 'C01BD07', 'J01FA01'])

tract_per_ATC = anticoagulants.union(verapamil, ciclo_drone_eri)

# Codis tractaments per dosi; _10 per indicador 10 i _11 per indicador 11
dabi_10 = set([654802])
riva_10 = set([686894])
api_10 = set([694840])
edo_10 = set([707044])

dabi_11 = set([654799, 654800, 654801])
riva_11 = set([686887])
api_11 = set([654803, 654804])
edo_11 = set([707043])

tract_per_dosi = dabi_10.union(riva_10, api_10, edo_10, dabi_11, riva_11,
                               api_11, edo_11)

# Valors de laboratori i pes (FG=30, Creatinina=875, pes=268)
lab_pes = set([30, 875, 268])


def get_date_extraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql = "select data_ext from {}.dextraccio".format('nodrizas')
    return u.getOne(sql, 'nodrizas')[0]


class ACOD_FA(object):
    """."""

    def __init__(self):
        """."""
        ts = d.datetime.now()
        print(ts)
        # self.test_id = 12014397 # 11976894
        self.get_denominador()
        print('''get_denominador: {}'''.format(d.datetime.now() - ts))
        ts = d.datetime.now()
        self.get_poblacio()
        print('''get_poblacio: {}'''.format(d.datetime.now() - ts))
        ts = d.datetime.now()
        # print('''poblacio: {}'''.format(self.poblacio[self.test_id]))
        self.get_farmacs()
        print('''get_farmacs: {}'''.format(d.datetime.now() - ts))
        ts = d.datetime.now()
        # print('''self.dabi_10: {}'''.format(self.test_id in self.dabi_10))
        # print('''self.dabi_11: {}'''.format(self.test_id in self.dabi_11))
        # print('''self.riva_10: {}'''.format(self.test_id in self.riva_10))
        # print('''self.riva_11: {}'''.format(self.test_id in self.riva_11))
        # print('''self.api_10: {}'''.format(self.test_id in self.api_10))
        # print('''self.api_11: {}'''.format(self.test_id in self.api_11))
        # print('''self.edo_10: {}'''.format(self.test_id in self.edo_10))
        # print('''self.edo_11: {}'''.format(self.test_id in self.edo_11))
        self.get_laboratori()
        print('''get_laboratori: {}'''.format(d.datetime.now() - ts))
        ts = d.datetime.now()

        self.get_indicadors()
        print('''get_indicadors: {}'''.format(d.datetime.now() - ts))
        ts = d.datetime.now()
        # for row in self.pacients:
        #     if row[0] == self.test_id:
        #         print(row)

        self.get_hash()

        self.export_taula()
        print('''export_taula: {}'''.format(d.datetime.now() - ts))
        ts = d.datetime.now()

        self.set_mst()
        self.set_pacients()
        self.set_resultats()
        self.set_catalegs()

    def get_denominador(self):
        """."""

        # Ids amb diagnòstic de FA en algún moment
        # (encara que s'hagi donat de baixa)
        sql = """select id_cip_sec from problemes where pr_cod_ps in {}
            """.format(tuple(FA))
        self.FA = set([id for id, in u.getAll(sql, 'import')])

        data_fi = get_date_extraccio()
        data_fi = d.datetime.strftime(data_fi, '%Y%m%d')
        u.printTime("Cerca anticoagulants")

        self.anticoagulants = set()
        self.verapamil = set()
        self.ciclo_drone_eri = set()

        # Guardo les freqüències en les prescripcions pels anticoagulants
        self.freq = {}
        # Ids amb anticoagulants orals amb prescripció activa
        # Ids amb Verapamil i amb Ciclosporina, dronedarona, eritromicina
        sql = "select id_cip_sec, ppfmc_atccodi, ppfmc_freq from tractaments \
               where ppfmc_atccodi in {} and \
               ppfmc_data_fi > {}".format(tuple(tract_per_ATC), data_fi)
        for id, codi, freq in u.getAll(sql, 'import'):
            if codi in anticoagulants:
                self.anticoagulants.add(id)
                # Guardo les freqüències en les prescripcions pels acos
                self.freq[id, codi] = freq
            elif codi in verapamil:
                self.verapamil.add(id)
            elif codi in ciclo_drone_eri:
                self.ciclo_drone_eri.add(id)
        u.printTime(len(self.anticoagulants))
        print(len(self.verapamil))
        print(len(self.ciclo_drone_eri))
        # Guardo només els que tenen anticoagulants i FA
        self.anticoagulants_FA = self.anticoagulants.intersection(self.FA)

        u.printTime(len(self.anticoagulants_FA))

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat from assignada_tot"
        self.poblacio = {id: (up, uba, ubainf, edat)
                         for (id, up, uba, ubainf, edat)
                         in u.getAll(sql, nod)
                         if id in self.anticoagulants_FA}
        u.printTime(len(self.poblacio))

    def get_farmacs(self):
        """."""

        data_fi = get_date_extraccio()
        data_fi = d.datetime.strftime(data_fi, '%Y%m%d')
        u.printTime("Cerca farmacs")

        # Creo vectors per guardar tots els ids amb medicaments que compleixen
        self.dabi_10 = set()
        self.dabi_11 = set()
        self.riva_10 = set()
        self.riva_11 = set()
        self.api_10 = set()
        self.api_11 = set()
        self.edo_10 = set()
        self.edo_11 = set()

        sql = "select id_cip_sec, ppfmc_pf_codi from tractaments where ppfmc_pf_codi in {} \
               and ppfmc_data_fi > {}".format(tuple(tract_per_dosi), data_fi)
        for id, codi in u.getAll(sql, 'import'):
            if id in self.poblacio:
                if codi in dabi_10:
                    self.dabi_10.add(id)
                elif codi in riva_10:
                    self.riva_10.add(id)
                elif codi in api_10:
                    self.api_10.add(id)
                elif codi in edo_10:
                    self.edo_10.add(id)
                elif codi in dabi_11:
                    self.dabi_11.add(id)
                elif codi in riva_11:
                    self.riva_11.add(id)
                elif codi in api_11:
                    self.api_11.add(id)
                elif codi in edo_11:
                    self.edo_11.add(id)

        print("DABI 10:", len(self.dabi_10))
        print("DABI 11:", len(self.dabi_11))
        print("RIVA 10:", len(self.riva_10))
        print("RIVA 11:", len(self.riva_11))
        print("API 10:", len(self.api_10))
        print("API 11:", len(self.api_11))
        print("EDO 10:", len(self.edo_10))
        print("EDO 11:", len(self.edo_11))

    def get_laboratori(self):
        """ Obtenim valors de laboratori (FG, Creatinina i pes)"""
        self.fg = {}
        self.creatinina = {}
        self.pes = {}

        u.printTime("Laboratori")
        # Darrer valor de laboratori del darrer any
        sql = "select id_cip_sec, agrupador, valor from eqa_variables, dextraccio \
               where agrupador in {} and data_var between \
               adddate(data_ext, interval -1 year) and data_ext and \
               usar = 1".format(tuple(lab_pes))
        for id, agrupador, valor in u.getAll(sql, 'nodrizas'):
            if agrupador == 30:
                self.fg[id] = valor
            elif agrupador == 875:
                self.creatinina[id] = valor
            # elif agrupador == 268:
            #     self.pes[id] = valor

        sql = "select id_cip_sec, valor from eqa_variables, dextraccio \
               where agrupador = 268 and data_var <= data_ext and \
               usar = 1"
        for id, valor in u.getAll(sql, 'nodrizas'):
            self.pes[id] = valor
        u.printTime("Final laboratori")

    def get_indicadors(self):
        """ Valors indicadors """

        self.pacients = []
        self.incidencia = []
        for id, (up, uba, ubainf, _edat) in self.poblacio.items():
            alerta = 0
            codi = "FARM0010"

            # Indicador FARM0010

            # DABI
            if id in self.dabi_10:
                # Falta afegir que l'ID no estigui al set de creatinina
                if (self.poblacio[id][3] >= 80
                    or id in self.verapamil 
                    or (id not in self.fg or id not in self.creatinina)):
                    alerta = 1

            # RIVA
            elif id in self.riva_10:
                # # Falta afegir que l'ID no estigui al set de creatinina
                # if (self.fg < 50 or
                #         (id not in self.fg or id not in self.creatinina) or
                #         self.freq[id, 'B01AF01'] == 12):
                #     alerta = 1
                if id not in self.fg or id not in self.creatinina:
                    alerta = 1
                elif self.fg[id] < 50 or self.freq[id, 'B01AF01'] == 12:
                    alerta = 1

            # API
            # elif id in self.api_10:
            #     # Falta afegir que l'ID no estigui al set de creatinina
            #     count = 0
            #     if id in self.pes:
            #         if (self.pes[id] <= 60):
            #             count += 1
            #     if self.poblacio[id][3] >= 80:
            #         count += 1
            #     if id in self.creatinina:
            #         if self.creatinina[id] >= 133:
            #             count += 1
            #     if (self.fg < 30 or
            #             (id not in self.fg or id not in self.creatinina)
            #             or count >= 2):
            #         alerta = 1
            elif id in self.api_10:
                if id not in self.fg or id not in self.creatinina or id not in self.pes:
                    alerta = 1
                else:
                    count = 0
                    if self.pes[id] <= 60:
                        count += 1
                    if self.poblacio[id][3] >= 80:
                        count += 1
                    if self.creatinina[id] >= 133:
                        count += 1
                    if self.fg[id] < 30 or count >= 2:
                        alerta = 1

            # EDO
            elif id in self.edo_10:
                # # Falta afegir que l'ID no estigui al set de creatinina
                # if (self.fg <= 50 or
                #         (id not in self.fg or id not in self.creatinina) or
                #         (id in self.pes and self.pes[id] <= 60) or
                #         id in self.ciclo_drone_eri or
                #         self.freq[id, 'B01AF03'] == 12):
                #     alerta = 1
                if id not in self.fg or id not in self.creatinina or id not in self.pes:
                    alerta = 1
                elif (self.fg[id] <= 50
                      or self.pes[id] <= 60
                      or id in self.ciclo_drone_eri
                      or self.freq[id, 'B01AF03'] == 12):
                    alerta = 1

            self.pacients.append((id, up, uba, ubainf, codi, alerta))

            # Indicador FARM0011 (indicador invers)
            alerta = 0
            codi = "FARM0011"

            # DABI
            if (id in self.dabi_11) and (self.poblacio[id][3] < 80):
                # Falta afegir que l'ID no estigui al set de creatinina
                if (id not in self.verapamil
                    or self.freq[id, 'B01AE07'] == 24
                    or (id not in self.fg or id not in self.creatinina)):
                    alerta = 1

            # RIVA
            # elif (id in self.riva_11) and (self.fg >= 50 or
            #                                (id not in self.fg or
            #                                 id not in self.creatinina)):
            #     alerta = 1            
            elif id in self.riva_11:
                if id not in self.fg or id not in self.creatinina:
                    alerta = 1
                elif self.fg[id] >= 50:
                    alerta = 1

            # API
            # elif (id in self.api_11) and (self.fg >= 30 or
            #                               (id not in self.fg or
            #                                id not in self.creatinina)):
            #     count = 0
            #     if id in self.pes:
            #         if(self.pes[id] <= 60):
            #             count = 1
            #     if self.poblacio[id][3] >= 80:
            #         count += 1
            #     if id in self.creatinina:
            #         if self.creatinina[id] >= 133:
            #             count += 1
            #     if (self.freq[id, 'B01AF02'] == 24 or count <= 1):
            #         alerta = 1            
            elif id in self.api_11:
                if id not in self.fg or id not in self.creatinina: #  or id not in self.pes
                    alerta = 1
                elif self.fg[id] >= 30:
                    count = 0
                    if id in self.pes:
                        if self.pes[id] <= 60:
                            count += 1
                    if self.poblacio[id][3] >= 80:
                        count += 1
                    if self.creatinina[id] >= 133:
                        count += 1
                    if (self.freq[id, 'B01AF02'] == 24 or count <= 1):
                        alerta = 1

            # EDO
            # elif (id in self.edo_11 and (self.fg[id] >= 50 or
            #                              (id not in self.fg or
            #                               id not in self.creatinina))
            #       and self.pes[id] > 60 and
            #       id not in self.ciclo_drone_eri):
            #     # Falta afegir que l'ID no estigui al set de creatinina
            #     alerta = 1
            elif id in self.edo_11:
                if id not in self.fg or id not in self.creatinina or id not in self.pes:
                    alerta = 1
                elif self.fg[id] >= 50 and self.pes[id] > 60 and id not in self.ciclo_drone_eri:
                    alerta = 1
         
            self.pacients.append((id, up, uba, ubainf, codi, alerta))

            # if id in (2119168, 1446386, 5038603, 2559267, 1913424):
            #     dab10 = 'd10' if id in self.dabi_10 else 'No'
            #     riva10 = 'r10' if id in self.riva_10 else 'No'
            #     api10 = 'a10' if id in self.api_10 else 'No'
            #     edo10 = 'e10' if id in self.edo_10 else 'No'
            #     dab11 = 'd11' if id in self.dabi_11 else 'No'
            #     riva11 = 'r11' if id in self.riva_11 else 'No'
            #     api11 = 'a11' if id in self.api_11 else 'No'
            #     edo11 = 'e11' if id in self.edo_11 else 'No'
            #     edat = self.poblacio[id][3]
            #     pes = self.pes[id] if id in self.pes else None
            #     fg = self.fg[id] if id in self.fg else None
            #     creatinina = self.creatinina[id] if id in self.creatinina else None
            #     ciclo = 1 if id in self.ciclo_drone_eri else 0
            #     self.incidencia.append((id, alerta, 
            #                             dab10, riva10, api10, edo10, 
            #                             dab11, riva11, api11, edo11,
            #                             edat, pes, fg, creatinina, ciclo))
                # (ABDFB6A7E39684616522A76482F73C4E99E89113, 2113663, 0, 0, 0, 1, 79, 77.0, 41.0, 111.3, 0)
                # (ABDFB6A7E39684616522A76482F73C4E99E89113, 2119168, 0, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'e11', 79, 77.0, 41.0, 111.3, 0),

                # (0A8CE5A54F0FD8F61BC528F64F929E1C1C326427, 5021243, 0, 0, 0, 1, 67, None, 49.0, 129.0, )
                # (0A8CE5A54F0FD8F61BC528F64F929E1C1C326427, 5038603, 1, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'e11', 67, None, 77.35, 88.0, 0),
                
                # (052BBE5D19D8664102C5197C646661A828F0A721, 2552458, 1, 0, 0, 0, 76, None, 32.5, 116.6, 0)
                # (052BBE5D19D8664102C5197C646661A828F0A721, 2559267, 1, 'No', 'No', 'No', 'No', 'd11', 'No', 'No', 'No', 76, None, 32.5, 116.6, 0),

                # (A4E0CD8D2B82F2AEEEC119FD6AEBB0C1B1CA57CE, 1908569, 0, 0, 0, 0, 74, 81.9, None, None, 0)
                # (A4E0CD8D2B82F2AEEEC119FD6AEBB0C1B1CA57CE, 1913424, 0, 'No', 'r10', 'No', 'No', 'No', 'No', 'No', 'No', 74, 81.9, 84.0, 78.6, 0)
                
        # print(self.incidencia)

    def get_hash(self):
        """."""
        pacients = set([row[0] for row in self.pacients])
        sql = "select id_cip_sec, hash_d, codi_sector from eqa_u11"
        self.hash = {id: (hash, sector) for (id, hash, sector)
                     in u.getAll(sql, nod) if id in pacients}

    def export_taula(self):
        """."""
        upload = self.pacients
        u.writeCSV(u.tempFolder + "alerta_ACOD.csv", upload, sep="@")

        header = ('hsh', 'sect', 'up', 'uba', 'ubainf', 'codi', 'alerta', 'id_cip_sec')  # noqa
        upload2 = []
        upload2.append(header)
        for row in self.pacients:
            hsh, sect = self.hash[row[0]]
            up, uba, ubainf = row[1], row[2], row[3]
            codi, alerta, id_cip_sec = row[4], row[5], row[0]
            upload2.append((hsh, sect, up, uba, ubainf, codi, alerta, id_cip_sec))  # noqa
        u.writeCSV(u.tempFolder + "alerta_ACOD_hsh.csv", upload2, sep=";")

    def delete_rows(self, tb, column='indicador'):
        """."""
        indicador = ('FARM0010','FARM0011')
        #sql = "delete from {} where {} in {}".format(tb, column,
        #                                             tuple(self.pacients))
        sql = "delete from {} where {} in {}".format(tb, column, indicador)                                                     
        u.execute(sql, db)

    def set_mst(self):
        """."""
        tb = 'mst_alertes_pacient'
        self.delete_rows(tb, 'indicador')
        upload = [(id, codi, 1 * alerta)
                  for (id, _up, _uba, _ubainf, codi, alerta) in self.pacients]
        u.listToTable(upload, tb, db)

    def set_pacients(self):
        """."""
        tb = 'exp_ecap_alertes_pacient'
        self.delete_rows(tb, 'grup_codi')
        upload = [(id, up, uba, up, ubainf, codi, 0) + self.hash[id]
                  for (id, up, uba, ubainf, codi, alerta)
                  in self.pacients
                  if alerta]
        u.listToTable(upload, tb, db)

    def set_resultats(self):
        """."""
        tb = 'exp_ecap_alertes_uba'
        self.delete_rows(tb)
        denominador, numerador = c.Counter(), c.Counter()
        for _id, up, uba, ubainf, codi, alerta in self.pacients:
            denominador[(up, uba, 'M', codi)] += 1
            numerador[(up, uba, 'M', codi)] += alerta
            denominador[(up, ubainf, 'I', codi)] += 1
            numerador[(up, ubainf, 'I', codi)] += alerta
        upload = [(up, uba, tipus, codi,
                   numerador[(up, uba, tipus, codi)], n,
                   numerador[(up, uba, tipus, codi)] / float(n))
                  for (up, uba, tipus, codi), n in denominador.items()]
        u.listToTable(upload, tb, db)

    def set_catalegs(self):
        """."""
        tb = 'exp_ecap_alertes_cataleg'
        self.delete_rows(tb)
        # FARM0010 i FARM0011 són inversos
        pare = 'FARM04'
        upload = [('FARM0010', 'Sobredosificació ACOD en FA no valvular', 1,
                   pare, 1, 1, 0, 0, 0, 0,
                   'http://sisap-umi.eines.portalics/indicador/codi/FARM0010',
                   'PCT'),
                  ('FARM0011', 'Infradosificació ACOD en FA no valvular', 2,
                   pare, 1, 1, 0, 0, 0, 0,
                   'http://sisap-umi.eines.portalics/indicador/codi/FARM0011',
                   'PCT')]
        u.listToTable(upload, tb, db)
        tb = 'exp_ecap_alertes_catalegpare'
        u.execute("delete from {} where pare = '{}'".format(tb, pare), db)
        upload = [(pare, 'Alertes de seguretat: ACOD en FA', 3, 'ALERTES')]
        u.listToTable(upload, tb, db)


if __name__ == '__main__':
    start = d.datetime.now()
    # ACOD_FA()
    end = d.datetime.now()
    print('''start: {start}, end:{end}, {delta}'''.format(start=start,
                                                          end=end,
                                                          delta=(end - start)
                                                          )
          )
