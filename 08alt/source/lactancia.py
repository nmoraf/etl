# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re


nod="nodrizas"
imp="import"
alt="altres"
agr_lact=(764,765)
codi='IAD0023'

def get_nens_amb_lact():
    sql='select id_cip_sec from {}.ped_variables where agrupador in {} and val >= 4'.format(nod,agr_lact)
    return {id for id, in getAll(sql,nod)}

def get_presc_vitD():
    presc=defaultdict(set)
    sql='select id_cip_sec,pres_orig from nodrizas.eqa_tractaments where farmac=781'
    for id, pres_orig in getAll(sql,nod):
        presc[id].add(pres_orig)
    return presc

def get_visites():
    sql='select id_cip_sec from {}.ped_visites where edat_m < 4'.format(nod)
    return {id for id, in getAll(sql,nod)}


def get_uba_pacient_tables(denominador,numerador):
    indicador=defaultdict(Counter)
    pacient_rows=[]
    sql = "select id_cip_sec, up, uba,upinf,ubainf,data_naix from {}.ped_assignada where edat_m < 12 ".format(nod)
    for id,up,uba,upinf,ubainf,data_naix in getAll(sql,nod):
        if id in denominador:
            indicador[(up,uba,'M')]['DEN']+=1
            indicador[(upinf,ubainf,'I')]['DEN']+=1
            if id in numerador:
                check_num= [yearsBetween(data_naix,data) < 1 for data in numerador[id]]
                if any(check_num):
                    indicador[(up,uba,'M')]['NUM']+=1
                    indicador[(upinf,ubainf,'I')]['NUM']+=1
                else:
                
                    pacient_rows.append((id, codi,up,uba,upinf,ubainf, 0))
            else:
                pacient_rows.append((id, codi,up,uba,upinf,ubainf, 0))
    rows= [(up,codi, uba, tipus,indicador[(up,uba,tipus)]['NUM'],indicador[(up,uba,tipus)]['DEN'], indicador[(up,uba,tipus)]['NUM']/float(indicador[(up,uba,tipus)]['DEN'])) for (up,uba,tipus) in indicador if indicador[(up,uba,tipus)]['DEN'] !=0 ]
    return rows,pacient_rows



if __name__ == '__main__':
    printTime('inici')

    printTime('id hash')
    table_name="exp_ecap_{}_uba".format(codi)
    uba_columns="(up varchar(5),indicador varchar(15),uba varchar(5),tipus varchar(5),numerador double, denominador int, resultat double)"
    createTable(table_name,uba_columns,alt,rm=True)
    table_name_pacient="exp_ecap_{}_pacient".format(codi)
    pacient_columns="(id_cip_sec int,indicador varchar(15),up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), exclos int)"	
    createTable(table_name_pacient,pacient_columns,alt,rm=True)



    presc_vitd=get_presc_vitD()
    printTime('vitD')

    visites=get_visites()
    printTime('visites')

    lactantes=get_nens_amb_lact()
    printTime('lactantes')


    denominador= visites & lactantes

    rows,pacient_rows= get_uba_pacient_tables(denominador,presc_vitd)
    printTime('rows')

    for rows_list, table in zip([rows,pacient_rows], [table_name,table_name_pacient]):
        listToTable(rows_list,table,alt)
        printTime('Export to table')
