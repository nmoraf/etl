# coding: iso-8859-1
import sisapUtils as u
import pccUtils as pu
import csv
import os
import sys
from collections import Counter
path = os.path.realpath("./") + "/source"
sys.path.append(path)

debug = False
UsarMorts = True

OutFile = u.tempFolder + 'pccmaca.txt'

imp = 'import'
nod = 'nodrizas'
db = 'altres'
taulaAss = 'assignada'
TaulaEstats = 'estats'
catedat = 'khx_edats5a'
TaulaPIIC = 'piic'
TaulaMy = 'exp_khalix_pccmaca'
TaulaMyPre = 'mst_pcc_individual'
khx_uba = 'export.khx_ubas'

u11 = "import.u11"

ConvertPccMaca = """
    CASE es_cod
        WHEN 'ER0001' THEN 'PCC'
        WHEN 'ER0002' THEN 'MACA'
        ELSE 'error'
    END
"""

u.printTime('Pob')
poblacio = {}

sql = """
    SELECT
        id_cip_sec,
        usua_uab_up,
        usua_uab_codi_uab,
        ass_codi_up,
        ass_codi_unitat,
        IF((YEAR(data_ext) - YEAR(usua_data_naixement)) <= 0, 0,
            (YEAR(data_ext) - YEAR(usua_data_naixement))
            - (MID(data_ext, 6, 5) < MID(usua_data_naixement, 6, 5))
            ) as edat
    FROM {0}, nodrizas.dextraccio
    WHERE
        usua_data_naixement <= data_ext
        and usua_cen_dom in ('C','')
        and (usua_situacio = 'A' {1})
    """.format(taulaAss,
               """
               or (usua_situacio='D')
               and usua_data_situacio > date_add(data_ext, interval -1 year)
               """ if UsarMorts else ""
               )

for id, up, uba, upinf, ubainf, edat in u.getAll(sql, imp):
    edat = int(edat)
    poblacio[int(id)] = {'up': up,
                         'uba': uba,
                         'upinf': upinf,
                         'ubainf': ubainf,
                         'edat': edat}

u.printTime('Estats')

gedat, piic, piir, piirV = {}, {}, {}, {}
piirT, pirda, estats, estatsInd = {}, {}, {}, {}

sql = """
    SELECT
        id_cip_sec,
        if(mi_cod_var like ('T4101014%'),'var','text'),
        right(mi_cod_var,2)
    FROM {}
    WHERE
        (mi_cod_var like ('T4101015%') or mi_cod_var like ('T4101014%'))
        and mi_ddb = 0
    """.format(TaulaPIIC)
for id, camp, valor in u.getAll(sql, imp):
    id = int(id)
    if camp == 'var':
        piirV[(id, valor)] = True
    elif camp == 'text':
        piirT[(id, valor)] = True

for (id, valor), r in piirV.items():
    if (id, valor) in piirT:
        piir[id] = True

sql = "select edat,khalix from {}".format(catedat)
for anys, khalix in u.getAll(sql, nod):
    gedat[anys] = khalix

sql = """
    SELECT
        id_cip_sec,
        if(mi_cod_var = 'T4101001', 'rec',
            if(mi_cod_var = 'T4101002' or mi_cod_var like 'T4101017%', 'dant',
            '0')
        )
    FROM {}
    WHERE mi_ddb = 0
    """.format(TaulaPIIC)

for id, tipii in u.getAll(sql, imp):
    piic[int(id)] = int(id)
    if tipii == 'rec':
        piir[int(id)] = True
    elif tipii == 'dant':
        pirda[int(id)] = True

sql = """
    SELECT id_cip_sec,{0},es_up FROM {1} {2}
    """.format(ConvertPccMaca, TaulaEstats, " limit 100" if debug else "")

for id, estat, up_estat in u.getAll(sql, imp):
    id = int(id)
    try:
        up = poblacio[id]['up']
        e = gedat[poblacio[id]['edat']]
        upinf = poblacio[id]['upinf']
        uba = poblacio[id]['uba']
        ubainf = poblacio[id]['ubainf']
    except KeyError:
        continue
    if up == '':
        up = up_estat
    else:
        up = up
    if upinf == '':
        upinf = up_estat
    else:
        upinf = upinf
    if (estat, up, uba, upinf, ubainf, e) in estats:
        estats[(estat, up, uba, upinf, ubainf, e)]['n'] += 1
    else:
        estats[(estat, up, uba, upinf, ubainf, e)] = {'n': 1,
                                                      'p': 0,
                                                      'r': 0,
                                                      'd': 0}
    estatsInd[(id, up, uba, upinf, ubainf)] = {'den': 1, 'num': 0}
    if id in piic:
        estats[(estat, up, uba, upinf, ubainf, e)]['p'] += 1
    if id in piir:
        estats[(estat, up, uba, upinf, ubainf, e)]['r'] += 1
        estatsInd[(id, up, uba, upinf, ubainf)]['num'] += 1
    if id in pirda:
        estats[(estat, up, uba, upinf, ubainf, e)]['d'] += 1


gedat.clear()
piic.clear()
poblacio.clear()

br = {}
sql = 'select distinct scs_codi,ics_codi from cat_centres'
for up1, khx in u.getAll(sql, nod):
    br[up1] = khx

with open(OutFile, 'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (estat, up, uba, upinf, ubainf, e), dict in estats.iteritems():
        num = dict['n']
        pii = dict['p']
        pir = dict['r']
        pid = dict['d']
        try:
            upkhx = br[up]
            upinfkhx = br[upinf]
        except KeyError:
            continue
        w.writerow((up, upkhx, uba, upinf, upinfkhx, ubainf, e, estat, num,
                    pii, pir, pid))
estats.clear()

tb_rec = 'mst_piir'  # guardar piir per altres procs
u.createTable(tb_rec, '(id_cip_sec int)', db, rm=True)
u.listToTable([(id_c,) for id_c in piir], tb_rec, db)

u.printTime('inici carrega')
sql = 'drop table if exists {}'.format(TaulaMy)
u.execute(sql, db)

sql = """
    CREATE TABLE {} (
        up varchar(5) not null default '',
        br varchar(5) not null default '',
        uba varchar(5) not null default '',
        upinf varchar(5) not null default '',
        brinf varchar(5) not null default '',
        ubainf varchar(5) not null default '',
        edat varchar(10) not null default '',
        estat varchar(10),
        recompte int,
        recomptePII int,
        recomptePIIR int,
        recomptePIIDA int
    )
    """.format(TaulaMy)
u.execute(sql, db)
u.loadData(OutFile, TaulaMy, db)

with open(OutFile, 'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (id, up, uba, upinf, ubainf), dict in estatsInd.iteritems():
        num = dict['num']
        den = dict['den']
        try:
            upkhx = br[up]
            upinfkhx = br[upinf]
        except KeyError:
            continue
        w.writerow((id, up, uba, upinf, ubainf, num, den))
estats.clear()
br.clear()

sql = "DROP TABLE IF EXISTS {}".format(TaulaMyPre)
u.execute(sql, db)
sql = """
    create table {} (
        id_cip_sec int,
        up varchar(5) not null default '',
        uba varchar(5) not null default '',
        upinf varchar(5) not null default '',
        ubainf varchar(5) not null default '',
        num int,
        den int
    )
    """.format(TaulaMyPre)
u.execute(sql, db)
u.loadData(OutFile, TaulaMyPre, db)
u.execute("alter table {} add index (id_cip_sec)".format(TaulaMyPre), db)

u.printTime('export a Khalix')
error = []
taula = "%s.%s" % (db, TaulaMy)
sql = """
    SELECT
        estat, concat('A', 'periodo'), br,
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N',
        sum(recompte)
    FROM {0} GROUP BY br, estat, edat
    UNION
    SELECT
        concat('PII', estat), concat('A','periodo'), br,
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N',
        sum(recomptePII)
    FROM {0} GROUP BY br, estat, edat
    UNION
    SELECT
        concat('PIIR', estat), concat('A', 'periodo'), br,
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N',
        sum(recomptePIIR)
    FROM {0} GROUP BY br, estat, edat
    UNION
    SELECT
        concat('PIIDA', estat), concat('A', 'periodo'), br,
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N',
        sum(recomptePIIDA)
    FROM {0} GROUP BY br, estat, edat
    """.format(taula)
filekhx = "PCCMACA"
error.append(u.exportKhalix(sql, filekhx))

taula = "%s.%s" % (db, TaulaMy)
sql = """
    select
        estat, concat('A', 'periodo'), concat(br, 'M', a.uba),
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N', sum(recompte)
    from
        {0} a inner join {1} b on
            a.up = b.up and a.uba = b.uba and b.tipus = 'M'
    group by
        br, b.uba, estat, edat
    union
    select
        concat('PII', estat), concat('A', 'periodo'), concat(br, 'M', a.uba),
        'NOCLI', edat, 'NOIMP','DIM6SET', 'N', sum(recomptePII)
    from
        {0} a inner join {1} b on
         a.up = b.up and a.uba = b.uba and b.tipus = 'M'
    group by
        br, b.uba, estat, edat
    union
    select
        concat('PIIR', estat), concat('A', 'periodo'), concat(br, 'M', a.uba),
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N', sum(recomptePIIR)
    from
        {0} a inner join {1} b on
            a.up = b.up and a.uba = b.uba and b.tipus = 'M'
    group by
        br, b.uba, estat, edat
    union
    select
        concat('PIIDA', estat), concat('A', 'periodo'), concat(br, 'M', a.uba),
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N', sum(recomptePIIDA)
    from
        {0} a inner join {1} b on
            a.up = b.up and a.uba = b.uba and b.tipus = 'M'
    group by
        br, b.uba, estat, edat
    union
    select
        estat, concat('A', 'periodo'), concat(brinf, 'I', ubainf),
        'NOCLI', edat,'NOIMP', 'DIM6SET', 'N', sum(recompte)
    from
        {0} a inner join {1} b on
            a.upinf = b.up and a.ubainf = b.uba and b.tipus = 'I'
    group by
        brinf, ubainf, estat, edat
    union
    select
        concat('PII',estat), concat('A','periodo'), concat(brinf,'I',ubainf),
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N', sum(recomptePII)
    from
        {0} a inner join {1} b on
            a.upinf = b.up and a.ubainf = b.uba and b.tipus = 'I'
    group by
        brinf, ubainf, estat, edat
    union
    select
        concat('PIIR',estat), concat('A','periodo'), concat(brinf,'I',ubainf),
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N', sum(recomptePIIR)
    from
        {0} a inner join {1} b on
            a.upinf = b.up and a.ubainf = b.uba and b.tipus = 'I'
    group by
        brinf, ubainf, estat, edat
    union
    select
        concat('PIIDA',estat), concat('A','periodo'), concat(brinf,'I',ubainf),
        'NOCLI', edat, 'NOIMP', 'DIM6SET', 'N', sum(recomptePIIDA)
    from
        {0} a inner join {1} b on
            a.upinf  =b.up and a.ubainf = b.uba and b.tipus = 'I'
    group by
        brinf, ubainf, estat, edat
    """.format(taula, khx_uba)
filekhx = "PCCMACA_UBA"
error.append(u.exportKhalix(sql, filekhx))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

u.printTime('export a ecap')

ecap_ind = "exp_ecap_pcc_uba"
ecap_pac = "exp_ecap_pcc_pacient"
cataleg = "exp_ecap_pcc_cataleg"
catalegPare = "exp_ecap_pcc_catalegPare"

for indi in pu.Indicadors:
    indicador = indi[0]

pobassig = Counter()
sql = "select id_cip_sec,up,uba,upinf,ubainf from assignada_tot"
for id, up, uba, upinf, ubainf in u.getAll(sql, nod):
    if uba != '':
        pobassig[(up, uba, 'M')] += 1
    if ubainf != '':
        pobassig[(upinf, ubainf, 'I')] += 1

u.execute("drop table if exists {}".format(catalegPare), db)
u.execute("drop table if exists {}".format(cataleg), db)
u.execute("drop table if exists {}".format(ecap_pac), db)
u.execute("drop table if exists {}".format(ecap_ind), db)
u.execute("""
          create table {} (
              up varchar(5) not null default '',
              uba varchar(20) not null default '',
              tipus varchar(1) not null default '',
              indicador varchar(8) not null default '',
              numerador int,
              denominador int,
              resultat double)
    """.format(ecap_ind), db)

indFinalPCC = Counter()
sql = "select up,uba,upinf,ubainf,num,den from {}".format(TaulaMyPre)
for up, uba, upinf, ubainf, num, den in u.getAll(sql, db):
    indFinalPCC[(up, uba, 'M')] += num
    indFinalPCC[(upinf, ubainf, 'I')] += num
with open(OutFile, 'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (up, uba, tipus), dict in pobassig.iteritems():
        try:
            num = indFinalPCC[(up, uba, tipus)]
        except KeyError:
            num = 0
        resul = float(num)/float(dict)
        w.writerow((up, uba, tipus, indicador, num, dict, resul))
u.execute(sql, db)
u.loadData(OutFile, ecap_ind, db)

u.execute("""
          create table {} (
              id_cip_sec double,
              up varchar(5) not null default '',
              uba varchar(20) not null default '',
              upinf varchar(5) not null default '',
              ubainf varchar(7) not null default '',
              grup_codi varchar(10) not null default '',
              exclos int,
              hash_d varchar(40) not null default '',
              sector varchar(4) not null default '')
    """.format(ecap_pac), db)

u.execute("""
          insert into {0}
          select
            a.id_cip_sec, up, uba, upinf, ubainf, '{1}' as grup_codi, 0 exclos,
            hash_d, codi_sector sector
          from
            {2} a inner join {3} b on
                a.id_cip_sec = b.id_cip_sec
          where
            num = 0 and uba <> ''
    """.format(ecap_pac, indicador, TaulaMyPre, u11), db)

u.execute("""
          create table {} (
              indicador varchar(8),
              literal varchar(300),
              ordre int,pare varchar(8),
              llistat int,
              invers int,
              mmin double,
              mint double,
              mmax double,
              toShow int,
              wiki varchar(250),
              tipusvalor varchar(5))
    """.format(cataleg), db)

u.execute("""
    insert into {0}
    values('{1}','{2}','{3}','{4}','{5}','{6}',
           '{7}','{8}','{9}','{10}','{11}','{12}')
    """.format(cataleg,
               indicador,
               pu.indicadorsDict[indicador]['literal'],
               pu.indicadorsDict[indicador]['ordre'],
               pu.indicadorsDict[indicador]['pare'],
               pu.indicadorsDict[indicador]['llistat'],
               pu.indicadorsDict[indicador]['invers'],
               pu.indicadorsDict[indicador]['mmin'],
               pu.indicadorsDict[indicador]['mmint'],
               pu.indicadorsDict[indicador]['mmax'],
               pu.indicadorsDict[indicador]['toShow'],
               pu.indicadorsDict[indicador]['wiki'],
               pu.indicadorsDict[indicador]['tipusvalor']), db)

u.execute("""
          create table {} (
              pare varchar(8) not null default '',
              literal varchar(300) not null default '',
              ordre int,
              pantalla varchar(20)
          )
    """.format(catalegPare), db)

u.execute("""
          insert into {0} values('{1}','{2}','{3}','{4}')
    """.format(catalegPare,
               pu.indicadorsDict[indicador]['pare'],
               pu.indicadorsDict[indicador]['literalPare'],
               pu.indicadorsDict[indicador]['ordrePare'],
               pu.indicadorsDict[indicador]['pantalla']), db)

u.printTime('Fi')
