# coding: iso-8859-1

from sisapUtils import *
from datetime import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'altres'

############ALERTA: PROCÉS MOLT CUTRE PERÒ ÉS URGENT AIXÍ QUE...

#################ho voeln ràpid així que no automatitzo. ALERTAS: Creo els indicadors un per un en compte de des de catàleg

nod = 'nodrizas'
dext, = getOne('select data_ext from dextraccio', nod)

###Agafem la poblacio
pob = {}
sql = 'select id_cip_sec, up, uba, ubainf from assignada_tot'
for id, up, uba, ubainf in getAll(sql, nod):
    pob[id] = {'up': up, 'uba': uba, 'ubainf': ubainf}

######################sacubitril agrupador 735

sacubitril = {}
sql = 'select id_cip_sec, pres_orig from eqa_tractaments where farmac = 735'
for id, dat in getAll(sql, nod):
    try:
        up = pob[id]['up']
        uba = pob[id]['uba']
        ubainf = pob[id]['ubainf']
    except KeyError:
        continue
    sacubitril[id] = {'up': up, 'uba': uba, 'ubainf': ubainf, 'dat': dat}

##############CODIS########################

###############FARM0001 - Sacubitril sense IC 
###############FARM0002 - Continuïtat sacubitril no indicada
###############FARM0003 - Sense criteris inicials sacubitril
###############FARM0004 - Sacubitril contraindicat

cataleg = {
            'FARM0001': {'literal': 'Sacubitril/Valsartan sense IC', 'ordre': 1, 'pare': 'FARM01', 'llistat': 1, 'toShow': 1, 'tipusvalor': 'PCT'},
            'FARM0002': {'literal': 'Incompliment de criteris de Continu�tat', 'ordre': 2, 'pare': 'FARM01', 'llistat': 1, 'toShow': 1, 'tipusvalor': 'PCT'},
            'FARM0003': {'literal': 'Incompliment de criteris indicació', 'ordre': 3, 'pare': 'FARM01', 'llistat': 0, 'toShow': 0, 'tipusvalor': 'PCT'},
            'FARM0004': {'literal': 'Sacubitril/Valsartan contraindicat', 'ordre': 4, 'pare': 'FARM01', 'llistat': 1, 'toShow': 1, 'tipusvalor': 'PCT'},
}

table = 'exp_ecap_alertes_cataleg'
createTable(table, "(indicador varchar(8),literal varchar(300), ordre int, pare varchar(8), llistat int	, invers int not null default 0\
                , mmin double not null default 0, mint double not null default 0, mmax double not null default 0, toShow int\
                , wiki varchar(250) not null default ''	, tipusvalor varchar(5))", db, rm=True)
upload = []
for indicador in cataleg:
        upload.append([indicador, cataleg[indicador]['literal'], cataleg[indicador]['ordre'], cataleg[indicador]['pare'],cataleg[indicador]['llistat'], 0,0,0,0,cataleg[indicador]['toShow'],'http://sisap-umi.eines.portalics/indicador/codi/{}'.format(indicador),cataleg[indicador]['tipusvalor']])   
listToTable(upload, table, db)

table = 'exp_ecap_alertes_catalegpare'
createTable(table, "(pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))", db, rm=True)
upload = []
upload.append(['FARM01', 'Alertes de seguretat: Sacubitril/Valsartan', 0, 'ALERTES'])
listToTable(upload, table, db)



alerta = {}

def CodisAlerta(agr):

    codiAlert = False
    if str(agr) in ['21']:
        codiAlert = 'FARM001'
    elif str(agr) in ['740', '741', '30', '739', '16']:
        codiAlert = 'FARM002'
    elif str(agr) in ['427', '191', '736', '737']:
        codiAlert = 'FARM003'
    elif str(agr) in ['742','743', '160','744','745','56','72']:
        codiAlert = 'FARM004'
        
    return codiAlert
    
###########problemes de salut: IC (21), edema obert o tancat (740, 741), insuficiència hepàtica(160), cirrosi biliar (744),Colestasi(745)

agrupadors = '(21, 740, 741, 160, 744, 745)'

sql = 'select id_cip_sec, ps, dde from eqa_problemes where ps in {}'.format(agrupadors)
for id, ps, dde in getAll(sql, nod):
    alerta[(id, CodisAlerta(ps))] = True
    if ps == 740 or ps ==741:
        alerta[(id, 'FARM004')] = True
    
#############RAM a sacubitril o valsartan (742, 743)

agrupadors = '(742, 743)'

sql = 'select id_cip_sec, agr from eqa_ram where agr in {} and gravetat = 1'.format(agrupadors)
for id, agr in getAll(sql, nod):
    alerta[(id, CodisAlerta(agr))] = True
    
#############Fàrmacs: IECA (56), ARA II (72)


agrupadors = '(56, 72)'
sql = 'select id_cip_sec, farmac, pres_orig from eqa_tractaments where farmac in {}'.format(agrupadors)
for id, farmac, dat in getAll(sql, nod):
    if id in sacubitril:
        dsacu = sacubitril[id]['dat']
        if farmac == 72:
            if dat <> dsacu:
                alerta[(id, CodisAlerta(farmac))] = True
        else:
            alerta[(id, CodisAlerta(farmac))] = True

###############Variables: FG (30), Potassi (739), TAS(16), NYHA (427), FE (191), Peptid natriuretic (736), pro-PNB (737)

agrupadors = '(30, 739, 16, 427, 191, 736, 737)'
alertaVar = {}
ultimaTAS = {}
ultimaNYHA = {}
ultimaFE = {}
ultimPNB = {}
ultimproPNB = {}
ultimFG = {}
ultimK = {}
tenen_potassi = set()

sql = 'select id_cip_sec, agrupador, data_var, valor from eqa_variables where agrupador in {}'.format(agrupadors)
for id, agr, dat, valor in getAll(sql, nod):
    if id in sacubitril:
        dsacu = sacubitril[id]['dat']
        b = monthsBetween(dat, dext)
        if 0 <= b <= 11:
            if agr == 739:
                if (id) in ultimK:
                    datTAS =ultimK[(id)]['data']
                    if datTAS < dat:
                        ultimK[(id)]['data'] = dat
                        ultimK[(id)]['valor'] = valor
                else:
                    ultimK[(id)] = {'data': dat, 'valor': valor} 
            if agr == 30:
                if (id) in ultimFG:
                    datTAS =ultimFG[(id)]['data']
                    if datTAS < dat:
                        ultimFG[(id)]['data'] = dat
                        ultimFG[(id)]['valor'] = valor
                else:
                    ultimFG[(id)] = {'data': dat, 'valor': valor} 
            if agr == 16:
                if (id) in ultimaTAS:
                    datTAS =ultimaTAS[(id)]['data']
                    if datTAS < dat:
                        ultimaTAS[(id)]['data'] = dat
                        ultimaTAS[(id)]['valor'] = valor
                else:
                    ultimaTAS[(id)] = {'data': dat, 'valor': valor}
        if dat <= dsacu:
            if agr == 427:
                if (id) in ultimaNYHA:
                    datTAS =ultimaNYHA[(id)]['data']
                    if datTAS < dat:
                        ultimaNYHA[(id)]['data'] = dat
                        ultimaNYHA[(id)]['valor'] = valor
                else:
                    ultimaNYHA[(id)] = {'data': dat, 'valor': valor}
            elif agr == 191:
                if (id) in ultimaFE:
                    datTAS =ultimaFE[(id)]['data']
                    if datTAS < dat:
                        ultimaFE[(id)]['data'] = dat
                        ultimaFE[(id)]['valor'] = valor
                else:
                    ultimaFE[(id)] = {'data': dat, 'valor': valor}
            elif agr == 736:
                if (id) in ultimPNB:
                    datTAS =ultimPNB[(id)]['data']
                    if datTAS < dat:
                        ultimPNB[(id)]['data'] = dat
                        ultimPNB[(id)]['valor'] = valor
                else:
                    ultimPNB[(id)] = {'data': dat, 'valor': valor}
            elif agr == 737:
                if (id) in ultimproPNB:
                    datTAS =ultimproPNB[(id)]['data']
                    if datTAS < dat:
                        ultimproPNB[(id)]['data'] = dat
                        ultimproPNB[(id)]['valor'] = valor
                else:
                    ultimproPNB[(id)] = {'data': dat, 'valor': valor}

                    
################alerta de dm2 o FG + aliskiren 

crit1 = {}
alertesCutre = {}
sql = 'select id_cip_sec from eqa_problemes where ps=18'
for id, in getAll(sql, nod):
    crit1[id] = True
sql = 'select id_cip_sec, valor, data_var from eqa_variables where agrupador=30 and usar = 1'
for id, valor, data in getAll(sql, nod):
    b = monthsBetween(data, dext)
    if 0 <= b <= 11:
        if 0 <= valor < 60:
            crit1[id] = True
            
sql = 'select id_cip_sec from eqa_tractaments where farmac = 738'
for id, in getAll(sql, nod):
    if id in crit1:
        alertesCutre[id] = True

##################ALERTES#######################

upload = []
for id, valors in sacubitril.items():
    den = 1
    num1, num2, num3, num4 = 1, 0, 0, 0
    if (id, 'FARM001') in alerta:
        num1 = 0
    if (id) in ultimK:
        valor = ultimK[(id)]['valor']
        if 0 <= valor < 3.5:
            num2 = 1
        if 5.4 < valor <1000:
            num2 = 1
    else:
        num2 = 1
    if (id) in ultimFG:
        valor = ultimFG[(id)]['valor']
        if valor <= 30:
            num2 = 1
    else:
        num2 = 1
    if (id) in ultimaTAS:
        valor = ultimaTAS[(id)]['valor']
        if valor <= 100:
            num2 = 1
    else:
        num2 = 1
    if (id) in ultimaNYHA:
        valor = ultimaNYHA[(id)]['valor']
        if valor < 2 or valor >3:
            num3 = 1
    else:
        num3 = 1
    if (id) in ultimaFE:
        valor = ultimaFE[(id)]['valor']
        if valor >35:
            num3 = 1
    else:
        num3 = 1
    if (id) in ultimPNB:
        valor = ultimPNB[(id)]['valor']
        if valor <150:
            num3 = 1
    else:
        num3 = 1
    if (id) in ultimproPNB:
        valor = ultimproPNB[(id)]['valor']
        if valor <500:
            num3 = 1
    else:
        num3 = 1
    if (id, 'FARM004') in alerta:
        num4 = 1
    if (id) in alertesCutre:
        num4 = 1
    upload.append([id, valors['up'], valors['uba'], valors['ubainf'], den, num1, num2, num3, num4])   


table = 'mst_alerta_sacubitril'
createTable(table, '(id_cip_sec int, up varchar(5), uba varchar(5), ubainf varchar(5), den int, num1 int, num2 int, num3 int, num4 int)', db, rm=True)
listToTable(upload, table, db)


u11 = {}
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, 'import'):
    u11[id] = {'sector': sector, 'hash': hash}
    

pacients = []
mst = []
numeradors = Counter()
denominadors = Counter()
sql = 'select id_cip_sec, up, uba, ubainf, den, num1, num2, num3, num4 from {}'.format(table)
for id, up, uba, ubainf, den, num1, num2, num3, num4 in getAll(sql, db):
    try:
        sector = u11[id]['sector']
        hash = u11[id]['hash']
    except KeyError:
        continue
    for indicador in cataleg:
        denominadors[(up, uba, 'M', indicador)] += den
        denominadors[(up, ubainf, 'I', indicador)] += den
    if num1 == 1:
        pacients.append([id, up, uba, up, ubainf, 'FARM0001', 0, hash, sector])
        numeradors[(up, uba, 'M', 'FARM0001')] += num1
        numeradors[(up, ubainf, 'I', 'FARM0001')] += num1
    if num2 == 1:
        pacients.append([id, up, uba, up, ubainf, 'FARM0002', 0, hash, sector])
        numeradors[(up, uba, 'M', 'FARM0002')] += num2
        numeradors[(up, ubainf, 'I', 'FARM0002')] += num2
    if num4 == 1:
        pacients.append([id, up, uba, up, ubainf, 'FARM0004', 0, hash, sector])
        numeradors[(up, uba, 'M', 'FARM0004')] += num4
        numeradors[(up, ubainf, 'I', 'FARM0004')] += num4
    if num3 == 1:
        numeradors[(up, uba, 'M', 'FARM0003')] += num3
        numeradors[(up, ubainf, 'I', 'FARM0003')] += num3
    mst.extend([(id, 'FARM0001', num1),
                (id, 'FARM0002', num2),
                (id, 'FARM0003', num3),
                (id, 'FARM0004', num4)])
  
table = 'exp_ecap_alertes_pacient'
createTable(table, "(id_cip_sec int,up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), grup_codi varchar(10), exclos int,hash_d  varchar(40), sector  varchar(5), index(grup_codi))", db, rm=True)
listToTable(pacients, table, db)

table = 'mst_alertes_pacient'
createTable(table, "(id_cip_sec int, indicador varchar(10), alerta int)", db, rm=True)
listToTable(mst, table, db)
  
resultats = []
for (up, uba, tip, indicador), den in denominadors.items():
    num = 0
    if (up, uba, tip, indicador) in numeradors:
        num = numeradors[(up, uba, tip, indicador)]
    ind = float(num)/float(den)
    resultats.append([up, uba, tip, indicador, num, den, ind])
    
table = 'exp_ecap_alertes_uba'
createTable(table, "(up varchar(5),uba varchar(5),tipus varchar(1), indicador varchar(10), numerador int, denominador int, resultat double, index(indicador))", db, rm=True)
listToTable(resultats, table, db)


professionals = {}

sql = "select up, uab, concat(nom,' ',cognom1, ' ', cognom2) from cat_professionals where tipus='M'"
for up, uba, nom in getAll(sql, 'import'):
    professionals[up, uba] = nom
    

exp4MM = []
sql = "select amb_codi, amb_desc, sap_codi,sap_desc, up, ics_codi, ics_desc, uba, indicador, numerador from {} inner join nodrizas.cat_centres on up=scs_codi where numerador>0 and amb_codi<>'00'".format(table)
for a, b, c, d, up, f, g, uba, i, j in getAll(sql, db):
    desc = cataleg[i]['literal']
    try:
        nom = professionals[up, uba]
    except KeyError:
        nom = 'No el tenim'
    exp4MM.append([a, b, c, d, up, f, g, uba, nom, i, desc, j])
    
file = tempFolder + 'Alertes_sacubitril.txt'
# writeCSV(file, exp4MM, sep=';')
