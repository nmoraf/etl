from sisapUtils import *

db= "altres"
pf= "alt"
    
pac = "%s.exp_ecap_alt_pacient" % db
dext = "nodrizas.dextraccio"    #compte

query= "select up,uba,'M',grup_codi,hash_d,sector,exclos from %s where uba<>'' " % (pac)
query+= " union select upinf,ubainf,'I',grup_codi,hash_d,sector,exclos from %s where ubainf<>''" % (pac)
table= pf + "Llistats"
exportPDP(query=query,table=table,truncate=True,pkOut=True,pkIn=True)

query= "select data_ext from %s limit 1" % dext
table= pf + "LlistatsData"
exportPDP(query=query,table=table,truncate=True)