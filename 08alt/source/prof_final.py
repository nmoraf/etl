# coding: iso-8859-1

from sisapUtils import *
from datetime import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
import string

db = 'altres'

#fem taules pels indicadors de professionals



#u11

u11 = {}
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, 'import'):
    u11[id] = {'sector': sector, 'hash': hash}



#primer cataleg

cataleg = {
            'IAD0004': {'literal': 'Transaminases elevades sense serologies hep�tiques', 'ordre': 1, 'pare': 'IAD', 'llistat': 1, 'invers': 1,'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': True},
            
            'IAD0005': {'literal': 'IC amb escala NYHA', 'ordre': 2, 'pare': 'IAD', 'llistat': 1, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': True},
            'IAD0006': {'literal': 'MPOC amb escala de la dispnea feta', 'ordre': 3, 'pare': 'IAD', 'llistat': 1, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': True},
            'IAD0008': {'literal': 'MACA amb prescripci� estatines', 'ordre': 4, 'pare': 'IADV', 'llistat': 0, 'invers': 1, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0008A': {'literal': 'MACA amb prescripci� estatines com a prevenci� prim�ria', 'ordre': 5, 'pare': 'IADV', 'llistat': 1, 'invers': 1, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0008B': {'literal': 'MACA amb prescripci� estatines com a prevenci� secund�ria', 'ordre': 6, 'pare': 'IADV', 'llistat': 1, 'invers': 1, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0009': {'literal': 'MACA amb prescripci� antiosteopor�tics', 'ordre': 7, 'pare': 'IADV', 'llistat': 1, 'invers': 1, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0011': {'literal': "Mitjana de CAO entre 6 i 14 anys.", 'ordre': 12, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'VA', 'publish': True, 'khalix': False},
            'IAD0012': {'literal': "Mitjana de co entre 6 i 14 anys.", 'ordre': 13, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'VA', 'publish': True, 'khalix': False},
            'IAD0013': {'literal': "Percentatge ARC entre 6 i 14 anys.", 'ordre': 14, 'pare': 'IAD', 'llistat': 0, 'invers': 1, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0014': {'literal': "Percentatge lliures c�ries en dentici� definitiva i temporal entre 6 i 14 anys.", 'ordre': 15, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0015': {'literal': "Percentatge lliures de c�ries en dentici� definitiva entre 6 i 14 anys.", 'ordre': 16, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0016': {'literal': "Percentatge lliures de c�ries en dentici� temporal entre 6 i 14 anys.", 'ordre': 17, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0017': {'literal': "�ndex de restauraci� en dentici� definitiva i temporal entre 6 i 14 anys.", 'ordre': 18, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0018': {'literal': "�ndex de restauraci� en dentici� definitiva entre 6 i 14 anys.", 'ordre': 19, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0019': {'literal': "�ndex de restauraci� en dentici� temporal entre 6 i 14 anys.", 'ordre': 20, 'pare': 'IAD', 'llistat': 0, 'invers': 0, 'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': False},
            'IAD0023': {'literal': 'Suplementaci� amb vitamina D als nens amb lact�ncia materna', 'ordre': 21, 'pare': 'IAD', 'llistat': 1, 'invers': 1,'toShow': 1, 'tipusvalor': 'PCT', 'publish': True, 'khalix': True}}


#Crear catalogo para subindicadors index caries
for numero in range(11,20):
    for letter,edat in zip(string.ascii_lowercase[0:9],range(6,15)):

        pare="IAD00"+str(numero)
        codi=pare+letter
        cataleg[codi]=cataleg[pare].copy()
        cataleg[codi]['literal']=cataleg[codi]['literal'][:-18]+ str(edat)+ " anys."
        cataleg[codi]['pare']=pare
        if numero in (11,12):
            cataleg[codi]['llistat']=0
        else:
            cataleg[codi]['llistat']=1

    

# Atomatizar mas de un indicador en una tabla
list_caries_index= []  
for numero in range(11,20):
    list_caries_index.append("IAD00"+str(numero))
    list_caries_index.extend(["IAD00"+str(numero) +letter for letter in string.ascii_lowercase[0:9]])


indicador_taula= {ind: name for ind, name in zip( ['IAD0008','IAD0008A','IAD0008B','IAD0009']+ list_caries_index,
                                                ["maca"]*4+["index_caries"]* ( len(range(11,20)) * len(string.ascii_lowercase[0:10])))}


#Creem taules de catalegs

table = 'exp_ecap_prof_catalegpare'
createTable(table, "(pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))", db, rm=True)
upload = []
upload.append(['IAD', 'Altres indicadors cl�nics proposats per professionals', 5, 'ALTRES'])
upload.append(['IADV', 'Altres indicadors cl�nics proposats per professionals', 5, 'VALIDACIO'])
listToTable(upload, table, db)




uploadc, uploadp, uploadu = [], [], []
for indicador in cataleg:
    publish = cataleg[indicador]['publish']
    if publish:
        uploadc.append([indicador, cataleg[indicador]['literal'], cataleg[indicador]['ordre'], cataleg[indicador]['pare'],cataleg[indicador]['llistat'], cataleg[indicador]['invers'],0,0,0,cataleg[indicador]['toShow'],'http://sisap-umi.eines.portalics/indicador/codi/' + indicador,cataleg[indicador]['tipusvalor']])   
        if indicador in indicador_taula:
            table_name=indicador_taula[indicador]
            taulaPAC = 'exp_ecap_{}_pacient'.format(table_name)
            sql = "select hash, sector, up, uba, upinf, ubainf, exclos from {0} where indicador = '{1}'".format(taulaPAC, indicador)
            for hash, sector, up, uba, upinf, ubainf, exclos in getAll(sql, db):
                uploadp.append(['', up, uba, upinf, ubainf, indicador, exclos, hash, sector])
            taulaUBA = 'exp_ecap_{}_uba'.format(table_name)
            sql = "select  up, uba, tipus,  numerador, denominador, resultat from {0} where indicador = '{1}'".format(taulaUBA, indicador)
            for up, uba, tipus, num, den, res in getAll(sql, db):
                uploadu.append([up, uba, tipus, indicador, num, den, res])
        else:
            taulaPAC = 'exp_ecap_' + indicador + '_pacient'
            sql = "select id_cip_sec, up, uba, upinf, ubainf, exclos from {}".format(taulaPAC)
            for id, up, uba, upinf, ubainf, exclos in getAll(sql, db):
                try:
                    hash = u11[id]['hash']
                    sector = u11[id]['sector']
                except KeyError:
                    continue
                uploadp.append([id, up, uba, upinf, ubainf, indicador, exclos, hash, sector])
            taulaUBA = 'exp_ecap_' + indicador + '_uba'
            sql = "select  up, uba, tipus, numerador, denominador, resultat from {}".format(taulaUBA)
            for up, uba, tipus, num, den, res in getAll(sql, db):
                uploadu.append([up, uba, tipus, indicador, num, den, res])
        
table = 'exp_ecap_prof_cataleg'
createTable(table, "(indicador varchar(8),literal varchar(300), ordre int, pare varchar(8), llistat int	, invers int not null default 0\
                , mmin double not null default 0, mint double not null default 0, mmax double not null default 0, toShow int\
                , wiki varchar(250) not null default ''	, tipusvalor varchar(5))", db, rm=True)    
listToTable(uploadc, table, db)

table = 'exp_ecap_prof_uba'
createTable(table, "(up varchar(5),uba varchar(5),tipus varchar(1), indicador varchar(10), numerador int, denominador int, resultat double, index(indicador))", db, rm=True)
listToTable(uploadu, table, db)
  
table = 'exp_ecap_prof_pacient'
createTable(table, "(id_cip_sec int,up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), grup_codi varchar(10), exclos int,hash_d  varchar(40), sector  varchar(5), index(grup_codi))", db, rm=True)
listToTable(uploadp, table, db)

#export a khalix (de tots menys els IADV i els de MACA que es carreguen apart)
if IS_MENSUAL:
    uploadup, uploaduba, = Counter(), []
    sql = 'select up, uba, tipus, indicador, numerador, denominador from exp_ecap_prof_uba'
    for up, uba, tipus, indicador, num, den in getAll(sql, db):
        khalix = cataleg[indicador]['khalix']
        if khalix:
            if tipus != 'I':
                uploadup[(indicador, up, 'NUM')] += num
                uploadup[(indicador, up, 'DEN')] += den
            uploaduba.append([indicador, up, tipus, uba, 'NUM', num])
            uploaduba.append([indicador, up, tipus, uba, 'DEN', den])

    upload = []
    for (indicador, up, conc), n in uploadup.items():
        upload.append([indicador, up, conc,n])
       
    table = 'exp_khalix_prof_up'
    createTable(table, "(indicador varchar(10),up varchar(5), conc varchar(10), n int)", db, rm=True)
    listToTable(upload, table, db)   

    table = 'exp_khalix_prof_uba'
    createTable(table, "(indicador varchar(10), up varchar(5), tipus varchar(10), uba varchar(10), conc varchar(10), n int)", db, rm=True)
    listToTable(uploaduba, table, db)   

    #export a khalix
    error= []
    taulaExport = 'altres.exp_khalix_prof_up'
    centres="export.khx_centres"
    query = "select indicador,concat('A','periodo'),ics_codi,conc,'NOCAT','NOINSAT','DIM6SET','N', n from {0} a inner join {1} b on a.up=scs_codi".format(taulaExport, centres)    
    file="IAD"
    error.append(exportKhalix(query,file)) 

    taulaExport = 'altres.exp_khalix_prof_uba'
    centres="export.khx_centres"
    query = "select indicador,concat('A','periodo'),concat(ics_codi,tipus,uba),conc,'NOCAT','NOINSAT','DIM6SET','N',n from {} a inner join {} b on a.up=scs_codi".format(taulaExport, centres)    
    file="IAD_UBA"
    error.append(exportKhalix(query,file))   

