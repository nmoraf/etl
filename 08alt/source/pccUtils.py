# coding: iso-8859-1

Indicadors = [['CRO001'],]


indicadorsDict = {
        'CRO001': {
                'literal': 'PCC i MACA amb recomanacions en cas de crisi partit per poblaci� assignada (tant per mil)',
                'ordre': 1,
                'pare': 'ALT01',
                'llistat': 1,
                'invers': 0,
                'mmin': 0,
                'mmint': 0,
                'mmax': 0,
                'toShow': 1,
                'wiki': 'http://sisap-umi.eines.portalics/indicador/codi/CRONICITAT',
                'tipusvalor': 'TPM',
                'literalPare': 'Atenci� a la cronicitat',
                'ordrePare': 4,
                'pantalla': 'ALTRES'
        },
}
                
               