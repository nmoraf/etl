# coding: utf8

"""
Procés per treure dades de longitudinalitat de inf i comparar en pacient amb i sense UPP
"""

import collections as c
import datetime as d

import sisapUtils as u

tb_pac = "mst_long_cont_pacient_nfc"
tbup = "exp_long_cont_up_nfc"
db = "altres"
file = 'LONG_CONT_NFC'

if u.IS_MENSUAL:

    class LongCont(object):
        """."""

        def __init__(self):
            """Execució seqüencial."""
            self.get_centres()
            self.get_usuaris()
            self.get_pob()
            self.get_upp()
            self.get_visites()
            self.get_taula()
            self.process_sequencia()
            self.get_calculs()
            self.export_dades()
            self.export_khalix()
     
        def get_centres(self):
            """."""
            sql = "select scs_codi, ics_codi from khx_centres"
            self.centres = {up: br for up, br in u.getAll(sql, 'export')}
        
        def get_usuaris(self):
            """."""
            self.usuaris = {}
            sql = "select left(ide_dni, 8), ide_categ_prof_c \
                   from cat_pritb992 where ide_categ_prof_c <> ''"
            for usu, categ in u.getAll(sql, "import"):
                if categ == '30999':
                    self.usuaris[(usu, 'INF')] = categ
        
        def get_pob(self):
            """."""
            u.printTime("Assignada")
            sql = "select id_cip_sec, up, uba, upinf, ubainf from assignada_tot where edat >14"
            self.pob = {id: (up, uba, upinf, ubainf) for (id, up, uba, upinf, ubainf) in u.getAll(sql, 'nodrizas')}
        
        def get_upp(self):
            """Agrupadors UPP és el 91 (obert) i 92 (tancat). però uso només els codis cim i els busco a import"""
            agrs = []
            self.upps = c.defaultdict(lambda: c.defaultdict(set))
            sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in (91, 833)"
            for agrupador, criteri in u.getAll(sql, 'nodrizas'):
                agrs.append(criteri)
            in_crit = tuple(agrs)
            sql = "select id_cip_sec, pr_dde,if(pr_dba = 0,data_ext,pr_dba), if(pr_dba=0,0,1) from problemes, nodrizas.dextraccio \
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and pr_cod_ps in {0} \
                and pr_dde > date_add(data_ext, interval -2 year)".format(in_crit)
            for id, dde, dba, tancades in u.getAll(sql, 'import'):
                self.upps[id][dde].add(dba)
        
        def get_visites(self):
            """Per agafar visites entre les UPP"""
            u.printTime("Visites")
            self.visitespacients = c.Counter()
            self.data = c.defaultdict(lambda: c.defaultdict(list))
            sql = "select id_cip_sec, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei\
                        from visites2 \
                        where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>'' and visi_col_prov_resp like ('3%') \
                        and  s_espe_codi_especialitat not in ('EXTRA', '10102') and visi_tipus_visita not in ('EXTRA', 'EXT')"
            for id, sector, up, col, dat, categoria, serveis in u.getAll(sql, "import"):
                if up in self.centres:
                    if id in self.upps:
                        for dde in self.upps[id]:
                            dbas = self.upps[id][dde]
                            for dba in dbas:
                                if dde <= dat <= dba:
                                    if (col, 'INF') in self.usuaris:
                                        servei = 'INF'
                                        self.visitespacients[(id, dde, servei, col)] += 1
                                        identify = str(id) + '@' + str(dde) + '@' + servei
                                        self.data[identify][dat].append(col)
            
        def get_taula(self):
                """Calculem les tres dades necessàries per calcular els indicadors. A nivell de pacient necessitem:
                    1) Nombre de professionals diferents
                    2) Nombre de visites totals
                    3) Nombre visites professional majoritari
                    4) El sumatori dels quadrats de les visites de cada professional (per a calcular COC)
                """
                u.printTime("Càlculs")
                self.pacients = {}
                for (id, dde, servei, dummy_col), rec in self.visitespacients.items():
                    Rq2 = rec * rec
                    if (id, dde, servei) in self.pacients:
                        self.pacients[(id, dde, servei)]['totals'] += rec
                        self.pacients[(id, dde, servei)]['nprof'] += 1
                        self.pacients[(id, dde, servei)]['rq2'] += Rq2
                        majoritari = self.pacients[(id, dde, servei)]['vprof']
                        if rec > majoritari:
                            self.pacients[(id, dde, servei)]['vprof'] = rec
                    else:
                        self.pacients[(id, dde, servei)] = {'totals': rec, 'nprof': 1, 'vprof': rec, 'rq2': Rq2}

        def process_sequencia(self):
            """Ordenem les visites per ordre temporal per poder calcular el de sequencia"""
            u.printTime("Seqüència")
            resultat = []
            for (id), dates in self.data.items():
                pac = ((id, dat, cols) for dat, cols in dates.items())
                colegiat = {}
                for i, (id, dat, cols) in enumerate(sorted(pac, key=lambda x: x[1]), start=1):
                    for col in cols:
                            if col == colegiat:
                                seq = 1
                            else:
                                seq = 0
                                colegiat = col
                            identities = id.split('@')
                            resultat.append((int(identities[0]), identities[1],identities[2], dat, col, i, seq))
            Stb = "Long_sequencia_upp"
            Scolumns =["id_cip_sec int", "dde varchar(20)", "servei varchar(10)", "data date", "colegiat varchar(10)", "ordre int","sequencia int"] 
            u.createTable(Stb, "({})".format(", ".join(Scolumns)), db, rm=True)            
            u.listToTable(resultat, Stb, db)
                
            self.indicador_sequencia = c.Counter()
            sql = "select id_cip_Sec, dde, servei, sequencia from {}".format(Stb)
            for id, dde, servei, seq in u.getAll(sql, db):
                self.indicador_sequencia[id, dde, servei, 'num'] += seq
                self.indicador_sequencia[id, dde, servei, 'den'] += 1

            
        def get_calculs(self):
            """Calculem els indicadors a nivell de pacient
            Aqui filtrem per població assignada i pacients entre 3 i 300 visites"""
            u.printTime("Més càlculs")
            self.upload = []
            for (id, dde, servei), dad in self.pacients.items():
                totals, nprof, vprof, rq2 = dad['totals'], dad['nprof'], dad['vprof'], dad['rq2']
                if 3 <= totals <= 300:
                    n1 = 1 - (nprof/(totals + 0.1))
                    d1 = 1 - (1/(totals + 0.1))
                    n3 = self.indicador_sequencia[id, str(dde), servei, 'num']
                    d3 = self.indicador_sequencia[id, str(dde), servei, 'den']
                    r1 = float(n1/d1)
                    r2 = float(vprof) / float(totals)
                    d3 = d3 -1
                    r3 = float(n3)/float(d3)
                    n4 = rq2 - totals
                    d4 = totals * (totals - 1)
                    r4 = float(n4) / float(d4)
                    if id in self.pob:
                        dbas = self.upps[id][dde]
                        for dba in dbas:
                            up, uba, upinf, ubainf = self.pob[id][0], self.pob[id][1], self.pob[id][2], self.pob[id][3]
                            br = self.centres[up]
                            self.upload.append([id, br, servei, totals, dde, dba, r1, r2, r3, r4])
                            
        def export_dades(self):
            """."""
            columns = ["id varchar(100)",   "up varchar(5)", "servei varchar(10)", "nvis_upp int",
                        "dini_upp date", "dfi_upp date",
                         "r1 double", "r2 double", "r3 double", "r4 double"]
            u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
            u.listToTable(self.upload, tb_pac, db)
        
        def export_khalix(self):
            """Exportar a khalix els indicadors"""
            u.printTime("Khalix UP")
            export = []
            sql = "select right(year(data_ext),2),date_format(data_ext,'%m') from dextraccio"
            for anys, mes in u.getAll(sql, "nodrizas"):
                periodo = "A" + str(anys) + str(mes)
                    
            sql = "select up, servei, sum(r1), sum(r2), sum(r3), sum(r4), count(*) from {} group by up, servei".format(tb_pac)
            for br, servei, r1, r2, r3, r4, den in u.getAll(sql, db):
                detalle = 'TIPPROF4'
                export.append(['CONT0001N', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r1])
                export.append(['CONT0001N', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0002N', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r2])
                export.append(['CONT0002N', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0003N', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r3])
                export.append(['CONT0003N', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0004N', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r4])
                export.append(['CONT0004N', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
            
            columnsU = ["indicador varchar(10)", "periode varchar(10)", "up varchar(5)","analisi varchar(5)", "nc varchar(10)", "detalle varchar(10)", "d1 varchar(10)", "n varchar(5)", "resultat double"]
            u.createTable(tbup, "({})".format(", ".join(columnsU)), db, rm=True)
            u.listToTable(export, tbup, db)  
            sql = "select * from {}.{}".format(db, tbup)
            u.exportKhalix(sql, file)  
    
    
    if __name__ == "__main__":
        LongCont()

    