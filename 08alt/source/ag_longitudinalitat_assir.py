from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random

nod="nodrizas"
imp="import"
validation=False
limit=""
codi_indicador="LONGASS001"
alt="altres"
exclusions=("VU","U","E", "ES","TN","GR", "GM")
ups=('01422',)

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_llevadores():
    """Selecciona el dni de aquellos profesionales que son llevadores.
       Devuelve un set de estos dnis.
    """
    sql = "select IDE_DNI from {0}.cat_professionals_assir where TIPUS='L';".format(imp)
    return {id for (id,) in getAll(sql,imp)}
    
def get_embaras_actiu(current_date):
	""" Selecciona los id_cip_seq de las mujeres en assir con embarazo activo 
        (que la fecha de inicio o la de fin sea de hace menos de 12 meses y que al menos hayan pasado mas de 3 meses de gestacion)
		Devuelve un set de id_cip_seq
	"""
	sql = "select id_cip_sec,inici,fi from {0}.ass_embaras;".format(nod)
    #monthsBetween(inici,fi) > 3
	embaras_ids = {id for id,inici,fi in getAll(sql,nod) if (monthsBetween(inici,current_date) <12 or monthsBetween(fi,current_date) <12)}
	return embaras_ids

def get_embaras_atessas(embaras_ids,llevadores_ids,exclusions,current_date):
    """Selecciona aquellas embarazadas que han hecho una visita a una llevadora en los ultimos 12 meses. Primero se mira si la visita se ha hecho en el ultimo any,
       luego que la paciente tiene un embarazo (esta embaras_ids) y luego que el profesional que llevo la visita es una llevadora.
       Se devuelve un diccionario de id: set(dni llevadoras) 
    """
    sql = "select id_cip_sec,visi_data_visita, visi_dni_prov_resp from {}.ass_visites where visi_tipus_visita not in {} ;".format(nod,exclusions)
    print(sql)
    embaras_llevadores=defaultdict(set)
    for id,data,dni in getAll(sql,nod):
        if monthsBetween(data,current_date) < 12 and id in embaras_ids and dni in llevadores_ids:
            embaras_llevadores[id].add(dni)
            
    return embaras_llevadores

def get_indicador (embaras_llevadores,val=False):
    """Se crea las filas de la tabla final agrupadas por up asignada a la embarazada en assir. Se anyade al denominador si el id esta en las keys del diccionario
    embaras_llevadores que recoge aquellas pacientes con embarazo activo atendidas por llevadores en el el ultimo any. Se anyade al numerador si ha sido atendida por un maximo
    de dos llevadores (la longitud del set de llevadoras para una paciente es mayor o igual a 2)
    """
    if val:
        counter_1=0
        counter_2=0
        
    cumplidores_ids=defaultdict(set)
    no_cumplidores_ids=defaultdict(set)
    uba_rows=[]
    indicador=defaultdict(Counter)
    sql = "select id_cip_sec, visi_up from {}.ass_imputacio_up".format(nod)
    for id, up in getAll(sql, nod):
        if id in embaras_llevadores:
            indicador[up]["DEN"]+=1
            if 0 < len(embaras_llevadores[id]) <=2 :
                indicador[up]["NUM"]+=1
                if val and counter_1 < 10 and up in ups:
                    cumplidores_ids[up].add(id)
                    counter_1+=1
            else:
                if val and counter_2 < 10 and up in ups:
                    no_cumplidores_ids[up].add(id)
                    counter_2+=1


                
    uba_rows = [(up,  codi_indicador,indicador[up]["NUM"], indicador[up]["DEN"])
                          for up in indicador]
    
    return uba_rows,no_cumplidores_ids,cumplidores_ids
	

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

def get_up_desc(up):
    sql="select up_desc from ass_centres where up={};".format(up)
    return getOne(sql,"nodrizas")[0]

def get_hashd_dict():
    sql="select id_cip_sec,hash_d from import.u11;"
    return {id:hashd for (id,hashd) in getAll(sql,"import")}	

def get_cip_dict(hashd):
    sql="select usua_cip,usua_cip_cod from pdptb101 where usua_cip_cod='{}'".format(hashd)
    print(sql)
    return getOne(sql,"pdp")[0]

def write(filename,dict_ids,hash_dict):
    f=open(filename,"w")
    for up in dict_ids:
        up_desc=get_up_desc(up)
        for id in dict_ids[up]:
            print(hash_dict[id])
            cip=get_cip_dict(hash_dict[id])
            printTime("cip")
            f.write("{}\t{}\n".format(up_desc,cip))
    f.close()
	
if __name__ == '__main__':
    printTime('inici')
    current_date=get_date_dextraccio()

    printTime("Dia actual {}".format(current_date))
    
    embaras_ids=get_embaras_actiu(current_date)
    print(random.sample(embaras_ids,5))
    printTime("Emb activas")
    llevadores_ids=get_llevadores()
    print(random.sample(llevadores_ids,5))
    printTime("Llevadores")

    embaras_llevadores=get_embaras_atessas(embaras_ids,llevadores_ids,exclusions,current_date)
    print(random.sample(embaras_llevadores,5))
    printTime('Retrieving num and den')

    rows,no_cumplidores_ids,cumplidores_ids=get_indicador(embaras_llevadores,validation)
    printTime('Get Rows')
	#exportar las tablas a la base de datos altres
    columns="(up varchar(5),indicador varchar(11),numerador int, denominador int)"
    export_table("exp_khalix_{}_up".format(codi_indicador),
				columns,
				alt,
				rows)

    if validation:
    
        hash_id_dict=get_hashd_dict()
        printTime("hash")
        #cip_hash_dict=get_cip_dict()
        #printTime("cip")

       
        for filename,dict_ids in zip(["LONGASS001_{}.txt".format(cump) for cump in ["no_cumplidores","cumplidores"]], [no_cumplidores_ids,cumplidores_ids]):
            write(filename,dict_ids,hash_id_dict)



        


    
    



    
				