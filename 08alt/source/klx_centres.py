from sisapUtils import getOne, getAll, createTable, listToTable, exportKhalix
from string import ascii_lowercase


valors = {(1, 1): 'AMBTS',
          (0, 1): 'SENSETS',
          (1, 0): 'SENSEODO',
          (0, 0): 'SENSETSODO'}
tb_in = 'cat_centres'
db_in = 'nodrizas'
tb_out = 'exp_khalix_centres'
db_out = 'altres'
file = 'CENTRES_CATALEG'
period, = getOne('select year(data_ext) from dextraccio', db_in)


sql = "select ics_codi, soc, odn from {0} \
       union select concat('SAP', sap_codi), if(avg(soc) = 0, 0, 1), \
                    if(avg(odn) = 0, 0, 1) from {0} group by 1 \
       union select concat('AMB', amb_codi), if(avg(soc) = 0, 0, 1), \
                    if(avg(odn) = 0, 0, 1) from {0} group by 1 \
       union select 'AMBITOS', 1, 1 from {0}".format(tb_in)
resul = [('TSODOTIP',
          'DEF{}'.format(period),
          centre,
          'NOCLI',
          'NOCAT',
          'NOIMP',
          'DIM6SET',
          'S',
          valors[(soc, odn)])
         for (centre, soc, odn) in getAll(sql, db_in)]

cols = ', '.join(('{} varchar(10)'.format(ascii_lowercase[i]) for i in range(9)))
createTable(tb_out, '({})'.format(cols), db_out, rm=True)
listToTable(resul, tb_out, db_out)

error = []
query = 'select * from {}.{}'.format(db_out, tb_out)
error.append(exportKhalix(query, file))
if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
