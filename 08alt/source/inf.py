# coding: iso-8859-1
# estat igual a I son les de abordatge propi
# motiu A es Altres. Hi havia calcul diferenciat per altres i no altres.

from sisapUtils import *
import csv, os, sys
from time import strftime
from collections import defaultdict, Counter

debug = False

db = "altres"
imp = "import"
nod = "nodrizas"

dag = "aguda"
pla = "pla2"
dextr = "dextraccio"
ps = "eqa_problemes"
u11 = "u11"


agr_pla = ('55', '18', '62', '21', '1', '58', '86', '239', '45', '276')


path0 = os.path.realpath("../")
path = path0.replace("\\", "/") + "/08alt/dades_noesb/inf_indicadors.txt"
OutFile = tempFolder + 'DAG.txt'
OutFileC = tempFolder + 'Cures.txt'
OutFileG = tempFolder + 'Grupal.txt'
OutFileSa = tempFolder + 'SeguimentNenSa.txt'
indicadorDAG = "DAG001"
indicadorPLA = "PLA001"

cataleg = "exp_ecap_inf_cataleg"
catalegPare = "exp_ecap_inf_catalegPare"
llistats = "exp_ecap_inf_pacient"

conceptes = [['NUM', 'num'], ['DEN', 'den']]

sql = "select data_ext,year(data_ext) from {}".format(dextr)
for d, e in getAll(sql, nod):
    dext = d
    aactual = int(e)

printTime('Pob')


def getPacients():

    assig = {}
    sql = 'select id_cip_sec,upinf,ubainf,if(institucionalitzat=1,2,if(ates=0,5,if(maca=1,1,0))) exclos from assignada_tot'
    for id_cip_sec, upinf, ubainf, exclos in getAll(sql, nod):
        assig[int(id_cip_sec)] = {'upinf': upinf, 'ubainf': ubainf, 'exclos': exclos}
    return assig
ass = getPacients()

uhash = {}
sql = "select id_cip_sec,hash_d,codi_sector from {}".format(u11)
for id_cip_sec, hash, sector in getAll(sql, imp):
    id_cip_sec = int(id_cip_sec)
    uhash[id_cip_sec] = {'hash': hash, 'sector': sector}

printTime('Inici calcul DAG')
nens = set([id for id, in getAll('select id_cip_sec from ped_assignada', 'nodrizas')])
idag = {}
sql = "select id_cip_sec,cesp_up,cesp_cod_mce,cesp_estat,cesp_usu_alta,cesp_data_alta, date_format(cesp_data_alta,'%m'),year(cesp_data_alta) from {0} where year(cesp_data_alta)='{1}' {2}".format(dag, aactual, ' limit 10' if debug else '')
for id, up, mot, estat, usu, data, mes, a in getAll(sql, imp):
    if mot == 'A':
        if id in nens:
            mot = 'ALPEDIA'
        else:
            mot = 'ALADULT'
    a = int(a)
    if data <= dext:
        if up:
            if estat == 'I':
                if (usu, up, mot, mes, a) in idag:
                    idag[(usu, up, mot, mes, a)]['num'] += 1
                    idag[(usu, up, mot, mes, a)]['den'] += 1
                else:
                    try:
                        idag[(usu, up, mot, mes, a)] = {'num': 1, 'den': 1}
                    except KeyError:
                        ok = 1
            else:
                if (usu, up, mot, mes, a) in idag:
                    idag[(usu, up, mot, mes, a)]['den'] += 1
                else:
                    try:
                        idag[(usu, up, mot, mes, a)] = {'num': 0, 'den': 1}
                    except KeyError:
                        ok = 1
with open(OutFile, 'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (usu, up, mot, mes, a), d in idag.items():
        num = d['num']
        den = d['den']
        try:
            w.writerow([usu, up, mes, a, mot, num, den])
        except KeyError:
            continue
idag.clear()
table = "mst_dag"
sql = 'drop table if exists {}'.format(table)
execute(sql, db)
sql = "create table {} (usuari varchar(15) not null default'',up varchar(5) not null default'',mes varchar(2) not null default'', anys varchar(4) not null default'',motiu varchar(20) not null default'',num double,den double)".format(table)
execute(sql, db)
loadData(OutFile, table, db)

printTime('Export a khalix')
tkhalix = "exp_khalix_dag_up_ind"

sql = 'drop table if exists {}'.format(tkhalix)
execute(sql, db)
sql = "create table {} (up varchar(5) not null default'',mes varchar(2) not null default'', anys varchar(4) not null default'',motiu varchar(20) not null default'',conc varchar(5) not null default'',n double)".format(tkhalix)
execute(sql, db)
for r in conceptes:
    conc, var = r[0], r[1]
    sql = "insert into {0} select up,mes,anys,motiu,'{1}',sum({2}) n from {3} group by up,mes,anys,motiu".format(tkhalix, conc, var, table)
    execute(sql, db)

error = []

centres = "export.khx_centres"
taula = "%s.%s" % (db, tkhalix)
query = "select motiu,concat('A',right(anys,2),mes),ics_codi,conc,'NOCAT','NOIMP','DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula, 'centres': centres}
file = "INF_EAP"
error.append(exportKhalix(query, file))

centres = "nodrizas.urg_centres"
taula = "%s.%s" % (db, tkhalix)
query = "select motiu,concat('A',right(anys,2),mes),ics_codi,conc,'NOCAT','NOIMP','DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula, 'centres': centres}
file = "INF_ACUT"
error.append(exportKhalix(query, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Fi calcul DAG')

printTime('Inici calcul Cures')

plac = {}
sql = "select id_cip_sec,snnv_num_spc,snnv_data_alta,snnv_dus,snnv_cod_noc,snnv_cod_nic,snnv_cod_o_ac,snnv_cod_ac,snnv_cod_var from {0},\
        nodrizas.{1} where snnv_data_alta between date_add(date_add(data_ext,interval -2 year),interval +1 day) \
        and data_ext or snnv_dus between date_add(date_add(data_ext,interval -2 year),interval +1 day) and data_ext".format(pla, dextr)
for idcipsec, codi, alta, seg, noc, nic, x, act, var in getAll(sql, imp):
    idcipsec = int(idcipsec)
    if nic == '*':
        plac[(idcipsec, codi, noc)] = {'control': 0}
sql = "select id_cip_sec,snnv_num_spc,snnv_data_alta,snnv_dus,snnv_cod_noc,snnv_cod_nic,snnv_cod_o_ac,snnv_cod_ac,snnv_cod_var from {0},nodrizas.{1}\
        where year(snnv_data_alta)=year(data_ext) or year(snnv_dus)=year(data_ext)".format(pla, dextr)
for idcipsec, codi, alta, seg, noc, nic, x, act, var in getAll(sql, imp):
    idcipsec = int(idcipsec)
    if nic <> '*' and act == '*' and var == '*':
        try:
            plac[(idcipsec, codi, noc)]['control'] = 1
        except KeyError:
            ok = 1
    if act <> '*' or var <> '*':
        try:
            plac[(idcipsec, codi, noc)]['control'] = 1
        except KeyError:
            ok = 1
pla2 = {}
for (idcipsec, codi, noc), dict in plac.iteritems():
    n = dict['control']
    if n == 1:
        try:
            if (idcipsec, codi) in pla2:
                ok = 1
            else:
                pla2[(idcipsec, codi)] = 1
        except KeyError:
            continue
plac.clear()
plaf = {}
for (idcipsec, codi), a in pla2.iteritems():
    try:
        if idcipsec in plaf:
            plaf[(idcipsec)]['n'] += 1
        else:
            plaf[(idcipsec)] = {'n': 1}
    except KeyError:
        ok = 1
pla2.clear()
placures = {}
sql = "select distinct id_cip_sec , ps from {0} where ps in {1}".format(ps, agr_pla)
for idcipsec, agr in getAll(sql, nod):
    idcipsec = int(idcipsec)
    agr = int(agr)
    try:
        upinf = ass[idcipsec]['upinf']
        ubainf = ass[idcipsec]['ubainf']
        exclos = ass[idcipsec]['exclos']
    except KeyError:
        continue
    if agr == 45 or agr == 276:
        try:
            if idcipsec in placures:
                placures[idcipsec]['n'] += 2
            else:
                placures[idcipsec] = {'up': upinf, 'uba': ubainf, 'exclos': exclos, 'n': 2, 'pla': 0}
        except KeyError:
            ok = 1
    else:
        try:
            if idcipsec in placures:
                placures[idcipsec]['n'] += 1
            else:
                placures[idcipsec] = {'up': upinf, 'uba': ubainf, 'exclos': exclos, 'n': 1, 'pla': 0}
        except KeyError:
            ok = 1

for (idcipsec), dict in plaf.iteritems():
    p = dict['n']
    try:
        placures[idcipsec]['pla'] = p
    except KeyError:
        continue
with open(OutFileC, 'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (idcipsec), d in placures.items():
        n = d['n']
        n = int(n)
        if n >= 2:
            try:
                hash = uhash[idcipsec]['hash']
                sector = uhash[idcipsec]['sector']
            except KeyError:
                continue
            try:
                w.writerow([idcipsec, hash, sector, d['up'], d['uba'], d['exclos'], d['pla']])
            except KeyError:
                continue
placures.clear()
tablep = "mst_pla"
sql = 'drop table if exists {}'.format(tablep)
execute(sql, db)
sql = "create table {} (id_cip_sec int,hash varchar(40) not null default'', sector varchar(4) not null default'',up varchar(5) not null default'',uba varchar(5) not null default'',exclos int, num double)".format(tablep)
execute(sql, db)
loadData(OutFileC, tablep, db)

tablep2 = "mst_pla_tots"  # guardar tots els plans de cures per altres procs
createTable(tablep2, '(id_cip_sec int, num int)', db, rm=True)
listToTable([(k, v['n']) for (k, v) in plaf.items()], tablep2, db)

printTime('Export a khalix')

error = []
centres = "export.khx_centres"

taulapla = "%s.%s" % (db, tablep)
query = "select 'CURES',concat('A','periodo','YTD'),ics_codi,'NUM','NOCAT','NOIMP','DIM6SET','N',sum(if(num=0,0,1)) from %(taula)s a inner join %(centres)s b on a.up=scs_codi group by ics_codi\
        union select 'CURES',concat('A','periodo','YTD'),ics_codi,'DEN','NOCAT','NOIMP','DIM6SET','N',count(*)  from %(taula)s a inner join %(centres)s b on a.up=scs_codi group by ics_codi" % {'taula': taulapla, 'centres': centres}
file = "CURES_UP"
error.append(exportKhalix(query, file))

taulaubapla = "%s.%s" % (db, tablep)
query = "select 'CURES',concat('A','periodo','YTD'),concat(ics_codi,'I',uba),'NUM','NOCAT','NOIMP','DIM6SET','N',sum(if(num=0,0,1))  from %(taula)s a inner join %(centres)s b on a.up=scs_codi group by ics_codi,uba\
        union select 'CURES',concat('A','periodo','YTD'),concat(ics_codi,'I',uba),'DEN','NOCAT','NOIMP','DIM6SET','N',count(*)  from %(taula)s a inner join %(centres)s b on a.up=scs_codi group by ics_codi,uba" % {'taula': taulaubapla, 'centres': centres}
file = "CURES_IND"
error.append(exportKhalix(query, file))
if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Export a ECAP')
etaula = "exp_ecap_inf_uba"
sql = 'drop table if exists {}'.format(etaula)
execute(sql, db)
sql = "create table {} (up varchar(5) not null default'',uba varchar(20) not null default'',tipus varchar(1) not null default'',indicador varchar(7) not null default'',num double, den double, resultat double)".format(etaula)
execute(sql, db)

sql = "insert into {0} select up, usuari uba, 'I' tipus, '{1}' indicador, sum(num),sum(den), round(sum(num)/sum(den),4) resultat from {2} group by up, usuari".format(etaula, indicadorDAG, table)
execute(sql, db)

sql = "insert into {0} select up, uba, 'I' tipus, '{1}' indicador, sum(if(num=0,0,1)),count(*), round(sum(if(num=0,0,1))/count(*),4) resultat from {2} group by up, uba".format(etaula, indicadorPLA, tablep)
execute(sql, db)

sql = "drop table if exists {}".format(llistats)
execute(sql, db)
sql = "create table {} (id_cip_sec double,up varchar(5) not null default '',uba varchar(7) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')".format(llistats)
execute(sql, db)
sql = "alter table {} add index(id_cip_sec)".format(llistats)
execute(sql, db)
sql = "insert into {0} select id_cip_sec,'' up,0 uba,up upinf,uba ubainf,'{1}' as grup_codi,exclos,hash,sector from {2} where num=0".format(llistats, indicadorPLA, tablep)
execute(sql, db)

sql = "drop table if exists {0},{1}".format(cataleg, catalegPare)
execute(sql, db)
sql = "create table {} (indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),llistat int,invers int,mmin double,mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))".format(cataleg)
execute(sql, db)
sql = "create table {0} (pare varchar(8) not null default'',literal varchar(300) not null default '',ordre int, pantalla varchar(20))".format(catalegPare)
execute(sql, db)
sql = "alter table {} add primary key(pare)".format(catalegPare)
execute(sql, db)
with open(path, 'rb') as file:
    p = csv.reader(file, delimiter='@', quotechar='|')
    for ti in p:
        i, desc, grup, grupdesc, baixa, dbaixa, llistat, ordre, grup_ordre, tipusvalor, pantalla = ti[0], ti[1], ti[2], ti[3], ti[4], ti[5], ti[6], ti[7], ti[8], ti[9], ti[10]
        if baixa == "1":
            ok = 1
        else:
            sql = "insert into {0} values('{1}','{2}','{3}','{4}','{5}',0,0,0,0,1,concat('http://sisap-umi.eines.portalics/indicador/codi/','{1}'),'{6}')".format(cataleg, i, desc, ordre, grup, llistat, tipusvalor)
            execute(sql, db)
            sql = "insert ignore into {0} values('{1}','{2}','{3}','{4}')".format(catalegPare, grup, grupdesc, grup_ordre, pantalla)
            execute(sql, db)
printTime('Fi Calcul Cures')

printTime('Inici calcul Altres estrategics')

grupal = Counter()
periodes = {}
sql = "select id_cip_sec,data,date_format(data,'%m') from inf_grupal where year(data)={0} and data<='{1}'".format(aactual, dext)
for id, data, mes in getAll(sql, nod):
    anys = str(aactual)[2:]
    periode = 'A' + anys + mes
    periodes[periode] = True
    grupal[(id, data, periode)] += 1

sql = "select id_cip_sec, data_var,date_format(data_var,'%m') from eqa_variables where agrupador=499 and year(data_var)={0} and data_var<='{1}'\
        union select id_cip_sec,dat,date_format(dat,'%m') from ped_variables where agrupador=499 and year(dat)={0} and dat<='{1}'".format(aactual, dext)
for id, data, mes in getAll(sql, nod):
    anys = str(aactual)[2:]
    periode = 'A' + anys + mes
    periodes[periode] = True
    if (id, data, periode) in grupal:
        continue
    else:
        grupal[(id, data, periode)] += 1

tablegrup = "mst_grupal"
sql = 'drop table if exists {}'.format(tablegrup)
execute(sql, db)
sql = "create table {} (id_cip_sec int,up varchar(5) not null default'',uba varchar(5) not null default'',periode varchar(5) not null default'',n double)".format(tablegrup)
execute(sql, db)

for pe in periodes.items():
    periode = pe[0]
    grupals = Counter()
    for (id, data, period), n in grupal.items():
        if period <= periode:
            grupals[id] += n

    with open(OutFileG, 'wb') as file:
        w = csv.writer(file, delimiter='@', quotechar='|')
        for id, n in grupals.items():
            try:
                upinf = ass[id]['upinf']
                ubainf = ass[id]['ubainf']
            except KeyError:
                continue
            if n > 3:
                w.writerow([id, upinf, ubainf, periode, n])
    loadData(OutFileG, tablegrup, db)


def RNSAConverter(valor):

    if valor == 6 or valor == 25:
        return 'PNS_8m'
    if valor == 13 or valor == 30:
        return 'PNS_6a'
    else:
        return 'NO'
professionals = {}
sql = "select ide_usuari,if(left(ide_categ_prof_c,1)='1',0,1) from cat_pritb992"
for usu, isInf in getAll(sql, imp):
    isInf = int(isInf)
    professionals[usu] = {'isInf': isInf}
RNSA = {}
sql = "select id_cip_sec,dat,val,year(dat), date_format(dat,'%m') from ped_variables where agrupador=498 and year(dat)={0} and dat<='{1}'".format(aactual, dext)
for id, data, val, anys, mes in getAll(sql, nod):
    val = int(float(val))
    pns = RNSAConverter(val)
    anys = int(anys)
    if pns == 'NO':
        continue
    else:
        RNSA[(id, data, anys, mes)] = {'pns': pns}
nensa = {}

sql = "select id_cip_sec,val_data,val_usu,year(val_data),date_format(val_data,'%m') from nen12 where val_var like ('PEN%') and year(val_data)={0} and val_data<='{1}'".format(aactual, dext)
for id, data, usu, anys, mes in getAll(sql, imp):
    anys = int(anys)
    try:
        isInf = professionals[usu]['isInf']
    except KeyError:
        continue
    try:
        pns = RNSA[(id, data, anys, mes)]['pns']
    except KeyError:
        continue
    if (id, pns, anys, mes) in nensa:
        if nensa[(id, pns, anys, mes)]['valor'] > isInf:
            nensa[(id, pns, anys, mes)]['valor'] = isInf
    else:
        nensa[(id, pns, anys, mes)] = {'valor': isInf}


tablesa = "mst_seguimentnensa"
sql = 'drop table if exists {}'.format(tablesa)
execute(sql, db)
sql = "create table {} (id_cip_sec int,up varchar(5) not null default'',uba varchar(5) not null default'',anys varchar(4),mes varchar(2),revisio varchar(10) not null default'',n double)".format(tablesa)
execute(sql, db)
with open(OutFileSa, 'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (id, pns, anys, mes), n in nensa.items():
        valor = n['valor']
        try:
            upinf = ass[id]['upinf']
            ubainf = ass[id]['ubainf']
        except KeyError:
            continue
        w.writerow([id, upinf, ubainf, anys, mes, pns, valor])

loadData(OutFileSa, tablesa, db)

printTime('Export a khalix')

error = []

centres = "export.khx_centres"

query = "select 'INFGRUPAL',periode,ics_codi,'NUM','NOCAT','NOIMP','DIM6SET','N',sum(n) from {0}.{1} a inner join {2} b on a.up=scs_codi group by periode,ics_codi\
        union select 'INFNENSA',concat('A',right(anys,2),mes),ics_codi,'NUM','NOCAT','NOIMP','DIM6SET','N',sum(n) from {0}.{3} a inner join {2} b on a.up=scs_codi group by ics_codi,concat('A',right(anys,2),mes)\
        union select 'INFNENSA',concat('A',right(anys,2),mes),ics_codi,'DEN','NOCAT','NOIMP','DIM6SET','N',count(n) from {0}.{3} a inner join {2} b on a.up=scs_codi group by ics_codi,concat('A',right(anys,2),mes)".format(db, tablegrup, centres, tablesa)
file = "INF_ALTRES_EAP"
error.append(exportKhalix(query, file))

query = "select 'INFGRUPAL',periode,concat(ics_codi,'I',uba),'NUM','NOCAT','NOIMP','DIM6SET','N',sum(n) from {0}.{1} a inner join {2} b on a.up=scs_codi group by concat(ics_codi,'I',uba),periode\
        union select 'INFNENSA',concat('A',right(anys,2),mes),concat(ics_codi,'I',uba),'NUM','NOCAT','NOIMP','DIM6SET','N',sum(n) from {0}.{3} a inner join {2} b on a.up=scs_codi group by concat(ics_codi,'I',uba),concat('A',right(anys,2),mes)\
        union select 'INFNENSA',concat('A',right(anys,2),mes),concat(ics_codi,'I',uba),'DEN','NOCAT','NOIMP','DIM6SET','N',count(n) from {0}.{3} a inner join {2} b on a.up=scs_codi group by concat(ics_codi,'I',uba),concat('A',right(anys,2),mes)".format(db, tablegrup, centres, tablesa)
file = "INF_ALTRES_IND"
error.append(exportKhalix(query, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Fi khalix')
