# -*- coding: utf8 -*-

"""
Long amb visites 1 any

Com a info:
- CONT0001: MMCI
- CONT0002: UPC
- CONT0003: SECON
- CONT0004: COC
"""

import collections as c

import sisapUtils as u


db = "altres"
tbup = "exp_long_cont_up_A"
tbuba = "exp_long_cont_uba_A"

file = 'LONG_CONT_A'
file_uba = 'LONG_CONT_UBA_A'

ids = {'id_cip_sec': 'sisap',
       'id_cip': 'sidiap',
       }

categories = {'10999': 'MG', '10117': 'MG', '10116': 'MG',
              '30999': 'INF',
              '10888': 'PED'}

prof_dicc = {
            'MG': {
                    'edat': '>14',
                    'tipprof': 'TIPPROF1',
                    'up': 'up',
                    'uba': 'uba',
                    'taula': 'eqa_ind.mst_ubas',
                    'tipus': 'M',
                    'categories': ['10999', '10117', '10116']
                },
            'PED': {
                    'edat': '<15',
                    'tipprof': 'TIPPROF2',
                    'up': 'up',
                    'uba': 'uba',
                    'taula': 'pedia.mst_ubas',
                    'tipus': 'M',
                    'categories': ['10888', '10999', '10117', '10116']
                },
            'INF': {
                    'edat': '>14',
                    'tipprof': 'TIPPROF4',
                    'up': 'upinf',
                    'uba': 'ubainf',
                    'taula': 'eqa_ind.mst_ubas',
                    'tipus': 'I',
                    'categories': ['30999']
                },
            'ODN': {
                    'edat': '>=0',
                    'tipprof': 'TIPPROF3',
                    'up': 'up',
                    'uba': 'uba',
                    'taula': 'eqa_ind.mst_ubas',
                    'tipus': 'O',
                    'categories': ['10106', '10777']
                }
            }


u.printTime("inici")

dext, = u.getOne("select date_format(data_ext, '%Y-%m-%d') from dextraccio", 'nodrizas')
print dext


class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_centres()
        for self.prof in prof_dicc:
            self.get_usuaris()
            print self.prof
            self.get_pob()
            self.get_visites()
            self.get_taula()
            self.process_sequencia()
            self.get_calculs()
            if model == 'sisap':
                self.khalix_uba()
        if model == 'sisap':
            self.export_khalix()

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = {up: True for up, in u.getAll(sql, 'nodrizas')}

    def get_usuaris(self):
        """."""
        self.usuaris = {}
        sql = "select left(ide_dni, 8), ide_categ_prof_c \
                from cat_pritb992 where ide_categ_prof_c <> ''"
        for usu, categ in u.getAll(sql, "import"):
            if categ in prof_dicc[self.prof]['categories']:
                self.usuaris[(usu, self.prof)] = categ

    def get_pob(self):
        """."""
        u.printTime("Assignada")
        sql = "select {0}, up, uba, upinf, ubainf from assignada_tot where edat {1}".format(identificador, prof_dicc[self.prof]['edat'])
        self.pob = {id: (up, uba, upinf, ubainf) for (id, up, uba, upinf, ubainf) in u.getAll(sql, 'nodrizas')}

    def get_visites(self):
        """Agafem visites dels dos últims anys"""
        u.printTime("Visites")
        self.visitespacients = c.Counter()
        self.data = c.defaultdict(lambda: c.defaultdict(list))
        if self.prof in ['MG', 'PED']:
            sql = "select {}, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei \
                    from visites1 \
                    where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>'' and visi_col_prov_resp like ('1%')".format(identificador)
        elif self.prof in ['INF']:
            sql = "select {}, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei \
                from visites1 \
                where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>'' and visi_col_prov_resp like ('3%') \
                and  s_espe_codi_especialitat not in ('EXTRA', '10102') and visi_tipus_visita not in ('EXTRA', 'EXT')".format(identificador)
        elif self.prof in ['ODN']:
            sql = "select {}, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei \
                from visites1 \
                where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>''".format(identificador)
        for id, _sector, up, col, dat, _categoria, _serveis in u.getAll(sql, "import"):
            if up in self.centres or self.prof == 'ODN':
                if (col, self.prof) in self.usuaris:
                    servei = self.prof
                    self.visitespacients[(id, servei, col)] += 1
                    identify = str(id) + '@' + servei
                    self.data[identify][dat].append(col)

    def get_taula(self):
        """Calculem les tres dades necessàries per calcular els indicadors. A nivell de pacient necessitem:
            1) Nombre de professionals diferents
            2) Nombre de visites totals
            3) Nombre visites professional majoritari
            4) El sumatori dels quadrats de les visites de cada professional (per a calcular COC)
        """
        u.printTime("Càlculs")
        self.pacients = {}
        for (id, servei, dummy_col), rec in self.visitespacients.items():
            Rq2 = rec * rec
            if (id, servei) in self.pacients:
                self.pacients[(id, servei)]['totals'] += rec
                self.pacients[(id, servei)]['nprof'] += 1
                self.pacients[(id, servei)]['rq2'] += Rq2
                majoritari = self.pacients[(id, servei)]['vprof']
                if rec > majoritari:
                    self.pacients[(id, servei)]['vprof'] = rec
            else:
                self.pacients[(id, servei)] = {'totals': rec, 'nprof': 1, 'vprof': rec, 'rq2': Rq2}

    def process_sequencia(self):
        """Ordenem les visites per ordre temporal per poder calcular el de sequencia"""
        u.printTime("Seqüència")
        resultat = []
        for (id), dates in self.data.items():
            pac = ((id, dat, cols) for dat, cols in dates.items())
            colegiat = {}
            for i, (id, dat, cols) in enumerate(sorted(pac, key=lambda x: x[1]), start=1):
                for col in cols:
                    if col == colegiat:
                        seq = 1
                    else:
                        seq = 0
                        colegiat = col
                    identities = id.split('@')
                    resultat.append((int(identities[0]), identities[1], dat, col, i, seq))
        Stb = "Long_sequencia_A"
        Scolumns = ["{} int".format(identificador), "servei varchar(10)", "data date", "colegiat varchar(10)", "ordre int", "sequencia int"]
        u.createTable(Stb, "({})".format(", ".join(Scolumns)), db, rm=True)
        u.listToTable(resultat, Stb, db)

        self.indicador_sequencia = c.Counter()
        sql = "select {}, servei, sequencia from {}".format(identificador, Stb)
        for id, servei, seq in u.getAll(sql, db):
            self.indicador_sequencia[id, servei, 'num'] += seq
            self.indicador_sequencia[id, servei, 'den'] += 1

    def get_calculs(self):
        """Calculem els indicadors a nivell de pacient
        Aqui filtrem per població assignada i pacients entre 3 i 300 visites"""
        u.printTime("Més càlculs")
        upload = []
        for (id, servei), dad in self.pacients.items():
            totals, nprof, vprof, rq2 = dad['totals'], dad['nprof'], dad['vprof'], dad['rq2']
            if 3 <= totals <= 300:
                n1 = 1 - (nprof/(totals + 0.1))
                d1 = 1 - (1/(totals + 0.1))
                n3 = self.indicador_sequencia[id, servei, 'num']
                d3 = self.indicador_sequencia[id, servei, 'den']
                r1 = float(n1/d1)
                r2 = float(vprof) / float(totals)
                d3 = d3 - 1
                r3 = float(n3)/float(d3)
                n4 = rq2 - totals
                d4 = totals * (totals - 1)
                r4 = float(n4) / float(d4)
                if model == 'sidiap':
                    upload.append([id, servei, 'nprof', dext, nprof])
                    upload.append([id, servei, 'CONT0001', dext, r1])
                    upload.append([id, servei, 'CONT0001n', dext, n1])
                    upload.append([id, servei, 'CONT0001d', dext, d1])
                    upload.append([id, servei, 'CONT0002', dext, r2])
                    upload.append([id, servei, 'CONT0002n', dext, vprof])
                    upload.append([id, servei, 'CONT0002d', dext, totals])
                    upload.append([id, servei, 'CONT0003', dext, r3])
                    upload.append([id, servei, 'CONT0003n', dext, n3])
                    upload.append([id, servei, 'CONT0003d', dext, d3])
                    upload.append([id, servei, 'CONT0004', dext, r4])
                    upload.append([id, servei, 'CONT0004n', dext, n4])
                    upload.append([id, servei, 'CONT0004d', dext, d4])
                else:
                    if id in self.pob:
                        up, uba, upinf, ubainf = self.pob[id][0], self.pob[id][1], self.pob[id][2], self.pob[id][3]
                        upload.append([id, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1, n3, r1, r2, r3, r4])

        u.listToTable(upload, tb, db)
        u.listToTable(upload, tb, "permanent")

    def khalix_uba(self):
        """."""
        u.printTime("Khalix UBA")
        upload= []
        sql = "select a.{0}, a.{1}, servei, sum(r1), sum(r2), sum(r3), sum(r4), count(*) from {2} a inner join {3} b on a.{0}=b.up and a.{1}=b.uba where tipus='{4}' and servei='{5}' group by a.{0}, a.{1}, servei".format(prof_dicc[self.prof]['up'], prof_dicc[self.prof]['uba'], tb, prof_dicc[self.prof]['taula'], prof_dicc[self.prof]['tipus'], self.prof)
        for up, uba, _servei, r1, r2, r3, r4, den in u.getAll(sql, db):
            br = centres[up] if up in centres else False
            if br:
                if self.prof != 'ODN':
                    detalle = prof_dicc[self.prof]['tipprof']
                    entity = br + prof_dicc[self.prof]['tipus'] + uba
                    upload.append(['CONT0001A', periodo, entity, "NUM", "NOCAT", detalle, "DIM6SET", "N", r1])
                    upload.append(['CONT0001A', periodo, entity, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                    upload.append(['CONT0002A', periodo, entity, "NUM", "NOCAT", detalle, "DIM6SET", "N", r2])
                    upload.append(['CONT0002A', periodo, entity, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                    upload.append(['CONT0003A', periodo, entity, "NUM", "NOCAT", detalle, "DIM6SET", "N", r3])
                    upload.append(['CONT0003A', periodo, entity, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                    upload.append(['CONT0004A', periodo, entity, "NUM", "NOCAT", detalle, "DIM6SET", "N", r4])
                    upload.append(['CONT0004A', periodo, entity, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])


        u.listToTable(upload, tbuba, db)

    def export_khalix(self):
        """Exportar a khalix els indicadors"""
        u.printTime("Khalix UP")
        export = []
        sql = "select up, servei, sum(r1), sum(r2), sum(r3), sum(r4), count(*) from {} group by up, servei".format(tb)
        for up, servei, r1, r2, r3, r4, den in u.getAll(sql, db):
            br = centres[up] if up in centres else False
            if br:
                detalle = prof_dicc[servei]['tipprof']
                export.append(['CONT0001A', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r1])
                export.append(['CONT0001A', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0002A', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r2])
                export.append(['CONT0002A', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0003A', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r3])
                export.append(['CONT0003A', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])
                export.append(['CONT0004A', periodo, br, "NUM", "NOCAT", detalle, "DIM6SET", "N", r4])
                export.append(['CONT0004A', periodo, br, "DEN", "NOCAT", detalle, "DIM6SET", "N", den])

        u.listToTable(export, tbup, db)
        sql = "select * from {}.{}".format(db, tbup)
        u.exportKhalix(sql, file)

        sql = "select * from {}.{}".format(db, tbuba)
        u.exportKhalix(sql, file_uba)


if __name__ == '__main__' and u.IS_MENSUAL:
    for identificador in ids:
        do = False
        model = ids[identificador]
        if model == 'sisap':
            do = True
            print model
            tb = "mst_long_cont_pacient_A"
            columns = ["{} int".format(identificador), "up varchar(5)", "uba varchar(5)",  "upinf varchar(5)", "ubainf varchar(5)", "servei varchar(10)", "nprof int", "vprof int", "totals int", "n1 double", "d1 double", "n3 double", "r1 double", "r2 double", "r3 double", "r4 double"]

            columnsU = ["indicador varchar(10)", "periode varchar(10)", "up varchar(5)", "analisi varchar(5)", "nc varchar(10)", "detalle varchar(10)", "d1 varchar(10)", "n varchar(5)", "resultat double"]
            u.createTable(tbup, "({})".format(", ".join(columnsU)), db, rm=True)

            columnsU = ["indicador varchar(10)", "periode varchar(10)", "entity varchar(20)", "analisi varchar(5)", "nc varchar(10)", "detalle varchar(10)", "d1 varchar(10)", "n varchar(5)", "resultat double"]
            u.createTable(tbuba, "({})".format(", ".join(columnsU)), db, rm=True)
            
            sql = "select scs_codi, ics_codi from khx_centres"
            centres = {up: br for up, br in u.getAll(sql, 'export')}
            sql = "select right(year(data_ext),2),date_format(data_ext,'%m') from dextraccio"
            for anys, mes in u.getAll(sql, "nodrizas"):
                periodo = "A" + str(anys) + str(mes)
        if do:
            u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
            u.createTable(tb, "({})".format(", ".join(columns)), "permanent", rm=True)
            Continuitat()

    u.printTime("Fi")
