from sisapUtils import *
import time
from collections import defaultdict,Counter
from datetime import *

file_name="AG_ASSIR"

codis=["LONGASS001","AGASSEMAIL","AGASSDEP","LONGASS002"] 
 
taulas= ["exp_khalix_{}_up".format(codi) for codi in codis]       

query_template_string="select indicador,concat('A','periodo'),br_assir,{strg},'NOCAT','NOIMP','DIM6SET','N', sum({var}) from {db}.{taula} a inner join nodrizas.ass_centres b on a.up=b.up where a.{var} != 0 group by br_assir"
query_string=" union ".join(query_template_string.format(strg="'NUM'",var="numerador",taula=taula_name_kh,db="altres")+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=taula_name_kh,db="altres") for taula_name_kh in taulas)
print(query_string)

exportKhalix(query_string,file_name)
