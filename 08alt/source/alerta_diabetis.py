# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


nod = "nodrizas"
db = "altres"


class Diabetis(object):
    """."""

    def __init__(self):
        """."""
        self.get_denominadors()
        self.get_numeradors()
        self.get_poblacio()
        self.get_exclusions()
        self.get_farm0008()
        self.get_master()
        self.get_hash()
        self.set_mst()
        self.set_pacients()
        self.set_resultats()
        self.set_catalegs()

    def get_denominadors(self):
        """."""
        codis = {805: "FARM0007A", 806: "FARM0007B", 807: "FARM0007C",
                 808: "FARM0007D", 809: "FARM0007E", 810: "FARM0009"}
        self.denominadors = c.defaultdict(set)
        sql = "select id_cip_sec, farmac from eqa_tractaments_ep \
               where farmac in {}".format(tuple(codis))
        for id, codi in u.getAll(sql, nod):
            indicador = codis[codi]
            self.denominadors[indicador].add(id)

    def get_numeradors(self):
        """."""
        self.numeradors = c.defaultdict(set)
        sql = "select id_cip_sec, valor \
               from eqa_variables, dextraccio \
               where agrupador = 30 and \
                     data_var between adddate(data_ext, interval -1 year) and \
                                      data_ext and \
                     usar = 1"
        for id, valor in u.getAll(sql, nod):
            indicadors = set(["FARM0009"])
            if valor < 60:
                indicadors.add("FARM0007A")
            if valor < 45:
                indicadors.add("FARM0007B")
            if valor < 30:
                indicadors.add("FARM0007C")
            if valor < 25:
                indicadors.add("FARM0007D")
            if valor < 15:
                indicadors.add("FARM0007E")
            for indicador in indicadors:
                if id in self.denominadors[indicador]:
                    self.numeradors[indicador[:8]].add(id)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat from assignada_tot \
               where edat > 14"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, nod)}

    def get_exclusions(self):
        """."""
        self.exclusions = c.defaultdict(set)
        codis = {665: ("FARM0007", "FARM0009"), 39439: ("xxx",)}
        sql = "select id_cip_sec, ps from eqa_problemes \
               where ps in {}".format(tuple(codis))
        for id, codi in u.getAll(sql, nod):
            for ind in codis[codi]:
                self.exclusions[ind].add(id)

    def get_farm0008(self):
        """."""
        ind = "FARM0008"
        fets = set()
        sql = "select id_cip_sec, farmac from eqa_tractaments_ep \
               where farmac = 22"
        for id, codi in u.getAll(sql, "nodrizas"):
            if id not in fets:
                fets.add(id)
            else:
                self.denominadors[ind].add(id)
        sql = "select id_cip_sec \
               from eqa_variables, dextraccio \
               where agrupador = 20 and \
                     data_var between adddate(data_ext, interval -1 year) and \
                                      data_ext and \
                     valor between 3 and 6.99 and \
                     usar = 1"
        self.numeradors[ind] = set([id for id, in u.getAll(sql, "nodrizas")])

    def get_master(self):
        """."""
        edats = {"FARM0007": 14, "FARM0008": 80, "FARM0009": 14}
        self.master = []
        fets = c.defaultdict(set)
        for codi, pacients in self.denominadors.items():
            ind = codi[:8]
            for id in pacients:
                if id not in fets[ind] and id in self.poblacio and id not in self.exclusions[ind]:
                    up, uba, ubainf, edat = self.poblacio[id]
                    if edat > edats[ind]:
                        if ind == "FARM0009":
                            alert = id not in self.numeradors[ind]
                        else:
                            alert = id in self.numeradors[ind]
                        self.master.append((id, up, uba, ubainf, ind, alert))
                    fets[ind].add(id)
        self.indicadors = set([row[4] for row in self.master])

    def get_hash(self):
        """."""
        pacients = set([row[0] for row in self.master])
        sql = "select id_cip_sec, hash_d, codi_sector from eqa_u11"
        self.hash = {id: (hash, sector) for (id, hash, sector)
                     in u.getAll(sql, nod) if id in pacients}

    def delete_rows(self, tb, column="indicador"):
        """."""
        sql = "delete from {} where {} in {}".format(tb, column,
                                                     tuple(self.indicadors))
        u.execute(sql, db)

    def set_mst(self):
        """."""
        tb = 'mst_alertes_pacient'
        self.delete_rows(tb, 'indicador')
        upload = [(id, codi, 1 * alerta)
                  for (id, up, uba, ubainf, codi, alerta) in self.master]
        u.listToTable(upload, tb, db)

    def set_pacients(self):
        """."""
        tb = "exp_ecap_alertes_pacient"
        self.delete_rows(tb, "grup_codi")
        upload = [(id, up, uba, up, ubainf, codi, 0) + self.hash[id]
                  for (id, up, uba, ubainf, codi, alerta)
                  in self.master
                  if alerta]
        u.listToTable(upload, tb, db)

    def set_resultats(self):
        """."""
        tb = "exp_ecap_alertes_uba"
        self.delete_rows(tb)
        denominador, numerador = c.Counter(), c.Counter()
        for id, up, uba, ubainf, codi, alerta in self.master:
            denominador[(up, uba, "M", codi)] += 1
            numerador[(up, uba, "M", codi)] += alerta
            denominador[(up, ubainf, "I", codi)] += 1
            numerador[(up, ubainf, "I", codi)] += alerta
        upload = [(up, uba, tipus, codi,
                   numerador[(up, uba, tipus, codi)], n,
                   numerador[(up, uba, tipus, codi)] / float(n))
                  for (up, uba, tipus, codi), n in denominador.items()]
        u.listToTable(upload, tb, db)

    def set_catalegs(self):
        """."""
        tb = "exp_ecap_alertes_cataleg"
        self.delete_rows(tb)
        pare = "FARM03"
        upload = [("FARM0007", "Prescripci� inadequada antidiab�tics per funci� renal",
                   1, pare, 1, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/FARM0007",
                   "PCT"),
                  ("FARM0008", "Control metab�lic excessiu DM2 en poblaci� envellida",
                   2, pare, 1, 0, 0, 0, 0, 0,
                   "http://sisap-umi.eines.portalics/indicador/codi/FARM0008",
                   "PCT"),
                  ("FARM0009", "Prescripci� antidiab�tics sense funci� renal el darrer any",
                   3, pare, 1, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/FARM0009",
                   "PCT")]
        u.listToTable(upload, tb, db)
        tb = "exp_ecap_alertes_catalegpare"
        u.execute("delete from {} where pare = '{}'".format(tb, pare), db)
        upload = [(pare, "Alertes de seguretat: Diabetis", 2, "ALERTES")]
        u.listToTable(upload, tb, db)


if __name__ == "__main__":
    Diabetis()
