from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random

nod="nodrizas"
imp="import"
validation=False
limit=""
codi_indicador="AGASSEMAIL"
alt="altres"
exclusions=("VU","U","E", "ES","TN","GR", "GM")
ups=('01422',)


def get_date_dextraccio():
    """ Consigue la fecha de hace 12 meses y la fecha actual.
        Devuelve a tuple de dos fechas en in datetime.date format
    """
    sql="select date_add(date_add(data_ext, interval - 12 month), interval + 1 day), data_ext from nodrizas.dextraccio;"
    for past, current in getAll(sql,nod):
        return past,current	

def get_llevadores():
    """Selecciona el dni de aquellos profesionales que son llevadores.
       Devuelve un set de estos dnis.
    """
    sql = "select IDE_NUMCOL from {0}.cat_professionals_assir where TIPUS='L';".format(imp)
    return {id for (id,) in getAll(sql,imp)}
    

def get_dones_ates_ultim_any():
    sql="select id_cip_sec from {}.ass_poblacio".format(nod)
    return {id for id, in getAll(sql,nod)}

def get_id_emails(params):
    """
    """
    table,current_date=params
    printTime(table)
    sql = "select id_cip_sec,ceu_data from {} where ceu_tipus_desti= 'E'".format(table)
    return {id for id,data in getAll(sql, imp) if monthsBetween(data,current_date) < 12 }

def get_id_email_total(current_date):
    params=[(table,current_date) for table in getSubTables("enviaments")]
    return set.union(*[id_email for id_email in multiprocess(get_id_emails, params)])

def get_vvirtuals(params):
    current_date,llevadores=params
    sql="SELECT VVI_NUM_COL,id_cip_sec,vvi_data_alta FROM vvirtuals WHERE vvi_situacio='R'"
    cip_visites=set()
    for col,cip,data in getAll(sql,imp):
        if monthsBetween(data,current_date) < 12 and col in llevadores:
            cip_visites.add(cip)
    return cip_visites


def get_indicador (dones_ates,cip_visites,id_emails,val=False):
    """
    """
    if val:
        counter_1=0
        counter_2=0
        
    cumplidores_ids=defaultdict(set)
    no_cumplidores_ids=defaultdict(set)
    uba_rows=[]
    indicador=defaultdict(Counter)
    sql = "select id_cip_sec, visi_up from {}.ass_imputacio_up".format(nod)
    for id, up in getAll(sql, nod):
        if id in dones_ates:
            indicador[up]["DEN"]+=1
            if id in cip_visites or id in id_emails:
                indicador[up]["NUM"]+=1
                if val and counter_1 < 10 and up in ups:
                    cumplidores_ids[up].add(id)
                    counter_1+=1
            else:
                if val and counter_2 < 10 and up in ups:
                    no_cumplidores_ids[up].add(id)
                    counter_2+=1
                
    uba_rows = [(up,  codi_indicador,indicador[up]["NUM"], indicador[up]["DEN"])
                          for up in indicador]
    
    return uba_rows,no_cumplidores_ids,cumplidores_ids
       
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)


def get_up_desc(up):
    sql="select up_desc from ass_centres where up={};".format(up)
    return getOne(sql,"nodrizas")[0]

def get_hashd_dict():
    sql="select id_cip_sec,hash_d,codi_sector from import.u11;"
    return {id:hashd for (id,hashd,sector) in getAll(sql,"import")}	

def get_cip_dict_all():
    sql="select usua_cip,usua_cip_cod from pdptb101"
    return {hashd:cip for cip,hashd in getAll(sql,"pdp")}

def get_cip_dict(hashd):
    sql="select usua_cip from pdptb101 where usua_cip_cod='{}'".format(hashd)
    return getOne(sql,"pdp")[0]


def write(filename,dict_ids,hash_dict):
    f=open(filename,"w")
    for up in dict_ids:
        up_desc=get_up_desc(up)
        for id in dict_ids[up]:
            print(hash_dict[id])
            cip=get_cip_dict(hash_dict[id])
            printTime("cip")
            f.write("{}\t{}\n".format(up_desc,cip))
    f.close()

if __name__ == '__main__':

    printTime('inici')
    
    past_date,current_date=get_date_dextraccio()
    printTime("current date {} past_date {}".format(current_date,past_date))

    llevadores=get_llevadores()
    printTime('Llevadores')

    visites_total=get_vvirtuals((current_date,llevadores))    

    
    dones_ates=get_dones_ates_ultim_any()
    printTime("Dones ates {}".format(len(dones_ates)))
    id_emails_total=get_id_email_total(current_date)
    printTime("id_emails_total {}".format(len(id_emails_total)))

    uba_rows,no_cumplidores_ids,cumplidores_ids=get_indicador(dones_ates,visites_total,id_emails_total,validation)
    printTime('Get Rows')
	#exportar las tablas a la base de datos altres
    columns="(up varchar(5),indicador varchar(11),numerador int, denominador int)"
    export_table("exp_khalix_{}_up".format(codi_indicador),
				columns,
				alt,
				uba_rows)
    
    
    if validation:
        hash_id_dict=get_hashd_dict()
        printTime("hash")
        for filename,dict_ids in zip(["LONGASS001_{}.txt".format(cump) for cump in ["no_cumplidores","cumplidores"]], [no_cumplidores_ids,cumplidores_ids]):
            write(filename,dict_ids,hash_id_dict)
