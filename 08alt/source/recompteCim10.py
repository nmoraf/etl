# coding: iso-8859-1
from sisapUtils import *
import sys,datetime,os
from time import strftime
from collections import defaultdict,Counter

imp = "import"
nod = "nodrizas"

sql="select data_ext,date_format(data_ext,'%Y%m') from dextraccio"
for d1,d in getAll(sql, nod):
    dcalcul=d
    data_ext=d1
    
OutFile = tempFolder + 'RecompteCim10_' + dcalcul + '.txt'
OutFileE = tempFolder + 'codis_ps_no_cataleg_' + dcalcul + '.txt'

printTime('Inici Recompte')

descCim10 = {}
sql = "select ps_cod,ps_des from cat_prstb001"
for ps,desc in getAll(sql,imp):
    descCim10[ps] = {'desc':desc}
    
recompteCim10 = Counter()
recompteErrors = Counter()
enviar = False
sql= "select pr_cod_ps from problemes, nodrizas.dextraccio where (pr_dde between date_add(data_ext,interval -1 year) and data_ext) and\
    pr_cod_o_ps='C' and pr_hist=1 and (pr_data_baixa = 0 or pr_data_baixa>data_ext)"
for ps, in getAll(sql,imp):
    calculs = False
    try:
        desc = descCim10[ps]['desc']
        calculs = True
    except KeyError:
        recompteErrors[ps] += 1
        enviar = True
    if calculs:
        recompteCim10[ps,desc] += 1
    
with openCSV(OutFileE) as c:
    for (ps),count in recompteErrors.items():
        c.writerow([ps, count])
        
with openCSV(OutFile) as c:
    for (ps,desc),count in recompteCim10.items():
        c.writerow([ps,desc,count])

if IS_MENSUAL:
    text= "Us fem arribar el fitxer d'aquest mes amb el recompte de codis cim10 dels darrers 12 mesos"
    sendSISAP(['miglesiasrodal@gencat.cat','colmos@gencat.cat'],'Recomptes cim10','Manolo i Carmen',text,OutFile)
    
    if enviar:
        text= "Us fem arribar el fitxer amb els codis de problemes de salut que no creuen amb el catàleg"
        sendSISAP(['mfabregase@gencat.cat','lmendezboo@gencat.cat'],'codis problemes que no creuen','Mireia i Leo',text,OutFileE)

try:
    os.remove(OutFile)
    os.remove(OutFileE)
except:
    pass 

printTime('Fi Recompte')