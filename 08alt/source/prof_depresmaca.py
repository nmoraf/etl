import sisapUtils as u
import datetime
import time
from collections import defaultdict,Counter
start_time = time.time()

valida = True

profvalida = {1 : {'tip' : 'M', 'up' : '00108', 'uba' : 'UAB-V'},
                2: {'tip' : 'M', 'up' : '00441', 'uba' : '4T'}}

class Depresmaca(object):

    def __init__(self):
        self.get_poblacio()
        self.get_estat()
        self.get_prev()
        self.get_osteo()
        self.datos_paciente()
        self.datos_depres()
        self.dadesup()
        self.dadesupinf()
        self.datos_unidad()
        self.dadesupk()
        self.datos_unidadk()
        
    def get_poblacio(self):
        self.poblacio = {}
        sql = ("select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, institucionalitzat from assignada_tot where maca = 1 and edat >= 40", "nodrizas")
        self.poblacio = {id : {'id_cip_sec' : id, 'up' : up, 'uba' : uba, 'upinf' : upinf,
                            'ubainf' : ubainf, 'edat': edat, 'sexe': sexe, 'ates': ates, 'insti': insti, 'hash' : None, 'sector' : None, 'indest' : 0,  
                            'prev1' : 0, 'prev2' : 0, 'indost' : 0, 'cdv' : 0} for id, up, uba, upinf, ubainf, edat, sexe, ates, insti in u.getAll(*sql)}
        print len(self.poblacio)
                       
        sql = ("select id_cip_sec, codi_sector, hash_d from u11", "import")
        for id, sector, hash in u.getAll(*sql):
            if id in self.poblacio:
                self.poblacio[id]['hash'] = hash
                self.poblacio[id]['sector'] = sector

    def get_estat(self):
        num = 0
        sql = ("select id_cip_sec from eqa_tractaments where farmac = 82", "nodrizas")
        for id, in u.getAll(*sql):
            if id in self.poblacio:
                self.poblacio[id]['indest'] = 1
                num += 1
        print "estat"
        print num
        
    def get_prev(self):
        sql = ("select id_cip_sec from eqa_problemes where ps = 1 or ps = 7 or ps = 11", "nodrizas")
        for id, in u.getAll(*sql):
            if id in self.poblacio:
                self.poblacio[id]['cdv'] = 1
                if self.poblacio[id]['indest'] == 1:
                    self.poblacio[id]['prev2'] = 1
        
        for id, v in self.poblacio.iteritems():
            if v['indest'] == 1 and v['cdv'] == 0:
                v['prev1'] = 1

    def get_osteo(self):
        num = 0
        sql = ("select id_cip_sec from eqa_tractaments where farmac = 80 or farmac = 719", "nodrizas")
        for id, in u.getAll(*sql):
            if id in self.poblacio:
                self.poblacio[id]['indost'] = 1
                num += 1
        print "osteo"
        print num
        
    def datos_paciente(self):
        datosp = []
        for id, d in self.poblacio.iteritems():
            datosp.append((d['id_cip_sec'], d['up'], d['uba'], d['upinf'], d['ubainf'],  d['edat'],  d['sexe'],  d['ates'], d['insti'],d['indest'], d['prev1'],
                            d['prev2'], d['indost'], d['cdv'], d['hash'], d['sector']))
        tablep = "mst_prof_maca"
        u.createTable(tablep, "(id varchar(40), up varchar(6), uba varchar(5), upinf varchar(6), ubainf varchar(6), edat int, sexe varchar(1), ates int, insti int,\
                                indest int, prev1 int, prev2 int, indost int, cdv int, hash varchar(40), sector varchar(8))", "altres", rm=True)
        u.listToTable(datosp, tablep, "altres")

    def datos_depres(self):
        datosd = []
        for id, d in self.poblacio.iteritems():
            if d['indest'] == 1 and d['prev1'] == 1:                
                datosd.append((d['id_cip_sec'], d['up'], d['uba'], d['upinf'], d['ubainf'], 'IAD0008A',0, d['hash'], d['sector']))            
            if d['indest'] == 1 and d['prev2'] == 1:
                datosd.append((d['id_cip_sec'], d['up'], d['uba'], d['upinf'], d['ubainf'], 'IAD0008B',0, d['hash'], d['sector']))          
            if d['indost'] == 1:
                datosd.append((d['id_cip_sec'], d['up'], d['uba'], d['upinf'], d['ubainf'], 'IAD0009',0, d['hash'], d['sector']))

        tabled = "exp_ecap_maca_pacient"
        u.createTable(tabled, "(ID int, UP varchar(6), UBA varchar(6), UPINF varchar(6), UBAINF varchar(6), INDICADOR  varchar(9), \
            exclos int, hash varchar(40), SECTOR varchar(8))", "altres", rm=True)
        u.listToTable(datosd, tabled, "altres")
      
    def dadesup(self):
        tipus = 'M'
        self.dades = []
        sql = ("select up, uba, sum(indest), count(*), sum(indest)/count(*) from mst_prof_maca group by up, uba", "altres")
        for up, uba, num, den, perc in u.getAll(*sql):
            self.dades.append((up, uba, tipus, 'IAD0008', num, den, perc))
        print len(self.dades)

        sql = ("select up, uba, sum(prev1), count(*), sum(prev1)/count(*) from mst_prof_maca where cdv = 0 group by up, uba", "altres")
        for up, uba, num, den, perc in u.getAll(*sql):
            self.dades.append((up, uba, tipus, 'IAD0008A', num, den, perc))
        print len(self.dades)

        sql = ("select up, uba, sum(prev2), count(*), sum(prev2)/count(*) from mst_prof_maca where cdv = 1 group by up, uba", "altres")
        for up, uba, num, den, perc in u.getAll(*sql):
            self.dades.append((up, uba, tipus, 'IAD0008B', num, den, perc))
        print len(self.dades)

        sql = ("select up, uba, sum(indost), count(*), sum(indost)/count(*) from mst_prof_maca group by up, uba", "altres")
        for up, uba, num, den, perc in u.getAll(*sql):
            self.dades.append((up, uba, tipus, 'IAD0009', num, den, perc))
        print len(self.dades)

    def dadesupinf(self):
        tipus = 'I'
        sql = ("select upinf, ubainf, sum(indest), count(*), sum(indest)/count(*) from mst_prof_maca group by upinf, ubainf", "altres")
        for upinf, ubainf, num, den, perc in u.getAll(*sql):
            self.dades.append((upinf, ubainf, tipus, 'IAD0008', num, den, perc))
        print len(self.dades)

        sql = ("select upinf, ubainf, sum(prev1), count(*), sum(prev1)/count(*) from mst_prof_maca where cdv = 0 group by upinf, ubainf", "altres")
        for upinf, ubainf, num, den, perc in u.getAll(*sql):
            self.dades.append((upinf, ubainf, tipus, 'IAD0008A', num, den, perc))
        print len(self.dades)
    
        sql = ("select upinf, ubainf, sum(prev2), count(*), sum(prev2)/count(*) from mst_prof_maca where cdv = 1 group by up, uba", "altres")
        for upinf, ubainf, num, den, perc in u.getAll(*sql):
            self.dades.append((upinf, ubainf, tipus, 'IAD0008B', num, den, perc))
        print len(self.dades)

        sql = ("select upinf, ubainf, sum(indost), count(*), sum(indost)/count(*) from mst_prof_maca group by upinf, ubainf", "altres")
        for upinf, ubainf, num, den, perc in u.getAll(*sql):
            self.dades.append((upinf, ubainf, tipus, 'IAD0009', num, den, perc))
        print len(self.dades)

    def datos_unidad(self):
        tableu = 'exp_ecap_maca_uba'
        u.createTable(tableu, "(UP varchar(6), UBA varchar(5), TIPUS varchar(1), INDICADOR varchar(9), NUMERADOR int, DENOMINADOR int, RESULTAT float)",
            'altres', rm=True)
        u.listToTable(self.dades, tableu, 'altres')
    
    def dadesupk(self):
        self.dadesk = Counter()
        sql = ("select up, edat, sexe, ates, insti, indest, prev1, prev2,indost, cdv  from mst_prof_maca", "altres")
        for up, edat, sexe, ates, insti, indest, prev1, prev2, indost,  cdv in u.getAll(*sql):
            comb = ('NOINS' if insti == 0 else 'INS')+ 'ASS'
            self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008', 'DEN')] += 1
            self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008', 'NUM')] += indest
            self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0009', 'DEN')] += 1
            self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0009', 'NUM')] += indost
            if cdv == 0:
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008A', 'DEN')] += 1
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008A', 'NUM')] += prev1
            else:
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008B', 'DEN')] += 1
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008B', 'NUM')] += prev2
            if ates == 1:
                comb = ('NOINS' if insti == 0 else 'INS')+ 'AT'
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008', 'DEN')] += 1
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008', 'NUM')] += indest
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0009', 'DEN')] += 1
                self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0009', 'NUM')] += indost
                if cdv == 0:
                    self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008A', 'DEN')] += 1
                    self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008A', 'NUM')] += prev1
                else:
                    self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008B', 'DEN')] += 1
                    self.dadesk[(up, u.ageConverter(edat, 5),  u.sexConverter(sexe), comb, 'IAD0008B', 'NUM')] += prev2
        
    def datos_unidadk(self):
        tableu = 'exp_khalix_maca_up'
        self.upload = []
        for (up, edat, sexe, comb, indicador, tipus), rec in self.dadesk.items():
            self.upload.append((up, edat, sexe, indicador, comb, tipus,  rec))
        u.createTable(tableu, "(UP varchar(6), edat varchar(10), sexe varchar(10), INDICADOR varchar(9),comb varchar(10), conc varchar(5), recomptes int)",
            'altres', rm=True)
        u.listToTable(self.upload, tableu, 'altres')
if __name__ == '__main__':
    Depresmaca()

#export a khalix
error= []
taulaExport = 'altres.exp_khalix_maca_up'
centres="export.khx_centres"
query = "select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',recomptes from {0} a inner join {1} b on a.up=scs_codi  where indicador in ('IAD0008A','IAD0008B','IAD0009')".format(taulaExport, centres)    
file="IAD_MACA"
error.append(u.exportKhalix(query,file))  
  
#validar
  
if valida:
    uploadvalp = []
    uploadval = []
    for prof in profvalida:
        up = profvalida[prof]['up']
        uba = profvalida[prof]['uba']
        tip = profvalida[prof]['tip']
        sql = "select id, up, indest, prev1, prev2, indost, hash, sector from mst_prof_maca where up = '{0}'".format(up)
        for id,up,indest,prev1,prev2,indost,hash,sector in u.getAll(sql, 'altres'):
            if indest == 0:
                uploadvalp.append((id, up, uba,'','', 'IAD0008A', 0, hash, sector))
                uploadvalp.append((id, up, uba,'','', 'IAD0008B', 0, hash, sector))
            if indest == 1 and prev1 == 1:
                uploadvalp.append((id, up, uba,'','', 'IAD0008A', 9, hash, sector))
            if indest == 1 and prev2 == 1:
                uploadvalp.append((id, up, uba,'','', 'IAD0008B', 9, hash, sector))
            if indost == 0:
                uploadvalp.append((id, up, uba,'','', 'IAD0009', 0, hash, sector))
            if indost == 1:
                uploadvalp.append((id, up, uba,'','', 'IAD0009', 9, hash, sector))
        sql = "select up, tipus, indicador, sum(numerador), sum(denominador), sum(numerador)/sum(denominador) from exp_ecap_maca_uba where up='{0}' and tipus='{1}' group by indicador".format(up, tip)
        for up, tipus, indicador, num, den, res in u.getAll(sql, 'altres'):
            uploadval.append([ up, uba, tipus, indicador, num, den, res])
    tablep = "exp_ecap_maca_pacient"
    u.createTable(tablep, "(id int, up varchar(5),uba varchar(5), upinf varchar(5),ubainf varchar(5),indicador varchar(10), \
                            exclos int, hash varchar(40), SECTOR varchar(8))", 'altres', rm=True)
    u.listToTable(uploadvalp, tablep, 'altres')
    table = "exp_ecap_maca_uba"
    u.createTable(table, "(up varchar(5),uba varchar(5),tipus varchar(1), indicador varchar(10), numerador int, denominador int, resultat float)", 'altres', rm=True)
    u.listToTable(uploadval, table, 'altres')
    
print "--- %s seconds ---" % (time.time() - start_time)
