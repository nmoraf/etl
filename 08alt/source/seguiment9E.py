# coding: utf8

"""
Procés per posar les visites 9E a una taula de redics.
Cada cop omplim tota la taula
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

inici = '2018-01-01'

tb = "SISAP_VISITES_9E"
db = "redics"

class visita9E(object):
    """Classe principal a instanciar."""
    
    def __init__(self):
        """Execució seqüencial."""
        
        self.create_tb()
        self.get_visites()
        self.upload_data()
        
    def create_tb(self):
        """Creació i neteja de la taula de visites (la creem cada cop de nou)"""
        
        u.createTable(tb, "(data date, sector varchar(4), up varchar(5), centre varchar(10), classe varchar(2), servei varchar(10), modul varchar(10),\
                                visita_feta int, etiqueta varchar(10))", db, rm=True)
        u.grantSelect(tb, ('PREDUMMP', 'PREDUPRP'), db)
        
    def get_visites(self):
        """Visites 9 E"""
        self.upload = []
        sql = "select codi_sector,visi_up, visi_centre_codi_centre, visi_centre_classe_centre, \
               visi_servei_codi_servei, visi_modul_codi_modul, \
               visi_data_visita, if(visi_situacio_visita = 'R',1,0), visi_etiqueta \
               from visites \
               where visi_tipus_visita = '9E' and visi_data_baixa = 0"
        for sector, up, codi, classe, servei, modul, data, situacio,etiqueta in u.getAll(sql, 'import'):
            self.upload.append([data, sector, up, codi, classe, servei, modul, situacio, etiqueta])
            
    def upload_data(self):
        """."""
        u.listToTable(self.upload, tb, db)
       
if __name__ == '__main__':
    visita9E()
        