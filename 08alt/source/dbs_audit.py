from sisapUtils import *
import urllib2
from collections import Counter
from sys import exit

error = []
tipus = {('A','M'):'6CATDMET',('A','I'):'5CATDINF',('N','M'):'4CATDPED',('N','I'):'3CATDINFP','T':'2CATDTS','O':'1CATDODO',None:'0CATDNA','up':'CATDRUP','sap':'CATDRSAP',('A','A'):'7CATDALT',('N','A'):'7CATDALT'}

################################################################################

accessos = Counter()

brs = {}
ups = {}
saps = {}
sql = "select ics_codi_orig,ics_codi,scs_codi,sap_codi from cat_centres_with_jail"
for orig,klx,up,sap in getAll(sql,'nodrizas'):
    brs[orig] = klx
    ups[up] = klx
    saps[sap] = 'SAP' + sap
    accessos['NUMPOT','DBSRUPTS',klx,tipus['up']] += 2
    accessos['NUMPOT','DBSFRUPTS',klx,tipus['up']] += 2
    accessos['NUMPOT','DBSRUPTS',klx,tipus['sap']] += 1
    accessos['NUMPOT','DBSFRUPTS',klx,tipus['sap']] += 1

ubas = {}
sql = "select up,uba,tipus,'A' from eqa_ind.mst_ubas union select up,uba,tipus,'N' from pedia.mst_ubas"
for up,uba,tip,grup in getAll(sql,'import'):
    ubas[up,uba,tip] = tipus[grup,tip]
    accessos['NUMPOT','DBSFPROF',ups[up],tipus[grup,tip][1:]] += 1

categ = {}
sql = "select ide_usuari,up,uab,tipus from cat_professionals"
for usu,up,uba,tip in getAll(sql,'import'):
    if tip in ('T','O'):
        categoria = tipus[tip]
    elif tip in ('M','I'):
        try:
            categoria = ubas[up,uba,tip]
        except KeyError:
            categoria = tipus[None]
    else:
        categoria = tipus[None]
    try:
        existent = categ[usu]
    except KeyError:
        existent = -9
    if categoria > existent:
        categ[usu] = categoria

fet = {}
fetFormR = {}
fetDbs = {}
dext, = getOne("select date_format(data_ext,'%m%Y') from dextraccio",'nodrizas')
response = urllib2.urlopen('http://p400.ecap.intranet.gencat.cat/sisap/csv/auditdbs/rte9836/hitrft92/mesany/{}'.format(dext))
audit = csv.reader(response,delimiter=';')
bad = 0
try:
    for id,data,sector,usuari,rol,usuari_prof,br,codi_centre,classe_centre,codi_unitat,tipus_unitat,pantalla,indicador,caracteristiques,dataentrarpantalla in audit:

        if 'dbs' not in pantalla or br == '\N':
            continue

        if 'up' in br:
            try:
                centre = ups[br[-5:]]
            except KeyError:
                continue
        elif 'sap' in br:
            try:
                centre = saps[br[-2:]]
            except KeyError:
                continue
        else:
            try:
                centre = brs[br]
            except KeyError:
                try:
                    centre = ups[br]
                except KeyError:
                    continue
        
        if rol == 'responsable_sap':
            categoria = tipus['sap']
        elif rol == 'responsable_up':
            categoria = tipus['up']
        elif rol == 'individual':
            try:
                categoria = categ[usuari][1:]
            except KeyError:
                categoria = tipus[None][1:]
        else:
            continue
            
        if pantalla == 'descarrega_dbs':
            aplicacio = 'DBSRUPTS'
            accessos['DBSDESC',aplicacio,centre,categoria] += 1
        else:
            if pantalla == 'dbsform' and rol == 'individual':
                aplicacio = 'DBSFPROF'
            elif pantalla == 'dbsform' and rol in ('responsable_sap','responsable_up'):
                aplicacio = 'DBSFRUPTS'
            elif pantalla == 'dbs':
                aplicacio = 'DBSRUPTS'
            else:
                continue

            if indicador[:6] <> 'select':    
                accessos['NUMACC',aplicacio,centre,categoria] += 1
            else:
                accessos['DBSCONS',aplicacio,centre,categoria] += 1
            if (aplicacio,usuari,centre,categoria) not in fet:
                accessos['NUMUSU',aplicacio,centre,categoria] += 1
                fet[aplicacio,usuari,centre,categoria] = True
except Exception as e:
    if "line contains NUL" in str(e):
        bad += 1
    else:
        raise
print bad
file = tempFolder + 'accessos.txt'            
table = 'dbs_audit_accessos'
khalix = 'DBS_ACCESSOS'
with openCSV(file) as c:
    for (ind,app,ent,cat),n in accessos.items():
        c.writerow([ind,app,ent,cat,n])
execute('drop table if exists {}'.format(table),'altres')
execute('create table {} (ind varchar(10),app varchar(10),ent varchar(5),cat varchar(10),n int)'.format(table),'altres')
loadData(file,table,'altres')
q = "select ind,concat('A','periodo'),ent,app,cat,'NOIMP','DIM6SET','N',n from altres.{}".format(table)
error.append(exportKhalix(q,khalix))

################################################################################

rol = {}
sql = 'select rol_usu from cat_pritb799_def'
for usu, in getAll(sql,'import'):
    rol[usu] = 'ROLRUP'
sql = 'select usuari from cat_indtb271_def'
for usu, in getAll(sql,'import'):
    rol[usu] = 'ROLRSAP'

responsable = Counter()
response = urllib2.urlopen('http://p400.ecap.intranet.gencat.cat/sisap/csv/projectedbs/dfasdf/lkjhlk')
projectes = csv.reader(response,delimiter=';')
for id,tip,resp,login,limit,elim,data in projectes:
    try:
        responsable[id] = rol[login]
    except KeyError:
        responsable[id] = 'ROLRUP'
        
estudis = Counter()
fet = {}
response = urllib2.urlopen('http://p400.ecap.intranet.gencat.cat/sisap/csv/projecteprofessionalsdbs/dfasdf/lkjhlk')
professionals = csv.reader(response,delimiter=';')
for id,up,uba,tip,registres,rebutjats in professionals:
    if uba == '':
        continue
    try:
        categoria = ubas[up,uba,tip][1:]
    except KeyError:
        categoria = tipus['A',tip][1:]
    try:
        centre = ups[up]
    except KeyError:
        continue
    if (id,centre,categoria) not in fet:
        estudis['DBSCARRU','DBSRUPTS',centre,responsable[id],categoria] += 1
        fet[id,centre,categoria] = True
    estudis['DBSCARRP','DBSRUPTS',centre,responsable[id],categoria] += 1
    estudis['DBSCARRA','DBSRUPTS',centre,responsable[id],categoria] += int(registres)
    
file = tempFolder + 'estudis.txt'            
table = 'dbs_audit_estudis'
khalix = 'DBS_ESTUDIS'
with openCSV(file) as c:
    for (ind,app,ent,rol,cat),n in estudis.items():
        c.writerow([ind,app,ent,rol,cat,n])
execute('drop table if exists {}'.format(table),'altres')
execute('create table {} (ind varchar(10),app varchar(10),ent varchar(5),rol varchar(10),cat varchar(10),n int)'.format(table),'altres')
loadData(file,table,'altres')
q = "select ind,concat('A','periodo'),ent,app,cat,rol,'DIM6SET','N',n from altres.{}".format(table)
error.append(exportKhalix(q,khalix)) 

################################################################################

if any(error):
    exit("he hagut d'escriure al directori de rescat")
