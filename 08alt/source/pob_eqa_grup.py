# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

nod = "nodrizas"
db = "altres"
bds = ['eqa_ind', 'pedia']

assignada = "assignada_tot"
ubas = "mst_ubas"
centres = "export.khx_centres"

OutFile = tempFolder + 'freq.txt'
tipusEQA = {}
tableMy = "freq"


def getTipusEQA(adults, pedia, tipus):
    if adults == 1 and pedia == 1:
        if tipus == 'M':
            return 'TIPOMEDM'
        elif tipus == 'I':
            return 'TIPOENFM'
    elif adults == 1 and pedia == 0:
        if tipus == 'M':
            return 'TIPOMEDA'
        elif tipus == 'I':
            return 'TIPOENFA'
    elif adults == 0 and pedia == 1:
        if tipus == 'M':
            return 'TIPOMEDP'
        elif tipus == 'I':
            return 'TIPOENFP'
    elif adults == 0 and pedia == 0:
        if tipus == 'M':
            return 'TIPOMEDZ'
        elif tipus == 'I':
            return 'TIPOENFZ'

sql = "select up,uba,ates,count(*) from {} group by up,uba,ates".format(assignada)
for up, uba, ates, recompte in getAll(sql, nod):
    tipusEQA[(up, uba, int(ates), 'M')] = {'pob': recompte, 'adults': 0, 'pedia': 0}

sql = "select upinf,ubainf,ates,count(*) from {} group by upinf,ubainf,ates".format(assignada)
for up, uba, ates, recompte in getAll(sql, nod):
    tipusEQA[(up, uba, int(ates), 'I')] = {'pob': recompte, 'adults': 0, 'pedia': 0}

for bd in bds:
    sql = "select up,uba,tipus from {}".format(ubas)
    for up, uba, tipus in getAll(sql, bd):
        if (up, uba, 1, tipus) in tipusEQA:
            if bd == 'pedia':
                tipusEQA[(up, uba, 1, tipus)]['pedia'] = 1
            else:
                tipusEQA[(up, uba, 1, tipus)]['adults'] = 1
        if (up, uba, 0, tipus) in tipusEQA:
            if bd == 'pedia':
                tipusEQA[(up, uba, 0, tipus)]['pedia'] = 1
            else:
                tipusEQA[(up, uba, 0, tipus)]['adults'] = 1
tipuspob = 0
with openCSV(OutFile) as c:
    for (up, uba, ates, tipuse), d in tipusEQA.items():
        adults = d['adults']
        pedia = d['pedia']
        pob = d['pob']
        tipuspob = getTipusEQA(adults, pedia, tipuse)
        c.writerow([up, uba, ates, tipuspob, pob])

execute("drop table if exists {}".format(tableMy), db)
execute("create table {} (up varchar(5) not null default'', uba varchar(5) not null default'',ates double,tipus varchar(10) not null default'', recompte double)".format(tableMy), db)
loadData(OutFile, tableMy, db)

error = []

sql = "select 'GRPEQAPOBA',concat('A','periodo'),ics_codi,'NUM',tipus,'NOIMP','DIM6SET', 'N', sum(recompte) from {0}.{1} inner join {2} on up=scs_codi group by ics_codi,tipus \
        union select 'GRPEQAPOBA',concat('A','periodo'),ics_codi,'DEN',tipus,'NOIMP', 'DIM6SET', 'N', count(distinct up,uba) from {0}.{1} inner join {2} on up=scs_codi group by ics_codi,tipus\
        union select 'GRPEQAPOBT',concat('A','periodo'),ics_codi,'NUM',tipus,'NOIMP','DIM6SET', 'N', sum(recompte) from {0}.{1} inner join {2} on up=scs_codi where ates=1 group by ics_codi,tipus \
        union select 'GRPEQAPOBT',concat('A','periodo'),ics_codi,'DEN',tipus,'NOIMP', 'DIM6SET', 'N', count(distinct up,uba) from {0}.{1} inner join {2} on up=scs_codi where ates=1 group by ics_codi,tipus".format(db, tableMy, centres)
file = "POBL_GRP_EQA"
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
