# coding: utf8
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

nod = "nodrizas"
imp = "import"

db = "altres"
tb = "mst_prestacions_odn_up"

file_up = "PRESTACIONS_ODN"

codis = {'FC': {'p': 'PRODOP0001', 'a': 'PRODOA0001'},
        'SEG':{'p': 'PRODOP0002', 'a': 'PRODOA0002'},
        'EX': {'p': 'PRODOP0003', 'a': 'PRODOA0003'},
        'EXO': {'p': 'PRODOP0004', 'a': 'PRODOA0004'}, 
        'OBT': {'p': 'PRODOP0005', 'a': 'PRODOA0005'},
        'TT': {'p': 'PRODOP0006', 'a': 'PRODOA0006'}
        }

class prestacions(object):
    """Classe principal a instanciar."""
    
    def __init__(self):
        """Execució seqüencial."""
        printTime('inici')
        self.dades = Counter()
        self.dext, self.dext_1 = getOne("select date_format(data_ext, '%Y%m%d') , date_format(date_add(date_add(data_ext,interval - 1 year),interval +1 day), '%Y%m%d') from dextraccio", nod)
        print self.dext, self.dext_1
        self.get_poblacio()
        self.get_odn511()
        self.get_odn508()
        self.get_calculs()
        self.export_data()
    
    def get_poblacio(self):
        """poblacions"""
        self.pob = {}
        sql = "select id_cip_sec, upOrigen, edat, sexe from assignada_tot"
        for id, up, edat, sexe in getAll(sql, nod):
            self.pob[id] = {'up': up, 'edat': edat, 'sexe': sexe}
    
    def get_odn511(self):
        """Fluoritzacions"""
        self.fluors = defaultdict(list)
        sql = "select id_cip_sec, date_format(tb_data, '%Y%m%d'), if(tb_trac = 'FSC', 'FC', tb_trac) from odn511 where TB_TRAC in ('FC', 'FSC', 'TT')"
        for id, dat, trac in getAll(sql,imp):
            if self.dext_1 <= dat <= self.dext:
                self.fluors[(id, trac)].append(dat)
                
    def get_odn508(self):
        """Segellats, exodòncies i exodòncies quirúrgiques"""
        self.tractaments = Counter()
        sql = "select id_cip_sec, dtd_cod_p, dtd_tra, date_format(dtd_data, '%Y%m%d') from odn508 where dtd_tra in ('SEG', 'EX', 'EXO', 'OBT') and dtd_et = 'E'"
        for id, peca, tract, dat in getAll(sql, imp):
            if self.dext_1 <= dat <= self.dext:
                agr = 'DDEF' if peca <= 50 else 'DTEMP'
                self.tractaments[id, tract, agr] += 1
                
    def get_calculs(self):
        """Calculem els indicadors"""
        for (id, tract), dats in self.fluors.items():
            if id in self.pob:
                up, edat, sexe = self.pob[id]['up'], self.pob[id]['edat'], self.pob[id]['sexe']
                edat, sexe = ageConverter(edat, 5), sexConverter(sexe)
                t1 = codis[tract]['p']
                t2 = codis[tract]['a']
                self.dades[up, edat, sexe, t1, 'NOIMP'] += 1
                for dat in dats:
                    self.dades[up, edat, sexe, t2, 'NOIMP'] += 1
                    
        for (id, tract, agr), r in self.tractaments.items():
             if id in self.pob:
                up, edat, sexe = self.pob[id]['up'], self.pob[id]['edat'], self.pob[id]['sexe']
                edat, sexe = ageConverter(edat, 5), sexConverter(sexe)
                t1 = codis[tract]['p']
                t2 = codis[tract]['a']
                self.dades[up, edat, sexe, t1, agr] += 1
                self.dades[up, edat, sexe, t2, agr] += r
    
    def export_data(self):
        """Creem les taules i exportem"""
        upload = []
        for (up, edat, sexe, indicador, dents), valors in self.dades.items():
            upload.append([up, edat, sexe, indicador, dents, valors])
        
        columns = [ "up varchar(5)", "edat varchar(10)", "sexe varchar(5)", "indicador varchar(10)", "dents varchar(10)", "n int"]
        createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        listToTable(upload, tb, db)
        
        exportKhalix("select indicador,concat('A','periodo'),ics_codi,'NOCLI',edat,dents,sexe,'N',n from {}.{} a inner join export.khx_centres b on a.up=scs_codi".format(db, tb), file_up)
        
        printTime('fi')    

if __name__ == '__main__':
    prestacions()
