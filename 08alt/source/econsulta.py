# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import dateutil.relativedelta as r

import sisapUtils as u


tb_br = 'exp_khalix_up_econsulta'
tb_uba = 'exp_khalix_uba_econsulta'
tb_qc = 'exp_qc_econsulta'
db = 'altres'


class EConsulta(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_ubas()
        self.get_poblacio()
        self.get_professionals()
        self.get_dni_espe()
        self.get_converses()
        self.get_missatges()
        self.resultat = []
        self.get_econs0000_0002()
        self.get_econs0001()
        self.get_econs0003()
        self.get_econs0004()
        self.get_econs0005()
        self.get_econs0006_0007_0008()
        self.export_br()
        self.export_uba()
        self.export_qc()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_ubas(self):
        """."""
        self.ubas = c.defaultdict(set)
        sql = "select up, tipus, uba from mst_ubas"
        for up, tipus, uba in u.getAll(sql, 'eqa_ind'):
            if up in self.centres:
                self.ubas[(self.centres[up], tipus)].add(uba)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat from assignada_tot \
               where ates = 1 and institucionalitzat = 0"
        self.poblacio_pac = {}
        self.poblacio_ent = c.Counter()
        self.poblacio_edat = {}
        self.poblacio_qc = c.Counter()
        for id, up, uba, ubainf, edat in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up]
                if uba in self.ubas[(br, 'M')] or ubainf in self.ubas[(br, 'I')]:  # noqa
                    adult = edat > 14
                    entities = (br, ''.join((br, 'M', uba)),
                                ''.join((br, 'I', ubainf)))
                    self.poblacio_pac[id] = entities
                    for entity in entities:
                        self.poblacio_ent[entity] += 1
                    self.poblacio_edat[id] = adult
                    tipus = ["TIPPROF", "TIPPROF4", "TIPPROF1" if adult else "TIPPROF2"]  # noqa
                    for tip in tipus:
                        self.poblacio_qc[(br, tip)] += 1

    def get_professionals(self):
        """."""
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, \
                      lloc_proveidor_dni_proveidor \
               from cat_pritb025"
        llocs = {(self.centres[up], lloc): dni for (up, lloc, dni)
                 in u.getAll(sql, 'import') if up in self.centres}
        params = (('uab_codi_up', 'M', 'uab_codi_uab',
                   'uab_lloc_de_tr_codi_lloc_de_tr', 'cat_vistb039'),
                  ('uni_codi_up', 'I', 'uni_codi_unitat',
                   'uni_ambit_treball', 'cat_vistb059'))
        sql = "select {}, '{}', {}, {} from {}"
        self.professionals = c.defaultdict(set)
        for param in params:
            for up, tipus, uba, lloc in u.getAll(sql.format(*param), 'import'):
                if up in self.centres:
                    br = self.centres[up]
                    dni = llocs.get((br, lloc))
                    if dni and (uba in self.ubas[(br, tipus)]):
                        self.professionals[(br, tipus)].add(dni)

    def get_dni_espe(self):
        """."""
        sql = "select codi_sector, prov_dni_proveidor, prov_categoria \
               from cat_pritb031"
        self.dni_espe = {row[:2]: (1 if row[2][0] == '1' else 2)
                         for row in u.getAll(sql, 'import')}

    def get_converses(self):
        """."""
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv, nodrizas.dextraccio \
               where conv_dini between \
                adddate(adddate(data_ext, interval -1 year), interval +1 day) \
                and data_ext"
        self.converses = {}
        for pac, sector, id, autor, desti, estat, ini in u.getAll(sql, 'import'):  # noqa
            if pac in self.poblacio_pac:
                prof = (autor if autor else desti)
                espe = self.dni_espe.get((sector, prof))
                if espe:
                    entities = [self.poblacio_pac[pac][i] for i in (0, espe)]
                    tipprof = 4 if espe == 1 else 1 if self.poblacio_edat[pac] else 2  # noqa
                    self.converses[(sector, id)] = {'pac': pac,
                                                    'br': entities[0],
                                                    'entities': entities,
                                                    'prof': prof,
                                                    'inicia': not desti,
                                                    'estat': estat,
                                                    'ini': ini,
                                                    'tipprof': "TIPPROF{}".format(tipprof),  # noqa
                                                    'msg': {}}

    def get_missatges(self):
        """."""
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data in u.getAll(sql, 'import'):
            if (sector, id_c) in self.converses:
                this = {'prof': autor if autor else desti,
                        'inicia': not desti,
                        'data': data}
                self.converses[(sector, id_c)]['msg'][id] = this

    def get_econs0000_0002(self):
        """."""
        ind = ('ECONS0000', 'ECONS0002')
        num = (c.Counter(), c.Counter())
        pacients = c.Counter()
        qc = c.defaultdict(set)
        self.qc = []
        for conversa in self.converses.values():
            if conversa['estat'] == 'T':
                id = conversa['pac']
                for entity in conversa['entities']:
                    pacients[(entity, id)] += 1
                periode = "A{}".format(conversa['ini'].strftime("%y%m"))
                for tipprof in ("TIPPROF", conversa["tipprof"]):
                    qc[(conversa['br'], tipprof, "ANUAL")].add(id)
                    qc[(conversa['br'], tipprof, periode)].add(id)
        for (entity, pac), n in pacients.items():
            num[0][entity] += 1
            if n > 1:
                num[1][entity] += 1
        for entity, n in self.poblacio_ent.items():
            for i in range(len(ind)):
                self.resultat.append((ind[i], entity, 'DEN', n))
                self.resultat.append((ind[i], entity, 'NUM', num[i][entity]))
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        tipus = {"ANUAL": ("ANUAL", pactual),
                 pactual: ("ACTUAL", pactual),
                 pprevi: ("PREVI", pprevi)}
        for (br, tipprof), n in self.poblacio_qc.items():
            for key, val in tipus.items():
                this = ("QECONS0000", val[1], br, "DEN", val[0], tipprof, n)
                self.qc.append(this)
                num = len(qc[(br, tipprof, key)])
                if num:
                    that = ("QECONS0000", val[1], br, "NUM", val[0], tipprof, num)  # noqa
                    self.qc.append(that)

    def get_econs0001(self):
        """."""
        ind = 'ECONS0001'
        resul = c.defaultdict(set)
        for conversa in self.converses.values():
            if conversa['estat'] == 'T':
                resul[conversa['br']].add(conversa['prof'])
        for (br, tipus), ubas in self.ubas.items():
            den = len(ubas)
            professionals = self.professionals[(br, tipus)]
            num = sum([prof in resul[br] for prof in professionals])
            self.resultat.append((ind + tipus, br, 'DEN', den))
            self.resultat.append((ind + tipus, br, 'NUM', num))

    def get_econs0003(self):
        """."""
        ind = 'ECONS0003'
        resul = c.Counter()
        for conversa in self.converses.values():
            if conversa['estat'] == 'T':
                for entity in conversa['entities']:
                    resul[entity] += 1
        for entity, n in self.poblacio_ent.items():
            self.resultat.append((ind, entity, 'DEN', n))
            self.resultat.append((ind, entity, 'NUM', resul[entity]))

    def get_econs0004(self):
        """."""
        ind = 'ECONS0004'
        den = c.Counter()
        num = c.Counter()
        for conversa in self.converses.values():
            if conversa['estat'] == 'T':
                for entity in conversa['entities']:
                    den[entity] += 1
                    num[entity] += len(conversa['msg'])
        for entity, n in den.items():
            self.resultat.append((ind, entity, 'DEN', n))
            self.resultat.append((ind, entity, 'NUM', num[entity]))

    def get_econs0005(self):
        """."""
        ind = 'ECONS0005'
        den = c.Counter()
        num = c.Counter()
        for conversa in self.converses.values():
            if conversa['estat'] == 'T':
                for entity in conversa['entities']:
                    den[entity] += 1
                    num[entity] += conversa['inicia']
        for entity, n in den.items():
            self.resultat.append((ind, entity, 'DEN', n))
            self.resultat.append((ind, entity, 'NUM', num[entity]))

    def get_econs0006_0007_0008(self):
        """."""
        ind = ('ECONS0006', 'ECONS0007', 'ECONS0008')
        den = c.Counter()
        num = (c.Counter(), c.Counter(), c.Counter())
        for conversa in self.converses.values():
            if conversa['estat'] == 'T' and not conversa['inicia']:
                for entity in conversa['entities']:
                    den[entity] += 1
                    pregunta = conversa['msg'][1]['data']
                    resposta = None
                    for id in range(2, 1000):
                        if id in conversa['msg']:
                            if conversa['msg'][id]['inicia']:
                                resposta = conversa['msg'][id]['data']
                                break
                        else:
                            break
                    if resposta:
                        dies = EConsulta.conta_dies(pregunta, resposta)
                        num[0][entity] += dies in (0, 1)
                        num[1][entity] += dies in (0, 1, 2)
                        num[2][entity] += dies
        for entity, n in den.items():
            for i in range(3):
                self.resultat.append((ind[i], entity, 'DEN', n))
                self.resultat.append((ind[i], entity, 'NUM', num[i][entity]))

    @staticmethod
    def conta_dies(inici, final):
        """."""
        total = (final - inici).days
        laborables = 0
        for i in range(total):
            dia = inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                laborables += 1
        return laborables

    def export_br(self):
        """."""
        u.createTable(tb_br, '(indicador varchar(10), br varchar(5), \
                               analisis varchar(10), n int)', db, rm=True)
        u.listToTable([row for row in self.resultat if len(row[1]) == 5],
                      tb_br, db)
        sql = "select indicador, 'Aperiodo', br, analisis, 'NOCAT', \
               'NOINSAT', 'DIM6SET', 'N', n from {}.{}".format(db, tb_br)
        file = 'ECONSULTA'
        u.exportKhalix(sql, file)

    def export_uba(self):
        """."""
        u.createTable(tb_uba, '(indicador varchar(10), ent varchar(11), \
                                analisis varchar(10), n int)', db, rm=True)
        u.listToTable([row for row in self.resultat if len(row[1]) > 5],
                      tb_uba, db)
        sql = "select indicador, 'Aperiodo', ent, analisis, 'NOCAT', \
               'NOINSAT', 'DIM6SET', 'N', n from {}.{}".format(db, tb_uba)
        file = 'ECONSULTA_UBA'
        u.exportKhalix(sql, file)

    def export_qc(self):
        """."""
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(tb_qc, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.qc, tb_qc, db)


if __name__ == '__main__':
    EConsulta()
