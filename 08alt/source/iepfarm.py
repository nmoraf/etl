from sisapUtils import *
import time
from collections import defaultdict,Counter
from datetime import *

file_name="IEPFAR"
nod="nodrizas"
imp_jail="import_jail"
alt="altres"
khalix=True
exclusions ={ ('07674','M15'), ('07675','M21'),('07675','M26')}


psicofarmacos_atc= set(['N03AG01', 'N05CM02', 'N05AX08', 'N04BA02', 'N06BA09', 'N06AA09', 'N05BA06', 'N05AH06', 'N05AH05',
             'N05AH04', 'N05AH03', 'N05AH02', 'N05BA12', 'N07BB04', 'N03AX09', 'N07BB01', 'N05BA10', 'N05AG02', 
             'N05BA05', 'N05AN01', 'N06AX05', 'N05BA01', 'N03AE01', 'N07BC02', 'N02AB03', 'N02AA01', 'N04AA02', 
             'N06AB04', 'N05AX12', 'N05AX13', 'N05AD01', 'N06AB05', 'N06BA04', 'N05BA09', 'N05BA08', 'N05AL03', 
             'N05AF05', 'N05AL01', 'N05BA02', 'N05AE04', 'N06AB06', 'N07BC51', 'N03AF01', 'N05CD06', 'N05CD01', 
             'N06AB03', 'N05AB03', 'N05AB02', 'N03AX12', 'N06AA04', 'N03AX11', 'N03AX16', 'N05AC01', 'N03AX14', 
             'N06AX11', 'N06AX16', 'N05AA02', 'N03AA02', 'N05AA01'])

depot= set(['711282', '700662', '700661', '700660', '711278', '741256', '741264', '711288','700659', '741207', '701736', '711280', '701735','700535','665966',
                    '700536', '665967', '700533', '665965','700532','665964','700535','665966','700536','665967','700533','665965','700532','665964'])

antipisoc=set(['N05AF05', 'N05AB03', 'N05AB02', 'N05AN01', 'N05AH03', 'N05AG02', 'N05AX08', 'N05AC01', 'N05AX12', 
                    'N05AX13', 'N05AL01', 'N05AD01', 'N05AL03', 'N05AH06', 'N05AH05', 'N05AH04', 'N05AA02', 'N05AH02', 
                    'N05AE04', 'N05AA01'])

benzo=set(['N05CD06', 'N05CD01', 'N05CD08', 'N05BA06', 'N05BA12', 'N05BA09', 'N05BA08', 'N03AE01', 'N05BA05', 'N05BA10', 'N05BA02', 'N05BA01'])

preglabina=set(['N03AX16'])

#Cada subindicador, para el set de farmacos que deben tener prescrito actualmente los ids del numerador y, si procede, del denominador
ind_farmac={'1a': {'num': psicofarmacos_atc},
            '1b': {'num': psicofarmacos_atc, 'den':  psicofarmacos_atc},
            '2': {'num': depot, 'den': antipisoc }, 
            '3': {'num': benzo },
            '4': {'num': antipisoc, 'den': antipisoc},
            '5': {"num": preglabina, 'den':preglabina},
            '6': { "num":benzo, 'den': benzo}
  }


#Diccionario para en la busqueda general a tractaments, asociar cada codigo a un subindicador y al numerador o denominador
farm_ind=defaultdict(set)
for ind in ind_farmac:
    for num_den, set_codis in ind_farmac[ind].iteritems():
        for codi in set_codis:
            farm_ind[codi].add((ind,num_den))



def get_problemes(current_date):
    """ Selecciona de import.problemas los ids que presenten un pr_cod_ps dentro de las opciones en in_statement o que empiecen con los caracteres que hay 
        en la variable like_statement, que se ha 'traducido' a query de sql en la variable where_like. import.problemes se filtra como se hace para crear eqa.problemes
        (pr_cod_o_ps = 'C' and pr_hist = 1, y que la fecha de baixa logica del servicio sea nula)
        De lo id que resultan, se coge la fecha de diagnostico (dde) y la de fin del problema de salud (dba). Si dba es nula, se le asigna la del fin del periodo de calculo.
        Por ultimo, se anyaden a un set aquellos ids para los que la dba es mayor o igual a la 
        del fin del periodo (es mas reciente o la misma)
    """
    
    like_statement=['G40%','G41%','C01-G40%','C01-G41%']
    where_like=" ".join("or pr_cod_ps like '{}'".format(el) for el in like_statement)
    in_statement=('F41.1','M79.2')


    sql="select id_cip_sec,pr_dde,pr_dba from {}.problemes\
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0 and (pr_cod_ps in {} {})".format(imp_jail,in_statement,where_like)
    
    pacients=set()
    for id, dde,dba in getAll(sql,imp_jail):
        #dba= fi if not dba else dba
        if not dba or dba >= current_date:
            pacients.add(id)
    return pacients

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]	

def get_total_farmacs(ind_farmac):
    farmac_total= defaultdict(set)
    for number in ind_farmac:
        for typ in ind_farmac[number]:
            for farm in ind_farmac[number][typ]:
                try: 
                    int(farm)
                    key='ppfmc_pf_codi'
                except:
                    key='ppfmc_atccodi'
                farmac_total[key].add(farm)
    return farmac_total


def get_id_pa_farm(set_farms,current_date,codi_field):
    id_pa_farm=defaultdict(lambda: defaultdict(set))
    sql="select id_cip_sec,{0},ppfmc_pmc_data_ini,ppfmc_data_fi from {1}.tractaments where {0} in {2}".format(codi_field,imp_jail,tuple(set_farms))
    print(sql)
    for id,farm_codi,ini,fi in getAll(sql,imp_jail):
        if ini <= current_date <= fi:
            for ind_type in farm_ind[str(farm_codi)]:
                id_pa_farm[ind_type][id].add((farm_codi,ini))
    return id_pa_farm


def get_indicador(codi,num, pac,den=None):
    indicador=defaultdict(Counter)
    rows=[]
    sql="select id_cip_sec, up,uba from {}.jail_assignada".format(nod)
    print codi
    for id,up,uba in getAll(sql,nod):
        if not (up,uba) in exclusions:
            denominador=id in den if den else True 
            if denominador:
                number_den= 1 if pac else len(den[id])
                indicador[up]["DEN"]+=number_den
                if id in num:
                    number_num= 1 if pac else len(num[id])
                    indicador[up]["NUM"]+=number_num
        
               

    return [(up,codi,indicador[up]["NUM"],indicador[up]["DEN"]) for up in indicador]


def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

if __name__ == '__main__':
    
    printTime('inici')
   
    current_date=get_date_dextraccio()
    print(current_date)

    table_name="exp_ecap_iepfarm_uba"
    uba_columns="(up varchar(5),indicador varchar(15),numerador int, denominador int)"
    createTable(table_name,uba_columns,alt,rm=True)

    #pacientes con problemas de salud registrados
    prob_salut_preg=get_problemes(current_date)

    #Diccionario que asocia, para cada subindicador y tipo (num/den) un tuple con los elementos siguientes:
    #   -> times (int), el numero de farmacos al que el paciente debe estar prescrito para entrar en el tipo correspondiente
    #   -> days (int), el numero de dias minimos en los que tiene que estar hecha la prescripcion
    #   -> set_ids_in (set_ids), un set de pacientes al que tienen que pertenecer los pacientes prescritos con los farmacos correspondientes 
    # La key 'PAC' indica si el indicador es a nivel de paciente o a nivel de prescripcion
    # EL DICCIONARIO CON LOS FARMACOS PARA CADA SUBINDICADOR Y TIPO ES OTRO, el ind_farmac

    ind_opt = {
    "1a":{ "num": (5,None,None),'PAC':1 },
    "1b":{ "num": (5,None,None),'PAC':1, "den":(1,None,None) },
    "2": { "num": (1,None,None),'PAC':1, "den":(1,None,None) },
    "3": { "num": (3,None,None),'PAC':1 },
    "4": { "num": (3,None,None),'PAC':1, "den":(1,None,None)},
    "5": { "num": (1,None,prob_salut_preg),'PAC':0 ,"den":(1,None,None)},
    "6": { "num": (1,6*7,None),'PAC':1, "den":(1,None,None) },
    }


    #obtiene para cada tipo de codigo (atc o pf) el set de farmacos y asi hacer una select conjunta por tipo de codigo
    farmac_total= get_total_farmacs(ind_farmac)
    
    #obtiene para el subindicador y tipo (num/den) el id del paciente y un set (farm, fecha_inicio) para cada farmaco que corresponde
    id_pa_farmac_total=defaultdict(lambda: defaultdict(set))
    for codi_field,set_farms in farmac_total.iteritems():
        id_pa_farmac=get_id_pa_farm(set_farms,current_date,codi_field)
        for key in id_pa_farmac:
            for id in id_pa_farmac[key]:
                id_pa_farmac_total[key][id]|=id_pa_farmac[key][id]

    print len(prob_salut_preg)

    #MONTA CADA UNO DE LOS INDICADORES USANDO LA INFO EN IND_OPT
    for ind in ind_opt:
        
        codi="IEPFAR00"+ind
        
        num_den_ids=defaultdict(lambda: defaultdict(set))
        for tipus in ind_opt[ind]:
                
            if tipus=='PAC':
                continue
            times,days,set_ids_in=ind_opt[ind][tipus]
            for id in id_pa_farmac_total[(ind,tipus)]:
                if set_ids_in:
                    if id not in set_ids_in:
                        continue
                set_fechas= {ini for farm_codi,ini in id_pa_farmac_total[(ind,tipus)][id] if daysBetween(ini,current_date) >= days} if days else id_pa_farmac_total[(ind,tipus)][id]
                if len(set_fechas) >= times:
                    num_den_ids[tipus][id]|=set_fechas
            
        den= num_den_ids['den'] if 'den' in ind_opt[ind] else None
        print type(den)
        print len(num_den_ids['num'])
        rows=get_indicador(codi,num_den_ids['num'],ind_opt[ind]["PAC"],den)
        listToTable(rows,table_name,alt)
        printTime("Insert into the table {}".format(codi))
        
    query_template_string="select indicador,concat('A','periodo'),ics_codi,{strg},'NOCAT','NOIMP','DIM6SET','N',{var} from altres.{taula} a inner join nodrizas.jail_centres b on a.up=scs_codi where a.{var} != 0"
    query_string=query_template_string.format(strg="'NUM'",var="numerador",taula=table_name)+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=table_name)
    print(query_string)
    exportKhalix(query_string,file_name)