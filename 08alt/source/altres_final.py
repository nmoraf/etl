from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="altres"
conn = connect((db,'aux'))
c=conn.cursor()

ecap_ind = "exp_ecap_alt_uba"
ecap_pac = "exp_ecap_alt_pacient"
cataleg = "exp_ecap_alt_cataleg"
catalegPare = "exp_ecap_alt_catalegPare"
catalegExclosos = "exp_ecap_alt_catalegExclosos"


taules = [['exp_ecap_tir_uba','exp_ecap_tir_pacient','exp_ecap_tir_cataleg','exp_ecap_tir_catalegPare','exp_ecap_tir_catalegexclosos'],
          ['exp_ecap_tao_uba','exp_ecap_tao_pacient','exp_ecap_tao_cataleg','exp_ecap_tao_catalegPare','exp_ecap_tao_catalegexclosos'],
          ['exp_ecap_inf_uba','exp_ecap_inf_pacient','exp_ecap_inf_cataleg','exp_ecap_inf_catalegPare',''],
          ['exp_ecap_pcc_uba','exp_ecap_pcc_pacient','exp_ecap_pcc_cataleg','exp_ecap_pcc_catalegPare',''],
          ['exp_ecap_aprop_uba','exp_ecap_aprop_pacient','exp_ecap_aprop_cataleg','exp_ecap_aprop_catalegPare',''],
          ['exp_ecap_alertes_uba','exp_ecap_alertes_pacient','exp_ecap_alertes_cataleg','exp_ecap_alertes_catalegPare',''],
          ['exp_ecap_prof_uba','exp_ecap_prof_pacient','exp_ecap_prof_cataleg','exp_ecap_prof_catalegPare',''],
          ['exp_ecap_gescasos_uba','exp_ecap_gescasos_pacient','exp_ecap_gescasos_cataleg','exp_ecap_gescasos_catalegPare',''],]

c.execute("drop table if exists %s,%s,%s,%s,%s" % (ecap_ind,ecap_pac,cataleg,catalegPare,catalegExclosos))
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',numerador int,denominador int,resultat double)" % ecap_ind)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',uba varchar(20) not null default '',upinf varchar(5) not null default '',ubainf varchar(7) not null default '',grup_codi varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("create table %s (indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),llistat int,invers int,mmin double,mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))" % cataleg)
c.execute("create table %s (pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))" % catalegPare)
c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos)
c.execute("alter table %s add unique(codi,descripcio,ordre)" % catalegExclosos)

for r in taules:
    uba,llist,cat,catP,catE=r[0],r[1],r[2],r[3],r[4]
    c.execute("insert into %s select * from %s where uba<>''" % (ecap_ind,uba))
    if llist.endswith("pacient"):
        c.execute("insert into %s select * from %s where uba<>''" % (ecap_pac,llist))
    c.execute("insert into %s select * from %s" % (cataleg,cat))
    c.execute("insert into %s select * from %s" % (catalegPare,catP))
    if catE.endswith("catalegexclosos"):
        c.execute("insert ignore into %s select * from %s" % (catalegExclosos,catE))

c.execute("update %s set uba='' where uba=0 and grup_codi like ('PLA%%')" % ecap_pac)
    
    
conn.close()

print strftime("%Y-%m-%d %H:%M:%S")