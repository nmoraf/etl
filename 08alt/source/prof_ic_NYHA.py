# coding: iso-8859-1

from sisapUtils import *
from datetime import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

alt = 'altres'
nod = 'nodrizas'

valida = False

profvalida = {
            1: {'tip': 'I','up': '00488', 'uba': '25888'}
            }

dext, = getOne('select data_ext from dextraccio', nod)

#assignats

poblacio = {}
sql = 'select id_cip_sec, up, uba, upinf, ubainf, edat, ates, institucionalitzat from assignada_tot'
for id, up, uba, upinf, ubainf, edat, ates, insti in getAll(sql, nod):
    if edat>14:
        if insti == 0:
            poblacio[id] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf':ubainf, 'ates': ates}

#agafem els registres de nyha dels ultims 6 mesos
nyha = {}

sql = 'select id_cip_sec, data_var from eqa_variables where agrupador=427'
for id, data_var in getAll(sql, nod):
    mesos = monthsBetween(data_var, dext)
    if 0 <= mesos <= 5:
        nyha[id] = True

#mirem les ICC

upload = []
sql = 'select id_cip_sec from eqa_problemes where ps = 21'
for id, in getAll(sql, nod):
    try:
        up = poblacio[id]['up']
        uba = poblacio[id]['uba']
        upinf = poblacio[id]['upinf']
        ubainf = poblacio[id]['ubainf']
        ates = poblacio[id]['ates']
    except KeyError:
        continue
    num = 0
    if id in nyha:
        num = 1
    upload.append([id, up, uba, upinf,ubainf, ates, num])
    
#taula de my

table = 'mst_prof_nyha'
createTable(table, '(id_cip_sec int, up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5),ates int, num int)', alt, rm=True)
listToTable(upload, table, alt)

#taules pacients i uba

pacients, denominador, numerador = [], Counter(), Counter()

sql = 'select id_cip_sec, up, uba, upinf, ubainf, ates, num from {}'.format(table)
for id, up, uba, upinf,ubainf, ates, num in getAll(sql, alt):
    if ates == 1:
        if num == 0:
            pacients.append([id, up, uba, upinf, ubainf, 0])
        denominador[(up, uba, 'M')] += 1
        denominador[(upinf, ubainf, 'I')] += 1
        numerador[(up, uba, 'M')] += num
        numerador[(upinf, ubainf, 'I')] += num

upload = [(up, uba, tipus, 
                   numerador[(up, uba, tipus)], n,
                   numerador[(up, uba, tipus)] / float(n))
                  for (up, uba, tipus), n in denominador.items()]
  
tablep = 'exp_ecap_IAD0005_pacient'
createTable(tablep, "(id_cip_sec int,up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), exclos int)", alt, rm=True)
listToTable(pacients, tablep, alt)
  
table = 'exp_ecap_IAD0005_uba'
createTable(table, "(up varchar(5),uba varchar(5),tipus varchar(1), numerador int, denominador int, resultat double)", alt, rm=True)
listToTable(upload, table, alt)

uploadvalp = []
uploadval = []
if valida:
    for professional in profvalida:
        up = profvalida[professional]['up']
        uba = profvalida[professional]['uba']
        tip = profvalida[professional]['tip']
        if tip == 'M':
            sql = "select id_cip_sec, up, uba, upinf, ubainf from {0} where up = '{1}' and uba = '{2}'".format(tablep, up, uba)
        elif tip == 'I':
            sql = "select id_cip_sec, up, uba, upinf, ubainf from {0} where upinf = '{1}' and ubainf = '{2}'".format(tablep, up, uba)
        for a,b,c,d,e in getAll(sql, alt):
            uploadvalp.append([a,b,c,d,e, 0])
        sql = "select up, uba, tipus, numerador,denominador,resultat from {0} where up = '{1}' and uba = '{2}' and tipus = '{3}'".format(table, up, uba, tip)
        for a,b,c,d,e,f in getAll(sql, alt):
            uploadval.append([a,b,c,d,e,f])    


    createTable(tablep, "(id_cip_sec int,up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), exclos int)", alt, rm=True)
    listToTable(uploadvalp, tablep, alt)
      
    createTable(table, "(up varchar(5),uba varchar(5),tipus varchar(1), numerador int, denominador int, resultat double)", alt, rm=True)
    listToTable(uploadval, table, alt)            

