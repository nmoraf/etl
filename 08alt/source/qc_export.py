# coding: latin1

"""
.
"""

import sisapUtils as u


TABLES = ("EXP_QC_FORATS", "EXP_QC_ECONSULTA", "EXP_QC_GIDA", "EXP_QC_VISITES")
FILE = "QC_ORGANITZACIO"
BASE = "select k0, k1, k2, k3, if(k4 = 'ANUAL', 'ANUAL', 'MENSUAL'), k5, \
               'DIM6SET', 'N', v \
        from altres.{} where k4 {} 'PREVI'"


for suffix, symbol in (("_ACTUAL", "<>"), ("_PREVI", "=")):
    sql = " union ".join(BASE.format(table, symbol) for table in TABLES)
    u.exportKhalix(sql, FILE + suffix)
