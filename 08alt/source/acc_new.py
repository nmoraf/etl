# coding: latin1

"""
.
"""

import collections as c
import dateutil.relativedelta as r

import sisapUtils as u


tb = "exp_khalix_forats"
tb_uba = "exp_ecap_forats"
tb_qc = "exp_qc_forats"
db = "altres"
file_up = "ACCESSIBILITAT_FORATS"
file_uba = "ACCESSIBILITAT_FORATS_UBA"

tipprof = {"MG": 1, "PED": 2,
           "ODN": 3, "ODO": 3,
           "INF": 4, "ENF": 4, "INFP": 4, "INFG": 4, "INFPD": 4, "INFGR": 4,
           "GCAS": 6}
tipuba = {1: "M", 2: "M", 3: None, 4: "I", 6: "I"}


class Accessibilitat(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_ubas()
        self.get_dades()
        self.get_khalix()
        self.get_ecap()
        self.export_khalix()
        self.export_ecap()
        self.set_catalegs()
        self.get_qc()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_ubas(self):
        """."""
        sql = "select up, uba, tipus from eqa_ind.mst_ubas union \
               select up, uba, tipus from pedia.mst_ubas"
        self.ubas = set([row for row in u.getAll(sql, "eqa_ind")])

    def get_dades(self):
        """."""
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1a = dext - r.relativedelta(years=1) + r.relativedelta(days=1)
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sql = "select up, uba, servei, dia, peticions, \
                      forats_0_lab, forats_1_lab, forats_2_lab, forats_3_lab, \
                      forats_4_lab, forats_5_lab, forats_6_lab, forats_7_lab, \
                      forats_8_lab, forats_9_lab, forats_10_lab \
               from sisap_accessibilitat \
               where dia between to_date('{}', 'YYYYMMDD') and \
                                 to_date('{}', 'YYYYMMDD')".format(
                                                    fa1a.strftime("%Y%m%d"),
                                                    dext.strftime("%Y%m%d"))
        self.dades = c.defaultdict(lambda: c.Counter())
        self.dades_uba = c.defaultdict(lambda: c.Counter())
        self.dades_qc = c.Counter()
        for row in u.getAll(sql, "redics"):
            up, uba, servei, dia, peticions = row[:5]
            if up in self.centres and peticions:
                forats = row[5:]
                periode = "A{}".format(dia.strftime("%y%m"))
                centre = self.centres[up]
                tipus = "TIPPROF{}".format(tipprof[servei])
                ids = [(periode, centre, tipus)]
                ids_uba = []
                tipus_uba = tipuba[tipprof[servei]]
                if (up, uba, tipus_uba) in self.ubas:
                    entity = centre + tipus_uba + uba
                    ids.append((periode, entity, "NOIMP"))
                    ids_uba.append((periode, up, tipus_uba, uba))
                for dies in (2, 3, 5, 10):
                    cuenta = "ACC{}D".format(dies)
                    numerador = min(sum(forats[:(dies + 1)]), peticions)
                    for id in ids:
                        if dia.year == dext.year:
                            self.dades[id][(cuenta, "DEN")] += peticions
                            self.dades[id][(cuenta, "NUM")] += numerador
                    for idu in ids_uba:
                        if periode == pactual and dies != 3:
                            self.dades_uba[idu][cuenta, "DEN"] += peticions
                            self.dades_uba[idu][cuenta, "NUM"] += numerador
                    this = ("Q" + cuenta, centre, tipus)
                    self.dades_qc[this + (pactual, "ANUAL", "DEN")] += peticions  # noqa
                    self.dades_qc[this + (pactual, "ANUAL", "NUM")] += numerador  # noqa
                    if periode in (pprevi, pactual):
                        that = (periode, "ACTUAL" if periode == pactual else "PREVI")  # noqa
                        self.dades_qc[this + that + ("DEN",)] += peticions
                        self.dades_qc[this + that + ("NUM",)] += numerador

    def get_khalix(self):
        """."""
        self.upload = []
        for (periode, centre, tipus), dades in self.dades.items():
            for (cuenta, analisis), n in dades.items():
                self.upload.append((cuenta, periode, centre, analisis, "NOCAT", tipus, "DIM6SET", "N", n))  # noqa

    def get_ecap(self):
        """."""
        self.upload_ecap = []
        for (periode, up, tipus_uba, uba), dades in self.dades_uba.items():
            for (cuenta, analisis), n in dades.items():
                if analisis == "DEN":
                    num = self.dades_uba[periode, up, tipus_uba, uba][cuenta, "NUM"]  # noqa
                    perc = float(num)/float(n)
                    self.upload_ecap.append((up, uba, tipus_uba, cuenta, num, n, perc))  # noqa

    def export_khalix(self):
        """."""
        columns = ["d{} varchar(11)".format(i) for i in range(8)]
        columns.append("n int")
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.exportKhalix("select * from {}.{} where length(d2) = 5".format(db, tb), file_up)  # noqa
        u.exportKhalix("select * from {}.{} where length(d2) > 5".format(db, tb), file_uba)  # noqa

    def export_ecap(self):
        """."""
        columns = ["d{} varchar(11)".format(i) for i in range(4)]
        columns.append("num int")
        columns.append("den int")
        columns.append("resultat double")
        u.createTable(tb_uba, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload_ecap, tb_uba, db)

    def set_catalegs(self):
        """."""
        tb = "exp_ecap_forats_cataleg"
        cols = """(indicador varchar(8),literal varchar(300), ordre int,
                   pare varchar(8), llistat int, invers int not null default 0,
                   mmin double not null default 0, mint double not null default 0,
                   mmax double not null default 0, toShow int,
                   wiki varchar(250) not null default '')"""
        u.createTable(tb, cols, db, rm=True)
        pare = "ACC"
        pare2 = "LONG"  # aquests es pugen a alt_ecap_indicadors.py
        upload = [("ACC2D", "Accessibilitat 48 hores",
                   1, pare, 0, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/ACC2D"),
                  ("ACC3D", "Accessibilitat 72 hores",
                   1, pare, 0, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/ACC3D"),
                  ("ACC5D", "Accessibilitat 5 dies",
                   2, pare, 0, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/ACC5D"),
                  ("ACC10D", "Accessibilitat 10 dies",
                   3, pare, 0, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/ACC10D"),
                  ("CONT0002", "�ndex del prove�dor assistencial principal",
                   1, pare2, 0, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/CONT0002"),
                  ("VISUBA", "% Percentatge de visites realitzades en agendes UBA",  # noqa
                   2, pare2, 0, 0, 0, 0, 0, 1,
                   "http://sisap-umi.eines.portalics/indicador/codi/VISUBA")]
        u.listToTable(upload, tb, db)
        tb = "exp_ecap_forats_catalegpare"
        cols = "(pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))"  # noqa
        u.createTable(tb, cols, db, rm=True)
        upload = [(pare, "Accessibilitat", 2, "acc"),
                  (pare2, "Longitudinalitat", 1, "acc")]
        u.listToTable(upload, tb, db)

    def get_qc(self):
        """."""
        dades = [(cuenta, periodo, centro, analisis, anual, tipo, n)
                 for (cuenta, centro, tipo, periodo, anual, analisis), n
                 in self.dades_qc.items()]
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(tb_qc, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(dades, tb_qc, db)


if __name__ == "__main__":
    Accessibilitat()
