# coding: iso-8859-1
from datetime import *
from sisapUtils import *
from collections import defaultdict, Counter
import sys

debug = False

printTime()

# db i taules

db = 'altres'
imp = 'import'
nod = 'nodrizas'

source = 'cmbdh'

OutFile = tempFolder + 'ingressos.txt'
TableMy = 'exp_khalix_ingressos'


def sexeConv(sex):
    if sex == '0':
        return 'H'
    elif sex == '1':
        return 'D'
    else:
        return '9'

sql = "select year(data_ext),data_ext, month(data_ext), concat(year(date_add(data_ext, interval - 6 month)), date_format(date_add(data_ext, interval - 6 month), '%m'))  from dextraccio"
for a, d, m, f6m in getAll(sql, nod):
    anys = a
    dext = d
    fa6m = f6m

centres = {}
sql = "select a.up_codi_up_scs, a.up_codi_up_ics from cat_gcctb008 a inner join cat_gcctb007 b on a.up_codi_up_ics=b.up_codi_up_ics where e_p_cod_ep='0208'"
for up, br in getAll(sql, imp):
    centres[up] = br

relAgrs = defaultdict(list)
sql = "select cod_cod, concat('CCS',cod_agr) from cat_sisap_ct_cmbdh where COD_CAT = 'D'"
for cie9, ps in getAll(sql, imp):
    relAgrs[(cie9)].append(ps.upper())

ingressos = Counter()
sql = 'select eap,dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, d_ingres, d_naix, up_ass, sexe, pr_ingres from {0}{1}'.format(source, ' limit 10' if debug else '')
for eap, dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, dat, naix, upass, sex, pr in getAll(sql, imp):
    if pr == '2':
        continue
    else:
        d = str(dat)
        periodeA = d[2:-6]
        periodeM = d[5:-3]
        data = str(dat.year) + str(periodeM)
        mes = 'A' + str(periodeA) + str(periodeM)
        if data <= fa6m and (dat.year == anys or dat.year == anys -1):
            sexe = sexeConv(sex)
            try:
                ed = yearsBetween(naix, dat)
            except AttributeError:
                continue
            if ed < 0:
                ed = 0
            edat = ageConverter(ed, 5)
            dxsecundaris = {}
            try:
                br1 = centres[upass]
            except KeyError:
                br1 = 'NOICS'
            br = 'HA' + br1
            ingressos[dat.year, eap, 'DHATOTAL', mes, 'PRINCIPAL', edat, br, sexConverter(sexe)] += 1
            if (dp) in relAgrs:
                for diag in relAgrs[(dp)]:
                    ingressos[dat.year, eap, diag, mes, 'PRINCIPAL', edat, br, sexConverter(sexe)] += 1
            else:
                ok = 1
            if (ds1) in relAgrs:
                for diag in relAgrs[(ds1)]:
                    dxsecundaris[diag] = True
            if (ds2) in relAgrs:
                for diag in relAgrs[(ds2)]:
                    dxsecundaris[diag] = True
            if (ds3) in relAgrs:
                for diag in relAgrs[(ds3)]:
                    dxsecundaris[diag] = True
            if (ds4) in relAgrs:
                for diag in relAgrs[(ds4)]:
                    dxsecundaris[diag] = True
            if (ds5) in relAgrs:
                for diag in relAgrs[(ds5)]:
                    dxsecundaris[diag] = True
            if (ds6) in relAgrs:
                for diag in relAgrs[(ds6)]:
                    dxsecundaris[diag] = True
            if (ds7) in relAgrs:
                for diag in relAgrs[(ds7)]:
                    dxsecundaris[diag] = True
            if (ds8) in relAgrs:
                for diag in relAgrs[(ds8)]:
                    dxsecundaris[diag] = True
            if (ds9) in relAgrs:
                for diag in relAgrs[(ds9)]:
                    dxsecundaris[diag] = True
            for dx in dxsecundaris:
                ingressos[dat.year, eap, dx, mes, 'SECUNDARI', edat, br, sexConverter(sexe)] += 1

with openCSV(OutFile) as c:
    for (anys1, eap, diag9, mes, analisi, edat, br, sexe), d in ingressos.items():
        c.writerow([anys1, eap, diag9, mes, analisi, edat, br, sexe, d])
execute("drop table if exists {}".format(TableMy), db)
execute("create table {} (any varchar(4) not null default'', up varchar(5) not null default'', ps varchar(10) not null default'', mes varchar(10) not null default'', analisi varchar(10) not null default'', edat varchar(10) not null default'',\
    br varchar(10) not null default'', sexe varchar(10) not null default'', valor double)".format(TableMy), db)
loadData(OutFile, TableMy, db)


error = []
sql = "select  ps, mes, ics_codi, analisi, edat, br, sexe, 'N', valor from {}.{} a inner join export.khx_centres b on up=scs_codi where any ='{}'".format(db, TableMy, anys)
file = 'CMBDH_' + str(anys)
error.append(exportKhalix(sql, file))

sql = "select  ps, mes, ics_codi, analisi, edat, br, sexe, 'N', valor from {}.{} a inner join export.khx_centres b on up=scs_codi where any <>'{}'".format(db, TableMy, anys)
file = 'CMBDH_' + str(anys - 1)
error.append(exportKhalix(sql, file))


if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime()
