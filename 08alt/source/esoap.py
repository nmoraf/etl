# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "exp_khalix_esoap"
DATABASE = "altres"
FILE = "ESOAP"

COPIAR = {"EQA": {"sql": "select up, edat, indicador, conc, n \
                          from odn.exp_khalix_up_ind \
                          where indicador in {} and comb = 'NOINSAT'",
                  "ind": {("EQA9113", (0, 200)): "ESOAP0401",
                          ("EQA9104", (0, 200)): "ESOAP0402",
                          ("EQA9103", (0, 200)): "ESOAP0403",
                          ("EQA9110", (0, 200)): "ESOAP0506",
                          ("EQA9102", (0, 200)): "ESOAP0701",
                          ("EQA9117", (0, 200)): "ESOAP0702",
                          ("EQA9108", (0, 200)): "ESOAP0703"}},
          "COB": {"sql": "select uporigen, edat, 'dummy', 'NUM', num \
                          from altres.exp_khalix_pob_odn \
                          union \
                          select uporigen, edat, 'dummy', 'DEN', den \
                          from altres.exp_khalix_pob_odn",
                  "ind": {("dummy", (0, 14)): "ESOAP0101A",
                          ("dummy", (15, 200)): "ESOAP0101B"}},
          "PRODO": {"sql": "select up, edat, indicador, 'NUM', n \
                            from altres.mst_prestacions_odn_up \
                            where indicador in {}",
                    "ind": {("PRODOA0001", (5, 14)): "ESOAP0501",
                            ("PRODOA0002", (5, 14)): "ESOAP0502",
                            ("PRODOA0003", (0, 14)): "ESOAP0601",
                            ("PRODOA0004", (0, 14)): "ESOAP0601",
                            ("PRODOA0003", (15, 200)): "ESOAP0602",
                            ("PRODOA0004", (15, 200)): "ESOAP0602"}}}


class ESOAP(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.get_cataleg()
        self.get_periode()
        self.get_copiar()
        self.get_denominadors()
        self.get_accessibilitat()
        self.get_longitudinalitat()
        self.get_dbs()
        self.export_khalix()

    def create_table(self):
        """."""
        cols = "(up varchar(5), indicador varchar(10), poblacio varchar(10), \
                 analisi varchar(3), periode varchar(5), recompte int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)

    def get_cataleg(self):
        """."""
        self.cataleg = {}
        self.cataleg_invers = c.defaultdict(set)
        sql = "select distinct uporigen, up from cat_linies"
        for uporigen, up in u.getAll(sql, "nodrizas"):
            self.cataleg[uporigen] = up
            self.cataleg_invers[up].add(uporigen)

    def get_periode(self):
        """."""
        sql = "select data_ext from dextraccio"
        self.dextraccio = u.getOne(sql, "nodrizas")[0]
        self.periode = "A{}".format(self.dextraccio.strftime("%y%m"))

    def _get_conversio(self, up, edat):
        """."""
        if edat in ("EC01", "EC24", "EC59", "EC1014"):
            return self.cataleg.get(up, up)
        else:
            return up

    def get_copiar(self):
        """."""
        resultat = c.Counter()
        self.poblacio = c.Counter()
        for domini, dades in COPIAR.items():
            conv = {}
            for (old, edats), new in dades["ind"].items():
                for edat in range(edats[0], edats[1] + 1):
                    conv[(old, u.ageConverter(edat))] = new
            codis = (old for (old, edat) in conv)
            sql = dades["sql"].format(tuple(codis))
            for up, edat, ind, an, n in u.getAll(sql, "import"):
                if (ind, edat) in conv:
                    new_ind = conv[(ind, edat)]
                    new_up = self._get_conversio(up, edat)
                    resultat[(up, new_ind, "ODOORIG", an)] += n
                    resultat[(new_up, new_ind, "ODOASSIG", an)] += n
                if domini == "COB" and an == "DEN":
                    self.poblacio[(up, "ODOORIG", edat)] += n
                    self.poblacio[(self._get_conversio(up, edat), "ODOASSIG", edat)] += n  # noqa
        upload = [k + (self.periode, v) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_denominadors(self):
        """."""
        entren = c.defaultdict(set)
        for (old, edats), new in COPIAR["PRODO"]["ind"].items():
            for edat in range(edats[0], edats[1] + 1):
                entren[u.ageConverter(edat)].add(new)
        resultat = c.Counter()
        for (up, pob, edat), n in self.poblacio.items():
            for ind in entren[edat]:
                resultat[(up, ind, pob, "DEN")] += n
        upload = [k + (self.periode, v) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_accessibilitat(self):
        """."""
        indicadors = {"ACC2D": "ESOAP0201A", "ACC5D": "ESOAP0201B",
                      "ACC10D": "ESOAP0201C"}
        centres = {row[0]: row[1] for row
                   in u.getAll("select ics_codi, scs_codi from cat_centres",
                               "nodrizas")}
        sql = "select d2, d0, d3, d1, n from exp_khalix_forats \
               where d5 = 'TIPPROF3' and \
                     d1 = '{}' and \
                     d0 in {}".format(self.periode, tuple(indicadors.keys()))
        resultat = c.Counter()
        for br, _ind, analisi, periode, n in u.getAll(sql, "altres"):
            up = centres[br]
            ind = indicadors[_ind]
            keys = [(up, ind, "ODOASSIG", analisi, periode)]
            if up in self.cataleg_invers:
                keys.extend([(orig, ind, "ODOORIG", analisi, periode)
                             for orig in self.cataleg_invers[up]])
            else:
                keys.append((up, ind, "ODOORIG", analisi, periode))
            for key in keys:
                resultat[key] += n
        upload = [k + (v,) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_longitudinalitat(self):
        """."""
        indicador = "ESOAP0301"
        centres = {row[0]: row[1] for row
                   in u.getAll("select ics_codi, scs_codi from cat_centres",
                               "nodrizas")}
        sql = "select up, analisi, periode, resultat \
               from exp_long_cont_up \
               where indicador = 'CONT0002' and detalle = 'TIPPROF3'"
        resultat = c.Counter()
        for br, analisi, periode, n in u.getAll(sql, "altres"):
            up = centres[br]
            keys = [(up, indicador, "ODOASSIG", analisi, periode)]
            if up in self.cataleg_invers:
                keys.extend([(orig, indicador, "ODOORIG", analisi, periode)
                             for orig in self.cataleg_invers[up]])
            else:
                keys.append((up, indicador, "ODOORIG", analisi, periode))
            for key in keys:
                resultat[key] += n
        upload = [k + (v,) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_dbs(self):
        """."""
        resultat = c.Counter()
        sql = "select c_up, c_up_origen, c_edat_anys, \
                      decode(oe_ca_def, null, 0, 1),\
                      decode(oe_ca_temp, null, 0, 1), oc_risc, ot_fluor \
               from dbs \
               where c_cip like '{}%' and c_edat_anys < 15"
        jobs = [sql.format(format(digit, 'x').upper()) for digit in range(16)]
        dades = u.multiprocess(_worker, jobs)
        for chunk in dades:
            for up_a, up_o, edat, ca_d, ca_t, risc, fluor in chunk:
                keys = []
                ind = "05" if risc == 1 else "07"
                keys.append((up_a, "ESOAP05" + ind, "ODOASSIG", "DEN"))
                keys.append((up_o, "ESOAP05" + ind, "ODOORIG", "DEN"))
                if fluor and u.yearsBetween(fluor, self.dextraccio) == 0:
                    keys.append((up_a, "ESOAP05" + ind, "ODOASSIG", "NUM"))
                    keys.append((up_o, "ESOAP05" + ind, "ODOORIG", "NUM"))
                if edat >= 6:
                    keys.append((up_a, "ESOAP0503", "ODOASSIG", "DEN"))
                    keys.append((up_o, "ESOAP0503", "ODOORIG", "DEN"))
                    if risc == 1:
                        keys.append((up_a, "ESOAP0503", "ODOASSIG", "NUM"))
                        keys.append((up_o, "ESOAP0503", "ODOORIG", "NUM"))
                if edat in (7, 12):
                    subi = "A" if edat == 7 else "B"
                    keys.append((up_a, "ESOAP0504" + subi, "ODOASSIG", "DEN"))
                    keys.append((up_o, "ESOAP0504" + subi, "ODOORIG", "DEN"))
                    if ca_d or ca_t:
                        keys.append((up_a, "ESOAP0504" + subi, "ODOASSIG", "NUM"))  # noqa
                        keys.append((up_o, "ESOAP0504" + subi, "ODOORIG", "NUM"))  # noqa
                for key in keys:
                    resultat[key] += 1
        upload = [k + (self.periode, v) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        sql = "select indicador, periode, ics_codi, analisi, \
                      'NOCAT', poblacio, 'DIM6SET', 'N', recompte \
               from {}.{} a \
               inner join nodrizas.cat_centres b \
                    on a.up = b.scs_codi".format(DATABASE, TABLE)
        u.exportKhalix(sql, FILE)


def _worker(sql):
    return [row for row in u.getAll(sql, "redics")]


if __name__ == "__main__":
    if u.IS_MENSUAL:
        ESOAP()
