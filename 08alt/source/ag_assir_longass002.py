# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


INDICADOR = "LONGASS002"
TABLE = "exp_khalix_{}_up".format(INDICADOR)
DATABASE = "altres"


class Indicador(object):
    """."""

    def __init__(self):
        """."""
        self.get_visites()
        self.get_pacients()
        self.get_indicador()
        self.export()

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(set)
        sql = "select id_cip_sec, pavi_emb_cod, pavi_n_col \
               from assir221, nodrizas.dextraccio \
               where pavi_seg_tipus in ('PPD', 'PSD', 'PPC', 'PSC') and \
                     left(pavi_n_col, 1) = '3' and \
                     pavi_data_a between adddate(data_ext, interval -1 year) and data_ext"  # noqa
        for id, emb, col in u.getAll(sql, "import"):
            self.visites[(id, emb)].add(col)

    def get_pacients(self):
        """."""
        self.pacients = c.defaultdict(set)
        sql = "select id_cip_sec, visi_up from ass_imputacio_up"
        for id, up in u.getAll(sql, "nodrizas"):
            self.pacients[id].add(up)

    def get_indicador(self):
        """."""
        self.indicador = c.defaultdict(c.Counter)
        for (id, emb), cols in self.visites.items():
            if id in self.pacients:
                for up in self.pacients[id]:
                    self.indicador[up]["DEN"] += 1
                    if len(cols) < 3:
                        self.indicador[up]["NUM"] += 1

    def export(self):
        """."""
        cols = "(up varchar(5), indicador varchar(10), \
                 numerador int, denominador int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        upload = [(up, INDICADOR, v["NUM"], v["DEN"])
                  for (up, v) in self.indicador.items()]
        u.listToTable(upload, TABLE, DATABASE)


if __name__ == "__main__":
    Indicador()
