# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


class Grupal(object):
    """."""

    def __init__(self):
        """."""
        self.get_dates()
        self.get_centres()
        self.get_sessions()
        self.resultat = c.Counter()
        self.get_participants()
        self.get_activitats()
        self.export_klx()

    def get_dates(self):
        """."""
        sql = "select date_format(date_add(date_add( \
                   data_ext, interval -1 year), interval +1 day), '%Y%m%d'), \
                   date_format(data_ext, '%Y%m%d') from dextraccio"
        self.dates = u.getOne(sql, 'nodrizas')

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_sessions(self):
        """."""
        sql = "select codi_sector, sgru_num_grup \
               from grupal5 \
               where sgru_data between {} and {}".format(*self.dates)
        self.sessions = set([row for row in u.getAll(sql, 'import')])

    def get_participants(self):
        """."""
        sql = "select id_cip_sec, codi_sector, pagr_num_grup, pagr_up \
               from grupal3 \
               where pagr_data_alta between {} and {}".format(*self.dates)
        for pac, sector, numero, up in u.getAll(sql, 'import'):
            id = (sector, numero)
            if id in self.sessions and up in self.centres:
                br = self.centres[up]
                self.resultat[(br, 'AGC0201')] += 1

    def get_activitats(self):
        """."""
        map_act = {'SE': 'PJ', 'OTR': 'AL', 'SB': 'PG', 'PR': 'PG',
                   'CO': 'MC', 'CA': 'PG'}
        map_tem = {'PS': 'AL', 'VA': 'AL', 'OTR': 'AL', 'II': 'AL',
                   'CR': 'AL', 'CO': 'AL'}
        keys_tip = {'C': '03', 'G': '01', 'GC': '02'}
        keys_act = {'SI': '01', 'TA': '02', 'PJ': '03', 'PG': '04',
                    'AL': '05', 'SE': '06', '': '07', 'ST': '08',
                    'MC': '09', 'SB': '10', 'PR': '11', 'CO': '12',
                    'CA': '13'}
        keys_tem = {'AF': '01', 'MC': '02', 'SE': '03', 'SM': '04',
                    'ES': '05', 'MI': '06', 'PS': '07', 'AL': '08',
                    'VA': '09', '': '10', 'TA': '11', 'SB': '12',
                    'CU': '13', 'VI': '14', 'RE': '15', 'SS': '16',
                    'II': '17', 'CR': '18', 'CO': '19', 'S2': '20',
                    'S3': '21', 'S1': '22', 'S4': '23', 'S5': '24'}
        sql = "select codi_sector, grup_num, grup_codi_up, grup_tipus, \
               grup_hora_ini, grup_hora_fi, grup_num_sessions, \
               grup_tipus_activitat, grup_activitat \
               from grupal4 \
               where grup_data_ini between {} and {} \
               and grup_validada = 'S'".format(*self.dates)
        for sec, num, up, tip, ini, fi, n, act, tem in u.getAll(sql, 'import'):
            id = (sec, num)
            if up in self.centres and (tip == 'C' or id in self.sessions):
                br = self.centres[up]
                if not n:
                    n = 1
                if ini and fi:
                    hores = n * ((fi - ini) / 3600.0)
                else:
                    hores = 0
                act = map_act.get(act, act)
                tem = map_tem.get(tem, tem)
                self.resultat[(br, 'AGC01')] += 1
                self.resultat[(br, 'AGC01{}'.format(keys_tip[tip]))] += 1
                self.resultat[(br, 'AGC04{}'.format(keys_act[act]))] += 1
                self.resultat[(br, 'AGC05{}'.format(keys_tem[tem]))] += 1
                self.resultat[(br, 'AGC03')] += hores
                self.resultat[(br, 'AGC03{}'.format(keys_tip[tip]))] += hores
                self.resultat[(br, 'AGC06{}'.format(keys_act[act]))] += hores
                self.resultat[(br, 'AGC07{}'.format(keys_tem[tem]))] += hores

    def export_klx(self):
        """."""
        tb = 'exp_khalix_grupal_comunitaria'
        db = 'altres'
        file = 'GRUPAL_COMUNITARIA'
        u.createTable(tb, '(br varchar(5), ind varchar(10), n double)',
                      db, rm=True)
        upload = [(br, ind, n) for (br, ind), n in self.resultat.items()]
        u.listToTable(upload, tb, db)
        sql = "select ind, 'Aperiodo', br, 'NUM', 'NOCAT', \
               'NOIMP', 'DIM6SET', 'N', n from {}.{}".format(db, tb)
        u.exportKhalix(sql, file)


if __name__ == '__main__':
    Grupal()
