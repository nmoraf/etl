from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random

nod="nodrizas"
imp="import"
limit=""
alt='altres'
agrupadores=(826,825)
xml_codis=("XML0000014","XML0000013")
table_name="exp_khalix_presc_soc"
file="PRESCRIPCIO_SOCIAL"

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_print_format(obj):
    if type(obj) == list or type(obj) == tuple:
        list_obj=obj
    else:
        list_obj=[obj]
    return ",".join("'{0}'".format(w) for w in list_obj)

def get_ps_ult_any_per_pacient(current_date):
    """Conseguir el numero de prescripciones sociales del ultimo any. Busca pacientes con prescripciones que no esten cerradas por baja logica del ultimo any. Si seguiment=True,
       construye un diccionario con las keys como ids y el valor el numero de prescripciones con seguimiento.
       Devuelve un diccionario de paciente id como key y valor el numero de prescripciones sociales que tiene dadas de alta.
    """
    ps_dict=Counter()
    ps_seguiment=Counter()
    ps_no_seg=defaultdict(Counter)

    sql="select id_cip_sec,so_opcio,so_seguiment,so_data_alta from {}.presc_social".format(imp)
    for id,so_opt,so_seg,data_alta in getAll(sql,imp):
        if monthsBetween(data_alta,current_date)<12:
            ps_dict[id]+=1
            if so_seg=="S":
                ps_seguiment[id]+=1
            if so_seg=='N':
                ps_no_seg[so_opt][id]+=1
    return ps_dict,ps_seguiment,ps_no_seg


def get_test_fet_by_agr(current_date):
    """Consigue los id de los pacientes que tienen hecho un test en los ultimos 12 meses a traves del agrupador correspondiente (agr)
       Devuelve un set de ids.   
    """
    test_fet=defaultdict(set)
    diferencies_test=defaultdict(lambda: defaultdict(dict))
    sql="select id_cip_sec,agrupador,data_var,valor,usar from {}.eqa_variables where agrupador in {} {};".format(nod,agrupadores,limit)
    for id,agr,data_var,valor,usar in getAll(sql,nod):
        if monthsBetween(data_var,current_date) <12:
            test_fet[agr].add(id)
            diferencies_test[agr][id][int(usar)]=valor
    return test_fet,diferencies_test


def get_test_fet_by_xml(current_date):
    """Consigue los id de los pacientes que tienen hecho un test en los ultimos 12 meses a traves del codi xml
       Devuelve un set de ids.   
    """
    test_fet=defaultdict(set)
    sql="select id_cip_sec,xml_tipus_orig,xml_data_alta from {}.xml where xml_origen='GABINETS' and xml_tipus_orig in {} {}".format(imp,xml_codis,limit)
    for id,xml_codi,data_var in getAll(sql,imp):
        if monthsBetween(data_var,current_date) <12:
            test_fet[xml_codi].add(id)
    return test_fet

#Testear estas lineas, si estan bien aplicar cambios al resto de poblacio_at
def get_indicador_pacient(denominador,numerador,codi_indicador):
    """Construye las filas de la tabla agrupando por up a nivel de paciente"""
    rows=[]
    indicador=defaultdict(Counter)

    for id in poblacio_at:
        up=poblacio_at[id][0]
        sexe=poblacio_at[id][1]
        if id in denominador:
            indicador[up, sexe]["DEN"]+=1
            if id in numerador:
                indicador[up, sexe]["NUM"]+=1
    
    rows= [(up, sexe, codi_indicador, indicador[up, sexe]["NUM"], indicador[up, sexe]["DEN"])
                          for up, sexe in indicador]   
    return rows
#Fin de las lineas de test

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)




def get_indicador_1(numerador,codi_indicador):
    """Construye las filas de la tabla.
       El denominador es el numero de pacientes asignados y atendidos en el ultimo any por up -> id de nodrizas.assignada_tot
       El numerador es el numero de pacientes con prescripcion social en el ultimo any -> si el id esta en ps_12meses (se le ha hecho alguna prescripcion el los ultimos 12 meses)
    """
    rows=[]
    indicador=defaultdict(Counter)
    
    for id in poblacio_at:
        up=poblacio_at[id][0]
        sexe=poblacio_at[id][1]
        indicador[up, sexe]["DEN"]+=1
        if id in numerador:
            indicador[up, sexe]["NUM"]+=1
            
    
    rows= [(up, sexe, codi_indicador, indicador[up, sexe]["NUM"], indicador[up, sexe]["DEN"])
                          for up, sexe in indicador]   
    return rows

def get_indicador_2(denominador,numerador,codi):
    """Construye las filas de la tabla.
       El denominador es el numero de prescripciones por up -> el valor de ps_12meses[id], el numero de prescripciones del ultimo any para un id de una up determinada
       El numerador es el numero de prescripciones con seguimiento -> el valor de ps_seg[id], el numero de prescripciones con seg del ultimo any para un id de una up determinada
    """
    rows=[]
    indicador=defaultdict(Counter)

    for id in poblacio_at:
        up=poblacio_at[id][0]
        sexe=poblacio_at[id][1]
        if id in denominador:
            indicador[up, sexe]["DEN"]+=denominador[id]
            if id in numerador:
                indicador[up, sexe]["NUM"]+=numerador[id]
    
    rows= [(up, sexe, codi, indicador[up, sexe]["NUM"], indicador[up, sexe]["DEN"])
                          for up, sexe in indicador]   
    return rows

def get_indicador_diferencies(denominador,numerador,codi_indicador):
    """ Construye las filas de la tabla output agrupados por up.
        El denominador es el numero de pacientes con prescripcion social en seguimiento con mas de un test hecho que hay en una up -> si el paciente esta en ps_seg, esta en diferencies_test y la
        longitud de este diccionario diferencies_test[id] es mayor que 1 (ha hecho mas de un test) 
        El numerador es la diferencia de la calificacion del test mas reciente y el mas antiguo -> la diferencia de los valores del diccionario diferencies_test[id][usar=1] y diferencies_test[id][usar=2].
    
    """
    indicador=defaultdict(Counter)

    for id in poblacio_at:
        up=poblacio_at[id][0]
        sexe=poblacio_at[id][1]
        if id in denominador:
            if id in numerador and len(numerador[id]) > 1:
                indicador[up, sexe]["DEN"]+=1
                if (numerador[id][1] - numerador[id][2]) >0:
                    indicador[up, sexe]["NUM"]+= 1

    rows= [(up, sexe, codi_indicador, indicador[up, sexe]["NUM"], indicador[up, sexe]["DEN"])
                          for up, sexe in indicador]   
    return rows

def get_indicador_valor(denominador, numerador, codi):
    """."""
    indicador=defaultdict(Counter)
    for id in poblacio_at:
        up=poblacio_at[id][0]
        sexe=poblacio_at[id][1]
        if id in denominador and id in ps_12meses:
            indicador[up, sexe]["DEN"]+=1
            last = numerador[0][id][1]
            min = numerador[1][0]
            max = numerador[1][1]
            if min <= last <= max:
                indicador[up, sexe]["NUM"]+=1
    
    rows= [(up, sexe, codi, indicador[up, sexe]["NUM"], indicador[up, sexe]["DEN"]) for up, sexe in indicador]
    return rows

def get_indicador_func(func,codis):
    for codi in codis:
        denominador,numerador=codis[codi]
        rows=func(denominador,numerador,codi)
        printTime('Retrieving rows {} OK'.format(codi))
        listToTable(rows,table_name,alt)


if __name__ == '__main__':
    printTime('inici')
    current_date=get_date_dextraccio()
    printTime("Dia actual")

    
    columns="(up varchar(5), sexe varchar(2), indicador varchar(11),numerador int, denominador int)"
    createTable(table_name,columns,alt,rm=True)
    

    ps_12meses,ps_seg,ps_no_seg=get_ps_ult_any_per_pacient(current_date)
    printTime("ps_12meses")

    test_variables,tests_valor= get_test_fet_by_agr(current_date)
    printTime("Tests agr")

    # test_xml=get_test_fet_by_xml(current_date)
    printTime("Tests XML codis")
   
    test_total= (set.intersection(*[test_variables[agr] for agr in agrupadores]))
    print(random.sample(test_total,5))

    
    ps_no_seg_den=Counter()
    for so_opt in ps_no_seg:
        for id in ps_no_seg[so_opt]:
            ps_no_seg_den[id]+=ps_no_seg[so_opt][id]

    poblacio_at = {id: (up, sexe) for (id, up, sexe) in getAll("select id_cip_sec, up, sexe from assignada_tot where ates = 1", "nodrizas")}
    print(random.sample(poblacio_at,5))
    #codi1
    codi_indicador="PRSIS01"
    rows=get_indicador_1(ps_12meses,codi_indicador)
    printTime('Retrieving rows new table OK')
    listToTable(rows,table_name,alt)

    #Resto de codis
    codis_indicador_count={
        "PRSIS02": (ps_12meses,ps_seg),
        "PRSIS10a":(ps_no_seg_den,ps_no_seg['P']),
        "PRSIS10b":(ps_no_seg_den,ps_no_seg['A'])
    }
    
    codis_indicador_pac={
        "PRSIS04": (ps_12meses, test_variables[826]),
        "PRSIS05": (ps_12meses, test_variables[825]),
        "PRSIS03": (ps_12meses, test_total)
    } 

    codis_indicador_dif={

        "PRSIS06": (ps_seg,tests_valor[826]),
        "PRSIS07": (ps_seg,tests_valor[825]),
    }
    
    codis_indicador_valor = {"PRSIS04A": (test_variables[826], (tests_valor[826], (0, 25))),
                             "PRSIS04B": (test_variables[826], (tests_valor[826], (26, 999))),
                             "PRSIS05A": (test_variables[825], (tests_valor[825], (0, 8))),
                             "PRSIS05B": (test_variables[825], (tests_valor[825], (9, 11))),
                             "PRSIS05C": (test_variables[825], (tests_valor[825], (12, 999)))}

    for funcc, codis_dict in zip ([get_indicador_pacient,
                                   get_indicador_diferencies,
                                   get_indicador_2,
                                   get_indicador_valor],
                                   [codis_indicador_pac,
                                   codis_indicador_dif,
                                   codis_indicador_count,
                                   codis_indicador_valor]):
        
        get_indicador_func(funcc,codis_dict)

    query_template_string="select indicador,concat('A','periodo'),ics_codi,{strg},'NOCAT','NOIMP','DIM6SET','N',{var} from altres.{taula} a inner join export.khx_centres b on a.up=scs_codi where a.{var} != 0"
    query_string=query_template_string.format(strg="'NUM'",var="numerador",taula=table_name)+" union "+ query_template_string.format(strg="'DEN'",var="denominador",taula=table_name)
    exportKhalix(query_string,file)
				
	
	