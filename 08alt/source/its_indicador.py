# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod="nodrizas"
imp="import"
db="altres"

printTime('Inici')
sql='select data_ext from dextraccio'
for d in getOne(sql, nod):
    dext=d

OutFile = tempFolder + 'ITS_VIH.txt'
codiIndicador = 'ITS03'

altres_indicadors = {'ITS04': {
                                'tip': 'nousVIH',
                                'calcular': True,
                                'problemes': '(647, 648, 649)',
                                'serologia': '650',
                                'vacunes': False,
                             },
                    'ITS05':  {
                                'tip': 'nousVIH',
                                'calcular': True,
                                'problemes': '(651)',
                                'serologia': '652',
                                'vacunes': False,
                             }, 
                    'ITS06':  {
                                'tip': 'nousVIH',
                                'calcular': True,
                                'problemes': '(653)',
                                'serologia': '654',
                                'vacunes': False,
                             },  
                    'ITS07':  {
                                'tip': 'nousVIH',
                                'calcular': True,
                                'problemes': '(13,14)',
                                'serologia': '655',
                                'vacunes': '15',
                             }, 
                    'ITS08A':  {
                                'tip': 'nousITS',
                                'calcular': True,
                                'problemes': '(550, 553, 12, 29, 13, 14, 752, 753, 754, 755)',
                                'serologia': '388',
                                'excl': [('problemes', ['101', 'res'], 17, 1000)]
                             },  
                    'ITS08B':  {
                                'tip': 'nousITS',
                                'calcular': True,
                                'problemes': '(550, 553, 12, 29, 13, 14, 752, 753, 754, 755, 757, 758, 406, 680)',
                                'serologia': '388',
                                'excl': [('problemes', ['101', 'res'], 17, 1000)]
                             },  
                     'ITS09A':  {
                                'tip': 'nousITS',
                                'calcular': True,
                                'problemes': '(550, 553, 12, 29, 13, 14, 752, 753, 754, 755)',
                                'serologia': False,
                                'problemesNum': '(101)',
                                'excl': [('problemes', ['101', 'res'], 12, 1000)]
                             }, 
                     'ITS09B':  {
                                'tip': 'nousITS',
                                'calcular': True,
                                'problemes': '(550, 553, 12, 29, 13, 14, 752, 753, 754, 755, 757, 758, 406, 680)',
                                'serologia': False,
                                'problemesNum': '(101)',
                                'excl': [('problemes', ['101', 'res'], 12, 1000)]
                             }, 
                    }
  
origens_exclusions = {'problemes': [('nodrizas', 'eqa_problemes', 'ps', 'dde')]}
 


indicadors_inc_prev = {'ITS0100': {
                                    'tip': 'I',
                                    'ps': '(406, 101, 680, 681, 682)'
                                    },
                        'ITS0101': {
                                    'tip': 'I',
                                    'ps': '(666, 681)'
                                    },
                        'ITS0102': {
                                    'tip': 'I',
                                    'ps': '(667, 682)'
                                    },
                        'ITS0103': {
                                    'tip': 'I',
                                    'ps': '(647, 683)'
                                    },
                        'ITS0104': {
                                    'tip': 'I',
                                    'ps': '(648, 684)'
                                    },
                        'ITS0105': {
                                    'tip': 'I',
                                    'ps': '(649, 685)'
                                    },
                        'ITS0106': {
                                    'tip': 'I',
                                    'ps': '(651,  686)'
                                    },
                        'ITS0107': {
                                    'tip': 'I',
                                    'ps': '(653, 687)'
                                    },
                        'ITS0108': {
                                    'tip': 'I',
                                    'ps': '(668, 688)'
                                    },            
                        'ITS0109': {
                                    'tip': 'I',
                                    'ps': '(669, 698)'
                                    },
                        'ITS0110': {
                                    'tip': 'I',
                                    'ps': '(495, 690)'
                                    },
                        'ITS0111': {
                                    'tip': 'I',
                                    'ps': '(13, 14)'
                                    },
                        'ITS0112': {
                                    'tip': 'I',
                                    'ps': '(670, 691)'
                                    },
                        'ITS0113': {
                                    'tip': 'I',
                                    'ps': '(671, 692)'
                                    },
                        'ITS0200': {
                                    'tip': 'P',
                                    'ps': '(406, 101)'
                                    },
                        'ITS0201': {
                                    'tip': 'P',
                                    'ps': '(666)'
                                    },
                        'ITS0202': {
                                    'tip': 'P',
                                    'ps': '(667)'
                                    },
                        'ITS0203': {
                                    'tip': 'P',
                                    'ps': '(647)'
                                    },
                        'ITS0204': {
                                    'tip': 'P',
                                    'ps': '(648)'
                                    },
                        'ITS0205': {
                                    'tip': 'P',
                                    'ps': '(649)'
                                    },
                        'ITS0206': {
                                    'tip': 'P',
                                    'ps': '(651)'
                                    },
                        'ITS0207': {
                                    'tip': 'P',
                                    'ps': '(653)'
                                    },
                        'ITS0208': {
                                    'tip': 'P',
                                    'ps': '(668)'
                                    },            
                        'ITS0209': {
                                    'tip': 'P',
                                    'ps': '(669)'
                                    },
                        'ITS0210': {
                                    'tip': 'P',
                                    'ps': '(495)'
                                    },
                        'ITS0211': {
                                    'tip': 'P',
                                    'ps': '(13)'
                                    },
                    }
                    
assig= {}
sql= "select id_cip_sec,up,edat,sexe,if(institucionalitzat=1,'INS','NOINS'),ates from assignada_tot_with_jail"
for id_cip_sec,up,edat,sexe,insti, ates in getAll(sql,nod):
    if edat > 14:
        assig[id_cip_sec]= {'up':up, 'edat':ageConverter(edat,5),'sexe':sexConverter(sexe),'insti':insti, 'ates': ates}                         

nous_dxVIH = {}
sql  = 'select id_cip_sec, dde from eqa_problemes where ps = 101'
for id, dde in getAll(sql, nod):
    b = monthsBetween(dde,dext)
    if 0 <= b <= 11:
        nous_dxVIH[id] = {'dde': dde}

ResultatsVIH = Counter()
ResultatsPacient = {}
for ind in altres_indicadors:
    critspr = []
    tip = altres_indicadors[ind]['tip']
    pr = altres_indicadors[ind]['problemes']
    seros = altres_indicadors[ind]['serologia']
    nums = {}
    if tip == 'nousVIH':
        sql = "select id_cip_sec, dde, ps from eqa_problemes_incid where ps in {}".format(pr)
        for id, dat, ps in getAll(sql, nod):
            num = 0
            if id in nous_dxVIH:
                dde = nous_dxVIH[id]['dde']
                b = monthsBetween(dde,dat)
                if ps == 13 or ps == 14:
                    num = 1
                else:
                    if -6<= b <= 6:
                        num = 1
                if id in nums:
                    nums[id] += num
                else:
                    nums[id] = num
        sql = 'select id_cip_sec, data_var from eqa_variables where agrupador ={}'.format(seros)
        for id, dat in getAll(sql, nod):
            num = 0
            if id in nous_dxVIH:
                dde = nous_dxVIH[id]['dde']
                b = monthsBetween(dde,dat)
                if -6<= b <= 6:
                    num = 1
                if id in nums:
                    nums[id] += num
                else:
                    nums[id] = num
        if altres_indicadors[ind]['vacunes']:
            vac = altres_indicadors[ind]['vacunes']
            sql = 'select id_cip_sec from eqa_vacunes where agrupador={}'.format(vac)
            for id, in getAll(sql, nod):
                if id in nums:
                    nums[id] += 1
                else:
                    nums[id] = 1
        for id, date in nous_dxVIH.items():
            try:
                up = assig[id]['up']
                edat = assig[id]['edat']
                sexe = assig[id]['sexe']
                insti = assig[id]['insti']
                ates = assig[id]['ates']
            except KeyError:
                continue 
            num, excl = 0, 0
            if id in nums:
                nume = nums[id]
                if nume > 0:
                    num = 1
            if excl == 0:
                comb = insti + 'ASS'
                ResultatsVIH[(ind,up,edat,sexe,comb,'DEN')] += 1
                if num > 0:
                    ResultatsVIH[(ind,up,edat,sexe,comb,'NUM')] += 1
                if ates == 1:
                    comb = insti + 'AT'
                    ResultatsVIH[(ind,up,edat,sexe,comb,'DEN')] += 1
                    if num > 0:
                        ResultatsVIH[(ind,up,edat,sexe,comb,'NUM')] += 1
                    if insti == 'NOINS':
                        ResultatsPacient[(id, ind)] = {'num': num, 'den': 1, 'up': up}
    else:
        nous_dxITS, exclusions = {}, {}
        sql  = 'select id_cip_sec, dde from eqa_problemes_incid where ps in {}'.format(pr)
        for id, dde in getAll(sql, nod):
            b = monthsBetween(dde,dext)
            if 0 <= b <= 11:
                nous_dxITS[id] = {'dde': dde}
        if seros:
            sql = 'select id_cip_sec, data_var from eqa_variables where agrupador ={}'.format(seros)
            for id, dat in getAll(sql, nod):
                num = 0
                if id in nous_dxITS:
                    dde = nous_dxITS[id]['dde']
                    b = monthsBetween(dde,dat)
                    if -6<= b <= 6:
                        num = 1
                    if id in nums:
                        nums[id] += num
                    else:
                        nums[id] = num
        try:
            prNUM = altres_indicadors[ind]['problemesNum']
        except KeyError:
            prNUM = False
        if prNUM:
            sql  = 'select id_cip_sec, dde from eqa_problemes_incid where ps in {}'.format(prNUM)
            for id, dde in getAll(sql, nod):
                num = 0
                if id in nous_dxITS:
                    b = monthsBetween(dde,dext)
                    if 0 <= b <= 11:
                        num = 1
                    if id in nums:
                        nums[id] += num
                    else:
                        nums[id] = num
        excloure = altres_indicadors[ind]['excl']
        if excloure:
            for taula, codis, minimt, maximt in excloure:
                origen = origens_exclusions[taula]
                for bbdd, tab, where, column in origen:
                    sql = 'select id_cip_sec, {0} from {1} where {2} in {3}'.format(column, tab, where, tuple(codis))
                    for id, dat in getAll(sql, bbdd):
                        b = monthsBetween(dat, dext)
                        if minimt <= b <= maximt:
                            exclusions[id] = True
        for id, date in nous_dxITS.items():
            try:
                up = assig[id]['up']
                edat = assig[id]['edat']
                sexe = assig[id]['sexe']
                insti = assig[id]['insti']
                ates = assig[id]['ates']
            except KeyError:
                continue 
            num, excl = 0, 0
            if id in nums:
                nume = nums[id]
                if nume > 0:
                    num = 1
            if id in exclusions:
                excl = 1
            if excl == 0:
                comb = insti + 'ASS'
                ResultatsVIH[(ind,up,edat,sexe,comb,'DEN')] += 1
                if num > 0:
                    ResultatsVIH[(ind,up,edat,sexe,comb,'NUM')] += 1
                if ates == 1:
                    comb = insti + 'AT'
                    ResultatsVIH[(ind,up,edat,sexe,comb,'DEN')] += 1
                    if num > 0:
                        ResultatsVIH[(ind,up,edat,sexe,comb,'NUM')] += 1
                    if insti == 'NOINS':
                        ResultatsPacient[(id, ind)] = {'num': num, 'den': 1, 'up': up}            
   
nousVIH = {}
sql = 'select id_cip_sec,dde from eqa_problemes_incid where ps in (406,680)'
for id,dde in getAll(sql,nod):
    b = monthsBetween(dde,dext)
    if 0 <= b <= 11:
        nousVIH[id] = {'num':0,'excl':0,'dde':dde}
        
sql = "select id_cip_sec,val_var,val_val,val_data from assir216,nodrizas.dextraccio where val_var='PAB003' and val_data <= data_ext"
for id,agr,valor,data in getAll(sql,imp):
    try:
        if id in nousVIH:
            dde = nousVIH[id]['dde']
            b = monthsBetween(data,dde)
            if -23<= b <= 23:
                nousVIH[id]['num'] = 1
    except KeyError:
        continue
sql = "select id_cip_sec,agrupador,valor,data_var from eqa_variables,nodrizas.dextraccio where agrupador in ('398','388','644') and data_var <= data_ext"
for id,agr,valor,data in getAll(sql,nod):
    if agr == 398 and valor == 1:
        try:
            if id in nousVIH:
                nousVIH[id]['num'] = 1
        except KeyError:
            continue
    if agr in (388, 644):
        try:
            if id in nousVIH:
                dde = nousVIH[id]['dde']
                b = monthsBetween(dde,data)
                if -6<= b <= 6:
                    nousVIH[id]['num'] = 1
        except KeyError:
            continue

sql = 'select id_cip_sec from eqa_problemes where ps=101'
for id, in getAll(sql,nod):
    b = monthsBetween(dde,dext)
    try:
        if id in nousVIH:
            nousVIH[id]['num'] = 1
    except KeyError:
        continue


for (id),count in nousVIH.items():
    try:
        up = assig[id]['up']
        edat = assig[id]['edat']
        sexe = assig[id]['sexe']
        insti = assig[id]['insti']
        ates = assig[id]['ates']
    except KeyError:
        continue
    num = count['num']
    excl = count['excl']
    if excl == 0:
        comb = insti + 'ASS'
        ResultatsVIH[(codiIndicador,up,edat,sexe,comb,'DEN')] += 1
        if num > 0:
            ResultatsVIH[(codiIndicador,up,edat,sexe,comb,'NUM')] += 1
        if ates == 1:
            comb = insti + 'AT'
            ResultatsVIH[(codiIndicador,up,edat,sexe,comb,'DEN')] += 1
            if num > 0:
                ResultatsVIH[(codiIndicador,up,edat,sexe,comb,'NUM')] += 1        
            if insti == 'NOINS':
                ResultatsPacient[(id, codiIndicador)] = {'num': num, 'den': 1, 'up': up}

for id, vals in assig.items():
    ates = vals['ates']
    insti = vals['insti']
    for indic in indicadors_inc_prev:
        comb = insti + 'ASS'
        ResultatsVIH[(indic, vals['up'],vals['edat'],vals['sexe'],comb,'DEN')] += 1
        if ates == 1:
            comb = insti + 'AT'
            ResultatsVIH[(indic, vals['up'],vals['edat'],vals['sexe'],comb,'DEN')] += 1


for indic in indicadors_inc_prev:
    tip = indicadors_inc_prev[indic]['tip']
    sql = 'select id_cip_sec, dde from eqa_problemes_incid where ps in {}'.format(indicadors_inc_prev[indic]['ps'])
    for id, dde in getAll(sql, nod):
        carregar = 0
        if tip == 'I':
            b = monthsBetween(dde,dext)
            if 0 <= b <= 11:
                carregar = 1
        elif tip == 'P' and dde <= dext:
            carregar = 1
        try:
            up = assig[id]['up']
            edat = assig[id]['edat']
            sexe = assig[id]['sexe']
            insti = assig[id]['insti']
            ates = assig[id]['ates']
        except KeyError:
            continue
        if carregar == 1:
            comb = insti + 'ASS'
            ResultatsVIH[(indic, up, edat, sexe,comb,'NUM')] += 1
            if ates == 1:
                comb = insti + 'AT'
                ResultatsVIH[(indic, up, edat, sexe,comb,'NUM')] += 1
                if insti == 'NOINS':
                    if (id, indic) in ResultatsPacient:
                        ResultatsPacient[(id, indic)]['num'] += 1
                        ResultatsPacient[(id, indic)]['den'] += 1
                    else:
                        ResultatsPacient[(id, indic)] = {'num': 1, 'den': 1, 'up': up}
               
                
try:
    remove(OutFile)
except:
    pass 
    
with openCSV(OutFile) as c:
    for (indicador,up,edat,sexe,comb,analisi),count in ResultatsVIH.items():
        c.writerow([indicador,up,edat,sexe,comb,analisi,count])
TaulaMy = "exp_khalix_its_indicador"
execute('drop table if exists {}'.format(TaulaMy),db)
execute('create table {} (indicador varchar(10),up varchar(5),edat varchar(10),sexe varchar(10),comb varchar(10),analisi varchar(10),resultat double)'.format(TaulaMy),db)
loadData(OutFile,TaulaMy,db)  

upload = []
for (id, indicador),count in ResultatsPacient.items():
    upload.append([id, indicador, count['up'], count['num'],count['den']])

table = 'mst_indicadors_its_pacient' 
createTable(table, '(id_cip_sec int, indicador varchar(10), up varchar(5), num double, den double)', db, rm=True)
listToTable(upload, table, db)

error= []

centres="export.khx_centres"
query="select indicador,concat('A','periodo'),ics_codi,analisi,edat,comb,sexe,'N',resultat from altres.{0} a inner join {1} b on a.up=scs_codi".format(TaulaMy,centres)
file = 'ITS_INDICADORS'
error.append(exportKhalix(query,file))
if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")  

centres="nodrizas.jail_centres"
query="select indicador,concat('A','periodo'),ics_codi,analisi,edat,comb,sexe,'N',resultat from altres.{0} a inner join {1} b on a.up=scs_codi".format(TaulaMy,centres)
file = 'ITS_INDICADORS_JAIL'
error.append(exportKhalix(query,file))
if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")  

printTime('Fi')
