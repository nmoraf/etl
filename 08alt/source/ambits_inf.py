# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


DEBUG = False
TB_KLX = "EXP_KHALIX_AMBITS_INF"
TB_UBA = "EXP_AMBITS_INF_ESIAP"
DB = "ALTRES"
FILE = "AMBITS_INF"

SPEC = [
    {
        "ambit": "ACO", "tb": "ambits_tao", "db": "import",
        "dat": "aco_data", "up": "aco_up", "usu": "aco_usu",
        "accepted": 1,
        "where": ["aco_dia = 1"],
        "all": True,
        "esiap": True
    },
    {
        "ambit": "IT", "tb": "ambits_it", "db": "import",
        "dat": "itv_data_baixa", "up": "itv_up", "usu": "itv_usuari",
        "accepted": 1,
        "all": {"tb": "baixes", "db": "import",
                "dat": "ilt_data_baixa", "up": "ilt_up_b",
                "usu": "ilt_usuari", "accepted": 1}
    },
    {
        "ambit": "MDI", "tb": "ambits_mdi", "db": "import",
        "dat": "mdi_data_ins", "centre": ("mdi_v_cen", "mdi_v_cla"),
        "usu": "mdi_pr_usu",
        "accepted": 1,
        "all": True
    },
    {
        "ambit": "TS", "tb": "ambits_trans", "db": "import",
        "dat": "st_data_peticio", "up": "st_up", "usu": "st_usuari",
        "accepted": 1,
        "all": True
    },
    {
        "ambit": "LAB", "tb": "ambits_lab", "db": "import",
        "dat": "pet_data_reg", "up": "pet_up", "usu": "pet_usu_alta",
        "accepted": 1,
        "all": True
    },
    {
        "ambit": "DER", "tb": "nod_derivacions", "db": "nodrizas",
        "dat": "oc_data", "up": "oc_up_ori", "usu": "oc_usuari_alta",
        "accepted": 1,
        "all": True
    },
    {
        "ambit": "RE", "tb": "ambits_re", "db": "import",
        "dat": "mre_data", "up": "mre_cod_up_pre", "usu": "mre_usuari",
        "where": ["mre_estat = 'V'", "(mre_tipus <> 'B' or left(mre_mtu_blo, 3) in ('BLO', 'CAD'))"],  # noqa
        "accepted": "if(mre_data_baixa = 0, 1, 0)",
        "all": {"tb": "tractaments", "db": "import",
                "dat": "ppfmc_pmc_data_ini", "up": "ppfmc_pmc_amb_cod_up",
                "usu": "\'P000DPDM\'", "accepted": 1}
    }
]


class Main(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_eaps()
        self.get_usuaris()
        self.create_ubas()
        self.get_dades()
        self.upload_dades()
        self.export_khalix()

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "select id_cip_sec, up, ubainf from assignada_tot \
               where edat > 14 and ates = 1 and institucionalitzat = 0"
        sql += " limit 1000000" if DEBUG else ""
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_eaps(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.eaps = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_usuaris(self):
        """."""
        self.usuaris = c.defaultdict(set)
        sql = "select codi_sector, ide_usuari, ide_categ_prof_c \
               from cat_pritb992"
        for sector, usuari, categoria in u.getAll(sql, "import"):
            id = (sector, usuari)
            self.usuaris["all"].add(id)
            if categoria == "30999":
                self.usuaris["inf"].add(id)

    def create_ubas(self):
        """."""
        columns = "(up varchar(5), uba varchar(5), ambit varchar(10), \
                    analisi varchar(3), valor int)"
        u.createTable(TB_UBA, columns, DB, rm=True)

    def get_dades(self):
        """."""
        self.dades = []
        for spec in SPEC:
            account = "AMBINF{}".format(spec["ambit"])
            ambit = Ambit(spec, self.poblacio, self.eaps, self.usuaris)
            for (up, tipus, go), n in ambit.dades.items():
                if tipus == "inf" and go == "A":
                    if spec["accepted"] == 1:
                        keys = [(account, self.eaps[up], "NUM", n)]
                    else:
                        keys = [(account + "1", self.eaps[up], "NUM", n)]
                elif tipus == "inf" and go == "R":
                    keys = [(account + "2", self.eaps[up], "NUM", n)]
                elif tipus == "all" and go == "A":
                    if spec["accepted"] == 1:
                        keys = [(account, self.eaps[up], "DEN", n)]
                    else:
                        keys = [(account + i, self.eaps[up], "DEN", n)
                                for i in ("1", "2")]
                for key in keys:
                    self.dades.append(key)

    def upload_dades(self):
        """."""
        columns = ("account varchar(10)", "entity varchar(10)",
                   "analysis varchar(10)", "value int")
        u.createTable(TB_KLX, "({})".format(", ".join(columns)), DB, rm=True)
        u.listToTable(self.dades, TB_KLX, DB)

    def export_khalix(self):
        """."""
        sql = "select account, concat('A', 'periodo'), entity, analysis, \
                      'NOCAT', 'NOIMP', 'DIM6SET', 'N', value \
               from {}.{}".format(DB, TB_KLX)
        u.exportKhalix(sql, FILE)


class Ambit(object):
    """."""

    def __init__(self, spec, poblacio, eaps, usuaris):
        """."""
        self.spec = spec
        self.poblacio = poblacio
        self.eaps = eaps
        self.usuaris = usuaris
        self.get_dades()
        if isinstance(self.spec["all"], dict):
            self.get_total()
        if "esiap" in self.spec:
            self.upload_esiap()

    @staticmethod
    def get_sql(spec):
        """."""
        sql = "select a.codi_sector, id_cip_sec, {}, {}, {} \
               from {} a \
               inner join nodrizas.dextraccio b".format(
                                                spec.get("up", "cent_codi_up"),
                                                spec["usu"],
                                                spec["accepted"],
                                                spec["tb"])
        if "centre" in spec:
            sql += " inner join import.cat_pritb010 c \
                     on a.codi_sector = c.codi_sector and \
                        a.{} = c.cent_codi_centre and \
                        a.{} = c.cent_classe_centre".format(*spec["centre"])
        sql += " where {} between adddate(b.data_ext, interval -1 year)  \
                                  and b.data_ext".format(spec["dat"])
        if "where" in spec:
            sql += " and ".join([""] + spec["where"])
        if DEBUG:
            sql += " limit 1000"
        return sql

    def is_good(self, sector, id, up, usu):
        """."""
        return (id in self.poblacio and
                up in self.eaps and
                (sector, usu) in self.usuaris["all"])

    def get_dades(self):
        """."""
        self.dades = c.Counter()
        self.ubas = c.Counter()
        esiap = "esiap" in self.spec
        sql = Ambit.get_sql(self.spec)
        for sector, id, up, usu, go in u.getAll(sql, self.spec["db"]):
            if self.is_good(sector, id, up, usu):
                up_p, uba = self.poblacio[id]
                if self.spec["all"] is True:
                    self.dades[(up_p, "all", "A" if go else "R")] += 1
                    if esiap:
                        self.ubas[(up_p, uba, self.spec["ambit"], "DEN")] += 1
                if (sector, usu) in self.usuaris["inf"]:
                    self.dades[(up_p, "inf", "A" if go else "R")] += 1
                    if esiap:
                        self.ubas[(up_p, uba, self.spec["ambit"], "NUM")] += 1

    def get_total(self):
        """."""
        this = self.spec["all"]
        sql = Ambit.get_sql(self.spec["all"])
        for sector, id, up, usu, go in u.getAll(sql, this["db"]):
            if self.is_good(sector, id, up, usu):
                up_p, uba = self.poblacio[id]
                self.dades[(up_p, "all", "A")] += 1

    def upload_esiap(self):
        """."""
        upload = [k + (v,) for (k, v) in self.ubas.items()]
        u.listToTable(upload, TB_UBA, DB)


if __name__ == "__main__":
    Main()
