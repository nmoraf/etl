# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els indicadors de la carpeta altres
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'mst_control_eqa'
tb_cat = 'cat_control_indicadors'
tb_iad = "exp_ecap_prof_uba"
tb_iad_maca = "exp_khalix_maca_up"
tb_farm = "exp_ecap_alertes_uba"


db_origen = "altres"

"""Taules origen"""
tb_econsulta = "exp_khalix_up_econsulta"
tb_gestinf = "exp_khalix_gida"
tb_acc = "exp_khalix_forats"
tab_it_new = "exp_khalix_it"
tab_gescasos = "exp_khalix_up_gescasos"
tab_frag = "exp_khalix_fragilitat"
tab_upp = "exp_khalix_nafres_upp_uba"
tab_ambits_inf = "exp_khalix_ambits_inf"
tab_SiE = "exp_khalix_sie"
tab_presc_soc = "exp_khalix_presc_soc"
tb_tires = "exp_khalix_tir_up_ind"

periode,= u.getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')

class LadyBug_altres(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """."""
        self.indicadors, self.resultats, self.cataleg = [], Counter(), {}
        self.get_territoris()
        self.get_econsulta()
        self.get_gestinf()
        self.get_acc()
        self.get_IAD()
        self.get_alertesFarm()
        self.get_it_new()
        self.get_gestcasos()
        self.get_fragilitat()
        self.get_nafres()
        self.get_ambits_inf()
        self.get_SiE()
        self.get_presc_soc()
        self.get_tires()
        self.export_data()
        
    def get_territoris(self):
        """Obtenim àmbits i sector"""
        self.centres, self.centresSCS = {}, {}
        sql = 'select ics_codi, scs_codi, amb_desc, sector from cat_centres'
        for br, up, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
            self.centresSCS[up] = {'sector': sector, 'ambit': amb}
            
    def get_econsulta(self):
        """Agafem dades econsulta actuals"""
        sql = "select indicador, br, analisis, n from {}".format(tb_econsulta)
        for indicador, br, tip, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
                
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "econsulta_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
                
    def get_gestinf(self):
        """Agafem dades actuals de gestinf"""
        sql = "select ind, ent, analisi, motiu, valor from {} where length(ent) = 5 and valor > 0".format(tb_gestinf)
        for indicador, br, tip, motiu, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                if indicador in ['GESTINF03', 'GESTINF04', 'GESTINF05', 'GESTINF06']:
                    if motiu in ['PALADU', 'PALPED']:
                        self.resultats[periode, sector, ambit, indicador, 'den'] += n
                else:
                    self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
                
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "GESTINF_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
    
    def get_acc(self):
        """Agafem dades actuals de accessibilitat. com que cada mes s'actualitzen TOTS els períodes fem estructura diferent a la resta"""
        indis, periods = [],{}
        sql = "select d0, d1, d2,  d3, n from {} where length(d2)=5".format(tb_acc)
        for indicador, per, br, tip,  n in u.getAll(sql, db_origen):
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            perInt = per.replace('A', '20')
            periods[(perInt)] = True
            if tip == 'DEN':
                self.resultats[perInt, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[perInt, sector, ambit, indicador, 'num'] += n
                
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "ACC_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            indis.append(codi)
            sql = "delete a.* from {0} a where indicador = '{1}'".format(tb_cat, codi)
            u.execute(sql, db)
                   
        indis = tuple(indis)
        for (per),i in periods.items():
            sql = "delete a.* from {0} a where periode = '{1}' and indicador in {2}".format(tb, per, indis)
            u.execute(sql, db)
    
    def get_IAD(self):
        """Agafem IADs, com que són mensuals els agafem de la taula export a pdp que és setmanal"""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "IAD_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, tipus, numerador, denominador from {}".format(tb_iad)
        for indicador, up, tipus, num, den in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            if tipus != 'I' and indicador in self.cataleg and indicador not in ('IAD0008','IAD0008A','IAD0008B','IAD0009'):
                self.resultats[periode, sector, ambit, indicador, 'den'] += den
                self.resultats[periode, sector, ambit, indicador, 'num'] += num
                
        sql = "select up, indicador, conc, sum(recomptes) from {} where comb='NOINSAT' group by up, indicador, conc".format(tb_iad_maca)
        for  up, indicador, tip, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            if indicador in self.cataleg:
                if tip == 'DEN':
                    self.resultats[periode, sector, ambit, indicador, 'den'] += n
                if tip == 'NUM':
                    self.resultats[periode, sector, ambit, indicador, 'num'] += n
    
    def get_alertesFarm(self):
        """Agafem ALERTES de farmàcia, de moment només les de FARMDIAB"""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "ALERT_FARM_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, tipus, numerador, denominador from {}".format(tb_farm)
        for indicador, up, tipus, num, den in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            if tipus != 'I' and indicador in self.cataleg:
                self.resultats[periode, sector, ambit, indicador, 'den'] += den
                self.resultats[periode, sector, ambit, indicador, 'num'] += num
                
    def get_it_new(self):
        """Agafem it del catsalut que es calcula a altres segons uns criteris"""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "Catsalut_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            if codi == 'ICAM01AP':
                self.cataleg[codi] = {'d': desc, 'p': pare}
                self.indicadors.append(codi)
        
        sql = "select indicador, entity, analisi, valor from {} where length(entity) = 5 and indicador in ('IT003OST', 'IT003MEN','IT003TRA','IT003SIG')".format(tab_it_new)
        for indicador, br, tip, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, 'ICAM01AP', 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, 'ICAM01AP', 'num'] += n    
    
    def get_gestcasos(self):
        """."""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "gestcasos_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, tipus, n from {}".format(tab_gescasos)
        for indicador, up, tip, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n  
                
    def get_fragilitat(self):
        """."""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "FRAG_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, numerador, denominador from {} where tipus_pob='NOINSAT'".format(tab_frag)
        for indicador, up, num, den in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            self.resultats[periode, sector, ambit, indicador, 'den'] += den
            self.resultats[periode, sector, ambit, indicador, 'num'] += num
    
    def get_nafres(self):
        """Nafres i UPP"""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "upp_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, numerador, denominador from {}".format(tab_upp)
        for indicador, up, num, den in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            self.resultats[periode, sector, ambit, indicador, 'den'] += den
            self.resultats[periode, sector, ambit, indicador, 'num'] += num
    
    def get_ambits_inf(self):
        """."""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "ambits_inf_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select account, entity, analysis, value from {}".format(tab_ambits_inf)
        for indicador, br, tip, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n  
    
    def get_SiE(self):
        """Salut i Escola"""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "SiE_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select codi_indicador, up, sum(numerador), sum(denominador) from {} \
                group by codi_indicador, up".format(tab_SiE)
        for indicador, up, num, den in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            self.resultats[periode, sector, ambit, indicador, 'den'] += den
            self.resultats[periode, sector, ambit, indicador, 'num'] += num
        
    def get_presc_soc(self):
        """Prescripció social"""
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "presc_soc_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
        
        sql = "select indicador, up, sum(numerador), sum(denominador) from {} \
                group by indicador, up".format(tab_presc_soc)
        for indicador, up, num, den in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            self.resultats[periode, sector, ambit, indicador, 'den'] += den
            self.resultats[periode, sector, ambit, indicador, 'num'] += num
    
    def get_tires(self):
        """Agafem dades de tires"""
        sql = "select indicador, up, conc, n from {} where comb='NOINSAT'".format(tb_tires)
        for indicador, up, tip, n in u.getAll(sql, db_origen):
            try:
                ambit = self.centresSCS[up]['ambit']
                sector = self.centresSCS[up]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
                
        file = u.baseFolder + ["67LadyBug", "dades_noesb", "tir_cataleg.txt"]
        file = u.SLASH.join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
            self.indicadors.append(codi)
    
    
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indicadors = tuple(self.indicadors)
        sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(tb, periode, indicadors)
        u.execute(sql, db)
        u.listToTable(upload, tb, db)
        
        upload_cat = []
        for (codi), dadesc in self.cataleg.items():
            desc = dadesc['d']
            pare = dadesc['p']
            upload_cat.append([codi, desc, pare])

        sql = "delete a.* from {0} a where indicador in {1}".format(tb_cat, indicadors)
        u.execute(sql, db)
        u.listToTable(upload_cat, tb_cat, db)
   
if __name__ == '__main__':
    LadyBug_altres()
    