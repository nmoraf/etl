from sisapUtils import *

db = "altres"
pob = "%s.exp_ecap_pob" % db
pobresum = "%s.exp_ecap_pobresum" % db
pobresumeap = "%s.exp_ecap_pobresumeap" % db
cataleg= "%s.exp_ecap_cataleg" % db
catalegresum= "%s.exp_ecap_catalegresum" % db

query= "select up,uba,tipus,concat('POB',edat,sexe),ass,assup,at,atup from %s where uba<>''" % pob
table= "pobIndicadors"
exportPDP(query=query,table=table,dat=True)

query= "select * from %s where uba<>''" % pobresum
table= "pobIndicadorsResum"
exportPDP(query=query,table=table,dat=True)

query= "select up,indicador,num,den,ind  from %s" % pobresumeap
table= "pobIndicadorsResumEAP"
exportPDP(query=query,table=table,dat=True)

query= "select indicador,sum(num),sum(den),sum(num)/sum(den) from %s group by indicador" % pobresumeap
table= "pobIndicadorsResumICS"
exportPDP(query=query,table=table,dat=True)

query= "select * from %s" % cataleg
table= "PobCataleg"
exportPDP(query=query,table=table,truncate=True)

query= "select * from %s" % catalegresum
table= "PobCataleg"
exportPDP(query=query,table=table)
