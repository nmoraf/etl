# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "sisap_agendes_atributs"
DATABASE = "redics"
USERS = ("PREDUMMP", "PREDUECR", "PREDULMB", "PREDUPRP", "PDP", "PREDUJVG")


class Atributs(object):
    """."""

    def __init__(self):
        """."""
        self.get_visites()
        self.get_significats()
        self.get_centres()
        self.get_consultoris()
        self.get_moduls()
        self.upload_data()
        # self.send_estranys()

    def get_visites(self):
        """."""
        self.visites = c.Counter()
        sql = "select visi_centre_codi_centre, visi_centre_classe_centre, \
                      visi_servei_codi_servei, visi_modul_codi_modul, \
                      visi_data_visita between data_ext - interval 1 month and\
                                               data_ext \
               from visites1, nodrizas.dextraccio \
               where visi_situacio_visita = 'R' and \
                     visi_data_visita between \
                            adddate(data_ext, interval -1 year) and \
                            data_ext"
        for key in u.getAll(sql, "import"):
            self.visites[key] += 1

    def get_significats(self):
        """."""
        sql = "select rv_high_value, rv_low_value, rv_meaning \
               from cat_pritb000 \
               where codi_sector = '6734' and \
                     rv_table = 'VISTB027' and \
                     rv_column = 'ATRIBUT_{}'"
        self.pares = {row[0]: row[2]
                      for row in u.getAll(sql.format(1), "import")}
        self.fills = {row[:2]: row[2]
                      for row in u.getAll(sql.format(2), "import")}

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc \
               from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_consultoris(self):
        """."""
        sql = "select codi_sector, cent_codi_centre, cent_classe_centre, \
                      cent_nom_centre \
               from cat_pritb010"
        self.consultoris = {row[:3]: row[3] for row in u.getAll(sql, "import")}

    def get_moduls(self):
        """."""
        self.moduls = []
        self.estranys = []
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, modu_codi_up, \
                      modu_descripcio, modu_codi_uab, \
                      if(modu_data_baixa = 47120101, null, modu_data_baixa), \
                      modu_act_agenda, modu_subact_agenda, codi_sector \
               from cat_vistb027"
        for row in u.getAll(sql, "import"):
            key = row[:4]
            serv, cod, up, des, uba, baixa, attr_1, attr_2, sector = row[2:]
            if up in self.centres:
                br = self.centres[up][0]
                atribut_1 = self.pares.get(attr_1)
                atribut_2 = self.fills.get((attr_1, attr_2))
                visites = {i: self.visites[key + (i,)] for i in (0, 1)}
                anuals = sum(visites.values())
                mensuals = visites[1]
                if anuals:
                    this = (br, serv, cod, des, uba, baixa,
                            atribut_1, atribut_2, anuals, mensuals)
                    self.moduls.append(this)
                if uba and attr_1 not in ("", "1") and not baixa:
                    if (attr_1, attr_2) != ("2", "2"):
                        centre = self.centres[up][1:]
                        cons = self.consultoris[(sector, row[0], row[1])]
                        this = (cons, serv, cod, des, uba, atribut_1, atribut_2)  # noqa
                        self.estranys.append(centre + this)

    def upload_data(self):
        """."""
        cols = "(eap varchar(5), servei varchar(5), modul varchar(5), \
                 descripcio varchar(255), uba varchar(5), baixa date, \
                 atribut_1 varchar(255), atribut_2 varchar(255), \
                 visites_any int, visites_mes int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.moduls, TABLE, DATABASE)
        u.grantSelect(TABLE, USERS, DATABASE)

    def send_estranys(self):
        """."""
        header = [("ambit", "sap", "eap", "consultori", "servei",
                   "agenda_codi", "agenda_desc", "uba",
                   "atribut1", "atribut2")]
        data = header + sorted(self.estranys)
        u.writeCSV(u.tempFolder + "agendes.csv", data, sep=";")


if __name__ == "__main__":
    Atributs()
