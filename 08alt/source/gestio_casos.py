# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import itertools as it

import sisapUtils as u


tb_klx = 'exp_khalix_up_gescasos'
tb_pac = 'mst_indicadors_pacient_gescasos'
tb_ecap = ('exp_ecap_gescasos_uba', 'exp_ecap_gescasos_pacient',
           'exp_ecap_gescasos_cataleg', 'exp_ecap_gescasos_catalegPare')
no_ecap = ('GC0000', 'GC0008', 'GC0009')
db = 'altres'


class GestioCasos(object):
    """."""

    def __init__(self):
        """."""
        self.resultat = []
        self.pacients = {}
        self.literals = {}
        self.get_poblacio()
        self.get_denominador()
        self.get_01()
        self.get_02()
        self.get_03()
        self.get_04()
        self.get_05()
        self.get_06()
        self.get_07()
        self.get_08()
        self.get_09()
        self.upload_tables_klx()
        self.export_klx()
        self.get_u11()
        self.upload_tables_ecap()

    def get_poblacio(self):
        """."""
        sql = 'select id_cip_sec, up from assignada_tot'
        self.poblacio = {id: up for id, up in u.getAll(sql, 'nodrizas')}

    def get_denominador(self):
        """."""
        denominador_de = (1, 2, 3, 4, 8, 9)
        sql = "select id_cip_sec, min(gesc_estat) \
               from gescasos, nodrizas.dextraccio \
               where gesc_estat in ('A', 'T') and \
               gesc_data_alta <= data_ext and \
               (gesc_data_sortida = 0 or gesc_data_sortida between \
                adddate(adddate(data_ext, interval -1 year), interval +1 day) \
                and data_ext) \
               group by id_cip_sec"
        self.denominador = {}
        recompte = c.Counter()
        self.actius = set()
        for id, estat in u.getAll(sql, 'import'):
            if id in self.poblacio:
                up = self.poblacio[id]
                self.denominador[id] = up
                recompte[up] += 1
                for i in denominador_de:
                    if i != 8:
                        self.pacients[(id, 'GC000{}'.format(i), up)] = False
                if estat == 'A':
                    self.actius.add(id)
        for up, n in recompte.items():
            self.resultat.append(('GC0000', up, 'NUM', n))
            for i in denominador_de:
                self.resultat.append(('GC000{}'.format(i), up, 'DEN', n))

    def get_01(self):
        """."""
        ind = 'GC0001'
        self.literals[ind] = 'Pacients amb Pla de Cures'
        pla = c.Counter()
        sql = 'select id_cip_sec from mst_pla_tots where num > 0'
        for id, in u.getAll(sql, 'altres'):
            if id in self.denominador:
                up = self.denominador[id]
                pla[up] += 1
                self.pacients[(id, ind, up)] = True
        for up, n in pla.items():
            self.resultat.append((ind, up, 'NUM', n))

    def get_02(self):
        """."""
        ind = 'GC0002'
        self.literals[ind] = 'Pacients amb valoraci� integral'
        per_pacient = c.Counter()
        sql = 'select id_cip_sec, agrupador from eqa_variables, dextraccio \
               where agrupador between 87 and 90 and usar = 1 and \
               data_var between \
                adddate(adddate(data_ext, interval -1 year), interval +1 day) \
                and data_ext'
        for id, agr in u.getAll(sql, 'nodrizas'):
            if id in self.denominador:
                per_pacient[id] += 3 if agr == 90 else 1
        per_up = c.Counter()
        for id, n in per_pacient.items():
            if n >= 3:
                up = self.denominador[id]
                per_up[up] += 1
                self.pacients[(id, ind, up)] = True
        for up, n in per_up.items():
            self.resultat.append((ind, up, 'NUM', n))

    def get_03(self):
        """."""
        ind = 'GC0003'
        self.literals[ind] = 'Pacients amb adher�ncia tractament'
        adherencia = c.defaultdict(set)
        sqls = (('select id_cip_sec from eqa_variables, dextraccio \
                  where agrupador = 701 and usar = 1 and \
                  data_var between \
                   adddate(adddate(data_ext, interval -1 year), \
                    interval +1 day) \
                   and data_ext', 'nodrizas'),
                ('select id_cip_sec from pla2, nodrizas.dextraccio \
                  where snnv_cod_noc = 1808 and \
                  (snnv_data_alta between \
                    adddate(adddate(data_ext, interval -1 year), \
                     interval +1 day) \
                    and data_ext or \
                   snnv_dus between \
                    adddate(adddate(data_ext, interval -1 year), \
                     interval +1 day) \
                    and data_ext)', 'import'))
        for sql, db in sqls:
            for id, in u.getAll(sql, db):
                if id in self.denominador:
                    up = self.denominador[id]
                    adherencia[up].add(id)
                    self.pacients[(id, ind, up)] = True
        for up, pacients in adherencia.items():
            self.resultat.append((ind, up, 'NUM', len(pacients)))

    def get_04(self):
        """."""
        ind = 'GC0004'
        self.literals[ind] = 'Caigudes'
        llar = c.defaultdict(set)
        fa1a = 'adddate(adddate(data_ext, interval -1 year), interval +1 day)'
        sqls = (('select id_cip_sec from eqa_variables, dextraccio \
                  where agrupador in (97, 751) and usar = 1 and \
                  data_var between {} and data_ext'.format(fa1a), 'nodrizas'),
                ('select id_cip_sec from pla2, nodrizas.dextraccio \
                  where snnv_cod_nic = 6490 and \
                  (snnv_data_alta between {0} and data_ext or \
                   snnv_dus between {0} and data_ext)'.format(fa1a), 'import'),
                ("select id_cip_sec from grupal1, nodrizas.dextraccio \
                  where cinp_cod_int = '6490' and \
                  cinp_data_alta between {} and data_ext".format(fa1a),
                 'import'),
                ('select id_cip_sec from eqa_problemes where ps in (183, 184)',
                 'nodrizas'))
        for sql, db in sqls:
            for id, in u.getAll(sql, db):
                if id in self.denominador:
                    up = self.denominador[id]
                    llar[up].add(id)
                    self.pacients[(id, ind, up)] = True
        for up, pacients in llar.items():
            self.resultat.append((ind, up, 'NUM', len(pacients)))

    def get_05(self):
        """."""
        ind = 'GC0005'
        self.literals[ind] = 'PIIC fet'
        denominador = c.Counter()
        numerador = c.Counter()
        sql = 'select id_cip_sec, num from mst_pcc_individual'
        for id, num in u.getAll(sql, 'altres'):
            if id in self.denominador:
                up = self.denominador[id]
                denominador[up] += 1
                numerador[up] += num
                self.pacients[(id, ind, up)] = bool(num)
        for up, n in denominador.items():
            self.resultat.append((ind, up, 'DEN', n))
            self.resultat.append((ind, up, 'NUM', numerador[up]))

    def get_06(self):
        """."""
        ind = 'GC0006'
        self.literals[ind] = 'Pla de decisions anticipades'
        denominador = c.Counter()
        numerador = c.Counter()
        sql = "select id_cip_sec from piic \
               where mi_cod_var = 'T4101002' and mi_val_txt <> ''"
        pda = set([id for id, in u.getAll(sql, 'import')])
        sql = "select id_cip_sec from estats, nodrizas.dextraccio \
               where es_cod = 'ER0002' and es_dde <= data_ext"
        for id, in u.getAll(sql, 'import'):
            if id in self.denominador:
                up = self.denominador[id]
                denominador[up] += 1
                self.pacients[(id, ind, up)] = False
                if id in pda:
                    numerador[up] += 1
                    self.pacients[(id, ind, up)] = True
        for up, n in denominador.items():
            self.resultat.append((ind, up, 'DEN', n))
            self.resultat.append((ind, up, 'NUM', numerador[up]))

    def get_07(self):
        """."""
        ind = 'GC0007'
        self.literals[ind] = 'MNA passat'
        denominador = c.Counter()
        mna = c.defaultdict(set)
        fa1a = 'adddate(adddate(data_ext, interval -1 year), interval +1 day)'
        sql = "select id_cip_sec from nodrizas.eqa_problemes \
               where ps in (838, 704)"
        sonda = set([id for id, in u.getAll(sql, 'nodrizas')])
        sqls = ('select id_cip_sec from eqa_variables, dextraccio \
                 where agrupador = 700 and usar = 1 and \
                 data_var between {} and data_ext'.format(fa1a),
                "select id_cip_sec from import.xml, dextraccio \
                 where xml_tipus_orig = 'MNA' and \
                 xml_data_alta between {} and data_ext".format(fa1a))
        for id, up in self.denominador.items():
            if id not in sonda:
                denominador[up] += 1
                self.pacients[(id, ind, up)] = False
        for sql in sqls:
            for id, in u.getAll(sql, 'nodrizas'):
                if id in self.denominador:
                    up = self.denominador[id]
                    if id not in sonda:
                        mna[up].add(id)
                        self.pacients[(id, ind, up)] = True
        for up, n in denominador.items():
            self.resultat.append((ind, up, 'DEN', n))
        for up, pacients in mna.items():
            self.resultat.append((ind, up, 'NUM', len(pacients)))

    def get_08(self):
        """."""
        ind = 'GC0008'
        serveis = ('INFG', 'UGC', 'GCAS')
        sql = "select left(ide_numcol, 8) from cat_pritb799 a \
               inner join cat_pritb992 b on a.rol_usu = b.ide_usuari \
               and a.codi_sector = b.codi_sector \
               where rol_rol = 'ECAP_GEST' and ide_numcol <> ''"
        numcols = tuple(set([numcol for numcol, in u.getAll(sql, 'import')]))
        visites = c.Counter()
        sql = "select id_cip_sec from visites1, nodrizas.dextraccio where \
               visi_data_visita between \
                date_add(date_add(data_ext, interval -1 year), \
                 interval +1 day) and data_ext and \
                visi_situacio_visita = 'R' and ( \
                visi_servei_codi_servei in {} or \
                left(visi_col_prov_resp, 8) in {})".format(serveis, numcols)
        for id, in u.getAll(sql, 'import'):
            if id in self.denominador:
                up = self.denominador[id]
                visites[up] += 1
        for up, n in visites.items():
            self.resultat.append((ind, up, 'NUM', n))

    def get_09(self):
        """."""
        ind = 'GC0009'
        self.literals[ind] = 'Pacients en atdom'
        atdom = c.Counter()
        sql = 'select id_cip_sec from eqa_problemes where ps=45'
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.denominador:
                up = self.denominador[id]
                atdom[up] += 1
                self.pacients[(id, ind, up)] = True
        for up, n in atdom.items():
            self.resultat.append((ind, up, 'NUM', n))

    def upload_tables_klx(self):
        """."""
        u.createTable(tb_klx, '(indicador varchar(10), up varchar(10), \
                               tipus varchar(10), n int)', db, rm=True)
        u.listToTable(self.resultat, tb_klx, db)
        u.createTable(tb_pac, '(id_cip_sec int, indicador varchar(10), \
                               up varchar(10), compleix int)', db, rm=True)
        u.listToTable([k + (1 * v,) for (k, v) in self.pacients.items()],
                      tb_pac, db)

    def export_klx(self):
        """."""
        error = []
        sql = "select indicador, 'Aperiodo', ics_codi, tipus, 'NOCAT', \
               'NOIMP', 'DIM6SET', 'N', n from {0}.{1} a inner join \
               nodrizas.cat_centres b on up = scs_codi".format(db, tb_klx)
        file = 'GESTIO_CASOS'
        error.append(u.exportKhalix(sql, file))

    def get_u11(self):
        """."""
        sql = 'select id_cip_sec from {} where compleix = 0'.format(tb_pac)
        pacients = set([id for id, in u.getAll(sql, db)])
        sql = 'select id_cip_sec, hash_d, codi_sector from u11'
        self.u11 = {row[0]: row[1:] for row in u.getAll(sql, 'import')
                    if row[0] in pacients}

    def upload_tables_ecap(self):
        """."""
        for tb in tb_ecap:
            orig = tb.replace('gescasos', 'alertes')
            u.createTable(tb, 'like {}'.format(orig), db, rm=True)
        ecap_resultat = {}
        indicadors = set()
        ups = set()
        sql = """
            select
                indicador, up, tipus, n
            from {}
            where
                indicador not in {}
            """.format(tb_klx, no_ecap)
        for ind, up, analisis, n in u.getAll(sql, db):
            id = (up, 'sense', 's', ind)
            if id not in ecap_resultat:
                ecap_resultat[id] = [0, 0]
            ecap_resultat[id][0 if analisis == 'NUM' else 1] = n
            ups.add(up)
            indicadors.add(ind)
        for up, ind in it.product(ups, indicadors):
            id = (up, 'sense', 's', ind)
            if id not in ecap_resultat:
                ecap_resultat[id] = [0, 0]
        u.listToTable([k + (v[0], v[1], (float(v[0]) / v[1]) if v[1] else 0)
                      for (k, v) in ecap_resultat.items()], tb_ecap[0], db)
        sql = """
            select
                id_cip_sec,
                indicador,
                up
            from {}
            where
                compleix = 0
            """.format(tb_pac)
        ecap_pacient = [(id_cip, up, 'sense', None, None, ind, 0)
                        + self.u11[id_cip]
                        for (id_cip, ind, up) in u.getAll(sql, db)]
        u.listToTable(ecap_pacient, tb_ecap[1], db)
        pare = 'GC'
        link = 'http://sisap-umi.eines.portalics/indicador/codi/{}'
        u.listToTable([(ind, self.literals.get(ind), ind[-1:], pare,
                        1, 0, 0, 0, 0, 1,
                        link.format(ind),
                        'PCT')
                       for ind in indicadors], tb_ecap[2], db)
        u.listToTable([(pare, 'Indicadors de gesti� de casos', 8, 'ALTRES')],
                      tb_ecap[3],
                      db)


if __name__ == '__main__':
    GestioCasos()
