# -*- coding: latin1 -*-

"""
.
"""

import collections as c

import sisapUtils as u


TB_ORIGEN = "AG_LONGITUDINALITAT_NEW"
DB_ORIGEN = "NODRIZAS"
TB_DESTI = "EXP_KHALIX_AG_LONGITUDINALITAT_NEW"
DB_DESTI = "ALTRES"
FILE_UP_DESTI = "AG_LONG_UP_NOU"
FILE_UBA_DESTI = "AG_LONG_UBA_NOU"


class Longitudinalitat(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_ubas()
        self.get_partition()
        self.get_dades()
        self.export_data()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, DB_ORIGEN)}

    def get_ubas(self):
        """."""
        sql = "select up, uba, tipus from mst_ubas"
        self.ubas = set([row for row in u.getAll(sql, "eqa_ind")])

    def get_partition(self):
        """."""
        self.partitions = {}
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        dexta, = u.getOne("select date_add(data_ext, interval - 1 month) from dextraccio", "nodrizas")
        sql = "select vis_data from {} partition({}) limit 1"
        for partition in u.getTablePartitions(TB_ORIGEN, DB_ORIGEN):
            try:
                dvis, = u.getOne(sql.format(TB_ORIGEN, partition), DB_ORIGEN)
            except TypeError:
                continue
            if (dvis.year == dext.year and dvis.month == dext.month) or (dvis.year == dexta.year and dvis.month == dexta.month):
                self.partitions[partition] = True
               

    def get_dades(self):
        """."""
        self.dades = c.Counter()
        for parti in self.partitions:
            sql = "select pac_id, pac_up, pac_uba, pac_ubainf, \
                          pac_pcc, pac_maca, pac_atdom, \
                          modul_serv_homol, modul_eap, \
                          prof_responsable, prof_delegat, modul_relacio, \
                          prof_rol_gestor, modul_serv_gestor, \
                          right(year(vis_data),2),date_format(vis_data,'%m')\
                   from {} partition({}) \
                   where pac_instit = 0 and \
                         prof_numcol <> '' and \
                         vis_laborable = 1".format(TB_ORIGEN, parti)
            for (id, up, uba, ubainf, pcc, maca, atdom, serv, eap, resp,
                 deleg, rel, gc_rol, gc_serv, anys, mes) in u.getAll(sql, DB_ORIGEN):
                gc = (gc_rol == 1 or gc_serv == 1)
                periodo = "A" + str(anys) + str(mes)
                if (eap and serv in ("MG", "PED", "INF", "INFP")) or gc:
                    br = self.centres[up]
                    tip = "1" if (pcc or maca or atdom) else "2"
                    tipprof = 1 if serv == "MG" else 2 if serv == "PED" else 4
                    detalle = "TIPPROF{}".format(tipprof)
                    entities = [br]
                    entities.extend([br + entity[2] + entity[1]
                                     for entity
                                     in ((up, uba, "M"), (up, ubainf, "I"))
                                     if entity in self.ubas])
                    for entity in entities:
                        cuenta = "LONG000{}".format(tip)
                        self.dades[(cuenta, periodo, entity, detalle, "DEN")] += 1
                        if resp == 1 or gc == 1 or deleg == 1:
                            self.dades[("LONG000" + tip, periodo, entity, detalle, "NUM")] += 1
                        for i in ['A','B','C']:
                            cuenta = "LONG000{}{}".format(tip, i)
                            self.dades[(cuenta, periodo, entity, detalle, "DEN")] += 1
                        if resp == 1:
                            self.dades[("LONG000" + tip + "A", periodo, entity, detalle, "NUM")] += 1
                        elif gc == 1:
                            self.dades[("LONG000" + tip + "C", periodo, entity, detalle, "NUM")] += 1
                        elif deleg == 1:
                            self.dades[("LONG000" + tip + "B", periodo, entity, detalle, "NUM")] += 1

    def export_data(self):
        """."""
        columns = ", ".join(["d{} varchar(12)".format(i) for i in range(8)] + ["n int"])
        u.createTable(TB_DESTI, "({})".format(columns), DB_DESTI, rm=True)
        export = [(cuenta, periodo, entity, analisis, "NOCAT", detalle, "DIM6SET", "N", n)
                  for (cuenta, periodo, entity, detalle, analisis), n
                  in self.dades.items()]
        u.listToTable(export, TB_DESTI, DB_DESTI)
        sql = "select * from {}.{} where length(d2) {{}} 5".format(DB_DESTI, TB_DESTI)
        for simbol, file in (("=", FILE_UP_DESTI), (">", FILE_UBA_DESTI)):
            u.exportKhalix(sql.format(simbol), file)


if __name__ == "__main__":
    Longitudinalitat()
