# coding: utf8

"""
Població atesa per odonto
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u



etiquetes = "('RODN', 'RODON')"
moduls = "( 'RODN', 'RODN1', 'RODN2', 'RODN3', 'RODN4', 'RODN5', 'RODON', 'RODON1', 'RODON2', 'RODON3', 'RODON4', 'RODON5')"


imp = "import"
db = "altres"
file_up = "POBATESA_ODO"
file_detall = "POBL_ODO"

KhxCobertura = {'AC': 'USUACT', 'PN': 'USUPEN', 'FG': 'USUPEN'}

class atesa_odn(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_nacionalitat()
        self.get_visites()
        self.get_pob()
        self.export_data()
        self.export_data_detall()
        
    def get_centres(self):
        """EAP ICS"""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_nacionalitat(self):
        """Catàleg nacionalitat"""
        self.nac = {}
        sql = 'select codi_k,codi_nac from cat_nacionalitat'
        for k,n in u.getAll(sql,'nodrizas'):
            self.nac[int(n)] = k
        
    def get_visites(self):
        """."""
        self.visites = Counter()
        sql = "select id_cip_sec, \
                      visi_up, \
                      date_format(visi_data_visita,'%Y%m%d'), \
                      if(visi_modul_codi_modul in {} or visi_etiqueta in {}, 1, 0) \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                     s_espe_codi_especialitat in ('10106','10777')".format( moduls, etiquetes)
        for id, up, dat, escolar in u.getAll(sql, imp):
            self.visites[id] += 1
    
       
    def get_pob(self):
        """Obtenim pob atesa odn"""
        self.pob = Counter()
        self.pob_detall = Counter()
        sql = "select id_cip_sec, up, uporigen, edat, sexe, if(nacionalitat='',724,nacionalitat),nivell_cobertura from assignada_tot"
        for id, up, uporigen, edat, sexe, nacio, cob in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up][0]
                visita_odn = 1 if (id) in self.visites else 0
                self.pob[(br, up, uporigen, u.ageConverter(edat), u.sexConverter(sexe), 'den')] += 1
                self.pob[(br, up, uporigen, u.ageConverter(edat), u.sexConverter(sexe), 'num')] += visita_odn
                if visita_odn == 1:
                    try:
                        codi_nacio = self.nac[int(nacio)]
                    except KeyError:
                        codi_nacio = 'NAC19'
                    try:
                        cobertura = KhxCobertura[cob]
                    except KeyError:
                        cobertura = 'USUALT'
                    self.pob_detall[(br, codi_nacio, u.ageConverter(edat, 1), cobertura, u.sexConverter(sexe))] += visita_odn
        
            
    def export_data(self):
        """."""
        self.upload = []
        for (br, up, uporigen, edat, sexe, tip), n in self.pob.items():
            if tip == 'den':
                num = self.pob[(br, up, uporigen, edat, sexe, 'num')]
                self.upload.append([br, up, uporigen, edat, sexe, num, n])
                
        u.listToTable(self.upload, tb, db)    
        
        sql = "select 'POBATODO' ,concat('A','periodo'),br,'NUM', 'NOCAT' ,'NOIMP', 'DIM6SET','N',sum(num) from {0}.{1} group by br \
                union \
                select 'POBATODO' ,concat('A','periodo'),br,'DEN', 'NOCAT' ,'NOIMP', 'DIM6SET','N',sum(den) from {0}.{1} group by br ".format(db, tb)
        u.exportKhalix(sql, file_up)
        
    def export_data_detall(self):
        """."""
        self.upload = []
        for (br, codi_nacio, edat, cobertura, sexe), n in self.pob_detall.items():
            self.upload.append([br, codi_nacio, edat, cobertura, sexe, n])
                
        u.listToTable(self.upload, tb_detall, db)    
        
        sql = "select 'POBATOTODO', concat('A','periodo'),br, nac, edat, cob, sexe, 'N', valor from {0}.{1}".format(db, tb_detall)
        u.exportKhalix(sql, file_detall)
   
if __name__ == '__main__':
    u.printTime("Inici")
    
    tb = "exp_khalix_pob_odn"
    columns =  ["br varchar(10)", "up varchar(5)", "uporigen varchar(5)", "edat varchar(10)", "sexe varchar(10)", "num int", "den int"]     
    u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    
    tb_detall = "exp_khalix_pob_odn_detall"
    columns =  ["br varchar(10)", "nac  varchar(10)","edat varchar(10)", "cob varchar(10)","sexe varchar(10)", "valor int"]     
    u.createTable(tb_detall, "({})".format(", ".join(columns)), db, rm=True)
    
    atesa_odn()
    
    u.printTime("Fi")
    