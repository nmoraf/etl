# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
 
debug = False
proves = False

db = "altres"
nod = "nodrizas"
imp = "import"

sql = "select data_ext from dextraccio"
dext, = getOne(sql,nod)

sql = "select year(data_ext),month(data_ext) from dextraccio"
for y,m in getAll(sql,nod):
    anys = y
    mes = m
    
OutFile = tempFolder + 'acc.txt'
 
printTime('Inici Acc')  

def getCategoria(espe):
    if espe == '10888':
        return 'P'
    elif espe == '10999':
        return 'M'
    elif espe == '10777' or espe == '10106':
        return 'O'
    elif espe == '05999':
        return 'T'
    elif espe == '30999':
        return 'I'
    else:
        return 'E'
        
def getCategoriaCSL(categoria):
    if categoria == 'M':
        return 'MFiC'
    elif categoria == 'I':
        return 'INF'
    elif categoria == 'P':
        return 'PED'
    elif categoria == 'O':
        return 'ODN'
    elif categoria == 'T':
        return 'TS'
    else:
        return 'E'

catUP,catEspe,noAgenda,diadeVisita,visiInternet,diadeVisitaAge = {},{},{},{},{},{}
sql = "select codi_sector,modu_centre_codi_centre,modu_centre_classe_centre,modu_servei_codi_servei,modu_codi_modul,modu_codi_up,modu_codi_uab,modu_internet from cat_vistb027"
for sector,centre,classe,servei,modul,up,uba,internet in getAll(sql,imp):
    catUP[sector,centre,classe,servei,modul] = {'up':up,'uba':uba}
    visiInternet[sector,centre,classe,servei,modul,internet] = True


sql = "select codi_sector,s_codi_servei,s_espe_codi_especialitat from cat_pritb103"
for sector,servei,espe in getAll(sql,imp):
    catEspe[sector,servei] = espe

sql = "select codi_sector,fest_centre_codi_centre,fest_centre_classe_centre,fest_servei_codi_servei,fest_modul_codi_modul,fest_dia_festiu,fest_data_baixa from acc_festius {}".format(" where fest_centre_codi_centre='E08002752' and fest_centre_classe_centre='01' and fest_servei_codi_servei='MG' and fest_modul_codi_modul='K'" if debug else '')
for sector,centre,classe,servei,modul,diafesta,databaixa in getAll(sql,imp):
    try:
        festa = datetime.date(*(jd2gcal(diafesta)))
    except ValueError:
        continue
    mesos = monthsBetween(festa,dext)
    if mesos < 5:
        noAgenda[sector,centre,classe,servei,modul,festa] = {'dbaixa':1 if databaixa.year == 4712 else databaixa}
    else:
        continue

blocs = {}
sql = "select codi_sector,bp_centre_cod,bp_centre_cla,bp_servei,bp_modul,bp_bloc_p from acc_blocs where bp_principal='S'"
for sector,centre,classe,servei,modul,bloc in getAll(sql,imp):
    blocs[sector,centre,classe,servei,modul,bloc] = True

if proves:
    sql = "select codi_sector,visi_centre_codi_centre,visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul,visi_data_visita,visi_tipus_visita,visi_tipus_citacio,visi_bloc_codi_bloc from visitesperacc where visi_data_baixa=0 and visi_bloc_codi_bloc>'' and visi_forcada_s_n='N' {0}".format(" and visi_centre_codi_centre='E08002752' and visi_centre_classe_centre='01' and visi_servei_codi_servei='MG' and visi_modul_codi_modul='K'" if debug else '')
    for sector,centre,classe,servei,modul,visita,vtipus,citacio,bloc in getAll(sql,imp):
        diadeVisitaAge[sector,centre,classe,servei,modul,visita] = True
        try:
            if blocs[sector,centre,classe,servei,modul,bloc]:
                vinternet = citacio + vtipus
                try:
                    if visiInternet[sector,centre,classe,servei,modul,vinternet]:
                        diadeVisita[sector,centre,classe,servei,modul,visita] = True
                except KeyError:
                    continue
        except KeyError:
            continue
else:
    sql= "select table_name from information_schema.tables where table_schema='%s' and table_name like '%s\_%%'" % (imp,'visites')
    particions = {}
    for particio in getAll(sql,imp):
        sql = "select if(date_format(visi_data_visita,'%Y%m')>=date_format(data_ext,'%Y%m'),1,0) from {},nodrizas.dextraccio  limit 1".format(particio[0])
        try:
            enter, = getOne(sql,imp)
        except:
            enter = 0
        if enter == 1:
            particions[particio[0]] = True

    for particio,t in particions.items():   
        sql = "select codi_sector,visi_centre_codi_centre,visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul,visi_data_visita,visi_tipus_visita,visi_tipus_citacio,visi_bloc_codi_bloc from {0} where visi_data_baixa=0 and visi_bloc_codi_bloc>'' and visi_forcada_s_n='N' {1}".format(particio," and visi_centre_codi_centre='E08002752' and visi_centre_classe_centre='01' and visi_servei_codi_servei='MG' and visi_modul_codi_modul='K'" if debug else '')
        for sector,centre,classe,servei,modul,visita,vtipus,citacio,bloc in getAll(sql,imp):
            diadeVisitaAge[sector,centre,classe,servei,modul,visita] = True
            try:
                if blocs[sector,centre,classe,servei,modul,bloc]:
                    vinternet = citacio + vtipus
                    try:
                        if visiInternet[sector,centre,classe,servei,modul,vinternet]:
                            diadeVisita[sector,centre,classe,servei,modul,visita] = True
                    except KeyError:
                        continue
            except KeyError:
                continue

workingDays,agendaDays,citapreviaDays = Counter(), Counter(), Counter()
sql = "select codi_sector, codi_centre, classe_centre, codi_servei, codi_modul, data_calcul, primer_dia from acc_intents, nodrizas.dextraccio \
        where (data_calcul between date_add(date_add(data_ext,interval - 1 month),interval +1 day) and data_ext) and dies <60{}".format(" and codi_centre='E08002752' and classe_centre='01' and codi_servei='MG' and codi_modul='K' limit 10" if debug else '')
for sector,centre,classe,servei,modul, dcalcul, intent in getAll(sql,imp):
    workingDays[sector,centre,classe,servei,modul,'DEN'] += 1
    agendaDays[sector,centre,classe,servei,modul,'DEN'] += 1
    citapreviaDays[sector,centre,classe,servei,modul,'DEN'] += 1
    daysBetween = (intent - dcalcul).days
    if daysBetween == 0:
        workingDays[sector,centre,classe,servei,modul,'NUM'] += 1
        workingDays[sector,centre,classe,servei,modul,'NUM_MITJANA'] += 0
        agendaDays[sector,centre,classe,servei,modul,'NUM'] += 1
        agendaDays[sector,centre,classe,servei,modul,'NUM_MITJANA'] += 0
        citapreviaDays[sector,centre,classe,servei,modul,'NUM'] += 1
        citapreviaDays[sector,centre,classe,servei,modul,'NUM_MITJANA'] += 0
    else:
        a = 0
        b = 0
        e = 0
        for single_date in dateRange(dcalcul, intent):
            if isWorkingDay(single_date):
                a += 1
                try:
                    dbaixa = noAgenda[sector,centre,classe,servei,modul,single_date]['dbaixa']
                    if dbaixa == 1 or dbaixa >= dcalcul:
                        b += 0
                        e += 0
                    else:
                        try:
                            if diadeVisita[sector,centre,classe,servei,modul,single_date]:
                                b += 1
                        except KeyError:
                            b += 0
                        try:
                            if diadeVisitaAge[sector,centre,classe,servei,modul,single_date]:
                                e += 1
                        except KeyError:
                            e += 0
                except KeyError:
                    try:
                        if diadeVisita[sector,centre,classe,servei,modul,single_date]:
                            b += 1
                    except KeyError:
                        b += 0
                    try:
                        if diadeVisitaAge[sector,centre,classe,servei,modul,single_date]:
                            e += 1
                    except KeyError:
                        e += 0
        workingDays[sector,centre,classe,servei,modul,'NUM_MITJANA'] += a
        agendaDays[sector,centre,classe,servei,modul,'NUM_MITJANA'] += e
        citapreviaDays[sector,centre,classe,servei,modul,'NUM_MITJANA'] += b
        if a <= 2:
            workingDays[sector,centre,classe,servei,modul,'NUM'] += 1
        else:
            workingDays[sector,centre,classe,servei,modul,'NUM'] += 0
        if e <= 2:
            agendaDays[sector,centre,classe,servei,modul,'NUM'] += 1
        else:
            agendaDays[sector,centre,classe,servei,modul,'NUM'] += 0
        if b <= 2:
            citapreviaDays[sector,centre,classe,servei,modul,'NUM'] += 1
        else:
            citapreviaDays[sector,centre,classe,servei,modul,'NUM'] += 0

with openCSV(OutFile) as c:
    for (sector,centre,classe,servei,modul,indicador),count in workingDays.items():
        try:
            up = catUP[sector,centre,classe,servei,modul]['up']
            espe = catEspe[sector,servei]
        except KeyError:
            continue
        try:
            uba = catUP[sector,centre,classe,servei,modul]['uba']
        except KeyError:
            uba = None
        categoria = getCategoria(espe)
        c.writerow([sector,centre,classe,servei,modul,up,uba,indicador,categoria,count])

taula1 = "mst_acc_workingDays"
execute('drop table if exists {}'.format(taula1),db)
execute("create table {} (sector varchar(5) not null default'',centre varchar(20) not null default'',classe varchar(2) not null default'',servei varchar(10) not null default'', \
        modul varchar(10) not null default'', up varchar(5) not null default'',uba varchar(5) not null default'',indicador varchar(15),categoria varchar(1) not null default'',recompte double)".format(taula1),db)
loadData(OutFile,taula1,db)

with openCSV(OutFile) as c:
    for (sector,centre,classe,servei,modul,indicador),count in agendaDays.items():
        try:
            up = catUP[sector,centre,classe,servei,modul]['up']
            espe = catEspe[sector,servei]
        except KeyError:
            continue
        try:
            uba = catUP[sector,centre,classe,servei,modul]['uba']
        except KeyError:
            uba = None
        categoria = getCategoria(espe)
        c.writerow([sector,centre,classe,servei,modul,up,uba,indicador,categoria,count])

taula2 = "mst_acc_agendaDays"
execute('drop table if exists {}'.format(taula2),db)
execute("create table {} (sector varchar(5) not null default'',centre varchar(20) not null default'',classe varchar(2) not null default'',servei varchar(10) not null default'', \
        modul varchar(10) not null default'', up varchar(5) not null default'',uba varchar(5) not null default'',indicador varchar(15),categoria varchar(1) not null default'',recompte double)".format(taula2),db)
loadData(OutFile,taula2,db)


with openCSV(OutFile) as c:
    for (sector,centre,classe,servei,modul,indicador),count in citapreviaDays.items():
        try:
            up = catUP[sector,centre,classe,servei,modul]['up']
            espe = catEspe[sector,servei]
        except KeyError:
            continue
        try:
            uba = catUP[sector,centre,classe,servei,modul]['uba']
        except KeyError:
            uba = None
        categoria = getCategoria(espe)
        c.writerow([sector,centre,classe,servei,modul,up,uba,indicador,categoria,count])

taula = "mst_acc_citapreviaDays"
execute('drop table if exists {}'.format(taula),db)
execute("create table {} (sector varchar(5) not null default'',centre varchar(20) not null default'',classe varchar(2) not null default'',servei varchar(10) not null default'', \
        modul varchar(10) not null default'', up varchar(5) not null default'',uba varchar(5) not null default'',indicador varchar(15),categoria varchar(1) not null default'',recompte double)".format(taula),db)
loadData(OutFile,taula,db)


indCSL = [['LAB'],['AGE'],['CP']]
taules={'LAB':'mst_acc_workingDays','AGE':'mst_acc_agendaDays','CP':'mst_acc_citapreviaDays'}
fitxerCSL,denominadors = Counter(),Counter()
for r in indCSL:
    ind=r[0]
    sql = "select up,categoria,indicador,recompte from {}".format(taules[ind])
    for up,categoria,indicador,recompte in getAll(sql,db):
        if categoria == 'E':
            continue
        else:
            cate = getCategoriaCSL(categoria)
            if indicador == 'DEN':
                denominadors[(up,cate,ind)] += recompte
            else:
                if indicador == 'NUM':
                    tipus = 'PERC'
                elif indicador == 'NUM_MITJANA':
                    tipus = 'DIES'
                fitxerCSL[(up,cate,tipus,ind)] += recompte
                
OutFileCSL = tempFolder + 'accICS.txt'        
try:
    remove(OutFileCSL)
except:
    pass 
with open(OutFileCSL,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,cate,tipus,ind),count in fitxerCSL.items():
        num = int(count)
        den = int(denominadors[(up,cate,ind)])
        w.writerow([anys,mes,up,cate,tipus,ind,num,den])               

if IS_MENSUAL:
    text= "Us fem arribar el fitxer d'aquest mes d'accessibilitat."
    sendPolite('llistes.rsb@catsalut.cat','accICS',text,OutFileCSL)

try:
    remove(OutFileCSL)
except:
    pass 

centres="export.khx_centres"
error = []
sql = "select concat('AGDEMPCT',categoria),concat('A','periodo'),ics_codi,indicador,'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{1} a inner join {2} b on a.up=scs_codi where indicador in ('NUM','DEN') and categoria<>'E' group by ics_codi,categoria,indicador\
    union select concat('AGDEMMTJ',categoria),concat('A','periodo'),ics_codi,if(indicador='NUM_MITJANA','NUM','DEN'),'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{1} a inner join {2} b on a.up=scs_codi where indicador in ('NUM_MITJANA','DEN') and categoria<>'E' group by ics_codi,categoria,indicador\
    union select concat('AGDEMPC2',categoria),concat('A','periodo'),ics_codi,indicador,'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{3} a inner join {2} b on a.up=scs_codi where indicador in ('NUM','DEN') and categoria<>'E' group by ics_codi,categoria,indicador\
    union select concat('AGDEMMT2',categoria),concat('A','periodo'),ics_codi,if(indicador='NUM_MITJANA','NUM','DEN'),'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{3} a inner join {2} b on a.up=scs_codi where indicador in ('NUM_MITJANA','DEN') and categoria<>'E' group by ics_codi,categoria,indicador\
    union select concat('AGDEMPC3',categoria),concat('A','periodo'),ics_codi,indicador,'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{4} a inner join {2} b on a.up=scs_codi where indicador in ('NUM','DEN') and categoria<>'E' group by ics_codi,categoria,indicador\
    union select concat('AGDEMMT3',categoria),concat('A','periodo'),ics_codi,if(indicador='NUM_MITJANA','NUM','DEN'),'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{4} a inner join {2} b on a.up=scs_codi where indicador in ('NUM_MITJANA','DEN') and categoria<>'E' group by ics_codi,categoria,indicador".format(db,taula,centres,taula1,taula2)
file = 'ACC'
error.append(exportKhalix(sql,file))

sql = "select concat('AGDEMPCT',categoria),concat('A','periodo'),CONCAT(ics_codi,if(categoria='P','M',categoria),uba),indicador,'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{1} a inner join {2} b on a.up=scs_codi where indicador in ('NUM','DEN') and categoria in ('M','I','P') and uba<>'' group by CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(categoria='P','M',categoria),indicador\
    union select concat('AGDEMMTJ',categoria),concat('A','periodo'),CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(indicador='NUM_MITJANA','NUM','DEN'),'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{1} a inner join {2} b on a.up=scs_codi where indicador in ('NUM_MITJANA','DEN') and categoria in ('M','I','P') and uba<>'' group by CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(categoria='P','M',categoria),indicador\
    union select concat('AGDEMPC2',categoria),concat('A','periodo'),CONCAT(ics_codi,if(categoria='P','M',categoria),uba),indicador,'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{3} a inner join {2} b on a.up=scs_codi where indicador in ('NUM','DEN') and categoria in ('M','I','P') and uba<>'' group by CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(categoria='P','M',categoria),indicador\
    union select concat('AGDEMMT2',categoria),concat('A','periodo'),CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(indicador='NUM_MITJANA','NUM','DEN'),'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{3} a inner join {2} b on a.up=scs_codi where indicador in ('NUM_MITJANA','DEN') and categoria in ('M','I','P') and uba<>'' group by CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(categoria='P','M',categoria),indicador\
    union select concat('AGDEMPC3',categoria),concat('A','periodo'),CONCAT(ics_codi,if(categoria='P','M',categoria),uba),indicador,'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{4} a inner join {2} b on a.up=scs_codi where indicador in ('NUM','DEN') and categoria in ('M','I','P') and uba<>'' group by CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(categoria='P','M',categoria),indicador\
    union select concat('AGDEMMT3',categoria),concat('A','periodo'),CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(indicador='NUM_MITJANA','NUM','DEN'),'NOCAT','NOIMP','DIM6SET','N',sum(recompte) from {0}.{4} a inner join {2} b on a.up=scs_codi where indicador in ('NUM_MITJANA','DEN') and categoria in ('M','I','P') and uba<>'' group by CONCAT(ics_codi,if(categoria='P','M',categoria),uba),if(categoria='P','M',categoria),indicador".format(db,taula,centres,taula1,taula2)
file = 'ACC_IND'
error.append(exportKhalix(sql,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('Fi Acc')  
