# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


INDICADOR = "AGASSDEP"
TABLE = "exp_khalix_{}_up".format(INDICADOR)
DATABASE = "altres"


class Indicador(object):
    """."""

    def __init__(self):
        """."""
        self.get_test()
        self.get_poblacio()
        self.get_indicador()
        self.upload_data()

    def get_test(self):
        """."""
        self.test = c.defaultdict(set)
        sql = "select id_cip_sec, dat \
               from ass_variables, nodrizas.dextraccio \
               where agrupador = 860 and \
                     dat between adddate(data_ext, interval -2 year) and \
                                 data_ext"
        for id, dat in u.getAll(sql, "nodrizas"):
            self.test[id].add(dat)

    def get_poblacio(self):
        """."""
        self.poblacio = c.defaultdict(set)
        sql = "select id_cip_sec, visi_up from ass_imputacio_up"
        for id, up in u.getAll(sql, "nodrizas"):
            self.poblacio[id].add(up)

    def get_indicador(self):
        """."""
        self.indicador = c.defaultdict(c.Counter)
        sql = "select id_cip_sec, inici, fi \
               from ass_embaras, nodrizas.dextraccio \
               where fi > adddate(data_ext, interval -1 year)"
        for id, ini, fi in u.getAll(sql, "nodrizas"):
            compleix = any([ini < dat < fi for dat in self.test[id]])
            for up in self.poblacio[id]:
                self.indicador[up]["DEN"] += 1
                if compleix:
                    self.indicador[up]["NUM"] += 1

    def upload_data(self):
        """."""
        cols = "(up varchar(5), indicador varchar(10), numerador double, denominador double)"  # noqa
        u.createTable(TABLE, cols, DATABASE, rm=True)
        upload = [(up, INDICADOR, dades["NUM"], dades["DEN"])
                  for (up, dades) in self.indicador.items()]
        u.listToTable(upload, TABLE, DATABASE)


if __name__ == "__main__":
    Indicador()
