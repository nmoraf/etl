# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import string
import random
import operator


ops = { ">": operator.gt, "=": operator.eq }

#Validation
validation=False
up_codi=("00380","00365",'00368', '00110')
#up_codi=("00380",)

alt = "altres"
nod = "nodrizas"
assig=nod+".assignada_tot"
odn=nod+".odn_variables"
peces_pat="import.odn507"
peces_trat="import.odn508"
#limit=2000

rev_agr=309


#peces
pat_trat_tuples_list=[
("CA","OBTt"),
("RR","OBTt") ,
("OF","OBTt"),
("OBTp","OBTt"),
("CA","CON"),
("RR","CON"), 
("OF","CON"),
("OBTp","CON"), 
("CA","PF"),
("RR","PF"), 
("OF","PF"),
("OBTp","PF") ,
]


pat_list=tuple({pat_trat[0] for pat_trat in pat_trat_tuples_list})
trat_list=tuple({pat_trat[1] for pat_trat in pat_trat_tuples_list})

#Agrupador indicador de ARC
agr_ARC_dict={286:"=1501",
			  307:">1",
			  308:">0"}

caries_temp={307:"=0"}
caries_def= {308:"=0"}
caries_all={308:"=0",307:"=0"}


#Cada uno de los agrupadores puede ser clasificado en 3 categorias:
# --> mitjana : Suma de los indices de caries
# --> pacient: Cuenta los pacientes con index de caries =0
# --> obturacio: Calcula la O por paciente

# Dentro de estas categorias se tienen los diccionarios de
# {	codi:	agrupadores/condiciones para calcular ese indicador}
codi_indicadors= {"mitjana": {"IAD0011":308,"IAD0012":307},
				  "pacient": {"IAD0013": (agr_ARC_dict,set.union),
				  			  "IAD0014":(caries_all,set.intersection),
				  			  "IAD0015":(caries_def, None),
							  "IAD0016":(caries_temp,None)

				  },
				  "obturacio": {"IAD0017": ([0,float('Inf')], [307,308]),
				  				"IAD0019": ([1,50], [307]),
				  				"IAD0018": ([50,float('Inf')], [308])}
								}

#Para calcular los subindices(en los que solo cambia la edad del denominador), se crea un diccionario con  {edat: subindex(letra de a hasta i)}

ind_edat={edat: letter for letter,edat in zip(string.ascii_lowercase[0:9],range(6,15)) }

#Funciones	

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_revision_odn(agr,nens):
	"""Selecciona los pacientes que presenten el agrupador agr. Se filtra por los ids que esten en el set nens.
	   Devuelve un set de ids
	"""
	sql="select id_cip_sec from {}.odn_variables where agrupador= {}".format(nod,agr)
	return {id for (id,) in getAll(sql,nod) if id in nens}


def get_obturacio_pat_trat(obt_peces_id,agr_list):
	""" Obtiene para cada paciente al que se le haya calculado un index el numero de obturaciones totales que se almacenan en obt_peces a nivel de paciente y 
		de tipo de denticion (agr). 
		Las obturaciones se obtienen de obt_peces, un diccionario a nivel de paciente(id) y de denticion(agr): {id:{agr: n_obts}}, al igual que el index que se obtiene
		del numerador_by_agr {id:{agr: index}}
		El numero de obturaciones totales y el index depende de agr_list, una lista con los agr que determina si se estudia a nivel de denticion permanente 
		(agr = 307), denticion temporal (agr=308) o ambas. 
		Devuelve un diccionario de tuples: {id: (obts,index)}

	"""
	num=Counter()
	ids_with_index={id for agr in agr_list for id in numerador_by_agr[agr]}
	for id in ids_with_index:
		obts=0
		index= sum([numerador_by_agr[agr][id] for agr in agr_list])
		if index != 0:
			if id in obt_peces:
				obts= sum([obt_peces[id][agr] for agr in agr_list])
			num[id]=(obts,index)
		
	return num

def get_peces_dat_by_criteris(pat_trat_dict):
	"""Se obtiene para cada paciente (que tenga un index de caries) y para cada una de sus piezas dentales, las patologias o tratamientos que nos interesan 
	   y las fechas de diagnostico de cada una de ellas.
	   Devuelve  {id: {peca:criteri{fecha}}}	

	"""
	
	peces_dat=defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(set))))
	
	for (column,taula),criteris in pat_trat_dict.iteritems():
		sql='select id_cip_sec, d{2}d_cod_p, d{2}d_{1}, d{2}d_data from import.{0};'.format(taula,column,column[0])
		printTime(sql)
		for id,peca,pat_trat,data in getAll(sql,"import"):
			pat_trat= '{}{}'.format(pat_trat,column[0]) if pat_trat=='OBT' else pat_trat
			#peces_dat[id][peca].setdefault(pat_trat,set())
			peces_dat[id][peca][column[0]][pat_trat].add(data)
	return peces_dat

def get_absencies():
	peces_A=defaultdict(set)
	sql="select id_cip_sec, dfd_cod_p from import.odn506 where dfd_p_a='A' and dfd_mot_a not in ('N','A')"
	for id,peca in getAll(sql,"import"):
		peces_A[id].add(peca)
	return peces_A

def get_obt(obt_peces,id,agr):
	O= True 
	C= False
	obt_peces[id][agr]+=1
	return O,C,obt_peces

def get_indexes(rev_id,peces_dat,peces_A,numerador_by_agr):
	""" Calcula los indices de restauracion a nivel de paciente (id) y denticion (agr, 307 si es denticion temporal y 308 si es denticion definitiva)
		utilizando los datos de patologia y tratamiento por paciente y pieza guardados en peces_dat, las piezas ausentes peces_A y las incorpora al diccionario
		numerador_by_agr. Tambien consigue las obturaciones a nivel de paciente (id) y denticion (agr).
		Devuelve dos diccionarios,
			-> numerador_by_agr: {id {agr: index} }
			-> obt_peces: {id: {agr: n_obts} }
	"""
	#Cogemos los ids o que bien han tenido una patologia/tratamiento o que tienen una pieza ausente o ambos
	ids_with_data=peces_dat.viewkeys() | peces_A.viewkeys()

	obt_peces=defaultdict(Counter)

	#para cada paciente que ha tenido una revision odontologica
	for id in rev_id:
		numerador_by_agr[307].setdefault(id,0)
		numerador_by_agr[308].setdefault(id,0)

		if id in ids_with_data:
			#cogemos las piezas que tienen datos
			peca_with_data= peces_dat[id].viewkeys() | peces_A[id]

			for peca in peca_with_data:
				#Initialization
				C,A,O= False,False,False
				max_pat,max_trat='',''
				#Comprobar si es denticion def/temp
				agr= 308 if peca <= 50 else 307

				#CALCULO C
				#se coge la patologia mas reciente para el diente
				try:
					max_pat=max(peces_dat[id][peca]['p'].iteritems(), key=operator.itemgetter(1))[0]
				except:
					pass

				#se coge el tratamiento mas reciente para el diente
				try:
					max_trat=max(peces_dat[id][peca]['t'].iteritems(), key=operator.itemgetter(1))[0]
				except:
					pass
				
				C=  max_pat in ["CA","RR","OF","OBTp"]

				#CALCULO A
				#se mira si esta ausente la pieza si la patologia mas reciente es ca, rr, of o obt (C es True) o sino ha tenido ninguna patologia. 
				#En caso contrario no cuenta
				A= peca in peces_A[id] if C or max_pat=='' else False
				
				#CALCULO O
				#si no esta ausente la pieza, se calcula la O
				if not A:
					#se itera por las parejas patologia, tratamiento que definen una obturacion
					for pat,trat in pat_trat_tuples_list:
						# si el tratamiento mas reciente en esa pieza es 'OBTt' o
						# si esa pieza ha tenido esa patologia y ese tratamiento en ese diente Y
						# si la fecha del tratamiento es mas reciente o la misma que la patologia pareja o de la patología más reciente	
						if max_trat=='OBTt' or (peces_dat[id][peca]['p'][pat] and peces_dat[id][peca]['t'][trat] and \
							((max(peces_dat[id][peca]['p'][pat]) <= max(peces_dat[id][peca]['t'][trat])) \
								or \
								( max(peces_dat[id][peca]['t'][trat]) >= max(peces_dat[id][peca]['p'][max_pat]) ))) :
							
							#O es True y se cambia C a False
							O=True 
							if C:
								C=False
							#se anyade una obturacion al contador de obt para ese paciente y esa denticion, y se rompe el loop
							obt_peces[id][agr]+=1
							break
				

				#si la pieza esta ausente, C es False y para el COD se vuelve false para que no cuente en el index
				if A:
					C=False 
					if agr==307:
						A= False
				
				#Alguno de los tres cuenta como index
				if C or O or A:
					numerador_by_agr[agr][id]+=1
			
	return numerador_by_agr,obt_peces


def get_ids_by_agr(current_date):
	""" Selecciona los id_cip_seq de ninos que presentan un ARC segun el agrupador en la tabla odn_variables y guarda el valor de este agrupador
		Devuelve un diccionario de {id:valor_agr}
	"""
	sql = "select id_cip_sec,valor,dat,agrupador from {0} where agrupador =286 ;".format(odn)
	agr_id_valor=defaultdict(dict)
	for id,valor,dat,agr in getAll(sql,nod):
		if dat and monthsBetween(dat,current_date)<24:
			agr_id_valor[agr][id]=valor
	return agr_id_valor

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

def get_nens():
	""" Se obtiene para cada id de nino, la upOrigen y su edad y se guarda en un diccionario.
		Devuelve {id: (up,edat)}
	"""
	sql = "select id_cip_sec, upOrigen,edat from {} where edat >=6 and edat <= 14 and ates=1".format(assig)
	return {id: (up,edat) for id, up,edat in getAll(sql,nod)} 

def get_uba_pacient_tables(nens,numerador,codi,numerador_type,id_to_hash,rev_ids):
	""" Se generan el indicador en cuenta que tipo de numerador es necesario y en el caso de ser a nivel de paciente, se guardan los no cumplidores.
		Se itera por el diccionario de nens, donde se obtiene la up y la edat. Con la edat se obtiene el subindicador al que pertenece.
		El indicador se calcula agrupando por la up y el subindicador y a la hora de iterar dentro de una misma up, por los subindicadores
		que se han ido calculando. Para calcular el global, se van sumando en num_total y den_total cada uno de los valores de los subindicadores.

		Devuelve doss listas de filas (tuple) a nivel de up y a nivel de paciente.
	"""
	
	uba_rows=[]
	row_pacient=[]
	pacient_rows=[]
	
	indicador=defaultdict(lambda: defaultdict(Counter))
	#sql = "select id_cip_sec, upOrigen,edat from {} where edat >=6 and edat <= 14 and ates=1 ".format(assig)
	for id in rev_ids:
		up,edat=nens[id]
		sub_index=ind_edat[edat]
		if numerador_type in ('mitjana','pacient'):
			indicador[up][sub_index]['DEN']+=1
			
		if id in numerador:
			if numerador_type=="mitjana": 
				indicador[up][sub_index]['NUM']+=numerador[id]
						
			elif numerador_type=="obturacio":
				indicador[up][sub_index]['NUM']+=numerador[id][0]
				indicador[up][sub_index]['DEN']+=numerador[id][1]
						

			elif numerador_type=="pacient":
				indicador[up][sub_index]['NUM']+=1
                
			if codi == 'IAD0013':
				hash,sector=id_to_hash[id]
				for sub in (ind_edat[edat],''):
					pacient_rows.append((hash,sector, codi+sub,up,"sense","sense",None, 0))

		elif numerador_type=="pacient" and id not in numerador and codi != 'IAD0013':
			hash,sector=id_to_hash[id]
			for sub in (ind_edat[edat],''):
				pacient_rows.append((hash,sector, codi+sub,up,"sense","sense",None, 0))
			
	for up in indicador:
		num_total=0
		den_total=0
		for sub_index in indicador[up]:
			num_total+= indicador[up][sub_index]['NUM']
			den_total+= indicador[up][sub_index]['DEN']
			if indicador[up][sub_index]['DEN'] != 0:
				uba_rows.append((up, codi+sub_index,"sense", "O", indicador[up][sub_index]['NUM'],indicador[up][sub_index]['DEN'],indicador[up][sub_index]['NUM']/float(indicador[up][sub_index]['DEN']) ))
		
		if den_total !=0:
			uba_rows.append((up, codi,"sense", "O", num_total,den_total, num_total/float(den_total)) )  
	#export_txt_file("{}_{}.txt".format(numerador_type,codi),["CIP","SECTOR","CODI","UP","Obturacions","Index"],row_pacient)
	
	return uba_rows,pacient_rows


def validation_by_up_pacient(up_tuple,table_name,table_columns):
	sql="select * from altres.exp_ecap_index_caries_{0} where up in ({1})".format(table_name,",".join(up_tuple))
	up_rows= [(row) for row in getAll(sql,alt)]
	export_table("exp_ecap_index_caries_{0}".format(table_name),
				table_columns,
				alt,
				up_rows)
	
def get_id_hash():
    sql="select id_cip_sec,hash_d,codi_sector from import.u11"
    return {id:(hash_d,sector) for (id,hash_d,sector) in getAll(sql,"import")}

def get_cip_dict(hashd):
    sql="select usua_cip,usua_cip_cod from pdptb101 where usua_cip_cod='{}'".format(hashd)
    return getOne(sql,"pdp")[0]

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)


if __name__ == '__main__':
# comencem!

	#### VARIABLES COMUNES QUE SE NECESITAN PARA CASI TODOS LOS INDICADORES ###
	#se crea la tabla donde van a ir todos los indicadores
	printTime('inici')
	current_date=get_date_dextraccio()
	id_to_hash=get_id_hash()
	
	table_name="exp_ecap_index_caries_uba"
	uba_columns="(up varchar(5),indicador varchar(15),uba varchar(5),tipus varchar(5),numerador double, denominador int, resultat double)"
	createTable(table_name,uba_columns,alt,rm=True)

	table_name_pacient="exp_ecap_index_caries_pacient"
	pacient_columns="(hash varchar(100),sector int,indicador varchar(15),up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), exclos int)"	
	createTable(table_name_pacient,pacient_columns,alt,rm=True)

	#se obtienen para el agrupador de ARC (286), los ids de los pacientes -> {agr:set(ids)}
	numerador_by_agr=get_ids_by_agr(current_date) 
	printTime('Agr-Id')

	#para todas las patologias y tratamientos en pat_list y trat_list respectivamente, se obtiene los ids de los pacientes que la han sufrido y 
	#para cada uno de estos las piezas en donde lo han sufrido y la fecha de diagnostico
	print(pat_list)
	print(trat_list)
	peces_dat=get_peces_dat_by_criteris({
										("pat","odn507"):pat_list, 
										("tra","odn508"):trat_list		
										})
	
	printTime('{} peces_dat done'.format(len(peces_dat)))

	#Se obtienen las piezas ausentes
	peces_A=get_absencies()

	#Se obtienen los ninos
	nens=get_nens()
	printTime('nens',len(nens))

	#Se obtienen los ninos con revision odontologica
	rev_ids=get_revision_odn(rev_agr,nens)
	printTime("rev_ids", len(rev_ids))

	#Se obtienen los index de caries, que se guardan a nivel de paciente (id) y denticion (agr) en numerador_by_agr y 
	#las obturaciones en obt_peces a nivel de paciente (id) y denticion (agr)
	numerador_by_agr,obt_peces=get_indexes(rev_ids,peces_dat,peces_A,numerador_by_agr)
	printTime('indexes')
	printTime(len(numerador_by_agr[307]))
	printTime(len(numerador_by_agr[308]))

	#se obtiene el denominador total -> para cada rango de edad (letter-el subindex-), se obtienen {up: set(ids)}
	
	
	

	## MONTAR LOS INDICADORES ##

	for numerador_type, codi_agr_dict in codi_indicadors.iteritems():
		#Iteramos sobre tipo de numerador (mitjana,pacient,obturacio) y los diccionarios con los codigos globales
		for codi,agr_options in codi_agr_dict.iteritems():
			#Para cada codigo segun numerador type, con su diccionario de opciones correspondiente
			print(codi)
			print(agr_options) 
			if numerador_type=="mitjana":
				#si es mitjana, el numerador va a ser el valor del agrupador que hay en opciones para el indicador
				num=numerador_by_agr[agr_options]
					
			elif numerador_type=="pacient":
				#si es paciente, queremos a los pacientes con ciertos index de caries
				# Cuando se requiere mas de un index, es necesario especificar si son pacientes con ambos indices o con un index u otro, por eso en las
				#opciones para estos indicadores (agr_options) es un tuple (agr,set_operation) donde agr es un diccionario con {agr:valor agr} y set_operation es la
				# operacion "union" o "interseccion"
				agr,set_operation=agr_options
				print(set_operation)
				if set_operation:
					# se crean sets de ids que presentan el agrupador y el valor especificado y cada uno de estos sets se garda en una lista. La lista resultante de sets se pasa
					# a la operacion set en cuestion (union o interseccion) 
					num=set_operation(*[{id for id, val in numerador_by_agr[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:]))} for agrupador,valor in agr.iteritems()])
					printTime('Get nens amb {} OK'.format(agr))
				else:
					num={id for agrupador,valor in agr.iteritems() for id,val in numerador_by_agr[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:])) }
					printTime('Get nens amb {} OK'.format(agr))
				
			elif numerador_type=="obturacio":
				#se obtienen las piezas en las que hay que buscar por obturaciones y el la lista de agrupadores que se necesitan para el denominador de la O
				peces,agr_list=agr_options
					
				num=get_obturacio_pat_trat(obt_peces,agr_list)
				print(len(num))
					
					
			uba_rows,pacient_rows=get_uba_pacient_tables(nens,num,codi,numerador_type,id_to_hash,rev_ids)
			printTime('Retrieving rows new table OK codi {}'.format(codi))

			listToTable(uba_rows,table_name,alt)
			listToTable(pacient_rows,table_name_pacient,alt)
			printTime("Insert into the table {}".format(codi))

	if validation:
		validation_by_up_pacient(up_codi,"uba",uba_columns)
		validation_by_up_pacient(up_codi,"pacient",pacient_columns)

    # export a klx
	sql = ["select indicador, concat('A', 'periodo'), ics_codi, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', numerador \
            from altres.exp_ecap_index_caries_uba a \
            inner join nodrizas.cat_centres b on a.up = b.scs_codi"]
	sql.append("select indicador, concat('A', 'periodo'), ics_codi, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', denominador \
                from altres.exp_ecap_index_caries_uba a \
                inner join nodrizas.cat_centres b on a.up = b.scs_codi")
	exportKhalix(" union ".join(sql), "INDEX_CARIES")
