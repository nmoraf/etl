from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from rehabilitacion import *

nod="nodrizas"
imp="import"
validation=True
limit=""


def get_ids(motiu_tanca):
    sql="select concat(codi_sector,'-',fit_id),fit_up from import.rhb07 where fit_data_inici = fit_data_registre and fit_motiu_tanca='{}' ; ".format(motiu_tanca)	
    fitxas_ids={(id,up) for id,up in getAll(sql,imp)}
    return fitxas_ids

def get_sessions_up(fitxas_ids,visites):
    indicador=defaultdict(Counter)
   
    for (id,up_codi) in fitxas_ids:
        up=get_up_desc(up_codi)
        if id in visites:
            indicador[up][id]+=visites[id]
    return indicador

def get_up_desc(up_codi):
    sql_008="select UP_CODI_UP_ICS from import.cat_gcctb008 where UP_CODI_UP_SCS='{}';".format(up_codi)
    up_codi_ics=getOne(sql_008,imp)[0]
    sql_007="select UP_DESC_UP_ICS from import.cat_gcctb007 where UP_CODI_UP_ICS='{}';".format(up_codi_ics)
    return getOne(sql_007,imp)[0]

if __name__ == '__main__':
    pacient_ids=get_ids("AV")
    printTime("Pacient ids")
    visites=get_vis_per_fitxa(("S"," "))

    printTime("Sessiones")
    indicador=get_sessions_up(pacient_ids,visites)
    print(sum(len(indicador[key]) for key in indicador))
    for key in indicador:
        print(key)
        print(len(indicador[key]))
        for id in indicador[key]:
            print(id, indicador[key][id])
        
