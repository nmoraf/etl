from sisapUtils import getAll, createTable, listToTable, exportKhalix
from datetime import timedelta
from collections import defaultdict


nod = 'nodrizas'
db = 'altres'
tb = 'exp_khalix_ag_assir_acc'


def get_visites():
    visites_ive, visites_emb = {}, defaultdict(set)
    sql = 'select id_cip_sec, codi_sector, visi_tipus_visita, visi_tipus_citacio, visi_dia_peticio, visi_data_visita from ass_visites'
    for id, sector, tipus, cita, dpeti, dvisi in getAll(sql, nod):
        if dpeti and dvisi:
            diff = (dvisi - dpeti).days
            visites_ive[(id, dvisi)] = diff <= 10
            visites_emb[id].add((dpeti, diff <= 10, dvisi))
    return visites_ive, visites_emb


def get_imputacio():
    imputacio = defaultdict(set)
    for id, up in getAll('select id_cip_sec, visi_up from ass_imputacio_up', nod):
        imputacio[id].add(up)
    return imputacio


def get_ive():
    num, den = defaultdict(int), defaultdict(int)
    sql = "select id_cip_sec, dat from ass_variables where agrupador = 636 and val_num = 0"
    for id, dat in getAll(sql, nod):
        if id in imputacio:
            ups = imputacio[id]
            for up in ups:
                if (id, dat) in visites_ive:
                    den[up] += 1
                    if visites_ive[(id, dat)]:
                        num[up] += 1
    upload = [('AGACCIVE', up, 'DEN', n) for up, n in den.items()]
    upload.extend([('AGACCIVE', up, 'NUM', n) for up, n in num.items()])
    listToTable(upload, tb, db)


def get_embaras():
    vis_dur = defaultdict(list)
    vis_ok = set()
    sql = 'select id_cip_sec, inici from ass_embaras, nodrizas.dextraccio where inici between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext'
    for id, dur in getAll(sql, nod):
        if id in imputacio:
            first = dur + timedelta(weeks=8)
            last = dur + timedelta(weeks=11) - timedelta(days=1)
            twelve =  dur + timedelta(weeks=13) - timedelta(days=1)
            for (peti, good, visi) in visites_emb[id]:
                if peti > dur:
                    vis_dur[id].append((peti, first <= peti <= last, good))
                if dur <= visi <= twelve:
                    vis_ok.add(id)
    num, den = defaultdict(int), defaultdict(int)
    for id, dades in vis_dur.items():
        primera = sorted(dades)[0]
        if primera[1]:
            ups = imputacio[id]
            for up in ups:
                den[up] += 1
                num[up] += id in vis_ok
    upload = [('AGACCEMB', up, 'DEN', n) for up, n in den.items()]
    upload.extend([('AGACCEMB', up, 'NUM', n) for up, n in num.items()])
    listToTable(upload, tb, db)


def export_klx():
    error = []
    centres = "nodrizas.ass_centres"
    sql = "select indicador, concat('A','periodo'), br_assir, tipus, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(recompte) from {}.{} a inner join {} b on a.up = b.up group by 1, 3, 4".format(db, tb, centres)
    file = 'AG_ASSIR_ACC_UP'
    error.append(exportKhalix(sql, file))
    if any(error):
        sys.exit("he hagut d'escriure al directori de rescat")


if __name__ == '__main__':
    createTable(tb, '(indicador varchar(10), up varchar(5), tipus varchar(3), recompte int)', db, rm=True)
    visites_ive, visites_emb = get_visites()
    imputacio = get_imputacio()
    get_ive()
    get_embaras()
    export_klx()
