# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


nod = 'nodrizas'
db = 'altres'


class Fentanil(object):
    """."""

    def __init__(self):
        """."""
        self.get_denominador()
        self.get_poblacio()
        self.get_numeradors()
        self.get_indicadors()
        self.get_hash()
        self.set_mst()
        self.set_pacients()
        self.set_resultats()
        self.set_catalegs()

    def get_denominador(self):
        """."""
        sql = "select id_cip_sec from eqa_tractaments where farmac = 763"
        self.denominador = set([id for id, in u.getAll(sql, nod)])

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf from assignada_tot"
        self.poblacio = {id: (up, uba, ubainf) for (id, up, uba, ubainf)
                         in u.getAll(sql, nod) if id in self.denominador}

    def get_numeradors(self):
        """."""
        self.numerador = {}
        indicadors = {
            'FARM0005': "select id_cip_sec from eqa_tractaments \
                         where farmac = 762",
            'FARM0006': "select id_cip_sec from eqa_problemes where ps = 722"}
        for codi, sql in indicadors.items():
            self.numerador[codi] = set([id for id, in u.getAll(sql, nod)
                                        if id in self.poblacio])

    def get_indicadors(self):
        """."""
        self.pacients = []
        for id, (up, uba, ubainf) in self.poblacio.items():
            for codi, pacients in self.numerador.items():
                alerta = id not in pacients
                self.pacients.append((id, up, uba, ubainf, codi, alerta))

    def get_hash(self):
        """."""
        pacients = set([row[0] for row in self.pacients])
        sql = "select id_cip_sec, hash_d, codi_sector from eqa_u11"
        self.hash = {id: (hash, sector) for (id, hash, sector)
                     in u.getAll(sql, nod) if id in pacients}

    def delete_rows(self, tb, column='indicador'):
        """."""
        sql = "delete from {} where {} in {}".format(tb, column,
                                                     tuple(self.numerador))
        u.execute(sql, db)

    def set_mst(self):
        """."""
        tb = 'mst_alertes_pacient'
        self.delete_rows(tb, 'indicador')
        upload = [(id, codi, 1 * alerta)
                  for (id, up, uba, ubainf, codi, alerta) in self.pacients]
        u.listToTable(upload, tb, db)

    def set_pacients(self):
        """."""
        tb = 'exp_ecap_alertes_pacient'
        self.delete_rows(tb, 'grup_codi')
        upload = [(id, up, uba, up, ubainf, codi, 0) + self.hash[id]
                  for (id, up, uba, ubainf, codi, alerta)
                  in self.pacients
                  if alerta]
        u.listToTable(upload, tb, db)

    def set_resultats(self):
        """."""
        tb = 'exp_ecap_alertes_uba'
        self.delete_rows(tb)
        denominador, numerador = c.Counter(), c.Counter()
        for id, up, uba, ubainf, codi, alerta in self.pacients:
            denominador[(up, uba, 'M', codi)] += 1
            numerador[(up, uba, 'M', codi)] += alerta
            denominador[(up, ubainf, 'I', codi)] += 1
            numerador[(up, ubainf, 'I', codi)] += alerta
        upload = [(up, uba, tipus, codi,
                   numerador[(up, uba, tipus, codi)], n,
                   numerador[(up, uba, tipus, codi)] / float(n))
                  for (up, uba, tipus, codi), n in denominador.items()]
        u.listToTable(upload, tb, db)

    def set_catalegs(self):
        """."""
        tb = 'exp_ecap_alertes_cataleg'
        self.delete_rows(tb)
        pare = 'FARM02'
        upload = [('FARM0005', 'Fentanil r�pid sense opiaci forte de base', 1,
                   pare, 1, 0, 0, 0, 0, 1,
                   'http://sisap-umi.eines.portalics/indicador/codi/FARM0005',
                   'PCT'),
                  ('FARM0006', 'Fentanil r�pid sense diagn�stic oncol�gic', 2,
                   pare, 1, 0, 0, 0, 0, 1,
                   'http://sisap-umi.eines.portalics/indicador/codi/FARM0006',
                   'PCT')]
        u.listToTable(upload, tb, db)
        tb = 'exp_ecap_alertes_catalegpare'
        u.execute("delete from {} where pare = '{}'".format(tb, pare), db)
        upload = [(pare, 'Alertes de seguretat: Fentanil r�pid', 1, 'ALERTES')]
        u.listToTable(upload, tb, db)


if __name__ == '__main__':
    Fentanil()
