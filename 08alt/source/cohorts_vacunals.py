# coding: utf8

"""
Vacunes segons any de naixement
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u


db = 'permanent'

codis_vacunes = { 'Triple vírica (XRP)': 38,
                  'Tètanus': 49,
                  'Poliomieltitis (VPI)': 311,
                  'Haemophilus influenzae (Hib)': 312,
                  'Hepatitis B': 15,
                  'Meningocòccica invasora serogrup C (MCC)': 313,
                  'Malaltia neumocòccica invasora 13 valent(VNC13)': 698,
                  'Varicel·la (VVZ)': 314,
                  'Virus del papil·loma humà (VPH)': 782,
                  'Hepatitis A': 34,
                  'Rotavirus': 790,
                  'Tos ferina': 631,
                  'Malaltia neumocòccica invasora 23 valent(VNC23)': 48,
                  'Malaltia meningocòccica invasora serogrup B': 548,
                  'Malaltia meningocòccica tetravalent (MACYW)': 883,
                  }   

class vacunesCohorts(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_rs()
        self.get_centres()
        self.get_vacunes()
        self.get_pob()        
        self.export_data()
        self.export_cataleg()
        self.table_dates()
        self.table_dates()
        
    def get_rs(self):
        """."""
        self.rs_cat = {}
        sql = 'select rs_cod, rs_des from cat_sisap_agas'
        for rs, des in u.getAll(sql, 'import'):
            self.rs_cat[int(rs)] = des
    
    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        upload = []
        sql = "select scs_codi, ics_codi, ics_desc, right(rs, 2), amb_desc, ep from cat_centres"
        for up, br, desc, rs, ambit, ep in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
            upload.append([up, br, desc, self.rs_cat[int(rs)], ambit, ep])
           
        u.listToTable(upload, tb_eap, db)
    
    def get_vacunes(self):
        """aconseguim vacunes"""
        self.vacunats = {}
        sql = 'select id_cip_sec, agrupador, dosis from ped_vacunes'
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi

        sql = 'select id_cip_sec, agrupador, dosis from eqa_vacunes'
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi
                
    def get_pob(self):
        """agafem població segons criteris edat"""
        self.resultats = Counter()
        sql = "select id_cip_sec, sexe, year(data_naix), up from assignada_tot where ates=1"
        for id, sexe, any_naix, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                for c in codis_vacunes:
                    agr = codis_vacunes[c]
                    self.resultats[(any_naix, up, sexe, agr, 'den')] += 1
                    if (id, agr) in self.vacunats:
                        dosi = self.vacunats[(id, agr)]
                        self.resultats[(any_naix, up, sexe,agr, 'num1')] += 1 if dosi > 0 else 0
                        self.resultats[(any_naix, up, sexe, agr, 'num2')] += 1 if dosi > 1 else 0
                        self.resultats[(any_naix, up, sexe, agr, 'num3')] += 1 if dosi > 2 else 0
                        self.resultats[(any_naix, up, sexe, agr, 'num4')] += 1 if dosi > 3 else 0
                        self.resultats[(any_naix, up, sexe, agr,'num5')] += 1 if dosi > 4 else 0
                            
    def export_data(self):
        """."""
        upload = []
        for (any_naix, up, sexe, agr, tip), n in self.resultats.items():
            if tip == 'den':
                num1, num2, num3, num4, num5 = self.resultats[(any_naix, up, sexe, agr, 'num1')],self.resultats[(any_naix,up, sexe, agr, 'num2')],self.resultats[(any_naix,up, sexe, agr, 'num3')],self.resultats[(any_naix,up, sexe, agr, 'num4')],self.resultats[(any_naix,up, sexe, agr, 'num5')]
                upload.append([any_naix, up, sexe, agr, num1, num2, num3, num4, num5, n])

        u.listToTable(upload, tb, db)     
        
    def export_cataleg(self):
        """."""
        upload = []
        for c in codis_vacunes:
            agr = codis_vacunes[c]
            upload.append([agr, c])
            
        u.listToTable(upload, cataleg, db) 

    def table_dates(self):
        """."""
        upload = []
        sql = "select data_ext from dextraccio"
        for data_ext, in u.getAll(sql, 'nodrizas'):
            upload.append([data_ext])
           
        u.listToTable(upload, tb_date, db)  

   
if __name__ == '__main__':
    u.printTime("Inici")
    
    tb = "mst_cohorts_vacunals"
    columns =  ["Cohort int", "up varchar(5)", "sexe varchar(10)", "codi_vacuna int", "1dosi int", "2dosis int", "3dosis int", "4dosis int", "5dosis int", "poblacio int"]     
    u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    
    cataleg = "mst_cohorts_cataleg"
    columns = ["codi_vacuna int", "literal varchar(300)"]
    u.createTable(cataleg, "({})".format(", ".join(columns)), db, rm=True)
    
    tb_date = "mst_cohorts_data"
    columns = ["data_calcul date"]
    u.createTable(tb_date, "({})".format(", ".join(columns)), db, rm=True)
    
    tb_eap = "mst_cohorts_eaps"
    columns = ["up varchar(5)", "br varchar(5)", "descripcio varchar(500)", "RS varchar(100)", "ambit varchar(100)", "ep varchar(5)"]
    u.createTable(tb_eap, "({})".format(", ".join(columns)), db, rm=True)
    
    vacunesCohorts()
    
    u.printTime("Fi")