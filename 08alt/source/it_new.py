# coding: latin1

"""
.
"""

import collections as c
import simplejson as j
import os

import sisapUtils as u


TABLE = "exp_khalix_it"
DATABASE = "altres"
FILE_UP = "IT_NOU"
FILE_UBA = "IT_UBA_NOU"
FILE_ICAM = u.tempFolder + "IT_SISAP_ICAM.CSV"

COST = {2017: 36.2, 2018: 36.3}
GRUPS = {"M": "OST", "F": "MEN", "S": "TRA", "T": "TRA",
         "R": "SIG", "default": "ALT"}
DURADES = {(1, 366): "A", (366, 546): "B"}


class IT(object):
    """."""

    def __init__(self):
        """."""
        self.objectes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.get_actius()
        self.get_altes()
        self.get_spec()
        self.get_indicadors()
        self.get_centres()
        self.get_khalix()
        self.export_khalix()
        if u.IS_MENSUAL:
            self.get_periodes()
            self.get_icam()
            self.export_icam()

    def get_actius(self):
        """."""
        self.actius = {}
        sql = "select id_cip, up, uba, edat, sexe \
               from assignada_tot \
               where edat >= 16 and nivell_cobertura = 'AC' and relacio = 'T'"
        for id, up, uba, edat, sexe in u.getAll(sql, "nodrizas"):
            key = (up, uba, u.ageConverter(edat), u.sexConverter(sexe))
            self.actius[id] = key
            self.objectes["actius"][key][("numero", "brut")] += 1

    @staticmethod
    def get_grup(ps):
        """."""
        for patro, grup in GRUPS.items():
            if ps[0] == patro or ps[:5] == "C01-{}".format(patro):
                return grup
        return GRUPS["default"]

    @staticmethod
    def get_durada(dur):
        """."""
        for dies, literal in DURADES.items():
            if min(dur, 545) in range(*dies):
                return literal

    def get_altes(self):
        sql = "select id_cip, ilt_prob_salut, \
                      datediff(ilt_data_alta, ilt_data_baixa) + 1, \
                      ilt_durada_opt, ilt_data_alta \
               from baixes, nodrizas.dextraccio \
               where ilt_data_alta between \
                            adddate(data_ext, interval -1 year) and \
                            data_ext and \
                     ilt_data_alta >= ilt_data_baixa and \
                     ilt_cau_codi_baixa in ('MC', 'ANL', 'P-PO')"
        for id, ps, dur, opt, alta in u.getAll(sql, "import"):
            if id in self.actius:
                grup = IT.get_grup(ps)
                durada = IT.get_durada(dur)
                periode = alta.strftime("%Y%m")
                key = self.actius[id] + (grup, durada, periode)
                cost = COST.get(alta.year, COST[max(COST.keys())])
                self.objectes["altes"][key][("numero", "total")] += 1
                self.objectes["altes"][key][("dies", "total")] += dur
                self.objectes["altes"][key][("cost", "total")] += dur * cost
                if dur <= opt:
                    self.objectes["altes"][key][("numero", "optim")] += 1
                else:
                    exces = dur - opt
                    self.objectes["altes"][key][("dies", "exces")] += exces
                    self.objectes["altes"][key][("cost", "exces")] += exces * cost  # noqa

    def get_spec(self):
        """."""
        file = "dades_noesb/it_new.json"
        try:
            stream = open(file)
        except IOError:
            stream = open("../" + file)
        self.spec = j.load(stream)

    def get_indicadors(self):
        """."""
        self.indicadors = c.Counter()
        for cod, param in self.spec.items():
            for analysis, spec in param.items():
                obj = spec["objecte"]
                clau = tuple(spec["clau"])
                tip = analysis.upper()[:3]
                for key, values in self.objectes[obj].items():
                    val = values[clau]
                    if obj == "actius":
                        self.indicadors[key + (cod + "TOT", tip)] = val
                        for grup in set(GRUPS.values()):
                            for durada in DURADES.values() + [""]:
                                ind = cod + grup + durada
                                self.indicadors[key + (ind, tip)] = val
                    else:
                        inds = (cod + key[4] + key[5],
                                cod + key[4],
                                cod + "TOT")
                        for ind in inds:
                            self.indicadors[key[:4] + (ind, tip)] += val

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi, abs from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (up, uba, edat, sexe, ind, anal), n in self.indicadors.items():
            br = self.centres[up][0]
            self.khalix[(br, edat, sexe, ind, anal)] += n
            self.khalix[(br + "M" + uba, "NOCAT", "DIM6SET", ind, anal)] += n

    def export_khalix(self):
        """."""
        cols = "(entity varchar(11), edat varchar(10), sexe varchar(10), \
                 indicador varchar(10), analisi varchar(10), valor double)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        export = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(export, TABLE, DATABASE)
        for symbol, file in (("=", FILE_UP), (">", FILE_UBA)):
            sql = "select indicador, 'Aperiodo', entity, analisi, edat, \
                          'NOINSAT', sexe, 'N', valor \
                   from {}.{} \
                   where length(entity) {} 5".format(DATABASE, TABLE, symbol)
            u.exportKhalix(sql.format(DATABASE, TABLE), file)

    def get_periodes(self):
        """."""
        sql = "select \
                  date_format(adddate(data_ext, interval -1 month), '%Y%m'), \
                  date_format(data_ext, '%Y%m') \
               from dextraccio"
        self.periodes = u.getOne(sql, "nodrizas")

    def get_icam(self):
        """."""
        self.icam = c.defaultdict(c.Counter)
        for key, dades in self.objectes["actius"].items():
            up = key[0]
            actius = dades[("numero", "brut")]
            for periode in self.periodes:
                for grup in set(GRUPS.values()):
                    for durada in DURADES.values():
                        self.icam[(periode, up, grup, durada)]["actius"] += actius  # noqa
        for key, dades in self.objectes["altes"].items():
            up, _r, _r, _r, grup, durada, periode = key
            if periode in self.periodes:
                for concepte, n in dades.items():
                    self.icam[(periode, up, grup, durada)][concepte] += n

    def export_icam(self):
        """."""
        export = []
        conceptes = ["actius", ("numero", "total"), ("numero", "optim"),
                     ("dies", "total"), ("dies", "exces"), ("cost", "total"),
                     ("cost", "exces")]
        for (periode, up, grup, durada), dades in self.icam.items():
            abs = self.centres[up][1]
            this = [periode, up, abs, grup, "<=1a" if durada == "A" else ">1a"]
            for concepte in conceptes:
                this.append(str(round(dades[concepte], 2)).replace(".", ","))
            export.append(this)
        header = [("periode", "up", "abs", "grup", "durada", "actius",
                   "numero_total", "numero_optim", "dies_total", "dies_exces",
                   "cost_total", "cost_exces")]
        u.writeCSV(FILE_ICAM, header + sorted(export), sep=";")
        to = ["constanca.alberti@gencat.cat", "mfabregase@gencat.cat"]
        subject = "Dades SISAP - ICAM"
        text = "Us adjuntem les dades de IT dels periodes {} (complet) i {} (parcial)."  # noqa
        u.sendPolite(to, subject, text.format(*self.periodes), file=FILE_ICAM)
        os.remove(FILE_ICAM)


if __name__ == "__main__":
    IT()
