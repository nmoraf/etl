# -*- coding: utf8 -*-

"""
Cartera de serveis ICS.
"""

import collections as c

import sisapUtils as u

sql = 'select data_ext, date_add(date_add(data_ext, interval - 12 month), interval +1 day) from nodrizas.dextraccio'
for a, b in u.getAll(sql, 'nodrizas'):
    actual = a
    Fa1year = b

db = 'altres'
table = 'exp_khalix_cartera'
create = "(cuentas varchar(10), br varchar(5), tipus varchar(10), poblacio varchar(10),  recompte int)"
u.createTable(table,create,db, rm=True)

tancament = False  # marca si podem agafar variables1 i activitats1


serveis = [
    ('01', 'Retinografia', [('variables', ['RGRAD', 'RGRAE', 'VF3001', 'VF3002']),
                            ('activitats', ['434', 'CROC', 'RGRAD', 'RGRAE'])], None),
    ('02', 'Crioterapia', [('activitats', ['521', 'PROVA', '86.3'])], None),
    ('03', 'ITB', [('variables', ['TK2011', 'TK2012', 'TK2013', 'TK2014',
                                  'TK201D', 'TK201E','I:T/B', 'IT', 'I_/T/B', 'I_T/B', 'PD-403']),
                    ('nics', ['3480']),
                    ('nics2', ['3480']),
                    ('activitats', ['I:T/B'])], None),
    ('04', 'Infiltracions', [('activitats', ['VQL01', 'URG10', 'URG11'])], None),
    ('05', 'Prick test', [('variables', ['TS1001'])], None),
    ('06', 'Petita cirurgia', [('activitats', ['510','514']),
                                ('nics', ['3440']), ('nics2', ['3440'])],None),
    ('07', 'Molluscum', [('activitats', ['514']), ('variables', ['QS2001'])],
                        [('problemes', ['B08.1', 'cutre'])]),
    ('08', 'MAPA', [('variables', ['EK2081', 'EK2082', 'EK2083',
                                   'EK2084', 'EK2085', 'EK2086',
                                   'EK4012', 'EK4013', 'EK4014'])], None),
    ('09', 'AMPA', [('variables', ['ALS09', 'ALS10', 'EK2091', 'EK2092',
                                   'EK2093', 'EK2094', 'EK2095',
                                   'EK2096'])], None),
    ('10', 'Taps', [('activitats', ['URG16','582']),
                    ('nics', ['1640']),('nics2', ['1640'])], None),
    ('11', 'Peak flow', [('variables', ['TR305', 'PFLOW', 'VALO12'])], None),
    ('12', 'Variabilitat FEM', [('variables', ['VR105P'])], None),
    ('13', 'Espirometria', [('variables', ['TR3030', 'TR3031', 'TR3032',
                                           'TR3033', 'TR3034', 'TR3035',
                                           'TR3036', 'TR3037', 'PBD', 'BF/F', 'PESPE',
                                            'TR3037', 'TR3017', 'TR302B','EF/F']),
                            ('activitats', ['R301'])], None),
    ('14', 'Dopple vascular', [('activitats', ['ECOD']),
                               ('proves', ['RA00436', 'RA00438',
                                           'RA00439'])], None),
    ('15', 'Audiometria', [('activitats', ['44', '440', '449']),
                           ('proves', ['PD00055'])], None),
    ('16', 'Cordals', [('odonto', ['EX18', 'EX28', 'EX38', 'EX48'])], None),
    ('17', 'Holter', [('proves', ['PD00127'])], None),
    ('18', 'Obturacio', [('odonto', ['OBT{}'.format(i)
                                     for i in range(100)])], None),
    ('19', 'Test ale', [('variables', ['TD302']),
                        ('proves', ['MN00267'])], None),
    ('20', 'ECG', [('variables', ['VK4011',' VK4041', 'ECG21','ECG22',
                                    'VK4042',' VK4043','VK4044','ECG20',
                                     'ECG11','ECG12','ECG13','ECG14','ECG15','ECG16',
                                     'ECG17','ECG18','ECG19','ECG3','ECG','ECG2','ECG10']),
                        ('activitats', ['VK404','ECG','410','412'])], None),
    ('21', 'Pulsioximetria', [('variables', ['TR101'])], None),
    ('22', 'Tests psicoafectius', [('variables', ['EP5102','EP5202','EP5203','VMEDE', 'EDU23','VMEAN',
                                                   'TBAR','TIRS','VSERS','EDU18','EDU19','EDU22','VSEVS',
                                                   'AIVDL','ABVDB','EDU21','VMDLC','VMDLO','VMEHO','VMTIF','VMTPF','6494',
                                                   'BRADE', 'ICSVR', 'ABVDK', 'EDU20', 'TGCP', 'TCCH', 'NESCD']),
                                    ('problemes', ['I161', 'I1621B'])], None),
    ('23', 'Agudesa visual', [('activitats', ['OFT3', '433', 'OFT4', 'AVUE', 'AVUD', '433E', '433VA', 'AGVIS', 'AVIS']),
                              ('variables', ['AVUD', 'AVUE', 'TF1001','TF1002', 'EF5002', 'AVA-UD', 'VA-UE', 'AVE', 'AVE-UD','AVE-UE', 'AV-UD', 'AV-UE','OPTO-D','OPTO-E'])], None),
    ('24', 'Endodoncia', [('odonto', ['RAD{}'.format(i)
                                     for i in range(100)]),
                         ('odonto', ['END{}'.format(i)
                                     for i in range(100)])], 
                       [('assignada', ' edat <15')]),
    ('25', 'Oxigenoterapia', [('variables', ['O2DOM', 'FR1001', 'O2DOM2', 'FR1002']),
                              ('activitats', ['O2DOM'])], None),
    ('26', 'Taponament nasal', [('activitats', ['URG22'])], None),
    ('27', 'Tractaments parenterals', [('nics', ['2313', '2317']),('nics2', ['2313', '2317']),
                                        ('activitats', ['532', '530', 'URG30','URG32']),
                                        ('o_tract', ['IM','SC'])], None),
    ('28', 'Aerosols', [('nics', ['2311']),('nics2', ['2311']),('activitats', ['3140A','URG35'])], None),
    ('29', 'Fluor', [('odn511', ['FC','xxx'])], 
                   [('assignada', ' edat <15')]),
    ('30', 'Immobilitzacions', [('nics', ['0940']),('nics', ['0940']),('activitats', ['50','509'])], None),
    ('31', 'Fons ull', [('variables', ['FONS', 'SI_NO2']),
                       ('activitats', ['FO', 'FO-MG', 'FONS', 'FO-TA', 'F_ULL', 'VF301'])], None),
    ('32', 'Ecografia abdominal', [('activitats', ['AA701']),('variables', ['X00102']),
                       ('xml', ['ECO_P_ABDO', 'cutre'])], None),
    ('33', 'Cures, sutures i tractament úlceres cutànies', [('activitats', ['500']),
                       ('nics', ['3660', '3662', '3680', '4028', '3620', '3520', '3540']),('nics2', ['3660', '3662', '3680', '4028', '3620', '3520', '3540'])], None),
]

origens = {'variables': [('import', 'variables'),
                         ('vu_cod_vs', 'vu_dat_act', 'vu_up')],
           'activitats': [('import', 'activitats'),
                          ('au_cod_ac', 'au_dat_act', 'au_up')],
           'odonto': [('import', 'odn508'),
                      ('concat(dtd_tra, dtd_cod_p)', 'dtd_data',
                       'dtd_up')],
           'proves': [('nodrizas', 'nod_proves'),
                      ('inf_codi_prova', 'oc_data', 'oc_up_ori')],
           'nics': [('import', 'pla2'),
                      ('snnv_cod_nic', 'snnv_data_alta', 'snnv_up')],
           'odn511': [('import', 'odn511'),
                      ('tb_trac', 'tb_data', 'tb_up')],
           'xml': [('import', 'xml'),
                      ('xml_tipus_orig', 'xml_data_alta', 'xml_up')],
           'nics2': [('import', 'grupal2'),
                      ('inp_num_int', 'inp_data', 'inp_up')],
           'o_tract': [('import', 'oc_tract'),
                      ('tract_via', 'tract_data_creacio', 'tract_cod_up')],
        }


class Cartera(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_codis()
        self.get_denominador()
        self.get_centres()
        self.get_poblacio()
        self.get_dades()
        self.export_data()

    def get_codis(self):
        """Obté els codis dels diferents serveis."""
        self.codis = c.defaultdict(lambda: c.defaultdict(set))
        for servei, des, dades, denominador in serveis:
            servei = 'CART{}'.format(servei)
            for origen, codis in dades:
                for codi in codis:
                    self.codis[origen][codi].add(servei)

    def get_denominador(self):
        """
        Alguns serveis només es conten en pacients determinats.
        De moment només és per Molluscum, ho faig cutre.
        """
        self.denominador = {}
        for servei, des, dades, denominador in serveis:
            servei = 'CART{}'.format(servei)
            if denominador:
                for origen, codis in denominador:
                    if origen == 'problemes':
                        sql = 'select id_cip_sec from problemes \
                               where pr_cod_ps in {}'.format(tuple(codis))
                        db = 'import'
                        self.denominador[servei] = set([id for id,
                                                        in u.getAll(sql, db)])
                    elif origen == 'assignada':
                        sql = 'select id_cip_sec from assignada_tot \
                               where {}'.format(codis)
                        db = 'nodrizas'
                        self.denominador[servei] = set([id for id,
                                                       in u.getAll(sql, db)])
                    else:
                        raise BaseException('origen no contemplat')

    def get_centres(self):
        """EAP ICS."""
        sql = "select scs_codi, ics_codi from cat_centres where ep = '0208'"
        self.centres = {up: br for up, br in u.getAll(sql, 'nodrizas')}

    def get_poblacio(self):
        """Pacients ICS."""
        sql = 'select id_cip_sec, up from assignada_tot'
        self.poblacio = {id: self.centres[up]
                         for id, up
                         in u.getAll(sql, 'nodrizas')
                         if up in self.centres}

    def get_dades(self):
        """Captura les dades de les taules d'origen."""
        self.dades = {'REG': c.defaultdict(set),
                      'ASS': c.defaultdict(set)}
        for origen, info in origens.items():
            (db, tb), columns = info
            if origen in ('variables', 'activitats') and tancament:
                tb += '1'
            sql_columns = ', '.join(('id_cip_sec',) + columns)
            codis = self.codis[origen]
            sql = "select {0} from {1} where {2} in {3} \
                   and {4} between '{5}' and '{6}'".format(
                                                                sql_columns,
                                                                tb,
                                                                columns[0],
                                                                tuple(codis),
                                                                columns[1],
                                                                Fa1year,
                                                                actual)
            for id, cod, dat, up in u.getAll(sql, db):
                unic = (id, cod) if origen == 'odonto' else (id, dat)
                if up in self.centres:
                    br = self.centres[up]
                    for servei in codis[cod]:
                        if (servei not in self.denominador or
                           id in self.denominador[servei]):
                                self.dades['REG'][(servei, br)].add(unic)
                if id in self.poblacio:
                    br = self.poblacio[id]
                    for servei in codis[cod]:
                        if (servei not in self.denominador or
                           id in self.denominador[servei]):
                                self.dades['ASS'][(servei, br)].add(unic)

    def export_data(self):
        """Export a CSV."""
        fix = ('NUM', 'NOINSAT')
        self.export = []
        for key in ('REG', 'ASS'):
            for (servei, br), conjunt in self.dades[key].items():
                var = (servei + key, br)
                self.export.append(var + fix + (len(conjunt),))
        u.listToTable(self.export, table, db)
        


if __name__ == '__main__':
    Cartera()
    

error = []
sql = "select  cuentas, concat('A','periodo'), br, tipus, 'NOCAT', poblacio, 'DIM6SET', 'N', recompte from {0}.{1} ".format(db, table)
file = 'CARTERA_SERVEIS'
error.append(u.exportKhalix(sql, file))
