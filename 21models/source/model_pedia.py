# coding: utf8

"""
Passem les dades necessàries per EAP per calcular el model assignació de pedia
Model A és amb 13 minuts de visites a centre i model B ho és amb 13,5

"""

import numpy as np
import sisapUtils as u
from collections import Counter

pat_mental= "('P71','P72','P73','P74','P76','P76','P79','P79.01','P79.02','P82','P85','P98')"

ind_khalix = {'MA0020': {'num': 'ASS', 'den': False, 'perc': False},
              'MA0021': {'num': 'edat3a', 'den': 'ASS', 'perc': True},
              'MA0022': {'num': 'patmental', 'den': 'ASS', 'perc': True},
              'MA0023': {'num': 'gma', 'den': 'ASS', 'perc': False},
              'MA0024': {'num': 'aquas', 'den': False, 'perc': False},
              'MA0027': {'num': 'ASS', 'den': False, 'perc': False},
              'MA0029': {'num': 'consult20', 'den': False, 'perc': False}
             }

             
model_dict = {  'edat3a': {'MA0025': 0.60},
                'patmental': {'MA0025':2.69},
                'aquas': {'MA0025': 0.19},
                'centmitj': {'MA0025': -3.19},
                'centgran': {'MA0025': -4.52},
                'gma': {'MA0025': 9.8},
                'constant': {'MA0025': 22.45}
            }


def get_director(num):
    if num < 11500:
        director = 0.5
    elif num >= 23000:
        director = 1
    else:
        director = float(num)/float(23000)
    return float(director)


indicadors_mitjana = ['MA0024', 'MA0025', 'MA0028','MA0025SUP', 'MA0028SUP']
db = "models"



class Model_assignacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades = Counter()
        self.get_centres()
        self.get_gma()
        self.get_consultoris()
        self.get_patmental()
        self.get_pob()
        self.get_indicadors()
        self.get_calculs_model()
        self.get_intervals_model()
        self.get_oferta_despl_cons()
        self.get_regions()
        self.export_khalix()

    def get_centres(self):
        """."""
        self.centres = {}
        self.regions = {}
        sql = """
            SELECT scs_codi, ics_codi, sap_codi, amb_codi, ep, aquas, if(ics_desc like '%pedi%',1,0)
              FROM cat_centres
              """
        for up, br, sap, ambit, ep, aquas, lp in u.getAll(sql, 'nodrizas'):
            self.centres[br] = up
            self.dades[up, 'aquas'] += aquas
            if ep == '0208':
                ics = 'AMBITOS'
            else:
                ics = 0
            self.regions[up] = {'sap': 'SAP'+sap,
                                'ambit': 'AMB'+ambit,
                                'ics': ics, 'lp': lp}

    def get_gma(self):
        """Obté la complexitat de GMA."""
        self.gma = {}
        sql = ("select id_cip_sec, gma_ind_cmplx from gma", "import")
        for id, cmplx in u.getAll(*sql):
            self.gma[id] = cmplx

    def get_consultoris(self):
        """."""
        sql = """
            SELECT codi_departament, br, ped_hores, pob_n
              FROM preduffa.sisap_model_consultoris
             WHERE ped_hores > 0
               AND setmanes > 0 """
        for oficial,br, hores, pob_n in u.getAll(sql, 'redics'):
            up = self.centres[br] if br in self.centres else 0
            if hores < 20 and pob_n > 0:
                self.dades[up, 'consult20'] += pob_n

    
    def get_patmental(self):
        """Pacients amb patologia mental a final del període."""
        pmentalciap = {}
        sql = "select codi_cim10, codi_ciap from import.cat_md_ct_cim10_ciap where codi_ciap in {}".format(pat_mental)
        for cim10, ciap in u.getAll(sql, "import"):
            pmentalciap[cim10] = True
   
        in_crit = tuple(pmentalciap)
        self.patmental = {}
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(in_crit),
               "import")
        for id, in u.getAll(*sql):
            self.patmental[id] = 1

    def get_pob(self):
        """."""
        self.pob_tot = Counter()
        self.pob_regions = Counter()

        sql = """
            SELECT id_cip_sec, up, edat, sexe, institucionalitzat,
                   nivell_cobertura, centre_codi, centre_classe 
              FROM assignada_tot
             WHERE edat < 15
             """
        for id, up, edat, sexe,  insti, cob, cod, cla in u.getAll(sql, "nodrizas"):
            self.pob_tot[up] += 1
            sap = self.regions[up]['sap']
            ambit = self.regions[up]['ambit']
            ics = self.regions[up]['ics']
            self.pob_regions[sap] += 1
            self.pob_regions[ambit] += 1
            self.pob_regions[ics] += 1
            self.dades[up, 'ASS'] += 1
            gma_cmplx = self.gma[id] if id in self.gma else 0
            pment = self.patmental[id] if id in self.patmental else 0
            self.dades[up, 'gma'] += gma_cmplx
            self.dades[up, 'patmental'] += pment
            if edat < 3:
                self.dades[up, 'edat3a'] += 1
    
    def get_indicadors(self):
        """."""
        upload = []
        self.parametres = {}
        for indicador, valors in ind_khalix.items():
            for up, pobtot in self.pob_tot.items():
                n = ind_khalix[indicador]['num']
                num = self.dades[up, n] if (up, n) in self.dades else 0
                d = ind_khalix[indicador]['den']
                perc = ind_khalix[indicador]['perc']
                sap = self.regions[up]['sap']
                ambit = self.regions[up]['ambit']
                ics = self.regions[up]['ics']
                if d:
                    den = self.dades[up, d] if (up, d) in self.dades else 0
                    resultat = float(num)/float(den)
                    if perc:
                        resultat = resultat*100
                    upload.append([up, sap, ambit, ics, indicador, num, den, resultat])
                else:
                    if indicador == 'MA0027':
                        if up in self.regions:
                            lp = self.regions[up]['lp']
                            director = 0
                            if lp == 1:
                                director = get_director(num)
                        upload.append([up, sap, ambit, ics, indicador, director, 0, director])
                    else:
                        upload.append([up, sap, ambit, ics, indicador, num, 0, num])

        self.tb = "mst_model_ass_indicadors_ped"
        columns = ["up varchar(5)", "sap varchar(10)", "ambit varchar(10)",
                   "ics varchar(10)", "indicador varchar(10)", "num double",
                   "den double", "resultat double"]
        u.createTable(self.tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tb, db)

    def get_calculs_model(self):
        """Estimem el temps mig per UP segons:
        les beta del model, el cupo mig i el nombre de ped """
        self.resultats_model = Counter()
        self.metgesesperats = {}
        self.pobass = {}
        upload = []
        sql = "select up, indicador, resultat from {}".format(self.tb)
        for up, indicador, resultat in u.getAll(sql, db):
            parametre = ind_khalix[indicador]['num']
            if indicador == 'MA0020':
                self.pobass[up] = resultat
            try:
                beta = model_dict[parametre]['MA0025']
            except KeyError:
                continue
            if parametre == 'gma':
                if resultat < 0.8989314:
                    resultat = 0.8989314
                elif resultat > 1.727791:
                    resultat = 1.727791
                else:
                    resultat = resultat
            beta_model = float(beta)*float(resultat)
            self.resultats_model[up, 'MA0025'] += beta_model

        for (up, tip), resultat in self.resultats_model.items():
            beta = model_dict['constant'][tip]
            poba = self.pobass[up]
            if 3500 <= int(poba) <= 6000:
                betac = model_dict['centmitj']['MA0025']
            elif int(poba) > 6000:
                betac = model_dict['centgran']['MA0025']
            else:
                betac =0
            res = resultat + beta + betac
            cupo = 66000/res
            pob = self.pob_tot[up]
            ped = (pob*res)/66000
            sap = self.regions[up]['sap']
            ambit = self.regions[up]['ambit']
            ics = self.regions[up]['ics']
            upload.append([up, sap, ambit, ics, tip, res, 0, res])
            upload.append([up, sap, ambit, ics, 'MA0028', cupo, 0,  cupo])
            upload.append([up, sap, ambit, ics,  'MA0026', ped, 0,  ped])
            self.metgesesperats[up] = ped

        u.listToTable(upload, self.tb, db)

    def get_intervals_model(self):
        """Calculem interval superior del temps, pediatres i ràtio segons formula:
            -   yˆ0 ± 1.973934*sqrt (62.70981*(1 + X0’*INVERSA[(X’*X)]*X0))"""
        
        path = "./dades_noesb/matriu_model_pedia.txt"
        imp_matriu = []
        for a,b,c,d,e,f,g in u.readCSV(path, sep='@'):
            imp_matriu.append([a,b,c,d,e,f,g])
        
        X = np.array(imp_matriu, float)
        Xtransp = np.transpose(X)
        X_X = np.dot(Xtransp,X)
        invX_X = np.linalg.inv(X_X)
        
        pre_vectors = {}
        sql = "select up, indicador, resultat from {}".format(self.tb)
        for up, ind, resultat in u.getAll(sql, db):
            if ind in ['MA0021', 'MA0022', 'MA0024', 'MA0020', 'MA0023', 'MA0025']:
                pre_vectors[(up, ind)] = resultat
        
        vectors = []
        for (up, ind), res in pre_vectors.items():
            if ind == 'MA0020':
                grans, mitjans = 0, 0
                if res > 6000:
                    grans = 1
                if 3500 <= res <= 6000:
                    mitjans = 1
                vectors.append([up,1, pre_vectors[up, 'MA0021'],pre_vectors[up, 'MA0022'],pre_vectors[up, 'MA0024'], mitjans, grans, pre_vectors[up, 'MA0023']])
        
        upload = []
        for vec in vectors:
            X0 = np.array([vec[1],vec[2],vec[3],vec[4],vec[5],vec[6],vec[7]])
            up = vec[0]
            producte = np.dot(X0, invX_X)
            productefinal = np.dot(producte, X0)
            formula = productefinal *62.70981
            arrel = np.sqrt(formula)
            final = arrel*1.973934
            tmig = pre_vectors[up, 'MA0025']
            IC95sup = tmig + final
            cupoSUP = 66000/IC95sup
            pob_up = pre_vectors[up, 'MA0020']
            pediatres = (pob_up*IC95sup)/66000
            sap = self.regions[up]['sap']
            ambit = self.regions[up]['ambit']
            ics = self.regions[up]['ics']
            upload.append([up, sap, ambit, ics, 'MA0025SUP', IC95sup, 0, IC95sup])
            upload.append([up, sap, ambit, ics, 'MA0026SUP', pediatres, 0, pediatres])
            upload.append([up, sap, ambit, ics, 'MA0028SUP', cupoSUP, 0, cupoSUP])
        u.listToTable(upload, self.tb, db)       
            
   
    def get_oferta_despl_cons(self):
        """Estimem els pediatres per desplaçament i oferta dels consultoris.
        També la ràtio de de pacients per pediatres esperats + pediatres consultoris"""
        sql = ("""
            SELECT up, resultat
              FROM {}
             WHERE indicador='MA0025'
               """.format(self.tb), db
               )

        self.tempsmodel = {up: resultat for (up, resultat) in u.getAll(*sql)}

        self.result_cons = Counter()
        sql = """
            SELECT br,
                   case when ped_hores is null then 0 else ped_hores end,
                   case when ped_dies is null then 0 else ped_dies end,
                   case when setmanes is null then 0 else setmanes end,
                   case when pob_n is null then 0 else pob_n end,
                   case when distancia is null then 0 else distancia end
              FROM preduffa.sisap_model_consultoris
             WHERE mf_hores < 20 """
        for br, hores, dies, setmanes, pob_n, distancia in u.getAll(sql, 'redics'):
            up = self.centres[br] if br in self.centres else 0
            prediccio = self.tempsmodel[up] if up in self.tempsmodel else 0
            desplacament = (float(distancia)*2*float(dies)*float(setmanes))/66000
            try:
                oferta = (((float(hores)*float(setmanes)*60) / float(pob_n))
                          - float(prediccio)) * float(pob_n)/66000
            except ZeroDivisionError:
                oferta = 0
            oferta = 0 if oferta < 0 else oferta
            tot = oferta + desplacament
            self.result_cons[up, 'MA0030'] += desplacament
            self.result_cons[up, 'MA0031'] += oferta
            self.result_cons[up, 'MA0032'] += tot

        for up, pobtot in self.pob_tot.items():
            mesperats = self.metgesesperats[up]
            mconsultoris = self.result_cons[up, 'MA0032'] if (up, 'MA0032') in self.result_cons else 0
            ratio = float(pobtot) / (float(mesperats) + float(mconsultoris))

        upload = []
        for (up, indicador), resultat in self.result_cons.items():
            if up != 0:
                sap = self.regions[up]['sap']
                ambit = self.regions[up]['ambit']
                ics = self.regions[up]['ics']
                upload.append([up, sap, ambit, ics, indicador, resultat, 0, resultat])

        u.listToTable(upload, self.tb, db)
    
    
    def get_regions(self):
        """Passem dades del model per sap, ambit i ics"""
        self.dades_regions = Counter()
        sql = """SELECT up, sap, ambit, ics, indicador, num, den, resultat
                   FROM {}""".format(self.tb)
        for up, sap, ambit, ics, indicador, num, den, resultat in u.getAll(sql, db):
            if indicador in indicadors_mitjana:
                pob_up = self.pob_tot[up]
                num = float(resultat) * float(pob_up)
                self.dades_regions[sap, indicador, 'num'] += num
                self.dades_regions[ambit, indicador, 'num'] += num
                self.dades_regions[ics, indicador, 'num'] += num
            else:
                self.dades_regions[sap, indicador, 'num'] += num
                self.dades_regions[ambit, indicador, 'num'] += num
                self.dades_regions[ics, indicador, 'num'] += num
                if den > 0:
                    self.dades_regions[sap, indicador, 'den'] += den
                    self.dades_regions[ambit, indicador, 'den'] += den
                    self.dades_regions[ics, indicador, 'den'] += den

        upload = []
        for (regio, indicador, tip), valor in self.dades_regions.items():
            if tip == 'num' and regio != '0':
                if indicador in indicadors_mitjana:
                    pob_regio = self.pob_regions[regio]
                    resultat = float(valor)/float(pob_regio)
                    upload.append([regio, indicador, resultat])
                else:
                    if (regio, indicador, 'den') in self.dades_regions:
                        valor_den = self.dades_regions[regio, indicador, 'den']
                        resultat = float(valor)/float(valor_den)
                        try:
                            perc = ind_khalix[indicador]['perc']
                            if perc:
                                resultat = float(resultat)*100
                        except KeyError:
                            pass
                    else:
                        resultat = valor
                    upload.append([regio, indicador, resultat])

        self.tbr = "mst_model_ass_indicadors_regions_ped"
        columns = ["regio varchar(10)",
                   "indicador varchar(10)",
                   "resultat double"]
        u.createTable(self.tbr, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tbr, db)

    def export_khalix(self):
        # export a khalix
        error = []
        cent = "export.khx_centres"
        query = """
              SELECT indicador, concat('B','periodo'), ics_codi, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{0} a INNER JOIN {1} b ON a.up=scs_codi
               UNION
              SELECT indicador, concat('B','periodo'), regio, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{2}
               WHERE regio <> '0'""".format(self.tb, cent, self.tbr)
        file = "MODEL_ASSIGNACIO_PEDIA_B"
        error.append(u.exportKhalix(query, file))
       
        
if __name__ == '__main__':
    if u.IS_MENSUAL:
        Model_assignacio()
