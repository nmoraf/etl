# coding: utf8

"""
Passem les dades necessàries per EAP per calcular el model infermeria segons ppt de Manolo
Nomes es vol carregar a khalix temps i infermeres. Hi ha diferents ambits actuacio. Els poso
per separat
"""

import sisapUtils as u

from collections import Counter
import urllib as w
import os

db = "models"

dimensions = ['TEOR', 'PRAC']

temps_consulta = {
                    'E15G0':30,'E15G1': 30, 'E15G2': 50, 'E15G3': 60, 'E15G4': 70,
                  'E65G0': 30,'E65G1': 30, 'E65G2': 70, 'E65G3': 90, 'E65G4': 100,
                  'E79G0': 30,'E79G1': 30, 'E79G2': 100, 'E79G3': 120, 'E79G4': 140}
temps_dispersio = {'D1': 160, 'D2': 160, 'D3': 170, 'D4': 170, 'D5': 190, 'D6': 190}

temps_UPP = 690     

temps_extraccions = {'>7': {'ind': 'MII005', 'min': 4},
                     '<=7': {'ind': 'MII006', 'min': 10}}

distancia_escola = 10
#de moment posem distancia de 10 minuts, quan es tingui moodifcar
                     

file = "./dades_noesb/model_inf_coeficient2.txt"
file_edat = "./dades_noesb/temps_edat_ped.txt"
file_coef_ped = "./dades_noesb/coef_model_pedia.txt"
file_coef_ad = "./dades_noesb/coef_model_adults.txt"
            
def get_edat(edat):
    if 15 <= edat <= 64:
        ed = 'E15'
    elif 65 <= edat <= 79:
        ed = 'E65'
    elif edat > 79:
        ed = 'E79'
    else:
        ed = 'error'
    return ed  

def get_director(num):
    if num < 11500:
        director = 0.5
    elif num >= 23000:
        director = 1
    else:
        director = float(num)/float(23000)
    return float(director)    

class Model_assignacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades = Counter()
        self.create_table()
        self.get_centres()
        self.get_hacked_ecap_to_oficial()
        self.get_ecap_to_oficial()
        self.get_atdom()
        self.get_nafres()
        self.get_gma()
        self.get_dispersio()
        self.get_arxius()
        self.get_immigracio()
        self.get_escoles_sie()
        self.get_atesa_inf()
        for self.kh in dimensions:
            self.get_pob()
            """Comencem amb les N infermeria"""
            self.get_infermeres_desplacament()
            self.get_infermeres_oferta_consultoris()
            self.get_infermeres_activitat()
            self.get_infermeres_extraccions()
            self.get_infermeres_direccio()
            self.get_infermeres_control_qualitat()
            self.get_infermeres_comunitaria()
            self.get_infermeres_vac_escoles()
            self.get_infermeres_salut_escola()
            self.get_temps_mitjos_activitat()
            self.get_repartiment()
        """export"""
        self.upload_data()
        self.export_khalix()
        
    def create_table(self):
        """."""
        self.tb = "mst_model_ass_indicadors_inf_complet"
        columns = ["up varchar(5)", 
                   "model varchar(5)", 
                   "indicador varchar(10)",
                   "resultat double"]
        u.createTable(self.tb, "({})".format(", ".join(columns)), db, rm=True)
    
    def get_centres(self):
        """EAP ICS."""
        self.centres, self.br_to_up = {}, {}
        self.br_to_up = {}        
        sql = """select scs_codi, ics_codi 
                from cat_centres 
                where ep='0208'"""
        for up, br in u.getAll(sql, "nodrizas"):
            self.br_to_up[br] = up
            self.centres[up] = True
            
    def get_hacked_ecap_to_oficial(self):
        """."""
        file = u.baseFolder + ["96informes", "148sisap_centres_conversio.txt"]
        file = u.SLASH.join(file)
        self.hacked_ecap_to_oficial = {tuple(row[:3]): row[3] for row
                                       in u.readCSV(file, sep="@")}

    def get_ecap_to_oficial(self):
        """."""
        self.ecap_to_oficial = {}
        sql = "select codi_sector, cent_codi_centre, cent_classe_centre, \
                      cent_codi_oficial, cent_nom_centre \
               from cat_pritb010"
        for row in u.getAll(sql, "import"):
            key = row[:3]
            oficial = self.hacked_ecap_to_oficial.get(key, row[3])
            sector, codi, classe = key[0], key[1], key[2]
            self.ecap_to_oficial[(sector, codi, classe)] = oficial
    
    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdoms = {}
        sql = """SELECT id_cip_sec
                   FROM eqa_problemes
                  WHERE ps = 45 """
        for id, in u.getAll(sql, 'nodrizas'):
            self.atdoms[id] = 1
            
    def get_nafres(self):
        """Obtenim pacients amb nafres"""
        self.nafres = {}
        sql = """select id_cip_sec,date_format(pr_dde,'%Y%m%d'), date_format(pr_dba, '%Y%m%d')\
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and pr_data_baixa = 0 and pr_dba=0 and pr_cod_ps in ('L89', 'C01-L89.90', 'I83.0', 'C01-I83.009')"""
        for id, dde, dba in u.getAll(sql, 'import'):
            self.nafres[(id)] = 1
    
    def get_gma(self):
        """Obté el codi de GMA."""
        self.gma_cod = {}
        sql = ("select id_cip_sec, gma_pniv from gma", "import")
        for id, cod in u.getAll(*sql):
            cod = 'G' + str(cod)
            self.gma_cod[id] = cod
    
    def get_dispersio(self):
        """
        Exporta les dades de nivell UP a partir d'un Excel de l'Albert
        de Trello (https://goo.gl/U7AJQN) que formatejo manualment i
        torno a penjar a Trello (https://goo.gl/GWx3b5).
        """
        self.dispersio = {}
        file = 'jdjeux1nmc423nrr8widkfosjifsjh432rehc83cv.txt'
        w.urlretrieve("https://goo.gl/GWx3b5", file)
        
        for (up, disp, docencia, medea, assignada, atesa) in u.readCSV(file):
            self.dispersio[up] = disp
        os.remove(file)
    
    def get_arxius(self):
        """Agafo el coeficient SE de adults de la núria i el carreguem també a khalix i arxiu de temps per edat en pedia"""
        self.coeficient2, self.temps_ped, self.coef_ped, self.coef_ad = {}, {}, {}, {}
        for (br, coef) in u.readCSV(file, sep='@'):
            try:
                up = self.br_to_up[br]
            except KeyError:
                continue
            self.coeficient2[up] = coef
        for (ed, t) in u.readCSV(file_edat, sep='@'):
            self.temps_ped[int(ed)] = int(t)
        for (br, coef) in  u.readCSV(file_coef_ped, sep='@'):
            try:
                up = self.br_to_up[br]
            except KeyError:
                continue
            self.coef_ped[up] = coef
        for (br, coef) in  u.readCSV(file_coef_ad, sep='@'):
            try:
                up = self.br_to_up[br]
            except KeyError:
                continue
            self.coef_ad[up] = coef    
    
    def get_immigracio(self):
        """Obtenim immigrants renta baixa"""
        self.renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
    def get_escoles_sie(self):
        """Obtenim les escoles que fan SiE segons facin més de 5 visites en indicador CONSOB04 de SiE"""
        self.escoles_sie = {}
        sql = """
            select up, codi_escola,sum(denominador) 
            from exp_khalix_sie 
            where codi_indicador = 'CONSOB04A' 
            group by up,codi_escola
            """
        for up, escola, visites in u.getAll(sql, 'altres'):
            if visites > 5:
                self.escoles_sie[(up, escola)] = True
    
    def get_atesa_inf(self):
        """Obtenim atesa infermeria per fer càlculs de n infermeres"""
        self.atesa_inf = {}
        sql = """
            select id
            from mst_pobatinf where POBATINF02_NUM=1
            """
        for id, in u.getAll(sql, 'altres'):
            self.atesa_inf[id] = True
            
    def get_pob(self):
        """Agafem dades de població i obtenim el temps activitat en consulta i domicili. També calculem coeficient per pedia: % atesa infantil / % atesa ICS"""
        self.temps_activitat, self.temps_activitat_centre = Counter(), Counter()
        pob, self.pob_total = Counter(), Counter()
        atesa_ped, assignada_ped = 0,0
        self.coeficient_pedia = {}
        sql = """
            SELECT id_cip_sec, codi_sector, centre_codi, centre_classe, upinf, ubainf, edat, ates, nacionalitat
              FROM assignada_tot
             """
        for id, sec, codi, classe, up, uba, edat, atesc, nac in u.getAll(sql, 'nodrizas'):
            oficial = self.ecap_to_oficial[(sec, codi, classe)]
            self.pob_total[up, 'total'] += 1
            imm = 1 if nac in self.renta else 0
            temps = 0
            ates1 = 1 if id in self.atesa_inf else 0
            ates = ates1 if self.kh == 'PRAC' else atesc
            if edat > 14:
                self.pob_total[up, 'adults'] += 1
                self.pob_total[up, 'ad_imm'] += imm    
                if id in self.nafres:
                    temps = temps_UPP
                elif id in self.atdoms:
                    if up in self.dispersio:
                        disp = self.dispersio[up]
                    else:
                        disp = 'D1'
                    temps = temps_dispersio[disp] if disp in temps_dispersio else 160
                else:
                    ed = str(get_edat(edat))
                    gma = 'G1'
                    if id in self.gma_cod:
                        gma = self.gma_cod[id]
                    tip = ed + gma
                    try:
                        temps = temps_consulta[tip]
                    except KeyError:
                        temps = 30
                if ates == 1:              
                    self.temps_activitat[(up, 'MI001')] += temps 
                    self.temps_activitat_centre[(up, oficial)] += temps
                if atesc == 1:
                    self.temps_activitat[(up, 'MT001')] += temps 
            else:
                self.pob_total[up, 'nens'] += 1
                self.pob_total[up, 'ped_imm'] += imm
                if up in self.centres:
                    assignada_ped += 1
                    pob[up, 'den'] += 1
                temps = self.temps_ped[edat]
                if ates == 1:
                    self.temps_activitat[(up, 'MI002')] += temps             
                    pob[up, 'num'] += 1
                    if up in self.centres:
                        atesa_ped += 1
                if atesc == 1:
                    self.temps_activitat[(up, 'MT002')] += temps
            
        ratio_ped = float(atesa_ped)/float(assignada_ped)
        for (up, tip), nens in pob.items():
            if tip == 'den':
                num = pob[up, 'num']
                a_ass = float(num)/float(nens)
                coeficient = a_ass / ratio_ped
                self.coeficient_pedia[up] = coeficient
    
    def get_infermeres_activitat(self):
        """Carreguem les infermeres per activitat per adults i pedia amb i sense coeficient"""
        for (up, indicador), rec in self.temps_activitat.items():
            prof = float(rec)/float(66000)
            if indicador == 'MI001':
                self.dades[up, self.kh, 'MII001'] += prof
                coef2 = self.coeficient2[up] if up in self.coeficient2 else 1
                prof2 = float(prof)*float(coef2)
                self.dades[up, self.kh, 'MII001C'] += prof2
                self.dades[up, self.kh,'MIC001'] = coef2
            if indicador == 'MI002':
                self.dades[up, self.kh,'MII002'] += prof
                coef_p_at = self.coeficient_pedia[up] if up in self.coeficient_pedia else 1
                prof2 = float(prof)*float(coef_p_at)
                self.dades[up, self.kh,'MII002C'] += prof2
                self.dades[up, self.kh,'MIC002'] = coef_p_at
    
    def get_infermeres_desplacament(self):
        """Calculem infermeres segons desplacament a consultoris"""
        sql = """
            SELECT br,
                   case when inf_hores is null then 0 else inf_hores end,
                   case when inf_dies is null then 0 else inf_dies end,
                   case when setmanes is null then 0 else setmanes end,
                   case when distancia is null then 0 else distancia end
              FROM preduffa.sisap_model_consultoris """
        for br, hores, dies, setmanes, distancia in u.getAll(sql, 'redics'):
            up = self.br_to_up[br] if br in self.br_to_up else 0    
            desplacament = (float(distancia)*2*float(dies)*float(setmanes))/66000
            if up != 0:
                self.dades[up, self.kh, 'MII003'] += desplacament
        
    def get_infermeres_oferta_consultoris(self):
        """Calculem infermeres segons desplacament a consultoris"""
        sql = """
            SELECT br, codi_departament,
                   case when inf_hores is null then 0 else inf_hores end,
                   case when setmanes is null then 0 else setmanes end,
                   case when pob_a is null then 0 else pob_a end
              FROM preduffa.sisap_model_consultoris 
              WHERE inf_hores <20 """
        factor = 0.8 if self.kh == "TEOR" else 1
        for br, oficial, hores, setmanes, pob_a in u.getAll(sql, 'redics'):
            minuts_consultori = float(hores)*float(setmanes)*60
            up = self.br_to_up[br] if br in self.br_to_up else 0
            minuts_act = self.temps_activitat_centre[(up, oficial)] 
            if  minuts_consultori > minuts_act and pob_a >0:
                if up != 0:
                    minuts_extres = minuts_consultori - (factor * minuts_act)
                    inf_oferta = float(minuts_extres)/66000
                    self.dades[up, self.kh, 'MII004'] += inf_oferta
                
    
    def get_infermeres_extraccions(self):
        """calculem segons taxes esperades de laboratori: 4 minuts per > 7 anys i 10 minuts per < 7 anys"""
        sql = """
                select br, edat, n
                from exp_model
              """
        for br, edat, peticions in u.getAll(sql, 'laboratori'):
            up = self.br_to_up[br] if br in self.br_to_up else 0 
            indicador = temps_extraccions[edat]['ind']
            minuts = temps_extraccions[edat]['min']
            temps = float(peticions) * float(minuts)
            infermeres = float(temps)/66000
            if up != 0:        
                self.dades[up, self.kh, indicador] += infermeres
    
    def get_infermeres_direccio(self):
        """infermeres segons direcció"""
        for (up, tip), rec in self.pob_total.items():
            if tip == 'total':
                infermeres = get_director(rec)
                self.dades[up, self.kh, 'MII007'] += infermeres
 
    def get_infermeres_control_qualitat(self):
        """infermeres segons mida UP i consultoris. Si pob >23000 380h any. pob <11500 302h, resta continu. Afegim hores segons obertura consultoris"""
        maxh, minh = 380.00, 302.00
        factor = (maxh - minh) / float(11500)
        hores_cont_qual = Counter()
        for (up, tip), pob in self.pob_total.items():
            if tip == 'total':
                if pob>=23000:
                    hores = maxh
                elif pob <11500:
                    hores = minh
                else:
                    hores = minh + ((float(pob) - float(11500))*factor)
                hores_cont_qual[up] += hores

        sql = """
            SELECT br,
                   case when inf_hores is null then 0 else inf_hores end,
                   case when inf_dies is null then 0 else inf_dies end
              FROM preduffa.sisap_model_consultoris 
              """                
        for br, ho, dies in u.getAll(sql, 'redics'):
            up = self.br_to_up[br] if br in self.br_to_up else 0    
            hores = 40 if ho>=20 else 2*ho
            if up != 0:
                hores_cont_qual[up] += hores
        
        for (up), hores in hores_cont_qual.items():
            infermeres = float(hores) / float(1100)
            self.dades[up, self.kh, 'MII008'] += infermeres
        
    def get_infermeres_comunitaria(self):
        """Una infermera per cada 23000 estandarditzada segons el model adults i pedia per a no immigrants. Per a immigrants 1 cada 11500"""
        for (up, tip), pob in self.pob_total.items():
            if tip == 'ped_imm' or tip == 'ad_imm':
                if pob > 11500:
                    infermeres = 1
                else:
                    infermeres = float(pob) / float(11500)
                if tip == 'ped_imm':
                    self.dades[up, self.kh, 'MII010'] += infermeres
                else:
                    self.dades[up, self.kh, 'MII009'] += infermeres
            if tip == 'nens':
                imm_nens = self.pob_total[up, 'ped_imm']
                nens_noimm = pob - imm_nens
                coef = self.coef_ped[up] if up in self.coef_ped else 1
                infermeresN = (float(nens_noimm)*float(coef)) / float(23000)
                factorN = float(0.75) if self.kh == 'PRAC' else 1
                infermeresN05 = float(infermeresN) * float(factorN)
                self.dades[up, self.kh, 'MII010'] += infermeresN05
                self.dades[up, self.kh, 'MIC006'] = coef
            if tip == 'adults':
                imm_ad = self.pob_total[up, 'ad_imm']
                ad_noimm = pob - imm_ad
                coef = self.coef_ad[up] if up in self.coef_ad else 1
                infermeresA = (float(ad_noimm)*float(coef)) / float(23000)
                factorA = float(0.75) if self.kh == 'PRAC' else 1
                infermeresA05 = float(infermeresA) * float(factorA)
                self.dades[up, self.kh, 'MII009'] += infermeresA05
                self.dades[up, self.kh, 'MIC005'] = coef
                
                
    def get_infermeres_vac_escoles(self):
        """36 minuts any per nen de 6P i 8 min per nen de 2n eso + 120 per escola + distancia (de moment 10 min)
        Any 2020: actualitzem per covid tambe a 1r i 3r eso: 8 minuts + distancia"""
        sql = """
            select up, N_6_EP, N_2_ESO, N_1_ESO, N_3_ESO
            from cat_escoles
            """
        for up, n6p, n2eso, n1eso, n3eso in u.getAll(sql, 'nodrizas'):
            min6 = float(n6p) * 36
            min2e = float(n2eso) * 8
            min1e = float(n1eso) * 8
            min3e = float(n3eso)*8
            minescola, mindesp6, mindesp2e, mindesp1e, mindesp3e = 0, 0, 0, 0, 0
            if n6p > 0:
                minescola = 120
                mindesp6 = distancia_escola * 2 * 4
            if n2eso > 0:
                minescola = 120
                mindesp2e = distancia_escola * 2
            if n1eso > 0:
                minescola = 120
                mindesp1e = distancia_escola * 2 
            if n3eso > 0:
                minescola = 120
                mindesp3e = distancia_escola * 2 
            mintotals = minescola + min6 + min2e + mindesp6 + mindesp2e + min1e + min3e + mindesp1e + mindesp3e
            infermeres = float(mintotals) / float(66000)
            self.dades[up, self.kh, 'MII011'] += infermeres
            
    def get_infermeres_salut_escola(self):
        """Dos indicadors, el teòric i el que fan segons activitat de salut i escola. distancia de moment 10 min"""
        
        sql =  """
            select codi_escola, up, br, N_1_BATX + N_2_BATX + N_1_ESO + N_2_ESO + N_3_ESO + N_4_ESO 
            from cat_escoles
            """    
        for escola, up, br, nens_sie in u.getAll(sql, 'nodrizas'):
            try:
                tteoric_sie = float(nens_sie) * float(64)
            except  TypeError:
                tteoric_sie = 0
                nens_sie = 0
            coef_ped = self.coeficient_pedia[up] if up in self.coeficient_pedia else 1
            tempsnen = float(tteoric_sie) * float(coef_ped)
            if nens_sie > 0:
                tempsnen = tempsnen + 2880 + (distancia_escola * 64)    
            infermeres_teoric = float(tempsnen)/float(66000)
            if self.kh == 'PRAC':
                if (up, escola) in self.escoles_sie:
                    self.dades[up, self.kh, 'MII012'] += infermeres_teoric
            else:
                self.dades[up, self.kh, 'MII012'] += infermeres_teoric
            
    
    def get_temps_mitjos_activitat(self):
        """Carreguem també els temps mitjans activitat"""
        
        for (up, indicador), rec in self.temps_activitat.items():
            if indicador == 'MI001':
                pobass = self.pob_total[up, 'adults']
                temps_mig = float(rec)/float(pobass)
                self.dades[up, self.kh, 'MIT001'] += temps_mig
            if indicador == 'MI002':
                pobass = self.pob_total[up, 'nens']
                temps_mig = float(rec)/float(pobass)
                self.dades[up, self.kh, 'MIT002'] += temps_mig
    
    def get_repartiment(self):
        """Calculem percentatge de repartiment i infermeres totals per eap segons aquest repartiment"""
        pob_estand, pob_est_tot_ad, pob_est_tot_nens = {}, Counter(), Counter()
        for (up, tipus), n in self.pob_total.items():
            if up in self.centres:
                if tipus == 'adults':
                    coef = self.coef_ad[up] if up in self.coef_ad else 1
                    pobstandar = float(n) * float(coef)
                    pob_estand[(up, self.kh, 'adults')] = pobstandar
                    pob_est_tot_ad[self.kh] += pobstandar
                if tipus == 'nens':
                    coef = self.coef_ped [up] if up in self.coef_ped else 1
                    pobstandar = float(n) * float(coef)
                    pob_estand[(up, self.kh, 'nens')] = pobstandar
                    pob_est_tot_nens[self.kh] += pobstandar
        
        infermeres_ad, infermeres_nens = Counter(), Counter()
        for (up, ttt, indicador), rec in self.dades.items():
            if up in self.centres:
                if indicador == 'MII001' and ttt == self.kh:
                    infermeres_ad[self.kh] += rec
                if indicador == 'MII002' and ttt == self.kh:
                    infermeres_nens[self.kh] += rec
        
        for (up, ttt, tipus), n in pob_estand.items():
            if tipus == 'adults' and ttt == self.kh:
                perc_ad = float(n)/float(pob_est_tot_ad[self.kh])
                self.dades[up, self.kh, 'MIR001'] = perc_ad
                inf2 = float(infermeres_ad[self.kh]) * float(perc_ad)
                self.dades[up, self.kh, 'MII014'] = inf2
            if tipus == 'nens' and ttt == self.kh:
                perc_nens = float(n)/float(pob_est_tot_nens[self.kh])
                self.dades[up, self.kh, 'MIR002'] = perc_nens
                inf2 = float(infermeres_nens[self.kh]) * float(perc_nens)
                self.dades[up, self.kh, 'MII015'] = inf2
                
    
    def upload_data(self):
        """."""
        upload = []
        for (up, ttt, indicador), rec in self.dades.items():
            if up in self.centres:
                upload.append([up, ttt, indicador, rec]) 
        u.listToTable(upload, self.tb, db)

    def export_khalix(self):
        # export a khalix
        error = []
        cent = "export.khx_centres"
        query = """
              SELECT indicador, concat('A','periodo'), ics_codi, 'NOCLI',
                     model, 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{0} a INNER JOIN {1} b ON a.up=scs_codi""".format(self.tb, cent)
        file = "MODEL_ASSIGNACIO_INF_COMPLET"
        error.append(u.exportKhalix(query, file))         
        
if __name__ == '__main__':
    if u.IS_MENSUAL:
        Model_assignacio()
        