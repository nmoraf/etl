# coding: utf8

"""
Model odonto
Passem variables 
EAP MIXTE: VALOR 0, NENS 1, ADULTS 2
"""

import numpy as np
import sisapUtils as u
from collections import Counter

db = 'models'



model_dict = {  'MOI001': -0.00002125,
                'MOI002': 0.030215434,
                'MOI003': {'0': 0.752162649, '1': 1.983372183 },
                'constant': 1.2726
            }


class Model_assignacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades, self.dades2 = Counter(), {}
        self.get_centres()
        self.get_pob()
        self.get_calculs_model()
        self.get_odn_escoles()
        self.export_table()
        self.export_khalix()

    def get_centres(self):
        """EAP ICS."""
        self.centres, self.up_to_br = {}, {}    
        sql = """select scs_codi, ics_codi, tip_eap, aquas
                from cat_centres"""
        for up, br, tip_eap, aquas in u.getAll(sql, "nodrizas"):
            self.up_to_br[up] = br
            self.centres[up] = True
            tipus = 0
            if tip_eap == 'N':
                tipus = 1
            if tip_eap == 'A':
                tipus = 2                
            self.dades[up, 'MOI003'] = tipus
            self.dades[up, 'MOI002'] = float(aquas)
            
    def get_pob(self):
        """Necessitem població assignada"""
        self.pob = Counter()
        sql = """select id_cip_sec, up 
                from assignada_tot"""
        for id, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                self.pob[up] += 1
        
        for (up), n in self.pob.items():
            self.dades[up, 'MOI001'] = n
        
    def get_calculs_model(self):
        """Estimem el temps mig per UP segons les beta del model """
        self.temps_model = Counter()
        self.odnesperats = {}

        for (up, parametre), result in self.dades.items():
            if parametre == 'MOI003':
                try:
                    beta = model_dict[parametre][str(result)] 
                except KeyError:
                    beta = 0
                self.temps_model[up] += beta
            else:
                beta = model_dict[parametre]
                temps = result * beta
                self.temps_model[up] += temps
            
        for up, temps in self.temps_model.items():
            ct = model_dict['constant']
            res = temps + ct
            pob = self.dades[up, 'MOI001']
            odn = (pob*res)/66000
            self.dades[up, 'MOI004'] = res
            self.dades[up, 'MOI005'] = odn
   
    def get_odn_escoles(self):
        """Nens de primer i sisè"""
        total_nens, total_escoles = Counter(), Counter()
        sql = """
            select up, N_1_EP, N_6_EP 
            from cat_escoles
            """
        for up, n1p, n6p in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                total_alumnes = n1p + n6p
                if total_alumnes > 0:
                    self.dades[up, 'MOI006'] += total_alumnes
                    self.dades[up, 'MOI007'] += 1
            
        for (up, indicador), rec in self.dades.items():
            if indicador == 'MOI007':
                alumnes = self.dades[up, 'MOI006']
                odontos = (float((alumnes*8)) + float((rec*30)) + float((rec*180)))/66000
                self.dades[up, 'MOI008'] = odontos
        
    def export_table(self):
        """exportem taula"""
        upload = []
        for (up, ind), res in self.dades.items():
            br = self.up_to_br[up]
            upload.append([br, ind, res])
            
        self.tb = "mst_model_ass_indicadors_odn"
        columns = ["up varchar(10)",
                   "indicador varchar(10)",
                   "resultat double"]
        u.createTable(self.tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tb, db)

        
    def export_khalix(self):
        # export a khalix
        error = []
        cent = "export.khx_centres"
        query = """
              SELECT indicador, concat('A','periodo'), ics_codi, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{0} a INNER JOIN {1} b ON a.up=ics_codi
                where indicador not in ('MOI003')
               UNION
              SELECT indicador, concat('A','periodo'), ics_codi, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'S', if(resultat=0, 'MIXT', if(resultat=1,'NENS', 'ADULTS'))
                FROM models.{0} a INNER JOIN {1} b ON a.up=ics_codi
                where indicador in ('MOI003')""".format(self.tb, cent)
        file = "MODEL_ASSIGNACIO_ODN"
        error.append(u.exportKhalix(query, file))  
   
if __name__ == '__main__':
    if u.IS_MENSUAL:
        Model_assignacio()
