# coding: utf8

"""
Passem les dades necessàries per EAP per calcular el model infermeria

"""
import sisapUtils as u
from collections import Counter

db = "models"

ind_dict = {'15-64': {'0': 'MA0040', '1': 'MA0041', '2': 'MA0042', '3': 'MA0043', '4': 'MA0044'},
            '65-79': {'0': 'MA0045','1': 'MA0046', '2': 'MA0047', '3': 'MA0048', '4': 'MA0049'},
            '>79': {'0': 'MA0050','1': 'MA0051', '2': 'MA0052', '3': 'MA0053', '4': 'MA0054'}
            }
            
temps_dict = {'MA0040': 30 , 'MA0041': 30, 'MA0042': 50, 'MA0043': 60, 'MA0044': 70,
            'MA0045': 30, 'MA0046': 30, 'MA0047': 70, 'MA0048': 90, 'MA0049': 100,
            'MA0050': 30, 'MA0051': 30, 'MA0052': 110, 'MA0053': 120, 'MA0054': 120,
            'MA0055': 140}

file ="./dades_noesb/model_inf_coeficient2.txt"
            
def get_edat(edat):
    if 15 <= edat <= 64:
        ed = '15-64'
    elif 65 <= edat <= 79:
        ed = '65-79'
    elif edat > 79:
        ed = '>79'
    else:
        ed = 'error'
    return ed

class Model_assignacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades = Counter()
        self.coeficient1 = {}
        self.get_centres()
        self.get_atdom()
        self.get_gma()
        self.get_pob()
        #self.get_coeficient1()
        self.get_coeficient2()
        self.upload_data()
        self.get_temps()
        self.export_khalix()

    def get_centres(self):
        """EAP ICS."""
        self.centres = {}
        self.br_to_up = {}        
        sql = "select scs_codi, ics_codi, if(ep='0208',1,0) \
                from cat_centres"
        for up, br, ics in u.getAll(sql, "nodrizas"):
            self.br_to_up[br] = up
            if ics == 1:
                self.centres[up] = True
        
    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdoms = {}
        sql = """SELECT id_cip_sec
                   FROM eqa_problemes
                  WHERE ps = 45 """
        for id, in u.getAll(sql, 'nodrizas'):
            self.atdoms[id] = 1
    
    def get_gma(self):
        """Obté el codi de GMA."""
        self.gma_cod = {}
        sql = ("select id_cip_sec, gma_pniv from gma", "import")
        for id, cod in u.getAll(*sql):
            self.gma_cod[id] = cod
  
    def get_pob(self):
        """."""
        self.pob_atesa = Counter()
        sql = """
            SELECT id_cip_sec, upinf, ubainf, edat, ates
              FROM assignada_tot
             WHERE edat > 14 and institucionalitzat=0 and ates=1
             """
        for id, up, uba, edat, ates in u.getAll(sql, 'nodrizas'):
            indicador = 0
            if id in self.atdoms:
                indicador = 'MA0055'
            else:
                ed = get_edat(edat)
                gma = '0'
                if id in self.gma_cod:
                    gma = str(self.gma_cod[id])
                indicador = ind_dict[ed][gma]
            self.dades[(up, indicador)] += 1
            self.pob_atesa[(up, 'DEN')] += 1
            if up in self.centres:
                self.pob_atesa[('ICS', 'DEN')] += 1
            if ates == 1:
                self.pob_atesa[(up, 'NUM')] += 1
                if up in self.centres:
                    self.pob_atesa[('ICS', 'NUM')] += 1           
                
  
    def get_coeficient1(self):
        """."""
        for (up, tipus), rec in self.pob_atesa.items():
            if tipus == 'DEN':
                num = self.pob_atesa[(up, 'NUM')]
                atesa = float(num)/float(rec)
                denics, numics = self.pob_atesa[('ICS', 'DEN')], self.pob_atesa[('ICS', 'NUM')]
                atesaics = float(numics)/float(denics)
                coef1 = float(atesa)/float(atesaics)
                self.coeficient1[up] = coef1
    
    def get_coeficient2(self):
        """."""
        self.coeficient2 = {}
        for (br, coef) in u.readCSV(file, sep='@'):
            try:
                up = self.br_to_up[br]
            except KeyError:
                continue
            self.coeficient2[up] = coef
            
    def upload_data(self):
        """."""
        upload = []
        for (up,  indicador), rec in self.dades.items():
            upload.append([up, indicador, rec])
        self.tb = "mst_model_ass_indicadors_inf"
        columns = ["up varchar(5)", 
                   "indicador varchar(10)",
                   "resultat double"]
        u.createTable(self.tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tb, db)

    def get_temps(self):
        """."""
        resul_temps = Counter()
        sql = "select up, indicador, resultat from {}".format(self.tb)
        for up, indicador, resultat in u.getAll(sql, db):
            temps = temps_dict[indicador]
            t = resultat*temps
            resul_temps[up] += t
        upload = []
        for up, t in resul_temps.items():
            coef1, coef2 = 0, 0
            if up in self.coeficient1:
                coef1 = self.coeficient1[up]
            if up in self.coeficient2:
                coef2 = self.coeficient2[up]
            prof = float(t/66000)
            prof1 = float(prof)*float(coef1)
            prof2 = float(prof)*float(coef2)
            upload.append([up, 'MA0056', prof])
            upload.append([up, 'MA0057', coef1])
            upload.append([up, 'MA0058', coef2])
            upload.append([up, 'MA0059', prof1])
            upload.append([up, 'MA0060', prof2])
        
        u.listToTable(upload, self.tb, db)
             
    
    def export_khalix(self):
        # export a khalix
        error = []
        cent = "export.khx_centres"
        query = """
              SELECT indicador, concat('B','periodo'), ics_codi, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{0} a INNER JOIN {1} b ON a.up=scs_codi""".format(self.tb, cent)
        file = "MODEL_ASSIGNACIO_INF_B"
        error.append(u.exportKhalix(query, file))     
        
if __name__ == '__main__':
    if u.IS_MENSUAL:
        Model_assignacio()
