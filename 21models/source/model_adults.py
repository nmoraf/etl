# coding: utf8

"""
Passem les dades necessàries per EAP per calcular el model assignació adults
"""

any_encurs = False

import sisapUtils as u
from collections import Counter


ind_khalix = {'MA0001': {'num': 'ASSNOI', 'den': False, 'perc': False},
              'MA0004': {'num': 'edat', 'den': 'ASSNOI', 'perc': False},
              'MA0005': {'num': 'dones', 'den': 'ASSNOI', 'perc': True},
              'MA0006': {'num': 'gma', 'den': 'ASSNOI', 'perc': False},
              'MA0007': {'num': 'aquas', 'den': False, 'perc': False},
              'MA0008': {'num': 'pens', 'den': 'ass65', 'perc': True},
              'MA0009': {'num': 'atdom', 'den': 'ASSNOI', 'perc': True},
              'MA0010': {'num': 'ncentres', 'den': False, 'perc': False},
              'MA0003': {'num': 'consult20', 'den': False, 'perc': False},
              'MA0002': {'num': 'insti', 'den': False, 'perc': False},
              'MA0016': {'num': 'ASSNOIT', 'den': False, 'perc': False}
              }

            
"""
Coeficients per model adults de 45 minuts les domicili i 6 les no presencials:

'gma': {'MA0011': 1.68},
'edat': {'MA0011': 1.51},
'dones': {'MA0011': - 1.32},
'pens': {'MA0011': 0.84},
'aquas': {'MA0011': 0.12},
'ncentres': {'MA0011': 2.41},
'ASSNOI': {'MA0011': -0.18},
'atdom': {'MA0011': 3.89},
'constant': {'MA0011': 22.17}
"""
              
model_dict = {'A':{
                'gma': {'MA0011': 2.69},
                'edat': {'MA0011': 1.37},
                'dones': {'MA0011': - 1.30},
                'pens': {'MA0011': 0.99},
                'aquas': {'MA0011': 0.11},
                'ncentres': {'MA0011': 2.23},
                'ASSNOI': {'MA0011': -0.20},
                'atdom': {'MA0011': 3.37},
                'constant': {'MA0011': 27.29}
                },
             'B':{   
                'gma': {'MA0011': 2.72},
                'edat': {'MA0011': 1.27},
                'dones': {'MA0011': - 1.25},
                'pens': {'MA0011': 0.97},
                'aquas': {'MA0011': 0.11},
                'ncentres': {'MA0011': 2.13},
                'ASSNOI': {'MA0011': -0.19},
                'atdom': {'MA0011': 2.88},
                'constant': {'MA0011': 29.33}
                }
            }


def get_director(num):
    if num < 11500:
        director = 0.5
    elif num >= 23000:
        director = 1
    else:
        director = float(num)/float(23000)
    return float(director)


indicadors_mitjana = ['MA0007', 'MA0011', 'MA0017', 'MA0018']
db = "models"

albert = ("centres", "x0001")


class Model_assignacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades = Counter()
        self.get_centres()
        self.get_gma()
        self.get_consultoris()
        self.get_all_centres()
        self.get_atdom()
        self.get_pob()
        self.get_consultoris_pob()
        self.get_indicadors()
        self.get_calculs_model()
        self.get_oferta_despl_cons()
        self.get_regions()
        self.export_khalix()

    def get_centres(self):
        """."""
        self.centres = {}
        self.regions = {}
        sql = """
            SELECT scs_codi, ics_codi, sap_codi, amb_codi, ep, aquas
              FROM cat_centres
              """
        for up, br, sap, ambit, ep, aquas in u.getAll(sql, 'nodrizas'):
            self.centres[br] = up
            self.dades[up, 'aquas'] += aquas
            if ep == '0208':
                ics = 'AMBITOS'
            else:
                ics = 0
            self.regions[up] = {'sap': 'SAP'+sap,
                                'ambit': 'AMB'+ambit,
                                'ics': ics}

    def get_gma(self):
        """Obté la complexitat de GMA."""
        self.gma = {}
        sql = ("select id_cip_sec, gma_ind_cmplx from gma", "import")
        for id, cmplx in u.getAll(*sql):
            self.gma[id] = cmplx

    def get_consultoris(self):
        """."""
        self.centres_albert = {}
        sql = """
            SELECT codi_departament, br, mf_hores, pob_a
              FROM preduffa.sisap_model_consultoris
             WHERE mf_hores > 0
               AND setmanes > 0 """
        for oficial,br, hores, pob_a in u.getAll(sql, 'redics'):
            up = self.centres[br] if br in self.centres else 0
            self.centres_albert[(up, oficial)] = True
            if hores < 20 and pob_a > 0:
                self.dades[up, 'consult20'] += pob_a
    
    def get_all_centres(self):
        """Agafem els que no són consultoris de taula Albert"""
        
        sql = """select a.codi_departament,  c.codi
                  from centres_centre a 
                  inner join serveis_ocupacio b on a.id=b.centre_id inner join serveis_servei c on b.servei_id=c.id 
                  where a.tipus_id in (3, 15) and donat_de_baixa <> 1"""
        for oficial, br  in u.getAll(sql, albert):
            up = self.centres[br] if br in self.centres else 0
            self.centres_albert[(up, oficial)] = True
        
        self.all_centres = Counter()
        for (up, oficial), r in self.centres_albert.items():
            self.all_centres[up] += 1   
    
    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdoms = {}
        sql = """SELECT id_cip_sec
                   FROM eqa_problemes
                  WHERE ps = 45 """
        for id, in u.getAll(sql, 'nodrizas'):
            self.atdoms[id] = 1

    def get_pob(self):
        """."""
        self.pob_tot = Counter()
        self.pob_regions = Counter()

        sql = """
            SELECT id_cip_sec, up, edat, sexe, institucionalitzat,
                   nivell_cobertura, centre_codi, centre_classe 
              FROM assignada_tot
             """
        self.ambulatoris = Counter()
        for id, up, edat, sexe,  insti, cob, cod, cla in u.getAll(sql, "nodrizas"):
            if insti == 0:
                self.dades[up, 'ASSNOIT'] += 1
            if edat > 14:
                self.pob_tot[up] += 1
                sap = self.regions[up]['sap']
                ambit = self.regions[up]['ambit']
                ics = self.regions[up]['ics']
                self.pob_regions[sap] += 1
                self.pob_regions[ambit] += 1
                self.pob_regions[ics] += 1
                if insti == 0:
                    self.dades[up, 'ASSNOI'] += 1
                    dones = 1 if sexe == 'D' else 0
                    pens = 1 if cob == 'PN' else 0
                    gma_cmplx = self.gma[id] if id in self.gma else 0
                    atdom = self.atdoms[id] if id in self.atdoms else 0
                    self.dades[up, 'edat'] += edat
                    self.dades[up, 'dones'] += dones
                    self.dades[up, 'atdom'] += atdom
                    self.dades[up, 'gma'] += gma_cmplx
                    if edat < 65:
                        self.dades[up, 'ass65'] += 1
                        self.dades[up, 'pens'] += pens
                else:
                    self.dades[up, 'insti'] += 1
                self.ambulatoris[(up, cod, cla)] += 1
            
    def get_consultoris_pob(self):
        """."""
        self.consultorispob = Counter()
        for (up, cod, cla), recomptes in self.ambulatoris.items():
            self.consultorispob[up] += 1
    
    def get_indicadors(self):
        """."""
        upload = []
        self.parametres = {}
        for indicador, valors in ind_khalix.items():
            for up, pobtot in self.pob_tot.items():
                n = ind_khalix[indicador]['num']
                if n == 'ncentres':
                    ncent =  self.all_centres[up]
                    if ncent == 0:
                        num = self.consultorispob[up]
                    else:
                        num = self.all_centres[up]
                else:    
                    num = self.dades[up, n] if (up, n) in self.dades else 0
                d = ind_khalix[indicador]['den']
                perc = ind_khalix[indicador]['perc']
                sap = self.regions[up]['sap']
                ambit = self.regions[up]['ambit']
                ics = self.regions[up]['ics']
                if d:
                    den = self.dades[up, d] if (up, d) in self.dades else 0
                    resultat = float(num)/float(den)
                    if perc:
                        resultat = resultat*100
                    upload.append([up, sap, ambit, ics, indicador, num, den, resultat])
                else:
                    if indicador == 'MA0016':
                        director = get_director(num)
                        upload.append([up, sap, ambit, ics, indicador, director, 0, director])
                    else:
                        upload.append([up, sap, ambit, ics, indicador, num, 0, num])

        self.tb = "mst_model_ass_indicadors_{}".format(tipmodel)
        columns = ["up varchar(5)", "sap varchar(10)", "ambit varchar(10)",
                   "ics varchar(10)", "indicador varchar(10)", "num double",
                   "den double", "resultat double"]
        u.createTable(self.tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tb, db)

    def get_calculs_model(self):
        """Estimem el temps mig per UP segons:
        les beta del model, el cupo mig i el nombre de mf """
        self.resultats_model = Counter()
        self.metgesesperats = {}
        upload = []
        sql = "select up, indicador, resultat from {}".format(self.tb)
        for up, indicador, resultat in u.getAll(sql, db):
            parametre = ind_khalix[indicador]['num']
            try:
                beta = model_dict[tipmodel][parametre]['MA0011']
            except KeyError:
                continue
            if parametre == 'ncentres':
                resultat = 1 if resultat >= 3 else 0
            elif parametre == 'ASSNOI':
                resultat = float(resultat)/float(1000)
            beta_model = float(beta)*float(resultat)
            self.resultats_model[up, 'MA0011'] += beta_model

        for (up, tip), resultat in self.resultats_model.items():
            beta = model_dict[tipmodel]['constant'][tip]
            res = resultat + beta
            cupo = 66000/res
            pob = self.pob_tot[up]
            mf = (pob*res)/66000
            sap = self.regions[up]['sap']
            ambit = self.regions[up]['ambit']
            ics = self.regions[up]['ics']
            upload.append([up, sap, ambit, ics, tip, res, 0, res])
            upload.append([up, sap, ambit, ics, 'MA0017', cupo, 0,  cupo])
            upload.append([up, sap, ambit, ics,  'MA0012', mf, 0,  mf])
            self.metgesesperats[up] = mf

        u.listToTable(upload, self.tb, db)

    def get_oferta_despl_cons(self):
        """Estimem els metges per desplaçament i oferta dels consultoris.
        També la ràtio de de pacients per metge esperats + metge consultoris"""
        sql = ("""
            SELECT up, resultat
              FROM {}
             WHERE indicador='MA0011'
               """.format(self.tb), db
               )

        self.tempsmodel = {up: resultat for (up, resultat) in u.getAll(*sql)}

        self.result_cons = Counter()
        sql = """
            SELECT br,
                   case when mf_hores is null then 0 else mf_hores end,
                   case when mf_dies is null then 0 else mf_dies end,
                   case when setmanes is null then 0 else setmanes end,
                   case when pob_a is null then 0 else pob_a end,
                   case when distancia is null then 0 else distancia end
              FROM preduffa.sisap_model_consultoris
             WHERE mf_hores < 20 """
        for br, hores, dies, setmanes, pob_a, distancia in u.getAll(sql, 'redics'):
            up = self.centres[br] if br in self.centres else 0
            prediccio = self.tempsmodel[up] if up in self.tempsmodel else 0
            desplacament = (float(distancia)*2*float(dies)*float(setmanes))/66000
            try:
                oferta = (((float(hores)*float(setmanes)*60) / float(pob_a))
                          - float(prediccio)) * float(pob_a)/66000
            except ZeroDivisionError:
                oferta = 0
            oferta = 0 if oferta < 0 else oferta
            tot = oferta + desplacament
            self.result_cons[up, 'MA0013'] += desplacament
            self.result_cons[up, 'MA0014'] += oferta
            self.result_cons[up, 'MA0015'] += tot

        for up, pobtot in self.pob_tot.items():
            mesperats = self.metgesesperats[up]
            mconsultoris = self.result_cons[up, 'MA0015'] if (up, 'MA0015') in self.result_cons else 0
            ratio = float(pobtot) / (float(mesperats) + float(mconsultoris))
            self.result_cons[up, 'MA0018'] += ratio

        upload = []
        for (up, indicador), resultat in self.result_cons.items():
            if up != 0:
                sap = self.regions[up]['sap']
                ambit = self.regions[up]['ambit']
                ics = self.regions[up]['ics']
                upload.append([up, sap, ambit, ics, indicador, resultat, 0, resultat])

        u.listToTable(upload, self.tb, db)

    def get_regions(self):
        """Passem dades del model per sap, ambit i ics"""
        self.dades_regions = Counter()
        sql = """SELECT up, sap, ambit, ics, indicador, num, den, resultat
                   FROM {}""".format(self.tb)
        for up, sap, ambit, ics, indicador, num, den, resultat in u.getAll(sql, db):
            if indicador in indicadors_mitjana:
                pob_up = self.pob_tot[up]
                num = float(resultat) * float(pob_up)
                self.dades_regions[sap, indicador, 'num'] += num
                self.dades_regions[ambit, indicador, 'num'] += num
                self.dades_regions[ics, indicador, 'num'] += num
            else:
                self.dades_regions[sap, indicador, 'num'] += num
                self.dades_regions[ambit, indicador, 'num'] += num
                self.dades_regions[ics, indicador, 'num'] += num
                if den > 0:
                    self.dades_regions[sap, indicador, 'den'] += den
                    self.dades_regions[ambit, indicador, 'den'] += den
                    self.dades_regions[ics, indicador, 'den'] += den

        upload = []
        for (regio, indicador, tip), valor in self.dades_regions.items():
            if tip == 'num' and regio != '0':
                if indicador in indicadors_mitjana:
                    pob_regio = self.pob_regions[regio]
                    resultat = float(valor)/float(pob_regio)
                    upload.append([regio, indicador, resultat])
                else:
                    if (regio, indicador, 'den') in self.dades_regions:
                        valor_den = self.dades_regions[regio, indicador, 'den']
                        resultat = float(valor)/float(valor_den)
                        try:
                            perc = ind_khalix[indicador]['perc']
                            if perc:
                                resultat = float(resultat)*100
                        except KeyError:
                            pass
                    else:
                        resultat = valor
                    upload.append([regio, indicador, resultat])

        self.tbr = "mst_model_ass_indicadors_regions_{}".format(tipmodel)
        columns = ["regio varchar(10)",
                   "indicador varchar(10)",
                   "resultat double"]
        u.createTable(self.tbr, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tbr, db)

    def export_khalix(self):
        # export a khalix
        error = []
        cent = "export.khx_centres"
        query = """
              SELECT indicador, concat('{0}','periodo'), ics_codi, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{1} a INNER JOIN {2} b ON a.up=scs_codi
               UNION
              SELECT indicador, concat('{0}','periodo'), regio, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{3}
               WHERE regio <> '0'""".format(tipmodel, self.tb, cent, self.tbr)
        file = "MODEL_ASSIGNACIO_{}".format(tipmodel)
        error.append(u.exportKhalix(query, file))
        
        dext, = u.getOne("select right(year(data_ext),2) from nodrizas.dextraccio", 'nodrizas')
        dext1 = int(dext) - 1
        if any_encurs:
            period = tipmodel + str(dext) + '12'
        else:
            period = tipmodel + str(dext1) + '12'
        print period
        query = """
              SELECT indicador, '{0}', ics_codi, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{1} a INNER JOIN {2} b ON a.up=scs_codi
                where indicador in ('MA0013','MA0014','MA0015','MA0018','MA0010')
               UNION
              SELECT indicador, '{0}', regio, 'NOCLI',
                     'NOCAT', 'NOIMP', 'DIM6SET', 'N', resultat
                FROM models.{3}
                where indicador in ('MA0013','MA0014','MA0015','MA0018','MA0010')
               AND regio <> '0'""".format(period, self.tb, cent, self.tbr)
        file = "MODEL_ASSIGNACIO_ACTUALITZAR_{}".format(tipmodel)
        error.append(u.exportKhalix(query, file))
        
        
if __name__ == '__main__':
    if u.IS_MENSUAL:
        for tipmodel in model_dict:
            Model_assignacio()
