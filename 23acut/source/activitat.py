# coding: latin1

"""
.
"""

import collections as c
import calendar as cl
import datetime as dt

import shared as s
import sisapUtils as u


class Activitat(object):
    """."""

    def __init__(self):
        """."""
        self.get_activitat()
        self.get_denominadors()
        self.export_data()

    def get_activitat(self):
        """."""
        self.activitat = c.Counter()
        self.periodes = set()
        sql = "select visi_id, visi_up, \
                      visi_data_visita, floor(visi_hora_visita / 3600) \
               from {}, nodrizas.dextraccio \
               where visi_data_visita between \
                      date_format(data_ext - interval 1 month, '%Y-%m-01') and\
                      data_ext".format(s.VISITES)
        for id, up, dat, hora in u.getAll(sql, s.DATABASE):
            entity = s.CENTRES[up]
            period = "A{}{}".format(str(dat.year)[-2:], str(dat.month).zfill(2))  # noqa
            self.periodes.add((dat.year, dat.month))
            tipo = "VISITLAB" if u.isWorkingDay(dat) else "VISITFES"
            detalle = "D{}".format(dat.weekday() + 1)
            control = "HORA{}".format(str(hora).zfill(2))
            keys = [("ACUT002", period, entity, "NUM", tipo, detalle, "DIM6SET"),  # noqa
                    ("ACUT003", period, entity, "NUM", tipo, "NOIMP", control),  # noqa
                    ("ACUT004", period, entity, "NUM", tipo, detalle, control)]  # noqa
            for key in keys:
                self.activitat[key] += 1

    def get_denominadors(self):
        """."""
        for y, m in self.periodes:
            period = "A{}{}".format(str(y)[-2:], str(m).zfill(2))
            for d in range(1, cl.monthrange(y, m)[1] + 1):
                dat = dt.date(y, m, d)
                tipo = "VISITLAB" if u.isWorkingDay(dat) else "VISITFES"
                detalle = "D{}".format(dat.weekday() + 1)
                for entity in s.CENTRES.values():
                    keys = [("ACUT002", period, entity, "DEN", tipo, detalle, "DIM6SET")]  # noqa
                    for hora in range(24):
                        control = "HORA{}".format(str(hora).zfill(2))
                        keys.append(("ACUT003", period, entity, "DEN", tipo, "NOIMP", control))  # noqa
                        keys.append(("ACUT004", period, entity, "DEN", tipo, detalle, control))  # noqa
                    for key in keys:
                        self.activitat[key] += 1

    def export_data(self):
        """."""
        export = [k + ("N", v) for (k, v) in self.activitat.items()]
        u.listToTable(export, s.TABLE, s.DATABASE)


if __name__ == "__main__":
    Activitat()
