# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


cols = ["k{} varchar(10)".format(i) for i in range(8)] + ["v int"]
u.createTable(s.TABLE, "({})".format(", ".join(cols)), s.DATABASE, rm=True)
