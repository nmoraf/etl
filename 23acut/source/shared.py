# coding: latin1

"""
.
"""

import sisapUtils as u


TABLE = "exp_khalix"
DATABASE = "acut"
FILE = "ACUT_NEW"

CENTRES = {scs: ics for (scs, ics)
           in u.getAll("select scs_codi, ics_codi from urg_centres", "nodrizas")}  # noqa
VISITES = "pac_visites"


def worker(params):
    """."""
    return([row for row in u.getAll(*params)])
