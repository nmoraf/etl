# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


sql = "select distinct k1 from {}".format(s.TABLE)
periods = [period for period, in u.getAll(sql, s.DATABASE)]
for period in periods:
    sql = "select * from {}.{} \
           where k1 = '{}'".format(s.DATABASE, s.TABLE, period)
    u.exportKhalix(sql, s.FILE + "_" + period)
