# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


DEFAULT = ("URGEN", "URCEN")


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.get_cataleg()
        self.get_visites()

    def create_table(self):
        """."""
        cols = "(visi_id varchar(17), visi_up varchar(5), \
                 visi_data_visita date, visi_hora_visita int)"
        u.createTable(s.VISITES, cols, s.DATABASE, rm=True)

    def get_cataleg(self):
        """."""
        sql = "select cuag_sector, cuag_codi_centre, cuag_classe_centre, \
                      cuag_codi_servei, cuag_codi_modul \
               from cat_cmbdtb002"
        self.cataleg = set([row for row in u.getAll(sql, "import")
                            if row[3:5] != DEFAULT])

    def get_visites(self):
        """."""
        sql = "show create table visites1"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        centres = tuple(s.CENTRES.keys())
        sql = "select codi_sector, visi_centre_codi_centre, \
                      visi_centre_classe_centre, visi_servei_codi_servei, \
                      visi_modul_codi_modul, visi_id, visi_up, \
                      visi_data_visita, visi_hora_visita \
               from {{}}, nodrizas.dextraccio \
               where visi_up in {} and \
                     visi_data_visita between \
                                adddate(data_ext, interval -1 year) and \
                                data_ext and \
                     visi_tipus_visita not in ('9T', '9E') and \
                     visi_situacio_visita = 'R'".format(centres)
        jobs = [(sql.format(table), "import") for table in tables]
        for visites in u.multiprocess(s.worker, jobs):
            upload = [row[5:] for row in visites
                      if row[3:5] == DEFAULT or row[:5] in self.cataleg]
            u.listToTable(upload, s.VISITES, s.DATABASE)


if __name__ == "__main__":
    Visites()
