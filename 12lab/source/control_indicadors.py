# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els indicadors de la carpeta LABORATORI
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'mst_control_eqa'
tb_cat = 'cat_control_indicadors'
db_origen = "laboratori"

"""Taules origen"""
tb_ind = "exp_khalix"


periode,= u.getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')

class LadyBug_altres(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """."""
        self.indicadors, self.resultats, self.cataleg = {}, Counter(), {}
        self.get_territoris()
        self.get_ind()
        self.export_data()
        
    def get_territoris(self):
        """Obtenim àmbits i sector"""
        self.centres, self.centresSCS = {}, {}
        sql = 'select ics_codi, scs_codi, amb_desc, sector from cat_centres'
        for br, up, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
            self.centresSCS[up] = {'sector': sector, 'ambit': amb}
            
    def get_ind(self):
        """."""
          
        sql = "select account, entity, analysis, n from {} where length(entity)=5".format(tb_ind)
        for indicador, br, analisi, n in u.getAll(sql, db_origen):
            self.indicadors[indicador] = True
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if analisi == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if analisi == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
    
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indi = []
        for codi, n in self.indicadors.items():
            indi.append(codi)
            
        indicadors = tuple(indi)
        sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(tb, periode, indicadors)
        u.execute(sql, db)
        u.listToTable(upload, tb, db)
        
  
if __name__ == '__main__':
    LadyBug_altres()
    