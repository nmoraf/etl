# coding: latin1

"""
Taxes de peticions de laboratori.
"""

import collections as c
import datetime as d

import shared as s
import sisapUtils as u


DEBUG = False

ALL = "LABASS"
SPECIFIC = {
    "LABVIT": ["Q09585"],
    "LABGLI": ["LBB127", "Q32036", "BB127", "Q32136"]
}


class Taxes(object):
    """."""

    def __init__(self):
        """."""
        self.get_tables()
        self.get_peticions()
        self.get_centres()
        self.get_dades()
        self.get_ajustades()
        self.upload_khalix()
        self.upload_pacients()
        self.create_master()
        self.get_model()

    def get_tables(self):
        """."""
        self.tables = []
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        for table in u.getSubTables("laboratori"):
            this = d.date(int(table[12:16]), int(table[16:18]), 1)
            if -1 < u.monthsBetween(this, dext) < 13:
                self.tables.append(table)

    def get_peticions(self):
        """."""
        self.peticions = c.defaultdict(lambda: c.defaultdict(set))
        resultat = u.multiprocess(_worker, self.tables, 13)
        for worker in resultat:
            for id, dades in worker.items():
                for indicador, dates in dades.items():
                    self.peticions[id][indicador] |= dates

    def get_centres(self):
        """."""
        self.centres = {}
        self.medea = {}
        sql = "select scs_codi, ics_codi, if(medea = '', '2U', medea) \
               from cat_centres"
        for up, br, grup in u.getAll(sql, "nodrizas"):
            self.centres[up] = br
            self.medea[br] = grup

    def get_dades(self):
        """."""
        self.khalix = c.Counter()
        self.factors = c.defaultdict(c.Counter)
        self.pacients = []
        self.model = c.defaultdict(c.Counter)
        sql = "select id_cip_sec, up, uba, edat, sexe \
               from assignada_tot \
               where ates = 1"
        if DEBUG:
            sql += " limit 1000000"
        for id, up, uba, edat, sexe in u.getAll(sql, "nodrizas"):
            edat_k = u.ageConverter(edat)
            sexe_k = u.sexConverter(sexe)
            br = self.centres[up]
            uba_k = br + "M" + uba
            grup = self.medea[br]
            keys = ((br, edat_k, sexe_k), (uba_k, edat_k, sexe_k))
            pacient = [id, up, br, uba_k, edat_k, sexe_k, edat]
            for ind in [ALL] + sorted(SPECIFIC.keys()):
                dates = self.peticions[id][ind]
                n = len(dates)
                for key in keys:
                    self.khalix[key + (ind + "01", "DEN")] += 1
                    if n > 0:
                        self.khalix[key + (ind + "01", "NUM")] += n
                        self.khalix[key + (ind + "02", "NUM")] += n
                self.factors[(ind, edat_k, sexe_k, grup)]["DEN"] += 1
                if n > 0:
                    self.factors[(ind, edat_k, sexe_k, grup)]["NUM"] += n
                if ind != ALL:
                    pacient.append(n)
                if ind == ALL:
                    edat_m = u.ageConverter(edat, 1)
                    self.model[br][(edat_m, sexe, grup)] += 1
                    self.model["den"][(edat_m, sexe, grup)] += 1
                    if n > 0:
                        self.model["num"][(edat_m, sexe, grup)] += n
            self.pacients.append(pacient)

    def get_ajustades(self):
        """."""
        total = c.defaultdict(c.Counter)
        especifics = {}
        for key, values in self.factors.items():
            for analysis, n in values.items():
                total[key[0]][analysis] += float(n)
            especifics[key] = values["NUM"] / float(values["DEN"])
        pesos = {key: value / (total[key[0]]["NUM"] / total[key[0]]["DEN"])
                 for (key, value) in especifics.items()}
        for (entity, edat, sexe, account, analysis), n in self.khalix.items():
            if account[-2:] == "01" and analysis == "DEN":
                pes = pesos[(account[:-2], edat, sexe, self.medea[entity[:5]])]
                key = (entity, edat, sexe, account.replace("01", "02"), analysis)  # noqa
                self.khalix[key] = n * pes

    def upload_khalix(self):
        """."""
        cols = "(entity varchar(15), edat varchar(10), sexe varchar(10), \
                 account varchar(10), analysis varchar(10), n double)"
        u.createTable(s.TABLE_KHALIX, cols, s.DATABASE, rm=True)
        data = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(data, s.TABLE_KHALIX, s.DATABASE)
        u.execute("create index {0}_01 \
                   on {0} (account)".format(s.TABLE_KHALIX), s.DATABASE)

    def upload_pacients(self):
        """."""
        cols = ["id int", "up varchar(5)", "br varchar(5)",
                "uba_k varchar(15)", "edat_k varchar(10)",
                "sexe_k varchar(4)", "edat int"]
        cols += ["{} int".format(ind) for ind in sorted(SPECIFIC.keys())]
        u.createTable(s.TABLE_PACIENTS, "({})".format(", ".join(cols)),
                      s.DATABASE, rm=True)
        u.listToTable(self.pacients, s.TABLE_PACIENTS, s.DATABASE)

    def create_master(self):
        """."""
        cols = ["id int", "up varchar(5)", "uba varchar(10)",
                "tipus varchar(1)", "indicador varchar(10)",
                "inadequats int", "total int"]
        u.createTable(s.TABLE_MASTER, "({})".format(", ".join(cols)),
                      s.DATABASE, rm=True)
        u.execute("create index {0}_01 \
                   on {0} (indicador)".format(s.TABLE_MASTER), s.DATABASE)

    def _edat_m(self, edat):
        """."""
        if edat in ("ED0", "ED1", "ED2", "ED3", "ED4", "ED5", "ED6", "ED7"):
            return "<=7"
        else:
            return ">7"

    def get_model(self):
        """."""
        esperades = c.Counter()
        factors = {key: self.model["num"][key] / float(den)
                   for key, den in self.model["den"].items()}
        for br, dades in self.model.items():
            if br not in ("num", "den"):
                for key, n in dades.items():
                    esperades[(br, self._edat_m(key[0]))] += n * factors[key]
        upload = [key + (n,) for (key, n) in esperades.items()]
        cols = "(br varchar(5), edat varchar(3), n double)"
        u.createTable(s.TABLE_MODEL, cols, s.DATABASE, rm=True)
        u.listToTable(upload, s.TABLE_MODEL, s.DATABASE)


def _worker(table):
    """."""
    peticions = {}
    conversor = c.defaultdict(set)
    for indicador, codis in SPECIFIC.items():
        for codi in codis:
            conversor[codi].add(indicador)
    sql = "select id_cip_sec, cr_data_reg, cr_codi_prova_ics \
           from {}, nodrizas.dextraccio \
           where cr_data_reg between \
                             adddate(data_ext, interval -1 year) and \
                             data_ext and \
                 cr_codi_lab <> 'RIMAP'".format(table)
    if DEBUG:
        sql += " limit 100000"
    for id, dat, cod in u.getAll(sql, "import"):
        if id not in peticions:
            peticions[id] = {ALL: set()}
        peticions[id][ALL].add(dat)
        for indicador in conversor[cod]:
            if indicador not in peticions[id]:
                peticions[id][indicador] = set()
            peticions[id][indicador].add(dat)
    return peticions


if __name__ == "__main__":
    Taxes()
