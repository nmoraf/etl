# coding: latin1

"""
Adequació de les peticions de HbA1c. Extreu les dades de nodrizas, i per tant
en principi no es poden recalcular directament mesos antics.
Un problema adicional és que eqa_variables només té 2 anys de glicèmia,
si en algun moment es vol ampliar a tota la vida caldrà anar a import
i replicar el canvi d'unitats, la unió de variables i laboratori, etc.
"""

import shared as s
import sisapUtils as u


INDICADOR = "LABGLI"


class Glicada(object):
    """."""

    def __init__(self):
        """."""
        self.get_adequats()
        s.Upload(INDICADOR, self.adequats, ["edat > 14"])

    def get_adequats(self):
        """."""
        self.adequats = set()
        sqls = (("select id_cip_sec from eqa_variables \
                  where agrupador = 460 and valor > 110", "nodrizas"),
                ("select id_cip_sec from eqa_problemes \
                  where ps in (18, 24, 565)", "nodrizas"))
        resultat = u.multiprocess(self._worker, sqls)
        for worker in resultat:
            self.adequats |= worker

    def _worker(self, sql):
        return set([id for id, in u.getAll(*sql)])


if __name__ == "__main__":
    Glicada()
