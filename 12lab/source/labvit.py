# flake8: noqa
# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re
import operator

import shared as s


INDICADOR = "LABVIT"


nod="nodrizas"
imp="import"
alt="altres"

ups=('00441,')
validacion=False

validation_dict= {'M': {('00455','M36M'),('00438','16'),
                  ('00458','MG3'),
                  ('00446','MM8'),
                  ('00441','7T'), 
                  ('00441','6M'),
                ('00441','6T'),
                  ('00447','11M'),
                  ('00284','M5T')},
                  'I':{}}
             


class VitDLab(object):
    
    def __init__(self):
        self.past_date,self.current_date=self.get_date_dextraccio()
        self.subtables_by_pare=self.get_tables_year({'laboratori':'cr_data_reg'})
        self.den_total=defaultdict(set)
        self.cumplidores_ids,self.no_cumplidores_ids=defaultdict(set),defaultdict(set)
        

        #Criterios de exclusion del numerador
        self.agr_variables = { 30: (operator.le,30), 746 : (operator.gt,300)}
        self.var_codi= {'VU2000': set([4,5])}
        self.farmacs= ('N03AA02', 'N03AF01', 'N03AB02', 'N03AG01', 'C10AC01', ' C10AC02', 'A2BA01', 'A2BA51', 'J05AE', 'H02AB02', 'H02AB04', 'H02AB05', 'H02AB06', 'H02AB07', 'H02AB09', 'H02AB13', 'H02AB15','C03AA03', 'C03BA04', 'C03BA11')
        self.ps=(12,13,26,77,842,843,844,845,846,847,854)
        self.codis_lab= {
            'Q09385': { 'mmol': (operator.gt,2.625), 'mg': (operator.gt,10.5)},
            'Q35285': { 'mmol': (operator.gt,2.625), 'mg': (operator.gt,10.5)},
            'Q24259': { 'mmol': (operator.gt,99.8), 'mg': (operator.gt,400.0),'g': (operator.gt,0.4),},
            'Q42785': { 'mmol': (operator.lt,0.84), 'g': (operator.lt,8.0)},

        }
    
    def get_date_dextraccio(self):
        """ Consigue la fecha de hace 12 meses y la fecha actual.
            Devuelve a tuple de dos fechas en in datetime.date format
        """
        sql="select date_add(date_add(data_ext, interval - 12 month), interval + 1 day), data_ext from nodrizas.dextraccio;"
        for past, current in getAll(sql,nod):
            return past,current	
  

    def get_variables(self,id_var):
        """ Consigue pacientes que tienen un agrupador en eqa_variables de los especificados en self.agr_variables con fecha del ultimo any y 
            cuyo valor cumpla unos requisitos comparandolo con un valor de referencia.
            El valor de referencia y el operador correspondiente se obtienen como valores del diccionario self.agr_variables cuando se utiliza como key el agrupador.
            Devuelve : {id_cip_sec: set(fechas de variables)}
        """
        #id_var=defaultdict(set)
        sql='select id_cip_sec,data_var,agrupador,valor from {}.eqa_variables where agrupador in {}'.format(nod,tuple(self.agr_variables.iterkeys()))
        for id, data, agr, val in getAll(sql,nod):
            if monthsBetween(data,self.current_date) < 12:
                opr, ref = self.agr_variables[agr]
                if opr(val,ref):
                    id_var[id].add(data)
        return id_var

    
    def get_tractaments(self,table):
        """ Consigue los pacientes que han tenido algun tratamiento en el ultimo any de los farmacos en self.farmacs.
            Devuelve : {id_cip_sec: set(fechas de inicio de tratamiento)}
        """
        id_trat=defaultdict(set)
        sql="select id_cip_sec,ppfmc_atccodi,ppfmc_pmc_data_ini,ppfmc_data_fi from {} where ppfmc_atccodi in {}".format(table,self.farmacs)
        for id, atc_codi,ini,fi in getAll(sql,imp):
            if ini >= self.past_date or fi >= self.past_date:
                id_trat[id].add(ini)
        return id_trat

    def get_impvar(self,table):
        id_trat=defaultdict(set)
        sql = "select id_cip_sec,vu_val, vu_cod_vs,vu_dat_act from {} where vu_cod_vs in ('{}')".format(table,tuple(self.var_codi.iterkeys())[0])
        for id, vu_val, cod,dat in getAll(sql,imp):
            if monthsBetween(self.current_date,dat) < 12:
                if vu_val in self.var_codi[cod]:
                    id_trat[id].add(dat)
        return id_trat

       
    def get_laboratori(self,table):
        """ Consigue los pacientes que han tenido alguna de las peticiones de laboratorio en self.codis_lab.
            Si la prueba es la de la vitD ('Q09585') anyade al diccionario den_total el paciente como key, que tendra como valor un set de fechas de las peticiones
            de vitD que ha tenido.
            Si se trata de las otras pruebas, que especifican exclusiones, primero se procesa el resultado para pasarlo a float y se obtiene un identificador para las unidades.
            Luego, utlizando como keys las unidades parseadas, se obtiene el operador y el valor referencia con el que se comparara el valor de la prueba.
            Si el valor de la prueba evalua a True para ese operador y ese valor de prueba, se anyade el id a un diccionario como key y como valor se a�ade a un set 
            la fecha de la prueba.
            Devuelve: {id_cip_sec: set(fechas de pruebas de laboratorio)} , {id_cip_sec: set(fechas de pruebas de vitD)}
        """
        id_trat=defaultdict(set)
        sql="select id_cip_sec,cr_data_reg,cr_res_lab,cr_unitats_lab,cr_codi_prova_ics from {} where cr_codi_lab <> 'RIMAP' and cr_codi_prova_ics in {}".format(table,tuple(self.codis_lab.keys()))
        for id, data, result,unitats,prova in getAll(sql,imp):
            if monthsBetween(data,self.current_date) < 12:
                try:
                    if ',' in result:
                        result=float(result.replace(',','.'))
                    result=float(result)

                    if 'mg' in unitats:
                        unitats='mg'
                    elif 'g' in unitats:
                        unitats='g'
                    elif 'mol' in unitats:
                        unitats='mmol'
                    else:
                        continue
                    opr, ref = self.codis_lab[prova][unitats]
                    if opr(result,ref):
                        id_trat[id].add(data)
                except:
                    continue
       
        return id_trat

    def get_problemes(self,id_prob):
        """ Obtiene los ids de los pacientes que presentan un problema de salud de self.ps.
            Devuelve {id_cip_sec: set(fechas de diagnositico)} 
        """
        #id_prob=defaultdict(set)
        sql="select id_cip_sec,dde from {}.eqa_problemes where ps in {}".format(nod,self.ps)
        for id, dde in getAll(sql,imp):
            id_prob[id].add(dde)
        return id_prob

    def get_unificacion_dict(self,dict_total,dict_sub):
        """ Actualiza un diccionario dict_total con los datos del diccionario dict_sub, teniendo en cuenta que son diccionarios de sets.
            De este modo, si una key de dict_sub ya existe en dict_total, realiza la union de los valores que tenia dict_total con los nuevos que
            vienen de dict_sub
            Devuelve dict_total actualizado {key: set(valores)}
        """
        for key in dict_sub:
            dict_total[key]|= dict_sub[key]
        return dict_total


    def get_num_multiprocess(self,func,tables,id_num):
        """ Unifica los resultados de func en cada una de las tablas de tables en id_num utilizando la funcion get_unificacion_dict si la funcion que se lanza al multiproceso 
            devuelve un solo elemento. Si la funcion devuelve un tuple de diccionarios,
            se trata de la funcion que obtiene, ademas de las ids para exclur, los ids del denominador del indicador b (la que lanza el multiproceso a las tablas de laboratorio).
            Se unifican cada uno de los diccionarios obtenidos de una particion con el correspondiente diccionario global que unifica los datos de todas las particiones utlizando
            la funcion get_unificacion_dict. El denominador se agrupa en un atributo de la clase, self.den_total.

            Devuelve {id_cip_sec: set(fechas)} 
        """
        for id_fecha in multiprocess(func, tables,workers=6):
            if type(id_fecha) == tuple:
                id_sub, den_sub = id_fecha
                id_num=self.get_unificacion_dict(id_num,id_sub)
            else:
                id_num=self.get_unificacion_dict(id_num,id_fecha)
        
        return id_num

    def get_tables_year(self,names):
        """ Consigue las tablas del ultimo any
            Para ello selecciona una fecha de visita de cada una de las particiones y comprueba que el any de esta fecha sea el any actual o el pasado.
            Si es asi, se anyade la tabla a una lista.
            Devuelve una lista de tablas.
        """
        tables = defaultdict(list)
        for name in names:
            for table in getSubTables(name): 
                sql="select {0} from {1} where {0} != 0 limit 1".format(names[name],table)
                dat_tuple = getOne(sql, 'import')
                if dat_tuple:
                    dat,=dat_tuple
                    if dat.year in (self.current_date.year,self.past_date.year):
                        tables[name].append(table)
        printTime("Tables")
        return tables

    def get_id_num(self):
        """ Consigue todos los ids de los pacientes que se excluyen del indicador. Va buscando en las tablas de laboratorio,
            import.variables, import.tractaments, eqa_variables y eqa_problemes los pacientes que cumplen con los requisitos (tener una var, problema o prescripcion)
            que se han de excluir del numerador. Construye un diccionario de id_cip_sec: set fechas en las que se le ha diagnosticado un problema de salut o se le ha asignado
            una variable o un tratamiento.
            Devuelve: {id_cip_sec: set(fechas)} 
        """

        id_num=defaultdict(set)

        id_num=self.get_num_multiprocess(self.get_laboratori,self.subtables_by_pare['laboratori'],id_num)
        printTime('lab',len(id_num))

        id_num=self.get_num_multiprocess(self.get_impvar,getSubTables('variables'),id_num)
        printTime('var',len(id_num))

        id_num=self.get_num_multiprocess(self.get_tractaments,getSubTables('tractaments'),id_num)
        printTime('Tract',len(id_num))

        id_num=self.get_variables(id_num)
        printTime('Var',len(id_num))

        id_num=self.get_problemes(id_num)
        printTime('Problemes',len(id_num))

        return id_num


if __name__ == '__main__':
    vit_lab = VitDLab()
    s.Upload(INDICADOR, vit_lab.get_id_num(), ["edat > 14"])
