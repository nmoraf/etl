# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE_KHALIX = "exp_khalix"
TABLE_PACIENTS = "pac_indicadors"
TABLE_MASTER = "mst_pacients"
TABLE_MODEL = "exp_model"
DATABASE = "laboratori"


class Upload(object):
    """."""

    def __init__(self, codi, adequats, filter=None):
        """."""
        self.codi = codi
        self.adequats = adequats
        self.filter = filter
        self.get_data()
        self.del_data()
        self.set_khalix()
        self.set_master()

    def get_data(self):
        """."""
        self.data = c.Counter()
        self.master = []
        sql = "select id, up, br, uba_k, edat_k, sexe_k, {} \
               from {}".format(self.codi, TABLE_PACIENTS)
        if self.filter:
            sql += " where {}".format(" and ".join(self.filter))
        for id, up, br, uba, edat, sexe, n in u.getAll(sql, DATABASE):
            self.data[(br, edat, sexe, self.codi + "03", "DEN")] += 1
            self.data[(uba, edat, sexe, self.codi + "03", "DEN")] += 1
            if n:
                self.data[(br, edat, sexe, self.codi + "04", "DEN")] += n
                self.data[(uba, edat, sexe, self.codi + "04", "DEN")] += n
                if id not in self.adequats:
                    self.data[(br, edat, sexe, self.codi + "03", "NUM")] += n
                    self.data[(uba, edat, sexe, self.codi + "03", "NUM")] += n
                    self.data[(br, edat, sexe, self.codi + "04", "NUM")] += n
                    self.data[(uba, edat, sexe, self.codi + "04", "NUM")] += n
                this = (id, up, uba[6:], uba[5], self.codi,
                        n if id not in self.adequats else 0, n)
                self.master.append(this)

    def del_data(self):
        """."""
        sqls = ("delete from {0} \
                 where account in ('{1}03', '{1}04')".format(TABLE_KHALIX,
                                                             self.codi),
                "delete from {} where indicador = '{}'".format(TABLE_MASTER,
                                                               self.codi))
        for sql in sqls:
            u.execute(sql, DATABASE)

    def set_khalix(self):
        """."""
        upload = [k + (v,) for (k, v) in self.data.items()]
        u.listToTable(upload, TABLE_KHALIX, DATABASE)

    def set_master(self):
        """."""
        u.listToTable(self.master, TABLE_MASTER, DATABASE)
