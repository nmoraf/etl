from sisapUtils import *
from collections import Counter
from sys import argv,exit


posem_nosaltres_una_meta_inicial = False


try:
    db = argv[1]
    pf = db[:3]
except:
    exit("no m'has passat arguments")

values = {
    'eqa': {'minFix':550,'maxFix':720,'p20':563.61,'p80':752.35,'esforc':('888'),'mensual':0.05,'tipus':'M'},
    'ped': {'minFix':460,'maxFix':720,'p20':493.2,'p80':756.84,'esforc':('888'),'mensual':0.05,'tipus':'M'},
    'soc': {'minFix':30,'maxFix':75,'p20':35.85,'p80':78.01,'esforc':(),'mensual':0.05,'tipus':'T'},
    'odn': {'minFix':25,'maxFix':80,'p20':29.93,'p80':81.24,'esforc':(),'mensual':0.05,'tipus':'O'},
    'ass': {'minFix':0,'maxFix':0,'p20':0,'p80':0,'esforc':(),'mensual':0.05,'tipus':'G'}
}

ora = 'pdp'
dataAny,dataMes = getOne("select year(data_ext),date_format(data_ext,'%c') from dextraccio",'nodrizas')
max, = getOne("select sum(punts) from {}CatalegPunts where dataAny={} and tipus='{}'".format(pf,dataAny,values[pf]['tipus']),ora)

execute("delete from {}Sintetic where dataAny={} and dataMes={}".format(pf,dataAny,dataMes),ora)
execute("delete from {}SinteticMetes where basalAny={} and basalMes={}".format(pf,dataAny,dataMes),ora)

ambit = {}
q = "select scs_codi,amb_codi from cat_centres"
for up,amb in getAll(q,'nodrizas'):
    ambit[up] = amb

showed = {}
q = "select indicador,toShow from {}Cataleg where dataAny={} and pantalla = 'GENERAL'".format(pf,dataAny)
for indicador,toShow in getAll(q,ora):
    showed[indicador] = True if toShow == 1 else False
    
selected = {}
q = "select up,uab,tipus,indicador,punts from {}Individualitzat where dataAny={}".format(pf,dataAny)
for up,uba,tipus,indicador,punts in getAll(q,ora):
    selected[up,uba,tipus,indicador] = punts

goals = {}
q = "select eqa,up,uab,tipus,mmin,mint,mmax from {}SinteticMetes where dataAny={}".format(pf,dataAny)
for eqa,up,uba,tipus,mmin,mint,mmax in getAll(q,ora):
    goals[eqa,up,uba,tipus] = mmin,mint,mmax

firstMonth = {}
firstUpload = []
q = "select up,uab,tipus,min(dataMes) from {}Indicadors where dataAny={} group by up,uab,tipus".format(pf,dataAny)
for up,uba,tipus,mes in getAll(q,ora):
    firstMonth[up,uba,tipus] = mes
    firstUpload.append([dataAny,mes,up,uba,tipus])
try:    
    execute('drop table sisap_first',ora)
except:
    pass
execute('create table sisap_first (dataany int,datames int,up varchar2(5),uab varchar2(20),tipus varchar2(1),constraint pk PRIMARY KEY (dataany,datames,up,uab,tipus))',ora)
uploadOra(firstUpload,'sisap_first',ora)

start = Counter()
q = "select a.up,a.uab,a.tipus,indicador,punts,assolResultat from {0}Indicadors a,sisap_first b where a.dataany=b.dataany and a.dataMes=b.dataMes and a.up=b.up and a.uab=b.uab and a.tipus=b.tipus".format(pf)
for up,uba,tipus,indicador,punts,assolResultat in getAll(q,ora):
    if indicador in showed:
        try:
            start['G',up,uba,tipus] += punts
        except TypeError:
            continue
        try:
            start['I',up,uba,tipus] += assolResultat * selected[up,uba,tipus,indicador]
        except KeyError:
            pass
execute('drop table sisap_first',ora)

points = Counter()
unresolved = Counter()
q = "select up,uab,tipus,indicador,punts,noResolts,assolResultat from {}Indicadors where dataAny={} and dataMes={}".format(pf,dataAny,dataMes)
for up,uba,tipus,indicador,punts,noResolts,assolResultat in getAll(q,ora):
    if indicador in showed and showed[indicador]:
        points['G',up,uba,tipus] += punts
        unresolved['G',up,uba,tipus] += noResolts
        try:
            points['I',up,uba,tipus] += assolResultat * selected[up,uba,tipus,indicador]
            unresolved['I',up,uba,tipus] += noResolts
        except KeyError:
            pass
        
newGoals = []
achievement = []
khalix = {'G':[],'I':[]}
for (eqa,up,uba,tipus),total in points.items():
    try:
        mmin,mint,mmax = goals[eqa,up,uba,tipus]
    except KeyError:
        baseMes = firstMonth[up,uba,tipus]
        basal = start[eqa,up,uba,tipus]
        try:
            ambits = ambit[up]
        except KeyError:
            ambits = 'UP'
        if ambits not in values[pf]['esforc']:
            mmin = values[pf]['minFix'] if posem_nosaltres_una_meta_inicial else None
            mmax = values[pf]['maxFix'] if posem_nosaltres_una_meta_inicial else None
        else:
            mesos = 12 - baseMes
            esforc = mesos * values[pf]['mensual']
            p20 = values[pf]['p20']
            p80 = values[pf]['p80']
            mmin = p20 if basal >= p20 else basal + (esforc * (p20 - basal))
            mmax = p80 if basal >= p80 else basal + (esforc * (p80 - basal))
        mint = ((mmin + mmax) / 2) if posem_nosaltres_una_meta_inicial else None
        newGoals.append([dataAny,eqa,up,uba,tipus,dataAny,baseMes,basal,mmin,mint,mmax,'',basal,mmin,mint,mmax])
    if mmin:
        if total >= mmax:
            assoliment = 1
        elif total <= mmin:
            assoliment = 0
        else:
            assoliment = (total - mmin) / (mmax - mmin)
    else:
        assoliment = None
    achievement.append([dataAny,dataMes,eqa,up,uba,tipus,unresolved[eqa,up,uba,tipus],total,max,assoliment])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGRESULT',total])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGASSOLP',assoliment])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGMMIN',mmin])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGMINT',mint])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGMMAX',mmax])

uploadOra(newGoals,'{}SinteticMetes'.format(pf),ora)
uploadOra(achievement,'{}Sintetic'.format(pf),ora)

for eqa, in ('G','I'):
    file = tempFolder + eqa + '.txt'
    table = 'exp_khalix_uba_sintetic' if eqa == 'G' else 'exp_khalix_uba_carta'
    writeCSV(file,khalix[eqa])
    execute('drop table if exists {}'.format(table),db)
    execute('create table {} (up varchar(5),uba varchar(10),tipus varchar(1),analisis varchar(10),dada double)'.format(table),db)
    loadData(file,table,db)
