from sisapUtils import *

db="ass"
pf="ass"
  
uba = "%s.exp_ecap_uba" % db
cat = [['%s.exp_ecap_cataleg' % db,pf + 'Cataleg'],['%s.exp_ecap_catalegPare' % db,pf + 'CatalegPare'],['%s.exp_ecap_catalegPunts' % db,pf + 'CatalegPunts']]
carta = "%s.exp_ecap_carta" % db
doCarta = True if db == "eqa_ind" else False

query= "select * from %s" % uba
table= pf + "Indicadors"
exportPDP(query=query,table=table,dat=True)


query= "select indicador,sum(resolts)/sum(detectats) resolucio from %s group by indicador" % (uba)
table= pf + "IndicadorsICS"
exportPDP(query=query,table=table,dat=True)

for r in cat:
    my,ora= r[0],r[1]
    query= "select * from %s" % my
    if my.endswith("exp_ecap_catalegExclosos"):
        exportPDP(query=query,table=ora,truncate=True)
    else:    
        exportPDP(query=query,table=ora,datAny=True)

if doCarta:
    query= "select up,uba,tipus,indicador,pond_ind from %s" % carta
    table= pf + "Individualitzat"
    exportPDP(query=query,table=table,datAny=True)
