from sisapUtils import getOne, getAll, createTable, listToTable
from collections import defaultdict


dbs = ('eqa_ind', 'pedia', 'social', 'odn')  # per social i odn nomes es fa per dni, no per up ja que potencialment cada up pot tenir n professionals; i compte que per aquests no tenim en compte canvis a nivell amb / sap / up
nod = 'nodrizas'
pdp = 'pdp'
tb = 'exp_ecap_carta'
decimals = 2


class Individualitzat(object):

    def __init__(self, db):
        self.db = db
        if self.db in ('eqa_ind', 'pedia'):
            self.model = 'medinf'
        elif self.db in ('odn', 'social'):
            self.model = 'odosoc'
        self.get_catalegs()
        self.get_exclusions()
        self.get_ponderacio()
        self.upload()

    def get_catalegs(self):
        self.data = defaultdict(dict)
        self.prof_set = defaultdict(lambda: defaultdict(set))
        centres = {up: (amb, sap) for (up, amb, sap) in getAll('select scs_codi, amb_codi, sap_codi from cat_centres', nod)}
        self.ponderacio = {(cod, tip): pond for (cod, tip, pond) in getAll('select indicador, tipus, valor from exp_ecap_catalegPunts a where exists (select 1 from exp_ecap_cataleg b where a.indicador = b.indicador)', self.db)}
        self.maxim = {tip: max for (tip, max) in getAll('select tipus, sum(valor) from exp_ecap_catalegPunts a where exists (select 1 from exp_ecap_cataleg b where a.indicador = b.indicador) group by tipus', self.db)}
        if self.model == 'medinf':
            sql = 'select up, uba, a.tipus, indicador from mst_ubas a inner join exp_ecap_catalegPunts b on a.tipus = b.tipus where exists (select 1 from exp_ecap_cataleg c where b.indicador = c.indicador)'
        elif self.model == 'odosoc':
            sql = "select distinct 'UP', dni, tipus, indicador from mst_professionals a, exp_ecap_catalegPunts b where exists (select 1 from exp_ecap_cataleg c where b.indicador = c.indicador)"
        for up, uba, tipus, indicador in getAll(sql, self.db):
            if self.model == 'medinf':
                if up in centres:
                    id = (up, uba, tipus)
                    self.data[id][indicador] = True
                    self.prof_set['ambit'][centres[up][0]].add(id)
                    self.prof_set['sap'][centres[up][1]].add(id)
                    self.prof_set['centre'][up].add(id)
            elif self.model == 'odosoc':
                id = (up, uba, tipus)
                self.data[id][indicador] = True

    def get_exclusions(self):
        dext, = getOne('select year(data_ext) from dextraccio', nod)
        sql = "select id_unitat, categoria, indicador, inclos, centre_id from eqaexclusions where dataany = {} and tipus_unitat = '{{}}'".format(dext)
        for nivell in ('ambit', 'sap', 'centre', 'uab'):
            for unitat, categoria, indicador, inclos, up in getAll(sql.format(nivell), pdp):
                if self.model == 'odosoc':
                    up = 'UP'
                    unitat = unitat[:8]
                professionals = self.prof_set[nivell][unitat] if nivell != 'uab' else set([(up, unitat, categoria)])
                for professional in professionals:
                    if professional in self.data:
                        up, uba, tipus = professional
                        if (indicador, tipus) in self.ponderacio:
                            if categoria is None or tipus == categoria:
                                self.data[professional][indicador] = bool(inclos)

    def get_ponderacio(self):
        self.data_mod = []
        for (up, uba, tipus), indicadors in self.data.items():
            modificat = not all(indicadors.values())
            if modificat:
                total = sum([self.ponderacio[(indicador, tipus)] for indicador, inclos in indicadors.items() if inclos])
                if total > 0:
                    seus = sorted([[indicador, round(self.maxim[tipus] * self.ponderacio[(indicador, tipus)] / total, decimals)] for indicador, inclos in indicadors.items() if inclos], key=lambda x: x[1], reverse=True)
                    diff = round(self.maxim[tipus] - sum([pond for [ind, pond] in seus]), decimals)
                    seus[0][1] += diff
                    self.data_mod.extend([(up, uba, tipus, ind, self.ponderacio[ind, tipus], pond) for (ind, pond) in seus])

    def upload(self):
        createTable(tb, '(up varchar(5), uba varchar(10), tipus varchar(1), indicador varchar(10), pond_gen int, pond_ind decimal({}, {}))'.format(decimals + 4, decimals), self.db, rm=True)
        listToTable(self.data_mod, tb, self.db)


for db in dbs:
    Individualitzat(db)
