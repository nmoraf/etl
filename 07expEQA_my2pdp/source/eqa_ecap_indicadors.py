# coding: iso-8859-1
from sisapUtils import *
import sys

try:
    db= sys.argv[1]
    pf= sys.argv[1][:3]
except:
    sys.exit("no m'has passat arguments")

t= {'eqa_ind':'M','pedia':'M','social':'T','odn':'O'}    
uba = "%s.exp_ecap_uba" % db
cat = [['%s.exp_ecap_cataleg' % db,pf + 'Cataleg'],['%s.exp_ecap_catalegPare' % db,pf + 'CatalegPare'],['%s.exp_ecap_catalegPunts' % db,pf + 'CatalegPunts'],['%s.exp_ecap_catalegExclosos' % db,pf + 'CatalegExclosos']]
carta = "%s.exp_ecap_carta" % db
doCarta = True

if db == 'odn' or db == 'social':
    query= "select a.* from %s a" % uba
else:
    query= "select a.* from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi" % uba
    if db == "eqa_ind":
        query += " union select * from eqa_ind.exp_ecap_uba_proc"
table= pf + "Indicadors"
exportPDP(query=query,table=table,dat=True)

tip=t[db]
query= "select indicador,sum(resolts)/sum(detectats) resolucio from %s where tipus='%s' and up<>'UP' group by indicador" % (uba,tip)
table= pf + "IndicadorsICS"
exportPDP(query=query,table=table,dat=True)

for r in cat:
    my,ora= r[0],r[1]
    query= "select * from %s" % my
    if my.endswith("exp_ecap_catalegExclosos"):
        exportPDP(query=query,table=ora,truncate=True)
    else:    
        exportPDP(query=query,table=ora,datAny=True)

if doCarta:
    query= "select up,uba,tipus,indicador,pond_ind from %s" % carta
    table= pf + "Individualitzat"
    exportPDP(query=query,table=table,datAny=True)

if db == "eqa_ind":
    for my, ora in [("exp_ecap_cataleg_proc", "eqaCataleg"), ("exp_ecap_catalegpare_proc", "eqaCatalegPare")]:
        sql = "select year(data_ext), a.* from eqa_ind.{} a, nodrizas.dextraccio b".format(my)
        exportPDP(sql, ora)

    exportPDP("select * from eqa_ind.exp_ecap_cataleg_processos", "eqaCatalegProcessos", datAny=True)
    
if db== 'eqa_ind':
    text= 'S\'ha carregat l\'EQA a l\'ECAP; pots revisar els resultats a 10.80.192.26/ladybug/'
    sendSISAP(['mfabregase@gencat.cat','eballo.girona.ics@gencat.cat','mbenitez.bcn.ics@gencat.cat','cguiriguet.bnm.ics@gencat.cat', 'ajuvany.cc.ics@gencat.cat','sflayeh.bnm.ics@gencat.cat'],'C�rrega EQA','Mireia',text)
