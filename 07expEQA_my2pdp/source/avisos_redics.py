# coding: iso-8859-1
import sisapUtils as u
# import csv
# import os
# import sys
# from time import strftime
# from collections import defaultdict, Counter

TaulaMy = "mst_rec_avisos"
db = "eqa_ind"
table = "rec_avisos"
redics = 'redics'

upload = []
sql = """select
        sector,id_cip_sec,hash,up,cli,
        text1,text2,text3,text4,text5,text6,text8,
        grup from {}""".format(TaulaMy)
for sector, id, hash, up, cli, text1, text2, text3, text4, text5, text6, text8, grup in u.getAll(sql, db):  # noqa
    text7 = None
    upload.append([sector, hash, up, cli, text1, text2, text3, text4, text5,
                   text6, text7, text8, grup])

try:
    u.execute('drop table {}'.format(table), redics)
except Exception as e:
    print(str(e))
    print("table '{}' not exists in '{}'".format(table, redics))

sql = """create table {} (
            codi_sector varchar2(4),
            usua_cip VARCHAR2(40),
            usua_uab_up VARCHAR2(5),
            talv_tipus varchar2(5),
            talv_text1 varchar2(200),
            talv_text2 varchar2(1),
            talv_text3 varchar2(100),
            talv_text4 varchar2(100),
            talv_text5 varchar2(50),
            talv_text6 varchar2(100),
            talv_text7 varchar2(100),
            talv_text8 varchar2(50),
            talv_grup varchar2(15))""".format(table)
u.execute(sql, redics)
u.uploadOra(upload, table, redics)
u.execute("""
          CREATE INDEX {tb}_idx ON {tb} ({col})
          """.format(tb=table, col='usua_uab_up'), redics)

users = ['PREDUECR', 'PREDUMMP', 'PREDUPRP', 'REDICS', 'PREDULMB', 'PDP']
for user in users:
    u.execute("grant select on {} to {}".format(table, user), redics)
u.calcStatistics(table, redics)
