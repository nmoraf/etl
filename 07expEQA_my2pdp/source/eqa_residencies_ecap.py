from sisapUtils import *
import sys

db='eqa_ind'
dext = 'nodrizas.dextraccio'

pac = "%s.exp_ecap_resis_pacient" % db
dext = "nodrizas.dextraccio"    #compte
uba = "%s.exp_ecap_resis_uba" % db
cat = [['%s.exp_ecap_resis_cataleg' % db, 'resCataleg'],['%s.exp_ecap_resis_catalegPare' % db,'resCatalegPare']]


query= "select * from %s where uba <>''" % uba
table= "resIndicadors"
exportPDP(query=query,table=table,dat=True)

for r in cat:
    my,ora= r[0],r[1]
    query= "select * from %s" % my 
    exportPDP(query=query,table=ora,datAny=True)

query= "select up,uba,'M',grup_codi,hash_d,sector,exclos,up_residencia from %s where uba<>''" % (pac)
query+= " union select upinf,ubainf,'I',grup_codi,hash_d,sector,exclos,up_residencia from %s a where ubainf<>''" % (pac)
table= "resLlistats"
exportPDP(query=query,table=table,truncate=True,pkOut=True,pkIn=True)

query= "select data_ext from %s limit 1" % dext
table= "resLlistatsData"
exportPDP(query=query,table=table,truncate=True)

