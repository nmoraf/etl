from sisapUtils import *
import sys

try:
    db= sys.argv[1]
    pf= sys.argv[1][:3]
except:
    sys.exit("no m'has passat arguments")

do= {'eqa_ind':True,'pedia':True,'social':False,'odn':False}
t= {'eqa_ind':'M','pedia':'M','social':'T','odn':'O'}        
    
pac = "%s.exp_ecap_pacient" % db
uba_in = "%s.mst_ubas" % db
dext = "nodrizas.dextraccio"    #compte

if do[db]:
    if db == "eqa_ind":
        query= "select up,uba,'M',grup_codi,hash_d,a.sector,status,valor from %s_new a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.up=b.up and a.uba=b.uba and tipus='M')" % (pac,uba_in)
        query += " union select upinf,ubainf,'I',grup_codi,hash_d,a.sector,status,valor from %s_new a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.upinf=b.up and a.ubainf=b.uba and tipus='I')" % (pac,uba_in)
        query += " union select a.*, '' valor from eqa_ind.exp_ecap_pacient_proc a"
    else:
        query= "select up,uba,'M',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.up=b.up and a.uba=b.uba and tipus='M')" % (pac,uba_in)
        query += " union select upinf,ubainf,'I',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.upinf=b.up and a.ubainf=b.uba and tipus='I')" % (pac,uba_in)
        
else:
   tip=t[db]
   query= "select up,'','%s',indicador,hash_d,sector,exclos from %s a where exists (select 1 from %s b where a.up=b.up and tipus='%s')" % (tip,pac,uba_in,tip)
table= pf + "Llistats"
exportPDP(query=query,table=table,truncate=True,pkOut=True,pkIn=True)

query= "select data_ext from %s limit 1" % dext
table= pf + "LlistatsData"
exportPDP(query=query,table=table,truncate=True)
