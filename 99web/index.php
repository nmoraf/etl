<?php
define('secret', '/secret/secret.php');
include(secret);
$light= isset($_GET['l']);

#drop user 'control'@'%';
#flush privileges;
#create user 'control'@'%';
#grant select on *.* to 'control'@'%';
#grant process on *.* to 'control'@'%';
#drop table if exists sisap_ctl.path;
#create table sisap_ctl.path (path varchar(150) not null default '');
#truncate table sisap_ctl.path;
#insert into sisap_ctl.path select 'p:\\sisap' from dual;

error_reporting(E_ALL);
ini_set('display_errors', '1');

$conexion = mysqli_connect($dbhost, $dbuser);

$db="sisap_ctl";
mysqli_select_db($conexion, $db);
$query_path=mysqli_query($conexion, "select path from path limit 1;");
$path=mysqli_fetch_row($query_path);
$path=str_replace('\\','\\\\',$path[0]);
$query_doNot=mysqli_query($conexion, "select start,temps from donotpush_web limit 1;");
$doNot=mysqli_fetch_row($query_doNot);
$query_new=mysqli_query($conexion, "select scs_codi,ics_codi,ics_desc from eap_nous;");
$nous=mysqli_num_rows($query_new) > 0 ? true : false;
$query_delayed=mysqli_query($conexion, "select wd,file,ext,param from delay;");
$delayed=mysqli_num_rows($query_delayed) > 0 ? true : false;

$db="import";
mysqli_select_db($conexion, $db);
$query_agr=mysqli_query($conexion, "select conc, part, crt, status, temps_c, '', progress, error from ctl_web order by conc;");
$query_conc=mysqli_query($conexion, "select tab, conc, ts_start, ts_finish, status, temps, progress_n, progress_d, progress from ctl_status order by conc,status desc,tab;");
$query_stat=mysqli_query($conexion, "select tab, conc, ts_start, ts_finish, status, temps, progress_n, progress_d, progress from ctl_status order by status,tab;");
$query_glob=mysqli_query($conexion, "select * from ctl_web_tot;");

$file = fopen("/sisap/00all/components/__components.txt", "r");
$taulesGen = array();
$in='(';
while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
    if($datos[0] != 'down') {
        mysqli_select_db($conexion, $datos[3]);
        $taulesGen[]=$datos[0];
        $in.="'".$datos[3]."',";
        ${'query_'.$datos[0]}=mysqli_query($conexion, "select * from ctl_".$datos[3]."_web;");
        ${'query_'.$datos[0].'_glob'}=mysqli_query($conexion, "select * from ctl_".$datos[3]."_web_tot;");
        }
    }
$in=substr($in,0,-1).')';
fclose($file);

if(!$light) {
    $db="information_schema";
    mysqli_select_db($conexion, $db);
    $query_proc=mysqli_query($conexion, "select if(state is null,'',state) state,time,info from processlist where user='sisap' and db in ".$in." and (state is null or state <> '')");
    $proc=mysqli_fetch_row($query_proc);
    if (!$proc) {
        $proc=array();
        $proc[0]="";
    }
    $db="mysql";
    mysqli_select_db($conexion, $db);
    $query_slow=mysqli_query($conexion, "select db,date_format(start_time,'%Y-%m-%d %H:%i:%S'),date_format(query_time,'%H:%i:%S'),sql_text from slow_log where db in ".$in." order by query_time desc;");
} else {
    $proc=array();
    $proc[0]=""; }
mysqli_close($conexion);

function format_time($t,$z=false,$f=':') {
    if($z and $t==0) {
        return $z;
    } else {
        return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60); }}

function clean($str) {
    return nl2br(str_replace("\t","&nbsp;&nbsp;&nbsp;&nbsp;",str_replace(' ','&nbsp;',str_replace('"','&quot;',str_replace("'","&apos;",$str))))); }

$status_list=array('done','downloading','creating','waiting','unknown','failed');
$stat_list=array('done','downloading','waiting','unknown','failed');
$color=array('#BAD696','#FFFFB1','#FFFF77','#F0D860','#C0C0C0','#9A3334');
$col=array('#BAD696','#FFFFB1','#F0D860','#C0C0C0','#9A3334');
foreach ($stat_list as $valor) {${$valor}=0;}

?>
<html>
    <head>
        <title>control sisap</title>
        <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <style type="text/css">
            td {
                padding: 3px 10px;
                vertical-align: top;
                text-align: center;
                font-size: .9em; }
			td.noPad {
				padding: 0px; }
            .hide, .table_conc, .table_stat {
                display: none; }
            table.space {
                border-spacing: 10px;
                margin: -10px; }
            td.big {
                font-size: 1.5em;
				padding: 3px 10px; }
            td.alert {
                color: red;
                font-weight: bold; }
            fieldset {
                background: #f9f9f9;
                margin: -2px 10px 7px 10px;
                padding: 4px 10px 10px 10px;
                border: 1px solid #ddd;
                border-radius: 10px; 
                display: inline-block;
                vertical-align: top; }
            legend {
                color: #ddd; 
                margin-left: 5px; }
            #container {
                #border: 1px solid black;
                height: 190px; }
            .info {
                background-color: #C3C3E5; }
			.left {
                text-align: left; }
            .full {
                width: 100%; }
            .ui-tooltip {
                font-size: 0.7em; 
                max-width: 800px; }
            <?php for ($i=0;$i<count($status_list);$i++) {
                echo ".".$status_list[$i]." {background-color: ".$color[$i].";}"; }
            ?>
        </style>
        <script src='http://code.jquery.com/jquery-2.1.0.min.js'></script>
        <script src='http://code.jquery.com/ui/1.10.4/jquery-ui.min.js'></script>
        <script src='http://code.highcharts.com/highcharts.js'></script>
        <script>
            $( document ).ready(function() {
                $( '.conc' ).click(function() {
                    var taula = '#tab_' + $( this ).parent().attr( 'id' );
                    $( '.table_conc').not(taula).hide();
                    $( '.table_stat').hide();
                    $( taula ).toggle();
                    $( '#hideTabs' ).css('display',$( taula ).css('display'));
                });
                $( '.stat' ).click(function() {
                    var taula = '#tab_' + $( this ).attr( 'id' );
                    $( '.table_conc' ).hide();
                    $( '.table_stat' ).not( taula ).hide();
                    $( taula ).toggle();
                    $( '#hideTabs' ).css('display',$( taula ).css('display'));
                });
				$( '#slowClick' ).click(function() {
					$( '#slowTable' ).toggle();
				});
            });
            $(function () {
                $(document).tooltip({
                    content: function () {
                        return $(this).prop('title');
                    }
                });
            });
        </script>
    </head>
    <body>
        <?php
        
            $table_agr="<table>";$tempsT_down=0;
            while($row=mysqli_fetch_row($query_agr)) {
                $tab=$row[0];$part=$row[1];$create=$row[2];$status=$status_list[$row[3]];$long_cr=$create == 0 ? "" : format_time($row[4],'no iniciat');$long_dn=format_time($row[5],'no iniciat');$progress=$row[6];$log=clean($row[7]);
                $clickable= $part == 1 ? " class='conc'" : " class='conc'"; #inicialment nom�s click a particionades
                $tab_show= $part == 1 ? $tab." (p)" : $tab;
                $alert= $log <> '' ? " title='".$log."' class='alert'" : '';
                $table_agr.="<tr id='".$tab."' class='".$status."'><td".$clickable.">".$tab_show."</td><td".$alert.">".$long_cr."</td><td>".$progress."%</td>";
                $table_agr.="</tr>"; }
            $table_agr.="</table>";
            
            $taules=array("conc","stat");
            $first=1;
            foreach ($taules as $taula) {
                $i=0;$table="";
                while($row=mysqli_fetch_row(${"query_".$taula})){
                    $tabl[$i]=$row[0];$conc[$i]=$row[1];$stat[$i]=$status_list[$row[4]];$long_d[$i]=format_time($row[5]);$prog[$i]=$row[8];
                    if($i==0) {
                        $table.= "<table id='tab_".${$taula}[$i]."' class='table_".$taula."'>";} else {
                            if(${$taula}[$i] != ${$taula}[$i-1]) {
                                $table.= "</table><table id='tab_".${$taula}[$i]."' class='table_".$taula."'>";}}
                    $table.= "<tr class='".$stat[$i]."'><td>".$tabl[$i]."</td><td>".$stat[$i]."</td>";
                    if($stat[$i] != "waiting") $table.= "<td>".$long_d[$i]."</td><td>".$prog[$i]."%</td>";
                    if($first==1) ${$stat[$i]} = ${$stat[$i]} + 1;
                    $i++;
                }
                $table.= "</table>";
                ${"table_".$taula} = $table;
                $first++;
            }
            
            $total=0;$ptotal=0;$table_res="<table>";
            foreach ($stat_list as $valor) {
                $total = $total + ${$valor};}
            foreach ($stat_list as $valor) {
                ${"p".$valor} = round(100 * ${$valor} / $total,1); }
            foreach ($stat_list as $valor) {
                $ptotal = $ptotal + ${"p".$valor}; }
            if($ptotal != 100) $pdone = round($pdone + (100 - $ptotal),1);
            foreach ($stat_list as $key => $valor) {
                if(${'p'.$valor} > 0) {
                    $chart[]=array('name' => $valor, 'y' => ${'p'.$valor}, 'color' => $col[$key], 'borderWidth' => 0);}
                $cl= ${$valor} > 0 ? "stat " : "";
                $table_res.="<tr id='".$valor."' class='".$cl.$valor."'><td>".$valor."</td><td>".${$valor}."</td><td>".${'p'.$valor}."%</td></tr>"; }
            $table_res.="</table>";
            
            foreach($taulesGen as $taula) {
                ${$taula}="<table>";
                while($row=mysqli_fetch_row(${"query_".$taula})) {
                    $arxiu=$row[0];$status=$status_list[$row[1]];$temps=$row[1] <> 3 ? format_time($row[2]) : "";$error=clean($row[3]);
                    $process= $proc[0] != '' ? $proc[0]." (".format_time($proc[1])."):<br><br>".clean($proc[2]) : '';
                    switch($status) {case 'downloading': $title=$process; break; case 'failed': $title=$error; break; case 'unknown': $title=$error; break; default: $title='';}
                    ${$taula}.="<tr class='".$status."'><td title='".$title."'>".$arxiu."</td><td>".$temps."</td></tr>";
                }
                ${$taula}.="</table>"; }
            
            $table_glob="<table>";$tempsTotal=0;
            $globs=array();
            $globs[]="";
            foreach($taulesGen as $taula) {
                $globs[]=$taula.'_'; }
            foreach($globs as $glob) {
                while($row=mysqli_fetch_row(${"query_".$glob."glob"})) {
                    $tema=$row[0];$status=$status_list[$row[1]];$temps=format_time($row[2],'no iniciat');
                    $table_glob.= "<tr class='".$status."'><td>".$tema."</td><td>".$temps."</td></tr>";
                    $tempsTotal+= $row[2];
                }
            }
            //$table_glob.= $tempsTotal>0 ? "<tr><td></td><td>".format_time($tempsTotal)."</td></tr>" : "";
            $table_glob.= "</table>";
            
            $doNotLogs= array('log' => 'done','error' => 'failed');$doNotTR="";
            foreach($doNotLogs as $title => $class) {
                $file= $path."/00all/log/".$title.".txt";
                $doNotLog= file_exists($file) ? file_get_contents($file) : "";
                $doNotTR.= $doNotLog <> "" ? "<tr><td class='".$class."' title='".clean($doNotLog)."'>".$title."</td></tr>" : ""; }
            if($nous) {
                $nou="";
                while($row=mysqli_fetch_row($query_new)){
                    $nou.=$row[0]." / ".$row[1]." / ".clean($row[2])."<br />";}
                $doNotTR.="<tr><td class='waiting' title='".$nou."'>nous eap</td></tr>";}
            if($delayed) {
                $delays="";
                while($row=mysqli_fetch_row($query_delayed)){
                    $delays.=$row[0]." / ".$row[1].".".$row[2]." (".$row[3].")"."<br />";}
                $doNotTR.="<tr><td class='waiting' title='".$delays."'>delayed</td></tr>";}
            $doNotTR= $doNotTR <> "" ? "<tr><td></td></tr>".$doNotTR : "";
            $doNotFieldset= "<fieldset><legend>last</legend><table><tr><td class='big noPad space' title='".$doNot[0]."'>".format_time($doNot[1],'waiting')."</td></tr>".$doNotTR."</table></fieldset>";

            echo $doNotFieldset."<fieldset><legend>resum</legend><table class='space'><tr><td class='noPad'>".$table_glob."</td></tr></table></fieldset><br />";
            echo "<fieldset><legend>desc&agrave;rregues</legend><table class='space'><tr><td class='noPad'><div>".$table_res."</div><div id='container'></div></td><td class='noPad'>".$table_agr."</td><td id='hideTabs' class='noPad hide'><div>".$table_conc.$table_stat."</div></td></tr></table></fieldset>";
            foreach($taulesGen as $taula) {
                echo "<fieldset><legend>".$taula."</legend><table class='space'></tr><td class='noPad'>".${$taula}."</td></tr></table></fieldset>"; }

            echo "<script>
                    $(function () {
                        $('#container').highcharts({
                            chart: {
                                plotBackgroundColor: null,
                                backgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                margin: [30, 0, 0, -3]
                            },
                            title: {
                                text: ''
                            },
                            tooltip: {
                                pointFormat: '{point.percentage:.1f}%'
                            },
                            plotOptions: {
                                pie: {
                                    size: '100%',
                                    allowPointSelect: false,
                                    enableMouseTracking: true,
                                    dataLabels: {
                                        enabled: false,
                                        color: '#000000',
                                        connectorColor: '#000000',
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                    }
                                }
                            },
                            series: [{
                                type: 'pie',
                                name: '',
                                data: ".json_encode($chart)."
                            }],
                            credits: {
                                enabled: false
                            }
                        });
                    });
                </script>";
        ?>
    </body>
</html>
