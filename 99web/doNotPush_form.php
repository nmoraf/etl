<?php
define('secret', '/secret/secret.php');
include(secret);

//foreach ($_POST as $idx => $value) {
   //echo $idx.": ".$value."<br />";}

if(isset($_POST['path'])) {
    $path=$_POST['path'];

    $run=$path.'/execute.sh';
    $log=' 1>'.$path.'/00all/log/log.txt 2>'.$path.'/00all/log/error.txt';
    $ext=$path."/02nod/dades_noesb/nod_dextraccio.txt";
    
    $source=array();
    $components=array();
    
    $file = fopen($path.'/00all/components/__components.txt', "r");
    while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
        $source[$datos[0]]=$path."/".$datos[1]."/proc/".$datos[2];
        $components[$datos[0]]=$datos; }
    fclose($file);

    $file = fopen($path.'/00all/components/__components.txt', "w");
    foreach($components as $titol => $linia) {
        $del= isset($_POST[$titol."_del"]) ? 1 : 0;
        $components[$titol][5] = $del;
        $c=count($linia);
        for($i=0;$i<$c;$i++) {
            $final= $i==$c-1 ? "\n" : ";" ;
            fwrite($file,$components[$titol][$i].$final); }}
    fclose($file);

    $ext_d= $_POST['ext_d'];
    $ext_m= $_POST['ext_m'];
    $ext_y= $_POST['ext_y'];
    $datExt=$ext_y."/".$ext_m."/".$ext_d;
    $content=$datExt.substr(file_get_contents($ext),10,250);
    $file=fopen($ext, 'w');
    fwrite($file,$content);
    fclose($file);

    $output=array();$downGen=isset($_POST['down_del']) ? 1 : 0;
    foreach($source as $titol => $ruta) {
        $down = $titol == 'down';
        $output[$titol]=array();
        $file = fopen($ruta, "r");
        while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
            $output[$titol][]=$datos; }
        fclose($file);
        $dest = fopen($ruta, "w");
        foreach($output[$titol] as $idx => $linia) {
            
            $arxiu = $down ? $output[$titol][$idx][1] : $output[$titol][$idx][0]."_".$output[$titol][$idx][1].($output[$titol][$idx][3]!='' ? "_(".$output[$titol][$idx][3].")" : "");
            $exe = $down ? $output[$titol][$idx][4] : $output[$titol][$idx][2];
            $exeC = $down ? $output[$titol][$idx][3] : "";

            $exe = isset($_POST[$titol."_".$arxiu]) ? 1 : 0;
            $exeC = $exeC == "NA" ? $exeC : (isset($_POST[$titol."_".$arxiu."_create"]) ? 1 : 0); 
            
            if($down) {
                $output[$titol][$idx][4] = $exe;
                $output[$titol][$idx][3] = $exeC;
                $downGen= $downGen + ($exe == 1) + ($exeC == 1);
            } else {
                $output[$titol][$idx][2] = $exe;
            }
            
            $c=count($linia);
            for($i=0;$i<$c;$i++) {
                $final= $i==$c-1 ? "\n" : ";" ;
                fwrite($dest,$output[$titol][$idx][$i].$final); }}     
        
        fclose($dest); }

    $doNotPush="";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"truncate table sisap_ctl.delay\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"drop table if exists sisap_ctl.doNotPush\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"create table sisap_ctl.doNotPush (punt varchar(25),ts timestamp not null default 0)\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"drop table if exists sisap_ctl.doNotPushAgr\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"create table sisap_ctl.doNotPushAgr (ts_waiting timestamp not null default 0,initiated int not null default 0,ts_start timestamp not null default 0,finished int not null default 0,ts_finish timestamp not null default 0)\"\n";
    
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"insert into sisap_ctl.doNotPush VALUES ('start',0)\"\n";
    $file = fopen($path.'/00all/components/__components.txt', "r");
    while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
        $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"insert into sisap_ctl.doNotPush VALUES ('".$datos[0]."',0)\"\n"; }
    fclose($file);
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"insert into sisap_ctl.doNotPush VALUES ('finish',0)\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"create or replace view sisap_ctl.doNotPush_web as select if(initiated=0,ts_waiting,ts_start) start,if(initiated=0,0,if(finished=1,unix_timestamp(ts_finish)-unix_timestamp(ts_start),unix_timestamp(current_timestamp)-unix_timestamp(ts_start))) temps from sisap_ctl.doNotPushAgr\"\n\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"update sisap_ctl.doNotPush set ts=current_timestamp where punt='start'\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"insert into sisap_ctl.doNotPushAgr (ts_waiting) VALUES (current_timestamp)\"\n\n";

    $file = fopen($path.'/00all/components/__components.txt', "r");
        while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
            $doNotPush.="cd /sisap/".$datos[1]."\n";
            $doNotPush.= $datos[5] == 1 ? "" : "# ";
            $doNotPush.="bash 01del.sh\n";
            $doNotPush.= $datos[0] == "down" && $downGen == 0 ? "# " : "";
            $doNotPush.="bash 02gen.sh\n\n"; }
    fclose($file);

    $doNotPush.="cd /sisap/00all/utils\n";
    $doNotPush.= $downGen == 0 ? "# " : "";
    $doNotPush.="python wait4redics.py\n\n";

    $file = fopen($path.'/00all/components/__components.txt', "r");
        while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
            $doNotPush.="cd /sisap/".$datos[1]."\n";
            $doNotPush.= $datos[5] == 1 ? "" : "# ";
            $doNotPush.="bash 01del.sh\n";
            $doNotPush.= $datos[0] == "down" && $downGen == 0 ? "# " : "";
            $doNotPush.="bash 02gen.sh\n";
            $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"update sisap_ctl.doNotPush set ts=current_timestamp where punt='".$datos[0]."'\"\n";
            $doNotPush.= $datos[0] == "down" ? "mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"update sisap_ctl.doNotPushAgr set ts_start=current_timestamp,initiated=1\"\n" : "";
            $doNotPush.= $datos[0] == "down" && $downGen == 0 ? "# " : "";
            $doNotPush.= $datos[4]."\n\n"; }
    fclose($file);

    $doNotPush.="sleep 60\n";
    $doNotPush.="cd /sisap/00all/utils\n";
    $doNotPush.="python prepare.py res\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"update sisap_ctl.doNotPush set ts=current_timestamp where punt='finish'\"\n";
    $doNotPush.="mysql -h".$dbhost_." -u".$user." --port ".$port." -e \"update sisap_ctl.doNotPushAgr set finished=1,ts_finish=current_timestamp\"\n";
    
    $file = fopen($run, "w");   
    fwrite($file,$doNotPush);
    fclose($file);

    if(sha1($_POST["pass"]) === "6e953f49a14acf635a6926e1e34ce305e9ace92c") {
        $cmd = "docker exec sisap_sisap_1 sh -c '".$run.$log."' &";
        pclose(popen($cmd, "r"));
        sleep(1);}
}

header("HTTP/1.1 303 See Other");
header("Location: doNotPush.php");
?>
