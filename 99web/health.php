<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>health</title>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
}
td, th {
  border: 1px solid #dddddd;
  text-align: left;
  vertical-align: top;
  padding: 8px;
}
td.m, th.m {
  border: 0px;
  padding: 8px;
}
</style>
</head>
<body>
<table>
<?php
define('secret', '/secret/secret.php');
include(secret);
$header = "<table><tr><th>id</th><th>host</th><th>user</th><th>db</th><th>info</th><th>state</th><th>time</th></tr>";
$sql = "select id, host, user, db, info, state, time from information_schema.processlist where db is not null and info <> '' order by time desc, id asc";
$data07 = mysqli_query(mysqli_connect($dbhost, $dbuser), $sql);
$data08 = mysqli_query(mysqli_connect(str_replace("3307", "3308", $dbhost), "sisap"), $sql);
echo "<tr class='m'><td class='m'>".$header;
while($row = mysqli_fetch_row($data07)) {
    echo "<tr><td>".join("</td><td>", $row)."</td></tr>";
}
echo "</table></td></tr><tr class='m'><td class='m'>".$header;
while($row = mysqli_fetch_row($data08)) {
    echo "<tr><td>".join("</td><td>", $row)."</td></tr>";
}
echo "</table></td></tr><tr class='m'><td class='m'>";
echo shell_exec("bash /sisap/99web/sysinfo");
echo "</td></tr>";
if(!isset($_GET['l'])) {
    echo "<tr class='m'><td class='m'>";
    echo "<table>".shell_exec("docker ps -a --format '<tr><td>{{.Names}}</td><td>{{.Image}}</td><td>{{.Ports}}</td><td>{{.Status}}</td></tr>'")."</table>";
    echo "</td></tr><tr class='m'><td class='m'>";
    echo "<table><tr><td>".nl2br(shell_exec("docker exec sisap_hpacucli_1 hpacucli ctrl all show config"))."</td></tr></table>";
    echo "</td></tr>";
}
?>
</table>
</body>
</html>
