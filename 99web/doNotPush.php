<html>
    <head>
        <title>doNotPush</title>
        <?php
            define('secret', '/secret/secret.php');
            include(secret);
            $conexion = mysqli_connect($dbhost, $dbuser);

            $db="sisap_ctl";
            mysqli_select_db($conexion,$db);
            $query_path=mysqli_query($conexion,"select path from path limit 1;",);
            $path=mysqli_fetch_row($query_path);
            $path=str_replace('\\','/',$path[0]);
            
            $redics = shell_exec("docker exec sisap_sisap_1 python ".$path."/00all/utils/wait4redics.py web") == 1;
            $query=mysqli_query($conexion,"select min(finished) from doNotPushAgr;");
            $acabat=mysqli_fetch_row($query);
            $go= $acabat[0] == 1;
            //$go=true;
            $disable= $go ? 0 : 1;
            echo "<script>var disable=".$disable.";</script>";
            mysqli_close($conexion);

            $ext="/sisap/02nod/dades_noesb/nod_dextraccio.txt";

            $source=array();
            $file = fopen('/sisap/00all/components/__components.txt', "r");
            while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
                $source[$datos[0]]=array();
                $source[$datos[0]][]=$path."/".$datos[1]."/proc/".$datos[2];
                $source[$datos[0]][]=$datos[5]; }
            fclose($file);

            function clean($str) {
                return nl2br(str_replace("\t","&nbsp;&nbsp;&nbsp;&nbsp;",str_replace(' ','&nbsp;',str_replace('"','&quot;',str_replace("'","&apos;",$str))))); }
                
            $ext_y= file_exists($ext) ? substr(file_get_contents($ext),0,4) : "er";
            $ext_m= file_exists($ext) ? substr(file_get_contents($ext),5,2) : "er";
            $ext_d= file_exists($ext) ? substr(file_get_contents($ext),8,2) : "er";
        ?>
        <style type="text/css">
            fieldset {
                background: #f9f9f9;
                margin: -2px 10px 7px 10px;
                padding: 4px 10px 10px 10px;
                border: 1px solid #ddd;
                border-radius: 10px; 
                display: inline-block;
                vertical-align: top; }
            legend {
                color: #ddd; 
                margin-left: 5px;}
            legend.red {
                color: #E06B6C; }
            #push {
                width: 250px; }
            td {
                vertical-align: top; }
            td.space {
                text-align: center;
                padding: 5px; }
            td.middle {
                vertical-align: middle; }
            input {
                padding: 3px; }
        </style>
        <script src='http://code.jquery.com/jquery-2.1.0.min.js'></script>
        <script>    

            $( document ).ready(function() {

                var all = function(classe) {
                    var total = $('.' + classe).size();
                    var checked = $('.' + classe + ':checked').size();
                    if (total == checked) {
                        $('#' + classe).prop('checked', true);
                        $('#' + classe).prop('indeterminate', false);
                    } else if (checked == 0) {
                        $('#' + classe).prop('checked', false);
                        $('#' + classe).prop('indeterminate', false);
                    } else {
                        $('#' + classe).prop('indeterminate', true);
                    }
                };
   
                $( '.all' ).each(function() {
                    var classe = $(this).attr('id').split(" ")[0];
                    all(classe);
                });

                if(disable==1) {
                    $( 'input' ).each(function() {
                        $(this).attr('disabled','disabled');
                    });
                };

                $( 'input[type=checkbox].check' ).click(function() {
                    var classe = $(this).attr('class').split(" ")[0];
                    all(classe);
                });
                
                $('.all').click(function() { 
                    if(this.checked) {
                        $( '.' + $( this ).attr( 'id' ) ).each(function() {
                            this.checked = true;               
                        });
                    } else {
                        $( '.' + $( this ).attr( 'id' ) ).each(function() {
                            this.checked = false;                        
                        });    
                    }
                });
                
            });

        </script>
    </head>
    <body>
<?php

echo "<form action='doNotPush_form.php' method='post'>
    <input type='hidden' name='path' value='".$path."' />
    <fieldset><legend>data extraccio</legend>
        <input type='text' name='ext_d' value='".$ext_d."' maxlength='2' size='2'>
        <input type='text' name='ext_m' value='".$ext_m."' maxlength='2' size='2'>
        <input type='text' name='ext_y' value='".$ext_y."' maxlength='4' size='4'>
    </fieldset>
    <fieldset><legend>executar</legend><table><tr>
            <td class='middle'><input type='password' name='pass' maxlength='25' size='25'></td>";
if($go) {echo "<td><input type='image' src='doNotPush.png' title='do not push!' id='push' /></td>"; }
echo "</tr></table></fieldset><br /><table><tr>";
        
foreach($source as $titol => $dades) {
    $ruta = $dades[0];
    $del = $dades[1] == 1 ? " checked" : "";
    $down = $titol == 'down';
    $red= $down & !$redics ? " class='red'" : "";
    echo "<td><fieldset><legend".$red.">".$titol."</legend><table><tr><td class='space' colspan='2'>
    <input name='".$titol."_del' type='checkbox'".$del.">del";
    echo "<br /><br /><input id='".$titol."' name='".$titol."_all' type='checkbox' class='all' checked>all
    </td></tr>";
    $file = fopen($ruta, "r");
    while (($datos = fgetcsv($file, 200, ";")) !== FALSE) {
        $arxiu = $down ? $datos[1] : $datos[0].".".$datos[1].($datos[3]!='' ? " (".$datos[3].")" : "");
        $exe = $down ? $datos[4] : $datos[2];
        $checked = $exe == 1 ? " checked" : "";
        $exeC = $down ? $datos[3] : "";
        $checkedC = $exeC == 1 ? " checked" : "";
        $create = $down & $exeC <> "NA";
        $txt = $create ? "<input class='".$titol." check' type='checkbox' name='".$titol."_".$arxiu."_create'".$checkedC." title='create'>" : "";
        echo "<tr><td>".$arxiu."</td><td>".$txt."</td><td><input class='".$titol." check' type='checkbox' name='".$titol."_".$arxiu."'".$checked." title='down'></td></tr>";}
    fclose($file);
    echo "</table></fieldset></td>"; }
echo "</tr></table></form>";

?>

    </body>
</html>