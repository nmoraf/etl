
/*##########################################################################################################################################

ES07-IECA Y ARA 2 EN PACIENTES CON INSUFICIENCIA CARDIACA
INDICADOR CL�NICO
ENFERMEDAD CR�NICA CADA FILA ES UN PACIENTE


##########################################################################################################################################*/

/*CREAR DENOMINADOR*/

/*Seleccionar pacientes mayores de 14 a�os para empezar a construi el denominador*/

DROP TABLE IF EXISTS edad_ic;

CREATE TABLE edad_ic
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
from 
nodrizas.assignada_tot
where
edat > 14
;

 
/*Seleccionar insuficiencia cardiaca y coger los activos a la fecha que yo le digo de extracci�n*/

set @dataext=(select data_ext from nodrizas.dextraccio)
;

DROP TABLE IF EXISTS ps_ic;

CREATE TABLE ps_ic
select
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from
import.problemes
where
pr_cod_ps in (select criteri_codi from nodrizas.eqa_criteris where agrupador = 21)
and ((pr_dde<@dataext and pr_dba>@dataext) or (pr_dde<@dataext and pr_dba = 0))
;


 
/*Esta tabla intermedia me sirve para cada cip quede con un problema de salud cogiendo el ultimo dx*/  


drop table if exists ps_ic_ddemax;
create table ps_ic_ddemax
select
id_cip_sec
,max(pr_dde)
from
ps_ic
group by
id_cip_sec
;


 
/*Voy a hacer el denominador. Pero lo que tengo que hacer antes es hacer un indice de la tabla m�s peque�a*/
ALTER TABLE ps_ic_ddemax
ADD INDEX (id_cip_sec)
;

 
/*Vamos a unir las dos tablas para tener el denominador. La intermedia y la de poblaci�n adulta*/

DROP TABLE IF EXISTS den_ic;
CREATE TABLE den_ic
select 
a.id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,if(b.id_cip_sec is not null,1,0) as ic
from
edad_ic a
left join
ps_ic_ddemax b
on
a.id_cip_sec=b.id_cip_sec
;

 
/*Esta tabla que har� es para hacer una tabla mas peque�ita y asi poder actualizarla y que sea m�s r�pido*/

DROP TABLE IF EXISTS ic;

CREATE TABLE ic
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
from
den_ic
where
ic=1
;


/*CREAR NUMERADOR*/

 
/*Crear 2 tablas de prescripci�n diferente una para IECA y otra para ARAII*/

DROP TABLE IF EXISTS presc_ieca;

CREATE TABLE presc_ieca
select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi 
from
import.tractaments
where
((ppfmc_atccodi like 'C09A%') or (ppfmc_atccodi like 'C09B%'))
and (ppfmc_pmc_data_ini<@dataext and ppfmc_data_fi>@dataext)
;


DROP TABLE IF EXISTS presc_ara2;

CREATE TABLE presc_ara2
select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi 
from
import.tractaments
where
((ppfmc_atccodi like 'C09C%') or (ppfmc_atccodi like 'C09D%'))
and (ppfmc_pmc_data_ini<@dataext and ppfmc_data_fi>@dataext)
;

 
/*Agregar 2 columnas mas en la tabla de ic para cada uno de los medicamentos y actualizarlos a 0*/


alter table ic
ADD (ieca DOUBLE,ara2 DOUBLE);

update ic a 
set a.ieca=0
;

update ic a 
set a.ara2=0
;

 
/*Hacer indices de las tablas m�s peque�as con todas las variables que usar�*/

ALTER TABLE presc_ieca
ADD INDEX (id_cip_sec,ppfmc_atccodi,ppfmc_pmc_data_ini,ppfmc_data_fi)
;


ALTER TABLE presc_ara2
ADD INDEX (id_cip_sec,ppfmc_atccodi,ppfmc_pmc_data_ini,ppfmc_data_fi)
;

 
/*Indexo la grande*/

ALTER TABLE ic
ADD INDEX (id_cip_sec,up,uba,data_naix,edat,sexe,ates,institucionalitzat)
;

/*Llenar la prescripci�n de los medicamentos...APRENDIZAJE PARA EL FUTURO: USAR WHERE WHERE Y WHERE */
update ic a 
inner join presc_ieca b
set a.ieca=1
where
a.id_cip_sec=b.id_cip_sec
;


update ic a 
inner join presc_ara2 b
set a.ara2=1
where
a.id_cip_sec=b.id_cip_sec
;



/*Hacer una columna en ic de num*/

alter table ic
ADD (den DOUBLE);

update ic
set den=1;

alter table ic
ADD (num DOUBLE);


update ic a
set a.num=0;

update ic a 
set a.num=1
where ieca=1 and ara2=1;

 

/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table ic add (
cuentas char(5) default 'ES07');

 
/*Crear columna de Periodos que es el periodo*/

alter table ic add (
periodos varchar(10));


DROP TABLE IF EXISTS any_ic;

CREATE TABLE any_ic
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;



select*
from any_ic;

DROP TABLE IF EXISTS periodos_ic1;

CREATE TABLE periodos_ic1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_ic;

select*
from periodos_ic1;

UPDATE ic a
inner join periodos_ic1 b
SET a.periodos = b.tst;

select*
from ic;

 
/*Crear columna de Entidades que es la UP*/

alter table ic add (
entidades char(5));

UPDATE ic
SET entidades = up;


 
/*Crear columna de Tipo*/


alter table ic add (
tipo char(10));

DROP TABLE IF EXISTS edad_ic;

CREATE TABLE edad_ic(
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
ic;

ALTER TABLE edad_ic
ADD INDEX (id_cip_sec)
;

UPDATE ic a
INNER JOIN edad_ic b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;

/*CREAR DETALLES ESTA MAS ABAJO*/

/*Crear Controles*/

alter table ic add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_ic;

CREATE TABLE sexe_ic(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
ic;

alter table ic
add index (id_cip_sec)
;

UPDATE ic a
INNER JOIN sexe_ic b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

 
/*N*/
alter table ic add (N char(1));

update ic
set N='N';

/*DETALLES NOTA: Aqui es importante saber que se hara una tabla para asignados y otra para atesos
porque para clasificarlos como se quiere no se podria porque cada grupo no es excluyente*/

/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table ic add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_ic;

CREATE TABLE institu_ic(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
ic;

select*
from institu_ic;

alter table institu_ic
add index (id_cip_sec)
;

UPDATE ic a
INNER JOIN institu_ic b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es07;
create table pac_es07 as select id_cip_sec id, num, den from ic;
