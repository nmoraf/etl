
/*##########################################################################################################################################

ES11-Tractament mal indicat amb antidepressius pel episodi depressiu major lleu
INDICADOR CL�NICO
ENFERMEDAD CR�NICA CADA FILA ES UN PACIENTE

##########################################################################################################################################*/


/*CREAR DENOMINADOR*/

/*Seleccionar pacientes mayores de 14 a�os para empezar a construi el denominador*/

DROP TABLE IF EXISTS edad_depre;

CREATE TABLE edad_depre
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates,institucionalitzat
from 
nodrizas.assignada_tot
where
edat > 14
;


 
/*Seleccionar problemas de salud y saber si estan activos*/

set @dataext=(select data_ext from nodrizas.dextraccio);
;


DROP TABLE IF EXISTS ps_depre;

CREATE TABLE ps_depre
select
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from
import.problemes
where
pr_cod_ps in ('F32.0','F32.9','C01-F32.0','C01-F32.9')
and ((pr_dde<@dataext and pr_dba>@dataext) or (pr_dde<@dataext and pr_dba = 0)) 
;



/*Esta tabla intermedia me sirve para cada cip quede con un problema de salud cogiendo el ultimo dx*/

drop table if exists ps_depre_ddemax;
create table ps_depre_ddemax
select
id_cip_sec
,pr_cod_ps
,pr_th
,max(pr_dde)as pr_dde
from
ps_depre
group by
id_cip_sec
;



/*4. Voy a hacer el denominador. Pero lo que tengo que hacer antes es hacer un indice de la tabla m�s peque�a*/
ALTER TABLE ps_depre_ddemax
ADD INDEX (id_cip_sec)
;

/*Vamos a unir las dos tablas para tener el denominador. La intermedia y la de poblaci�n adulta*/

DROP TABLE IF EXISTS den_depre;
CREATE TABLE den_depre
select 
a.id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,pr_cod_ps
,if(b.id_cip_sec is not null,1,0) as depre
,pr_dde
from
edad_depre a
left join
ps_depre_ddemax b
on
a.id_cip_sec=b.id_cip_sec
;

/*Esta tabla que har� es para hacer una tabla mas peque�ita y asi poder actualizarla y que sea m�s r�pido*/

DROP TABLE IF EXISTS depre;

CREATE TABLE depre
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,pr_cod_ps
,pr_dde
from
den_depre
where
depre=1
;


/*CREAR NUMERADOR*/

/*Crear tabla de prescripci�n de antidepresivos*/

DROP TABLE IF EXISTS presc_depre;

CREATE TABLE presc_depre
select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi 
from
import.tractaments
where
(ppfmc_atccodi like 'N06A%') 
and (ppfmc_pmc_data_ini<@dataext and ppfmc_data_fi>@dataext)
;

/*Agregar columna mas en la tabla de depreleve para cada uno de los medicamentos y actualizarlos a 0*/


alter table depre
ADD (antidepre DOUBLE);

update depre a 
set a.antidepre=0
;


/*Hacer indices de la tabla m�s peque�a con todas las variables que usar�*/

ALTER TABLE presc_depre
ADD INDEX (id_cip_sec,ppfmc_atccodi,ppfmc_pmc_data_ini,ppfmc_data_fi)
;


/*Indexo la grande*/

ALTER TABLE depre
ADD INDEX (id_cip_sec,up,uba,data_naix,edat,sexe,ates,institucionalitzat)
;

/*Llenar la prescripci�n de los medicamentos...APRENDIZAJE PARA EL FUTURO: USAR WHERE WHERE Y WHERE*/
update depre a 
inner join presc_depre b
set a.antidepre=1
where
a.id_cip_sec=b.id_cip_sec
;




/*Hacer una columna en depreleve de num*/

alter table depre
ADD (den DOUBLE);

update depre
set den=1;

alter table depre
ADD (num DOUBLE);


update depre a
set a.num=0;

update depre a 
set a.num=1
where antidepre=1;




/*Ahora vamos a crear una tabla que son las patologias en las que si estarian indicados los antidepresivos en depresi�n leve (depresi�n moderada F32.1 y depresi�n severa F32.2)*/

DROP TABLE IF EXISTS ps_ant_depre;

CREATE TABLE ps_ant_depre

select 
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
import.problemes
where pr_cod_ps in('F32.1','F32.2','F32.3','C01-F32.1','C01-F32.2','C01-F32.3')
and ((pr_dde<@dataext and pr_dba>@dataext) or (pr_dde<@dataext and pr_dba = 0))
;



/*Crear una columna en depreleve con indicaciones*/

alter table depre
ADD (antecedepre DOUBLE)
;

update depre a
set a.antecedepre=0
;

update depre a 
inner join ps_ant_depre b
set a.antecedepre=1
where
a.id_cip_sec=b.id_cip_sec
and((a.pr_dde between b.pr_dde and b.pr_dba)or (a.pr_dde>b.pr_dde and b.pr_dba = 0))
;


/*Hare una tabla seleccionando en los que no estaria indicado dar antidepre excluyendo a los c�digos F32.1, F32.2, F32.3*/

DROP TABLE IF EXISTS depre_d;

CREATE TABLE depre_d

select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,num
,den
,pr_cod_ps
,pr_dde
from 
depre
where antecedepre=0
;



/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table depre_d add (
cuentas char(5) default 'ES11');

/*Crear columna de Periodos que es el periodo*/

alter table depre_d add (
periodos varchar(10));


DROP TABLE IF EXISTS any_depre;

CREATE TABLE any_depre
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;


DROP TABLE IF EXISTS periodos_depre1;

CREATE TABLE periodos_depre1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_depre;


UPDATE depre_d a
inner join periodos_depre1 b
SET a.periodos = b.tst;


/*Crear columna de Entidades que es la UP*/

alter table depre_d add (
entidades char(5));

UPDATE depre_d
SET entidades = up;


/*Crear columna de Tipo*/


alter table depre_d add (
tipo char(10));

DROP TABLE IF EXISTS edad_depre;

CREATE TABLE edad_depre(
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
depre_d;

ALTER TABLE edad_depre
ADD INDEX (id_cip_sec)
;

UPDATE depre_d a
INNER JOIN edad_depre b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;


/*Crear Controles*/

alter table depre_d add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_depre;

CREATE TABLE sexe_depre(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
depre_d;


ALTER TABLE sexe_depre
ADD INDEX (id_cip_sec)
;

UPDATE depre_d a
INNER JOIN sexe_depre b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table depre_d add (N char(1));

update depre_d
set N='N';

/*Crear Detalles*/


/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/



alter table depre_d add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_depre;

CREATE TABLE institu_depre(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
depre_d;


ALTER TABLE institu_depre
ADD INDEX (id_cip_sec)
;

UPDATE depre_d a
INNER JOIN institu_depre b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es11;
create table pac_es11 as select id_cip_sec id, num, den from depre_d;
