
/*##########################################################################################################################################

ES09-Radiografia de tÃ²rax no indicat en el diagnÃ²stic de la bronquiolitis en poblaciÃ³ pediÃ trica
INDICADOR CLÃ�NICO
ENFERMEDAD AGUDA CADA FILA ES UN EPISODIO

##########################################################################################################################################*/

/*CREAR DENOMINADOR*/

/*Escoger los problemas de salud de la bronquiolitis  un aÃ±o hacia atras*/
set @dataext=(select data_ext from nodrizas.dextraccio);


DROP TABLE IF EXISTS ps_bronquiolitis;

CREATE TABLE ps_bronquiolitis

select 
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
import.problemes
where pr_cod_ps in ('J21','J21.0','J21.8','J21.9','C01-J21','C01-J21.0','C01-J21.8','C01-J21.9') 
and (pr_dde between date_add(@dataext, interval -1 year) and @dataext)
;


DROP TABLE IF EXISTS ps_bronquio_agrup;

CREATE TABLE ps_bronquio_agrup
select
id_cip_sec
,pr_cod_ps
,pr_th
,min(pr_dde) as pr_dde
,pr_dba
from
ps_bronquiolitis
group by
id_cip_sec
order by 1,4 asc
;



/*Crear el Ã­ndice ps*/
ALTER TABLE ps_bronquio_agrup
ADD INDEX (id_cip_sec,pr_cod_ps,pr_th,pr_dde,pr_dba)
;

/*Crear el denominador con la tabla de pediatrÃ­a asignada*/

DROP TABLE IF EXISTS bronquio;

CREATE TABLE bronquio
select 
a.id_cip_sec
,up
,uba
,data_naix
,edat_m
,edat_a edat
,sexe
,institucionalitzat
,ates
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
ps_bronquio_agrup a
inner join
nodrizas.ped_assignada b
where
a.id_cip_sec=b.id_cip_sec
and (datediff(pr_dde,data_naix)/30)<24
;


/* Crear un indice para den_bronquio*/


ALTER TABLE bronquio
ADD INDEX (id_cip_sec,pr_dde)
;

/*NUMERADOR*/

/*Crear el numerador primero explotando las rx de bronquiolitis*/

DROP TABLE IF EXISTS rx_bronquiolitis;

CREATE TABLE rx_bronquiolitis

select
id_cip_sec
,oc_data
,oc_servei_ori
,inf_codi_prova
,inf_radiologia 
from 
nodrizas.nod_proves
where
inf_codi_prova in ('RA00001','RA00002','RA00003','RA00004','RA00005','RA00006','RA00007','RA00591','RA00590','RA01055')
;

DROP TABLE IF EXISTS rx_bronquiolitis_agrup;

CREATE TABLE rx_bronquiolitis_agrup
select
id_cip_sec
,oc_data
,oc_servei_ori
,inf_codi_prova
,inf_radiologia 
from
rx_bronquiolitis
group by
id_cip_sec
,oc_data
;


/*Agregar 2 columnas a bronquio*/


alter table bronquio
ADD (den DOUBLE,num DOUBLE);

/*Llenar el denominador*/
update bronquio
set den=1
;

/*Antes de actualizar hacer Ã­ndices*/

ALTER TABLE bronquio
ADD INDEX (id_cip_sec,pr_dde)
;

ALTER TABLE rx_bronquiolitis_agrup
ADD INDEX (id_cip_sec,oc_data)
;

/*Uniendo directamente la tabla de rx de torax con el denominador. Con un intervalo de 1 mes*/ 
update bronquio 
set num=0
;

update bronquio a 
inner join rx_bronquiolitis_agrup b
set a.num=1 
where
a.id_cip_sec=b.id_cip_sec
and a.pr_dde between DATE_SUB(b.oc_data,INTERVAL 30 DAY)and DATE_ADD(b.oc_data,INTERVAL 30 DAY)
;


/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table bronquio add (
cuentas char(5) default 'ES09');

/*Crear columna de Periodos que es el periodo*/

alter table bronquio add (
periodos varchar(10));


DROP TABLE IF EXISTS any_bronquio;

CREATE TABLE any_bronquio
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;


DROP TABLE IF EXISTS periodos_bronquio1;

CREATE TABLE periodos_bronquio1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_bronquio;


UPDATE bronquio a
inner join periodos_bronquio1 b
SET a.periodos = b.tst;


/*Crear columna de Entidades que es la UP*/

alter table bronquio add (
entidades char(5));

UPDATE bronquio
SET entidades = up;


/*Crear columna de Tipo*/


alter table bronquio add (tipo char(10));

update bronquio
set tipo='EC01';



/*Crear Controles*/

alter table bronquio add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_bronquio;

CREATE TABLE sexe_bronquio(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
bronquio;

ALTER TABLE sexe_bronquio
ADD INDEX (id_cip_sec)
;


UPDATE bronquio a
INNER JOIN sexe_bronquio b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table bronquio add (N char(1));

update bronquio
set N='N';

/*Crear Detalles*/


/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table bronquio add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_bronquio;

CREATE TABLE institu_bronquio(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
bronquio;

ALTER TABLE institu_bronquio
ADD INDEX (id_cip_sec)
;

UPDATE bronquio a
INNER JOIN institu_bronquio b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es09;
create table pac_es09 as select id_cip_sec id, num, den from bronquio;
