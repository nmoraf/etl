# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


MST_TB = "mst_pacients"
KLX_TB = "exp_khalix"
DB = "essencial"


class Main(object):
    """."""

    def __init__(self):
        """."""
        self.create_structure()
        self.get_poblacio()
        self.get_u11()
        self.get_centres()
        for table in u.getDatabaseTables(DB):
            if table[:4] == "pac_":
                Indicador(table, self.poblacio, self.u11, self.centres)

    def create_structure(self):
        """."""
        tables = {MST_TB: "(id int, hash varchar(40), sector varchar(4), \
                            up varchar(5), uba varchar(5), ubainf varchar(5), \
                            ind varchar(10), num int, den int)",
                  KLX_TB: "({})".format(", ".join(["k{} varchar(10)".format(i)
                                                   for i in range(6)] + ["v int"]))}  # noqa
        for table, cols in tables.items():
            u.createTable(table, cols, DB, rm=True)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, \
                      ates, institucionalitzat \
               from assignada_tot"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_u11(self):
        """."""
        sql = "select id_cip_sec, hash_d, codi_sector from u11"
        self.u11 = {row[0]: row[1:] for row in u.getAll(sql, "import")}

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}


class Indicador(object):
    """."""

    def __init__(self, table, poblacio, u11, centres):
        """."""
        self.table = table
        self.indicador = table[-4:].upper()
        self.poblacio = poblacio
        self.u11 = u11
        self.centres = centres
        self.get_data()
        self.set_data()

    def get_data(self):
        """."""
        self.master = []
        self.khalix = c.Counter()
        sql = "select id, num, den from {}".format(self.table)
        for id, num, den in u.getAll(sql, DB):
            if id in self.poblacio:
                up, uba, ubainf, edat, sexe, ates, instit = self.poblacio[id]
                hash, sec = self.u11[id]
                self.master.append((id, hash, sec, up, uba, ubainf, self.indicador, num, den))  # noqa
                br = self.centres[up]
                edat_k = u.ageConverter(edat)
                sexe_k = u.sexConverter(sexe)
                instit_k = "{}INS".format("" if instit else "NO")
                keys = [(self.indicador, br, edat_k, sexe_k, instit_k + "ASS")]
                if ates:
                    keys.append((self.indicador, br, edat_k, sexe_k, instit_k + "AT"))  # noqa
                for key in keys:
                    self.khalix[key + ("DEN",)] += den
                    if num:
                        self.khalix[key + ("NUM",)] += num

    def set_data(self):
        """."""
        u.listToTable(self.master, MST_TB, DB)
        khalix = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(khalix, KLX_TB, DB)


if __name__ == "__main__":
    Main()
