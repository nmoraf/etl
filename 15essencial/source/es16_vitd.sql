
/*##########################################################################################################################################

ES16-Vitamina D en persones grans en la comunitat
INDICADOR CL�NICO
ENFERMEDAD CR�NICA CADA FILA ES UN PACIENTE

##########################################################################################################################################*/



/*CREAR DENOMINADOR*/

/*Edad mayor de 65 a�os*/

drop table if exists den_vit;

create table den_vit
select
id_cip_sec
,up
,uba
,edat
,sexe
,ates
,institucionalitzat
from 
nodrizas.assignada_tot
where
edat > 65
;
 
/*Seleccionar insuficiencia cardiaca y coger los activos a la fecha que yo le digo de extracci�n*/

set @dataext=(select data_ext from nodrizas.dextraccio);
;

drop table if exists vitd;

create table vitd
select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi
from
import.tractaments
where
ppfmc_atccodi in ('A11CB','A11CB91','A11CC','A11CC01','A11CC02','A11CC03','A11CC04','A11CC05','A11CC06','A11CC07','A11CC20',
'A12AX','A12AX91','A12AX92','A12AX93','A12AX94','A12AX96','A12AX97','M05BB03','M05BB04','M05BB05')
and ((ppfmc_pmc_data_ini<@dataext and ppfmc_data_fi>@dataext) or (ppfmc_pmc_data_ini<@dataext and ppfmc_data_fi is null))
;

/*Agregar 2 columnas a den_vit*/

alter table den_vit
ADD (den DOUBLE);

update den_vit
set den=1;

alter table den_vit
ADD (num DOUBLE);


update den_vit a
set a.num=0;


/*Hacer indices*/
alter table den_vit
add index (id_cip_sec)
;

alter table vitd
add index (id_cip_sec)
;


/*Uniendo la tabla de vitd a den_vit*/

update den_vit a
inner join vitd b
set a.num=1
where
a.id_cip_sec=b.id_cip_sec
;


/*Ahora vamos a crear una tabla en las patologias en las que si estaria indicada la vitamina d (deficit de vitamina D y osteoporosis)*/

DROP TABLE IF EXISTS ps_exclusiones;

CREATE TABLE ps_exclusiones

select 
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
import.problemes
where 
((pr_cod_ps like 'E55%')or(pr_cod_ps like 'M80%')or(pr_cod_ps like 'M81%')or(pr_cod_ps like 'M82%'))
and ((pr_dde<@dataext and pr_dba>@dataext) or (pr_dde<@dataext and pr_dba = 0))
;


/*Crear una columna en den_vit con indicaciones*/

alter table den_vit
ADD (ps_exclusiones DOUBLE)
;


update den_vit
set ps_exclusiones=0
;

update den_vit a 
inner join ps_exclusiones b
set a.ps_exclusiones=1
where
a.id_cip_sec=b.id_cip_sec
;


/*Hare una tabla seleccionando en las que no estaria indicado dar vitamina d*/

DROP TABLE IF EXISTS vit_d;

CREATE TABLE vit_d

select 
id_cip_sec
,up
,uba
,edat
,sexe
,ates
,institucionalitzat
,num
,den
,ps_exclusiones
from 
den_vit
where ps_exclusiones=0
;

select*
from vit_d;


/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table vit_d add (
cuentas char(5) default 'ES16');

/*Crear columna de Periodos que es el periodo*/

alter table vit_d  add (
periodos varchar(10));


DROP TABLE IF EXISTS any_vit_d ;

CREATE TABLE any_vit_d 
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;

select*
from any_vit_d ;

DROP TABLE IF EXISTS periodos_vit_d1;

CREATE TABLE periodos_vit_d1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_vit_d ;

select*
from periodos_vit_d1;

UPDATE vit_d  a
inner join periodos_vit_d1 b
SET a.periodos = b.tst;

select*
from vit_d;

/*Crear columna de Entidades que es la UP*/

alter table vit_d  add (
entidades char(5));

UPDATE vit_d 
SET entidades = up;


/*Crear columna de Tipo*/


alter table vit_d  add (
tipo char(10));

DROP TABLE IF EXISTS edad_vit_d ;

CREATE TABLE edad_vit_d (
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
vit_d;

ALTER TABLE edad_vit_d
ADD INDEX (id_cip_sec)
;

UPDATE vit_d a
INNER JOIN edad_vit_d b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;

/*CREAR DETALLES ESTA MAS ABAJO*/

/*Crear Controles*/

alter table vit_d add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_vit_d;

CREATE TABLE sexe_vit_d(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
vit_d;

alter table vit_d
add index (id_cip_sec)
;

UPDATE vit_d a
INNER JOIN sexe_vit_d b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table vit_d add (N char(1));

update vit_d
set N='N';

/*DETALLES NOTA: Aqui es importante saber que se hara una tabla para asignados y otra para atesos porque para clasificarlos como se quiere no se podria porque cada grupo no es excluyente*/

/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table vit_d add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_vit_d;

CREATE TABLE institu_vit_d(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
vit_d;

select*
from institu_vit_d;

alter table institu_vit_d
add index (id_cip_sec)
;

UPDATE vit_d a
INNER JOIN institu_vit_d b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es16;
create table pac_es16 as select id_cip_sec id, num, den from vit_d;

