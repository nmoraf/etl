/*##########################################################################################################################################
ES-10 Proves d'imatge en la sinusitis en l'edat pedi�trica
INDICADOR CL�NICO
ENFERMEDAD AGUDA CADA FILA ES UN EPISODIO

##########################################################################################################################################*/



/*CREAR DENOMINADOR*/

/*ESTE SE VA A HACER EN MENORES DE 6 A�OS QUE ES DONDE NO ES CONTROVERTIDO EL USO DE PRUEBAS DE IMAGEN*/

/*Escoger los problemas de salud de sinusitis un a�o hacia atras*/
set @dataext=(select data_ext from nodrizas.dextraccio);

DROP TABLE IF EXISTS ps_sinusitis;

CREATE TABLE ps_sinusitis

select 
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
import.problemes
where pr_cod_ps in ('J01','J01.0','J01.1','J01.2','J01.3','J01.4','J01.8','J01.9','C01-J01.90','C01-J01.00','C01-J01.10','C01-J01.20','C01-J01.30','C01-J01.40','C01-J01.80','C01-J01.90') 
and (pr_dde between date_add(@dataext, interval -1 year) and @dataext)
;

DROP TABLE IF EXISTS ps_sinusitis_agrup;

CREATE TABLE ps_sinusitis_agrup
select
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from
ps_sinusitis
group by
id_cip_sec
,pr_dde
;

/*Crear �ndicede ps*/
ALTER TABLE ps_sinusitis_agrup
ADD INDEX (id_cip_sec,pr_cod_ps,pr_th,pr_dde,pr_dba)
;

/*Menores de 6 a�os*/

DROP TABLE IF EXISTS sinusitis;

CREATE TABLE sinusitis
select 
a.id_cip_sec
,up
,uba
,data_naix
,sexe
,edat_a edat
,institucionalitzat
,ates
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
ps_sinusitis_agrup a
inner join
nodrizas.ped_assignada b
where
a.id_cip_sec=b.id_cip_sec
and (datediff(pr_dde,data_naix)/30)<72
;



/*Crear un indice para esta table*/


ALTER TABLE sinusitis
ADD INDEX (id_cip_sec,pr_dde)
;

/*NUMERADOR*/

/*Crear el numerador primero explotando las imagenes de sinusitis*/
DROP TABLE IF EXISTS rx_sinusitis;

CREATE TABLE rx_sinusitis

select
id_cip_sec
,oc_data
,oc_servei_ori
,inf_codi_prova
,inf_radiologia 
from nodrizas.nod_proves
where
inf_codi_prova in ('RA00021','RA00020','RA00211','RA00212','RA00601','RA00602')
;

DROP TABLE IF EXISTS rx_sinusitis_agrup;

CREATE TABLE rx_sinusitis_agrup
select
id_cip_sec
,oc_data
,oc_servei_ori
,inf_codi_prova
,inf_radiologia 
from
rx_sinusitis
group by
id_cip_sec
,oc_data
;
/*Agregar 2 columnas a sinusitis*/


alter table sinusitis
ADD (den DOUBLE,num DOUBLE);

/*Llenar el denominador*/
update sinusitis
set den=1
;

/*Antes de actualizar hacer �ndices*/

ALTER TABLE sinusitis
ADD INDEX (id_cip_sec,pr_dde)
;

ALTER TABLE rx_sinusitis
ADD INDEX (id_cip_sec,oc_data)
;

/*Uniendo directamente la tabla de imagenes con el denominador. Con un intervalo de 1 mes*/ 
update sinusitis
set num=0
;

update sinusitis a 
inner join rx_sinusitis_agrup b
set a.num=1 
where
a.id_cip_sec=b.id_cip_sec
and a.pr_dde between DATE_SUB(b.oc_data,INTERVAL 30 DAY)and DATE_ADD(b.oc_data,INTERVAL 30 DAY)
;


/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table sinusitis add (
cuentas char(5) default 'ES10');

/*Crear columna de Periodos que es el periodo*/

alter table sinusitis add (
periodos varchar(10));


DROP TABLE IF EXISTS any_sinusitis;

CREATE TABLE any_sinusitis
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;

select*
from any_sinusitis;

DROP TABLE IF EXISTS periodos_sinusitis1;

CREATE TABLE periodos_sinusitis1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_sinusitis;


UPDATE sinusitis a
inner join periodos_sinusitis1 b
SET a.periodos = b.tst;



/*Crear columna de Entidades que es la UP*/

alter table sinusitis add (
entidades char(5));

UPDATE sinusitis
SET entidades = up;


/*Crear columna de Tipo*/

alter table sinusitis add (
tipo char(10));

DROP TABLE IF EXISTS edad_sinusitis;

CREATE TABLE edad_sinusitis(
edat5 varchar(10)
)
select
id_cip_sec
,pr_dde
,data_naix
,if(TRUNCATE(datediff(pr_dde,data_naix)/30,0)<24,'EC01',
  if(TRUNCATE(datediff(pr_dde,data_naix)/30,0) between 24 and 59,'EC24',
  if(TRUNCATE(datediff(pr_dde,data_naix)/30,0) between 60 and 108,'EC59',
  if(TRUNCATE(datediff(pr_dde,data_naix)/30,0) between 109 and 168,'EC1519',0)))) as edat5
from
sinusitis;

ALTER TABLE edad_sinusitis
ADD INDEX (id_cip_sec)
;

UPDATE sinusitis a
INNER JOIN edad_sinusitis b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;



/*Crear Controles*/

alter table sinusitis add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_sinusitis;

CREATE TABLE sexe_sinusitis(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
sinusitis;

ALTER TABLE sexe_sinusitis
ADD INDEX (id_cip_sec)
;

UPDATE sinusitis a
INNER JOIN sexe_sinusitis b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table sinusitis add (N char(1));

update sinusitis
set N='N';


/*Crear Detalles*/

/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table sinusitis add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_sinu;

CREATE TABLE institu_sinu(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
sinusitis;


ALTER TABLE institu_sinu
ADD INDEX (id_cip_sec)
;


UPDATE sinusitis a
INNER JOIN institu_sinu b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es10;
create table pac_es10 as select id_cip_sec id, num, den from sinusitis;
