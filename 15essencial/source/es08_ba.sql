
/*##########################################################################################################################################

ES08-Tractament inadequat amb antibi�tics en pacients amb bacteri�ria asimptom�tica
INDICADOR CL�NICO
ENFERMEDAD AGUDA CADA FILA ES UN EPISODIO

##########################################################################################################################################*/

/*CREAR DENOMINADOR*/

/*Escoger los problemas de salud de la bacteriuria asintom�tica del Julio 2014 un a�o hacia atras*/
set @dataext=(select data_ext from nodrizas.dextraccio);


DROP TABLE IF EXISTS ps_ba;

CREATE TABLE ps_ba

select 
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
import.problemes
where (pr_cod_ps in ('N39.0', 'C01-N39.0') and pr_th = '2485')
and (pr_dde between date_add(@dataext, interval -1 year) and @dataext)
;


/*Crear el �ndice ps*/
ALTER TABLE ps_ba
ADD INDEX (id_cip_sec,pr_cod_ps,pr_th,pr_dde,pr_dba)
;

/*Crear el denominador ba con poblaci�n asignada con la tabla de nodrizas.assignada_tot*/

DROP TABLE IF EXISTS ba;

CREATE TABLE ba

select 
a.id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
ps_ba a
inner join
nodrizas.assignada_tot b
on
a.id_cip_sec=b.id_cip_sec
;


/*Crear un indice para el den_ba*/


ALTER TABLE ba
ADD INDEX (id_cip_sec,pr_dde)
;

/*CREAR NUMERADOR*/

/*Crear el numerador primero explotando los antibi�ticos  para ITU*/

DROP TABLE IF EXISTS presc_ba;

CREATE TABLE presc_ba

select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi 
from
import.tractaments
where
ppfmc_atccodi in ('J01XX01','J01CR02','J01MA06','J01MA02','J01MA01','J01XE01')
;

DROP TABLE IF EXISTS presc_ba_agrup;

CREATE TABLE presc_ba_agrup
select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi
from
presc_ba
group by
id_cip_sec
,ppfmc_pmc_data_ini
;




/*Agregar 2 columnas a den_ba */

alter table ba
ADD (den DOUBLE,num DOUBLE);

/*Llenar las columnas con 1 y 0 segun cumpla la condici�n*/

update ba a 
set a.den=1
;

/*Actualizando directamente la tabla de den_ba con la de prescripci�n*/

update ba
set num=0
;

update ba a 
inner join presc_ba b
set a.num=1 
where
a.id_cip_sec=b.id_cip_sec
and datediff (b.ppfmc_pmc_data_ini,a.pr_dde) between 0 and 8
;


/*Ahora vamos a crear una tabla que son las patologias en las que si estarian indicados los antibioticos con un dx de bacteriuria asintomatica*/

DROP TABLE IF EXISTS ps_emb_rvu;

CREATE TABLE ps_emb_rvu

select 
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
import.problemes
where pr_cod_ps in('O30','O30.0','O30.1','O30.2','O30.8','O30.9','O36.9','Z32.1','Q62.7','C01-O30.90','C01-O30.009','C01-O30.109','C01-O30.209','C01-O30.809','C01-O30.90','C01-O36.90X9','C01-Q62.7','C01-Z34.90')
;

DROP TABLE IF EXISTS ps_emb_rvu_agrup;

CREATE TABLE ps_emb_rvu_agrup
select
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from
ps_emb_rvu
group by
id_cip_sec
,pr_dde
;

/*Crear una columna en den_ba con indicaciones*/

alter table ba
ADD (emb_rvu DOUBLE)
;


update ba
set emb_rvu=0
;

update ba a 
inner join ps_emb_rvu_agrup b
set a.emb_rvu=1
where
a.id_cip_sec=b.id_cip_sec
and((a.pr_dde between b.pr_dde and b.pr_dba)or (a.pr_dde>b.pr_dde and b.pr_dba = 0))
;

select*
from ba;

/*Hare una tabla seleccionando en los que no estaria indicado dar antibioticos excluyendo a las mujeres embarazadas o que tengan reflujo*/

DROP TABLE IF EXISTS ba_d;

CREATE TABLE ba_d

select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,num
,den
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from 
ba
where emb_rvu=0
;


/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador^*/
alter table ba_d add (
cuentas char(5) default 'ES08');

/*Crear columna de Periodos que es el periodo*/

alter table ba_d add (
periodos varchar(10));


DROP TABLE IF EXISTS any_ba;

CREATE TABLE any_ba
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;

select*
from any_ba;

DROP TABLE IF EXISTS periodos_ba1;

CREATE TABLE periodos_ba1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_ba;

select*
from periodos_ba1;

UPDATE ba_d a
inner join periodos_ba1 b
SET a.periodos = b.tst;

select*
from ba_d;

/*Crear columna de Entidades que es la UP*/

alter table ba_d add (
entidades char(5));

UPDATE ba_d
SET entidades = up;


/*Crear columna de Tipo*/


alter table ba_d add (
tipo char(10));

DROP TABLE IF EXISTS edad_ba;

CREATE TABLE edad_ba(
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
ba_d;

ALTER TABLE edad_ba
ADD INDEX (id_cip_sec)
;

UPDATE ba_d a
INNER JOIN edad_ba b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;


/*Crear Controles*/

alter table ba_d add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_ba;

CREATE TABLE sexe_ba(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
ba_d;

ALTER TABLE sexe_ba
ADD INDEX (id_cip_sec)
;

UPDATE ba_d a
INNER JOIN sexe_ba b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table ba_d add (N char(1));

update ba_d
set N='N';

/*Crear Detalles*/


/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table ba_d add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_ba;

CREATE TABLE institu_ba(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
ba_d;

select*
from institu_ba;

ALTER TABLE institu_ba
ADD INDEX (id_cip_sec)
;

UPDATE ba_d a
INNER JOIN institu_ba b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es08;
create table pac_es08 as select id_cip_sec id, num, den from ba_d;
