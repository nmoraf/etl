# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
import datetime

db = 'essencial'
nod = 'nodrizas'
imp = 'import'

agr = 0

cataleg_essencial = {'ES04': {
                                'calcular': True,
                                'desc': ' Criteri clínic en mamografies fora del programa de cribatge',
                                'poblacio': 'A',
                                'den': 'al codi directament',
                                'num': 'al codi directament',
                                'excl': '',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES17': {
                                'calcular': True,
                                'desc': 'AINE en malaltia cardiovascular, renal crònica o insuficiència hepàtica',
                                'poblacio': 'A',
                                'den': '(1, 7, 11, 55, 53, 160)',
                                'num': '(118)',
                                'excl': '',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES18': {
                                'calcular': True,
                                'desc': 'Broncodilatadors en lactants amb bronquiolitis',
                                'poblacio': 'P',
                                'den': '(617, 618, 298, 299)',
                                'num': '(598,599)',
                                'excl': '',
                                'edat_min': 0,
                                'edat_max': 1,
                             },
                    'ES19': {
                                'calcular': True,
                                'desc': 'Ús de salbutamol en menors de 24 mesos',
                                'poblacio': 'P',
                                'den': '',
                                'num': '(598, 599)',
                                'excl': '',
                                'edat_min': 0,
                                'edat_max': 1,
                             },
                    'ES20': {
                                'calcular': True,
                                'desc': 'Pacients amb diagnòstic específic de vertigen',
                                'poblacio': 'A',
                                'den': '(614, 615, 616)',
                                'num': '(615, 616)',
                                'excl': '',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES21': {
                                'calcular': True,
                                'desc': 'Pacients amb sedants vestibulars',
                                'poblacio': 'A',
                                'den': '',
                                'num': '(600, 601, 602, 603, 606, 607, 608, 609, 610, 611, 612, 613)',
                                'excl': '',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES22': {
                                'calcular': True,
                                'desc': 'Pacients amb betahistina, sulpiride i dimenhidrinat',
                                'poblacio': 'A',
                                'den': '(614)',
                                'num': '(600, 601, 602, 603, 604, 605)',
                                'excl': '',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES23': {
                                'calcular': True,
                                'desc': 'Medicaments sedants vestibulars per al vertigen benigne',
                                'poblacio': 'A',
                                'den': '(614)',
                                'num': '(600, 601, 602, 603, 606, 607, 608, 609, 610, 611, 612, 613)',
                                'excl': '',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES27': {
                                'calcular': True,
                                'desc': 'Radiografia de tòrax en asma ',
                                'poblacio': 'P',
                                'den': '(58)',
                                'num': '(643)',
                                'excl': '',
                                'edat_min': 0,
                                'edat_max': 14,
                             },
                    'ES28': {
                                'calcular': True,
                                'desc': 'Antibiòtics en infeccions del tracte respiratori inferior en adults',
                                'poblacio': 'A',
                                'den': '(244, 245)',
                                'num': '(139, 140)',
                                'excl': '(62, 157, 158, 159, 136, 58, 250, 251, 252, 253, 141, 142, 470, 471)',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    'ES30': {
                                'calcular': True,
                                'desc': 'Beta-agonistes de llarga durada en asma',
                                'poblacio': 'A',
                                'den': '(58)',
                                'num': '(641)',
                                'excl': '(642)',
                                'edat_min': 15,
                                'edat_max': 200,
                             },
                    }


def get_dextraccio():
    dext = {}
    sql = 'select data_ext from dextraccio'
    for data, in getAll(sql, nod):
        dext = data
    return dext


def get_poblacio():
    pob = {}
    sql = "select id_cip_sec, up, uba, edat, sexe, if(institucionalitzat=1,'INS','NOINS'), ates, institucionalitzat from assignada_tot"
    for id, up, uba, edat, sexe, insti, ates, insti_raw in getAll(sql, nod):
        pob[id] = {'up': up, 'uba': uba, 'edat': edat, 'sexe': sexe, 'insti': insti, 'ates': ates, 'insti_raw': insti_raw}
    return pob
    
def get_pedPoblacio():
    Pedpob = {}
    sql = "select id_cip_sec, up, uba, edat_a, sexe, if(institucionalitzat=1,'INS','NOINS'), ates, institucionalitzat from ped_assignada"
    for id, up, uba, edat, sexe, insti, ates, insti_raw in getAll(sql, nod):
        Pedpob[id] = {'up': up, 'uba': uba, 'edat': edat, 'sexe': sexe, 'insti': insti, 'ates': ates, 'insti_raw': insti_raw}
    return Pedpob


def get_centres():
    centres = {}
    sql = "select scs_codi, ics_codi from cat_centres"
    for up, br in getAll(sql, nod):
        centres[up] = br
    return centres


def get_problemes(agr):
    problemes = {}
    sql = 'select id_cip_sec, dde from eqa_problemes where ps in {0}'.format(agr)
    for id, dde in getAll(sql, nod):
        problemes[id] = dde
    return problemes

def get_problemes_incid(agr):
    problemes_incid = defaultdict(list)
    sql = 'select id_cip_sec, dde from eqa_problemes_incid where ps in {0}'.format(agr)
    for id, dde in getAll(sql, nod):
        problemes_incid[id].append([dde])
    return problemes_incid
    
def get_problemes_incid_first(agr):
    problemes_incid_first = {}
    sql = 'select id_cip_sec, dde from eqa_problemes_incid where ps in {0} order by 1,2 asc'.format(agr)
    actual=0
    for id, dde in getAll(sql, nod):
        if id != actual:
            problemes_incid_first[id] = dde
            id = actual
    return problemes_incid_first

def get_tractaments(agr):
    tractaments = {}
    sql = 'select id_cip_sec, pres_orig from eqa_tractaments where farmac in {0} order by pres_orig asc'.format(agr)
    for id, data in getAll(sql, nod):
        tractaments[id] = data
    return tractaments

def get_tractaments_all(agr):
    tractaments_all = defaultdict(list)
    sql = 'select id_cip_sec, pres_orig from eqa_tractaments where farmac in {0} order by pres_orig asc'.format(agr)
    for id, data in getAll(sql, nod):
        tractaments_all[id].append([data])
    return tractaments_all
    
def get_proves_all(agr):
    proves_all = defaultdict(list)
    sql = 'select id_cip_sec, data from eqa_proves where agrupador in {0} order by data asc'.format(agr)
    for id, data in getAll(sql, nod):
        proves_all[id].append([data])
    return proves_all
    
def get_variables_assir(agr, text):
    variables_ass = {}
    sql = "select id_cip_sec, dat from ass_variables where agrupador in {0} {1} order by dat asc".format(agr, text)
    for id, data in getAll(sql, nod):
        variables_ass[id] = data
    return variables_ass
    
def get_rinitis():
    id_rinitis = {}
    dext = get_dextraccio()
    codis_rinitis = ('J30.2','J30','J30.1','J30.3','J30.4','477-','C01-J30.8','C01-J30.81','C01-J30.89','C01-J30.9','C01-J30.1','C01-J30.2','C01-J30','C01-J30.5')
    sql = "select id_cip_sec, pr_dde from problemes where pr_cod_ps in {0}".format(codis_rinitis)
    for id, data in getAll(sql, imp):
        fa = monthsBetween(data, dext)
        if 0 <= fa <= 11:
            id_rinitis[id] = data
    return id_rinitis
    
def get_ES04(dens, nums, emin, emax, pob, dext):
    ES04_ind = {}
    mamografies = get_variables_assir('(394)', ' and val_num=1')
    mamografies_ok = get_variables_assir('(395,396)', "and left(val_txt,5)<>'Sense'")
    problemes = get_problemes('(411)')
    for id, rows in pob.items():
        edat = rows['edat']
        sexe = rows['sexe']
        if 14 < edat < 50:
            if sexe == 'D':
                if id in mamografies:
                    dmamo = mamografies[id]
                    fa = monthsBetween(dmamo, dext)
                    if 0 <= fa <= 11:
                        den = 1
                        num = 0
                        excl = 0
                        if id in mamografies_ok:
                            dmamook = mamografies_ok[id]
                            fa = monthsBetween(dmamook, dext)
                            if 0 <= fa <= 11:
                                num =1
                        if id in problemes:
                            num = 1
                        ES04_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES04_ind

def get_ES17(dens, nums, emin, emax, pob, dext):
    ES17_ind = {}
    denominador = get_problemes(dens)
    numerador = get_tractaments(nums)
    for id, dde in denominador.items():
        try:
            edat = pob[id]['edat']
        except KeyError:
            continue
        if emin <= edat <= emax:    
            den = 1
            num = 0
            excl = 0
            if id in numerador:
                num = 1
            ES17_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES17_ind

def get_ES18(dens, nums, emin, emax, pob, dext):
    pre18 = {}
    ES18_ind = {}
    denominador = get_problemes_incid_first(dens)
    numerador = get_tractaments_all(nums)
    for id, rows in pob.items():
        edat = rows['edat']
        if emin <= edat <= emax:
            if id in denominador:
                dde = denominador[id]
                #dde, = denominador[id]
                fa = monthsBetween(dde, dext)
                if 0 <= fa <= 11:
                    den = 1
                    num = 0
                    excl = 0
                    if id in numerador:
                        #for dat, in numerador[id]:
                        for dat, in numerador[id]:
                            en_dies = daysBetween(dat, dde)
                            if 0<= en_dies <= 7:
                                num = 1
                    pre18[(id, dde)] = {'num': num, 'den': den, 'excl': excl}
    for (id, dde), rows in pre18.items():
        num = rows['num']
        den = rows['den']
        excl = rows['excl']
        if id in ES18_ind:
            ES18_ind[id]['num'] += num
            ES18_ind[id]['den'] += den
            ES18_ind[id]['excl'] += excl
        else:
            ES18_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES18_ind    
  
def get_ES19(dens, nums, emin, emax, pedPob, dext):
    ES19_ind = {}
    numerador = get_tractaments(nums)
    for id, rows in pedPob.items():
        edat = rows['edat']
        if emin <= edat <= emax:    
            den = 1
            num = 0
            excl = 0
            if id in numerador:
                dat = numerador[id]
                fa = monthsBetween(dat, dext)
                if 0 <= fa <= 11:
                    num = 1
            ES19_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES19_ind


def get_ES20(dens, nums, emin, emax, pob, dext):
    ES20_ind = {}
    denominador = get_problemes(dens)
    numerador = get_problemes(nums)
    for id, dde in denominador.items():
        try:
            edat = pob[id]['edat']
        except KeyError:
            continue
        if emin <= edat <= emax:    
            den = 1
            num = 0
            excl = 0
            if id in numerador:
                num = 1
            ES20_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES20_ind
    

def get_ES21(dens, nums, emin, emax, pob, dext):
    ES21_ind = {}
    numerador = get_tractaments(nums)
    for id, rows in pob.items():
        edat = rows['edat']
        if emin <= edat <= emax:    
            den = 1
            num = 0
            excl = 0
            if id in numerador:
                dat = numerador[id]
                fa = monthsBetween(dat, dext)
                if 0 <= fa <= 11:
                    num = 1
            ES21_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES21_ind

    
def get_ES22(dens, nums, emin, emax, pob, dext):
    ES22_ind = {}
    numerador = get_tractaments(nums)
    for id, rows in pob.items():
        edat = rows['edat']
        if emin <= edat <= emax:    
            den = 1
            num = 0
            excl = 0
            if id in numerador:
                dat = numerador[id]
                fa = monthsBetween(dat, dext)
                if 0 <= fa <= 11:
                    num = 1
            ES22_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES22_ind

def get_ES23(dens, nums, emin, emax, pob, dext):
    ES23_ind = {}
    denominador = get_problemes(dens)
    numerador = get_tractaments_all(nums)
    
    #trobar els id's del denominador que tenen rinitis el darrer any
    rinitis_id = get_rinitis()
    
    for id, dde in denominador.items():
        try:
            edat = pob[id]['edat']
        except KeyError:
            continue
        if emin <= edat <= emax and id not in rinitis_id:
            fa = monthsBetween(dde, dext)
            if 0 <= fa <= 11:
                den = 1
                num = 0
                excl = 0
                if id in numerador:
                    for dat, in numerador[id]:
                        en_dies = daysBetween(dat, dde)
                        if 0<= en_dies <= 15:
                            num = 1
                #Explorar per up's
                up = pob[id]['up']
                ES23_ind[id] = {'up': up, 'num': num, 'den': den, 'excl': excl}
    return ES23_ind

def get_ES27(dens, nums, emin, emax, pedPob, dext):
    pre27 = {}
    ES27_ind = {}
    denominador = get_problemes_incid(dens)
    numerador = get_proves_all(nums)
    for id, rows in pedPob.items():
        edat = rows['edat']
        if emin <= edat <= emax:
            if id in denominador:
                for dde, in denominador[id]:
                    fa = monthsBetween(dde, dext)
                    if 0 <= fa <= 11:
                        den = 1
                        num = 0
                        excl = 0
                        if id in numerador:
                            for dat, in numerador[id]:
                                fa_mesos = monthsBetween(dde, dat)
                                if 0 <= fa_mesos <= 5:
                                    num = 1
                                en_mesos = monthsBetween(dat, dde)
                                if 0 <= en_mesos <= 2:
                                    num = 1
                        pre27[(id, dde)] = {'num': num, 'den': den, 'excl': excl}
    for (id, dde), rows in pre27.items():
        num = rows['num']
        den = rows['den']
        excl = rows['excl']
        if id in ES27_ind:
            ES27_ind[id]['num'] += num
            ES27_ind[id]['den'] += den
            ES27_ind[id]['excl'] += excl
        else:
            ES27_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES27_ind  

def get_ES28(dens, nums, emin, emax, pob, dext, excls):
    pre28 = {}
    ES28_ind = {}
    denominador = get_problemes_incid(dens)
    numerador = get_tractaments_all(nums)
    exclusions = get_problemes(excls)
    for id, rows in pob.items():
        edat = rows['edat']
        if emin <= edat <= emax:
            if id in denominador:
                for dde, in denominador[id]:
                    fa = monthsBetween(dde, dext)
                    if 0 <= fa <= 11:
                        den = 1
                        num = 0
                        excl = 0
                        if id in numerador:
                            for dat, in numerador[id]:
                                en_dies = daysBetween(dat, dde)
                                if 0<= en_dies <= 7:
                                    num = 1
                        if id in exclusions:
                            excl = 1
                        pre28[(id, dde)] = {'num': num, 'den': den, 'excl': excl}
    for (id, dde), rows in pre28.items():
        num = rows['num']
        den = rows['den']
        excl = rows['excl']
        if id in ES28_ind:
            ES28_ind[id]['num'] += num
            ES28_ind[id]['den'] += den
            ES28_ind[id]['excl'] += excl
        else:
            ES28_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES28_ind      

def get_ES30(dens, nums, emin, emax, pob, dext,excls):
    ES30_ind = {}
    denominador = get_problemes(dens)
    numerador = get_tractaments(nums)
    exclusions = get_tractaments(excls)
    for id, dde in denominador.items():
        try:
            edat = pob[id]['edat']
        except KeyError:
            continue
        if emin <= edat <= emax:    
            den = 1
            num = 0
            excl = 0
            if id in numerador:
                num = 1
            if id in exclusions:
                num = 0
            ES30_ind[id] = {'num': num, 'den': den, 'excl': excl}
    return ES30_ind
    
pob = get_poblacio()
pedPob = get_pedPoblacio()
dext = get_dextraccio()
centres = get_centres()    
for indicador in cataleg_essencial:
    dens = cataleg_essencial[indicador]['den']
    nums = cataleg_essencial[indicador]['num']
    emin = cataleg_essencial[indicador]['edat_min']
    emax = cataleg_essencial[indicador]['edat_max']
    desc = cataleg_essencial[indicador]['desc']
    calcular = cataleg_essencial[indicador]['calcular']
    poblacio = cataleg_essencial[indicador]['poblacio']
    excls = cataleg_essencial[indicador]['excl']
    if calcular:
        table = 'exp_resultados_khalix_' + str(indicador)
        if indicador == 'ES04':
            resultats = get_ES04(dens, nums, emin, emax, pob, dext)
        if indicador == 'ES17':
            resultats = get_ES17(dens, nums, emin, emax, pob, dext)
        elif indicador == 'ES18':
            resultats = get_ES18(dens, nums, emin, emax, pedPob, dext)
        elif indicador == 'ES19':
            resultats = get_ES19(dens, nums, emin, emax, pedPob, dext)
        elif indicador == 'ES20':
            resultats = get_ES20(dens, nums, emin, emax, pob, dext)
        elif indicador == 'ES21':
            resultats = get_ES21(dens, nums, emin, emax, pob, dext)
        elif indicador == 'ES22':
            resultats = get_ES22(dens, nums, emin, emax, pob, dext)
        elif indicador == 'ES23':
            resultats_per_up = get_ES23(dens, nums, emin, emax, pob, dext)
        elif indicador == 'ES28':
            resultats = get_ES28(dens, nums, emin, emax, pob, dext, excls)
        elif indicador == 'ES27':
            resultats = get_ES27(dens, nums, emin, emax, pedPob, dext)
        elif indicador == 'ES30':
            resultats = get_ES30(dens, nums, emin, emax, pob, dext, excls)
        any,any2,mes = getKhalixDates()
        periodo = 'B' + str(any2) + str(mes)
        recompte = Counter()
        master = []
        
        #Explorar per UP's si indicador és ES23
        if indicador == 'ES23':
            for id, rows in resultats_per_up.items():
                sexe, edat, up, uba, insti, ates, insti_raw, den, num, excl = pob[id]['sexe'], pob[id]['edat'], pob[id]['up'], pob[id]['uba'], pob[id]['insti'],pob[id]['ates'], pob[id]['insti_raw'],rows['den'],rows['num'],rows['excl']
                if int(excl) == 0:
                    master.append((id, up, num, den))
                    
            master_tb = "pac_{}".format(indicador)
            createTable(master_tb, "(id int, up varchar(6), num int, den int)", db, rm=True)
            listToTable(master, master_tb, db)
        
        else:
            for id, rows in resultats.items():
                if poblacio == 'A':
                    sexe, edat, up, uba, insti, ates, insti_raw, den, num, excl = pob[id]['sexe'], pob[id]['edat'], pob[id]['up'], pob[id]['uba'], pob[id]['insti'],pob[id]['ates'], pob[id]['insti_raw'],rows['den'],rows['num'],rows['excl']
                elif poblacio == 'P':
                    sexe, edat, up, uba, insti, ates, insti_raw, den, num, excl = pedPob[id]['sexe'], int(pedPob[id]['edat']), pedPob[id]['up'], pedPob[id]['uba'], pedPob[id]['insti'],pedPob[id]['ates'], pedPob[id]['insti_raw'],rows['den'],rows['num'],rows['excl']
                if int(excl) == 0:
                    master.append((id, num, den))
        
            master_tb = "pac_{}".format(indicador)
            createTable(master_tb, "(id int, num int, den int)", db, rm=True)
            listToTable(master, master_tb, db)
        
 