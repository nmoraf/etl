from sisapUtils import *
import datetime, time

##### Indicador:(Event,edat,Ab,periodettm,Exclusionsps, exclusionsfar) #####

antibiotic=('193','140')
ituamigpneu=('141','142','250','251','158','159')
asmacardio=('58','630')
isupresors=('41')
nod_db = 'nodrizas'
ps='ped_problemes'
farma='eqa_tractaments'


#####  Indicadors ES24 Antibiotics en bronquiolitis en lactants,  #####
ES24={'event':('298','299','617','618'), 'edat':24,'Ab':antibiotic, 'periode':3, 'exclusio':ituamigpneu, 'farma':0, 'idep':False}
#####  ES25 Antibiotics en bronquitis en poblacio pediatrica i    #####
ES25={'event':('244','245'), 'edat':179,'Ab':antibiotic, 'periode':2, 'exclusio':ituamigpneu+asmacardio, 'farma':isupresors, 'idep':True}
#####   ES26 Antibiotics en rinosinusitis aguda en infants        #####
ES26={'event':('628','629'), 'edat':179,'Ab':antibiotic, 'periode':10, 'exclusio':ituamigpneu, 'farma':0, 'idep':False}
dictind = {'ES24':ES24, 'ES25':ES25, 'ES26':ES26}


################################################################



def get_idepre (params): 
    edat=params['edat']
    sql = "select id_cip_sec from {} where ps=40 and edat_m<={}".format(ps, edat)
    idepre = set([id for id, in getAll(sql, nod_db)])  
        
        
    sql = "select id_cip_sec from {} where farmac={} ".format(farma, isupresors)
    isupri = set([id for id, in getAll(sql, nod_db)])
    idepres=idepre.union(isupri)
    
    return idepres

      
def get_denominador (params, taula):
    event=params['event']
    edat=params['edat']
    exclusio= params['exclusio']
    interval=params['periode']
    psevent={}
    psexclusio={}
    dos=()  
    
      
    sql = "select id_cip_sec, dde, ps, edat_m from {}, nodrizas.dextraccio where ps in {} and edat_m <= {} and edat_m >= 0 and dde between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext".format(taula, event+exclusio, edat+1)
    
    for id, data, ps, edat_m in getAll(sql, nod_db):
        if str(ps) in event and edat_m <= edat:
              
            if id not in psevent:
                psevent[id]=[]
            psevent[id].append(data)
            
        elif str(ps) in exclusio:  
            if id not in psexclusio: 
                psexclusio[id]=[]
            psexclusio[id].append(data)
         
           
    dos=(psevent,psexclusio)    
    return dos

def get_farmacs (params, taula):
    ab=params
    farmacsid={}  
    
    sql = "select id_cip_sec, pres_orig from {}, nodrizas.dextraccio where farmac in {} and pres_orig between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext".format(taula, ab)
    for id, data in getAll(sql, nod_db):
        if id not in farmacsid:
            farmacsid[id]=[]
        farmacsid[id].append(data)
    
    return farmacsid
    
    
    
    
def genera_taula (indicador, psevent, psexclusio, idepre, antibiotics):
    taula=[['id','data','exclusio','deprimit','ab'],]
    periode=indicador['periode']
    for id in psevent:
        if idepre:    
            if id in idepre:
                deprimit=1
            else:
                deprimit=0
        elif not idepre:
            deprimit=0
        
        if id in psexclusio:
            loopexclusio=True
          
        else:
            loopexclusio=False
            exclusio=0
        
        if id in antibiotics:
            loopab=True
        else:
            loopab=False
            ab=0
            
        for data in psevent[id]:
            
            if loopexclusio:
                for dataexcl in psexclusio[id]:
                    mesosexcl = monthsBetween(data,dataexcl) 
                    if mesosexcl<=1 and mesosexcl>=-1: 
                        exclusio=1
                        break
                    else:
                        exclusio=0
            if loopab:
                for dataab in antibiotics[id]:
                    diesab = daysBetween(data,dataab) 
                    if diesab<=periode and diesab>=0: 
                        ab=1
                        break
                    else:
                        ab=0
            
            fila=[id,data,exclusio,deprimit,ab]
            taula.append(fila)
    
               
      
    
    return taula


def tau_a_dict(taula):

    numerador={}
    denominador={} 

    for fila in taula:
        
        
        if fila[0]=='id':
            continue
        id=int(fila[0])
        exclusio=int(fila[2])
        idepre=int(fila[3])
        ab=int(fila[4])



        if exclusio!=1 and  idepre!=1:
            if id not in denominador:
                denominador[id]=1
            else:
                denominador[id]+=1

            if ab==1:
                if id not in numerador:
                    numerador[id]=1
                else:
                    numerador[id]+=1


    numden=[numerador, denominador]  
    return numden

def preparataula(params, numerador, denominador, nomindicador):
    
    edat=params['edat']/12
    ### diccionari codis us ###
    diccionariup={}
    sqlup= "select scs_codi, ics_codi from cat_centres"
    for scs, icscodi in getAll(sqlup, nod_db):
        diccionariup[scs]=icscodi
    
    sqlper= "select data_ext from dextraccio"

    for data in getAll(sqlper, nod_db):
        periode_raw=data
    
    periode=str(periode_raw[0])
    periodos='B'+periode[2]+periode[3]+periode[5]+periode[6]    
        
    
    diccionarimestre={}
    master = []
    sqlass="select id_cip_sec, up, uba, edat, sexe, ates, institucionalitzat_ps  from assignada_tot where edat<={}".format(edat)
    for id, up_raw, uba, edat_raw, sexe_raw, ates_raw, insti_raw in getAll(sqlass, nod_db):
        if id in denominador:
            master.append((id, numerador[id] if id in numerador else 0, denominador[id]))
    return master


if __name__ == '__main__':
     
    printTime('inici')
    
    antibiotics=get_farmacs (antibiotic, farma) 
    printTime('antibiotics:',len(antibiotics)) 
    
    for nomindicador in dictind:
        
        print 'Indicador', nomindicador
        indicador=dictind[nomindicador]
        printTime(indicador)
        

        if indicador['idep']:
            idepre=get_idepre(indicador)
            printTime('inmunnodeprimits OK', len(idepre))
        else:   
            idepre=False  
            printTime('inmunnodeprimits OK', idepre)



        
        dos=get_denominador (indicador, ps)
        printTime('problema salut OK', len(dos))
        psevent=dos[0]
        psexclusio=dos[1]
        print 'id events:',len(psevent), 'id exclusions:', len(psexclusio)

        taulaseros=genera_taula (indicador, psevent, psexclusio, idepre, antibiotics)

        numdem=tau_a_dict(taulaseros)
    
        numerador=numdem[0]
        denominador=numdem[1]
    
        printTime('final', len(numerador), len(denominador)) 
    
        master = preparataula(indicador, numerador, denominador, nomindicador)  
        printTime('final', len(master)) 
        db = 'essencial'
        master_tb = 'pac_{}'.format(nomindicador)
        create = "(id int, num int, den int)"
        createTable(master_tb, create, db, rm=True)
        listToTable(master, master_tb, db)

        printTime('fet')

