
/*##########################################################################################################################################

ES14-Tractament inadequat amb benzodiazepines per l'insomni en gent gran
INDICADOR CL�NICO
ENFERMEDAD CR�NICA CADA FILA ES UN PACIENTE

##########################################################################################################################################*/

 
 /*CREAR DENOMINADOR*/

/*Seleccionar pacientes mayores de 65 a�os para empezar a construi el denominador*/

DROP TABLE IF EXISTS edad_insomnio;

CREATE TABLE edad_insomnio
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
from 
nodrizas.assignada_tot
where
edat > 65
;

/*Seleccionar problemas de salud y saber si estan activos*/

set @dataext=(select data_ext from nodrizas.dextraccio);
;

DROP TABLE IF EXISTS ps_insomnio;

CREATE TABLE ps_insomnio
select
id_cip_sec
,pr_cod_ps
,pr_th
,pr_dde
,pr_dba
from
import.problemes
where
pr_cod_ps in ('F51.0','G47.0','C01-G47.0','C01-F51.09','C01-G47.00','C01-F51.0')
and ((pr_dde<@dataext and pr_dba>@dataext) or (pr_dde<@dataext and pr_dba = 0)) 
;

/*Esta tabla intermedia me sirve para cada cip quede con un problema de salud cogiendo el ultimo dx*/
drop table if exists ps_insomnio_ddemax;
create table ps_insomnio_ddemax
select
id_cip_sec
,pr_cod_ps
,pr_th
,max(pr_dde)as pr_dde
from
ps_insomnio
group by
id_cip_sec
;

/*Voy a hacer el denominador. Pero lo que tengo que hacer antes es hacer un indice de la tabla m�s peque�a*/
ALTER TABLE ps_insomnio_ddemax
ADD INDEX (id_cip_sec)
;

/*Vamos a unir las dos tablas para tener el denominador. La intermedia y la de poblaci�n adulta*/

DROP TABLE IF EXISTS den_insomnio;
CREATE TABLE den_insomnio
select 
a.id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,pr_cod_ps
,if(b.id_cip_sec is not null,1,0) as insomnio
,pr_dde
from
edad_insomnio a
left join
ps_insomnio_ddemax b
on
a.id_cip_sec=b.id_cip_sec
;

/*Esta tabla que har� es para hacer una tabla mas peque�ita y asi poder actualizarla y que sea m�s r�pido*/

DROP TABLE IF EXISTS insomnio;

CREATE TABLE insomnio
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
,pr_cod_ps
,pr_dde
,insomnio
from
den_insomnio
where
insomnio=1
;



/*CREAR NUMERADOR*/

/*Crear tabla de prescripci�n de bzdp de vida media larga (los que estan en la recomendaci�n del Essencial)*/

DROP TABLE IF EXISTS bzdp;

CREATE TABLE bzdp
select
id_cip_sec
,ppfmc_atccodi
,ppfmc_pmc_data_ini
,ppfmc_data_fi 
from
import.tractaments
where
ppfmc_atccodi in ('N05BA02','N05BA09','N05CD01','N05CD02','N05BA05','N05BA01','N05BA51','N05BA52','N05BA55','N05BA14','N05BA21','N05BA11') 
and (ppfmc_pmc_data_ini<@dataext and ppfmc_data_fi>@dataext)
;


/*Agregar columna mas en la tabla de depreleve para cada uno de los medicamentos y actualizarlos a 0*/


alter table insomnio
ADD (bzdp DOUBLE);

update insomnio a 
set a.bzdp=0
;

/*Hacer indices de la tabla m�s peque�a con todas las variables que usar�*/

ALTER TABLE bzdp
ADD INDEX (id_cip_sec,ppfmc_atccodi,ppfmc_pmc_data_ini,ppfmc_data_fi)
;


/*Indexo la grande*/

ALTER TABLE insomnio
ADD INDEX (id_cip_sec,up,uba,data_naix,edat,sexe,ates,institucionalitzat)
;

/*Llenar la prescripci�n de los medicamentos...APRENDIZAJE PARA EL FUTURO: USAR WHERE WHERE Y WHERE*/ 
update insomnio a 
inner join bzdp b
set a.bzdp=1
where
a.id_cip_sec=b.id_cip_sec
;


/*Hacer una columna en depreleve de num*/

alter table insomnio
ADD (den DOUBLE);

update insomnio
set den=1;

alter table insomnio
ADD (num DOUBLE);


update insomnio a
set a.num=0;

update insomnio a 
set a.num=1
where bzdp=1;


/*denomi	numera	resul 119892	7634	6,3673973242585*/

/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table insomnio add (
cuentas char(5) default 'ES14');

/*Crear columna de Periodos que es el periodo*/

alter table insomnio add (
periodos varchar(10));


DROP TABLE IF EXISTS any_insomnio;

CREATE TABLE any_insomnio
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;

select*
from any_insomnio;

DROP TABLE IF EXISTS periodos_insomnio1;

CREATE TABLE periodos_insomnio1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_insomnio;

select*
from periodos_insomnio1;

UPDATE insomnio a
inner join periodos_insomnio1 b
SET a.periodos = b.tst;

select*
from insomnio;

/*Crear columna de Entidades que es la UP*/

alter table insomnio add (
entidades char(5));

UPDATE insomnio
SET entidades = up;


/*Crear columna de Tipo*/


alter table insomnio add (
tipo char(10));

DROP TABLE IF EXISTS edad_insomnio;

CREATE TABLE edad_insomnio(
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
insomnio;

ALTER TABLE edad_insomnio
ADD INDEX (id_cip_sec)
;

UPDATE insomnio a
INNER JOIN edad_insomnio b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;

/*CREAR DETALLES ESTA MAS ABAJO*/

/*Crear Controles*/

alter table insomnio add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_insomnio;

CREATE TABLE sexe_insomnio(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
insomnio;


alter table insomnio
add index (id_cip_sec)
;

UPDATE insomnio a
INNER JOIN sexe_insomnio b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table insomnio add (N char(1));

update insomnio
set N='N';

/*DETALLES NOTA: Aqui es importante saber que se hara una tabla para asignados y otra para atesos porque para clasifinsomnioarlos como se quiere no se podria porque cada grupo no es excluyente*/

/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table insomnio add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_insomnio;

CREATE TABLE institu_insomnio(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
insomnio;

select*
from institu_insomnio;


alter table institu_insomnio
add index (id_cip_sec)
;

UPDATE insomnio a
INNER JOIN institu_insomnio b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es14;
create table pac_es14 as select id_cip_sec id, num, den from insomnio;

