# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter
import csv,os,sys
from time import strftime

nod = "nodrizas"

sql='select concat(year(data_ext),month(data_ext)),year(data_ext) from dextraccio'
for d,y in getAll(sql, nod):
    dcalcul=d
    dataAny = y
    
OutFile = tempFolder + '03Refredats' + dcalcul + '.txt'
printTime('Inici procés')

refredats = Counter()
sql = "select dde from eqa_problemes_incid where ps in ('248','249') and year(dde)={}".format(dataAny)
for data, in getAll(sql,nod):
    refredats[data] += 1
        
try:
    remove(OutFile)
except:
    pass     
    
with openCSV(OutFile) as c:
    for (dde),d in refredats.items():
        c.writerow([dde,d])
        
printTime('Fi procés')
        