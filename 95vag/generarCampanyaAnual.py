# coding: iso-8859-1

# Octubre'14
# Objectiu: replicar a redics prstb057_aux2, per� evitant "l'error"
#   de no generar campanya a qui s'hagu�s excl�s en anys anteriors.
# S'ha d'executar contra redics despr�s de crear les taules.
#   sisap_assignada, sisap_u11 i sisap_problemes pels c�lculs de setembre.
# Inicialment li donem dues utilitats:
#   - EQA, que despr�s creuar� amb assignada pr�pia per posar up/sit/edat,
#       seleccionar amb quin ens quedem si est� a diversos sectors, etc.
#   - Diagnosticat, que usar� tal qual.
#       (els pacients poden contar a m�s de una up tot i que no arriben a 2000,
#       ens creurem la up/sit/edat durant tota la campanya, etc.).

import generarCampanyaAnualUtils as camp
import sisapUtils as u

# system('title generar campanya')
table = "sisap_campanya"
table_eapp = "sisap_eapp_campanya"


if __name__ == "__main__":
    for eapp in (False, True):
        campanya = camp.Campanya(eapp)
        campanya.getCodis()
        campanya.getProblemes()
        vag = campanya.getPoblacio()
        tab = table_eapp if eapp else table
        camp.createTable(tab)
        u.uploadOra(vag, tab, "redics")
        u.calcStatistics(tab, "redics")
