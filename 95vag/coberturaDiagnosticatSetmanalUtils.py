from sharedUtils import *

dates= {'inici': '{0}0901'.format(year),'final':'{0}0331'.format(year + 1)}
files= {'poblacio':tempFolder + 'VAGpoblacio.txt','vacunes':tempFolder + 'VAGvacunes.txt'}

def deleteFiles():

    for file in files.values():
        try:
            remove(file)
        except:
            pass
    
def getCampanya():
        
    campanya= {}
    sql= 'select vaf_usua_cip,codi_sector,up,edat from sisap_campanya union select vaf_usua_cip,codi_sector,up,edat from sisap_eapp_campanya'
    for hash,sector,up,edat in getAll(sql,'redics'):
        campanya[hash]= {'sector':sector,'up':up,'edat':edat}
    return campanya

def getPDP(campanya):
        
    sliced= defaultdict(lambda: defaultdict(int))
    sql= 'select usua_cip,usua_cip_cod from pdptb101'
    for cip,hash in getAll(sql,'pdp'):
        try:
            sliced[campanya[hash]['sector']][cip]= campanya[hash]
        except KeyError:
            continue
    return sliced
    
def uploadFTP():

    hst= diagnosticat_['hst']
    usr= diagnosticat_['usr']
    pwd= diagnosticat_['pwd']
    
    ftp = ftplib.FTP(hst)
    ftp.login(usr,pwd)
    ftp.cwd('/diagnosticat')
    for f in files.values():
        old= path.basename(f)
        wd= f.replace(old,'')
        file,extension= path.splitext(old)
        new= '{0}_{1}{2}'.format(file,datetime.date.today(),extension)
        chdir(wd)
        rename(old,new)
        try:
            ftp.delete(new)
        except ftplib.error_perm:
            pass
        ftp.storlines("STOR " + new, open(new, 'r'))
        rename(new,old)

class Diagnosticat:

    n= 0

    def __init__ (self,sector,sliced):
    
        self.sector= sector
        self.campanya= sliced[self.sector]
        Diagnosticat.n+= 1
        print Diagnosticat.n
        
    def getU11(self):
        
        self.u11= {}
        sql= 'select cip_cip_anterior,cip_usuari_cip from usutb011'
        for pre,post in getAll(sql,self.sector):
            self.u11[pre]= post
        
    def getVacunes(self):
    
        self.vacunes= {}
        #sql= "select va_u_usua_cip from prstb051 where va_u_cod in ('GRIP-A','GRIP','GRIP-N') and va_u_data_vac between to_date({inici},'YYYYMMDD') and to_date({final},'YYYYMMDD') and va_u_data_baixa is null".format(**dates)
        sql= "select va_u_usua_cip from prstb051 where va_u_cod in ('GRIP-A','GRIP','GRIP-N','P-G-60','P-G-AD','P-G-NE','P-G-AR') and va_u_data_vac between to_date({inici},'YYYYMMDD') and to_date({final},'YYYYMMDD') and va_u_data_baixa is null".format(**dates)
        for cip, in getAll(sql,self.sector):
            self.vacunes[self.u11[cip]]= True
            
    def getCount(self):

        self.pobCount= Counter()
        self.vacCount= Counter()
        for cip,values in self.campanya.iteritems():
            up,edat= values['up'],values['edat']
            self.pobCount[up,edat]+= 1
            try:
                self.vacunes[self.u11[cip]]
            except KeyError:
                continue
            else:
                self.vacCount[up,edat]+= 1
                
    def exportToFiles(self):
        with open(files['poblacio'],'ab') as f:
            c= csv.writer(f,delimiter='@')
            for k,v in self.pobCount.iteritems():
                k= list(k)
                k.extend([v])
                c.writerow(k)
        with open(files['vacunes'],'ab') as f:
            c= csv.writer(f,delimiter='@')
            for k,v in self.vacCount.iteritems():
                k= list(k)
                k.extend([v])
                c.writerow(k)
