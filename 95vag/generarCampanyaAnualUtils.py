# from sharedUtils import *
from collections import defaultdict
import datetime as d

import sharedUtils as s
import cx_Oracle as cx
import sisapUtils as u

vacuna_codi = 'P-G-AR'
dates = {
    '60y': d.datetime.strptime('{0}1130'.format(s.year - 60), '%Y%m%d').date(),
    '9y': d.datetime.strptime('{0}1130'.format(s.year - 9), '%Y%m%d').date(),
    '6m': d.datetime.strptime('{0}0531'.format(s.year), '%Y%m%d').date(),
    '15y': d.datetime.strptime('{0}1130'.format(s.year - 15), '%Y%m%d').date(),
    'vag': d.datetime.strptime('{0}1001'.format(s.year), '%Y%m%d').date(),
}


def createTable(table):

    try:
        u.execute('drop table {0}'.format(table), 'redics')
    except cx.cx_Oracle.DatabaseError:
        pass
    u.execute("""
            create table {0} (
                vaf_usua_cip varchar2(40),
                codi_sector varchar2(4),
                vaf_cod varchar2(6),
                up varchar2(5),
                edat number(1,0))
            """.format(table), "redics")


class Campanya:

    def __init__(self, eapp):
        self.taula_codis = "sisap_campanya_codis"
        self.taula_problemes = 'sisap_problemes' if not eapp \
            else 'redics51.prstb015r'
        self.taula_u11 = 'sisap_u11' if not eapp else 'sisap_u11_eapp'
        self.taula_assignada = 'sisap_assignada' if not eapp \
            else 'redics51.usutb040r'

    def getCodis(self):

        self.codis = {}
        sql = """
            SELECT fr_cod_ps, nvl(fr_emin, 0), nvl(fr_emax, 2400)
            FROM prstb042
            WHERE
                fr_cod_v = '{}'
                AND fr_data_baixa IS NULL
            """.format(vacuna_codi)
        for ps, emin, emax in u.getAll(sql, '6211'):
            self.codis[ps] = (emin, emax)
        u.createTable(self.taula_codis,
                      "(cod varchar(50), emin int, emax int)",
                      "redics", rm=True)
        u.listToTable([(k,) + v for (k, v) in self.codis.items()],
                      self.taula_codis, "redics")

    def getProblemes(self):

        self.problemes = defaultdict(set)
        sql = """
            SELECT hash_d, codi_sector, pr_cod_ps
            FROM {{}}, {0}, {1}
            WHERE
                pr_cod_ps = cod
                AND pr_cod_u = hash_a and codi_sector = sector
                AND pr_dde < to_date('{2}','YYYY-MM-DD')
                AND nvl(pr_dba, to_date('20991231', 'YYYYMMDD'))
                    >= to_date('{2}','YYYY-MM-DD')
                AND pr_hist=1
            """.format(self.taula_u11, self.taula_codis, dates['vag'])
        if self.taula_problemes == "sisap_problemes":
            jobs = [sql.format(self.taula_problemes
                               + " partition(s{})".format(s))
                    for s in u.sectors]
        else:
            jobs = [sql.format(self.taula_problemes)]
        data = u.multiprocess(worker, jobs)
        # data = [worker(jobs[0])]
        for job in data:
            for id, sec, ps in job:
                self.problemes[(id, sec)].add(ps)

    def getPoblacio(self):

        vag = []
        sql = """
            SELECT usua_cip, codi_sector, usua_data_naixement, usua_uab_up
            FROM {}
            WHERE
                usua_uab_up is not null
                AND usua_uab_codi_uab is not null
                AND usua_situacio='A'
        """
        if self.taula_assignada == "sisap_assignada":
            jobs = [sql.format(self.taula_assignada
                               + " partition(s{})".format(s))
                    for s in u.sectors]
        else:
            jobs = [sql.format(self.taula_assignada)]
        data = u.multiprocess(worker, jobs)
        for job in data:
            for id, sec, naix, up in job:
                edat = None
                naix = naix.date()
                if naix <= dates['60y']:
                    edat = 3
                elif (id, sec) in self.problemes:
                    mesos = u.monthsBetween(naix, dates["vag"])
                    if any([self.codis[ps][0] <= mesos <= self.codis[ps][1]
                            for ps in self.problemes[(id, sec)]]):
                        edat = 2 if naix <= dates['15y'] else 1
                if edat:
                    vag.append([id, sec, vacuna_codi, up, edat])
        return vag


def worker(sql):
    return [row for row in u.getAll(sql, "redics")]
