# coding: utf8

from sisapUtils import getAll, TrelloCard

card = TrelloCard('dbs', 'inbox', 'Taules DBS')

dbscatdesc_tb = 'dbscatdesc'
dbscat_tb = 'dbscat'
cim2bdcap_tb = 'md_ct_cim10_ciap'

SQLdbscatdesc_problemes = """
    SELECT DISTINCT agrupador
    FROM {}
    WHERE
        agrupador LIKE 'PS_%' or
        agrupador LIKE 'EP_%'
    """.format(dbscatdesc_tb)

SQLdbscat_problemes = """
    SELECT codi, agrupador
    FROM {}
    WHERE taula = '{}'
    """.format(dbscat_tb, 'problemes')

SQLcim2bdcap = "SELECT codi_cim10, codi_ciap_m FROM {}".format(cim2bdcap_tb)

dbscatdesc = set()
for agr, in getAll(SQLdbscatdesc_problemes, 'pdp'):
    dbscatdesc.add(agr)

bdcap2dbs = {}
for bdcap, dbs in getAll(SQLdbscat_problemes, 'pdp'):
    try:
        bdcap2dbs[dbs].append(bdcap)
    except KeyError:
        bdcap2dbs[dbs] = [bdcap]

cim2bdcap = {}
for cim, bdcap in getAll(SQLcim2bdcap, 'redics'):
    try:
        cim2bdcap[bdcap].append(cim)
    except KeyError:
        cim2bdcap[bdcap] = [cim]

algo = False
error_dbsNoDeclarat = []
for dbs in bdcap2dbs.keys():
    if dbs not in dbscatdesc:
        error_dbsNoDeclarat.append(dbs)
if len(error_dbsNoDeclarat) > 0:
    algo = True
    for error in error_dbsNoDeclarat:
        _text = "ERROR: agrupador dbs (dbscat) no declarat (a dbscatdesc)"
        card.add_to_checklist(_text, error)
    if algo:
        card.add_members('ramospaqui1')
        card.add_comment('inconsistencia dbscat <-> dbscatdesc')

algo = False
error_bdcapNoDeclarat = []
for k, v in bdcap2dbs.items():
    for bdcap in v:
        try:
            cim2bdcap[bdcap]
        except KeyError:
            error_bdcapNoDeclarat.append(bdcap)
if len(error_bdcapNoDeclarat) > 0:
    algo = True
    for error in error_bdcapNoDeclarat:
        _text = "ERROR: agrupador ciap (dbscat) no declarat (md_ct_cim10_ciap)"
        card.add_to_checklist(_text, error)
    if algo:
        card.add_members('ramospaqui1')
        card.add_comment('inconsistencia dbscat <-> md_ct_cim10_ciap')
