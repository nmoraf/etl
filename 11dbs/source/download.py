from sisapUtils import getRedisConnection, getAll, getTableCount, getSubTables, getOne, multiprocess, listToTable
import parameters as p
from collections import defaultdict
from json import loads
from time import sleep
import queue as q
import multiprocessing as m
import sys


download_sql = {
    'problemes': "select id_cip_sec, pr_cod_ps, date_format(pr_dde, '%Y%m%d'), if(pr_dba = 0 or pr_dba > data_ext, 0, 1) from {table}, nodrizas.dextraccio \
                 where pr_cod_ps in ({codes}) and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext",
    'tractaments': "select id_cip_sec, ppfmc_atccodi from {table}, nodrizas.dextraccio where ppfmc_atccodi in ({codes}) and ppfmc_pmc_data_ini <= data_ext and ppfmc_data_fi > data_ext",
    'variables': "select id_cip_sec, vu_cod_vs, date_format(vu_dat_act, '%Y%m%d'), vu_val from {table}, nodrizas.dextraccio where vu_cod_vs in ({codes}) and vu_dat_act <= data_ext",
    'laboratori': "select id_cip_sec, cr_codi_prova_ics, date_format(cr_data_reg, '%Y%m%d'), \
                   trim(replace(replace(replace(replace(replace(replace(cr_res_lab, ',', '.'), '>', ''), '<', ''), '+', ''), CHAR(9), ''), 'mL/min/1.73m2', '')), \
                   cr_unitats_lab from {table}, nodrizas.dextraccio where cr_codi_prova_ics in ({codes}) and cr_data_reg <= data_ext",
    'serologies': "select id_cip_sec, cod, dat, val from {table}, nodrizas.dextraccio where dat <= data_ext",
    # 'activitats': "select id_cip_sec, au_cod_ac, date_format(au_dat_act, '%Y%m%d'), au_val * 1 from {table}, nodrizas.dextraccio \
    #                where au_cod_ac in ({codes}) and au_dat_act <= data_ext and concat('', au_val * 1) = au_val",
    'vacunes': "select id_cip_sec, va_u_cod, date_format(va_u_data_vac, '%Y%m%d'), va_u_dosi from {table}, nodrizas.dextraccio \
                where va_u_cod in ({codes}) and va_u_data_vac <= data_ext and va_u_data_baixa = 0",
    'nen12': "select id_cip_sec, val_var, date_format(val_data, '%Y%m%d'), val_val from {table}, nodrizas.dextraccio where val_var in ({codes}) and val_data <= data_ext",
    'nen11': "select id_cip_sec, val_var, date_format(val_data, '%Y%m%d'), val_val from {table}, nodrizas.dextraccio where val_var in ({codes}) and val_data <= data_ext",
    'odn505': "select id_cip_sec, 'CAO', cfd_ind_cod from {table} union select id_cip_sec, 'CAOD', cfd_ind_caod from {table} union select id_cip_sec, 'IRD', cfd_ind_rest from {table} union select id_cip_sec, 'IRT', cfd_ind_rest_tmp from {table}",
    'odn506': "select id_cip_sec, concat(if(dfd_estat = 'C', 'CA', 'OBT'), '_', if(dfd_cod_p < 50, 'DEF', 'TEMP')), dfd_cod_p from {table} where dfd_p_a = 'P' and dfd_estat in ('C', 'O')",
    'odn507': "select id_cip_sec, concat(dpd_pat, '_', if(dpd_cod_p < 50, 'DEF', 'TEMP')), dpd_cod_p from {table}, nodrizas.dextraccio where dpd_data <= data_ext",
    'odn508': "select id_cip_sec, concat(dtd_tra, '_', if(dtd_cod_p < 50, 'DEF', 'TEMP')), dtd_cod_p from {table}, nodrizas.dextraccio where dtd_data <= data_ext and dtd_et in ('E','T')",
    'odn509': "select id_cip_sec, cb_car, round(if(cb_val=1501,1,cb_val), 0) from {table} where cb_data > 0 and cb_data_fi = 0 and cb_val <> 1502",
    'odn510': "select id_cip_sec, 'REV', date_format(ro_data, '%Y%m%d') from {table}, nodrizas.dextraccio where ro_data <= data_ext",
    'odn511': "select id_cip_sec, tb_trac, date_format(tb_data, '%Y%m%d') from {table}, nodrizas.dextraccio where tb_data > 0 and tb_data <= data_ext",
}


class Download(object):

    def __init__(self):
        self.get_cataleg()
        self.nens = set([int(id) for id in getRedisConnection(p.redis_host, pipe=False).smembers('dbs_child')])
        self.sexe = {int(id): sex for (id, sex) in loads(getRedisConnection(p.redis_host, pipe=False).get('dbs_sex')).items()}
        self.unitats = {(agr, uni): (conv, int(dec)) for (agr, uni, conv, dec) in getAll('select agrupador, unitat, conversio, decimals from cat_dbs_cataleg_unitats', p.imp_db)}
        self.txt_to_val = {(taula, variable, text): valor for (taula, variable, text, valor) in getAll('select taula, var, txt, val from cat_dbs_txt_to_val', p.imp_db)}
        self.get_cataleg()
        self.agr_tipus = {agr: 0 if adults and not (nens + odn) else 1 if nens and not (adults + odn) else 2 for (agr, adults, nens, odn)
                          in getAll("select agrupador, adults, nens, odn from cat_dbscatdesc where agrupador not like 'C_%' and agrupador not like 'PR_%'", p.imp_db)}
        self.exceptions = m.Manager().list()
        self.queue = m.JoinableQueue()
        for job in self.get_jobs():
            self.queue.put(job)
        for _i in range(p.procs):
            proc = m.Process(target=self.worker)
            proc.daemon = True
            proc.start()
        self.queue.join()
        if self.exceptions:
            excepcio = "errors a queue ({})".format(", ".join(set(self.exceptions)))
            raise BaseException(excepcio)

    def get_cataleg(self):
        ciap_to_cim = {}
        for ciap, sexe, cim in getAll('select codi_ciap_m, sexe, codi_cim10 from cat_md_ct_cim10_ciap', p.imp_db):
            if ciap not in ciap_to_cim:
                ciap_to_cim[ciap] = []
            if sexe == '':
                ciap_to_cim[ciap].extend([(cim, sex) for sex in ('H', 'D')])
            else:
                ciap_to_cim[ciap].append((cim, sexe))
        an_to_vac = {}
        for vac, an in getAll('select vacuna, antigen from cat_prstb040_new', p.imp_db):
            if an not in an_to_vac:
                an_to_vac[an] = []
            an_to_vac[an].append(vac)
        self.cataleg = {}
        sql = """
            select taula, codi, agrupador, ps_tancat from cat_dbscat c
            WHERE EXISTS (
                select 1
                from import.cat_dbscatdesc cd
                where c.agrupador = cd.agrupador)
            """
        for taula, codi, agrupador, tancat in getAll(sql, p.imp_db):
            if taula not in self.cataleg:
                self.cataleg[taula] = defaultdict(list)
            if taula == 'problemes':
                try:
                    ciap_to_cim[codi]
                except KeyError:
                    continue
                else:
                    keys = [(cim, sexe, tancat) for (cim, sexe) in ciap_to_cim[codi]]
            elif taula == 'vacunes':
                keys = an_to_vac[codi]
            else:
                keys = [codi]
            for key in keys:
                self.cataleg[taula][key].append(agrupador)

    def get_jobs(self):
        jobs = []
        for domini, _sql in download_sql.items():
            if domini == 'serologies':
                jobs.append((domini, 'nod_serologies', p.nod_db, getTableCount('nod_serologies', p.nod_db)))
            else:
                if getSubTables(domini):
                    jobs.extend([(domini, table, p.imp_db, n) for table, n in getSubTables(domini).items() if n > 0])
                    if getOne("select 1 from tables where table_schema = '{}' and table_name = '{}'".format(p.jail_db, domini), 'information_schema'):
                        jobs.append((domini, domini, p.jail_db, getTableCount(domini, p.jail_db)))
                else:
                    jobs.append((domini, domini, p.imp_db, getTableCount(domini, p.imp_db) * 8 if domini == 'odn505' else getTableCount(domini, p.imp_db)))
        return sorted(jobs, key=lambda x: x[3], reverse=True)

    def include_row(self, id, agr):
        is_nen = id in self.nens
        tipus = self.agr_tipus[agr]
        return tipus == 2 or is_nen == tipus

    def worker(self):
        while self.queue.qsize() > 0:
            try:
                domini, table, db, n = self.queue.get()
                self.download_it(domini, table, db, n)
            except Exception as e:
                self.exceptions.append(" -> ".join((domini, str(e))))
            finally:
                self.queue.task_done()
                    
    def download_it(self, domini, table, db, n):
            cataleg = self.cataleg[domini]
            codes = set([cim for (cim, _sexe, tancat) in cataleg]) if domini == 'problemes' else set(cataleg)
            codes_sql = ', '.join(map(repr, codes))
            data = {partition: [] for partition in range(p.partitions)}
            data = []
            sql = download_sql[domini].format(table=table, codes=codes_sql)
            for row in getAll(sql, db):
                id, codi = row[:2]
                if id in self.sexe:
                    if domini == 'problemes':
                        dat, tancat = row[2:]
                        key = (codi, self.sexe[id], tancat)
                        info = [(id, agr, dat, None) for agr in cataleg[key] if self.include_row(id, agr)]
                    elif domini == 'tractaments':
                        info = [(id, agr, codi, None) for agr in cataleg[codi] if self.include_row(id, agr)]
                    elif domini in ('variables', 'serologies', 'activitats', 'vacunes'):
                        dat, val = row[2:]
                        info = [(id, agr, dat, val) for agr in cataleg[codi] if self.include_row(id, agr)]
                    elif domini == 'laboratori':
                        dat, val, uni = row[2:]
                        try:
                            val = float(val)
                        except ValueError:
                            continue
                        info = []
                        for agr in cataleg[codi]:
                            if self.include_row(id, agr):
                                if (agr, uni) in self.unitats:
                                    conversio, decimals = self.unitats[agr, uni]
                                    val = round(val * conversio, decimals)
                                info.append((id, agr, dat, val))
                    elif domini in ('nen12', 'nen11'):
                        dat, val = row[2:]
                        try:
                            val = float(val)
                        except ValueError:
                            try:
                                val = self.txt_to_val[(domini, codi, val)]
                            except KeyError:
                                continue
                        info = [(id, agr, dat, val) for agr in cataleg[codi] if self.include_row(id, agr)]
                    elif domini in ('odn505', 'odn506', 'odn507', 'odn508', 'odn509'):
                        val = row[2]
                        info = [(id, agr, None, val) for agr in cataleg[codi]]
                    elif domini in ('odn510', 'odn511'):
                        dat = row[2]
                        info = [(id, agr, dat, None) for agr in cataleg[codi]]
                    # partition = abs(id) % partitions
                    # data[partition].extend(info)
                    data.extend(info)
            # for partition in data:
                # listToTable(data[partition], my_tb, my_db, partition='p{}'.format(partition))
            listToTable(data, p.my_tb, p.my_db)


if __name__ == '__main__':
    p.clean_my()
    download = Download()
