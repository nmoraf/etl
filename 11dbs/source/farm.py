from sisapUtils import sectors, getAll, listToTable, getRedisConnection, multiprocess, backupTable, execute
from parameters import *
from json import loads


def get_farm(sector):
    poblacio = loads(getRedisConnection(redis_host).get('dbs_farm:{}'.format(sector)))
    poblacio = {int(key): info for key, info in poblacio.items()}
    if sector == '6951':
        table = 'tractaments'
        db = jail_db
    else:
        table = 'tractaments_s{}'.format(sector)
        db = imp_db
    data = []
    sql = "select id_cip_sec, ppfmc_pmc_amb_cod_up, ppfmc_pf_codi, ppfmc_atccodi, date_format(ppfmc_pmc_data_ini, '%Y%m%d'), date_format(ppfmc_data_fi,'%Y%m%d'), \
           ppfmc_pmc_amb_num_col, ppfmc_posologia, ppfmc_freq, ppfmc_durada from {}, nodrizas.dextraccio \
           where ppfmc_pmc_data_ini <= data_ext and ppfmc_data_fi > data_ext".format(table)
    for row in getAll(sql, db):
        id, up = row[:2]
        if id in poblacio:
            data.append(poblacio[id][:4] + [sector, up, poblacio[id][4]] + list(row[2:]))
    listToTable(data, dbs_farm_tb, dbs_farm_db, format_date='YYYYMMDD')


if __name__ == '__main__':
    backupTable(dbs_farm_tb, dbs_farm_db)
    execute('truncate table {}'.format(dbs_farm_tb), dbs_farm_db)
    multiprocess(get_farm, sectors + ['6951'], procs, close=True)
