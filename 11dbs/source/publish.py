# coding: latin1
import sisapUtils as u
import parameters as p
from sys import exit
import datetime as d


force = False


def is_good():
    prd, dev, good, whats_wrong = {}, {}, {}, []
    label = ['n', 'ps', 'far', 'n lab', 'val lab', 'n var', 'val lab', 'vac',
             'nen']
    sql = """
        SELECT
            c_sector,
            count(1),
            sum(decode(ps_dislipemia_data, null, 0, 1)),
            sum(decode(f_hipolipemiants, null, 0, 1)),
            sum(decode(v_col_ldl_valor, null, 0, 1)),
            avg(v_col_ldl_valor),
            sum(decode(v_tas_valor, null, 0, 1)),
            avg(v_tas_valor),
            decode(c_sector, '6951', 9, sum(decode(i_hib_data, null, 0, 1))),
            sum(decode(v_ppes_data, null, 0, 1))
        FROM {}
        GROUP BY c_sector
        """
    for row in u.getAll(sql.format(p.dbs_tb_prd), p.dbs_db):
        prd[row[0]] = row[1:]
    for row in u.getAll(sql.format(p.dbs_tb_dev), p.dbs_db):
        dev[row[0]] = row[1:]
    for sector, valors in prd.items():
        if sector not in dev:
            good[sector] = False
            whats_wrong.append([sector, "no existeix"])
        else:
            good[sector] = all([prd[sector][i] * 0.95 <= dev[sector][i] <= prd[sector][i] * 1.1 for i in range(len(valors))])  # noqa
            whats_wrong.append([(sector, label[i], prd[sector][i], dev[sector][i]) for i in range(len(valors)) if not prd[sector][i] * 0.95 <= dev[sector][i] <= prd[sector][i] * 1.1])  # noqa
    return (all(good.values()), whats_wrong)


def dev_to_prd():
    # u.backupTable(dbs_tb_prd, dbs_db)
    proc = """
        BEGIN
            :out := trunca_dbs;
        END;
    """
    u.executeProc(proc, p.dbs_db)
    excluded = ("C_CIP", "C_SECTOR", "C_SAP", "C_UP", "C_UP_ORIGEN",
                "C_UP_ABS", "C_CENTRE", "C_METGE", "C_INFERMERA")
    for column in [column for column
                   in u.getTableColumns(p.dbs_tb_prd, p.dbs_db)
                   if column not in excluded]:
        u.execute('alter table redics.{} drop column {}'.format(p.dbs_tb_prd,
                                                                column),
                  p.dbs_db)
    for column in [u.getColumnInformation(column,
                                          p.dbs_tb_dev,
                                          p.dbs_db,
                                          desti='ora')['create']
                   for column in u.getTableColumns(p.dbs_tb_dev, p.dbs_db)
                   if column not in excluded]:
        u.execute('alter table redics.{} add ({})'.format(p.dbs_tb_prd,
                                                          column),
                  p.dbs_db)
        try:
            u.execute('alter table {} add ({})'.format(p.dbs_tb_frm, column),
                      p.dbs_farm_db)
        except Exception:
            pass
    u.execute('insert into {} select * from {}'.format(p.dbs_tb_prd,
                                                       p.dbs_tb_dev),
              p.dbs_db)
    u.execute('truncate table {}'.format(p.dbs_tb_dev), p.dbs_db)
    create_views()
    dat, = u.getOne("""
                    SELECT date_format(data_ext, '%d %b')
                    FROM dextraccio
                    """, p.nod_db)
    u.sendSISAP(['amercadecosta@gencat.cat',
                 'jcamus@gencat.cat',
                 'framospe@gencat.cat',
                 'lmendezboo@gencat.cat'],
                'C�rrega DBS',
                'all',
                """
                S\'ha carregat la taula DBS a REDICS
                amb dades corresponents a {0}.
                """.format(dat))


def create_views():
    excluded = {'adults': ['C_UP_ORIGEN', 'C_EDAT_MESOS',
                           'C_ULTIMA_VISITA_ODN', 'C_RNSA_DATA',
                           'C_RNSA_MESOS'],
                'nens': ['C_ULTIMA_VISITA_ODN'],
                'odn': ['C_EDAT_MESOS', 'C_RNSA_DATA', 'C_RNSA_MESOS']}
    prefix = {'problemes': 'P', 'variables': 'V', 'farmacs': 'F'}
    columns = u.getTableColumns(p.dbs_tb_prd, p.dbs_db)
    for categoria in ('adults', 'nens', 'odn'):
        include = [agr for agr, in u.getAll("""
                                            SELECT agrupador
                                            FROM cat_dbscatdesc
                                            WHERE
                                                {} = 1 and
                                                agrupador not like 'C_%'
                                            """.format(categoria), p.imp_db)]
        columns_categoria = [column for column in columns
                             if (column[0] == 'C'
                                 and column not in excluded[categoria]) or
                             any([col_in for col_in in include
                                  if col_in in column])]
        sql = """
            CREATE OR REPLACE view dbs_{} AS
            SELECT {} FROM {}
            """.format(categoria, ', '.join(columns_categoria), p.dbs_tb_prd)
        if categoria in ('adults', 'nens'):
            cond = " WHERE c_edat_anys {} 15"
            cond = cond.format('>=' if categoria == 'adults' else '<')
            sql += cond
        u.execute(sql, p.dbs_farm_db)

    categoria = 'adults'
    include = [agr for agr, in u.getAll("""
                                        SELECT agrupador
                                        FROM cat_dbscatdesc
                                        WHERE
                                            {} = 1 AND
                                            agrupador NOT LIKE 'C_%'
                                        """.format(categoria), 'import')]
    columns_categoria = [column for column in columns
                         if (column[0] == 'C'
                             and column not in excluded[categoria])
                         or [col_in for col_in in include
                             if (col_in in column
                                 and ((column[0] + column[1]) != 'PS')
                                 and (column[0] != 'V')
                                 and ((column[0] + column[1]) != 'F_'))
                             ]
                         ]
    tipus = 'general'
    nomview = categoria + '_' + tipus

    sql = """
        CREATE OR REPLACE view dbs_{} AS SELECT {} FROM {}
        """.format(nomview, ', '.join(columns_categoria), p.dbs_tb_prd)

    if categoria in ('adults', 'nens'):
        cond = """ where c_edat_anys {} 15"""
        cond = cond.format('>=' if categoria == 'adults' else '<')
        sql += cond
    u.execute(sql, p.dbs_farm_db)

    for tipus in ('problemes', 'variables', 'farmacs'):
        nomview = categoria + '_' + tipus
        columns_categoria = [column for column in columns
                             if (column[0] == 'C'
                                 and column not in excluded[categoria])
                             or [col_in for col_in in include
                                 if (col_in in column
                                     and (column[0] + column[1]) == 'PR')
                                 or [col_in for col_in in include
                                     if (col_in in column
                                         and (column[0] == prefix[tipus]))
                                     and ((column[0] + column[1]) != 'PR')]]]
        sql = """
            CREATE OR REPLACE VIEW dbs_{} AS SELECT {} FROM {}
            """.format(nomview, ', '.join(columns_categoria), p.dbs_tb_prd)
        if categoria in ('adults', 'nens'):
            cond = """ where c_edat_anys {} 15"""
            cond = cond.format('>=' if categoria == 'adults' else '<')
            sql += cond
        u.execute(sql, p.dbs_farm_db)


def createTweet(estatus):
    dext, = u.getOne('select data_ext from dextraccio', 'nodrizas')
    tuitGooden = "A partir de dilluns tindreu el #DBS i #DBSForm actualitzats amb dades fins al " + str(dext)  # noqa
    tuitGood = tuitGooden.decode('latin1')
    tuitNotGood = "Per motius t�cnics aquesta setmana no s'ha pogut actualitzar el #DBS ni el DBSForm #sisap".decode('latin1')  # noqa
    ara = d.datetime.today()
    diaSet = d.datetime.today().weekday()
    avui = d.date.today()
    string_date = str(avui) + " 18:00:00.0"
    deadline = d.datetime.strptime(string_date, "%Y-%m-%d %H:%M:%S.%f")
    if estatus == 'Good':
        if ara < deadline and diaSet == 6:
            u.newTweet(tuitGood)
        elif diaSet == 5:
            u.newTweet(tuitGood)
        else:
            pass
    if estatus == 'Not Good':
        u.newTweet(tuitNotGood)


def changeDate(estatus):
    query = "select data_ext from nodrizas.dextraccio"
    table = "DBSData"
    if estatus == 'Good':
        u.exportPDP(query=query, table=table, truncate=True)


if __name__ == '__main__':
    all_good, whats_wrong = is_good()
    if force or all_good:
        dev_to_prd()
        p.clean_redis()
        # p.clean_my()
        createTweet('Good')
        changeDate('Good')
    else:
        # createTweet('Not Good')
        exit(whats_wrong)
