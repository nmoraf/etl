import sisaptools as u
import sisapUtils as u2


table = "dbs"
dext, = u.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")  # noqa
anual = "dbs_{}".format(dext.year)


def worker(letter):
    sql = "select * from {} where c_cip like '{}%'".format(table, letter)
    dades = list(u.Database("redics", "data").get_all(sql))
    with u.Database("sidics", "dbs") as sidics:
        sidics.execute("set session wait_timeout = 28800")
        sidics.list_to_table(dades, anual)


if __name__ == "__main__":
    if u2.IS_MENSUAL and dext.month == 12:
        with u.Database("redics", "data") as redics:
            columns = [redics.get_column_information(column, table)["create"]
                       for column in redics.get_table_columns(table)]
        u.Database("sidics", "dbs").create_table(anual, columns, remove=True)
        jobs = []
        digits = [format(digit, 'x').upper() for digit in range(16)]
        for digit1 in digits:
            for digit2 in digits:
                jobs.append(digit1 + digit2)
        u2.multiprocess(worker, jobs, 32)
