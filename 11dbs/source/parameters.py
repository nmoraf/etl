from sisapUtils import getRedisConnection, createDatabase, createTable
import copy_reg
import types


imp_db = 'import'
nod_db = 'nodrizas'
jail_db = 'import_jail'
partitions = 2 ** 5
procs = 2 ** 3
redis_host = '10.80.217.68'
my_db = ('sisap', 'sisap_3308')
my_tb = 'dbs'
dbs_db = 'redics'
dbs_tb_prd = 'dbs'
dbs_tb_dev = 'dbs_dev'
dbs_tb_frm = 'dbsform'
dbs_farm_db = 'jch'
dbs_farm_tb = 'dbsfarm'


def clean_redis():
    r = getRedisConnection(redis_host)
    keys = r.keys('dbs*')
    for key in keys:
        r.delete(key)


def clean_my():
    createDatabase(my_db)
    createTable(my_tb, '(id_cip_sec int, agr varchar(25), attr_txt varchar(8), attr_num double) partition by hash(id_cip_sec) partitions {}'.format(partitions), my_db, rm=True)


def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)


def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)


copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)
