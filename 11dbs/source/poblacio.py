from sisapUtils import getAll, getSubTables, multiprocess, getOne, monthsBetween, getRedisConnection, sectors
from parameters import *
from json import dumps
from datetime import datetime


download_sql = "select id_cip_sec, codi_sector, up, upOrigen, upABS, concat(centre_codi, '-', centre_classe), uba, ubainf, data_naix, date_format(data_naix, '%Y%m%d'), edat, sexe, \
                if(last_visit = 0, '', date_format(last_visit, '%Y%m%d')), n_year, if(last_odn = 0, '', date_format(last_odn, '%Y%m%d')), up_residencia, nacionalitat \
                from assignada_tot_with_jail, dextraccio where data_naix <= data_ext and edat < 125 and sexe in ('H', 'D')"
create_sql = [('C_CIP', 'varchar2(40)'), ('C_SECTOR', 'varchar2(4)'), ('C_SAP', 'varchar2(2)'), ('C_UP', 'varchar2(5)'), ('C_UP_ORIGEN', 'varchar2(5)'), ('C_UP_ABS', 'varchar2(5)'),
              ('C_CENTRE', 'varchar2(12)'), ('C_METGE', 'varchar2(5)'), ('C_INFERMERA', 'varchar2(5)'), ('C_PROVEIDOR', 'varchar2(4)'), ('C_DATA_NAIX', 'date'), ('C_EDAT_ANYS', 'number(3, 0)'),
              ('C_GRUP_EDAT', 'number(2, 0)'), ('C_EDAT_MESOS', 'number(4, 0)'), ('C_SEXE', 'varchar2(1)'), ('C_GMA_CODI', 'varchar2(3)'), ('C_GMA_COMPLEXITAT', 'number(6, 3)'),
              ('C_GMA_N_CRONIQUES', 'number(3, 0)'), ('C_ULTIMA_VISITA_EAP', 'date'), ('C_VISITES_ANY', 'number(5, 0)'), ('C_ULTIMA_VISITA_ODN', 'date'), ('C_INSTITUCIONALITZAT', 'varchar2(13)'),
              ('C_NACIONALITAT', 'varchar2(100)'), ('PR_MACA_DATA', 'date'), ('PR_PCC_DATA', 'date'), ('C_RNSA_DATA', 'date'), ('C_RNSA_MESOS', 'number(6, 0)')]


class Poblacio(object):

    def __init__(self):
        self.get_revisio()
        self.hash = {id: hash for (id, hash) in getAll('select id_cip_sec, hash_d from eqa_u11_with_jail', nod_db)}
        self.up = {up: (ep, sap.decode('latin1')) for (up, ep, sap) in getAll('select scs_codi, ep, sap_codi from cat_centres_with_jail', nod_db)}
        self.gma = {(id, up): (cod, cmplx, num) for (id, up, cod, cmplx, num) in getAll('select id_cip_sec, gma_up, gma_cod, gma_ind_cmplx, gma_num_cron from gma', imp_db)}
        self.nacionalitat = {cod: des.decode('latin1') for (cod, des) in getAll('select resi_codi, resi_descripcio from cat_rittb034', imp_db)}
        self.pcc = {id: dat for (id, dat) in getAll("select id_cip_sec, date_format(es_dde, '%Y%m%d') from estats, nodrizas.dextraccio where es_cod = 'ER0001' and es_dde <= data_ext", imp_db)}
        self.maca = {id: dat for (id, dat) in getAll("select id_cip_sec, date_format(es_dde, '%Y%m%d') from estats, nodrizas.dextraccio where es_cod = 'ER0002' and es_dde <= data_ext", imp_db)}
        self.get_poblacio()
        self.upload()

    def get_revisio(self):
        self.revisio = {}
        jobs = [table for (table, n) in getSubTables('variables').items() if n < 20 * 10**6]
        mega_resul = multiprocess(self.get_revisio_aux, jobs, procs, close=True)
        for resul in mega_resul:
            for id, dat in resul:
                if id not in self.revisio:
                    self.revisio[id] = dat
                elif dat > self.revisio[id]:
                    self.revisio[id] = dat

    def get_revisio_aux(self, table):
        return [row for row in getAll("select id_cip_sec, date_format(vu_dat_act, '%Y%m%d') from {} where vu_cod_vs = 'RNSA'".format(table), imp_db)]

    def get_poblacio(self):
        dext, = getOne('select data_ext from dextraccio', nod_db)
        self.poblacio = {partition: {} for partition in range(partitions)}
        self.sexe = {}
        self.nens = set()
        self.farm = {sector: {} for sector in sectors + ['6951']}
        for id, sector, up, up_o, up_a, centre, uba, ubainf, naix, naix_str, edat, sexe, visita, n_year, visita_odn, residencia, nacionalitat in getAll(download_sql, nod_db):
            is_nen = edat < 15
            centre, uba, ubainf = centre.decode('latin1'), uba.decode('latin1'), ubainf.decode('latin1')
            hash = self.hash[id]
            ep, sap = self.up[up]
            edat_g = str(edat / 5 if edat < 100 else 20)
            gma_cod, gma_cmplx, gma_num = self.gma[(id, up)] if (id, up) in self.gma else (None, None, None)
            nac = self.nacionalitat[nacionalitat] if nacionalitat in self.nacionalitat else ''
            pcc = self.pcc[id] if id in self.pcc else ''
            maca = self.maca[id] if id in self.maca else ''
            if is_nen:
                self.nens.add(id)
                edat_m = monthsBetween(naix, dext)
                revisio_d = self.revisio[id] if id in self.revisio else None
                revisio_m = monthsBetween(naix, datetime.strptime(revisio_d, '%Y%m%d')) if revisio_d else None
            else:
                edat_m, revisio_d, revisio_m = None, None, None
            self.poblacio[abs(id) % partitions][id] = [hash, sector, sap, up, up_o, up_a, centre, uba, ubainf, ep, naix_str, edat, edat_g, edat_m, sexe, gma_cod, gma_cmplx, gma_num,
                                                       visita, n_year, visita_odn, residencia, nac, maca, pcc, revisio_d, revisio_m]
            self.sexe[id] = sexe
            self.farm[sector][id] = [sector, up, sap, ep, hash]

    def upload(self):
        r = getRedisConnection(redis_host)
        r.set('dbs_sex', dumps(self.sexe))
        r.sadd('dbs_child', *self.nens)
        for partition in self.poblacio:
            r.set('dbs_pob:{}'.format(partition), dumps(self.poblacio[partition]))
        r.set('dbs_create', dumps(create_sql))
        for sector in self.farm:
            r.set('dbs_farm:{}'.format(sector), dumps(self.farm[sector]))


if __name__ == '__main__':
    clean_redis()
    poblacio = Poblacio()
