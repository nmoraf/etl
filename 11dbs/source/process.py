# coding: latin1
from sisapUtils import getRedisConnection, getAll, multiprocess, createTable, listToTable
from parameters import *
from json import loads


class Process(object):

    def __init__(self):
        self.get_agrupadors()
        self.get_structure()
        self.create_structure()
        self.atc_desc = {cod: des for (cod, des) in getAll('select atc_codi, atc_desc from cat_cpftb010_def', imp_db)}
        multiprocess(self.process_it, range(partitions), procs, close=True)

    def get_agrupadors(self):
        self.agrupadors = {}
        for taula, agrupador in getAll('select taula, agrupador from cat_dbscat', imp_db):
            if taula not in self.agrupadors:
                self.agrupadors[taula] = set()
            self.agrupadors[taula].add(agrupador)
        self.num_to_txt = {(agr, cod): des for (agr, cod, des) in getAll("select a.agrupador, b.vi_min, b.vi_des from cat_dbscat a \
                                                                          inner join cat_prstb106 b on a.codi = b.vi_cod \
                                                                          where a.agrupador like 'VC%' and b.codi_sector = '6102'", imp_db)}  # compte si m�ltiples variables per un agrupador...

    def get_structure(self):
        self.structure = loads(getRedisConnection(redis_host).get('dbs_create'))
        inici = len(self.structure)
        self.structure += [('{}_DATA'.format(agr), 'date') for agr in sorted(list(self.agrupadors['problemes'])) if agr[:2] == 'PS']
        self.structure += [('{}_EPISODIS'.format(agr), 'number(5, 0)') for agr in sorted(list(self.agrupadors['problemes'])) if agr[:2] == 'EP']
        self.structure += [(agr, 'varchar2(500)') for agr in sorted(list(self.agrupadors['tractaments']))]
        temp = set()
        for taula in ('laboratori', 'variables', 'serologies', 'activitats', 'vacunes', 'nen12', 'nen11'):
            if taula in self.agrupadors:
                temp |= self.agrupadors[taula]
        for agr in sorted(list(set(temp))):
            if agr[:2] == 'AL':
                self.structure.append((agr, 'number(6, 0)'))
            else:
                self.structure.append(('{}_DATA'.format(agr), 'date'))
                self.structure.append(('{}_VALOR'.format(agr), 'varchar2(255)' if agr[:3] == 'VC_' else 'number(17, 2)'))
        temp = set()
        for taula in self.agrupadors:
            if taula[:3] == 'odn':
                temp |= self.agrupadors[taula]
        for agr in sorted(list(set(temp))):
            if agr[:2] == 'OE':
                self.structure.append((agr, 'varchar2(255)'))
            elif agr[:2] in ('OR', 'OT'):
                self.structure.append((agr, 'date'))
            elif agr[:5] == 'OC_IR':
                self.structure.append((agr, 'number(5, 2)'))
            else:
                self.structure.append((agr, 'number(4, 0)'))
        self.position = {field: i for i, (field, format) in enumerate(self.structure)}
        self.diff = len(self.structure) - inici

    def create_structure(self):
        sql = ','.join(['{} {}'.format(field, format) for field, format in self.structure])
        createTable(dbs_tb_dev, '({})'.format(sql), dbs_db, rm=True)

    def process_it(self, partition):
        data = {}
        sql = 'select id_cip_sec, agr, attr_txt, attr_num from {} partition (p{})'.format(my_tb, partition)
        for id, agr, txt, num in getAll(sql, my_db):
            domini = agr.split('_')[0]
            if domini == 'PS':
                key = (id, '{}_DATA'.format(agr))
                if key not in data:
                    data[key] = txt
                elif txt < data[key]:
                    data[key] = txt
            elif domini == 'F':
                key = (id, agr)
                if key not in data:
                    data[key] = set()
                data[key].add(txt)
            elif domini in ('V', 'I'):
                key_d = (id, '{}_DATA'.format(agr))
                key_v = (id, '{}_VALOR'.format(agr))
                if key_d not in data:
                    data[key_d] = txt
                    data[key_v] = num
                elif txt > data[key_d]:
                    data[key_d] = txt
                    data[key_v] = num
                elif txt == data[key_d] and "SEROLOGIA" in agr and num < data[key_v]:
                    data[key_v] = num
            elif domini == 'VC':
                key_d = (id, '{}_DATA'.format(agr))
                key_v = (id, '{}_VALOR'.format(agr))
                num_des = self.num_to_txt.get((agr, int(num)))
                if num_des:
                    if key_d not in data:
                        data[key_d] = txt
                        data[key_v] = num_des
                    elif txt > data[key_d]:
                        data[key_d] = txt
                        data[key_v] = num_des
            elif domini == 'EP':
                key = (id, '{}_EPISODIS'.format(agr))
                if key not in data:
                    data[key] = set()
                data[key].add(txt)
            elif domini == 'AL':
                key = (id, agr)
                first = '_INI' in agr
                if key not in data:
                    data[key] = num
                elif (first and num < data[key]) or (not first and num > data[key]):
                    data[key] = num
            elif domini == 'OC':
                key = (id, agr)
                data[key] = num
            elif domini in ('OP', 'OE'):
                key = (id, agr)
                if key not in data:
                    data[key] = set()
                data[key].add(num)
            elif domini in ('OR', 'OT'):
                key = (id, agr)
                if key not in data:
                    data[key] = txt
                elif txt > data[key]:
                    data[key] = txt
        self.upload_it(partition, data)

    def upload_it(self, partition, data):
        poblacio = loads(getRedisConnection(redis_host).get('dbs_pob:{}'.format(partition)))
        poblacio = {int(key): info + [None] * self.diff for key, info in poblacio.items()}
        for (id, field), val in data.items():
            domini = field.split('_')[0]
            if domini == 'F':
                poblacio[id][self.position[field]] = '�'.join(sorted([self.atc_desc[atc] for atc in val]))
            elif domini in ('EP', 'OP'):
                poblacio[id][self.position[field]] = len(val)
            elif domini == 'OE':
                poblacio[id][self.position[field]] = '�'.join(sorted([str(int(peca)) for peca in val]))
            else:
                poblacio[id][self.position[field]] = val
        listToTable([info for id, info in poblacio.items()], dbs_tb_dev, dbs_db, format_date='YYYYMMDD')


if __name__ == '__main__':
    process = Process()
