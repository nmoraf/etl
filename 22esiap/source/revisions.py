# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


DEBUG = False
INDICADOR = "ESIAPP0401"


class Revisions(object):
    """."""

    def __init__(self):
        """."""
        self.get_revisions()
        self.execute_pool()

    def get_revisions(self):
        """."""
        sql = "select id_cip_sec, dat \
               from ped_variables, dextraccio \
               where agrupador = 498 and \
                     dat between adddate(data_ext, interval -1 year) and \
                                 data_ext"
        self.revisions = set([row for row in u.getAll(sql, "nodrizas")])

    def execute_pool(self):
        """."""
        jobs = [(k[-4:], self.revisions)
                for (k, v) in sorted(u.getSubTables("nen12").items(),
                                     key=lambda x: x[1], reverse=True)
                if v > 0]
        if DEBUG:
            Sector(jobs[-1])
        else:
            u.multiprocess(Sector, jobs, 12)


class Sector(object):
    """."""

    def __init__(self, params):
        """."""
        self.sector, self.revisions = params
        self.get_usuaris()
        self.get_nen_sa()
        self.get_poblacio()
        self.get_resultat()
        self.export()

    def get_usuaris(self):
        """."""
        self.usuaris = c.defaultdict(set)
        sql = "select ide_usuari, ide_categ_prof_c \
               from cat_pritb992 \
               where codi_sector = '{}'".format(self.sector)
        self.usuaris = {usu: cat == "30999" for (usu, cat)
                        in u.getAll(sql, "import")}

    def get_nen_sa(self):
        """."""
        self.nen_sa = c.defaultdict(set)
        sql = "select id_cip_sec, val_data, val_usu \
               from nen12_s{}, nodrizas.dextraccio \
               where val_data between adddate(data_ext, interval -1 year) and \
                                      data_ext".format(self.sector)
        for id, dat, usu in u.getAll(sql, "import"):
            key = (id, dat)
            if key in self.revisions and usu in self.usuaris:
                self.nen_sa[key].add(self.usuaris[usu])

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, ubainf \
               from ped_assignada \
               where ates = 1 and \
                     codi_sector = '{}'".format(self.sector)
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_resultat(self):
        """."""
        self.resultat = c.Counter()
        for (id, dat), usuaris in self.nen_sa.items():
            if id in self.poblacio:
                up, uba = self.poblacio[id]
                self.resultat[(INDICADOR, up, uba, "DEN")] += 1
                if any(usuaris):
                    self.resultat[(INDICADOR, up, uba, "NUM")] += 1

    def export(self):
        """."""
        upload = [k + (v,) for (k, v) in self.resultat.items()]
        u.listToTable(upload, s.TABLE, s.DATABASE)


if __name__ == "__main__":
    Revisions()
