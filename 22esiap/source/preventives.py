# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


class Preventives(object):
    """."""

    def __init__(self):
        """."""
        self.cribrats = c.defaultdict(set)
        self.cribrables = c.defaultdict(set)
        self.get_variables()
        self.get_tabac()
        self.get_problemes()
        self.get_resultat()
        self.export()

    def get_variables(self):
        """."""
        codis = {84: "ESIAP0401", 240: "ESIAP0403", 241: "ESIAP0403",
                 242: "ESIAP0403", 243: "ESIAP0403", 859: "ESIAP0403"}
        sql = "select id_cip_sec, agrupador \
               from eqa_variables, dextraccio \
               where agrupador in {} and \
                     usar = 1 and \
                     data_var between adddate(data_ext, interval -2 year) and \
                                      data_ext".format(tuple(codis.keys()))
        for id, agr in u.getAll(sql, "nodrizas"):
            self.cribrats[codis[agr]].add(id)

    def get_tabac(self):
        """."""
        sql = "select id_cip_sec, tab \
               from eqa_tabac, dextraccio \
               where last = 1 and \
                     dlast between adddate(data_ext, interval -2 year) and \
                                   data_ext"
        for id, tab in u.getAll(sql, "nodrizas"):
            self.cribrats["ESIAP0402"].add(id)
            if tab == 1:
                self.cribrables["ESIAP0403"].add(id)

    def get_problemes(self):
        """."""
        sql = "select id_cip_sec, ps \
               from eqa_problemes \
               where ps in (84, 18, 24, 239, 267, 47, 55)"
        for id, ps in u.getAll(sql, "nodrizas"):
            if ps == 84:
                self.cribrats["ESIAP0401"].add(id)
            else:
                self.cribrables["ESIAP0403"].add(id)

    def get_resultat(self):
        """."""
        self.resultat = c.Counter()
        sql = "select id_cip_sec, up, ubainf \
               from assignada_tot \
               where edat between 15 and 60 and \
                     ates = 1"
        for id, up, uba in u.getAll(sql, "nodrizas"):
            for ind in self.cribrats:
                if (ind not in self.cribrables or id in self.cribrables[ind]):
                    self.resultat[(ind, up, uba, "DEN")] += 1
                    if id in self.cribrats[ind]:
                        self.resultat[(ind, up, uba, "NUM")] += 1

    def export(self):
        """."""
        upload = [k + (v,) for (k, v) in self.resultat.items()]
        u.listToTable(upload, s.TABLE, s.DATABASE)


if __name__ == "__main__":
    Preventives()
