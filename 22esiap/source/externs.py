# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


EQA = {"ESIAP0302": ("EQAU0208", "eqa_ind"),
       "ESIAP0303": ("EQAU0209", "eqa_ind"),
       "ESIAP0304": ("EQAU0213", "eqa_ind"),
       "ESIAP0305": ("EQAU0220", "eqa_ind"),
       "ESIAP0404": ("EQAU0308", "eqa_ind"),
       "ESIAP0405": ("EQAU0309", "eqa_ind"),
       "ESIAP0406": ("EQAU0310", "eqa_ind"),
       "ESIAP0407": ("EQAU0312", "eqa_ind"),
       "ESIAP0501": ("EQAU0401", "eqa_ind"),
       "ESIAP0502": ("EQAU0402", "eqa_ind"),
       "ESIAP0503": ("EQAU0403", "eqa_ind"),
       "ESIAP0504": ("EQAU0404", "eqa_ind"),
       "ESIAPP0301": ("EQAU1102", "pedia"),
       "ESIAPP0303": ("EQAU1103", "pedia"),
       "ESIAPP0304": ("EQAU1104", "pedia"),
       "ESIAPP0305": ("EQAU1106", "pedia"),
       "ESIAPP0402": ("EQAU0703", "pedia"),
       "ESIAPP0403": ("EQAU0704", "pedia"),
       "ESIAPP0404": ("EQAU0702", "pedia"),
       "ESIAPP0405": ("EQAU0706", "pedia"),
       "ESIAPP0406": ("EQAU0713", "pedia"),
       "ESIAPP0407": ("EQAU0705", "pedia"),
       "ESIAPP0408": ("EQAU0708", "pedia")}

ALT = {"ESIAP0101": "select '{0}', up, uba, 'NUM', sum(pobatinf02_num) \
                     from altres.mst_pobatinf \
                     where edat > 14 \
                     group by up, uba \
                     union \
                     select '{0}', up, uba, 'DEN', sum(pobatinf02_den) \
                     from altres.mst_pobatinf \
                     where edat > 14 \
                     group by up, uba",
       "ESIAP0201": "select '{0}', up, prof, 'NUM', sum(num) \
                     from altres.mst_gida \
                     where ind = 'GESTINF05' and grup = 'ADU' \
                     group by up, prof \
                     union \
                     select '{0}', up, prof, 'DEN', den \
                     from altres.mst_gida \
                     where ind = 'GESTINF05' and motiu = 'PALADU'",
       "ESIAP0202": "select '{0}', up, prof, 'NUM', sum(num) \
                     from altres.mst_gida \
                     where ind = 'GESTINF06' and grup = 'ADU' \
                     group by up, prof \
                     union \
                     select '{0}', up, prof, 'DEN', den \
                     from altres.mst_gida \
                     where ind = 'GESTINF06' and motiu = 'PALADU'",
       "ESIAP0306": "select '{}', up, uba, analisi, valor \
                     from altres.exp_ambits_inf_esiap \
                     where ambit = 'ACO'",
       "ESIAP0601": "select '{0}', up, ubainf, 'NUM', sum(r2) \
                     from permanent.mst_long_cont_pacient \
                     where servei = 'INF' \
                     group by up, ubainf \
                     union \
                     select '{0}', up, ubainf, 'DEN', count(1) \
                     from permanent.mst_long_cont_pacient \
                     where servei = 'INF' \
                     group by up, ubainf",
       "ESIAP0602": "select '{0}', up, ubainf, 'NUM', sum(recomptePIIR) \
                     from altres.exp_khalix_pccmaca \
                     where edat not in ('EC01', 'EC24', 'EC59', 'EC1014') \
                     group by up, ubainf \
                     union \
                     select '{0}', up, uba, 'DEN', sum(ass) \
                     from altres.exp_khalix_pobubageneral \
                     where tipus = 'I' and \
                           edat not in ('ED0', 'ED1', 'ED2', 'ED3', 'ED4', \
                                        'ED5', 'ED6', 'ED7', 'ED8', 'ED9', \
                                        'ED10', 'ED11', 'ED12', 'ED13', \
                                        'ED14')\
                     group by up, uba",
       "ESIAPP0101": "select '{0}', up, uba, 'NUM', sum(pobatinf02_num) \
                      from altres.mst_pobatinf \
                      where edat < 15 \
                      group by up, uba \
                      union \
                      select '{0}', up, uba, 'DEN', sum(pobatinf02_den) \
                      from altres.mst_pobatinf \
                      where edat < 15 \
                      group by up, uba",
       "ESIAPP0201": "select '{0}', up, prof, 'NUM', sum(num) \
                      from altres.mst_gida \
                      where ind = 'GESTINF05' and grup = 'PED' \
                      group by up, prof \
                      union \
                      select '{0}', up, prof, 'DEN', den \
                      from altres.mst_gida \
                     where ind = 'GESTINF05' and motiu = 'PALPED'",
       "ESIAPP0202": "select '{0}', up, prof, 'NUM', sum(num) \
                      from altres.mst_gida \
                      where ind = 'GESTINF06' and grup = 'PED' \
                      group by up, prof \
                      union \
                      select '{0}', up, prof, 'DEN', den \
                      from altres.mst_gida \
                      where ind = 'GESTINF06' and motiu = 'PALPED'",
       "ESIAPP0501": "select '{0}', up, '', 'NUM', sum(numerador) \
                      from altres.exp_khalix_sie \
                      where codi_indicador = 'CONSOB01' \
                      group by up \
                      union \
                      select '{0}', up, '', 'DEN', sum(denominador) \
                      from altres.exp_khalix_sie \
                      where codi_indicador = 'CONSOB01' \
                      group by up",
       "ESIAPP0502": "select '{0}', up, '', 'NUM', sum(numerador) \
                      from altres.exp_khalix_sie \
                      where codi_indicador = 'CONSOB03' \
                      group by up \
                      union \
                      select '{0}', up, '', 'DEN', sum(denominador) \
                      from altres.exp_khalix_sie \
                      where codi_indicador = 'CONSOB03' \
                      group by up"}


for codi, (orig, db) in EQA.items():
    insert = "insert into {} select '{}', up, uba, analisis, valor \
                             from {}.exp_khalix_uba_ind \
                             where indicador = '{}' and \
                                   tipus = 'I' and \
                                   analisis in ('NUM', 'DEN')".format(s.TABLE,
                                                                      codi, db,
                                                                      orig)
    u.execute(insert, s.DATABASE)

for codi, sql in ALT.items():
    insert = "insert into {} {}".format(s.TABLE, sql.format(codi))
    u.execute(insert, s.DATABASE)
