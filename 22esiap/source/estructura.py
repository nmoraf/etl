# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


cols = ("indicador varchar(15)", "up varchar(10)", "uba varchar(10)",
        "analisi varchar(15)", "valor decimal(10, 2)")
u.createTable(s.TABLE, "({})".format(", ".join(cols)), s.DATABASE, rm=True)
