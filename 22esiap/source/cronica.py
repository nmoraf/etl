# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


INDICADOR = "ESIAP0301"
FORA = ("HTA", "DM", "MPOC", "IC")


class Cronica(object):
    """."""

    def __init__(self):
        """."""
        self.get_problemes()
        self.get_visitats()
        self.get_resultat()
        self.export()

    def get_problemes(self):
        """."""
        self.problemes = set()
        sql = "select id, ps from inf_problemes"
        for id, ps in u.getAll(sql, "nodrizas"):
            if not any([grup in FORA for grup in ps.split(",")]):
                self.problemes.add(id)

    def get_visitats(self):
        """."""
        sql = "select id from mst_pobatinf where esiap = 1"
        self.visitats = set([id for id, in u.getAll(sql, "altres")])

    def get_resultat(self):
        """."""
        self.resultat = c.Counter()
        sql = "select id_cip_sec, up, ubainf from assignada_tot \
               where edat > 14 and ates = 1"
        for id, up, uba in u.getAll(sql, "nodrizas"):
            if id in self.problemes:
                self.resultat[(up, uba, "DEN")] += 1
                if id in self.visitats:
                    self.resultat[(up, uba, "NUM")] += 1

    def export(self):
        """."""
        upload = [(INDICADOR, k[0], k[1], k[2], v)
                  for (k, v) in self.resultat.items()]
        u.listToTable(upload, s.TABLE, s.DATABASE)


if __name__ == "__main__":
    Cronica()
