# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


sql = "select indicador, 'Aperiodo', ics_codi, analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(valor) \
       from {}.{} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       where tip_eap = 'M' or \
             (tip_eap = 'A' and indicador not like 'ESIAPP%') or \
             (tip_eap = 'N' and indicador like 'ESIAPP%') \
       group by indicador, ics_codi, analisi"
u.exportKhalix(sql.format(s.DATABASE, s.TABLE), s.FILE_UP)


sql = "select indicador, 'Aperiodo', concat(ics_codi, 'I', a.uba), analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
       from {0}.{1} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       inner join eqa_ind.mst_ubas c on a.up = c.up and a.uba = c.uba \
       where indicador not like 'ESIAPP%' and \
             c.tipus = 'I' \
       union \
       select indicador, 'Aperiodo', concat(ics_codi, 'I', a.uba), analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
       from {0}.{1} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       inner join pedia.mst_ubas c on a.up = c.up and a.uba = c.uba \
       where indicador like 'ESIAPP%' and \
             c.tipus = 'I'"
u.exportKhalix(sql.format(s.DATABASE, s.TABLE), s.FILE_UBA)
