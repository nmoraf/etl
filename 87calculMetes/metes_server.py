import socket
import json
from datetime import datetime
from Queue import Queue
from threading import Thread
from metes_calcul import Metes


def respond_petition(q):
    while True:
        conn, addr = q.get()
        print 'Connected by', addr
        inici = datetime.now()

        try:
            received_json = ''
            while True:
                packet = conn.recv(4096)
                if not packet:
                    break
                received_json += packet
                if '"fi_de_la_transmissio"]' in received_json:
                    break

            received = json.loads(received_json)[0]
            if received['fx'] == 'metes':
                Metes(received['file'])
                result = 'ok'
        except Exception as e:
            result = str(e)

        temps = datetime.now() - inici
        print 'Returned', result, temps
        conn.sendall(json.dumps(result, encoding='utf8'))
        conn.close()
        q.task_done()


if __name__ == '__main__':
    HOST = ''
    PORT = 50009
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, PORT))
    s.listen(5)
    q = Queue(maxsize=0)
    for i in range(4):
        worker = Thread(target=respond_petition, args=(q,))
        worker.setDaemon(True)
        worker.start()
    while True:
        conn, addr = s.accept()
        q.put((conn, addr))
