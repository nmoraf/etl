# aquest s'executa en local

import collections
import numpy
import os

import sisapUtils as utils


period = 'DEF2018'
mixtes = {'EQA0406': ('ta', 'EQA9001'),
          'EQA0407': ('ta', 'EQA9002'),
          'EQA1201': ('tn', 'EQA9003'),
          'EQA0602': ('oa', 'EQA9117'),
          'EQA0801': ('on', 'EQA9106'),
          'EQA0802': ('on', 'EQA9103'),
          'EQA0803': ('on', 'EQA9104')}


class Metes(object):

    def __init__(self):
        self.host = 'khalix'
        self.name = 'metas'
        self.folder = 'calc_metes_eqa\\'
        self.generate_files()
        self.get_eaps()
        self.get_has_esp()
        self.get_has_uba()
        self.get_dades()
        self.get_metes()
        self.export_klx()

    def generate_files(self):
        file = 'eqa_dades.bat'
        cmd = 'cd {} && {}'.format(self.folder, file)
        status, out, err = utils.sshExecuteCommand(cmd, self.name, self.host)

    def get_file(self, file):
        utils.sshGetFile(file, self.name, self.host, subfolder=self.folder)

    @staticmethod
    def del_file(file):
        os.remove(utils.tempFolder + file)

    def get_eaps(self):
        file = 'catalegeap.txt'
        self.get_file(file)
        eap_pob = {key: set() for key in ('i', 'a', 'n', 't', 'o')}
        for row in utils.readCSV(utils.tempFolder + file, sep='{'):
            conc, eap, valor = row[0], row[2], row[8]
            if conc == 'AMBDESC' and 'Xarxa Concertada' not in valor:
                eap_pob['i'].add(eap)
            if conc == 'POBTIP' and valor != 'NENS':
                eap_pob['a'].add(eap)
            if conc == 'POBTIP' and valor != 'ADULTS':
                eap_pob['n'].add(eap)
            if conc == 'TSODOTIP' and valor in ('AMBTS', 'SENSEODO'):
                eap_pob['t'].add(eap)
            if conc == 'TSODOTIP' and valor in ('AMBTS', 'SENSETS'):
                eap_pob['o'].add(eap)
        self.eaps = collections.defaultdict(set)
        self.eaps['a'] = eap_pob['i'].intersection(eap_pob['a'])
        self.eaps['n'] = eap_pob['i'].intersection(eap_pob['n'])
        self.eaps['ta'] = self.eaps['a'].intersection(eap_pob['t'])
        self.eaps['tn'] = self.eaps['n'].intersection(eap_pob['t'])
        self.eaps['oa'] = self.eaps['a'].intersection(eap_pob['o'])
        self.eaps['on'] = self.eaps['n'].intersection(eap_pob['o'])
        Metes.del_file(file)

    def get_has_esp(self):
        file = 'ambesper.txt'
        self.get_file(file)
        self.has_esp = set([row[0] for row
                           in utils.readCSV(utils.tempFolder + file, sep='{')])
        Metes.del_file(file)

    def get_has_uba(self):
        file = 'puntsmax_mf.txt'
        self.get_file(file)
        self.has_uba = set([row[0] for row
                           in utils.readCSV(utils.tempFolder + file, sep='{')
                           if int(float(row[8])) > 0])
        Metes.del_file(file)

    def get_dades(self):
        base = 'EQA{}_DADES.txt'
        names = {'a': 'DULTS',
                 'n': 'PEDIA',
                 'ta': 'TS',
                 'on': 'ODO',
                 's': 'SSIR'}
        files = {key: base.format(val) for key, val in names.items()}
        for file in files.values():
            self.get_file(file)
        dades = collections.defaultdict(int)
        for key, file in files.items():
            origen = utils.readCSV(utils.tempFolder + file, sep='{')
            for ind, per, eap, conc, _edat, pob, _r, _r, valor in origen:
                if ind not in [v[1] for k, v in mixtes.items()]:
                    tipus = mixtes[ind][0] if ind in mixtes else key
                    if eap in self.eaps[tipus] or eap[:2] == 'SD':
                        dades[(eap, pob, ind, conc)] = float(valor)
            Metes.del_file(file)

        self.dades = {'d': collections.defaultdict(list),
                      'r': collections.defaultdict(list)}
        for (eap, pob, ind, conc), valor in dades.items():
            if conc == 'DENESPER' or (eap[:2] == 'SD' and conc == 'DEN'):
                num = dades[(eap, pob, ind, 'NUM')]
                den = dades[(eap, pob, ind, 'DEN')]
                resolucio = (num / den) if den > 0 else 0
                self.dades['r'][(pob, ind)].append(resolucio)
                if ind in self.has_esp:
                    deteccio = den / valor
                    self.dades['d'][(pob, ind)].append(deteccio)

    @staticmethod
    def get_percentil(key, meta, coef):
        if meta == 'min':
            percentil = 20
        elif meta == 'max':
            if key == 'r':
                if coef < 0.1:
                    percentil = 50
                elif coef < 0.2:
                    percentil = 60
                elif coef < 0.3:
                    percentil = 70
                else:
                    percentil = 80
            elif key == 'd':
                if coef < 0.1:
                    percentil = 20
                elif coef < 0.2:
                    percentil = 30
                elif coef < 0.3:
                    percentil = 40
                else:
                    percentil = 50
        return percentil

    def get_metes(self):
        self.export = []
        for key, dades in self.dades.items():
            for (pob, ind), valors in dades.items():
                eaps = len(valors)
                mitjana = numpy.mean(valors)
                desviacio = numpy.std(valors)
                variacio = (desviacio / mitjana) if mitjana > 0 else 0
                perc_min = Metes.get_percentil(key, 'min', variacio)
                perc_max = Metes.get_percentil(key, 'max', variacio)
                meta_min = numpy.percentile(valors, perc_min)
                meta_max = numpy.percentile(valors, perc_max)
                if key == 'r' and meta_max > 0.95:
                    meta_max = 0.95
                if key == 'r' and meta_min > meta_max * 0.95:
                    meta_min = meta_max * 0.95
                if key == 'd' and meta_max > 1:
                    meta_max = 1
                meta_int = numpy.mean((meta_min, meta_max))
                if ind in mixtes:
                    codis = (ind, mixtes[ind][1])
                else:
                    codis = (ind,)
                for codi in codis:
                    if key == 'd':
                        self.export.append((codi, period, 'NOENT', 'NUMEAPD', 'EDATS5', pob, 'SEXE', 'N', eaps))
                        self.export.append((codi, period, 'NOENT', 'COEFVARD', 'EDATS5', pob, 'SEXE', 'N', variacio))
                        self.export.append((codi, period, 'NOENT', 'PCTLMMAXD', 'EDATS5', pob, 'SEXE', 'N', perc_max))
                        self.export.append((codi, period, 'NOENT', 'PORESPER', 'EDATS5', pob, 'SEXE', 'N', 100 * meta_max))
                        if codi in self.has_uba:
                            self.export.append((codi, period, 'NOENT', 'PORESPERU', 'EDATS5', pob, 'SEXE', 'N', 100 * meta_max * 0.8))
                    elif key == 'r':
                        self.export.append((codi, period, 'NOENT', 'NUMEAP', 'EDATS5', pob, 'SEXE', 'N', eaps))
                        self.export.append((codi, period, 'NOENT', 'COEFVAR', 'EDATS5', pob, 'SEXE', 'N', variacio))
                        self.export.append((codi, period, 'NOENT', 'PCTLMMIN', 'EDATS5', pob, 'SEXE', 'N', perc_min))
                        self.export.append((codi, period, 'NOENT', 'PCTLMMAX', 'EDATS5', pob, 'SEXE', 'N', perc_max))
                        self.export.append((codi, period, 'NOENT', 'AGMMINRES', 'EDATS5', pob, 'SEXE', 'N', 100 * meta_min))
                        self.export.append((codi, period, 'NOENT', 'AGMINTRES', 'EDATS5', pob, 'SEXE', 'N', 100 * meta_int))
                        self.export.append((codi, period, 'NOENT', 'AGMMAXRES', 'EDATS5', pob, 'SEXE', 'N', 100 * meta_max))

    def export_klx(self):
        file = 'EQA_METES_{}.TXT'.format(period)
        utils.writeCSV(utils.tempFolder + file, self.export, sep='{')
        utils.sshPutFile(file, self.name, self.host, subfolder=self.folder)


Metes()
