# -*- coding: latin-1
from sisapUtils import tempFolder, readCSV, writeCSV, sshExecuteCommand, sshGetFile, sshPutFile
from collections import defaultdict
from numpy import std, mean, percentile
from os import remove


class Metes(object):

    def __init__(self, file_in):
        self.file_in = file_in
        self.get_file()
        self.get_eap()
        self.get_indicadors()
        self.get_periodes()
        self.get_data_list()
        self.get_statistics()
        self.export_klx()
        self.export_xls()

    def get_file(self):
        file = '{}.txt'.format(self.file_in)
        sshGetFile(file, 'metas', 'khalix')
        self.data = [row for row in readCSV(tempFolder + file, sep='{')]
        remove(tempFolder + file)

    def get_eap(self):
        host = 'khalix'
        name = 'bridge'
        folder = 'out_calc_metes\\'
        file = 'CATALEGEAP.{}'
        command = 'cd {} && {}'.format(folder, file.format('bat'))
        status, stdout, stderr = sshExecuteCommand(command, name, host)
        sshGetFile(file.format('txt'), name, host, subfolder=folder)
        ics = set([row[2] for row in readCSV(tempFolder + file.format('txt'), sep='{') if row[0] == 'AMBDESC' and 'Xarxa Concertada' not in row[8]])
        self.eap = {row[2]: row[8][0] for row in readCSV(tempFolder + file.format('txt'), sep='{') if row[0] == 'POBTIP' and row[2] in ics}
        remove(tempFolder + file.format('txt'))

    def get_indicadors(self):
        self.indicadors = {}
        indicadors_pob = {}
        for row in self.data:
            if row[2][:2] in ('SD', 'PR'):
                self.indicadors[row[0]] = row[2][0]
            elif row[2] in self.eap and row[3] == 'DEN':
                if row[0] not in indicadors_pob:
                    indicadors_pob[row[0]] = {'A': 0, 'N': 0}
                if self.eap[row[2]] in ('A', 'N'):
                    indicadors_pob[row[0]][self.eap[row[2]]] += int(float(row[8]))
        for i, d in indicadors_pob.items():
            pob_a = 0 if not d['A'] else 1.0 * d['A'] / len([eap for eap, tip in self.eap.items() if tip == 'A'])
            pob_n = 0 if not d['N'] else 1.0 * d['N'] / len([eap for eap, tip in self.eap.items() if tip == 'N'])
            if (pob_a > pob_n * 2 and pob_n < 100) or pob_n == 0:
                self.indicadors[i] = 'A'
            elif pob_n > pob_a * 2 and pob_a < 100:
                self.indicadors[i] = 'N'
            else:
                self.indicadors[i] = 'M'

    def get_periodes(self):
        self.basal = {}
        self.periode = {}
        self.correccio = {}
        for row in self.data:
            if row[0] not in self.periode:
                if row[1][:3] == 'INI':
                    periode = 'DEF{}'.format(row[1][3:7])
                    correccio = 0
                elif row[1][3:5] in ('11', '12'):
                    periode = 'DEF20{}'.format(int(row[1][1:3]) + 1)
                    correccio = 0
                else:
                    periode = 'DEF20{}'.format(row[1][1:3])
                    correccio = 5 * (int(row[1][3:5]) - 1)
                self.basal[row[0]] = row[1]
                self.periode[row[0]] = periode
                self.correccio[row[0]] = correccio

    def get_data_list(self):
        self.data_list = defaultdict(list)
        numerador = {}
        denominador = {}
        for row in self.data:
            indicador, periode, entitat, concepte = row[:4]
            key = tuple(row[2:3] + row[0:1] + row[4:8])
            n = float(row[8])
            tipus_indicador = self.indicadors[row[0]]
            if entitat in self.eap:
                tipus_eap = self.eap[entitat]
            elif entitat[:2] in ('SD', 'PR'):
                tipus_eap = entitat[0]
            else:
                continue
            if tipus_indicador == tipus_eap or tipus_eap == 'M' or tipus_indicador == 'M':
                if concepte == 'NUM':
                    numerador[key] = n
                elif concepte == 'DEN':
                    denominador[key] = n
        for key, n in denominador.items():
            resultat = (numerador[key] if key in numerador else 0.0) / n
            self.data_list[key[1:]].append(resultat)

    def get_statistics(self):
        self.statistics = {}
        for key, valors in self.data_list.items():
            n = len(valors)
            cv = std(valors) / mean(valors)
            pmin = 20
            pmax = self.get_pmax(cv)
            pmax_c = max([pmax - self.correccio[key[0]], 50])
            mmax = min(percentile(valors, pmax_c), 0.95)
            mmin = min(percentile(valors, pmin), mmax * 0.95)            
            mint = mean([mmin, mmax])
            self.statistics[key] = {'n': n, 'cv': 100 * cv, 'pmin': pmin, 'pmax': pmax, 'pmax_c': pmax_c, 'mmin': 100 * mmin, 'mmax': 100 * mmax, 'mint': 100 * mint}

    def get_pmax(self, coef):
        return 50 if coef < 0.1 else 60 if coef < 0.2 else 70 if coef < 0.3 else 80

    def export_klx(self):
        file = '{}_METES.TXT'.format(self.file_in)
        upload = []
        for key, statistics in self.statistics.items():
            upload.append(key[0:1] + (self.periode[key[0]], 'NOENT', 'AGMMIN') + key[1:] + (statistics['mmin'],))
            upload.append(key[0:1] + (self.periode[key[0]], 'NOENT', 'AGMINT') + key[1:] + (statistics['mint'],))
            upload.append(key[0:1] + (self.periode[key[0]], 'NOENT', 'AGMMAX') + key[1:] + (statistics['mmax'],))
        writeCSV(tempFolder + file, upload, sep='{')
        sshPutFile(file, 'metas', 'khalix')

    def export_xls(self):
        file = '{}_METES.CSV'.format(self.file_in)
        header = [['ind', 'tipus', 'basal', 'pob', 'n_eap', 'cv', 'pmin', 'pmax', 'pmax_c', 'mmin', 'mint', 'mmax']]
        upload = []
        for key, statistics in self.statistics.items():
            row = (key[0], self.indicadors[key[0]], self.basal[key[0]], key[2], statistics['n'], statistics['cv'], statistics['pmin'], statistics['pmax'], statistics['pmax_c'],
                   statistics['mmin'], statistics['mint'], statistics['mmax'])
            upload.append(row)
        writeCSV(tempFolder + file, header + sorted([map(self.format_number, row) for row in upload]), sep=';')
        sshPutFile(file, 'metas', 'khalix')

    def format_number(self, num):
        if isinstance(num, float):
            num = str(round(num, 2)).replace('.', ',')
        return num
