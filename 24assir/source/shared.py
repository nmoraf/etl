# coding: latin1

"""
.
"""


QC_TABLE = "sisap_assir_qc"
QC_DATABASE = "redics"
QC_USERS = ("PREDUPRP", "PREDUMMP", "PREDUECR", "PREDULMB", "PREDUEHE")

DATABASE = "assir"
VISITES_TMP = "tmp_visites"
POBLACIO = "pac_poblacio"
ASSIGNACIO = "cat_assignacio"
VISITES = "pac_visites"
CATALEG = "cat_centres"
REVISAR = "rev_centres"
KHALIX_TB = "exp_khalix"
KHALIX_FILE = "QC_ASSIR"
CAT_UMI_TB = "sisap_rel_eap_assir"
CAT_UMI_DB = "pdp"

ESPECIALITATS = {"10099": "GIN", "10098": "GIN",
                 "30084": "LLE", "30085": "LLE"}
