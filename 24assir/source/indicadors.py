# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


AGRUPATS = {"GINE": "MODEL_JI", "TJOVE": "MODEL_JI",
            "EMB_PRI": "MODEL_JI", "EMB_SUC": "MODEL_JI"}

AGRUPATS2 = {"EMB_SUC": "EMB","EMB_PRI": "EMB",
             "ECO3": "EMB","ECO2": "EMB","ECO1": "EMB",
             "PINVO": "EMB",
             "PRGINE": "NOEMB","TJOVE": "NOEMB",
             "PPERI_DOM": "NOEMB","PPERI_CON": "NOEMB",
             "PSIC": "NOEMB","VIRTUAL": "NOEMB","TELF": "NOEMB",
             "GINE": "NOEMB","ECOG": "NOEMB","ALTRE": "NOEMB"}

class QC(object):
    """."""

    def __init__(self):
        """."""
        self.resultat = c.Counter()
        self.get_poblacio()
        self.get_visites()
        self.upload_data()

    def get_poblacio(self):
        """."""
        sql = "select assir, eap, 1, sexe, edat from {}".format(s.POBLACIO)
        for key in u.getAll(sql, s.DATABASE):
            self.resultat[key + ("TOT", "POBLACIO", "PERSONES")] += 1

    def get_visites(self):
        """."""
        atesos = c.defaultdict(set)
        sql = "select id, espe, up, eap, up = assir, sexe, edat, grup \
               from {}".format(s.VISITES)
        for id, _espe, up, eap, zona, sexe, edat, _tip in u.getAll(sql, s.DATABASE):  # noqa
            for espe in ("TOT", s.ESPECIALITATS.get(_espe, "ALT")):
                key = (up, eap, zona, sexe, edat, espe)
                tipus = [_tip]
                if _tip in AGRUPATS:
                    tipus.append(AGRUPATS[_tip])
                if _tip in AGRUPATS2:
                    tipus.append(AGRUPATS2[_tip])
                for tip in tipus:
                    self.resultat[key + (tip, "VISITES")] += 1
                    for ind in ("ATESA", tip):
                        atesos[key + (ind, "PERSONES")].add(id)
        for key, pacients in atesos.items():
            self.resultat[key] = len(pacients)

    def upload_data(self):
        """."""
        cols = "(assir varchar2(5), eap varchar2(5), zona int, \
                 sexe varchar2(1), edat int, especialitat varchar2(3), \
                 indicador varchar(10), metrica varchar2(10), valor int)"
        u.createTable(s.QC_TABLE, cols, s.QC_DATABASE, rm=True)
        u.grantSelect(s.QC_TABLE, s.QC_USERS, s.QC_DATABASE)
        upload = [k + (v,) for (k, v) in self.resultat.items()]
        u.listToTable(upload, s.QC_TABLE, s.QC_DATABASE)

if __name__ == "__main__":
    QC()
