# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.get_motius()
        self.get_centres()
        self.get_excepcions()
        self.get_homologacio()
        self.get_visites()
        self.upload_data()

    def get_motius(self):
        """."""
        self.motius = c.defaultdict(set)
        sql = "select id_cip, pavi_cod_cen, pavi_cla_cen, pavi_servei, \
                      pavi_modul, pavi_data_v, pavi_num_v, pavi_seg_tipus \
               from assir221, nodrizas.dextraccio \
               where pavi_data_v between \
                            adddate(data_ext, interval -1 year) and \
                            data_ext"
        for row in u.getAll(sql, "import"):
            self.motius[row[:-1]].add(row[-1])

    def get_centres(self):
        """."""
        sql = "select u_pr_cod_up \
               from cat_pritb008 \
               where u_pr_descripcio like '%ASSIR%' or \
                     u_pr_descripcio like '%PAD %' or \
                     u_pr_descripcio like '% DONA%'"
        self.assir = set([sd for sd, in u.getAll(sql, "import")])

    def get_excepcions(self):
        """."""
        sql = "select scs_codi, if(amb_codi = '01', '00034', '00530') \
               from cat_centres where amb_codi in ('01', '09')"
        self.excepcions = {up: sd for (up, sd) in u.getAll(sql, "nodrizas")}
        self.excepcions["00026"] = "00034"
        self.excepcions["00005"] = "00530"
        self.excepcions["00429"] = "00410"
        self.excepcions["00433"] = "00412"
        self.excepcions["01417"] = "01415"
        self.excepcions["01421"] = "01422"
        self.excepcions["04678"] = "01422"
        self.excepcions["01935"] = "01422"

    def get_homologacio(self):
        """."""
        sql = "select codi_sector, tv_tipus, tv_cita, tv_homol \
               from cat_vistb206"
        self.homologacio = {row[:3]: row[3] for row in u.getAll(sql, "import")}

    @staticmethod
    def _get_grup(tipus, homol, espe, motius):
        """."""
        if tipus in ("9E", "CNP"):
            grup = "VIRTUAL"
        elif tipus in ("9T", "VTEL", "CK", "TEL", "TELF", "TFE", "TLF"):
            grup = "TELF"
        elif tipus in ("BC", "AMC"):
            grup = "PINVO"
        elif tipus in ("HIST", "CMN", "CMA", "GCRI", "CRIO"):
            grup = "PRGINE"
        elif homol in ("ECO1", "ECO2", "ECO3", "ECOG"):
            grup = homol
        elif tipus in ("ECOU", "EGIN"):
            grup = "ECOG"
        elif homol == "PEMB" or tipus == "PTOC" or "EP" in motius:
            grup = "EMB_PRI"
        elif homol == "SEMB" or tipus == "STOC" or "ES" in motius:
            grup = "EMB_SUC"
        elif (tipus in ("PDOM", "PUED") or
              (tipus == "DOM" and s.ESPECIALITATS.get(espe) == "LLE") or
              any([key in motius for key in ("PPD", "PSD")])):
            grup = "PPERI_DOM"
        elif homol == "VPSI" or espe == "02004":
            grup = "PSIC"
        elif (tipus == "PUER" or
              (tipus == "PUEC" and s.ESPECIALITATS.get(espe) == "LLE") or
              any([key in motius for key in ("PPC", "PSC")])):
            grup = "PPERI_CON"
        elif ((tipus == "TJ" and homol == "VGIN") or
              (tipus in ("CJ", "JOVE") and espe in s.ESPECIALITATS)):
            grup = "TJOVE"
        elif (homol in ("VGIN", "PREV", "VURG") or
              (tipus in ("PREF", "URG", "U", "APRE", "CITO", "DIU", "GINE") and espe in s.ESPECIALITATS) or  # noqa
              (tipus in ("IVE", "PGIN", "SGII", "SGIN", "SITS") and s.ESPECIALITATS.get(espe) == "GIN") or  # noqa
              (tipus in ("IVE", "PGIN", "SGIN", "TSC") and s.ESPECIALITATS.get(espe) == "LLE")):  # noqa
            grup = "GINE"
        else:
            grup = "ALTRE"
        return grup

    def get_visites(self):
        """."""
        self.visites = []
        assir = tuple(self.assir)
        codis = tuple(s.ESPECIALITATS.keys())
        sql = "select id_cip, visi_centre_codi_centre, \
                      visi_centre_classe_centre, visi_servei_codi_servei, \
                      visi_modul_codi_modul, visi_data_visita, \
                      visi_numero_visita, visi_up, s_espe_codi_especialitat, \
                      codi_sector, visi_tipus_visita, visi_tipus_citacio \
               from visites1, nodrizas.dextraccio \
               where visi_situacio_visita = 'R' and \
                     visi_data_visita between \
                            adddate(data_ext, interval -1 year) and \
                            data_ext and \
                     (visi_up in {} or \
                      s_espe_codi_especialitat in {})".format(assir, codis)
        for row in u.getAll(sql, "import"):
            id = row[0]
            key = row[:7]
            motius = ",".join(self.motius[key])
            up, espe, sec, tip, cit = row[7:12]
            cod = self.excepcions.get(up, up)
            homol = self.homologacio.get((sec, tip, cit))
            grup = Visites._get_grup(tip, homol, espe, self.motius[key])
            self.visites.append((id, cod, espe, tip, cit, homol, motius, grup))

    def upload_data(self):
        """."""
        cols = "(id int, up varchar(5), espe varchar(5), tipus varchar(4), \
                 citacio varchar(1), homologat varchar(4), \
                 motius varchar(255), grup varchar(25))"
        u.createTable(s.VISITES_TMP, cols, s.DATABASE, rm=True)
        u.listToTable(self.visites, s.VISITES_TMP, s.DATABASE)


if __name__ == "__main__":
    Visites()
