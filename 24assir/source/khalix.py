# coding: latin1

"""
.
"""

import collections as c
import simplejson as j

import shared as s
import sisapUtils as u


class Khalix(object):
    """."""

    def __init__(self):
        """."""
        self.periode = "A{}{}".format(*u.getKhalixDates()[1:])
        self.get_indicadors()
        self.get_assirs()
        self.get_centres()
        self.get_dades()
        self.get_agrupats()
        self.export()

    @staticmethod
    def get_codi(codi):
        """."""
        return "ASSIR{}".format(codi.zfill(4))

    def get_indicadors(self):
        """."""
        self.indicadors = c.defaultdict(set)
        self.agrupacions = c.defaultdict(set)
        file = 'dades_noesb/khalix.json'
        try:
            stream = open(file)
        except IOError:
            stream = open('../' + file)
        origen = j.load(stream)
        for (codi, analisi), (zona_o, sexe_o, edat_o, espe_o, ind_o, met_o), agr_o in origen:  # noqa
            indicador = (Khalix.get_codi(codi), analisi)
            for zona in zona_o:
                for sexe in sexe_o:
                    for edat in range(edat_o[0], edat_o[1] + 1):
                        for espe in espe_o:
                            for ind in ind_o:
                                for met in met_o:
                                    key = (zona, sexe, edat, espe, ind, met)
                                    self.indicadors[key].add(indicador)
            for (codi_a, analisi_a) in agr_o:
                indicador_a = (Khalix.get_codi(codi_a), analisi_a)
                self.agrupacions[indicador].add(indicador_a)

    def get_assirs(self):
        """."""
        sql = "select up, br from {}".format(s.CATALEG)
        self.assirs = {up: "S" + sd[1:] for (up, sd)
                       in u.getAll(sql, s.DATABASE)}

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_dades(self):
        """."""
        self.dades = c.Counter()
        sql = "select assir, eap, zona, sexe, edat, especialitat, \
                      indicador, metrica, valor \
               from {}".format(s.QC_TABLE)
        for assir_o, eap_o, zona, sexe, edat, espe, ind, met, val in u.getAll(sql, s.QC_DATABASE):  # noqa
            this = (zona, sexe, edat, espe, ind, met)
            if self.indicadors[this]:
                assir = self.assirs[assir_o]
                eap = "A{}".format(self.centres.get(eap_o, "B" + eap_o[1:]))
                for (codi, analisi) in self.indicadors[this]:
                    key = (codi, self.periode, assir, analisi, "NOCAT",
                           "NOIMP", eap, "N")
                    self.dades[key] += val

    def get_agrupats(self):
        """."""
        for key, value in self.dades.items():
            old_key = (key[0], key[3])
            if old_key in self.agrupacions:
                for new_codi, new_analisi in self.agrupacions[old_key]:
                    new_key = (new_codi, key[1], key[2], new_analisi,
                               key[4], key[5], key[6], key[7])
                    self.dades[new_key] += value

    def export(self):
        """."""
        cols = ["k{} varchar(10)".format(i) for i in range(8)] + ["v int"]
        cols_s = "({})".format(", ".join(cols))
        u.createTable(s.KHALIX_TB, cols_s, s.DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.dades.items()]
        u.listToTable(upload, s.KHALIX_TB, s.DATABASE)
        u.exportKhalix("select * from {}.{}".format(s.DATABASE, s.KHALIX_TB),
                       s.KHALIX_FILE)


if __name__ == "__main__":
    Khalix()
