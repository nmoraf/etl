# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


class Poblacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_data()
        self.get_assignacio()
        self.get_visites()
        self.get_creuament()
        self.get_oficial()
        self.get_cataleg()
        self.get_poblacio()
        self.set_visites()
        self.get_descripcions()

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = set([up for up, in u.getAll(sql, "nodrizas")])

    def get_data(self):
        """."""
        self.demos = {}
        self.dades = c.defaultdict(lambda: c.defaultdict(set))
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        sql = "select id_cip, usua_uab_up, usua_up_rca, \
                      usua_sexe, usua_data_naixement \
               from assignada \
               where usua_situacio = 'A'"
        for id, up, rca, sexe, naix in u.getAll(sql, "import"):
            self.demos[id] = (sexe, u.yearsBetween(naix, dext))
            if up:
                self.dades[id]["up"].add(up)
            if rca:
                self.dades[id]["rca"].add(rca)

    def get_assignacio(self):
        """."""
        self.assignacio = {}
        for id, dades in self.dades.items():
            if len(dades["up"]) == 1:
                self.assignacio[id] = dades["up"].pop()
            elif len(dades["up"]) == 0 and len(dades["rca"]) == 1:
                this = dades["rca"].pop()
                if this not in self.centres:
                    self.assignacio[id] = this

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(c.Counter)
        self.visites_for_later = []
        sql = "select * from {}".format(s.VISITES_TMP)
        for row in u.getAll(sql, s.DATABASE):
            id, up = row[:2]
            self.visites[id][up] += 1
            self.visites_for_later.append(row)

    def get_oficial(self):
        """."""
        sql = "select eap, assir from cat_sisap_eap_assir"
        self.oficial = {up: sd for (up, sd) in u.getAll(sql, "import")}

    def get_creuament(self):
        """."""
        self.creuament = c.defaultdict(c.Counter)
        for id, up in self.assignacio.items():
            sexe, edat = self.demos[id]
            if sexe == "D" and 19 < edat < 50:
                self.creuament[up][("pob", up)] += 1
                for sd, n in self.visites[id].items():
                    self.creuament[up][("at", sd)] += 1
                    self.creuament[up][("vis", sd)] += n

    def get_cataleg(self):
        """."""
        self.cataleg = []
        for up, dades in self.creuament.items():
            pob = float(dades[("pob", up)])
            ateses = {sd: n for (_x, sd), n in dades.items() if _x == "at"}
            total = float(sum(ateses.values()))
            perc_total = total / pob
            oficial = self.oficial.get(up)
            ord = sorted([row for row in ateses.items()], key=lambda x: x[1],
                         reverse=True)
            if ord:
                sd, n = ord[0]
                perc_sd = n / pob
                perc_sd_total = n / total
            else:
                sd, n, perc_sd, perc_sd_total = "", 0, 0, 0
            include = all([pob > 249, sd, perc_total >= 0.05])
            this = (up, 1 * include, sd, oficial, 1 * (up in self.centres),
                    pob, total, perc_total, oficial, n, perc_sd, perc_sd_total)
            self.cataleg.append(this)
        for up, sd in self.oficial.items():
            if up not in self.creuament:
                this = (up, 9, "", sd, 0, 0, 0, 0, 0, 0, 0)
                self.cataleg.append(this)
        cols = "(eap varchar(5), include int, assir varchar(5), \
                 ofi varchar(5), ecap int, ass int, at int, at_ass double, \
                 at_sd int, at_ass_sd double, perc_sd double)"
        u.createTable(s.ASSIGNACIO, cols, s.DATABASE, rm=True)
        u.listToTable(self.cataleg, s.ASSIGNACIO, s.DATABASE)

    def get_poblacio(self):
        """."""
        sql = "select eap, assir from {} \
               where include = 1".format(s.ASSIGNACIO)
        cataleg = {up: assir for (up, assir) in u.getAll(sql, s.DATABASE)}
        self.assirs = set(cataleg.values())
        self.poblacio = {id: (up, cataleg[up]) + self.demos[id] + (1 * any([assir in self.assirs for assir in self.visites[id]]),)  # noqa
                         for (id, up) in self.assignacio.items()
                         if up in cataleg}
        cols = "(id int, eap varchar(5), assir varchar(5), \
                 sexe varchar(1), edat int, ates int)"
        u.createTable(s.POBLACIO, cols, s.DATABASE, rm=True)
        upload = [(k,) + v for (k, v) in self.poblacio.items()]
        u.listToTable(upload, s.POBLACIO, s.DATABASE)

    def set_visites(self):
        """."""
        upload = [(row[0],) + self.poblacio[row[0]] + row[1:]
                  for row in self.visites_for_later
                  if row[0] in self.poblacio and row[1] in self.assirs]
        cols = [u.getColumnInformation(column, s.POBLACIO, s.DATABASE)["create"]  # noqa
                for column in u.getTableColumns(s.POBLACIO, s.DATABASE)]
        cols += [u.getColumnInformation(column, s.VISITES_TMP, s.DATABASE)["create"]  # noqa
                 for column in u.getTableColumns(s.VISITES_TMP, s.DATABASE)
                 if column != "id"]
        cols_s = "({})".format(", ".join(cols))
        u.createTable(s.VISITES, cols_s, s.DATABASE, rm=True)
        u.listToTable(upload, s.VISITES, s.DATABASE)

    def get_descripcions(self):
        """."""
        sql = "select up_codi_up_scs, a.up_codi_up_ics, up_desc_up_ics, 1 \
               from cat_gcctb008 a inner join cat_gcctb007 b \
               on a.up_codi_up_ics = b.up_codi_up_ics \
               where a.up_data_baixa = 0"
        relacio = {row[0]: row for row in u.getAll(sql, "import")}
        sql = "select u_pr_cod_up, u_pr_cod_up, max(u_pr_descripcio), 2 \
               from cat_pritb008 \
               group by u_pr_cod_up"
        falten = {row[0]: row for row in u.getAll(sql, "import")}
        sql = "select distinct assir from {0} \
               where include = 1".format(s.ASSIGNACIO)
        cataleg = [relacio.get(up, falten[up])
                   for up, in u.getAll(sql, s.DATABASE)]
        cols = "(up varchar(5), br varchar(5), des varchar(255), font int)"
        u.createTable(s.CATALEG, cols, s.DATABASE, rm=True)
        u.listToTable(cataleg, s.CATALEG, s.DATABASE)
        sql = "select up, count(1) from {} \
               where up not in (select assir from {}) \
               group by up".format(s.VISITES_TMP, s.ASSIGNACIO)
        revisar = [relacio.get(up, falten[up]) + (n,)
                   for (up, n) in u.getAll(sql, s.DATABASE)]
        cols = "(up varchar(5), br varchar(5), des varchar(255), visites int)"
        u.createTable(s.REVISAR, cols, s.DATABASE, rm=True)
        u.listToTable(revisar, s.REVISAR, s.DATABASE)
        sql = "select a.eap, a.ecap, a.assir, b.br, b.des \
               from {} a inner join {} b on a.assir = b.up \
               where include = 1".format(s.ASSIGNACIO, s.CATALEG)
        cataleg_umi = [relacio.get(row[0], (row[0], None, None))[:3] + row[1:]
                       for row in u.getAll(sql, s.DATABASE)]
        cols = "(eap_cod1 varchar2(5), eap_cod2 varchar2(5), \
                 eap_des varchar2(255), eap_ecap int, assir_cod1 varchar2(5), \
                 assir_cod2 varchar2(5), assir_des varchar2(255))"
        u.createTable(s.CAT_UMI_TB, cols, s.CAT_UMI_DB, rm=True)
        u.listToTable(cataleg_umi, s.CAT_UMI_TB, s.CAT_UMI_DB)


if __name__ == "__main__":
    Poblacio()
